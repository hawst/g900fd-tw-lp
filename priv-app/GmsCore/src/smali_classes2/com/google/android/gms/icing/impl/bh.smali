.class public final Lcom/google/android/gms/icing/impl/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/ContentProviderClient;

.field final b:Landroid/database/Cursor;


# direct methods
.method private constructor <init>(Landroid/content/ContentProviderClient;Landroid/database/Cursor;)V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/bh;->a:Landroid/content/ContentProviderClient;

    .line 67
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    .line 68
    return-void
.end method

.method public static a(Landroid/content/ContentResolver;Landroid/net/Uri;[Ljava/lang/String;)Lcom/google/android/gms/icing/impl/bh;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 33
    :try_start_0
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_0

    .line 34
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireUnstableContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 41
    :goto_0
    if-nez v0, :cond_1

    .line 42
    const-string v0, "Could not connect to content provider %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v6

    .line 56
    :goto_1
    return-object v0

    .line 36
    :cond_0
    :try_start_1
    invoke-virtual {p0, p1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Landroid/net/Uri;)Landroid/content/ContentProviderClient;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 38
    :catch_0
    move-exception v0

    .line 39
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 47
    :cond_1
    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x0

    move-object v1, p1

    move-object v4, p2

    :try_start_2
    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    move-result-object v2

    .line 52
    if-nez v2, :cond_2

    .line 53
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    move-object v0, v6

    .line 54
    goto :goto_1

    .line 48
    :catch_1
    move-exception v1

    .line 49
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    .line 50
    new-instance v0, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 56
    :cond_2
    new-instance v1, Lcom/google/android/gms/icing/impl/bh;

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/icing/impl/bh;-><init>(Landroid/content/ContentProviderClient;Landroid/database/Cursor;)V

    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 82
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 83
    :catch_0
    move-exception v0

    .line 84
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 107
    :catch_0
    move-exception v0

    .line 108
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 90
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b(I)I
    .locals 2

    .prologue
    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 115
    :catch_0
    move-exception v0

    .line 116
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->isAfterLast()Z
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final c(I)J
    .locals 2

    .prologue
    .line 122
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bh;->b:Landroid/database/Cursor;

    invoke-interface {v0, p1}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    return-wide v0

    .line 123
    :catch_0
    move-exception v0

    .line 124
    new-instance v1, Lcom/google/android/gms/icing/impl/bi;

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/bi;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method
