.class public final Lcom/google/android/gms/drive/query/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/util/List;

.field public b:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/query/k;->a:Ljava/util/List;

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/query/k;->b:Z

    .line 44
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/SortOrder;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/gms/drive/query/SortOrder;

    iget-object v1, p0, Lcom/google/android/gms/drive/query/k;->a:Ljava/util/List;

    iget-boolean v2, p0, Lcom/google/android/gms/drive/query/k;->b:Z

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/drive/query/SortOrder;-><init>(Ljava/util/List;ZB)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/metadata/k;)Lcom/google/android/gms/drive/query/k;
    .locals 4

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/drive/query/k;->a:Ljava/util/List;

    new-instance v1, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;

    invoke-interface {p1}, Lcom/google/android/gms/drive/metadata/k;->a()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/query/internal/FieldWithSortOrder;-><init>(Ljava/lang/String;Z)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 56
    return-object p0
.end method
