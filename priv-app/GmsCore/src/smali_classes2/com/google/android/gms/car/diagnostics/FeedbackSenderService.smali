.class public Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/car/on;

.field private static final b:Ljava/lang/Object;


# instance fields
.field private final c:Lcom/google/android/gms/car/diagnostics/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->b:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/gms/car/diagnostics/e;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/diagnostics/e;-><init>(Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->c:Lcom/google/android/gms/car/diagnostics/e;

    .line 42
    return-void
.end method

.method static synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/car/on;)V
    .locals 2

    .prologue
    .line 29
    sget-object v1, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 30
    :try_start_0
    sput-object p0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->a:Lcom/google/android/gms/car/on;

    .line 31
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic b()Lcom/google/android/gms/car/on;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->a:Lcom/google/android/gms/car/on;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->c:Lcom/google/android/gms/car/diagnostics/e;

    return-object v0
.end method
