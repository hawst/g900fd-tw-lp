.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;
.super Lcom/google/android/gms/games/realtime/network/s;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Ljava/lang/String;

.field private j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V
    .locals 0

    .prologue
    .line 277
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/s;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    .line 278
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    .line 293
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 284
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->e:Ljava/lang/String;

    .line 285
    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v2, v2, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;-><init>(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/realtime/network/a;Lcom/google/android/gms/games/service/statemachine/d;)V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    .line 286
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->c()V

    .line 287
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 297
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 403
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->g:Z

    :goto_0
    return v0

    .line 300
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/d;

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "OP: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/gms/games/jingle/d;->d:I

    invoke-static {v3}, Lcom/google/android/gms/games/jingle/m;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 302
    iget v1, v0, Lcom/google/android/gms/games/jingle/d;->d:I

    const/16 v2, 0xf

    if-eq v1, v2, :cond_0

    .line 303
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "CALL_STATE_CHANGED_OP: New state: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/gms/games/jingle/d;->d:I

    invoke-static {v3}, Lcom/google/android/gms/games/jingle/m;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 305
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 307
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->g:Z

    goto :goto_0

    .line 311
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/j;

    .line 312
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 315
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/g;

    .line 316
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/g;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 319
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/f;

    .line 320
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 323
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/f;

    .line 324
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto :goto_0

    .line 327
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/e;

    .line 328
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 331
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/i;

    .line 332
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 335
    :sswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/m;

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/m;->b:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 339
    :sswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/h;

    .line 340
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/h;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 343
    :sswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/g;

    .line 344
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 347
    :sswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/n;

    .line 348
    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/n;->b:[Ljava/lang/String;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 349
    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    invoke-virtual {v4, v3, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    .line 348
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 351
    :cond_1
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 354
    :sswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/i;

    .line 355
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/i;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 358
    :sswitch_c
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/j;

    .line 359
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/j;->a:Ljava/lang/String;

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    move-result v0

    goto/16 :goto_0

    .line 363
    :sswitch_d
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/b;

    .line 364
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v2, v0, Lcom/google/android/gms/games/jingle/b;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/b;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/realtime/network/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 369
    :sswitch_e
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/l;

    .line 370
    sget-object v1, Lcom/google/android/gms/games/c/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 371
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/l;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->e:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 373
    :cond_2
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 376
    :sswitch_f
    sget-object v0, Lcom/google/android/gms/games/c/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 377
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->b()V

    .line 379
    :cond_3
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 382
    :sswitch_10
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 383
    const/16 v1, 0x177a

    iput v1, v0, Landroid/os/Message;->what:I

    .line 384
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->j:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Landroid/os/Message;)V

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->e(Ljava/lang/String;)V

    .line 386
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->f:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/r;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/r;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->a(Ljava/lang/Runnable;)V

    .line 392
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 395
    :sswitch_11
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/k;

    .line 396
    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/k;->a:Z

    if-nez v0, :cond_4

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/a;->b()V

    .line 398
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b()V

    .line 400
    :cond_4
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->f:Z

    goto/16 :goto_0

    .line 297
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_11
        0x2 -> :sswitch_0
        0x65 -> :sswitch_3
        0x66 -> :sswitch_2
        0xca -> :sswitch_d
        0x12d -> :sswitch_6
        0x12e -> :sswitch_1
        0x191 -> :sswitch_5
        0x1773 -> :sswitch_10
        0x1774 -> :sswitch_c
        0x1775 -> :sswitch_9
        0x1776 -> :sswitch_8
        0x1777 -> :sswitch_7
        0x1778 -> :sswitch_a
        0x1779 -> :sswitch_4
        0x177a -> :sswitch_b
        0x177b -> :sswitch_e
        0x177c -> :sswitch_f
    .end sparse-switch
.end method
