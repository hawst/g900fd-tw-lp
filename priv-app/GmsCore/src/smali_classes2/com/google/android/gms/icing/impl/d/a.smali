.class public final Lcom/google/android/gms/icing/impl/d/a;
.super Lcom/google/android/gms/icing/impl/d/b;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/clearcut/a;

.field private final b:Lcom/google/android/gms/common/api/v;

.field private c:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/d/b;-><init>(Landroid/content/Context;)V

    .line 22
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->c:Ljava/lang/Object;

    .line 26
    new-instance v0, Lcom/google/android/gms/clearcut/a;

    const/16 v1, 0xc

    invoke-direct {v0, p1, v1, v2, v2}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->a:Lcom/google/android/gms/clearcut/a;

    .line 29
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    .line 48
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 49
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->a:Lcom/google/android/gms/clearcut/a;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    const-wide/16 v4, 0xa

    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v4, v5, v3}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z

    .line 51
    const-string v0, "Logger flush failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 54
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final a(Ljava/lang/String;Lcom/google/k/f/an;Z)V
    .locals 2

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/a;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/d/a;->a:Lcom/google/android/gms/clearcut/a;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/clearcut/a;->a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/clearcut/c;->a(Ljava/lang/String;)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/d/a;->b:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 45
    return-void

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
