.class public final Lcom/google/android/gms/games/service/a/m/c;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/service/b;

.field private final e:Lcom/google/android/gms/games/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/c;->c:Lcom/google/android/gms/games/internal/dr;

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/m/c;->d:Lcom/google/android/gms/games/service/b;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/c;->e:Lcom/google/android/gms/games/a/au;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/c;->d:Lcom/google/android/gms/games/service/b;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/c;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 47
    :cond_0
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/c;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->G(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 40
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/c;->d:Lcom/google/android/gms/games/service/b;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 34
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/c;->e:Lcom/google/android/gms/games/a/au;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 33
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/c;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    goto :goto_0
.end method
