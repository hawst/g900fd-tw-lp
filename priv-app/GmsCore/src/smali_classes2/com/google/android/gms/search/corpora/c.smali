.class public Lcom/google/android/gms/search/corpora/c;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;)V
    .locals 6

    .prologue
    .line 23
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 25
    return-void
.end method

.method private c()Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/16 v4, 0x8

    .line 29
    new-instance v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    invoke-direct {v2}, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;-><init>()V

    .line 30
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v0, v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 31
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 33
    if-eqz v0, :cond_0

    .line 34
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v1, v4, v0, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v1, v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 50
    :goto_0
    return-object v2

    .line 36
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/c;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v0, Lcom/google/android/gms/icing/impl/a/aa;

    .line 38
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/search/corpora/c;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;

    iget-object v1, v1, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Request;->b:Ljava/lang/String;

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/aa;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/icing/impl/b/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_2

    goto :goto_0

    .line 39
    :catch_0
    move-exception v0

    .line 40
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v1, v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0

    .line 42
    :catch_1
    move-exception v0

    .line 43
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/d;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v1, v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0

    .line 45
    :catch_2
    move-exception v0

    .line 46
    new-instance v1, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Ljava/lang/SecurityException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v4, v0, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    iput-object v1, v2, Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/search/corpora/c;->c()Lcom/google/android/gms/search/corpora/ClearCorpusCall$Response;

    move-result-object v0

    return-object v0
.end method
