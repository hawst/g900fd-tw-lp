.class public final Lcom/google/android/gms/car/jv;
.super Lcom/google/android/gms/car/np;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field final a:Lcom/google/android/gms/car/jw;

.field b:I

.field c:I

.field d:I

.field e:I

.field volatile f:Landroid/view/Surface;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ns;)V
    .locals 2

    .prologue
    const/4 v0, 0x1

    const/4 v1, -0x1

    .line 37
    invoke-direct {p0, v0, v0, p1}, Lcom/google/android/gms/car/np;-><init>(ZZLcom/google/android/gms/car/ns;)V

    .line 26
    invoke-static {}, Lcom/google/android/gms/car/jv;->c()Lcom/google/android/gms/car/jw;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/jv;->a:Lcom/google/android/gms/car/jw;

    .line 27
    iput v1, p0, Lcom/google/android/gms/car/jv;->b:I

    .line 28
    iput v1, p0, Lcom/google/android/gms/car/jv;->c:I

    .line 29
    iput v1, p0, Lcom/google/android/gms/car/jv;->d:I

    .line 38
    return-void
.end method

.method public static c()Lcom/google/android/gms/car/jw;
    .locals 13

    .prologue
    const/4 v3, 0x0

    const v5, 0x3d0900

    const/16 v6, 0x2d0

    const/16 v4, 0x1e

    const/4 v1, 0x0

    .line 157
    const-string v8, "video/avc"

    invoke-static {}, Landroid/media/MediaCodecList;->getCodecCount()I

    move-result v9

    move v7, v1

    :goto_0
    if-ge v7, v9, :cond_2

    invoke-static {v7}, Landroid/media/MediaCodecList;->getCodecInfoAt(I)Landroid/media/MediaCodecInfo;

    move-result-object v2

    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->isEncoder()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v2}, Landroid/media/MediaCodecInfo;->getSupportedTypes()[Ljava/lang/String;

    move-result-object v10

    move v0, v1

    :goto_1
    array-length v11, v10

    if-ge v0, v11, :cond_1

    aget-object v11, v10, v0

    invoke-virtual {v11, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v11

    if-eqz v11, :cond_0

    move-object v0, v2

    .line 158
    :goto_2
    const-string v2, "video/avc"

    invoke-virtual {v0, v2}, Landroid/media/MediaCodecInfo;->getCapabilitiesForType(Ljava/lang/String;)Landroid/media/MediaCodecInfo$CodecCapabilities;

    move-result-object v2

    .line 159
    if-nez v2, :cond_3

    .line 230
    :goto_3
    :sswitch_0
    return-object v3

    .line 157
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_2
    move-object v0, v3

    goto :goto_2

    .line 162
    :cond_3
    new-instance v7, Lcom/google/android/gms/car/jw;

    invoke-direct {v7}, Lcom/google/android/gms/car/jw;-><init>()V

    .line 164
    iget-object v8, v2, Landroid/media/MediaCodecInfo$CodecCapabilities;->profileLevels:[Landroid/media/MediaCodecInfo$CodecProfileLevel;

    array-length v9, v8

    move v2, v1

    :goto_4
    if-ge v2, v9, :cond_5

    aget-object v10, v8, v2

    .line 165
    iget v11, v10, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    if-le v11, v1, :cond_4

    .line 166
    iget v1, v10, Landroid/media/MediaCodecInfo$CodecProfileLevel;->level:I

    .line 164
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 169
    :cond_5
    sparse-switch v1, :sswitch_data_0

    .line 214
    const/16 v6, 0x780

    .line 215
    const/16 v2, 0x438

    move v3, v5

    move v12, v4

    move v4, v2

    move v2, v12

    .line 220
    :goto_5
    iput v6, v7, Lcom/google/android/gms/car/jw;->a:I

    .line 221
    iput v4, v7, Lcom/google/android/gms/car/jw;->b:I

    .line 222
    iput v2, v7, Lcom/google/android/gms/car/jw;->c:I

    .line 223
    iput v3, v7, Lcom/google/android/gms/car/jw;->d:I

    .line 224
    invoke-virtual {v0}, Landroid/media/MediaCodecInfo;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v7, Lcom/google/android/gms/car/jw;->e:Ljava/lang/String;

    .line 225
    const-string v0, "CAR.VIDEO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 226
    const-string v0, "CAR.VIDEO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "AVC Level 0x"

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " bit rate "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " fps "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, v7, Lcom/google/android/gms/car/jw;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " w "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move-object v3, v7

    .line 230
    goto/16 :goto_3

    .line 183
    :sswitch_1
    const/16 v6, 0x160

    .line 184
    const/16 v3, 0x240

    .line 186
    const/16 v2, 0x19

    move v4, v3

    move v3, v5

    .line 187
    goto :goto_5

    .line 190
    :sswitch_2
    const/16 v3, 0x1e0

    .line 192
    const/16 v2, 0xf

    move v4, v3

    move v3, v5

    .line 193
    goto :goto_5

    .line 196
    :sswitch_3
    const/16 v3, 0x1e0

    .line 197
    const v2, 0x989680

    move v12, v4

    move v4, v3

    move v3, v2

    move v2, v12

    .line 199
    goto :goto_5

    .line 201
    :sswitch_4
    const/16 v3, 0x500

    .line 203
    const v2, 0x1e8480

    move v12, v4

    move v4, v6

    move v6, v3

    move v3, v2

    move v2, v12

    .line 205
    goto :goto_5

    .line 207
    :sswitch_5
    const/16 v3, 0x500

    .line 209
    const v2, 0x1312d00

    move v12, v4

    move v4, v6

    move v6, v3

    move v3, v2

    move v2, v12

    .line 211
    goto/16 :goto_5

    .line 169
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x4 -> :sswitch_0
        0x8 -> :sswitch_0
        0x10 -> :sswitch_0
        0x20 -> :sswitch_0
        0x40 -> :sswitch_1
        0x80 -> :sswitch_2
        0x100 -> :sswitch_3
        0x200 -> :sswitch_4
        0x400 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method protected final a()Landroid/media/MediaCodec;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 87
    const-string v0, "video/avc"

    iget v1, p0, Lcom/google/android/gms/car/jv;->b:I

    iget v2, p0, Lcom/google/android/gms/car/jv;->c:I

    invoke-static {v0, v1, v2}, Landroid/media/MediaFormat;->createVideoFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 89
    const-string v1, "color-format"

    const v2, 0x7f000789

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 91
    const-string v1, "bitrate"

    iget v2, p0, Lcom/google/android/gms/car/jv;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 92
    const-string v1, "frame-rate"

    iget v2, p0, Lcom/google/android/gms/car/jv;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 93
    const-string v1, "i-frame-interval"

    const v2, 0x83d600

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/car/jv;->a:Lcom/google/android/gms/car/jw;

    iget-object v1, v1, Lcom/google/android/gms/car/jw;->e:Ljava/lang/String;

    invoke-static {v1}, Landroid/media/MediaCodec;->createByCodecName(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    .line 97
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v3, v3, v2}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 98
    invoke-virtual {v1}, Landroid/media/MediaCodec;->createInputSurface()Landroid/view/Surface;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/jv;->f:Landroid/view/Surface;

    .line 99
    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    .line 100
    return-object v1
.end method

.method public final declared-synchronized b()V
    .locals 1

    .prologue
    .line 108
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/jv;->f:Landroid/view/Surface;

    if-eqz v0, :cond_0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/car/jv;->f:Landroid/view/Surface;

    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 110
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/jv;->f:Landroid/view/Surface;

    .line 112
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/car/np;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 113
    monitor-exit p0

    return-void

    .line 108
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
