.class public final Lcom/google/android/gms/games/e/d;
.super Lcom/google/android/gms/common/data/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/e/a;


# instance fields
.field private final c:I

.field private final d:Lcom/google/android/gms/games/Game;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;II)V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 28
    iput p3, p0, Lcom/google/android/gms/games/e/d;->c:I

    .line 29
    new-instance v0, Lcom/google/android/gms/games/GameRef;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/GameRef;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/e/d;->d:Lcom/google/android/gms/games/Game;

    .line 30
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 34
    const-string v0, "external_leaderboard_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/e/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 44
    const-string v0, "name"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/e/d;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    .line 45
    return-void
.end method

.method public final synthetic c()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/games/e/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/e/c;-><init>(Lcom/google/android/gms/games/e/a;)V

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 39
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/e/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 49
    const-string v0, "board_icon_image_uri"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/e/d;->i(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 88
    invoke-static {p0, p1}, Lcom/google/android/gms/games/e/c;->a(Lcom/google/android/gms/games/e/a;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 54
    const-string v0, "board_icon_image_url"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/e/d;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 59
    const-string v0, "score_order"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/e/d;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final h()Ljava/util/ArrayList;
    .locals 5

    .prologue
    .line 64
    new-instance v1, Ljava/util/ArrayList;

    iget v0, p0, Lcom/google/android/gms/games/e/d;->c:I

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 65
    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/gms/games/e/d;->c:I

    if-ge v0, v2, :cond_0

    .line 66
    new-instance v2, Lcom/google/android/gms/games/e/m;

    iget-object v3, p0, Lcom/google/android/gms/games/e/d;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v4, p0, Lcom/google/android/gms/games/e/d;->c_:I

    add-int/2addr v4, v0

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/e/m;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 65
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 68
    :cond_0
    return-object v1
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 83
    invoke-static {p0}, Lcom/google/android/gms/games/e/c;->a(Lcom/google/android/gms/games/e/a;)I

    move-result v0

    return v0
.end method

.method public final i()Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/e/d;->d:Lcom/google/android/gms/games/Game;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    invoke-static {p0}, Lcom/google/android/gms/games/e/c;->b(Lcom/google/android/gms/games/e/a;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
