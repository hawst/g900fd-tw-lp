.class public final Lcom/google/android/gms/car/qd;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private b:I

.field private c:Landroid/os/PowerManager$WakeLock;

.field private final d:I

.field private final e:Lcom/google/android/gms/car/gx;

.field private f:Z

.field private g:Z

.field private h:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gx;I)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-boolean v0, p0, Lcom/google/android/gms/car/qd;->f:Z

    .line 29
    iput-boolean v0, p0, Lcom/google/android/gms/car/qd;->g:Z

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/car/qd;->e:Lcom/google/android/gms/car/gx;

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->F()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    .line 35
    iput p2, p0, Lcom/google/android/gms/car/qd;->d:I

    .line 36
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 43
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.SYS"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const-string v0, "CAR.SYS"

    const-string v2, "starting car mode settings"

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    const-string v2, "power"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    invoke-static {}, Lcom/google/android/gms/car/ie;->a()Z

    move-result v2

    if-eqz v2, :cond_3

    :goto_0
    const-string v2, "CarService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 48
    iget v0, p0, Lcom/google/android/gms/car/qd;->d:I

    if-eqz v0, :cond_2

    .line 49
    iget-boolean v0, p0, Lcom/google/android/gms/car/qd;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_4

    .line 57
    :cond_2
    :goto_1
    monitor-exit p0

    return-void

    .line 46
    :cond_3
    const/4 v1, 0x6

    goto :goto_0

    .line 53
    :cond_4
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/qd;->f:Z

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->i()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->k()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/qd;->b:I

    :goto_2
    const-string v0, "screen_off_timeout"

    const/16 v2, 0x3a98

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->i()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "CAR.SYS"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "CAR.SYS"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "saving settings, screen timeout:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/qd;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget v1, p0, Lcom/google/android/gms/car/qd;->b:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/id;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 43
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 54
    :cond_6
    :try_start_2
    const-string v0, "screen_off_timeout"

    const/16 v2, 0x3a98

    invoke-static {v1, v0, v2}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/qd;->b:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method final a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    const-string v3, "uimode"

    invoke-virtual {v0, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 90
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v3

    const/4 v4, 0x3

    if-ne v3, v4, :cond_2

    .line 91
    const-string v3, "CAR.SYS"

    invoke-static {v3, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 92
    const-string v3, "CAR.SYS"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "setting night mode "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_0
    if-eqz p1, :cond_1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->setNightMode(I)V

    .line 100
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 94
    goto :goto_0

    .line 97
    :cond_2
    iput-boolean v2, p0, Lcom/google/android/gms/car/qd;->g:Z

    .line 98
    iput-boolean p1, p0, Lcom/google/android/gms/car/qd;->h:Z

    goto :goto_1
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 63
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.SYS"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "CAR.SYS"

    const-string v1, "end car mode and restore settings"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->isHeld()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/qd;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 68
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/qd;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 77
    :goto_0
    monitor-exit p0

    return-void

    .line 71
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/car/qd;->d:I

    if-eqz v0, :cond_4

    .line 73
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/qd;->b:I

    if-eqz v1, :cond_3

    const-string v1, "screen_off_timeout"

    iget v2, p0, Lcom/google/android/gms/car/qd;->b:I

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 74
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->j()V

    .line 76
    :cond_4
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/qd;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c()V
    .locals 4

    .prologue
    const/4 v1, 0x3

    .line 128
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/qd;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 147
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 132
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    const-string v2, "uimode"

    invoke-virtual {v0, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 134
    invoke-virtual {v0}, Landroid/app/UiModeManager;->getCurrentModeType()I

    move-result v2

    if-eq v2, v1, :cond_0

    .line 138
    const/4 v2, 0x1

    .line 139
    invoke-static {}, Lcom/google/android/gms/car/ie;->a()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 142
    :goto_1
    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->enableCarMode(I)V

    .line 144
    iget-boolean v0, p0, Lcom/google/android/gms/car/qd;->g:Z

    if-eqz v0, :cond_0

    .line 145
    iget-boolean v0, p0, Lcom/google/android/gms/car/qd;->h:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/qd;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 128
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_2
    move v1, v2

    goto :goto_1
.end method

.method public final declared-synchronized d()V
    .locals 2

    .prologue
    .line 150
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/qd;->d:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 157
    :goto_0
    monitor-exit p0

    return-void

    .line 154
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qd;->a:Landroid/content/Context;

    const-string v1, "uimode"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/UiModeManager;

    .line 156
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/UiModeManager;->disableCarMode(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
