.class public final Lcom/google/android/gms/udc/util/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static b:Ljava/util/regex/Pattern;


# instance fields
.field public a:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 318
    return-void
.end method

.method public static a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;
    .locals 1

    .prologue
    .line 124
    invoke-static {p0}, Lcom/google/android/gms/udc/util/f;->b(Lcom/google/android/gms/udc/e/s;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;ZLandroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 175
    const-string v0, "Root view must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    if-eqz p4, :cond_0

    if-eqz p5, :cond_5

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 177
    if-eqz p4, :cond_1

    if-eqz p6, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 179
    invoke-static {p3}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v3

    .line 180
    if-eqz p4, :cond_c

    .line 181
    iget-object v0, p3, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/udc/util/f;->b:Ljava/util/regex/Pattern;

    if-nez v1, :cond_3

    const-string v1, "<a(\\s+|\\s+[^>]*?\\s+)href\\s*=\\s*(\'([^\']*)\'|\"([^\"]*)\")(\\s+|\\s+[^>]*?\\s+)auth\\s*=\\s*[\'\"]yes[\'\"]([^>]*?)>"

    invoke-static {v1}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v1

    sput-object v1, Lcom/google/android/gms/udc/util/f;->b:Ljava/util/regex/Pattern;

    :cond_3
    sget-object v1, Lcom/google/android/gms/udc/util/f;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    :cond_4
    :goto_1
    invoke-virtual {v4}, Ljava/util/regex/Matcher;->find()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v2, ""

    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v0

    const/4 v1, 0x4

    invoke-virtual {v4, v1}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v1

    if-eqz v0, :cond_6

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v6

    if-eqz v6, :cond_6

    :goto_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    invoke-interface {v5, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_5
    move v0, v1

    .line 176
    goto :goto_0

    .line 181
    :cond_6
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v0

    if-eqz v0, :cond_b

    move-object v0, v1

    goto :goto_2

    :cond_7
    invoke-static {v3, p5, v5, p6}, Lcom/google/android/gms/udc/util/f;->a(Ljava/lang/CharSequence;Landroid/support/v4/app/q;Ljava/util/Set;Ljava/lang/String;)Ljava/lang/CharSequence;

    move-result-object v0

    move-object v1, v0

    .line 186
    :goto_3
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 187
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 188
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 189
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    iget-object v1, p0, Lcom/google/android/gms/udc/util/f;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_8

    if-eqz p3, :cond_8

    iget-object v1, p3, Lcom/google/android/gms/udc/e/s;->a:[B

    if-eqz v1, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/udc/util/f;->a:Ljava/util/ArrayList;

    iget-object v2, p3, Lcom/google/android/gms/udc/e/s;->a:[B

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 194
    :cond_8
    :goto_4
    if-eqz p4, :cond_9

    .line 195
    invoke-static {}, Landroid/text/method/LinkMovementMethod;->getInstance()Landroid/text/method/MovementMethod;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setMovementMethod(Landroid/text/method/MovementMethod;)V

    .line 203
    :cond_9
    :goto_5
    return-object v0

    .line 192
    :cond_a
    const-string v1, "UdcUiUtil"

    const-string v2, "Tried setting text, but text was empty"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_4

    .line 199
    :catch_0
    move-exception v0

    const-string v0, "UdcUiUtil"

    const-string v1, "Tried setting text, but couldn\'t find view"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 203
    :goto_6
    const/4 v0, 0x0

    goto :goto_5

    .line 200
    :catch_1
    move-exception v0

    .line 201
    const-string v1, "UdcUiUtil"

    const-string v2, "Tried setting text, but not on a TextView"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_6

    :cond_b
    move-object v0, v2

    goto :goto_2

    :cond_c
    move-object v1, v3

    goto :goto_3
.end method

.method private static a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 3

    .prologue
    .line 93
    if-eqz p0, :cond_0

    .line 95
    :try_start_0
    invoke-static {p1, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 101
    :goto_0
    return-object p1

    .line 97
    :catch_0
    move-exception v0

    .line 98
    const-string v1, "UdcUiUtil"

    const-string v2, "Error unbundling proto"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 101
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/CharSequence;Landroid/support/v4/app/q;Ljava/util/Set;Ljava/lang/String;)Ljava/lang/CharSequence;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 299
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 300
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2, p0}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 301
    invoke-interface {v2}, Landroid/text/Spannable;->length()I

    move-result v0

    const-class v3, Landroid/text/style/URLSpan;

    invoke-interface {v2, v1, v0, v3}, Landroid/text/Spannable;->getSpans(IILjava/lang/Class;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/text/style/URLSpan;

    .line 302
    array-length v3, v0

    if-lez v3, :cond_2

    .line 303
    array-length v4, v0

    move v3, v1

    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v1, v0, v3

    .line 304
    invoke-interface {v2, v1}, Landroid/text/Spannable;->getSpanStart(Ljava/lang/Object;)I

    move-result v5

    .line 305
    invoke-interface {v2, v1}, Landroid/text/Spannable;->getSpanEnd(Ljava/lang/Object;)I

    move-result v6

    .line 306
    invoke-interface {v2, v1}, Landroid/text/Spannable;->getSpanFlags(Ljava/lang/Object;)I

    move-result v7

    .line 307
    invoke-interface {v2, v1}, Landroid/text/Spannable;->removeSpan(Ljava/lang/Object;)V

    .line 308
    new-instance v8, Lcom/google/android/gms/udc/util/UdcAuthUrlSpan;

    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v1}, Landroid/text/style/URLSpan;->getURL()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p2, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v1, p3

    :goto_1
    invoke-direct {v8, p1, v9, v1}, Lcom/google/android/gms/udc/util/UdcAuthUrlSpan;-><init>(Landroid/support/v4/app/q;Ljava/lang/String;Ljava/lang/String;)V

    .line 310
    invoke-interface {v2, v8, v5, v6, v7}, Landroid/text/Spannable;->setSpan(Ljava/lang/Object;III)V

    .line 303
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 308
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    :cond_1
    move-object p0, v2

    .line 315
    :cond_2
    return-object p0
.end method

.method public static a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V
    .locals 1

    .prologue
    .line 77
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;

    .line 78
    return-void

    .line 77
    :cond_0
    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V
    .locals 1

    .prologue
    .line 81
    if-nez p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, p1, v0}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 82
    return-void

    .line 81
    :cond_0
    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Landroid/view/View;Ljava/lang/CharSequence;)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 260
    const/4 v0, 0x0

    .line 261
    if-eqz p0, :cond_3

    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 262
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    move-object v2, v0

    .line 264
    :goto_0
    if-eqz v2, :cond_0

    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 294
    :cond_0
    :goto_1
    return-void

    .line 267
    :cond_1
    const-string v0, "accessibility"

    invoke-virtual {v2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 272
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 277
    const/16 v1, 0x10

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/16 v1, 0x4000

    .line 283
    :goto_2
    invoke-static {v1}, Landroid/view/accessibility/AccessibilityEvent;->obtain(I)Landroid/view/accessibility/AccessibilityEvent;

    move-result-object v1

    .line 284
    invoke-virtual {v1}, Landroid/view/accessibility/AccessibilityEvent;->getText()Ljava/util/List;

    move-result-object v3

    invoke-interface {v3, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 285
    invoke-virtual {p0}, Landroid/view/View;->isEnabled()Z

    move-result v3

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setEnabled(Z)V

    .line 286
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/view/accessibility/AccessibilityEvent;->setClassName(Ljava/lang/CharSequence;)V

    .line 287
    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/accessibility/AccessibilityEvent;->setPackageName(Ljava/lang/CharSequence;)V

    .line 290
    new-instance v2, Landroid/support/v4/view/a/af;

    invoke-direct {v2, v1}, Landroid/support/v4/view/a/af;-><init>(Ljava/lang/Object;)V

    .line 291
    invoke-virtual {v2, p0}, Landroid/support/v4/view/a/af;->a(Landroid/view/View;)V

    .line 293
    invoke-virtual {v0, v1}, Landroid/view/accessibility/AccessibilityManager;->sendAccessibilityEvent(Landroid/view/accessibility/AccessibilityEvent;)V

    goto :goto_1

    .line 277
    :cond_2
    const/16 v1, 0x8

    goto :goto_2

    :cond_3
    move-object v2, v0

    goto :goto_0
.end method

.method public static b(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 85
    invoke-virtual {p0, p1}, Landroid/content/Intent;->getByteArrayExtra(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/udc/util/f;->a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 89
    invoke-virtual {p0, p1}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/gms/udc/util/f;->a([BLcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    return-object v0
.end method

.method public static b(Lcom/google/android/gms/udc/e/s;)Z
    .locals 1

    .prologue
    .line 128
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 165
    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;ZLandroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;Landroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;
    .locals 7

    .prologue
    .line 170
    const/4 v4, 0x1

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;ZLandroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/view/View;ILcom/google/android/gms/udc/e/k;Lcom/google/android/gms/udc/e/k;Lcom/android/volley/toolbox/m;)Lcom/android/volley/toolbox/NetworkImageView;
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 214
    const-string v0, "Root view must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 215
    const-string v0, "Image resource must not be null"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 220
    :try_start_0
    invoke-virtual {p1, p2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/android/volley/toolbox/NetworkImageView;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 225
    :goto_0
    if-eqz v0, :cond_2

    iget-object v2, p3, Lcom/google/android/gms/udc/e/k;->b:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 226
    if-eqz p4, :cond_0

    iget-object v1, p4, Lcom/google/android/gms/udc/e/k;->b:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 228
    iget-object v1, p4, Lcom/google/android/gms/udc/e/k;->b:Ljava/lang/String;

    new-instance v2, Lcom/google/android/gms/udc/util/g;

    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/udc/util/g;-><init>(Landroid/widget/ImageView;Landroid/content/res/Resources;)V

    invoke-virtual {p5, v1, v2, v4, v4}, Lcom/android/volley/toolbox/m;->a(Ljava/lang/String;Lcom/android/volley/toolbox/t;II)Lcom/android/volley/toolbox/s;

    .line 231
    :cond_0
    iget-object v1, p3, Lcom/google/android/gms/udc/e/k;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p5}, Lcom/android/volley/toolbox/NetworkImageView;->a(Ljava/lang/String;Lcom/android/volley/toolbox/m;)V

    .line 232
    invoke-virtual {v0, v4}, Lcom/android/volley/toolbox/NetworkImageView;->setVisibility(I)V

    .line 233
    iget-object v1, p0, Lcom/google/android/gms/udc/util/f;->a:Ljava/util/ArrayList;

    if-eqz v1, :cond_1

    if-eqz p3, :cond_1

    iget-object v1, p3, Lcom/google/android/gms/udc/e/k;->a:[B

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/udc/util/f;->a:Ljava/util/ArrayList;

    iget-object v2, p3, Lcom/google/android/gms/udc/e/k;->a:[B

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_1
    :goto_1
    return-object v0

    .line 221
    :catch_0
    move-exception v0

    .line 222
    const-string v2, "UdcUiUtil"

    const-string v3, "Found view, but not a LoadingImageView"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_0

    .line 236
    :cond_2
    const-string v0, "UdcUiUtil"

    const-string v2, "Tried loading image, but couldn\'t find view"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 238
    goto :goto_1
.end method
