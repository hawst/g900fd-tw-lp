.class final Lcom/google/android/gms/common/api/ak;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/aj;Landroid/os/Looper;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/gms/common/api/ak;->a:Lcom/google/android/gms/common/api/aj;

    .line 140
    invoke-direct {p0, p2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 141
    return-void
.end method


# virtual methods
.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 145
    iget v1, p1, Landroid/os/Message;->what:I

    if-ne v1, v0, :cond_0

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/common/api/ak;->a:Lcom/google/android/gms/common/api/aj;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/api/al;

    iget-object v1, v1, Lcom/google/android/gms/common/api/aj;->a:Ljava/lang/Object;

    if-nez v1, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/common/api/al;->a()V

    .line 147
    :goto_1
    return-void

    .line 145
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 146
    :cond_1
    :try_start_0
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/al;->a(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v2, "ListenerHolder"

    const-string v3, "Notifying listener failed"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-interface {v0}, Lcom/google/android/gms/common/api/al;->a()V

    goto :goto_1
.end method
