.class public final Lcom/google/android/gms/car/pb;
.super Lcom/google/android/gms/car/oi;
.source "SourceFile"


# static fields
.field private static final n:[F


# instance fields
.field private k:I

.field private l:I

.field private m:Ljava/nio/FloatBuffer;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 27
    sput-object v0, Lcom/google/android/gms/car/pb;->n:[F

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 28
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 37
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/car/oi;-><init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V

    .line 20
    iput v0, p0, Lcom/google/android/gms/car/pb;->k:I

    .line 21
    iput v0, p0, Lcom/google/android/gms/car/pb;->l:I

    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/car/pb;->x()Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/on;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/pb;->k:I

    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/car/pb;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/car/on;->i()I

    move-result v0

    :goto_1
    iput v0, p0, Lcom/google/android/gms/car/pb;->l:I

    .line 42
    return-void

    .line 38
    :cond_0
    const/16 v0, 0x4e

    goto :goto_0

    .line 40
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/on;->j()I

    move-result v0

    goto :goto_1
.end method

.method private x()Z
    .locals 2

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/gms/car/pb;->g:I

    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/pb;->g:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a(F[F)V
    .locals 8

    .prologue
    const/16 v4, 0x14

    const/4 v1, 0x3

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 87
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/car/oi;->a(F[F)V

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/car/on;->c(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v7}, Lcom/google/android/gms/car/on;->b(I)Lcom/google/android/gms/car/ot;

    move-result-object v6

    .line 94
    iget v0, v6, Lcom/google/android/gms/car/ot;->b:I

    iget-boolean v2, p0, Lcom/google/android/gms/car/pb;->i:Z

    if-eqz v2, :cond_0

    iget-object p2, p0, Lcom/google/android/gms/car/pb;->j:[F

    :cond_0
    invoke-static {v0, v7, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 97
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 98
    const/16 v0, 0xde1

    iget v2, p0, Lcom/google/android/gms/car/pb;->l:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 100
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 101
    invoke-static {v5, v5, v5, p1}, Landroid/opengl/GLES20;->glBlendColor(FFFF)V

    .line 102
    const/16 v0, 0x302

    const/16 v2, 0x303

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 104
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 106
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 109
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    const/4 v1, 0x2

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 111
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 112
    iget v0, v6, Lcom/google/android/gms/car/ot;->c:I

    sget-object v1, Lcom/google/android/gms/car/pb;->n:[F

    invoke-static {v0, v7, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 113
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    .line 114
    return-void
.end method

.method protected final i()V
    .locals 11

    .prologue
    const/4 v10, 0x3

    const/4 v9, 0x0

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v7, 0x0

    .line 51
    invoke-super {p0}, Lcom/google/android/gms/car/oi;->i()V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->b:Lcom/google/android/gms/car/ok;

    iget v0, v0, Lcom/google/android/gms/car/ok;->a:I

    int-to-float v1, v0

    .line 56
    invoke-direct {p0}, Lcom/google/android/gms/car/pb;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->b:Lcom/google/android/gms/car/ok;

    iget v0, v0, Lcom/google/android/gms/car/ok;->f:I

    int-to-float v0, v0

    .line 61
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/pb;->b:Lcom/google/android/gms/car/ok;

    iget v2, v2, Lcom/google/android/gms/car/ok;->c:I

    int-to-float v2, v2

    .line 62
    iget v3, p0, Lcom/google/android/gms/car/pb;->k:I

    int-to-float v3, v3

    .line 63
    iget-object v4, p0, Lcom/google/android/gms/car/pb;->b:Lcom/google/android/gms/car/ok;

    iget v4, v4, Lcom/google/android/gms/car/ok;->g:I

    int-to-float v4, v4

    .line 64
    add-float/2addr v2, v1

    .line 65
    add-float/2addr v3, v0

    .line 67
    const/16 v5, 0x14

    new-array v5, v5, [F

    aput v1, v5, v9

    const/4 v6, 0x1

    aput v0, v5, v6

    const/4 v6, 0x2

    aput v4, v5, v6

    aput v8, v5, v10

    const/4 v6, 0x4

    aput v8, v5, v6

    const/4 v6, 0x5

    aput v2, v5, v6

    const/4 v6, 0x6

    aput v0, v5, v6

    const/4 v0, 0x7

    aput v4, v5, v0

    const/16 v0, 0x8

    aput v7, v5, v0

    const/16 v0, 0x9

    aput v8, v5, v0

    const/16 v0, 0xa

    aput v1, v5, v0

    const/16 v0, 0xb

    aput v3, v5, v0

    const/16 v0, 0xc

    aput v4, v5, v0

    const/16 v0, 0xd

    aput v8, v5, v0

    const/16 v0, 0xe

    aput v7, v5, v0

    const/16 v0, 0xf

    aput v2, v5, v0

    const/16 v0, 0x10

    aput v3, v5, v0

    const/16 v0, 0x11

    aput v4, v5, v0

    const/16 v0, 0x12

    aput v7, v5, v0

    const/16 v0, 0x13

    aput v7, v5, v0

    .line 74
    array-length v0, v5

    mul-int/lit8 v0, v0, 0x4

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocateDirect(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->asFloatBuffer()Ljava/nio/FloatBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->m:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v5}, Ljava/nio/FloatBuffer;->put([F)Ljava/nio/FloatBuffer;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 78
    const-string v0, "CAR.WM.WINSHADOW"

    invoke-static {v0, v10}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 79
    const-string v0, "CAR.WM.WINSHADOW"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setNewVerticesData "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    return-void

    .line 59
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/pb;->b:Lcom/google/android/gms/car/ok;

    iget v0, v0, Lcom/google/android/gms/car/ok;->b:I

    iget v2, p0, Lcom/google/android/gms/car/pb;->k:I

    sub-int/2addr v0, v2

    int-to-float v0, v0

    goto/16 :goto_0
.end method
