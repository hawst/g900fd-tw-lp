.class final Lcom/google/android/gms/car/iu;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ip;)V
    .locals 1

    .prologue
    .line 621
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 622
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 630
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/iu;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 631
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/iu;->sendMessage(Landroid/os/Message;)Z

    .line 632
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/it;)V
    .locals 1

    .prologue
    .line 625
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/iu;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 626
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/iu;->sendMessage(Landroid/os/Message;)Z

    .line 627
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/it;)V
    .locals 1

    .prologue
    .line 635
    const/4 v0, 0x3

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/iu;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 636
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/iu;->sendMessage(Landroid/os/Message;)Z

    .line 637
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ip;

    .line 642
    if-nez v0, :cond_1

    .line 666
    :cond_0
    :goto_0
    return-void

    .line 646
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 660
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown display event message"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 648
    :pswitch_0
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/car/it;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/it;)V

    goto :goto_0

    .line 652
    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->f()V

    goto :goto_0

    .line 656
    :pswitch_2
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/car/it;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/gms/car/it;)V

    goto :goto_0

    .line 646
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
