.class public final Lcom/google/android/gms/games/ui/signin/b;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x1

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x4

    const/4 v2, 0x1

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->k()Z

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/i/b;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 36
    if-eqz v0, :cond_0

    .line 37
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/b;->a(Ljava/lang/String;)V

    .line 38
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/b;->b(I)V

    .line 63
    :goto_0
    return-void

    .line 43
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->f(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 45
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    .line 46
    if-ne v1, v2, :cond_2

    .line 47
    const/4 v1, 0x0

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/b;->a(Ljava/lang/String;)V

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/b;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/j/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->a(Z)V

    .line 57
    :cond_1
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/b;->b(I)V

    goto :goto_0

    .line 61
    :cond_2
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/b;->b(I)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 30
    const/4 v0, 0x2

    return v0
.end method
