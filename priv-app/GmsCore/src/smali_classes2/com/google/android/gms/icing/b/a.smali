.class public abstract Lcom/google/android/gms/icing/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/Object;

.field public b:Ljava/util/Set;

.field public final c:Ljava/util/Set;

.field public final d:Landroid/os/ConditionVariable;

.field public e:I

.field final f:Ljava/lang/ThreadLocal;

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    .line 42
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    .line 45
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/a;->c:Ljava/util/Set;

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    .line 51
    new-instance v0, Landroid/os/ConditionVariable;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/a;->d:Landroid/os/ConditionVariable;

    .line 66
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    .line 290
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 106
    const-string v0, "Scheduling task: %s %d"

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v0, p1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 107
    const-wide/16 v4, 0x0

    cmp-long v0, p2, v4

    if-ltz v0, :cond_0

    const-wide/16 v4, 0x1388

    cmp-long v0, p2, v4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Delay out of range: %d"

    new-array v4, v1, [Ljava/lang/Object;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v2

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 109
    iget v0, p1, Lcom/google/android/gms/icing/b/h;->i:I

    .line 110
    invoke-static {v0}, Lcom/google/android/gms/icing/b/i;->a(I)V

    .line 113
    invoke-virtual {p1}, Lcom/google/android/gms/icing/b/h;->b()Ljava/util/Collection;

    move-result-object v3

    .line 114
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 115
    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Runnable;

    .line 116
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_0
    move v0, v2

    .line 107
    goto :goto_0

    .line 119
    :cond_1
    iget-object v4, p0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v4

    .line 120
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->e()V

    .line 121
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->e:I

    if-eqz v0, :cond_2

    instance-of v0, p1, Lcom/google/android/gms/icing/b/b;

    if-nez v0, :cond_2

    .line 122
    const-string v0, "Scheduling new tasks while awaiting pending to complete: %s."

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    .line 124
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 125
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->close()V

    .line 127
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    if-ne v0, v1, :cond_3

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    iget-object v3, p0, Lcom/google/android/gms/icing/b/a;->c:Ljava/util/Set;

    invoke-interface {v0, v3}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->b()V

    .line 130
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->e()V

    .line 131
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 133
    invoke-virtual {p1, v2, v1}, Lcom/google/android/gms/icing/b/h;->a(II)V

    .line 134
    new-instance v0, Lcom/google/android/gms/icing/b/c;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/icing/b/c;-><init>(Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/b/h;)V

    invoke-virtual {p0, v0, p2, p3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/c;J)V

    .line 135
    return-object p1

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method

.method protected abstract a(Lcom/google/android/gms/icing/b/c;J)V
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 170
    iget-object v1, p0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 171
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->e()V

    .line 172
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)Z
    .locals 1

    .prologue
    .line 183
    invoke-static {p1}, Lcom/google/android/gms/icing/b/i;->a(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract b()V
.end method

.method public final b(I)V
    .locals 5

    .prologue
    .line 194
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/b/a;->a(I)Z

    move-result v0

    const-string v1, "Must be in a thread with scheduling %d vs %d"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/icing/b/a;->f:Ljava/lang/ThreadLocal;

    invoke-virtual {v4}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 197
    return-void
.end method

.method protected abstract c()V
.end method

.method final d()V
    .locals 6

    .prologue
    .line 232
    iget-object v1, p0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 233
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->e()V

    .line 234
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    .line 236
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/icing/b/a;->b:Ljava/util/Set;

    .line 240
    new-instance v2, Lcom/google/android/gms/icing/b/b;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/icing/b/b;-><init>(Lcom/google/android/gms/icing/b/a;Ljava/util/Set;)V

    const-wide/16 v4, 0x0

    invoke-virtual {p0, v2, v4, v5}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 242
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    .line 243
    iget v0, p0, Lcom/google/android/gms/icing/b/a;->g:I

    if-nez v0, :cond_1

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->c()V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/icing/b/a;->d:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->open()V

    .line 247
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/icing/b/a;->e()V

    .line 248
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 252
    iget-object v3, p0, Lcom/google/android/gms/icing/b/a;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 253
    :try_start_0
    iget v2, p0, Lcom/google/android/gms/icing/b/a;->g:I

    if-ltz v2, :cond_0

    move v2, v0

    :goto_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 255
    iget v2, p0, Lcom/google/android/gms/icing/b/a;->e:I

    if-ltz v2, :cond_1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 256
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :cond_0
    move v2, v1

    .line 253
    goto :goto_0

    :cond_1
    move v0, v1

    .line 255
    goto :goto_1

    .line 256
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method
