.class public final Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;
.super Lcom/google/android/gms/games/ui/headless/b;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/j;
.implements Lcom/google/android/gms/games/ui/b/a/m;
.implements Lcom/google/android/gms/games/ui/b/a/p;
.implements Lcom/google/android/gms/games/ui/b/a/s;
.implements Lcom/google/android/gms/games/ui/bo;
.implements Lcom/google/android/gms/games/ui/ch;
.implements Lcom/google/android/gms/games/ui/ci;
.implements Lcom/google/android/gms/games/ui/cj;
.implements Lcom/google/android/gms/games/ui/common/matches/v;
.implements Lcom/google/android/gms/games/ui/common/quests/j;
.implements Lcom/google/android/gms/games/ui/common/quests/k;
.implements Lcom/google/android/gms/games/ui/common/quests/l;
.implements Lcom/google/android/gms/games/ui/common/requests/m;
.implements Lcom/google/android/gms/games/ui/d/n;


# static fields
.field private static final k:Ljava/lang/String;

.field private static final l:I

.field private static final m:I

.field private static final n:[Lcom/google/android/gms/games/ui/d/ak;

.field private static final o:Lcom/google/android/gms/games/ui/d/aj;

.field private static final q:[I


# instance fields
.field private p:[I

.field private r:Lcom/google/android/gms/games/ui/headless/matches/a;

.field private s:Lcom/google/android/gms/games/ui/headless/requests/a;

.field private t:Lcom/google/android/gms/games/ui/headless/inbox/b;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 68
    const-class v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->k:Ljava/lang/String;

    .line 71
    sget v0, Lcom/google/android/gms/l;->aJ:I

    sput v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->l:I

    .line 74
    sget v0, Lcom/google/android/gms/m;->o:I

    sput v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->m:I

    .line 81
    new-array v0, v7, [Lcom/google/android/gms/games/ui/d/ak;

    new-instance v1, Lcom/google/android/gms/games/ui/d/ak;

    const-class v2, Lcom/google/android/gms/games/ui/common/matches/r;

    sget v3, Lcom/google/android/gms/p;->jS:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/ui/d/ak;-><init>(Ljava/lang/Class;I)V

    aput-object v1, v0, v5

    new-instance v1, Lcom/google/android/gms/games/ui/d/ak;

    const-class v2, Lcom/google/android/gms/games/ui/common/requests/k;

    sget v3, Lcom/google/android/gms/p;->kS:I

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/games/ui/d/ak;-><init>(Ljava/lang/Class;I)V

    aput-object v1, v0, v6

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/games/ui/d/ak;

    const-class v3, Lcom/google/android/gms/games/ui/common/quests/h;

    sget v4, Lcom/google/android/gms/p;->kA:I

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/ui/d/ak;-><init>(Ljava/lang/Class;I)V

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->n:[Lcom/google/android/gms/games/ui/d/ak;

    .line 92
    new-instance v0, Lcom/google/android/gms/games/ui/d/aj;

    sget-object v1, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->n:[Lcom/google/android/gms/games/ui/d/ak;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/d/aj;-><init>([Lcom/google/android/gms/games/ui/d/ak;)V

    sput-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->o:Lcom/google/android/gms/games/ui/d/aj;

    .line 99
    new-array v0, v6, [I

    aput v7, v0, v5

    sput-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->q:[I

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 108
    sget-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->o:Lcom/google/android/gms/games/ui/d/aj;

    sget v1, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->l:I

    sget v2, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->m:I

    const/4 v3, 0x1

    invoke-direct {p0, v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/headless/b;-><init>(Lcom/google/android/gms/games/ui/d/aj;IIZ)V

    .line 96
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->p:[I

    .line 109
    return-void

    .line 96
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method private a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 168
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/p;->f(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/headless/inbox/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/headless/inbox/a;-><init>(Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 181
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;)[I
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->p:[I

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;)Lcom/google/android/gms/games/ui/d/c;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->j:Lcom/google/android/gms/games/ui/d/c;

    return-object v0
.end method


# virtual methods
.method protected final K()V
    .locals 2

    .prologue
    .line 149
    invoke-super {p0}, Lcom/google/android/gms/games/ui/headless/b;->K()V

    .line 153
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 154
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 155
    return-void
.end method

.method public final K_()V
    .locals 5

    .prologue
    .line 297
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->N()Ljava/util/ArrayList;

    move-result-object v2

    .line 298
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 299
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 300
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 301
    instance-of v4, v0, Lcom/google/android/gms/games/ui/ch;

    if-eqz v4, :cond_0

    .line 302
    check-cast v0, Lcom/google/android/gms/games/ui/ch;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ch;->K_()V

    .line 299
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 305
    :cond_1
    return-void
.end method

.method public final L_()V
    .locals 5

    .prologue
    .line 321
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->N()Ljava/util/ArrayList;

    move-result-object v2

    .line 322
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 323
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 324
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 325
    instance-of v4, v0, Lcom/google/android/gms/games/ui/ci;

    if-eqz v4, :cond_0

    .line 326
    check-cast v0, Lcom/google/android/gms/games/ui/ci;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/ci;->L_()V

    .line 323
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 329
    :cond_1
    return-void
.end method

.method public final M_()V
    .locals 5

    .prologue
    .line 309
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->N()Ljava/util/ArrayList;

    move-result-object v2

    .line 310
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 311
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 312
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 313
    instance-of v4, v0, Lcom/google/android/gms/games/ui/cj;

    if-eqz v4, :cond_0

    .line 314
    check-cast v0, Lcom/google/android/gms/games/ui/cj;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cj;->M_()V

    .line 311
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 317
    :cond_1
    return-void
.end method

.method public final N_()Lcom/google/android/gms/games/ui/b/a/l;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->r:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final O()V
    .locals 2

    .prologue
    .line 276
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 277
    invoke-static {v0, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 279
    sget-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->k:Ljava/lang/String;

    const-string v1, "reloadData: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    :goto_0
    return-void

    .line 283
    :cond_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->a(Lcom/google/android/gms/common/api/v;)V

    goto :goto_0
.end method

.method public final O_()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->s:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final P_()Lcom/google/android/gms/games/ui/b/a/r;
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->s:Lcom/google/android/gms/games/ui/headless/requests/a;

    return-object v0
.end method

.method public final Q_()Lcom/google/android/gms/games/ui/common/quests/i;
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->t:Lcom/google/android/gms/games/ui/headless/inbox/b;

    return-object v0
.end method

.method public final T()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->r:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final U()Z
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x1

    return v0
.end method

.method public final V()[I
    .locals 1

    .prologue
    .line 261
    sget-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->q:[I

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/b/a/o;
    .locals 1

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->t:Lcom/google/android/gms/games/ui/headless/inbox/b;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 221
    const/4 v3, 0x0

    .line 222
    const/4 v2, -0x1

    .line 223
    const-string v4, "invitationsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 224
    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_LIST_INTERNAL"

    .line 243
    :goto_0
    if-eqz v1, :cond_5

    .line 244
    iget-object v2, p0, Lcom/google/android/gms/games/ui/q;->b:Lcom/google/android/gms/games/ui/bh;

    .line 246
    new-instance v3, Landroid/content/Intent;

    invoke-direct {v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 247
    const-string v1, "com.google.android.gms.games.FRAGMENT_INDEX"

    invoke-virtual {v3, v1, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 248
    const-string v0, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 249
    const-string v0, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 250
    const-string v0, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/bh;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 252
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->startActivity(Landroid/content/Intent;)V

    .line 257
    :goto_1
    return-void

    .line 226
    :cond_0
    const-string v4, "myTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 227
    const-string v0, "com.google.android.gms.games.SHOW_MULTIPLAYER_LIST_INTERNAL"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 228
    goto :goto_0

    .line 229
    :cond_1
    const-string v4, "theirTurnButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 230
    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_LIST_INTERNAL"

    .line 231
    const/4 v0, 0x2

    goto :goto_0

    .line 232
    :cond_2
    const-string v4, "completedMatchesButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 233
    const-string v1, "com.google.android.gms.games.SHOW_MULTIPLAYER_LIST_INTERNAL"

    .line 234
    const/4 v0, 0x3

    goto :goto_0

    .line 235
    :cond_3
    const-string v4, "giftsButton"

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 236
    const-string v1, "com.google.android.gms.games.SHOW_REQUEST_LIST_INTERNAL"

    goto :goto_0

    .line 238
    :cond_4
    const-string v0, "wishesButton"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 239
    const-string v0, "com.google.android.gms.games.SHOW_REQUEST_LIST_INTERNAL"

    move v5, v1

    move-object v1, v0

    move v0, v5

    .line 240
    goto :goto_0

    .line 254
    :cond_5
    sget-object v0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->k:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onSeeMoreClicked - Unexpected buttonTag: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    :cond_6
    move v0, v2

    move-object v1, v3

    goto/16 :goto_0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 159
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/b;->b_(Landroid/os/Bundle;)V

    .line 161
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 162
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/p;->a(Lcom/google/android/gms/common/api/v;)V

    .line 164
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->a(Lcom/google/android/gms/common/api/v;)V

    .line 165
    return-void
.end method

.method public final c(I)I
    .locals 1

    .prologue
    .line 288
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->p:[I

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->p:[I

    aget v0, v0, p1

    .line 291
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/b;->c(I)I

    move-result v0

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/games/ui/b/a/i;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->r:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 129
    const/4 v0, 0x1

    return v0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 144
    const/4 v0, 0x1

    return v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/b;->onCreate(Landroid/os/Bundle;)V

    .line 115
    new-instance v0, Lcom/google/android/gms/games/ui/headless/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/matches/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->r:Lcom/google/android/gms/games/ui/headless/matches/a;

    .line 116
    new-instance v0, Lcom/google/android/gms/games/ui/headless/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/requests/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->s:Lcom/google/android/gms/games/ui/headless/requests/a;

    .line 117
    new-instance v0, Lcom/google/android/gms/games/ui/headless/inbox/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/inbox/b;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->t:Lcom/google/android/gms/games/ui/headless/inbox/b;

    .line 119
    if-nez p1, :cond_0

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/inbox/HeadlessInboxActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 121
    const-string v1, "com.google.android.gms.games.TAB"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/b;->i:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->a(I)V

    .line 125
    :cond_0
    return-void
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 134
    sget v0, Lcom/google/android/gms/l;->bP:I

    return v0
.end method
