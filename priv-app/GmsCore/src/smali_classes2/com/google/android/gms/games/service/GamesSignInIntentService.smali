.class public final Lcom/google/android/gms/games/service/GamesSignInIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/ConcurrentLinkedQueue;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 429
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 483
    const-string v0, "SignInIntentService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 484
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;)V
    .locals 1

    .prologue
    .line 440
    new-instance v0, Lcom/google/android/gms/games/service/t;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/service/t;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 441
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 456
    new-instance v0, Lcom/google/android/gms/games/service/v;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/v;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 457
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 450
    new-instance v0, Lcom/google/android/gms/games/service/u;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/u;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 452
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 467
    new-instance v0, Lcom/google/android/gms/games/service/y;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/service/y;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 468
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/internal/dx;)V
    .locals 2

    .prologue
    .line 472
    new-instance v0, Lcom/google/android/gms/games/service/w;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1, p3}, Lcom/google/android/gms/games/service/w;-><init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLcom/google/android/gms/games/internal/dx;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 474
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 445
    new-instance v0, Lcom/google/android/gms/games/service/aa;

    invoke-direct {v0, p2, p1}, Lcom/google/android/gms/games/service/aa;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/internal/dx;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 446
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V
    .locals 1

    .prologue
    .line 433
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 434
    sget-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 435
    const-string v0, "com.google.android.gms.games.signin.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 436
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 478
    new-instance v0, Lcom/google/android/gms/games/service/x;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/games/service/x;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 480
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 461
    new-instance v0, Lcom/google/android/gms/games/service/z;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/z;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/service/s;)V

    .line 463
    return-void
.end method


# virtual methods
.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 488
    sget-object v0, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/s;

    .line 489
    if-nez v0, :cond_0

    .line 490
    const-string v0, "SignInIntentService"

    const-string v1, "operation missing"

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 505
    :goto_0
    return-void

    .line 494
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v1

    .line 496
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 499
    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/service/s;->a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V

    .line 500
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 504
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/t;->a()V

    throw v0
.end method
