.class final Lcom/google/android/gms/car/az;
.super Landroid/telephony/PhoneStateListener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/aw;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/aw;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/gms/car/az;->a:Lcom/google/android/gms/car/aw;

    invoke-direct {p0}, Landroid/telephony/PhoneStateListener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCallStateChanged(ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 84
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "call state change:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 87
    :cond_0
    if-nez p1, :cond_1

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/car/az;->a:Lcom/google/android/gms/car/aw;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/car/aw;->i:Z

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/car/az;->a:Lcom/google/android/gms/car/aw;

    iget-object v0, v0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->a(Lcom/google/android/gms/car/bb;)V

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/az;->a:Lcom/google/android/gms/car/aw;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/car/aw;->i:Z

    goto :goto_0
.end method
