.class public final Lcom/google/android/gms/games/service/a/g/b;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/g/b;->c:Lcom/google/android/gms/games/internal/dr;

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/g/b;->d:Lcom/google/android/gms/games/a/au;

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/g/b;->e:Ljava/lang/String;

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/b;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->c(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 43
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/b;->e:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/b;->d:Lcom/google/android/gms/games/a/au;

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 35
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/b;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/b;->e:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
