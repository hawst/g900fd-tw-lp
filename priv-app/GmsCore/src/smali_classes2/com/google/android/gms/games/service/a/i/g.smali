.class public final Lcom/google/android/gms/games/service/a/i/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field private final a:Lcom/google/android/gms/games/internal/dr;

.field private final b:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/internal/dr;)V
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/g;->a:Lcom/google/android/gms/games/internal/dr;

    .line 26
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/i/g;->b:Landroid/os/Bundle;

    .line 27
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 4

    .prologue
    .line 31
    const/4 v1, 0x1

    .line 34
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/g;->b:Landroid/os/Bundle;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Landroid/os/Bundle;)I
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 40
    :goto_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/i/g;->a:Lcom/google/android/gms/games/internal/dr;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/i/g;->b:Landroid/os/Bundle;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/internal/dr;->c(ILandroid/os/Bundle;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 46
    :goto_1
    return-void

    .line 35
    :catch_0
    move-exception v0

    .line 36
    const-string v2, "LoadOwnerCoverPhotosOp"

    const-string v3, "Killing (on development devices) due to RuntimeException"

    invoke-static {p1, v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v1

    goto :goto_0

    .line 41
    :catch_1
    move-exception v0

    .line 44
    const-string v1, "LoadOwnerCoverPhotosOp"

    const-string v2, "When providing result "

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
