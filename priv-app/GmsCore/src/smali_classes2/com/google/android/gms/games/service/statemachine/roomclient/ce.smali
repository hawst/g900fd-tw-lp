.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/ce;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/games/g/ag;

.field private final b:J

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/common/server/ClientContext;

.field private final e:Ljava/lang/String;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V
    .locals 4

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->f:Z

    .line 96
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->c:Landroid/content/Context;

    .line 97
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->d:Lcom/google/android/gms/common/server/ClientContext;

    .line 98
    iput-object p4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->e:Ljava/lang/String;

    .line 99
    new-instance v0, Lcom/google/android/gms/games/g/ag;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/ag;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ag;->c:J

    .line 101
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b:J

    .line 102
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/g/ag;->e:Ljava/lang/String;

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/games/g/ag;->f:Ljava/lang/String;

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/games/g/ag;->g:I

    .line 109
    const-string v0, "connectivity"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 111
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 112
    if-eqz v0, :cond_0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/games/g/ag;->h:I

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/games/g/ag;->i:I

    .line 116
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput p3, v0, Lcom/google/android/gms/games/g/ag;->d:I

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean p5, v0, Lcom/google/android/gms/games/g/ag;->A:Z

    .line 118
    return-void
.end method

.method static synthetic a(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 32
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v1, "SHA-1"

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "RtmpSessionLog"

    const-string v1, "no support for SHA-1"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1, v0}, Ljava/security/MessageDigest;->update([B)V

    invoke-virtual {v1}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 121
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "Cannot log the same log twice!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 122
    iput-boolean v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->f:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget-object v0, v0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget-object v0, v0, Lcom/google/android/gms/games/g/ag;->F:Lcom/google/android/gms/games/g/ah;

    iget-object v3, v0, Lcom/google/android/gms/games/g/ah;->b:[Lcom/google/android/gms/games/g/aj;

    array-length v4, v3

    move v1, v2

    :goto_1
    if-ge v1, v4, :cond_2

    aget-object v0, v3, v1

    .line 128
    iget-object v5, v0, Lcom/google/android/gms/games/g/aj;->d:[Lcom/google/android/gms/games/g/ak;

    array-length v6, v5

    move v0, v2

    :goto_2
    if-ge v0, v6, :cond_1

    aget-object v7, v5, v0

    .line 129
    iget-wide v8, v7, Lcom/google/android/gms/games/g/ak;->b:J

    iget-wide v10, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b:J

    sub-long/2addr v8, v10

    iput-wide v8, v7, Lcom/google/android/gms/games/g/ak;->b:J

    .line 128
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_0
    move v0, v2

    .line 121
    goto :goto_0

    .line 127
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 134
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/g/ag;)V

    .line 136
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget v0, v0, Lcom/google/android/gms/games/g/ag;->b:I

    if-nez v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput p1, v0, Lcom/google/android/gms/games/g/ag;->b:I

    .line 147
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 4

    .prologue
    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean p1, v0, Lcom/google/android/gms/games/g/ag;->m:Z

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ag;->k:J

    .line 175
    return-void
.end method

.method final b()J
    .locals 4

    .prologue
    .line 155
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ag;->t:J

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput p1, v0, Lcom/google/android/gms/games/g/ag;->u:I

    .line 180
    return-void
.end method

.method public final b(Z)V
    .locals 4

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ag;->w:J

    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean p1, v0, Lcom/google/android/gms/games/g/ag;->x:Z

    .line 198
    return-void
.end method

.method public final c()V
    .locals 4

    .prologue
    .line 201
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/ag;->v:J

    .line 202
    return-void
.end method
