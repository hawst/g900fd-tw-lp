.class public final Lcom/google/android/gms/common/util/s;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Z

.field static b:Ljava/lang/String;

.field static c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    sput-boolean v0, Lcom/google/android/gms/common/util/s;->a:Z

    .line 37
    const-string v0, ""

    sput-object v0, Lcom/google/android/gms/common/util/s;->b:Ljava/lang/String;

    .line 39
    const-string v0, ""

    sput-object v0, Lcom/google/android/gms/common/util/s;->c:Ljava/lang/String;

    return-void
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/gms/common/util/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/util/t;-><init>(Landroid/content/Context;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/os/AsyncTask;[Ljava/lang/Object;)V

    .line 49
    return-void
.end method

.method public static b(Landroid/content/Context;)Lcom/google/android/gms/common/util/u;
    .locals 7
    .annotation build Landroid/annotation/TargetApi;
        value = 0x9
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 60
    new-instance v3, Lcom/google/android/gms/common/util/u;

    invoke-direct {v3}, Lcom/google/android/gms/common/util/u;-><init>()V

    .line 61
    const-string v0, "deviceCapabilities"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 64
    const-string v0, "dirty"

    invoke-interface {v4, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 65
    sget-boolean v0, Lcom/google/android/gms/common/util/s;->a:Z

    if-eqz v0, :cond_4

    .line 67
    const-string v0, "armeabi-v7a"

    sget-object v5, Lcom/google/android/gms/common/util/s;->b:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "armeabi-v7a"

    sget-object v5, Lcom/google/android/gms/common/util/s;->c:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->a:Z

    .line 74
    :goto_1
    iget-boolean v0, v3, Lcom/google/android/gms/common/util/u;->a:Z

    if-nez v0, :cond_1

    .line 75
    const-string v0, "GMS_DeviceFeatureDetectionUtil"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Not arm7 "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v6, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 78
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/common/util/s;->c(Landroid/content/Context;)F

    move-result v0

    const/high16 v5, 0x40000000    # 2.0f

    cmpl-float v0, v0, v5

    if-ltz v0, :cond_2

    .line 79
    iput-boolean v2, v3, Lcom/google/android/gms/common/util/u;->b:Z

    .line 82
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v2, "android.hardware.microphone"

    invoke-virtual {v0, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->c:Z

    .line 84
    const-string v0, "GMS_DeviceFeatureDetectionUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Microphone: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v5, v3, Lcom/google/android/gms/common/util/u;->c:Z

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 87
    const-string v2, "armv7"

    iget-boolean v4, v3, Lcom/google/android/gms/common/util/u;->a:Z

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 89
    const-string v2, "glv2"

    iget-boolean v4, v3, Lcom/google/android/gms/common/util/u;->b:Z

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 91
    const-string v2, "microphone"

    iget-boolean v4, v3, Lcom/google/android/gms/common/util/u;->c:Z

    invoke-interface {v0, v2, v4}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 93
    const-string v2, "dirty"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 95
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 106
    :goto_2
    return-object v3

    :cond_3
    move v0, v1

    .line 67
    goto :goto_0

    .line 70
    :cond_4
    const-string v0, "armeabi-v7a"

    sget-object v5, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    const-string v0, "armeabi-v7a"

    sget-object v5, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    :cond_5
    move v0, v2

    :goto_3
    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->a:Z

    goto/16 :goto_1

    :cond_6
    move v0, v1

    goto :goto_3

    .line 98
    :cond_7
    const-string v0, "armv7"

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->a:Z

    .line 100
    const-string v0, "glv2"

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->b:Z

    .line 102
    const-string v0, "microphone"

    invoke-interface {v4, v0, v1}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, v3, Lcom/google/android/gms/common/util/u;->c:Z

    goto :goto_2
.end method

.method private static c(Landroid/content/Context;)F
    .locals 5

    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 120
    invoke-virtual {v0}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v1

    .line 121
    if-eqz v1, :cond_1

    array-length v0, v1

    if-lez v0, :cond_1

    .line 122
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 125
    iget-object v4, v3, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    if-nez v4, :cond_0

    .line 126
    invoke-virtual {v3}, Landroid/content/pm/FeatureInfo;->getGlEsVersion()Ljava/lang/String;

    move-result-object v0

    .line 127
    const-string v1, "GMS_DeviceFeatureDetectionUtil"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "gl version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    invoke-static {v0}, Ljava/lang/Float;->valueOf(Ljava/lang/String;)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 132
    :goto_1
    return v0

    .line 122
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 132
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
