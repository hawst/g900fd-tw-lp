.class public final Lcom/google/android/gms/common/util/as;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Ljava/lang/String;)[Lcom/google/android/gms/common/api/Scope;
    .locals 4

    .prologue
    .line 18
    const-string v0, "scopeStrings can\'t be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 20
    array-length v0, p0

    new-array v1, v0, [Lcom/google/android/gms/common/api/Scope;

    .line 21
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 22
    new-instance v2, Lcom/google/android/gms/common/api/Scope;

    aget-object v3, p0, v0

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    aput-object v2, v1, v0

    .line 21
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 24
    :cond_0
    return-object v1
.end method

.method public static a([Lcom/google/android/gms/common/api/Scope;)[Ljava/lang/String;
    .locals 3

    .prologue
    .line 31
    const-string v0, "scopes can\'t be null."

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 33
    array-length v0, p0

    new-array v1, v0, [Ljava/lang/String;

    .line 34
    const/4 v0, 0x0

    :goto_0
    array-length v2, p0

    if-ge v0, v2, :cond_0

    .line 35
    aget-object v2, p0, v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v1, v0

    .line 34
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 37
    :cond_0
    return-object v1
.end method
