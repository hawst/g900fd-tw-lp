.class final Lcom/google/android/gms/people/internal/aj;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 854
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 855
    iput-object p1, p0, Lcom/google/android/gms/people/internal/aj;->a:Lcom/google/android/gms/common/api/m;

    .line 856
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 861
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 862
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Avatar callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " resolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " pfd="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 866
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/p;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 870
    if-eqz p4, :cond_1

    .line 871
    const-string v0, "rewindable"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 872
    const-string v0, "width"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 873
    const-string v0, "height"

    invoke-virtual {p4, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 875
    :goto_0
    iget-object v6, p0, Lcom/google/android/gms/people/internal/aj;->a:Lcom/google/android/gms/common/api/m;

    new-instance v0, Lcom/google/android/gms/people/internal/ap;

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/internal/ap;-><init>(Lcom/google/android/gms/common/api/Status;Landroid/os/ParcelFileDescriptor;ZII)V

    invoke-interface {v6, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 877
    return-void

    :cond_1
    move v4, v5

    move v3, v5

    goto :goto_0
.end method
