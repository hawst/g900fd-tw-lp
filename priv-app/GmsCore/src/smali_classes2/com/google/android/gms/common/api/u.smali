.class public abstract Lcom/google/android/gms/common/api/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ao;
.implements Lcom/google/android/gms/common/api/ap;


# instance fields
.field protected final a:Lcom/google/android/gms/common/api/Status;

.field protected final b:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/api/Status;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p2, p0, Lcom/google/android/gms/common/api/u;->a:Lcom/google/android/gms/common/api/Status;

    .line 21
    iput-object p1, p0, Lcom/google/android/gms/common/api/u;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 22
    return-void
.end method


# virtual methods
.method public C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->b:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/common/api/u;->b:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 34
    :cond_0
    return-void
.end method
