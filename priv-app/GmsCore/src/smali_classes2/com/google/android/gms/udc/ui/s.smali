.class final Lcom/google/android/gms/udc/ui/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# instance fields
.field private final a:Landroid/view/View;

.field private final b:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/view/View;Landroid/widget/CompoundButton;)V
    .locals 0

    .prologue
    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/s;->a:Landroid/view/View;

    .line 356
    iput-object p2, p0, Lcom/google/android/gms/udc/ui/s;->b:Landroid/widget/CompoundButton;

    .line 357
    return-void
.end method


# virtual methods
.method public final onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/s;->a:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 362
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 363
    return-void

    .line 362
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/s;->b:Landroid/widget/CompoundButton;

    invoke-virtual {v0}, Landroid/widget/CompoundButton;->toggle()V

    .line 368
    return-void
.end method
