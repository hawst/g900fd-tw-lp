.class final Lcom/google/android/gms/games/service/statemachine/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/statemachine/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/h;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/service/statemachine/h;)V
    .locals 0

    .prologue
    .line 20
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/util/a/a;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;)Lcom/google/android/gms/common/util/a/a;

    move-result-object v0

    return-object v0
.end method

.method public final a(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Message;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;Landroid/os/Message;)V

    .line 29
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/a/a;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/a;)V

    .line 24
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/a/b;)V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/b;)V

    .line 64
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/util/a/b;Lcom/google/android/gms/common/util/a/b;)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    iget-object v0, v0, Lcom/google/android/gms/common/util/a/c;->f:Lcom/google/android/gms/common/util/a/f;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/common/util/a/f;->a(Lcom/google/android/gms/common/util/a/f;Lcom/google/android/gms/common/util/a/b;Lcom/google/android/gms/common/util/a/b;)Lcom/google/android/gms/common/util/a/i;

    .line 69
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/j;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 39
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/j;I)V
    .locals 5

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-interface {p1}, Lcom/google/android/gms/games/service/statemachine/j;->a()I

    move-result v1

    int-to-long v2, p2

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/util/a/c;->a(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-object v4, v0, Lcom/google/android/gms/common/util/a/c;->f:Lcom/google/android/gms/common/util/a/f;

    if-eqz v4, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/util/a/c;->b(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v4, v0, v2, v3}, Lcom/google/android/gms/common/util/a/f;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 49
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(Lcom/google/android/gms/games/service/statemachine/h;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Ljava/lang/Throwable;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;Ljava/lang/String;)V

    .line 54
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    iget-object v0, v0, Lcom/google/android/gms/common/util/a/c;->f:Lcom/google/android/gms/common/util/a/f;

    if-eqz v0, :cond_0

    invoke-static {v0}, Lcom/google/android/gms/common/util/a/f;->h(Lcom/google/android/gms/common/util/a/f;)V

    .line 79
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/util/a/b;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/b;)V

    .line 74
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/service/statemachine/j;)V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/i;->a:Lcom/google/android/gms/games/service/statemachine/h;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 44
    return-void
.end method
