.class public final Lcom/google/android/gms/games/h/a/co;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 63
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 65
    sput-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "bytesReceived"

    const-string v2, "bytesReceived"

    const-class v3, Lcom/google/android/gms/games/h/a/r;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 68
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "bytesSent"

    const-string v2, "bytesSent"

    const-class v3, Lcom/google/android/gms/games/h/a/r;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 71
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "numMessagesLost"

    const-string v2, "numMessagesLost"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "numMessagesReceived"

    const-string v2, "numMessagesReceived"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "numMessagesSent"

    const-string v2, "numMessagesSent"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "numSendFailures"

    const-string v2, "numSendFailures"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    const-string v1, "roundtripLatencyMillis"

    const-string v2, "roundtripLatencyMillis"

    const-class v3, Lcom/google/android/gms/games/h/a/r;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    .line 85
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/h/a/r;Lcom/google/android/gms/games/h/a/r;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Lcom/google/android/gms/games/h/a/r;)V
    .locals 2

    .prologue
    .line 95
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 119
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    .line 96
    if-eqz p1, :cond_0

    .line 97
    const-string v0, "bytesReceived"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 99
    :cond_0
    if-eqz p2, :cond_1

    .line 100
    const-string v0, "bytesSent"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 102
    :cond_1
    if-eqz p3, :cond_2

    .line 103
    const-string v0, "numMessagesLost"

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;I)V

    .line 105
    :cond_2
    if-eqz p4, :cond_3

    .line 106
    const-string v0, "numMessagesReceived"

    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;I)V

    .line 108
    :cond_3
    if-eqz p5, :cond_4

    .line 109
    const-string v0, "numMessagesSent"

    invoke-virtual {p5}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;I)V

    .line 111
    :cond_4
    if-eqz p6, :cond_5

    .line 112
    const-string v0, "numSendFailures"

    invoke-virtual {p6}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;I)V

    .line 114
    :cond_5
    if-eqz p7, :cond_6

    .line 115
    const-string v0, "roundtripLatencyMillis"

    invoke-virtual {p0, v0, p7}, Lcom/google/android/gms/games/h/a/co;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 117
    :cond_6
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/gms/games/h/a/co;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 124
    return-void
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getBytesReceived()Lcom/google/android/gms/games/h/a/r;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    const-string v1, "bytesReceived"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/r;

    return-object v0
.end method

.method public final getBytesSent()Lcom/google/android/gms/games/h/a/r;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    const-string v1, "bytesSent"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/r;

    return-object v0
.end method

.method public final getRoundtripLatencyMillis()Lcom/google/android/gms/games/h/a/r;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/co;->c:Ljava/util/HashMap;

    const-string v1, "roundtripLatencyMillis"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/r;

    return-object v0
.end method
