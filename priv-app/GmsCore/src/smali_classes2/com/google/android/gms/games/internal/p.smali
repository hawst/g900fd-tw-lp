.class final Lcom/google/android/gms/games/internal/p;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 639
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 640
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/p;->a:Lcom/google/android/gms/common/api/m;

    .line 641
    return-void
.end method


# virtual methods
.method public final h(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/gms/games/internal/p;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/games/internal/aj;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aj;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 646
    return-void
.end method
