.class public Lcom/google/android/gms/auth/trustagent/trustlet/u;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# instance fields
.field private final b:Landroid/app/PendingIntent;

.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/auth/trustagent/trustlet/w;

.field private final e:Landroid/app/AlarmManager;

.field private f:I

.field private g:Z

.field private h:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 20
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Lcom/google/android/gms/auth/trustagent/trustlet/u;

    invoke-virtual {v1}, Ljava/lang/Class;->getPackage()Ljava/lang/Package;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".INACTIVITY_TRIGGERED"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/w;)V
    .locals 3

    .prologue
    .line 52
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/v;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/v;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/u;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->h:Landroid/content/BroadcastReceiver;

    .line 53
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->c:Landroid/content/Context;

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->d:Lcom/google/android/gms/auth/trustagent/trustlet/w;

    .line 55
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    .line 59
    invoke-virtual {v0}, Ljava/util/Random;->nextInt()I

    move-result v0

    .line 60
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/google/android/gms/auth/trustagent/trustlet/u;->a:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->b:Landroid/app/PendingIntent;

    .line 62
    const-string v0, "alarm"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->e:Landroid/app/AlarmManager;

    .line 63
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/u;)V
    .locals 2

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->g:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->g:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->e:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->d:Lcom/google/android/gms/auth/trustagent/trustlet/w;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/w;->a()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/trustlet/u;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/u;->d()V

    return-void
.end method

.method static synthetic c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->a:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/trustlet/u;)V
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->d:Lcom/google/android/gms/auth/trustagent/trustlet/w;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/w;->b()V

    return-void
.end method

.method private d()V
    .locals 6

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->g:Z

    if-eqz v0, :cond_0

    .line 100
    :goto_0
    return-void

    .line 95
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->g:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->e:Landroid/app/AlarmManager;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->f:I

    mul-int/lit8 v4, v4, 0x3c

    mul-int/lit16 v4, v4, 0x3e8

    int-to-long v4, v4

    add-long/2addr v2, v4

    iget-object v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->b:Landroid/app/PendingIntent;

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 66
    const/16 v0, 0xf0

    iput v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->f:I

    .line 67
    new-instance v0, Landroid/content/IntentFilter;

    sget-object v1, Lcom/google/android/gms/auth/trustagent/trustlet/u;->a:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 68
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 69
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->c:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->c:Landroid/content/Context;

    const-string v1, "power"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 73
    invoke-virtual {v0}, Landroid/os/PowerManager;->isInteractive()Z

    move-result v0

    if-nez v0, :cond_0

    .line 75
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/u;->d()V

    .line 77
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->c:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/u;->h:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 81
    return-void
.end method
