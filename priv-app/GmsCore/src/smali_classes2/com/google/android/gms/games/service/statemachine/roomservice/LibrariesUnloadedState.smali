.class final Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 36
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 73
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->g:Z

    :goto_0
    return v0

    .line 38
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/k;

    .line 40
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/k;->a:Landroid/content/Context;

    iput-object v2, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->e:Landroid/content/Context;

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->c:Lcom/google/android/gms/games/realtime/network/d;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/k;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/v;

    invoke-static {}, Lcom/google/android/gms/games/internal/b/b;->b()I

    move-result v4

    invoke-interface {v2, v0, v3, v4}, Lcom/google/android/gms/games/realtime/network/d;->a(Landroid/content/Context;Lcom/google/android/gms/games/realtime/network/a;I)Lcom/google/android/gms/games/realtime/network/b;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/eg;->a(I)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->b()V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 54
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f:Z

    goto :goto_0

    .line 45
    :catch_0
    move-exception v0

    .line 47
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v2, "Unable to load libraries"

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v1, -0x1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/eg;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 50
    :catch_1
    move-exception v0

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 57
    :sswitch_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f:Z

    goto :goto_0

    .line 60
    :sswitch_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f:Z

    goto :goto_0

    .line 63
    :sswitch_3
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/eg;->b()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 67
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f:Z

    goto :goto_0

    .line 64
    :catch_2
    move-exception v0

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 70
    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h()V

    .line 71
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f:Z

    goto :goto_0

    .line 36
    nop

    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_2
        0x3 -> :sswitch_4
        0xf -> :sswitch_0
        0x12 -> :sswitch_1
        0x13 -> :sswitch_3
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->c()V

    .line 32
    return-void
.end method
