.class public final Lcom/google/android/gms/checkin/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static b:Ljava/util/regex/Pattern;

.field private static final c:Ljava/io/File;

.field private static d:Ljava/util/regex/Pattern;

.field private static e:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 127
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->i()I

    move-result v0

    sput v0, Lcom/google/android/gms/checkin/a;->a:I

    .line 131
    const-string v0, "^([0-9a-fA-F][0-9a-fA-F][:-]?){5}[0-9a-fA-F][0-9a-fA-F]$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/a;->b:Ljava/util/regex/Pattern;

    .line 201
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/etc/security/otacerts.zip"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/checkin/a;->c:Ljava/io/File;

    .line 359
    const-string v0, "^(([0-9]{15})|([0-9a-fA-F]{14}))$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/a;->d:Ljava/util/regex/Pattern;

    .line 362
    const-string v0, "^([0-9a-fA-F]{8})$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/a;->e:Ljava/util/regex/Pattern;

    return-void
.end method

.method static a(ZLandroid/os/DropBoxManager;IILjava/util/Map;JLcom/google/android/gms/checkin/b/b;)J
    .locals 21

    .prologue
    .line 1121
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1122
    const/4 v5, 0x0

    .line 1127
    :try_start_0
    move-object/from16 v0, p7

    iget-object v14, v0, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    .line 1128
    move-wide/from16 v0, p5

    invoke-virtual {v14, v0, v1}, Lcom/google/android/gms/checkin/b/j;->a(J)Lcom/google/android/gms/checkin/b/j;

    .line 1131
    const-string v4, "checkin_dropbox_upload"

    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1132
    if-eqz v4, :cond_7

    sget-object v8, Lcom/google/android/gsf/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v8, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_7

    const/4 v4, 0x1

    move v13, v4

    .line 1137
    :goto_0
    const/4 v4, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, p5

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_3
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v7

    .line 1138
    :try_start_1
    const-string v4, "event_log"

    move-object/from16 v0, p1

    move-wide/from16 v1, p5

    invoke-virtual {v0, v4, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_2

    move-result-object v6

    .line 1139
    :try_start_2
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    .line 1140
    const/4 v11, 0x0

    .line 1141
    const-wide/16 v8, -0x1

    .line 1143
    const/16 v4, 0x1000

    new-array v0, v4, [B

    move-object/from16 v16, v0

    .line 1144
    new-instance v17, Ljava/io/ByteArrayOutputStream;

    invoke-direct/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;-><init>()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_3

    .line 1145
    const/4 v4, 0x0

    move v12, v4

    move-object v10, v7

    move-object/from16 v20, v5

    move-object v5, v6

    move-wide v6, v8

    move-object/from16 v8, v20

    .line 1147
    :goto_1
    move/from16 v0, p2

    if-ge v12, v0, :cond_1f

    if-nez v5, :cond_0

    if-eqz v10, :cond_1f

    :cond_0
    move-object v9, v5

    .line 1150
    :goto_2
    if-eqz v9, :cond_c

    :try_start_3
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-lt v4, v5, :cond_1

    cmp-long v4, v6, p5

    if-gtz v4, :cond_c

    .line 1151
    :cond_1
    if-nez v11, :cond_2

    .line 1152
    invoke-virtual {v9}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    move-result-object v5

    .line 1153
    if-eqz v5, :cond_2

    .line 1154
    new-instance v4, Ljava/io/BufferedReader;

    new-instance v11, Ljava/io/InputStreamReader;

    invoke-direct {v11, v5}, Ljava/io/InputStreamReader;-><init>(Ljava/io/InputStream;)V

    const/16 v5, 0x1000

    invoke-direct {v4, v11, v5}, Ljava/io/BufferedReader;-><init>(Ljava/io/Reader;I)V

    move-object v11, v4

    .line 1158
    :cond_2
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    .line 1165
    if-eqz v11, :cond_3

    invoke-static {v11, v15}, Lcom/google/android/gms/common/internal/t;->a(Ljava/io/BufferedReader;Ljava/util/List;)Z

    move-result v4

    if-nez v4, :cond_8

    :cond_3
    const/4 v5, 0x1

    .line 1166
    :goto_3
    const/4 v4, 0x0

    .line 1167
    if-nez v5, :cond_4

    .line 1168
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v18

    const/16 v19, 0x4

    move/from16 v0, v18

    move/from16 v1, v19

    if-lt v0, v1, :cond_9

    .line 1169
    const/4 v4, 0x3

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1170
    move/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/gms/checkin/a;->a(ZLjava/lang/String;)Z

    move-result v4

    .line 1176
    :cond_4
    :goto_4
    if-nez v5, :cond_5

    if-eqz v4, :cond_a

    .line 1177
    :cond_5
    invoke-virtual {v9}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v4

    .line 1178
    if-eqz v11, :cond_6

    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V

    .line 1179
    :cond_6
    const/4 v11, 0x0

    .line 1180
    invoke-virtual {v9}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1181
    const-string v18, "event_log"

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v9

    goto :goto_2

    .line 1132
    :cond_7
    const/4 v4, 0x0

    move v13, v4

    goto/16 :goto_0

    .line 1165
    :cond_8
    const/4 v5, 0x0

    goto :goto_3

    .line 1172
    :cond_9
    const-string v5, "CheckinRequestBuilder"

    const-string v18, "event_log tagname does not exist"

    move-object/from16 v0, v18

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1173
    const/4 v5, 0x1

    goto :goto_4

    .line 1182
    :cond_a
    invoke-virtual {v15}, Ljava/util/ArrayList;->isEmpty()Z
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_4
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result v4

    if-nez v4, :cond_25

    .line 1184
    const/4 v4, 0x0

    :try_start_4
    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1185
    const/16 v5, 0x2e

    invoke-virtual {v4, v5}, Ljava/lang/String;->indexOf(I)I

    move-result v5

    .line 1186
    const/4 v6, -0x1

    if-ne v5, v6, :cond_b

    :goto_5
    invoke-static {v4}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    goto/16 :goto_2

    :cond_b
    const/4 v6, 0x0

    invoke-virtual {v4, v6, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/NumberFormatException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v4

    goto :goto_5

    .line 1187
    :catch_0
    move-exception v4

    move-object v5, v4

    .line 1188
    :try_start_5
    const-string v6, "CheckinRequestBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v4, "Can\'t parse event_log timestamp: "

    invoke-direct {v7, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v4, 0x0

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1189
    const-wide/16 v4, -0x1

    :goto_6
    move-wide v6, v4

    .line 1192
    goto/16 :goto_2

    .line 1194
    :cond_c
    :goto_7
    if-eqz v10, :cond_10

    .line 1196
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v4

    .line 1197
    const-string v5, "event_log"

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_d

    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getFlags()I

    move-result v5

    and-int/lit8 v5, v5, 0x1

    if-eqz v5, :cond_f

    .line 1199
    :cond_d
    move/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/gms/checkin/a;->a(ZLjava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_f

    .line 1200
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v18, "checkin_dropbox_upload:"

    move-object/from16 v0, v18

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 1201
    move-object/from16 v0, p4

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 1202
    if-eqz v4, :cond_e

    sget-object v5, Lcom/google/android/gsf/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/regex/Matcher;->matches()Z

    move-result v5

    if-nez v5, :cond_10

    .line 1204
    :cond_e
    if-eqz v13, :cond_f

    if-eqz v4, :cond_10

    sget-object v5, Lcom/google/android/gsf/f;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v5, v4}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/regex/Matcher;->matches()Z

    move-result v4

    if-eqz v4, :cond_10

    .line 1208
    :cond_f
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v4

    .line 1215
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1216
    const/16 v18, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v18

    invoke-virtual {v0, v1, v4, v5}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v10

    goto :goto_7

    .line 1220
    :cond_10
    if-eqz v9, :cond_17

    if-eqz v10, :cond_11

    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v4

    cmp-long v4, v6, v4

    if-gez v4, :cond_17

    .line 1224
    :cond_11
    new-instance v5, Lcom/google/android/gms/checkin/b/l;

    invoke-direct {v5}, Lcom/google/android/gms/checkin/b/l;-><init>()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_4
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1225
    :try_start_6
    invoke-virtual {v14, v5}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/l;)Lcom/google/android/gms/checkin/b/j;

    .line 1226
    const/4 v4, 0x3

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/google/android/gms/checkin/b/l;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;

    .line 1227
    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/checkin/b/l;->a(J)Lcom/google/android/gms/checkin/b/l;

    .line 1231
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v8, 0x5

    if-ne v4, v8, :cond_13

    .line 1233
    const/4 v4, 0x4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v4}, Lcom/google/android/gms/checkin/b/l;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;

    .line 1245
    :cond_12
    :goto_8
    invoke-virtual {v5}, Lcom/google/android/gms/checkin/b/l;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v12

    .line 1246
    invoke-virtual {v15}, Ljava/util/ArrayList;->clear()V

    move v12, v4

    move-object v8, v5

    move-object v5, v9

    goto/16 :goto_1

    .line 1234
    :cond_13
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v8, 0x5

    if-le v4, v8, :cond_12

    .line 1236
    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    .line 1237
    const/4 v4, 0x4

    invoke-virtual {v15, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 1238
    const/4 v4, 0x5

    move v8, v4

    :goto_9
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v8, v4, :cond_14

    .line 1239
    const-string v4, ","

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1240
    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-static {v4, v0}, Lcom/google/android/gms/common/internal/t;->a(Ljava/lang/String;Ljava/lang/Appendable;)V

    .line 1238
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_9

    .line 1242
    :cond_14
    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/checkin/b/l;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_1
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    goto :goto_8

    .line 1291
    :catch_1
    move-exception v4

    move-object v6, v9

    move-object v7, v10

    .line 1292
    :goto_a
    :try_start_7
    const-string v8, "CheckinRequestBuilder"

    const-string v9, "Can\'t copy dropbox data"

    invoke-static {v8, v9, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    .line 1294
    if-eqz v7, :cond_15

    invoke-virtual {v7}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1295
    :cond_15
    if-eqz v6, :cond_23

    invoke-virtual {v6}, Landroid/os/DropBoxManager$Entry;->close()V

    move-object v8, v5

    .line 1298
    :cond_16
    :goto_b
    if-nez v8, :cond_22

    :goto_c
    return-wide p5

    .line 1247
    :cond_17
    if-eqz v10, :cond_24

    if-eqz v9, :cond_18

    :try_start_8
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v4

    cmp-long v4, v4, v6

    if-gtz v4, :cond_24

    .line 1251
    :cond_18
    new-instance v5, Lcom/google/android/gms/checkin/b/l;

    invoke-direct {v5}, Lcom/google/android/gms/checkin/b/l;-><init>()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 1252
    :try_start_9
    invoke-virtual {v14, v5}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/l;)Lcom/google/android/gms/checkin/b/j;

    .line 1253
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/checkin/b/l;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;

    .line 1254
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-virtual {v5, v0, v1}, Lcom/google/android/gms/checkin/b/l;->a(J)Lcom/google/android/gms/checkin/b/l;

    .line 1257
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 1258
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getInputStream()Ljava/io/InputStream;

    move-result-object v4

    .line 1259
    :cond_19
    if-eqz v4, :cond_1a

    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/io/InputStream;->read([B)I

    move-result v8

    if-lez v8, :cond_1a

    .line 1260
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v18

    sub-int v18, p3, v18

    move/from16 v0, v18

    invoke-static {v0, v8}, Ljava/lang/Math;->min(II)I

    move-result v18

    .line 1261
    const/16 v19, 0x0

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    move/from16 v2, v19

    move/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Ljava/io/ByteArrayOutputStream;->write([BII)V

    .line 1262
    move/from16 v0, v18

    if-ge v0, v8, :cond_19

    .line 1263
    const-string v8, "CheckinRequestBuilder"

    new-instance v18, Ljava/lang/StringBuilder;

    const-string v19, "Truncating "

    invoke-direct/range {v18 .. v19}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTag()Ljava/lang/String;

    move-result-object v19

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " entry to "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    move/from16 v1, p3

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " bytes for upload"

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1265
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getFlags()I

    move-result v8

    and-int/lit8 v8, v8, 0x2

    if-eqz v8, :cond_1a

    .line 1266
    const-string v8, "\n=== TRUNCATED FOR UPLOAD ===\n"

    invoke-virtual {v8}, Ljava/lang/String;->getBytes()[B

    move-result-object v8

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/io/ByteArrayOutputStream;->write([B)V

    .line 1271
    :cond_1a
    if-eqz v4, :cond_1b

    invoke-virtual {v4}, Ljava/io/InputStream;->close()V

    .line 1273
    :cond_1b
    invoke-virtual/range {v17 .. v17}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_1
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v4

    .line 1276
    :try_start_a
    array-length v8, v4

    if-lez v8, :cond_1c

    new-instance v8, Ljava/lang/String;

    const-string v18, "ISO-8859-1"

    move-object/from16 v0, v18

    invoke-direct {v8, v4, v0}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, v8}, Lcom/google/android/gms/checkin/b/l;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;
    :try_end_a
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1281
    :cond_1c
    :try_start_b
    invoke-virtual {v5}, Lcom/google/android/gms/checkin/b/l;->b()I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    add-int/2addr v4, v12

    .line 1282
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->getTimeMillis()J

    move-result-wide v18

    .line 1283
    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1284
    const/4 v8, 0x0

    move-object/from16 v0, p1

    move-wide/from16 v1, v18

    invoke-virtual {v0, v8, v1, v2}, Landroid/os/DropBoxManager;->getNextEntry(Ljava/lang/String;J)Landroid/os/DropBoxManager$Entry;

    move-result-object v10

    move v12, v4

    move-object v8, v5

    move-object v5, v9

    .line 1285
    goto/16 :goto_1

    .line 1278
    :catch_2
    move-exception v4

    new-instance v4, Ljava/lang/RuntimeException;

    const-string v6, "ISO-8859-1 not supported?"

    invoke-direct {v4, v6}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v4
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_1
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1294
    :catchall_0
    move-exception v4

    :goto_d
    if-eqz v10, :cond_1d

    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1295
    :cond_1d
    if-eqz v9, :cond_1e

    invoke-virtual {v9}, Landroid/os/DropBoxManager$Entry;->close()V

    :cond_1e
    throw v4

    .line 1288
    :cond_1f
    if-eqz v11, :cond_20

    .line 1289
    :try_start_c
    invoke-virtual {v11}, Ljava/io/BufferedReader;->close()V
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_4

    .line 1294
    :cond_20
    if-eqz v10, :cond_21

    invoke-virtual {v10}, Landroid/os/DropBoxManager$Entry;->close()V

    .line 1295
    :cond_21
    if-eqz v5, :cond_16

    invoke-virtual {v5}, Landroid/os/DropBoxManager$Entry;->close()V

    goto/16 :goto_b

    .line 1298
    :cond_22
    iget-wide v0, v8, Lcom/google/android/gms/checkin/b/l;->a:J

    move-wide/from16 p5, v0

    goto/16 :goto_c

    .line 1294
    :catchall_1
    move-exception v4

    move-object v9, v6

    move-object v10, v7

    goto :goto_d

    :catchall_2
    move-exception v4

    move-object v9, v6

    move-object v10, v7

    goto :goto_d

    :catchall_3
    move-exception v4

    move-object v9, v6

    move-object v10, v7

    goto :goto_d

    :catchall_4
    move-exception v4

    move-object v9, v5

    goto :goto_d

    .line 1291
    :catch_3
    move-exception v4

    goto/16 :goto_a

    :catch_4
    move-exception v4

    move-object v5, v8

    move-object v6, v9

    move-object v7, v10

    goto/16 :goto_a

    :catch_5
    move-exception v4

    move-object v6, v5

    move-object v7, v10

    move-object v5, v8

    goto/16 :goto_a

    :cond_23
    move-object v8, v5

    goto/16 :goto_b

    :cond_24
    move-object v5, v9

    goto/16 :goto_1

    :cond_25
    move-wide v4, v6

    goto/16 :goto_6
.end method

.method public static a(ILcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/b;
    .locals 5

    .prologue
    .line 149
    new-instance v0, Lcom/google/android/gms/checkin/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/b;-><init>()V

    .line 150
    new-instance v1, Lcom/google/android/gms/checkin/b/j;

    invoke-direct {v1}, Lcom/google/android/gms/checkin/b/j;-><init>()V

    .line 151
    if-eqz p1, :cond_0

    .line 152
    invoke-virtual {v1, p1}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/j;

    .line 153
    const-string v2, "CheckinRequestBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Checkin reason type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p1, Lcom/google/android/gms/checkin/b/k;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " attempt count: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p1, Lcom/google/android/gms/checkin/b/k;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    new-instance v2, Lcom/google/android/gms/checkin/b/i;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/b/i;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/i;)Lcom/google/android/gms/checkin/b/j;

    .line 157
    invoke-virtual {v0, v1}, Lcom/google/android/gms/checkin/b/b;->a(Lcom/google/android/gms/checkin/b/j;)Lcom/google/android/gms/checkin/b/b;

    .line 158
    invoke-virtual {v0, p0}, Lcom/google/android/gms/checkin/b/b;->b(I)Lcom/google/android/gms/checkin/b/b;

    .line 159
    return-object v0
.end method

.method private static a()Ljava/util/Set;
    .locals 14

    .prologue
    .line 755
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 758
    invoke-static {}, Ljavax/microedition/khronos/egl/EGLContext;->getEGL()Ljavax/microedition/khronos/egl/EGL;

    move-result-object v0

    check-cast v0, Ljavax/microedition/khronos/egl/EGL10;

    .line 759
    if-nez v0, :cond_0

    .line 760
    const-string v0, "CheckinRequestBuilder"

    const-string v1, "Couldn\'t get EGL"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 835
    :goto_0
    return-object v5

    .line 765
    :cond_0
    sget-object v1, Ljavax/microedition/khronos/egl/EGL10;->EGL_DEFAULT_DISPLAY:Ljava/lang/Object;

    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglGetDisplay(Ljava/lang/Object;)Ljavax/microedition/khronos/egl/EGLDisplay;

    move-result-object v1

    .line 766
    const/4 v2, 0x2

    new-array v2, v2, [I

    .line 767
    invoke-interface {v0, v1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglInitialize(Ljavax/microedition/khronos/egl/EGLDisplay;[I)Z

    .line 770
    const/4 v2, 0x1

    new-array v9, v2, [I

    .line 771
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-interface {v0, v1, v2, v3, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-nez v2, :cond_1

    .line 772
    const-string v0, "CheckinRequestBuilder"

    const-string v1, "Couldn\'t get EGL config count"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 777
    :cond_1
    const/4 v2, 0x0

    aget v2, v9, v2

    new-array v10, v2, [Ljavax/microedition/khronos/egl/EGLConfig;

    .line 778
    const/4 v2, 0x0

    aget v2, v9, v2

    invoke-interface {v0, v1, v10, v2, v9}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigs(Ljavax/microedition/khronos/egl/EGLDisplay;[Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 779
    const-string v0, "CheckinRequestBuilder"

    const-string v1, "Couldn\'t get EGL configs"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 786
    :cond_2
    const/4 v2, 0x5

    new-array v3, v2, [I

    fill-array-data v3, :array_0

    .line 793
    const/4 v2, 0x3

    new-array v11, v2, [I

    fill-array-data v11, :array_1

    .line 797
    const/4 v2, 0x1

    new-array v12, v2, [I

    .line 799
    const/4 v6, 0x0

    .line 800
    const/4 v4, 0x0

    .line 801
    const/4 v2, 0x0

    move v7, v4

    move v13, v2

    move v2, v6

    move v6, v13

    :goto_1
    const/4 v4, 0x0

    aget v4, v9, v4

    if-ge v6, v4, :cond_5

    .line 803
    aget-object v4, v10, v6

    const/16 v8, 0x3027

    invoke-interface {v0, v1, v4, v8, v12}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 804
    const/4 v4, 0x0

    aget v4, v12, v4

    const/16 v8, 0x3050

    if-eq v4, v8, :cond_7

    .line 805
    aget-object v4, v10, v6

    const/16 v8, 0x3033

    invoke-interface {v0, v1, v4, v8, v12}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 812
    const/4 v4, 0x0

    aget v4, v12, v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_7

    .line 813
    aget-object v4, v10, v6

    const/16 v8, 0x3040

    invoke-interface {v0, v1, v4, v8, v12}, Ljavax/microedition/khronos/egl/EGL10;->eglGetConfigAttrib(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;I[I)Z

    .line 819
    if-nez v2, :cond_6

    const/4 v4, 0x0

    aget v4, v12, v4

    and-int/lit8 v4, v4, 0x1

    if-eqz v4, :cond_6

    .line 820
    aget-object v2, v10, v6

    const/4 v4, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/checkin/a;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V

    .line 821
    const/4 v2, 0x1

    move v8, v2

    .line 823
    :goto_2
    if-nez v7, :cond_3

    const/4 v2, 0x0

    aget v2, v12, v2

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_3

    .line 824
    aget-object v2, v10, v6

    move-object v4, v11

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/checkin/a;->a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V

    .line 825
    const/4 v7, 0x1

    .line 827
    :cond_3
    if-eqz v8, :cond_4

    if-nez v7, :cond_5

    :cond_4
    move v4, v7

    move v7, v8

    .line 828
    :goto_3
    add-int/lit8 v2, v6, 0x1

    move v6, v2

    move v2, v7

    move v7, v4

    goto :goto_1

    .line 833
    :cond_5
    invoke-interface {v0, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglTerminate(Ljavax/microedition/khronos/egl/EGLDisplay;)Z

    goto/16 :goto_0

    :cond_6
    move v8, v2

    goto :goto_2

    :cond_7
    move v4, v7

    move v7, v2

    goto :goto_3

    .line 786
    nop

    :array_0
    .array-data 4
        0x3057
        0x1
        0x3056
        0x1
        0x3038
    .end array-data

    .line 793
    :array_1
    .array-data 4
        0x3098
        0x2
        0x3038
    .end array-data
.end method

.method public static a(Landroid/content/Context;JLcom/google/android/gms/checkin/b/b;)V
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 482
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 483
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v1

    .line 485
    invoke-static {p0}, Lcom/google/android/gms/e/a;->a(Landroid/content/Context;)J

    move-result-wide v2

    .line 486
    invoke-virtual {p3, v2, v3}, Lcom/google/android/gms/checkin/b/b;->a(J)Lcom/google/android/gms/checkin/b/b;

    .line 488
    cmp-long v4, p1, v6

    if-eqz v4, :cond_0

    .line 489
    invoke-virtual {p3, p1, p2}, Lcom/google/android/gms/checkin/b/b;->c(J)Lcom/google/android/gms/checkin/b/b;

    .line 495
    :cond_0
    cmp-long v4, p1, v6

    if-nez v4, :cond_1

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 496
    :cond_1
    sget v2, Lcom/google/android/gms/checkin/a;->a:I

    invoke-virtual {p3, v2}, Lcom/google/android/gms/checkin/b/b;->a(I)Lcom/google/android/gms/checkin/b/b;

    .line 499
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/e/a;->b(Landroid/content/Context;)J

    move-result-wide v2

    .line 500
    invoke-virtual {p3, v2, v3}, Lcom/google/android/gms/checkin/b/b;->b(J)Lcom/google/android/gms/checkin/b/b;

    .line 504
    const-string v2, "digest"

    invoke-static {v0, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 505
    if-nez v0, :cond_3

    const-string v0, ""

    :cond_3
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->f(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 507
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    const-string v2, "unknown"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 509
    sget-object v0, Landroid/os/Build;->SERIAL:Ljava/lang/String;

    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 512
    :cond_4
    iget-object v0, p3, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    .line 513
    invoke-virtual {v1}, Lcom/google/android/gms/a/a;->b()I

    move-result v2

    .line 514
    invoke-virtual {v0, v2}, Lcom/google/android/gms/checkin/b/j;->a(I)Lcom/google/android/gms/checkin/b/j;

    .line 516
    invoke-virtual {v1, p0}, Lcom/google/android/gms/a/a;->d(Landroid/content/Context;)I

    move-result v0

    .line 518
    if-ltz v0, :cond_5

    .line 519
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->c(I)Lcom/google/android/gms/checkin/b/b;

    .line 521
    :cond_5
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 5

    .prologue
    .line 249
    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    iget-object v0, v0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    .line 251
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "client_id"

    invoke-static {v1, v2}, Lcom/google/android/gsf/e;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 253
    if-eqz v1, :cond_0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/checkin/b/i;->f(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 255
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    .line 257
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/checkin/b/i;->a(I)Lcom/google/android/gms/checkin/b/i;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 262
    :goto_0
    return-void

    .line 259
    :catch_0
    move-exception v0

    .line 260
    const-string v2, "CheckinRequestBuilder"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Our own package not found: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1, v0}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 571
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    .line 572
    const-string v0, "com.google"

    invoke-virtual {v1, v0}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    .line 573
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 574
    const-string v3, "["

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 575
    const-string v3, "com.google"

    invoke-virtual {v1, v3, v0}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 578
    :cond_1
    return-void
.end method

.method public static a(Landroid/content/SharedPreferences;Lcom/google/android/gms/checkin/b/b;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 164
    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    iget-object v2, v0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    .line 166
    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Landroid/os/Build;->FINGERPRINT:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 167
    :cond_0
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->c()Ljava/lang/String;

    move-result-object v0

    .line 168
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    move-object v0, v1

    .line 169
    :cond_1
    const-string v3, "CheckinTask_lastRadio"

    invoke-interface {p0, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 170
    if-nez v0, :cond_b

    move-object v0, v1

    .line 178
    :cond_2
    :goto_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 179
    :cond_3
    sget-object v0, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    sget-object v0, Landroid/os/Build;->BOOTLOADER:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 180
    :cond_4
    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    sget-object v0, Landroid/os/Build;->HARDWARE:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 181
    :cond_5
    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    sget-object v0, Landroid/os/Build;->BRAND:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->e(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 182
    :cond_6
    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    sget-object v0, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->g(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 183
    :cond_7
    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    sget-object v0, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->h(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 184
    :cond_8
    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    sget-object v0, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->i(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 185
    :cond_9
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->j(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/i;

    .line 186
    :cond_a
    sget-wide v0, Landroid/os/Build;->TIME:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v0, v4

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/checkin/b/i;->a(J)Lcom/google/android/gms/checkin/b/i;

    .line 187
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->b(I)Lcom/google/android/gms/checkin/b/i;

    .line 195
    new-instance v0, Ljava/io/File;

    const-string v1, "/system/recovery-from-boot.p"

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/i;->a(Z)Lcom/google/android/gms/checkin/b/i;

    .line 197
    invoke-static {p1}, Lcom/google/android/gms/checkin/a;->b(Lcom/google/android/gms/checkin/b/b;)V

    .line 198
    return-void

    .line 173
    :cond_b
    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 174
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "CheckinTask_lastRadio"

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto/16 :goto_0
.end method

.method public static a(Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Lcom/google/android/gms/checkin/b/b;)V
    .locals 6

    .prologue
    .line 374
    iget-object v1, p3, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    .line 376
    if-eqz p0, :cond_2

    .line 377
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v0

    .line 379
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {p0}, Lcom/google/android/gms/a/a;->a(Landroid/telephony/TelephonyManager;)I

    move-result v2

    .line 380
    packed-switch v2, :pswitch_data_0

    .line 413
    const-string v3, "CheckinRequestBuilder"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unknown phone type: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " id="

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    :cond_0
    :goto_0
    :pswitch_0
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getNetworkOperator()Ljava/lang/String;

    move-result-object v0

    .line 418
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/checkin/b/j;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    .line 419
    :cond_1
    invoke-virtual {p0}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    .line 420
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v1, v0}, Lcom/google/android/gms/checkin/b/j;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    .line 425
    :cond_2
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_3

    .line 427
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getExtraInfo()Ljava/lang/String;

    move-result-object v0

    .line 428
    if-eqz v0, :cond_3

    sget-object v2, Lcom/google/android/gms/checkin/a;->b:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 429
    const-string v2, ":"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "-"

    const-string v3, ""

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    .line 430
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 431
    const-string v0, "ethernet"

    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 435
    :cond_3
    invoke-virtual {p2}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    .line 436
    if-eqz v0, :cond_4

    .line 437
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getSubtypeName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "r"

    :goto_1
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/checkin/b/j;->d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    .line 441
    :cond_4
    if-eqz p1, :cond_5

    .line 445
    invoke-virtual {p1}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    .line 446
    if-eqz v0, :cond_5

    .line 447
    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getMacAddress()Ljava/lang/String;

    move-result-object v0

    .line 448
    if-eqz v0, :cond_5

    .line 449
    const-string v1, ":"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 450
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 451
    const-string v0, "wifi"

    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 455
    :cond_5
    return-void

    .line 382
    :pswitch_1
    if-eqz v0, :cond_7

    sget-object v2, Lcom/google/android/gms/checkin/a;->e:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_7

    .line 386
    const/4 v2, 0x0

    const/4 v3, 0x2

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    const-string v3, "80"

    if-ne v2, v3, :cond_6

    .line 387
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "TelephonyManager.getDeviceId() is returning a pseudo-ESN instead of an MEID"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 390
    :cond_6
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->e(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    goto/16 :goto_0

    .line 398
    :cond_7
    :pswitch_2
    if-eqz v0, :cond_0

    .line 399
    sget-object v2, Lcom/google/android/gms/checkin/a;->d:Ljava/util/regex/Pattern;

    invoke-virtual {v2, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-eqz v2, :cond_8

    .line 400
    invoke-virtual {p3, v0}, Lcom/google/android/gms/checkin/b/b;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    goto/16 :goto_0

    .line 402
    :cond_8
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "TelephonyManager.getDeviceId() must return a 15-decimal-digit IMEI, a 14-hex-digit MEID or an 8-hex-digit ESN "

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 437
    :cond_9
    const-string v0, ""

    goto :goto_1

    .line 380
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static a(Lcom/google/android/gms/checkin/b/b;)V
    .locals 2

    .prologue
    .line 927
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/a/a;->j()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 928
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    const-string v1, "te.bots"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/checkin/b/j;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    .line 930
    :cond_0
    return-void
.end method

.method public static a(Ljava/util/Locale;Lcom/google/android/gms/checkin/b/b;)V
    .locals 1

    .prologue
    .line 462
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->g(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 463
    return-void

    .line 462
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/util/TimeZone;Lcom/google/android/gms/checkin/b/b;)V
    .locals 1

    .prologue
    .line 470
    if-eqz p0, :cond_0

    .line 471
    invoke-virtual {p0}, Ljava/util/TimeZone;->getID()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->i(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 473
    :cond_0
    return-void
.end method

.method private static a(Ljavax/microedition/khronos/egl/EGL10;Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I[ILjava/util/Set;)V
    .locals 6

    .prologue
    .line 720
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {p0, p1, p2, v0, p4}, Ljavax/microedition/khronos/egl/EGL10;->eglCreateContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;Ljavax/microedition/khronos/egl/EGLContext;[I)Ljavax/microedition/khronos/egl/EGLContext;

    move-result-object v1

    .line 723
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    if-ne v1, v0, :cond_0

    .line 752
    :goto_0
    return-void

    .line 728
    :cond_0
    invoke-interface {p0, p1, p2, p3}, Ljavax/microedition/khronos/egl/EGL10;->eglCreatePbufferSurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLConfig;[I)Ljavax/microedition/khronos/egl/EGLSurface;

    move-result-object v2

    .line 729
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    if-ne v2, v0, :cond_1

    .line 730
    invoke-interface {p0, p1, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    goto :goto_0

    .line 735
    :cond_1
    invoke-interface {p0, p1, v2, v2, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 738
    const/16 v0, 0x1f03

    invoke-static {v0}, Landroid/opengl/GLES10;->glGetString(I)Ljava/lang/String;

    move-result-object v0

    .line 739
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 742
    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v4, v3

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v4, :cond_2

    aget-object v5, v3, v0

    .line 743
    invoke-interface {p5, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 742
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 748
    :cond_2
    sget-object v0, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v3, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_SURFACE:Ljavax/microedition/khronos/egl/EGLSurface;

    sget-object v4, Ljavax/microedition/khronos/egl/EGL10;->EGL_NO_CONTEXT:Ljavax/microedition/khronos/egl/EGLContext;

    invoke-interface {p0, p1, v0, v3, v4}, Ljavax/microedition/khronos/egl/EGL10;->eglMakeCurrent(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLSurface;Ljavax/microedition/khronos/egl/EGLContext;)Z

    .line 750
    invoke-interface {p0, p1, v2}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroySurface(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLSurface;)Z

    .line 751
    invoke-interface {p0, p1, v1}, Ljavax/microedition/khronos/egl/EGL10;->eglDestroyContext(Ljavax/microedition/khronos/egl/EGLDisplay;Ljavax/microedition/khronos/egl/EGLContext;)Z

    goto :goto_0
.end method

.method private static a(ZLjava/lang/String;)Z
    .locals 1

    .prologue
    .line 1308
    if-nez p0, :cond_0

    if-eqz p1, :cond_0

    const-string v0, "exp_det_"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v3, 0x0

    .line 1019
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 1022
    const/16 v2, 0x40

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 1028
    iget-object v1, v1, Landroid/content/pm/PackageInfo;->signatures:[Landroid/content/pm/Signature;

    .line 1029
    aget-object v1, v1, v3

    invoke-virtual {v1}, Landroid/content/pm/Signature;->toByteArray()[B

    move-result-object v1

    .line 1032
    :try_start_1
    const-string v2, "SHA1"

    invoke-static {v2}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_1
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    .line 1037
    array-length v2, v1

    invoke-virtual {v0, v1, v3, v2}, Ljava/security/MessageDigest;->update([BII)V

    .line 1038
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    :goto_0
    return-object v0

    .line 1024
    :catch_0
    move-exception v1

    .line 1025
    const-string v2, "CheckinRequestBuilder"

    const-string v3, "package info for managing app not found:"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 1033
    :catch_1
    move-exception v1

    .line 1034
    const-string v2, "CheckinRequestBuilder"

    const-string v3, "no support for SHA-1?"

    invoke-static {v2, v3, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 318
    iget-object v4, p1, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    .line 321
    :try_start_0
    iget-object v0, v4, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    iget-boolean v0, v0, Lcom/google/android/gms/checkin/b/i;->a:Z

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, v4, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    iget-object v0, v0, Lcom/google/android/gms/checkin/b/i;->b:Ljava/lang/String;

    .line 325
    :cond_0
    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    sget-object v0, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    const-string v3, "glass_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 327
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "Classify the device as Glass."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 328
    const/4 v0, 0x5

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    .line 355
    :goto_0
    return-void

    .line 329
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v3, "com.google.android.tv"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.hardware.type.television"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_2

    const-string v3, "android.software.leanback"

    invoke-virtual {v0, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_2
    move v0, v1

    :goto_1
    if-eqz v0, :cond_4

    .line 330
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "Classify the device as TV."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 331
    const/4 v0, 0x4

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 351
    :catch_0
    move-exception v0

    .line 352
    const-string v2, "CheckinRequestBuilder"

    const-string v3, "Could not determinate device sub type!"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 353
    invoke-virtual {v4, v1}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :cond_3
    move v0, v2

    .line 329
    goto :goto_1

    .line 332
    :cond_4
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v0

    const/16 v3, 0xa

    if-ne v0, v3, :cond_5

    move v0, v1

    :goto_2
    if-eqz v0, :cond_6

    .line 333
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "Classify the device as Wearable."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 334
    const/4 v0, 0x7

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :cond_5
    move v0, v2

    .line 332
    goto :goto_2

    .line 335
    :cond_6
    const-string v0, "phone"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    if-eqz v0, :cond_9

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getPhoneType()I

    move-result v0

    if-eq v0, v5, :cond_7

    if-ne v0, v1, :cond_8

    :cond_7
    move v3, v1

    .line 340
    :goto_3
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    if-lt v0, v6, :cond_a

    move v0, v1

    .line 341
    :goto_4
    if-nez v3, :cond_b

    if-eqz v0, :cond_b

    .line 342
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "Classify the device as Tablet."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    const/4 v0, 0x3

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    goto/16 :goto_0

    :cond_8
    move v3, v2

    .line 335
    goto :goto_3

    :cond_9
    move v3, v2

    goto :goto_3

    :cond_a
    move v0, v2

    .line 340
    goto :goto_4

    .line 344
    :cond_b
    if-eqz v3, :cond_c

    if-nez v0, :cond_c

    .line 345
    const-string v0, "CheckinRequestBuilder"

    const-string v2, "Classify the device as Phone."

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    const/4 v0, 0x2

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    goto/16 :goto_0

    .line 348
    :cond_c
    const/4 v0, 0x1

    invoke-virtual {v4, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method private static b(Lcom/google/android/gms/checkin/b/b;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 205
    const/4 v3, 0x0

    .line 209
    :try_start_0
    new-instance v2, Ljava/util/zip/ZipFile;

    sget-object v0, Lcom/google/android/gms/checkin/a;->c:Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/util/zip/ZipFile;-><init>(Ljava/io/File;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 210
    :try_start_1
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->entries()Ljava/util/Enumeration;

    move-result-object v3

    .line 211
    const/16 v0, 0x800

    new-array v4, v0, [B

    .line 212
    :goto_0
    invoke-interface {v3}, Ljava/util/Enumeration;->hasMoreElements()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 213
    invoke-interface {v3}, Ljava/util/Enumeration;->nextElement()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/zip/ZipEntry;

    .line 214
    invoke-virtual {v2, v0}, Ljava/util/zip/ZipFile;->getInputStream(Ljava/util/zip/ZipEntry;)Ljava/io/InputStream;

    move-result-object v0

    .line 215
    const-string v5, "SHA-1"

    invoke-static {v5}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v5

    .line 216
    if-nez v5, :cond_2

    .line 217
    const-string v5, "CheckinRequestBuilder"

    const-string v6, "no support for SHA-1?"

    invoke-static {v5, v6}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 218
    const-string v5, "--NoSuchAlgorithmException--"

    invoke-virtual {p0, v5}, Lcom/google/android/gms/checkin/b/b;->j(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 219
    add-int/lit8 v1, v1, 0x1

    .line 229
    :goto_1
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    :goto_2
    if-eqz v2, :cond_0

    :try_start_2
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V

    :cond_0
    throw v0
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 238
    :catch_0
    move-exception v0

    .line 235
    const-string v2, "CheckinRequestBuilder"

    const-string v3, "error reading OTA certs"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 236
    const-string v0, "--IOException--"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/b;->j(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 237
    add-int/lit8 v1, v1, 0x1

    .line 239
    :goto_3
    if-nez v1, :cond_1

    .line 240
    const-string v0, "--no-output--"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/b;->j(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 242
    :cond_1
    return-void

    .line 222
    :cond_2
    :goto_4
    :try_start_3
    invoke-virtual {v0, v4}, Ljava/io/InputStream;->read([B)I

    move-result v6

    if-lez v6, :cond_3

    .line 223
    const/4 v7, 0x0

    invoke-virtual {v5, v4, v7, v6}, Ljava/security/MessageDigest;->update([BII)V

    goto :goto_4

    .line 225
    :cond_3
    invoke-virtual {v5}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v5

    const/4 v6, 0x2

    invoke-static {v5, v6}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v5

    .line 226
    invoke-virtual {p0, v5}, Lcom/google/android/gms/checkin/b/b;->j(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 227
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 232
    :cond_4
    :try_start_4
    invoke-virtual {v2}, Ljava/util/zip/ZipFile;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_3

    :catchall_1
    move-exception v0

    move-object v2, v3

    goto :goto_2
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 585
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 586
    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v3

    .line 587
    array-length v4, v3

    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, v3, v2

    .line 590
    :try_start_0
    iget-object v0, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    const-string v6, "AndroidCheckInServer"

    const/4 v7, 0x0

    invoke-static {p0, v0, v6, v7}, Lcom/google/android/gms/auth/r;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Landroid/os/Bundle;)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/af; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    .line 606
    :goto_1
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "["

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v5, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "]"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p1, v5}, Lcom/google/android/gms/checkin/b/b;->h(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 607
    if-eqz v0, :cond_0

    const-string v5, ""

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 608
    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->h(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 587
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 593
    :catch_0
    move-exception v0

    const-string v0, "CheckinRequestBuilder"

    const-string v6, "awaiting user notification for token"

    invoke-static {v0, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move-object v0, v1

    .line 602
    goto :goto_1

    .line 596
    :catch_1
    move-exception v0

    .line 598
    const-string v6, "CheckinRequestBuilder"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Unrecoverable authentication exception: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/auth/q;->getMessage()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    .line 602
    goto :goto_1

    .line 600
    :catch_2
    move-exception v0

    .line 601
    const-string v6, "CheckinRequestBuilder"

    const-string v7, "error reading account token"

    invoke-static {v6, v7, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_1

    .line 614
    :cond_1
    array-length v0, v3

    if-nez v0, :cond_2

    .line 615
    const-string v0, ""

    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->h(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 617
    :cond_2
    return-void
.end method

.method public static d(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 9

    .prologue
    const/4 v5, 0x4

    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v4, 0x0

    .line 839
    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->n:Lcom/google/android/gms/checkin/b/g;

    if-nez v0, :cond_0

    .line 840
    new-instance v0, Lcom/google/android/gms/checkin/b/g;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/g;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->a(Lcom/google/android/gms/checkin/b/g;)Lcom/google/android/gms/checkin/b/b;

    .line 842
    :cond_0
    iget-object v6, p1, Lcom/google/android/gms/checkin/b/b;->n:Lcom/google/android/gms/checkin/b/g;

    .line 844
    const-string v0, "activity"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager;

    .line 845
    invoke-virtual {v0}, Landroid/app/ActivityManager;->getDeviceConfigurationInfo()Landroid/content/pm/ConfigurationInfo;

    move-result-object v7

    .line 848
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqTouchScreen:I

    packed-switch v0, :pswitch_data_0

    move v0, v4

    :goto_0
    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->a(I)Lcom/google/android/gms/checkin/b/g;

    .line 849
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqKeyboardType:I

    packed-switch v0, :pswitch_data_1

    move v0, v4

    :goto_1
    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->b(I)Lcom/google/android/gms/checkin/b/g;

    .line 850
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqNavigation:I

    packed-switch v0, :pswitch_data_2

    move v0, v4

    :goto_2
    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->c(I)Lcom/google/android/gms/checkin/b/g;

    .line 852
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    .line 853
    iget v0, v0, Landroid/content/res/Configuration;->screenLayout:I

    and-int/lit8 v0, v0, 0xf

    packed-switch v0, :pswitch_data_3

    move v2, v4

    :goto_3
    :pswitch_0
    invoke-virtual {v6, v2}, Lcom/google/android/gms/checkin/b/g;->d(I)Lcom/google/android/gms/checkin/b/g;

    .line 855
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v0, v0, 0x1

    if-lez v0, :cond_1

    move v0, v1

    :goto_4
    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->a(Z)Lcom/google/android/gms/checkin/b/g;

    .line 857
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqInputFeatures:I

    and-int/lit8 v0, v0, 0x2

    if-lez v0, :cond_2

    :goto_5
    invoke-virtual {v6, v1}, Lcom/google/android/gms/checkin/b/g;->b(Z)Lcom/google/android/gms/checkin/b/g;

    .line 861
    const-string v0, "window"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 862
    new-instance v1, Landroid/util/DisplayMetrics;

    invoke-direct {v1}, Landroid/util/DisplayMetrics;-><init>()V

    .line 863
    invoke-interface {v0}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 864
    iget v0, v1, Landroid/util/DisplayMetrics;->densityDpi:I

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->e(I)Lcom/google/android/gms/checkin/b/g;

    .line 865
    iget v0, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->f(I)Lcom/google/android/gms/checkin/b/g;

    .line 866
    iget v0, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->g(I)Lcom/google/android/gms/checkin/b/g;

    .line 869
    iget v0, v7, Landroid/content/pm/ConfigurationInfo;->reqGlEsVersion:I

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->h(I)Lcom/google/android/gms/checkin/b/g;

    .line 872
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 873
    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getSystemSharedLibraryNames()[Ljava/lang/String;

    move-result-object v2

    .line 874
    if-eqz v2, :cond_3

    .line 875
    invoke-static {v2}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 876
    array-length v3, v2

    move v0, v4

    :goto_6
    if-ge v0, v3, :cond_3

    aget-object v5, v2, v0

    .line 877
    invoke-virtual {v6, v5}, Lcom/google/android/gms/checkin/b/g;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    .line 876
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    :pswitch_1
    move v0, v1

    .line 848
    goto :goto_0

    :pswitch_2
    move v0, v2

    goto :goto_0

    :pswitch_3
    move v0, v3

    goto/16 :goto_0

    :pswitch_4
    move v0, v1

    .line 849
    goto :goto_1

    :pswitch_5
    move v0, v2

    goto :goto_1

    :pswitch_6
    move v0, v3

    goto :goto_1

    :pswitch_7
    move v0, v1

    .line 850
    goto :goto_2

    :pswitch_8
    move v0, v2

    goto :goto_2

    :pswitch_9
    move v0, v3

    goto :goto_2

    :pswitch_a
    move v0, v5

    goto :goto_2

    :pswitch_b
    move v2, v1

    .line 853
    goto :goto_3

    :pswitch_c
    move v2, v3

    goto :goto_3

    :pswitch_d
    move v2, v5

    goto :goto_3

    :cond_1
    move v0, v4

    .line 855
    goto :goto_4

    :cond_2
    move v1, v4

    .line 857
    goto :goto_5

    .line 882
    :cond_3
    invoke-virtual {v1}, Landroid/content/pm/PackageManager;->getSystemAvailableFeatures()[Landroid/content/pm/FeatureInfo;

    move-result-object v3

    .line 885
    if-eqz v3, :cond_5

    .line 886
    array-length v0, v3

    new-array v5, v0, [Ljava/lang/String;

    .line 888
    array-length v7, v3

    move v2, v4

    move v1, v4

    :goto_7
    if-ge v2, v7, :cond_4

    aget-object v8, v3, v2

    .line 889
    iget-object v0, v8, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 890
    add-int/lit8 v0, v1, 0x1

    iget-object v8, v8, Landroid/content/pm/FeatureInfo;->name:Ljava/lang/String;

    aput-object v8, v5, v1

    .line 888
    :goto_8
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_7

    .line 893
    :cond_4
    invoke-static {v5, v4, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;II)V

    move v0, v4

    .line 894
    :goto_9
    if-ge v0, v1, :cond_5

    .line 895
    aget-object v2, v5, v0

    invoke-virtual {v6, v2}, Lcom/google/android/gms/checkin/b/g;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    .line 894
    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    .line 900
    :cond_5
    sget-object v0, Landroid/os/Build;->CPU_ABI:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    .line 905
    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    const-string v1, "unknown"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 906
    sget-object v0, Landroid/os/Build;->CPU_ABI2:Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    .line 910
    :cond_6
    invoke-virtual {p0}, Landroid/content/Context;->getAssets()Landroid/content/res/AssetManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/AssetManager;->getLocales()[Ljava/lang/String;

    move-result-object v0

    .line 911
    if-eqz v0, :cond_8

    .line 912
    invoke-static {v0}, Ljava/util/Arrays;->sort([Ljava/lang/Object;)V

    .line 913
    array-length v1, v0

    :goto_a
    if-ge v4, v1, :cond_8

    aget-object v2, v0, v4

    .line 914
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_7

    .line 915
    invoke-virtual {v6, v2}, Lcom/google/android/gms/checkin/b/g;->d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    .line 913
    :cond_7
    add-int/lit8 v4, v4, 0x1

    goto :goto_a

    .line 921
    :cond_8
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "CheckinService_cachedGlExt"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p0}, Lcom/google/android/gms/checkin/CheckinService;->b(Landroid/content/Context;)Z

    move-result v3

    if-nez v3, :cond_9

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_9

    const-string v1, " "

    invoke-virtual {v2, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    :goto_b
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_c
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 922
    invoke-virtual {v6, v0}, Lcom/google/android/gms/checkin/b/g;->e(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/g;

    goto :goto_c

    .line 921
    :cond_9
    invoke-static {}, Lcom/google/android/gms/checkin/a;->a()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    const-string v2, " "

    invoke-static {v2, v0}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v3, "CheckinService_cachedGlExt"

    invoke-interface {v1, v3, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_b

    .line 924
    :cond_a
    return-void

    :cond_b
    move v0, v1

    goto/16 :goto_8

    .line 848
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 849
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 850
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 853
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_b
        :pswitch_0
        :pswitch_c
        :pswitch_d
    .end packed-switch
.end method

.method public static e(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 8

    .prologue
    .line 937
    iget v0, p1, Lcom/google/android/gms/checkin/b/b;->o:I

    if-nez v0, :cond_5

    invoke-static {}, Ljava/lang/Math;->random()D

    move-result-wide v0

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "checkin_droidguard_percent"

    invoke-static {v2, v3}, Lcom/google/android/gsf/f;->b(Landroid/content/ContentResolver;Ljava/lang/String;)F

    move-result v2

    float-to-double v2, v2

    cmpg-double v0, v0, v2

    if-gez v0, :cond_5

    .line 940
    const/4 v1, 0x0

    .line 942
    :try_start_0
    const-string v3, "checkin"

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    new-instance v5, Ljava/lang/StringBuffer;

    invoke-direct {v5}, Ljava/lang/StringBuffer;-><init>()V

    invoke-static {p0}, Lcom/google/android/gms/e/a;->a(Landroid/content/Context;)J

    move-result-wide v6

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuffer;->append(J)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-boolean v0, p1, Lcom/google/android/gms/checkin/b/b;->a:Z

    if-eqz v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->b:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/checkin/b/b;->c:Z

    if-eqz v0, :cond_1

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->d:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_1
    iget-boolean v0, p1, Lcom/google/android/gms/checkin/b/b;->i:Z

    if-eqz v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->j:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_2
    iget-boolean v0, p1, Lcom/google/android/gms/checkin/b/b;->g:Z

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->h:Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    :cond_3
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->e:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v2, v0, :cond_4

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->e:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    iget-object v0, p1, Lcom/google/android/gms/checkin/b/b;->f:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    const/16 v0, 0x3a

    invoke-virtual {v5, v0}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_4
    invoke-virtual {v5}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    const-string v2, "SHA-1"

    invoke-static {v2}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v2

    if-nez v2, :cond_6

    const-string v0, "CheckinRequestBuilder"

    const-string v2, "no support for SHA-1"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    invoke-static {p0, v3, v4}, Lcom/google/android/gms/droidguard/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/util/Map;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 949
    :goto_2
    if-eqz v0, :cond_5

    .line 950
    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->k(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 953
    :cond_5
    return-void

    .line 942
    :cond_6
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/security/MessageDigest;->update([B)V

    const-string v0, "CONTENT_BINDER"

    invoke-virtual {v2}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v2

    const/4 v5, 0x2

    invoke-static {v2, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4, v0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 946
    :catch_0
    move-exception v0

    .line 947
    const-string v2, "CheckinRequestBuilder"

    const-string v3, "Droidguard runtime exception: "

    invoke-static {v2, v3, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v1

    goto :goto_2
.end method

.method public static f(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 3

    .prologue
    .line 960
    invoke-static {p0}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 961
    const-string v1, "CheckinService_deviceDataVersionInfo"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 963
    if-eqz v0, :cond_1

    .line 964
    const-string v1, "CheckinRequestBuilder"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 965
    const-string v1, "CheckinRequestBuilder"

    const-string v2, "Found a deviceDataVersionInfo stored. Adding it to the request."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 967
    :cond_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/b;->l(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/b;

    .line 969
    :cond_1
    return-void
.end method

.method public static g(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V
    .locals 3

    .prologue
    .line 1046
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v1

    .line 1047
    invoke-virtual {v1, p0}, Lcom/google/android/gms/a/a;->f(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1048
    if-nez v0, :cond_0

    .line 1049
    invoke-virtual {v1, p0}, Lcom/google/android/gms/a/a;->g(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1051
    :cond_0
    if-eqz v0, :cond_2

    .line 1052
    invoke-static {p0, v0}, Lcom/google/android/gms/checkin/a;->a(Landroid/content/Context;Ljava/lang/String;)[B

    move-result-object v1

    .line 1053
    new-instance v2, Lcom/google/android/gms/checkin/b/e;

    invoke-direct {v2}, Lcom/google/android/gms/checkin/b/e;-><init>()V

    .line 1054
    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/e;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/e;

    .line 1055
    if-eqz v1, :cond_1

    invoke-static {v1}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/checkin/b/e;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/checkin/b/e;

    .line 1056
    :cond_1
    invoke-virtual {p1, v2}, Lcom/google/android/gms/checkin/b/b;->a(Lcom/google/android/gms/checkin/b/e;)Lcom/google/android/gms/checkin/b/b;

    .line 1058
    :cond_2
    return-void
.end method
