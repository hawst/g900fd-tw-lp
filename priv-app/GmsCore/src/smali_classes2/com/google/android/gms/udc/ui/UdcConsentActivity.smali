.class public Lcom/google/android/gms/udc/ui/UdcConsentActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/udc/ui/i;
.implements Lcom/google/android/gms/udc/ui/q;
.implements Lcom/google/android/gms/udc/ui/r;


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Ljava/lang/String;

.field private c:Lcom/google/android/gms/udc/ConsentFlowConfig;

.field private d:Lcom/google/android/gms/udc/e/d;

.field private e:Lcom/google/android/gms/udc/e/e;

.field private f:Landroid/content/Intent;

.field private g:Lcom/google/android/gms/udc/e/x;

.field private h:Z

.field private i:Lcom/google/android/gms/udc/util/j;

.field private j:Lcom/google/android/gms/udc/ui/l;

.field private k:Lcom/google/android/gms/udc/ui/m;

.field private l:Lcom/google/android/gms/analytics/bv;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 338
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/udc/e/d;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 99
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 100
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 101
    const-string v1, "UdcConsentRequest"

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/udc/util/f;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 103
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;Lcom/google/android/gms/udc/e/e;)Lcom/google/android/gms/udc/e/e;
    .locals 0

    .prologue
    .line 51
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e:Lcom/google/android/gms/udc/e/e;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    return-object v0
.end method

.method public static a(Landroid/content/Intent;)[I
    .locals 1

    .prologue
    .line 107
    const-string v0, "UdcConsentResultIds"

    invoke-virtual {p0, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Intent;)I
    .locals 2

    .prologue
    .line 111
    const-string v0, "UdcConsentResultValues"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/d;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Landroid/util/Pair;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e:Lcom/google/android/gms/udc/e/e;

    iget-object v1, v1, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v1, v1

    new-array v2, v1, [I

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e:Lcom/google/android/gms/udc/e/e;

    iget-object v1, v1, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v1, v1

    new-array v3, v1, [I

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e:Lcom/google/android/gms/udc/e/e;

    iget-object v4, v1, Lcom/google/android/gms/udc/e/e;->h:[Lcom/google/android/gms/udc/e/q;

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v0, v5, :cond_0

    aget-object v6, v4, v0

    iget-object v7, v6, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    iget v7, v7, Lcom/google/android/gms/udc/e/p;->a:I

    aput v7, v2, v1

    iget-object v6, v6, Lcom/google/android/gms/udc/e/q;->a:Lcom/google/android/gms/udc/e/p;

    iget v6, v6, Lcom/google/android/gms/udc/e/p;->b:I

    aput v6, v3, v1

    add-int/lit8 v1, v1, 0x1

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/analytics/bv;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/ConsentFlowConfig;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->c:Lcom/google/android/gms/udc/ConsentFlowConfig;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h:Z

    return v0
.end method

.method static synthetic i(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/x;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->f:Landroid/content/Intent;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/x;
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->j:Lcom/google/android/gms/udc/ui/l;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 249
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/t;)V
    .locals 4

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    const-string v1, "WriteConsent"

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget-object v2, v2, Lcom/google/android/gms/udc/e/d;->a:[I

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v3, v3, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;[II)V

    .line 229
    new-instance v0, Lcom/google/android/gms/udc/e/x;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/x;-><init>()V

    .line 230
    new-instance v1, Lcom/google/android/gms/udc/e/y;

    invoke-direct {v1}, Lcom/google/android/gms/udc/e/y;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    .line 231
    iget-object v1, v0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e:Lcom/google/android/gms/udc/e/e;

    iget-object v2, v2, Lcom/google/android/gms/udc/e/e;->a:[B

    iput-object v2, v1, Lcom/google/android/gms/udc/e/y;->a:[B

    .line 232
    iget-object v1, v0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v2, v2, Lcom/google/android/gms/udc/e/d;->b:I

    iput v2, v1, Lcom/google/android/gms/udc/e/y;->c:I

    .line 233
    iget-object v1, v0, Lcom/google/android/gms/udc/e/x;->a:Lcom/google/android/gms/udc/e/y;

    iput-object p1, v1, Lcom/google/android/gms/udc/e/y;->d:Lcom/google/android/gms/udc/e/t;

    .line 235
    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->k:Lcom/google/android/gms/udc/ui/m;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 237
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    const-string v1, "Cancel"

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget-object v2, v2, Lcom/google/android/gms/udc/e/d;->a:[I

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v3, v3, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;[II)V

    .line 243
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->finish()V

    .line 244
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 253
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h:Z

    .line 254
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 116
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 118
    invoke-static {p0, v8}, Lcom/google/android/gms/udc/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    .line 120
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    new-instance v0, Lcom/google/android/gms/udc/util/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/util/j;-><init>(Landroid/support/v4/app/q;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 131
    const-string v0, "UdcAccountName"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b:Ljava/lang/String;

    .line 132
    const-string v0, "UdcConsentRequest"

    new-instance v2, Lcom/google/android/gms/udc/e/d;

    invoke-direct {v2}, Lcom/google/android/gms/udc/e/d;-><init>()V

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/udc/util/f;->b(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/d;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    const-string v2, "Intent is missing consent request"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->f:Landroid/content/Intent;

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->f:Landroid/content/Intent;

    const-string v2, "UdcConsentResultIds"

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/d;->a:[I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[I)Landroid/content/Intent;

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->f:Landroid/content/Intent;

    const-string v2, "UdcConsentResultValues"

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v3, v3, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 140
    new-instance v0, Lcom/google/android/gms/udc/ui/l;

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/udc/ui/l;-><init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;B)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->j:Lcom/google/android/gms/udc/ui/l;

    .line 141
    new-instance v0, Lcom/google/android/gms/udc/ui/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/ui/m;-><init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->k:Lcom/google/android/gms/udc/ui/m;

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v0, v0, Lcom/google/android/gms/udc/e/d;->c:I

    packed-switch v0, :pswitch_data_0

    .line 155
    const-string v0, "UdcConsent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid ViewType"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v3, v3, Lcom/google/android/gms/udc/e/d;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    const-string v0, "UdcConsentFlowConfigBundle"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    const-string v0, "UdcConsentFlowConfigBundle"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "UdcConsentFlowConfig"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/ConsentFlowConfig;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->c:Lcom/google/android/gms/udc/ConsentFlowConfig;

    .line 163
    :cond_0
    if-eqz p1, :cond_2

    .line 164
    const-string v0, "UdcWriteRequest"

    new-instance v1, Lcom/google/android/gms/udc/e/x;

    invoke-direct {v1}, Lcom/google/android/gms/udc/e/x;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/udc/util/f;->b(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/x;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    .line 166
    const-string v0, "UdcConsentHasScrolledToEnd"

    invoke-virtual {p1, v0, v5}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h:Z

    .line 172
    :goto_1
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/udc/c;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0, p0, v5, v8}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a:Lcom/google/android/gms/common/api/v;

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->j:Lcom/google/android/gms/udc/ui/l;

    invoke-virtual {v0, v6, v1}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/av;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    if-eqz v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->k:Lcom/google/android/gms/udc/ui/m;

    invoke-virtual {v0, v7, v1}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/av;)V

    .line 183
    :cond_1
    :goto_2
    return-void

    .line 121
    :catch_0
    move-exception v0

    .line 122
    const-string v1, "UdcConsent"

    const-string v2, "Couldn\'t verify signature - finishing activity."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    new-instance v2, Lcom/google/android/gms/analytics/bu;

    new-array v3, v6, [Ljava/lang/String;

    const-string v4, "com.google.android.gms.udc"

    aput-object v4, v3, v5

    invoke-static {v3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/analytics/bu;-><init>(Landroid/content/Context;Ljava/util/Collection;)V

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Lcom/google/android/gms/analytics/bg;

    invoke-direct {v4}, Lcom/google/android/gms/analytics/bg;-><init>()V

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/analytics/bu;->a(Ljava/lang/String;Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/analytics/bg;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bg;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/analytics/bg;->a(Z)Lcom/google/android/gms/analytics/bg;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bg;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->finish()V

    goto :goto_2

    .line 145
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    const-string v2, "SettingUi"

    const-string v3, "&dr"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    const-string v2, "%s-%s"

    new-array v3, v7, [Ljava/lang/Object;

    const-string v4, "ConsentPrompt"

    aput-object v4, v3, v5

    iget-object v4, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget v4, v4, Lcom/google/android/gms/udc/e/d;->b:I

    invoke-static {v4}, Lcom/google/android/gms/udc/util/a;->a(I)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "&cd"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 150
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Product_"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->d:Lcom/google/android/gms/udc/e/d;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    iget v3, v3, Lcom/google/android/gms/udc/e/u;->c:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "&dr"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->l:Lcom/google/android/gms/analytics/bv;

    const-string v2, "ConsentFlow"

    const-string v3, "&cd"

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/analytics/bv;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 169
    :cond_2
    iput-boolean v5, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h:Z

    goto/16 :goto_1

    .line 143
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/util/j;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 200
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 201
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 205
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 206
    const-string v0, "UdcConsentHasScrolledToEnd"

    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->h:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 207
    const-string v0, "UdcWriteRequest"

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->g:Lcom/google/android/gms/udc/e/x;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 208
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/q;->onStart()V

    .line 188
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/app/Activity;)V

    .line 189
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 193
    invoke-super {p0}, Landroid/support/v4/app/q;->onStop()V

    .line 194
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->c(Landroid/app/Activity;)V

    .line 195
    return-void
.end method
