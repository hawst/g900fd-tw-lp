.class public abstract Lcom/google/android/gms/auth/trustagent/a;
.super Landroid/preference/PreferenceFragment;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/auth/trustagent/am;

.field b:Z

.field private c:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/a;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;->d()V

    return-void
.end method

.method private d()V
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->b:Z

    .line 97
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/a;->b()V

    .line 98
    return-void
.end method


# virtual methods
.method protected final a()Lcom/google/android/gms/auth/trustagent/am;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    return-object v0
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 36
    return-void
.end method

.method protected final c()Z
    .locals 1

    .prologue
    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->b:Z

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 40
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onCreate(Landroid/os/Bundle;)V

    .line 42
    new-instance v0, Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/a;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-string v2, "auth_"

    new-instance v3, Lcom/google/android/gms/auth/trustagent/b;

    invoke-direct {v3, p0}, Lcom/google/android/gms/auth/trustagent/b;-><init>(Lcom/google/android/gms/auth/trustagent/a;)V

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/auth/trustagent/am;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/auth/trustagent/aq;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->a:Landroid/content/Context;

    new-instance v3, Landroid/content/Intent;

    iget-object v4, v0, Lcom/google/android/gms/auth/trustagent/am;->a:Landroid/content/Context;

    const-class v5, Lcom/google/android/gms/auth/trustagent/PreferenceService;

    invoke-direct {v3, v4, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->h:Landroid/content/ServiceConnection;

    const/4 v4, 0x1

    invoke-virtual {v1, v2, v3, v0, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "Coffee - PreferenceServiceClient"

    const-string v1, "Failed to start PreferenceService. Preferences won\'t work"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 102
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onDestroy()V

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/am;->g:Ljava/lang/Object;

    monitor-enter v1

    const/4 v2, 0x0

    :try_start_0
    iput-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->h:Landroid/content/ServiceConnection;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    .line 104
    iput-object v3, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    .line 105
    return-void

    .line 103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 87
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onPause()V

    .line 88
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->b:Z

    .line 89
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 64
    invoke-super {p0}, Landroid/preference/PreferenceFragment;->onResume()V

    .line 65
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->c:Z

    if-nez v0, :cond_1

    .line 67
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/a;->c:Z

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/am;->a()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 70
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;->d()V

    .line 83
    :cond_0
    :goto_0
    return-void

    .line 75
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/am;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/am;->b()V

    goto :goto_0
.end method
