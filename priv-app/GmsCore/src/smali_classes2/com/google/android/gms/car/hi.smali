.class final Lcom/google/android/gms/car/hi;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/gx;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 754
    iput-object p1, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->i(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 758
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->d(Lcom/google/android/gms/car/gx;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 759
    const-string v0, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 760
    const-string v0, "CAR.SERVICE"

    const-string v2, "car disconnected while service discovery"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 762
    :cond_0
    monitor-exit v1

    .line 833
    :cond_1
    :goto_0
    return-void

    .line 764
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->j(Lcom/google/android/gms/car/gx;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 766
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 821
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 768
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->k(Lcom/google/android/gms/car/gx;)Z

    .line 769
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v0

    if-eqz v0, :cond_4

    .line 772
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->e()V

    .line 777
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->l(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/qd;->a()V

    .line 780
    new-instance v2, Lcom/google/android/gms/car/ae;

    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    iget-object v4, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v4}, Lcom/google/android/gms/car/gx;->m(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/pc;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/gms/car/pc;->h:Lcom/google/android/gms/car/senderprotocol/be;

    iget-object v5, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v5}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v5

    invoke-direct {v2, v0, v3, v4, v5}, Lcom/google/android/gms/car/ae;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/senderprotocol/be;Lcom/google/android/gms/car/ip;)V

    .line 784
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->n(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 785
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/ae;

    .line 786
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 787
    :try_start_3
    invoke-virtual {v2}, Lcom/google/android/gms/car/ae;->f()Lcom/google/android/gms/car/on;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/diagnostics/FeedbackSenderService;->a(Lcom/google/android/gms/car/on;)V

    .line 790
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    iget-object v3, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->c(Lcom/google/android/gms/car/gx;)I

    move-result v3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/gx;->b(Lcom/google/android/gms/car/gx;I)V

    .line 791
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarService;->b()V

    .line 794
    invoke-virtual {v2}, Lcom/google/android/gms/car/ae;->b()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->c(Lcom/google/android/gms/car/gx;)I

    move-result v0

    if-nez v0, :cond_5

    .line 796
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 797
    sget-object v3, Lcom/google/android/gms/car/ae;->c:Landroid/content/ComponentName;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 798
    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/ae;->c(Landroid/content/Intent;)Z

    .line 821
    :goto_2
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 823
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;J)J

    .line 824
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->q(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/at;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v1}, Lcom/google/android/gms/car/gx;->p(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/at;->a(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/at;

    .line 827
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v0

    iget-boolean v0, v0, Lcom/google/android/gms/car/qc;->e:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->b(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->h()Z

    move-result v0

    if-nez v0, :cond_1

    .line 831
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->d()V

    goto/16 :goto_0

    .line 774
    :cond_4
    :try_start_4
    const-string v0, "CAR.SERVICE"

    const-string v2, "Display service not created. Car not supporting video?"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 786
    :catchall_1
    move-exception v0

    monitor-exit v3

    throw v0

    .line 800
    :cond_5
    sget-object v0, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 802
    const-string v3, "first_connection_with_this_car"

    iget-object v4, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v4}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v4

    iget-boolean v4, v4, Lcom/google/android/gms/car/qc;->f:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 804
    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/ae;->c(Landroid/content/Intent;)Z

    goto :goto_2

    .line 807
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    .line 809
    if-nez v0, :cond_8

    .line 810
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->c(Lcom/google/android/gms/car/gx;)I

    move-result v0

    if-eqz v0, :cond_7

    .line 811
    iget-object v0, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    const/16 v2, 0xa

    const-string v3, "No launcher projection client installed."

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 814
    :cond_7
    monitor-exit v1

    goto/16 :goto_0

    .line 816
    :cond_8
    const-string v3, "first_connection_with_this_car"

    iget-object v4, p0, Lcom/google/android/gms/car/hi;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v4}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v4

    iget-boolean v4, v4, Lcom/google/android/gms/car/qc;->f:Z

    invoke-virtual {v0, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 818
    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/ae;->d(Landroid/content/Intent;)Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_2
.end method
