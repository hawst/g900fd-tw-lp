.class public Lcom/google/android/gms/common/download/provider/DownloadsProvider;
.super Lcom/android/a/a/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/Map;

.field private static final c:Landroid/content/UriMatcher;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 27
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    sput-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b:Ljava/util/Map;

    .line 28
    new-instance v1, Landroid/content/UriMatcher;

    const/4 v2, -0x1

    invoke-direct {v1, v2}, Landroid/content/UriMatcher;-><init>(I)V

    .line 31
    sput-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.download"

    const-string v3, "download"

    const/16 v4, 0x64

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 32
    sget-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    const-string v2, "com.google.android.gms.download"

    const-string v3, "download/*"

    const/16 v4, 0x65

    invoke-virtual {v1, v2, v3, v4}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 33
    const/16 v1, 0xd

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "_id"

    aput-object v2, v1, v0

    const/4 v2, 0x1

    const-string v3, "filename"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "url"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "sizeBytes"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "sha1"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "destination"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "minVersion"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "maxVersion"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "notificationTitle"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "notificationDescription"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "enabled"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "retries"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "dm_id"

    aput-object v3, v1, v2

    .line 53
    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 54
    sget-object v4, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b:Ljava/util/Map;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "downloads."

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4, v3, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 56
    :cond_0
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/android/a/a/c;-><init>()V

    return-void
.end method

.method private b()Landroid/database/sqlite/SQLiteDatabase;
    .locals 1

    .prologue
    .line 68
    sget-boolean v0, Lcom/google/android/gms/common/download/DownloadService;->b:Z

    if-eqz v0, :cond_0

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/download/provider/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/download/provider/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/download/provider/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 71
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->a:Landroid/database/sqlite/SQLiteDatabase;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 95
    sget-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 96
    packed-switch v1, :pswitch_data_0

    .line 107
    :goto_0
    return v0

    .line 99
    :pswitch_0
    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    .line 100
    const-string p3, "filename=?"

    .line 101
    const/4 v1, 0x1

    new-array p4, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p4, v0

    .line 103
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "downloads"

    invoke-virtual {v0, v1, p2, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 96
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 112
    sget-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 113
    packed-switch v1, :pswitch_data_0

    .line 124
    :goto_0
    return v0

    .line 116
    :pswitch_0
    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    .line 117
    const-string p2, "filename=?"

    .line 118
    const/4 v1, 0x1

    new-array p3, v1, [Ljava/lang/String;

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    aput-object v1, p3, v0

    .line 120
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "downloads"

    invoke-virtual {v0, v1, p2, p3}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 113
    nop

    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected final a(Landroid/content/Context;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    .prologue
    .line 60
    invoke-static {p1}, Lcom/google/android/gms/common/download/provider/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/download/provider/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 76
    sget-object v1, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v1, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v1

    .line 77
    packed-switch v1, :pswitch_data_0

    .line 81
    :cond_0
    :goto_0
    return-object v0

    .line 79
    :pswitch_0
    const-string v1, "downloads"

    invoke-direct {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    invoke-virtual {v2, v1, v0, p2}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    const-string v0, "filename"

    invoke-virtual {p2, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 77
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
    .end packed-switch
.end method

.method protected final a()V
    .locals 4

    .prologue
    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/common/download/provider/c;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 130
    return-void
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 165
    const/4 v0, 0x0

    return-object v0
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 136
    sget-object v0, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->c:Landroid/content/UriMatcher;

    invoke-virtual {v0, p1}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v2

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/download/provider/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/download/provider/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/download/provider/a;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 139
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 144
    const/16 v3, 0xe

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 145
    invoke-virtual {v0, v4}, Landroid/database/sqlite/SQLiteQueryBuilder;->setStrict(Z)V

    .line 148
    :cond_0
    packed-switch v2, :pswitch_data_0

    .line 160
    :goto_0
    return-object v5

    .line 151
    :pswitch_0
    const/16 v3, 0x65

    if-ne v2, v3, :cond_1

    .line 152
    const-string v3, "filename=?"

    .line 153
    new-array v4, v4, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v2

    .line 155
    :goto_1
    const-string v2, "downloads"

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 156
    sget-object v2, Lcom/google/android/gms/common/download/provider/DownloadsProvider;->b:Ljava/util/Map;

    invoke-virtual {v0, v2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    move-object v2, p2

    move-object v6, v5

    move-object v7, p5

    .line 157
    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v5

    goto :goto_0

    :cond_1
    move-object v4, p4

    move-object v3, p3

    goto :goto_1

    .line 148
    :pswitch_data_0
    .packed-switch 0x64
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
