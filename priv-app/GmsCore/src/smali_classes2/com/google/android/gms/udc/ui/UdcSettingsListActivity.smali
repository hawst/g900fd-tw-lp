.class public Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/udc/ui/aa;
.implements Lcom/google/android/gms/udc/ui/h;
.implements Lcom/google/android/gms/udc/ui/i;


# instance fields
.field private final a:Lcom/google/android/gms/udc/util/j;

.field private final b:Lcom/google/android/gms/udc/ui/y;

.field private c:Lcom/google/android/gms/common/account/g;

.field private d:Lcom/google/android/gms/analytics/bv;

.field private e:Ljava/lang/String;

.field private f:Lcom/google/android/gms/common/api/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 60
    new-instance v0, Lcom/google/android/gms/udc/util/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/util/j;-><init>(Landroid/support/v4/app/q;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    .line 61
    new-instance v0, Lcom/google/android/gms/udc/ui/y;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/ui/y;-><init>(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b:Lcom/google/android/gms/udc/ui/y;

    .line 241
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;
    .locals 3

    .prologue
    .line 185
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/udc/c;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object p1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/udc/util/j;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Lcom/google/android/gms/analytics/bv;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d:Lcom/google/android/gms/analytics/bv;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b:Lcom/google/android/gms/udc/ui/y;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 195
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 233
    const/4 v0, -0x1

    if-ne p1, v0, :cond_0

    .line 234
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 235
    const-string v1, "com.google.android.apps.maps"

    const-string v2, "com.google.android.maps.MapsActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->startActivity(Landroid/content/Intent;)V

    .line 239
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/m;)V
    .locals 4

    .prologue
    .line 199
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d:Lcom/google/android/gms/analytics/bv;

    const-string v1, "SettingDetail"

    iget-object v2, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v2, v2, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v3, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/udc/e/m;)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 206
    return-void
.end method

.method public final e()V
    .locals 4

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d:Lcom/google/android/gms/analytics/bv;

    const-string v1, "LocationHistoryAlias"

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 217
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/location/reporting/a/e;->b(Landroid/content/pm/PackageManager;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 218
    new-instance v0, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v0}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    sget v1, Lcom/google/android/gms/p;->yV:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->ze:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->a(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->yW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->c(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->dD:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->d(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/g;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/udc/ui/j;

    .line 229
    :goto_0
    return-void

    .line 225
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    const-string v2, "com.google"

    invoke-direct {v0, v1, v2}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/location/a/b;->a(Landroid/accounts/Account;)Landroid/content/Intent;

    move-result-object v0

    .line 227
    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 149
    invoke-super {p0, p1, p2, p3}, Landroid/support/v7/app/d;->onActivityResult(IILandroid/content/Intent;)V

    .line 151
    if-ne p1, v2, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b:Lcom/google/android/gms/udc/ui/y;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 155
    :cond_0
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 70
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 71
    sget v0, Lcom/google/android/gms/l;->fT:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->setContentView(I)V

    .line 73
    const-string v0, "AccountHistory"

    invoke-static {p0, v0}, Lcom/google/android/gms/udc/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d:Lcom/google/android/gms/analytics/bv;

    .line 75
    if-eqz p1, :cond_0

    .line 76
    const-string v0, "UdcAccountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    .line 79
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b:Lcom/google/android/gms/udc/ui/y;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/udc/util/j;->a(ILandroid/support/v4/app/av;)V

    .line 84
    :cond_1
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    .line 85
    invoke-virtual {v0, v2}, Landroid/support/v7/app/a;->a(Z)V

    .line 87
    new-instance v1, Lcom/google/android/gms/common/account/h;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/account/h;-><init>(Landroid/support/v7/app/a;)V

    sget v0, Lcom/google/android/gms/p;->ze:I

    iput v0, v1, Lcom/google/android/gms/common/account/h;->a:I

    iput-object p0, v1, Lcom/google/android/gms/common/account/h;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/common/account/h;->c:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/common/account/h;->a()Lcom/google/android/gms/common/account/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->c:Lcom/google/android/gms/common/account/g;

    .line 92
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/util/j;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 117
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 118
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->d:Lcom/google/android/gms/analytics/bv;

    const-string v1, "AccountPicker"

    new-instance v2, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v2}, Lcom/google/android/gms/analytics/bf;-><init>()V

    const-string v3, "UX"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v2

    const-string v3, "Select"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    .line 140
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->c:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/account/g;->a(I)Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    :goto_0
    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Landroid/support/v4/app/q;)V

    iput-object v4, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    :cond_0
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    iput-object v4, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    .line 141
    :cond_1
    :goto_1
    return-void

    .line 140
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0

    :cond_3
    iput-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a(Ljava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->f:Lcom/google/android/gms/common/api/v;

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->b:Lcom/google/android/gms/udc/ui/y;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    goto :goto_1
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 145
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 128
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 133
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 130
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->finish()V

    .line 131
    const/4 v0, 0x1

    goto :goto_0

    .line 128
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method protected onResume()V
    .locals 1

    .prologue
    .line 102
    invoke-super {p0}, Landroid/support/v7/app/d;->onResume()V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->c:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0}, Lcom/google/android/gms/common/account/g;->a()V

    .line 106
    return-void
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 122
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 123
    const-string v0, "UdcAccountName"

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingsListActivity;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 96
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 97
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/app/Activity;)V

    .line 98
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 110
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 111
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->c(Landroid/app/Activity;)V

    .line 112
    return-void
.end method
