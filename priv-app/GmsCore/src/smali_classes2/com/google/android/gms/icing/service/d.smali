.class final Lcom/google/android/gms/icing/service/d;
.super Lcom/google/android/gms/icing/b/h;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/Intent;

.field final synthetic b:Lcom/google/android/gms/icing/service/IndexWorkerService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/service/IndexWorkerService;Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 131
    iput-object p1, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    iput-object p2, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/b/h;-><init>(I)V

    return-void
.end method

.method private c()Ljava/lang/Void;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    const-wide/16 v2, -0x1

    const/4 v4, 0x0

    .line 134
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    if-nez v0, :cond_1

    .line 227
    :cond_0
    :goto_0
    return-object v4

    .line 138
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    const-string v1, "com.google.android.gms.icing.INDEX_RECURRING_MAINTENANCE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "com.google.android.gms.icing.INDEX_ONETIME_MAINTENANCE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 142
    :cond_2
    new-instance v0, Lcom/google/android/gms/icing/service/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/service/e;-><init>(Landroid/app/Service;)V

    .line 144
    iget-boolean v1, v0, Lcom/google/android/gms/icing/service/e;->a:Z

    if-eqz v1, :cond_4

    .line 148
    iget-boolean v0, v0, Lcom/google/android/gms/icing/service/e;->b:Z

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b()V

    goto :goto_0

    .line 151
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Z)V

    goto :goto_0

    .line 154
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    iget-object v0, v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->d()V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->o()Lcom/google/android/gms/icing/b/j;

    goto :goto_0

    .line 157
    :cond_5
    const-string v1, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    goto :goto_0

    .line 163
    :cond_6
    const-string v1, "android.intent.action.ACTION_POWER_CONNECTED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Z)V

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b()V

    goto :goto_0

    .line 168
    :cond_7
    const-string v1, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "android.intent.action.PACKAGE_DATA_CLEARED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "android.intent.action.PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_8

    const-string v1, "android.intent.action.PACKAGE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 173
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 174
    :cond_9
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "android.intent.action.MY_PACKAGE_REPLACED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 176
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v6}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b(Z)V

    goto/16 :goto_0

    .line 177
    :cond_b
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->b(Z)V

    goto/16 :goto_0

    .line 179
    :cond_c
    const-string v1, "android.intent.action.LOCALE_CHANGED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->x()V

    goto/16 :goto_0

    .line 181
    :cond_d
    const-string v1, "android.intent.action.DEVICE_STORAGE_LOW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_e

    const-string v1, "android.intent.action.DEVICE_STORAGE_OK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 183
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/u;->b(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 184
    :cond_f
    const-string v1, "com.google.android.gms.icing.INDEX_CONTENT_PROVIDER"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    .line 187
    if-eqz v0, :cond_17

    .line 188
    const-string v1, "delay"

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    .line 190
    :goto_1
    cmp-long v2, v0, v2

    if-nez v2, :cond_10

    .line 191
    const-string v0, "Received invalid intent: %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 193
    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(J)V

    goto/16 :goto_0

    .line 195
    :cond_11
    const-string v1, "com.google.android.c2dm.intent.RECEIVE"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    invoke-static {v0}, Lcom/google/android/gms/icing/GcmReceiverService;->a(Landroid/content/Intent;)[B

    move-result-object v0

    .line 197
    if-eqz v0, :cond_0

    .line 202
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/icing/c/a/r;->a([B)Lcom/google/android/gms/icing/c/a/r;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 207
    iget-object v1, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/c/a/r;)V

    goto/16 :goto_0

    .line 203
    :catch_0
    move-exception v0

    .line 204
    const-string v1, "Gcm message parse failed: %s"

    invoke-virtual {v0}, Lcom/google/protobuf/nano/i;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 208
    :cond_12
    const-string v1, "com.google.android.gms.icing.GCM_TASK"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_16

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-nez v0, :cond_13

    .line 210
    const-string v0, "Couldn\'t run gcm task due to initialization failure"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto/16 :goto_0

    .line 212
    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->a:Landroid/content/Intent;

    const-string v1, "tag"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 213
    const-string v1, "update-app-params"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_14

    .line 214
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    iget-object v0, v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->b()Lcom/google/android/gms/icing/b/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/search/queries/t;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/search/queries/t;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1, v8, v9}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto/16 :goto_0

    .line 216
    :cond_14
    const-string v1, "app-history-upload"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_15

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    iget-object v0, v0, Lcom/google/android/gms/icing/service/IndexWorkerService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->b()Lcom/google/android/gms/icing/b/a;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/icing/impl/b;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/d;->b:Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a()Lcom/google/android/gms/icing/impl/u;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/icing/impl/b;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v0, v1, v8, v9}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto/16 :goto_0

    .line 220
    :cond_15
    const-string v1, "Received unexpected gcm tag: %s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    .line 224
    :cond_16
    const-string v1, "Received unexpected intent: %s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_0

    :cond_17
    move-wide v0, v2

    goto/16 :goto_1
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/icing/service/d;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
