.class public final Lcom/google/android/gms/icing/bf;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:J

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:J

.field public g:I

.field public h:Lcom/google/android/gms/icing/o;

.field public i:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v1, -0x1

    .line 10405
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10406
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/bf;->g:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    iput v1, p0, Lcom/google/android/gms/icing/bf;->i:I

    iput v1, p0, Lcom/google/android/gms/icing/bf;->cachedSize:I

    .line 10407
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10541
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10542
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 10543
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10546
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 10547
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10550
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 10551
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10554
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 10555
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10558
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 10559
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10562
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 10563
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10566
    :cond_5
    iget v1, p0, Lcom/google/android/gms/icing/bf;->g:I

    if-eqz v1, :cond_6

    .line 10567
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/icing/bf;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10570
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-eqz v1, :cond_7

    .line 10571
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10574
    :cond_7
    iget v1, p0, Lcom/google/android/gms/icing/bf;->i:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_8

    .line 10575
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/icing/bf;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10578
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10425
    if-ne p1, p0, :cond_1

    .line 10481
    :cond_0
    :goto_0
    return v0

    .line 10428
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bf;

    if-nez v2, :cond_2

    move v0, v1

    .line 10429
    goto :goto_0

    .line 10431
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bf;

    .line 10432
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 10433
    iget-object v2, p1, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 10434
    goto :goto_0

    .line 10436
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 10437
    goto :goto_0

    .line 10439
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bf;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_5

    move v0, v1

    .line 10440
    goto :goto_0

    .line 10442
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 10443
    iget-object v2, p1, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 10444
    goto :goto_0

    .line 10446
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 10447
    goto :goto_0

    .line 10449
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    if-nez v2, :cond_8

    .line 10450
    iget-object v2, p1, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    if-eqz v2, :cond_9

    move v0, v1

    .line 10451
    goto :goto_0

    .line 10453
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 10454
    goto :goto_0

    .line 10456
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    if-nez v2, :cond_a

    .line 10457
    iget-object v2, p1, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    if-eqz v2, :cond_b

    move v0, v1

    .line 10458
    goto :goto_0

    .line 10460
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 10461
    goto :goto_0

    .line 10463
    :cond_b
    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bf;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 10464
    goto :goto_0

    .line 10466
    :cond_c
    iget v2, p0, Lcom/google/android/gms/icing/bf;->g:I

    iget v3, p1, Lcom/google/android/gms/icing/bf;->g:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 10467
    goto :goto_0

    .line 10469
    :cond_d
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-nez v2, :cond_e

    .line 10470
    iget-object v2, p1, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-eqz v2, :cond_f

    move v0, v1

    .line 10471
    goto/16 :goto_0

    .line 10474
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    iget-object v3, p1, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/o;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_f

    move v0, v1

    .line 10475
    goto/16 :goto_0

    .line 10478
    :cond_f
    iget v2, p0, Lcom/google/android/gms/icing/bf;->i:I

    iget v3, p1, Lcom/google/android/gms/icing/bf;->i:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 10479
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    const/4 v1, 0x0

    .line 10486
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 10489
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bf;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 10491
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 10493
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 10495
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v2

    .line 10497
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bf;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 10499
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/bf;->g:I

    add-int/2addr v0, v2

    .line 10500
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-nez v2, :cond_4

    :goto_4
    add-int/2addr v0, v1

    .line 10502
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bf;->i:I

    add-int/2addr v0, v1

    .line 10503
    return v0

    .line 10486
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 10491
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 10493
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 10495
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    .line 10500
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/o;->hashCode()I

    move-result v1

    goto :goto_4
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 10361
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/bf;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/bf;->f:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/bf;->g:I

    goto :goto_0

    :sswitch_8
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/o;

    invoke-direct {v0}, Lcom/google/android/gms/icing/o;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bf;->i:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10509
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 10510
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10512
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/bf;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 10513
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 10515
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 10516
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10518
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 10519
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10521
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 10522
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10524
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/icing/bf;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 10525
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/bf;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 10527
    :cond_5
    iget v0, p0, Lcom/google/android/gms/icing/bf;->g:I

    if-eqz v0, :cond_6

    .line 10528
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/icing/bf;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10530
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-eqz v0, :cond_7

    .line 10531
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 10533
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/bf;->i:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_8

    .line 10534
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/icing/bf;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10536
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10537
    return-void
.end method
