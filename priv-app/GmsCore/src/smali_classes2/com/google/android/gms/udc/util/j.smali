.class public final Lcom/google/android/gms/udc/util/j;
.super Landroid/os/Handler;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/app/q;


# direct methods
.method public constructor <init>(Landroid/support/v4/app/q;)V
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 43
    const/4 v0, 0x6

    const v1, 0x1020002

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/j;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 44
    return-void
.end method

.method public final a(ILandroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 39
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/google/android/gms/udc/util/j;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 40
    return-void
.end method

.method public final a(ILandroid/support/v4/app/av;)V
    .locals 2

    .prologue
    .line 55
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/google/android/gms/udc/util/j;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 56
    return-void
.end method

.method public final a(Landroid/support/v4/app/m;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, 0x5

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/udc/util/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 48
    return-void
.end method

.method public final b(ILandroid/support/v4/app/av;)V
    .locals 2

    .prologue
    .line 59
    const/4 v0, 0x4

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1, p2}, Lcom/google/android/gms/udc/util/j;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 60
    return-void
.end method

.method public final b(Landroid/support/v4/app/m;)V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/udc/util/j;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/util/j;->sendMessage(Landroid/os/Message;)Z

    .line 52
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 64
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 95
    :cond_0
    :goto_0
    return-void

    .line 66
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/Fragment;

    invoke-virtual {v1, v2, v0}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    goto :goto_0

    .line 71
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    iget v1, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    .line 73
    if-eqz v0, :cond_0

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/aj;->b()I

    goto :goto_0

    .line 80
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/m;

    iget-object v1, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "UdcDialog"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    goto :goto_0

    .line 84
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/m;

    invoke-virtual {v0}, Landroid/support/v4/app/m;->dismiss()V

    goto :goto_0

    .line 87
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/av;

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0

    .line 91
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/udc/util/j;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    iget v2, p1, Landroid/os/Message;->arg1:I

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Landroid/support/v4/app/av;

    invoke-virtual {v1, v2, v3, v0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
