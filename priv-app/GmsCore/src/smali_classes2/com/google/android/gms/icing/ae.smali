.class public final Lcom/google/android/gms/icing/ae;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/ae;


# instance fields
.field public a:I

.field public b:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 3425
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3426
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/ae;->a:I

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/ae;->b:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ae;->cachedSize:I

    .line 3427
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/ae;
    .locals 2

    .prologue
    .line 3408
    sget-object v0, Lcom/google/android/gms/icing/ae;->c:[Lcom/google/android/gms/icing/ae;

    if-nez v0, :cond_1

    .line 3409
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3411
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/ae;->c:[Lcom/google/android/gms/icing/ae;

    if-nez v0, :cond_0

    .line 3412
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/ae;

    sput-object v0, Lcom/google/android/gms/icing/ae;->c:[Lcom/google/android/gms/icing/ae;

    .line 3414
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3416
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/ae;->c:[Lcom/google/android/gms/icing/ae;

    return-object v0

    .line 3414
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 3477
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3478
    iget v1, p0, Lcom/google/android/gms/icing/ae;->a:I

    if-eqz v1, :cond_0

    .line 3479
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/ae;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3482
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/ae;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 3483
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/ae;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 3486
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3438
    if-ne p1, p0, :cond_1

    .line 3451
    :cond_0
    :goto_0
    return v0

    .line 3441
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ae;

    if-nez v2, :cond_2

    move v0, v1

    .line 3442
    goto :goto_0

    .line 3444
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ae;

    .line 3445
    iget v2, p0, Lcom/google/android/gms/icing/ae;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/ae;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3446
    goto :goto_0

    .line 3448
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/ae;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/ae;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 3449
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 3456
    iget v0, p0, Lcom/google/android/gms/icing/ae;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3458
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/ae;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/ae;->b:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 3460
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 3402
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ae;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/ae;->b:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 3466
    iget v0, p0, Lcom/google/android/gms/icing/ae;->a:I

    if-eqz v0, :cond_0

    .line 3467
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/ae;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 3469
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/ae;->b:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 3470
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/ae;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 3472
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3473
    return-void
.end method
