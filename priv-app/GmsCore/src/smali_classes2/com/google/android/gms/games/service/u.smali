.class final Lcom/google/android/gms/games/service/u;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/games/internal/dx;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private e:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 160
    iput-object p1, p0, Lcom/google/android/gms/games/service/u;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 161
    iput-object p2, p0, Lcom/google/android/gms/games/service/u;->b:Lcom/google/android/gms/games/internal/dx;

    .line 162
    iput-object p3, p0, Lcom/google/android/gms/games/service/u;->c:Ljava/lang/String;

    .line 163
    iput-object p4, p0, Lcom/google/android/gms/games/service/u;->d:Ljava/lang/String;

    .line 164
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 188
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->b:Lcom/google/android/gms/games/internal/dx;

    iget-object v1, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/dx;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 192
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 192
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v1, :cond_1

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_1
    throw v0
.end method

.method protected final a(Lcom/google/android/gms/games/h/c/a;)V
    .locals 1

    .prologue
    .line 182
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    .line 183
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 2

    .prologue
    .line 168
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/service/u;->c:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/games/service/u;->d:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->h(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    .line 177
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/u;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
