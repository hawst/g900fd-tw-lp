.class public final Lcom/google/android/gms/appdatasearch/aq;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/icing/ak;Lcom/google/android/gms/icing/ap;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 11

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 124
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v0, :cond_0

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    move-object v0, v4

    .line 142
    :goto_0
    return-object v0

    .line 128
    :cond_1
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 129
    new-instance v6, Ljava/util/HashSet;

    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-direct {v6, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    move v0, v1

    .line 130
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 131
    iget-object v2, p0, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    aget-object v7, v2, v0

    .line 132
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 133
    iget-object v2, p1, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    aget-object v2, v2, v1

    iget-object v2, v2, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    aget-object v2, v2, v0

    iget-object v8, v2, Lcom/google/android/gms/icing/as;->a:[B

    move v2, v1

    .line 134
    :goto_2
    array-length v3, p3

    if-ge v2, v3, :cond_4

    .line 135
    aget-byte v3, v8, v2

    if-eqz v3, :cond_3

    .line 136
    aget-object v9, p3, v2

    invoke-virtual {v5, v7}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v5, v7, v3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    :cond_2
    const/4 v10, 0x1

    invoke-virtual {v3, v9, v10}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 134
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 130
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 142
    :cond_5
    invoke-virtual {v5}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_6

    move-object v0, v4

    goto :goto_0

    :cond_6
    move-object v0, v5

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/icing/ak;Lcom/google/android/gms/icing/ap;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;
    .locals 14

    .prologue
    .line 84
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v1, :cond_0

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 85
    :cond_0
    const/4 v1, 0x0

    .line 119
    :goto_0
    return-object v1

    .line 87
    :cond_1
    new-instance v5, Ljava/util/HashSet;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v5, v1}, Ljava/util/HashSet;-><init>(I)V

    .line 88
    new-instance v4, Landroid/os/Bundle;

    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-direct {v4, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 89
    move-object/from16 v0, p3

    iget-object v1, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/appdatasearch/Section;

    .line 90
    iget-object v1, v1, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 92
    :cond_2
    const/4 v1, 0x0

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    array-length v2, v2

    if-ge v1, v2, :cond_6

    .line 93
    iget-object v2, p0, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    aget-object v2, v2, v1

    .line 94
    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    const/4 v6, 0x0

    aget-object v3, v3, v6

    iget-object v3, v3, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v3, v3, v1

    .line 97
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget v2, v2, Lcom/google/android/gms/icing/an;->a:I

    aget-object v2, v6, v2

    iget-object v6, v2, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    .line 98
    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 99
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 100
    iget-object v8, v3, Lcom/google/android/gms/icing/ar;->a:[I

    .line 101
    iget-object v9, v3, Lcom/google/android/gms/icing/ar;->b:[B

    .line 102
    const/4 v3, 0x0

    .line 103
    const/4 v2, 0x0

    :goto_3
    move-object/from16 v0, p4

    array-length v10, v0

    if-ge v2, v10, :cond_4

    .line 104
    aget v10, v8, v2

    if-lez v10, :cond_3

    .line 106
    :try_start_0
    aget-object v10, p4, v2

    new-instance v11, Ljava/lang/String;

    aget v12, v8, v2

    const-string v13, "UTF-8"

    invoke-direct {v11, v9, v3, v12, v13}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    invoke-virtual {v7, v10, v11}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 112
    aget v10, v8, v2

    add-int/2addr v3, v10

    .line 103
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 110
    :catch_0
    move-exception v1

    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Encoding utf8 not available"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 115
    :cond_4
    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 92
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    :cond_6
    move-object v1, v4

    .line 119
    goto/16 :goto_0
.end method

.method public static a([Ljava/lang/String;)Landroid/os/Bundle;
    .locals 5

    .prologue
    .line 75
    new-instance v1, Landroid/os/Bundle;

    array-length v0, p0

    invoke-direct {v1, v0}, Landroid/os/Bundle;-><init>(I)V

    .line 76
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, p0, v0

    .line 77
    const/4 v4, 0x1

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 76
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 79
    :cond_0
    return-object v1
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/icing/c/a/x;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 15

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 317
    if-nez p0, :cond_0

    .line 318
    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    const-string v1, "internal error"

    invoke-direct {v0, v1}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(Ljava/lang/String;)V

    .line 365
    :goto_0
    return-object v0

    .line 322
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    .line 323
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 324
    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-virtual {v7, v0, v2, v3}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v0, v0

    new-array v14, v0, [Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    .line 327
    const-string v0, "Creating IME Update with %d contextual suggestions"

    iget-object v1, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v1, v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    move v13, v6

    .line 329
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    array-length v0, v0

    if-ge v13, v0, :cond_2

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/icing/c/a/x;->a:[Lcom/google/android/gms/icing/c/a/ac;

    aget-object v5, v0, v13

    .line 331
    iget-object v0, v5, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v2, v0, [B

    .line 332
    array-length v0, v2

    invoke-static {v2, v6, v0}, Lcom/google/protobuf/nano/b;->a([BII)Lcom/google/protobuf/nano/b;

    move-result-object v3

    .line 334
    iget-object v0, v5, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    new-array v8, v0, [B

    .line 336
    iget-object v0, v5, Lcom/google/android/gms/icing/c/a/ac;->a:Ljava/lang/String;

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v9

    move v0, v6

    move v1, v6

    .line 339
    :goto_2
    :try_start_0
    array-length v10, v9

    if-ge v0, v10, :cond_1

    .line 340
    aget-object v10, v9, v0

    invoke-virtual {v10}, Ljava/lang/String;->getBytes()[B

    move-result-object v10

    .line 341
    const/4 v11, 0x0

    array-length v12, v10

    invoke-static {v10, v11, v8, v1, v12}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 342
    array-length v11, v10

    add-int/2addr v1, v11

    .line 343
    array-length v10, v10

    invoke-virtual {v3, v10}, Lcom/google/protobuf/nano/b;->a(I)V

    .line 345
    const/4 v10, 0x0

    invoke-virtual {v3, v10}, Lcom/google/protobuf/nano/b;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 339
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 347
    :catch_0
    move-exception v0

    .line 348
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 350
    :cond_1
    invoke-static {v8, v1}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v1

    .line 351
    array-length v0, v2

    iget-object v3, v3, Lcom/google/protobuf/nano/b;->a:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    sub-int/2addr v0, v3

    invoke-static {v2, v0}, Ljava/util/Arrays;->copyOf([BI)[B

    move-result-object v2

    .line 353
    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    const/4 v3, 0x3

    iget-wide v8, v5, Lcom/google/android/gms/icing/c/a/ac;->c:D

    const-wide v10, 0x40c3880000000000L    # 10000.0

    mul-double/2addr v8, v10

    double-to-long v8, v8

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    move-object v5, v4

    move-object v12, v4

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/appdatasearch/PIMEUpdate;-><init>([B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V

    aput-object v0, v14, v13

    .line 329
    add-int/lit8 v0, v13, 0x1

    move v13, v0

    goto :goto_1

    .line 365
    :cond_2
    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    invoke-direct {v0, v4, v14}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>([B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    goto/16 :goto_0
.end method

.method public static a(Lcom/google/android/gms/icing/z;Landroid/util/SparseArray;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 17

    .prologue
    .line 286
    if-nez p0, :cond_0

    .line 287
    new-instance v2, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    const-string v3, "internal error"

    invoke-direct {v2, v3}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(Ljava/lang/String;)V

    .line 312
    :goto_0
    return-object v2

    .line 290
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v2, v2

    new-array v0, v2, [Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    move-object/from16 v16, v0

    .line 291
    const/4 v2, 0x0

    move v15, v2

    :goto_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v2, v2

    if-ge v15, v2, :cond_2

    .line 292
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    aget-object v12, v2, v15

    .line 293
    iget v2, v12, Lcom/google/android/gms/icing/v;->a:I

    .line 294
    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    move-object v14, v2

    check-cast v14, Lcom/google/android/gms/icing/impl/a/y;

    .line 295
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 296
    const/4 v2, 0x0

    :goto_2
    iget-object v3, v12, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    array-length v3, v3

    if-ge v2, v3, :cond_1

    .line 297
    iget-object v3, v12, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    aget-object v3, v3, v2

    iget-object v3, v3, Lcom/google/android/gms/icing/w;->a:Ljava/lang/String;

    iget-object v4, v12, Lcom/google/android/gms/icing/v;->g:[Lcom/google/android/gms/icing/w;

    aget-object v4, v4, v2

    iget-wide v4, v4, Lcom/google/android/gms/icing/w;->b:D

    invoke-virtual {v9, v3, v4, v5}, Landroid/os/Bundle;->putDouble(Ljava/lang/String;D)V

    .line 296
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 300
    :cond_1
    new-instance v2, Lcom/google/android/gms/appdatasearch/PIMEUpdate;

    iget-object v3, v12, Lcom/google/android/gms/icing/v;->b:[B

    iget-object v4, v12, Lcom/google/android/gms/icing/v;->c:[B

    iget v5, v14, Lcom/google/android/gms/icing/impl/a/y;->a:I

    iget-object v6, v14, Lcom/google/android/gms/icing/impl/a/y;->c:Ljava/lang/String;

    iget-object v7, v14, Lcom/google/android/gms/icing/impl/a/y;->b:Ljava/lang/String;

    iget-boolean v8, v12, Lcom/google/android/gms/icing/v;->d:Z

    iget-wide v10, v12, Lcom/google/android/gms/icing/v;->e:J

    iget-wide v12, v12, Lcom/google/android/gms/icing/v;->f:J

    iget-object v14, v14, Lcom/google/android/gms/icing/impl/a/y;->d:Landroid/accounts/Account;

    invoke-direct/range {v2 .. v14}, Lcom/google/android/gms/appdatasearch/PIMEUpdate;-><init>([B[BILjava/lang/String;Ljava/lang/String;ZLandroid/os/Bundle;JJLandroid/accounts/Account;)V

    aput-object v2, v16, v15

    .line 291
    add-int/lit8 v2, v15, 0x1

    move v15, v2

    goto :goto_1

    .line 312
    :cond_2
    new-instance v2, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-static {v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v3

    move-object/from16 v0, v16

    invoke-direct {v2, v3, v0}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>([B[Lcom/google/android/gms/appdatasearch/PIMEUpdate;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/icing/aj;Lcom/google/android/gms/icing/ap;Landroid/util/SparseArray;Z)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 15

    .prologue
    .line 194
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    new-array v7, v1, [Landroid/os/Bundle;

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    new-array v8, v1, [Landroid/os/Bundle;

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    new-array v9, v1, [Landroid/os/Bundle;

    .line 198
    new-instance v6, Landroid/util/SparseIntArray;

    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    invoke-direct {v6, v1}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    new-array v4, v1, [Ljava/lang/String;

    .line 202
    const/4 v1, 0x0

    move v2, v1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v1, v1

    if-ge v2, v1, :cond_5

    .line 204
    iget-object v1, p0, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    aget-object v10, v1, v2

    .line 205
    iget v1, v10, Lcom/google/android/gms/icing/ak;->a:I

    invoke-virtual {v6, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 206
    iget v1, v10, Lcom/google/android/gms/icing/ak;->a:I

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    .line 207
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "-"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, v1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v4, v2

    .line 208
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    aput-object v3, v8, v2

    .line 209
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    aput-object v3, v9, v2

    .line 211
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    aget-object v11, v3, v2

    .line 214
    const/4 v3, 0x0

    :goto_1
    iget-object v5, v10, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    array-length v5, v5

    if-ge v3, v5, :cond_1

    .line 215
    iget-object v5, v10, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    aget-object v5, v5, v3

    .line 216
    iget-object v12, v11, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v12, v12, v3

    .line 218
    if-eqz p3, :cond_0

    iget-object v13, v1, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    iget-object v14, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget v5, v5, Lcom/google/android/gms/icing/an;->a:I

    aget-object v5, v14, v5

    iget-object v5, v5, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-static {v13, v5}, Lcom/google/android/gms/appdatasearch/Section;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 223
    :goto_2
    aget-object v13, v8, v2

    iget-object v14, v12, Lcom/google/android/gms/icing/ar;->a:[I

    invoke-virtual {v13, v5, v14}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 224
    aget-object v13, v9, v2

    iget-object v12, v12, Lcom/google/android/gms/icing/ar;->b:[B

    invoke-virtual {v13, v5, v12}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 214
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 218
    :cond_0
    iget-object v13, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget v5, v5, Lcom/google/android/gms/icing/an;->a:I

    aget-object v5, v13, v5

    iget-object v5, v5, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    goto :goto_2

    .line 228
    :cond_1
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    aput-object v1, v7, v2

    .line 229
    const/4 v1, 0x0

    :goto_3
    iget-object v3, v10, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    array-length v3, v3

    if-ge v1, v3, :cond_4

    .line 230
    iget-object v3, v10, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    aget-object v12, v3, v1

    .line 231
    iget-object v3, v11, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    aget-object v13, v3, v1

    .line 232
    iget-object v3, v13, Lcom/google/android/gms/icing/as;->a:[B

    array-length v3, v3

    new-array v14, v3, [Z

    .line 233
    const/4 v3, 0x0

    :goto_4
    array-length v5, v14

    if-ge v3, v5, :cond_3

    .line 234
    iget-object v5, v13, Lcom/google/android/gms/icing/as;->a:[B

    aget-byte v5, v5, v3

    if-eqz v5, :cond_2

    const/4 v5, 0x1

    :goto_5
    aput-boolean v5, v14, v3

    .line 233
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 234
    :cond_2
    const/4 v5, 0x0

    goto :goto_5

    .line 236
    :cond_3
    aget-object v3, v7, v2

    invoke-virtual {v3, v12, v14}, Landroid/os/Bundle;->putBooleanArray(Ljava/lang/String;[Z)V

    .line 229
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 202
    :cond_4
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto/16 :goto_0

    .line 241
    :cond_5
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/icing/ap;->c:[I

    .line 242
    const/4 v1, 0x0

    :goto_6
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/gms/icing/ap;->b:I

    if-ge v1, v2, :cond_6

    .line 243
    aget v2, v3, v1

    invoke-virtual {v6, v2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    aput v2, v3, v1

    .line 242
    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    .line 246
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->d:Z

    if-eqz v1, :cond_7

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/icing/ap;->f:[I

    .line 247
    :goto_7
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aj;->d:Z

    if-eqz v1, :cond_8

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/icing/ap;->g:[B

    .line 248
    :goto_8
    new-instance v1, Lcom/google/android/gms/appdatasearch/SearchResults;

    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/gms/icing/ap;->b:I

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-static {v10}, Lcom/google/android/gms/icing/impl/e/d;->a(Lcom/google/protobuf/nano/j;)[B

    move-result-object v10

    const/4 v11, 0x0

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;[B[D)V

    return-object v1

    .line 246
    :cond_7
    const/4 v5, 0x0

    goto :goto_7

    .line 247
    :cond_8
    const/4 v6, 0x0

    goto :goto_8
.end method

.method public static a([Ljava/lang/String;Lcom/google/android/gms/icing/ap;)[Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    .line 155
    new-instance v3, Ljava/util/ArrayList;

    array-length v1, p0

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 160
    array-length v4, p0

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v4, :cond_1

    aget-object v5, p0, v2

    .line 161
    iget v6, p1, Lcom/google/android/gms/icing/ap;->b:I

    if-ge v0, v6, :cond_1

    .line 162
    :try_start_0
    new-instance v6, Ljava/lang/String;

    iget-object v7, p1, Lcom/google/android/gms/icing/ap;->g:[B

    iget-object v8, p1, Lcom/google/android/gms/icing/ap;->f:[I

    aget v8, v8, v0

    const-string v9, "UTF-8"

    invoke-direct {v6, v7, v1, v8, v9}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 169
    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 170
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 171
    iget-object v5, p1, Lcom/google/android/gms/icing/ap;->f:[I

    aget v5, v5, v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/2addr v1, v5

    .line 172
    add-int/lit8 v0, v0, 0x1

    .line 160
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 175
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 179
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 1

    .prologue
    .line 187
    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static c(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;
    .locals 1

    .prologue
    .line 281
    new-instance v0, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/PIMEUpdateResponse;-><init>(Ljava/lang/String;)V

    return-object v0
.end method

.method public static d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 1

    .prologue
    .line 376
    new-instance v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    invoke-direct {v0, p0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>(Ljava/lang/String;)V

    return-object v0
.end method
