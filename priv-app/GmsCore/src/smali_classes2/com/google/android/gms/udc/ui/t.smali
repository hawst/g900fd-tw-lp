.class public final Lcom/google/android/gms/udc/ui/t;
.super Lcom/google/android/gms/udc/ui/j;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/udc/ui/j;-><init>()V

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 23
    sget v0, Lcom/google/android/gms/l;->fL:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/t;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 26
    sget v2, Lcom/google/android/gms/j;->sy:I

    const-string v3, "UdcDialogMessage"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/udc/ui/t;->a(Landroid/view/View;ILjava/lang/CharSequence;)V

    .line 28
    return-object v0
.end method

.method public final synthetic a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/t;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    new-instance v1, Landroid/app/ProgressDialog;

    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/t;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/ProgressDialog;-><init>(Landroid/content/Context;)V

    const-string v2, "UdcDialogMessage"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/app/ProgressDialog;->setMessage(Ljava/lang/CharSequence;)V

    return-object v1
.end method
