.class final Lcom/google/android/gms/games/provider/ba;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/provider/bb;


# instance fields
.field private final a:I

.field private final b:Ljava/lang/String;

.field private final c:Landroid/content/SharedPreferences$Editor;


# direct methods
.method constructor <init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1122
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/provider/ba;->a:I

    .line 1123
    iput-object p1, p0, Lcom/google/android/gms/games/provider/ba;->c:Landroid/content/SharedPreferences$Editor;

    .line 1124
    iput-object p2, p0, Lcom/google/android/gms/games/provider/ba;->b:Ljava/lang/String;

    .line 1125
    return-void
.end method


# virtual methods
.method public final a(Landroid/database/sqlite/SQLiteDatabase;I)V
    .locals 2

    .prologue
    .line 1129
    iget v0, p0, Lcom/google/android/gms/games/provider/ba;->a:I

    packed-switch v0, :pswitch_data_0

    .line 1136
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unsupported preference action, please add it to the builder."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1131
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/ba;->c:Landroid/content/SharedPreferences$Editor;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/ba;->b:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1140
    iget-object v0, p0, Lcom/google/android/gms/games/provider/ba;->c:Landroid/content/SharedPreferences$Editor;

    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 1141
    return-void

    .line 1129
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
