.class final Lcom/google/android/gms/icing/impl/am;
.super Lcom/google/android/gms/icing/impl/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Z

.field final synthetic b:Z

.field final synthetic c:Ljava/util/Set;

.field final synthetic d:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;ZZLjava/util/Set;)V
    .locals 0

    .prologue
    .line 2403
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iput-boolean p2, p0, Lcom/google/android/gms/icing/impl/am;->a:Z

    iput-boolean p3, p0, Lcom/google/android/gms/icing/impl/am;->b:Z

    iput-object p4, p0, Lcom/google/android/gms/icing/impl/am;->c:Ljava/util/Set;

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/ay;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 10

    .prologue
    .line 2406
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 2407
    new-instance v1, Landroid/util/TimingLogger;

    const-string v0, "Icing"

    const-string v4, "post-init"

    invoke-direct {v1, v0, v4}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 2410
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/icing/a/a;->a(Landroid/content/Context;)V

    .line 2411
    const-string v0, "gservices-cached"

    invoke-virtual {v1, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 2413
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/q;->i()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v0

    const/4 v4, 0x0

    invoke-static {v4}, Lcom/google/android/gms/search/queries/t;->a(Z)Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 2415
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/am;->a:Z

    if-nez v0, :cond_5

    .line 2418
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/am;->b:Z

    if-eqz v0, :cond_1

    .line 2419
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->q()V

    .line 2422
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->d()Z

    .line 2427
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c()Z

    .line 2428
    const-string v0, "index-restored"

    invoke-virtual {v1, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 2431
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->e:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->f()J

    move-result-wide v4

    .line 2432
    sget-object v0, Lcom/google/android/gms/icing/a/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    .line 2433
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    add-long/2addr v4, v6

    cmp-long v0, v8, v4

    if-lez v0, :cond_2

    .line 2435
    const-string v0, "No maintenance for too long"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 2436
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->l:Lcom/google/android/gms/icing/impl/m;

    const-string v4, "no_maint_too_long"

    invoke-interface {v0, v4}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 2437
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->w()V

    .line 2438
    const-string v0, "forced-maintenance"

    invoke-virtual {v1, v0}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 2442
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->c:Ljava/util/Set;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    .line 2443
    const-string v0, "%s corpora need re-polling"

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/am;->c:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 2444
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->c:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_3
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2445
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v5

    invoke-virtual {v5, v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v5

    .line 2446
    if-eqz v5, :cond_3

    iget-object v6, v5, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v6, v6, Lcom/google/android/gms/icing/j;->d:I

    if-nez v6, :cond_3

    iget-object v6, v5, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 2449
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v6, v6, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    iget-object v5, v5, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {v6, v0, v5}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;Lcom/google/android/gms/icing/g;)V

    goto :goto_0

    .line 2453
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->j:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->r()Z

    .line 2454
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/q;->n()Z

    move-result v4

    if-eqz v4, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/b;->a(Landroid/content/Context;)V

    .line 2457
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/am;->d:Lcom/google/android/gms/icing/impl/u;

    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3}, Lcom/google/android/gms/icing/impl/u;->a(IJ)V

    .line 2458
    invoke-virtual {v1}, Landroid/util/TimingLogger;->dumpToLog()V

    .line 2459
    const-string v0, "Post-init done"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 2460
    return-void
.end method
