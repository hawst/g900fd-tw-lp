.class public final Lcom/google/android/gms/icing/an;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/icing/an;


# instance fields
.field public a:I

.field public b:Z

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 4437
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4438
    iput v1, p0, Lcom/google/android/gms/icing/an;->a:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/an;->b:Z

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/an;->c:Z

    iput v1, p0, Lcom/google/android/gms/icing/an;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/an;->cachedSize:I

    .line 4439
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/an;
    .locals 2

    .prologue
    .line 4414
    sget-object v0, Lcom/google/android/gms/icing/an;->e:[Lcom/google/android/gms/icing/an;

    if-nez v0, :cond_1

    .line 4415
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4417
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/an;->e:[Lcom/google/android/gms/icing/an;

    if-nez v0, :cond_0

    .line 4418
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/an;

    sput-object v0, Lcom/google/android/gms/icing/an;->e:[Lcom/google/android/gms/icing/an;

    .line 4420
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4422
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/an;->e:[Lcom/google/android/gms/icing/an;

    return-object v0

    .line 4420
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 4504
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4505
    iget v1, p0, Lcom/google/android/gms/icing/an;->a:I

    if-eqz v1, :cond_0

    .line 4506
    iget v1, p0, Lcom/google/android/gms/icing/an;->a:I

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4509
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/icing/an;->b:Z

    if-eqz v1, :cond_1

    .line 4510
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/icing/an;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4513
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/an;->c:Z

    if-eq v1, v3, :cond_2

    .line 4514
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/an;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4517
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/an;->d:I

    if-eqz v1, :cond_3

    .line 4518
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/an;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4521
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4452
    if-ne p1, p0, :cond_1

    .line 4471
    :cond_0
    :goto_0
    return v0

    .line 4455
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/an;

    if-nez v2, :cond_2

    move v0, v1

    .line 4456
    goto :goto_0

    .line 4458
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/an;

    .line 4459
    iget v2, p0, Lcom/google/android/gms/icing/an;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/an;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4460
    goto :goto_0

    .line 4462
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/icing/an;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/an;->b:Z

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 4463
    goto :goto_0

    .line 4465
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/an;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/an;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 4466
    goto :goto_0

    .line 4468
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/an;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/an;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 4469
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 4

    .prologue
    const/16 v2, 0x4d5

    const/16 v1, 0x4cf

    .line 4476
    iget v0, p0, Lcom/google/android/gms/icing/an;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4478
    mul-int/lit8 v3, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/an;->b:Z

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v3

    .line 4479
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v3, p0, Lcom/google/android/gms/icing/an;->c:Z

    if-eqz v3, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 4480
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/an;->d:I

    add-int/2addr v0, v1

    .line 4481
    return v0

    :cond_0
    move v0, v2

    .line 4478
    goto :goto_0

    :cond_1
    move v1, v2

    .line 4479
    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4408
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/an;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/an;->b:Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/an;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/an;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4487
    iget v0, p0, Lcom/google/android/gms/icing/an;->a:I

    if-eqz v0, :cond_0

    .line 4488
    iget v0, p0, Lcom/google/android/gms/icing/an;->a:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 4490
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/an;->b:Z

    if-eqz v0, :cond_1

    .line 4491
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/icing/an;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4493
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/an;->c:Z

    if-eq v0, v2, :cond_2

    .line 4494
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/an;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4496
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/an;->d:I

    if-eqz v0, :cond_3

    .line 4497
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/an;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 4499
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4500
    return-void
.end method
