.class final Lcom/google/android/gms/checkin/c;
.super Lcom/google/android/gms/checkin/g;
.source "SourceFile"


# instance fields
.field final synthetic a:I

.field final synthetic b:I

.field final synthetic c:I

.field final synthetic d:Lcom/google/android/gms/checkin/CheckinService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/checkin/CheckinService;III)V
    .locals 0

    .prologue
    .line 414
    iput-object p1, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    iput p2, p0, Lcom/google/android/gms/checkin/c;->a:I

    iput p3, p0, Lcom/google/android/gms/checkin/c;->b:I

    iput p4, p0, Lcom/google/android/gms/checkin/c;->c:I

    invoke-direct {p0}, Lcom/google/android/gms/checkin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final varargs a([Lcom/google/android/gms/checkin/h;)Lcom/google/android/gms/checkin/b/c;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 417
    const-string v0, "CheckinService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "CheckinService"

    const-string v1, "task doInBackground"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    const/4 v0, 0x0

    .line 421
    :cond_1
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-static {v1}, Lcom/google/android/gms/checkin/CheckinService;->a(Lcom/google/android/gms/checkin/CheckinService;)Lcom/google/android/gms/checkin/d;

    move-result-object v1

    if-eqz v1, :cond_5

    .line 423
    const-string v0, "CheckinService"

    const-string v2, "Preparing to send checkin request"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget v0, v1, Lcom/google/android/gms/checkin/d;->b:I

    .line 425
    if-lez v0, :cond_3

    if-eqz p1, :cond_3

    array-length v1, p1

    if-lez v1, :cond_3

    aget-object v1, p1, v4

    if-eqz v1, :cond_3

    .line 427
    aget-object v1, p1, v4

    iget-object v1, v1, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    if-eqz v1, :cond_4

    .line 428
    aget-object v1, p1, v4

    iget-object v1, v1, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    iget v1, v1, Lcom/google/android/gms/checkin/b/k;->a:I

    if-lez v1, :cond_2

    .line 429
    const-string v1, "CheckinService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Overwrite current checkin reason type from: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    aget-object v3, p1, v4

    iget-object v3, v3, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    iget v3, v3, Lcom/google/android/gms/checkin/b/k;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 432
    :cond_2
    aget-object v1, p1, v4

    iget-object v1, v1, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/checkin/b/k;->a(I)Lcom/google/android/gms/checkin/b/k;

    .line 439
    :cond_3
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/EventLogService;->a(Landroid/content/Context;)V

    .line 440
    invoke-super {p0, p1}, Lcom/google/android/gms/checkin/g;->a([Lcom/google/android/gms/checkin/h;)Lcom/google/android/gms/checkin/b/c;

    move-result-object v0

    .line 441
    iget-object v1, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    iget-object v2, p0, Lcom/google/android/gms/checkin/c;->e:Lcom/google/android/gms/checkin/b/b;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/checkin/CheckinService;->a(Lcom/google/android/gms/checkin/CheckinService;Lcom/google/android/gms/checkin/b/b;Lcom/google/android/gms/checkin/b/c;)V

    .line 442
    if-eqz v0, :cond_1

    .line 443
    invoke-static {}, Lcom/google/android/gms/checkin/CheckinService;->a()Z

    goto :goto_0

    .line 434
    :cond_4
    aget-object v1, p1, v4

    iget-object v2, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    iget-object v2, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-static {v2}, Lcom/google/android/gms/checkin/CheckinService;->b(Lcom/google/android/gms/checkin/CheckinService;)I

    move-result v2

    invoke-static {v0, v2}, Lcom/google/android/gms/checkin/CheckinService;->a(II)Lcom/google/android/gms/checkin/b/k;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    goto :goto_1

    .line 446
    :cond_5
    return-object v0
.end method

.method public final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 414
    check-cast p1, [Lcom/google/android/gms/checkin/h;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/checkin/c;->a([Lcom/google/android/gms/checkin/h;)Lcom/google/android/gms/checkin/b/c;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 414
    check-cast p1, Lcom/google/android/gms/checkin/b/c;

    iget-object v0, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/CheckinService;->c(Lcom/google/android/gms/checkin/CheckinService;)Lcom/google/android/gms/checkin/g;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/gms/checkin/c;->a:I

    add-int/lit8 v0, v0, 0x1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-static {v1}, Lcom/google/android/gms/checkin/CheckinService;->b(Lcom/google/android/gms/checkin/CheckinService;)I

    move-result v1

    iget v2, p0, Lcom/google/android/gms/checkin/c;->b:I

    if-ne v1, v2, :cond_2

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.checkin.CHECKIN_COMPLETE"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    if-lez v0, :cond_1

    const-string v0, "success"

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/checkin/CheckinService;->sendBroadcast(Landroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    invoke-virtual {v0}, Lcom/google/android/gms/checkin/CheckinService;->stopSelf()V

    :goto_2
    return-void

    :cond_0
    iget v0, p0, Lcom/google/android/gms/checkin/c;->a:I

    goto :goto_0

    :cond_1
    const-string v0, "success"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_1

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/checkin/c;->d:Lcom/google/android/gms/checkin/CheckinService;

    iget v2, p0, Lcom/google/android/gms/checkin/c;->c:I

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/checkin/CheckinService;->a(Lcom/google/android/gms/checkin/CheckinService;II)V

    goto :goto_2
.end method
