.class public final Lcom/google/android/gms/people/internal/b/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/ai;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/google/android/gms/common/api/am;
    .locals 10

    .prologue
    .line 77
    new-instance v1, Lcom/google/android/gms/people/internal/b/ao;

    const/4 v9, 0x0

    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    move-wide v6, p4

    move/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/people/internal/b/ao;-><init>(Lcom/google/android/gms/people/internal/b/an;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;JZZ)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 23
    const-string v0, "PeopleClientCall"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 24
    const-string v0, "requestSync"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v6

    const/4 v2, 0x1

    aput-object p3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/n;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 28
    :cond_0
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/internal/b/an;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/gms/common/api/am;
    .locals 8

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x0

    .line 36
    const-string v0, "PeopleClientCall"

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "requestSync"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p2, v1, v6

    const/4 v2, 0x1

    aput-object p3, v1, v2

    const/4 v2, 0x2

    invoke-static {p4, p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/n;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    :cond_0
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-wide v4, p4

    .line 41
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/internal/b/an;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 63
    const-string v0, "PeopleClientCall"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 64
    const-string v0, "requestSyncByUserAction"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p2, v1, v2

    aput-object p3, v1, v6

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/n;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 68
    :cond_0
    const-wide/16 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/internal/b/an;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;JZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method
