.class public final Lcom/google/android/gms/icing/ui/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field public final c:Landroid/graphics/drawable/Drawable;

.field public final d:J

.field public final e:J


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/drawable/Drawable;JJ)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/icing/ui/m;->a:Ljava/lang/String;

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/icing/ui/m;->b:Ljava/lang/String;

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/icing/ui/m;->c:Landroid/graphics/drawable/Drawable;

    .line 27
    iput-wide p4, p0, Lcom/google/android/gms/icing/ui/m;->d:J

    .line 28
    iput-wide p6, p0, Lcom/google/android/gms/icing/ui/m;->e:J

    .line 29
    return-void
.end method

.method static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 41
    :try_start_0
    invoke-virtual {p0, p1}, Landroid/content/pm/PackageManager;->getApplicationIcon(Ljava/lang/String;)Landroid/graphics/drawable/Drawable;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 44
    :goto_0
    return-object v0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    const-string v1, "Application not found: %s. Cause: %s"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 44
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Landroid/content/pm/ApplicationInfo;
    .locals 2

    .prologue
    .line 58
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 61
    :goto_0
    return-object v0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    const-string v1, "Application not found: %s. Cause: %s"

    invoke-virtual {v0}, Landroid/content/pm/PackageManager$NameNotFoundException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, p1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 61
    const/4 v0, 0x0

    goto :goto_0
.end method
