.class public Lcom/google/android/gms/car/oy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/on;

.field private final b:Lcom/google/android/gms/car/oi;

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/on;)V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/car/oy;->a:Lcom/google/android/gms/car/on;

    .line 41
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    .line 43
    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getDeviceId()Ljava/lang/String;

    move-result-object v1

    .line 45
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    if-eqz v0, :cond_0

    array-length v2, v0

    if-lez v2, :cond_0

    const/4 v2, 0x0

    aget-object v0, v0, v2

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 46
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/car/oy;->c:Ljava/lang/String;

    .line 47
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/oy;->d:Ljava/lang/String;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/car/oy;->a:Lcom/google/android/gms/car/on;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/car/oz;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/oz;-><init>(Lcom/google/android/gms/car/oy;)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/on;->a(ILcom/google/android/gms/car/om;)Lcom/google/android/gms/car/oi;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/oy;->b:Lcom/google/android/gms/car/oi;

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/car/oy;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->h()V

    .line 99
    return-void

    .line 45
    :cond_0
    const-string v0, "no email"

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/oy;)Lcom/google/android/gms/car/on;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/gms/car/oy;->a:Lcom/google/android/gms/car/on;

    return-object v0
.end method

.method private static a(Landroid/graphics/Canvas;Ljava/lang/String;FI)V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/high16 v9, 0x42200000    # 40.0f

    const/16 v8, 0xff

    .line 158
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 159
    invoke-virtual {v6, v9}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 161
    const/4 v4, 0x0

    move v2, v7

    :goto_0
    int-to-float v0, p3

    cmpg-float v0, v4, v0

    if-gez v0, :cond_1

    .line 162
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    invoke-virtual {v0, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v3

    invoke-virtual {v0, v8}, Ljava/util/Random;->nextInt(I)I

    move-result v0

    invoke-static {v1, v3, v0}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    invoke-virtual {v6, v0}, Landroid/graphics/Paint;->setColor(I)V

    .line 163
    add-int/lit8 v3, v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move v5, p2

    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;IIFFLandroid/graphics/Paint;)V

    .line 164
    add-int/lit8 v0, v2, 0x1

    .line 165
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v1

    if-lt v0, v1, :cond_0

    move v0, v7

    .line 161
    :cond_0
    add-float/2addr v4, v9

    move v2, v0

    goto :goto_0

    .line 169
    :cond_1
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/oy;Lcom/google/android/gms/car/oi;II)V
    .locals 6

    .prologue
    .line 22
    const-string v0, "CAR.WM.WATERMARK"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.WM.WATERMARK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "renderWatermark to window "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->e()Landroid/view/Surface;

    move-result-object v1

    if-eqz v1, :cond_2

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/view/Surface;->lockCanvas(Landroid/graphics/Rect;)Landroid/graphics/Canvas;

    move-result-object v2

    div-int/lit8 v0, p3, 0x6

    int-to-float v3, v0

    const/4 v0, 0x0

    :goto_0
    const/4 v4, 0x6

    if-ge v0, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/gms/car/oy;->c:Ljava/lang/String;

    int-to-float v5, v0

    mul-float/2addr v5, v3

    invoke-static {v2, v4, v5, p2}, Lcom/google/android/gms/car/oy;->a(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    iget-object v4, p0, Lcom/google/android/gms/car/oy;->d:Ljava/lang/String;

    add-int/lit8 v5, v0, 0x1

    int-to-float v5, v5

    mul-float/2addr v5, v3

    invoke-static {v2, v4, v5, p2}, Lcom/google/android/gms/car/oy;->a(Landroid/graphics/Canvas;Ljava/lang/String;FI)V

    add-int/lit8 v0, v0, 0x2

    goto :goto_0

    :cond_1
    invoke-virtual {v1, v2}, Landroid/view/Surface;->unlockCanvasAndPost(Landroid/graphics/Canvas;)V

    :cond_2
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 106
    const-string v0, "release"

    invoke-static {p0, v0}, Lcom/google/android/gms/car/jp;->b(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    .line 109
    :goto_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "gearhead:should_show_watermark"

    invoke-static {v1, v2, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
