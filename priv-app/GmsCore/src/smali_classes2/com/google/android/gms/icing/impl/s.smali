.class final Lcom/google/android/gms/icing/impl/s;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Set;

.field final b:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 947
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 948
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    .line 949
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    .line 950
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/icing/impl/s;)V
    .locals 2

    .prologue
    .line 952
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 953
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clone()Landroid/util/SparseArray;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    .line 954
    new-instance v0, Ljava/util/HashSet;

    iget-object v1, p1, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    .line 955
    return-void
.end method

.method static a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lcom/google/android/gms/icing/impl/s;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1010
    new-instance v3, Lcom/google/android/gms/icing/impl/s;

    invoke-direct {v3}, Lcom/google/android/gms/icing/impl/s;-><init>()V

    .line 1012
    new-instance v0, Lcom/google/r/a/a/b;

    invoke-direct {v0}, Lcom/google/r/a/a/b;-><init>()V

    .line 1013
    const/4 v2, 0x0

    invoke-interface {p0, p1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/b;

    .line 1016
    iget-object v4, v0, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v5, v4

    move v2, v1

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, v4, v2

    .line 1017
    iget v7, v6, Lcom/google/r/a/a/c;->b:I

    invoke-virtual {v3, v7, v6}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    .line 1016
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1019
    :cond_0
    iget-object v2, v0, Lcom/google/r/a/a/b;->c:[I

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget v1, v2, v0

    .line 1020
    invoke-virtual {v3, v1}, Lcom/google/android/gms/icing/impl/s;->c(I)V

    .line 1019
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1022
    :cond_1
    return-object v3
.end method


# virtual methods
.method public final a(I)Lcom/google/r/a/a/c;
    .locals 1

    .prologue
    .line 962
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    return-object v0
.end method

.method public final a(ILcom/google/r/a/a/c;)V
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 995
    return-void
.end method

.method public final a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1031
    new-instance v4, Lcom/google/r/a/a/b;

    invoke-direct {v4}, Lcom/google/r/a/a/b;-><init>()V

    .line 1032
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, v4, Lcom/google/r/a/a/b;->c:[I

    .line 1034
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1035
    iget-object v6, v4, Lcom/google/r/a/a/b;->c:[I

    add-int/lit8 v3, v1, 0x1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v6, v1

    move v1, v3

    .line 1036
    goto :goto_0

    .line 1037
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/r/a/a/c;

    iput-object v0, v4, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    move v1, v2

    .line 1039
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_1

    .line 1040
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1041
    iget-object v5, v4, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    add-int/lit8 v3, v1, 0x1

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v6, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    aput-object v0, v5, v1

    .line 1039
    add-int/lit8 v2, v2, 0x1

    move v1, v3

    goto :goto_1

    .line 1043
    :cond_1
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1, p2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1045
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v1, 0x0

    .line 1097
    const-string v0, "%sExpts: "

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1098
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/s;->a()[I

    move-result-object v2

    .line 1099
    invoke-static {v2}, Ljava/util/Arrays;->sort([I)V

    move v0, v1

    .line 1100
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 1101
    const-string v3, "%d "

    new-array v4, v7, [Ljava/lang/Object;

    aget v5, v2, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-virtual {p1, v3, v4}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1100
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1103
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1106
    const-string v0, "%sFlags:\n"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p2, v2, v1

    invoke-virtual {p1, v0, v2}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    move v2, v1

    .line 1107
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_5

    .line 1108
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v4

    .line 1109
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/p;->d(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 1111
    const-string v0, "%s    %d: unknown"

    new-array v3, v8, [Ljava/lang/Object;

    aput-object p2, v3, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v7

    invoke-virtual {p1, v0, v3}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    .line 1107
    :cond_1
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1115
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    .line 1117
    const/4 v3, 0x0

    .line 1118
    iget-boolean v5, v0, Lcom/google/r/a/a/c;->c:Z

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/p;->b(I)Z

    move-result v6

    if-eq v5, v6, :cond_3

    .line 1119
    iget-boolean v0, v0, Lcom/google/r/a/a/c;->c:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    .line 1126
    :goto_3
    if-eqz v0, :cond_1

    .line 1127
    const-string v3, "%s    %d: %s\n"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object p2, v5, v1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v5, v7

    aput-object v0, v5, v8

    invoke-virtual {p1, v3, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    goto :goto_2

    .line 1120
    :cond_3
    iget v5, v0, Lcom/google/r/a/a/c;->e:I

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/p;->a(I)I

    move-result v6

    if-eq v5, v6, :cond_4

    .line 1121
    iget v0, v0, Lcom/google/r/a/a/c;->e:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    .line 1122
    :cond_4
    iget-object v5, v0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/p;->c(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_6

    .line 1123
    iget-object v0, v0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    goto :goto_3

    .line 1130
    :cond_5
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 1131
    return-void

    :cond_6
    move-object v0, v3

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/icing/impl/s;)Z
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 1051
    move v1, v2

    move v3, v2

    .line 1052
    :goto_0
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 1053
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 1054
    invoke-virtual {p0, v5}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    iget-object v6, p1, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v6, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v0, v6}, Lcom/google/r/a/a/c;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    iget-object v3, p1, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v3, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v0, v5, v3}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v3, v4

    .line 1052
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1063
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1064
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_8

    .line 1066
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move v0, v4

    :goto_2
    move v1, v0

    .line 1068
    goto :goto_1

    .line 1070
    :cond_3
    if-eqz v1, :cond_7

    if-nez v3, :cond_7

    .line 1071
    const-string v0, "Observed an experimentId change without accompanying flag change"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1076
    :cond_4
    :goto_3
    if-nez v3, :cond_5

    if-eqz v1, :cond_6

    :cond_5
    move v2, v4

    :cond_6
    return v2

    .line 1072
    :cond_7
    if-nez v1, :cond_4

    if-eqz v3, :cond_4

    .line 1073
    const-string v0, "Observed a flag change without accompanying experimentId change"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    goto :goto_3

    :cond_8
    move v0, v1

    goto :goto_2
.end method

.method public final a()[I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 973
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    if-eqz v1, :cond_0

    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    new-array v0, v0, [I

    :goto_0
    return-object v0

    :cond_1
    invoke-interface {v1}, Ljava/util/Collection;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/Integer;

    invoke-interface {v1, v2}, Ljava/util/Collection;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    array-length v1, v2

    new-array v1, v1, [I

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    aget-object v3, v2, v0

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    aput v3, v1, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    goto :goto_0
.end method

.method public final b(I)Z
    .locals 1

    .prologue
    .line 966
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 980
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 981
    return-void
.end method

.method public final d(I)Z
    .locals 1

    .prologue
    .line 1084
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    iget-boolean v0, v0, Lcom/google/r/a/a/c;->c:Z

    return v0
.end method

.method public final e(I)I
    .locals 1

    .prologue
    .line 1088
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    iget v0, v0, Lcom/google/r/a/a/c;->e:I

    return v0
.end method

.method public final f(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1092
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    iget-object v0, v0, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    return-object v0
.end method
