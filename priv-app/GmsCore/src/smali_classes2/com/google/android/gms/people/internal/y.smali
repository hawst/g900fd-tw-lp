.class final Lcom/google/android/gms/people/internal/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/m;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Z

.field private final c:Ljava/lang/String;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 1262
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1263
    iput-object p1, p0, Lcom/google/android/gms/people/internal/y;->a:Lcom/google/android/gms/common/api/Status;

    .line 1264
    iput-boolean p2, p0, Lcom/google/android/gms/people/internal/y;->b:Z

    .line 1265
    iput-object p3, p0, Lcom/google/android/gms/people/internal/y;->c:Ljava/lang/String;

    .line 1266
    iput-object p4, p0, Lcom/google/android/gms/people/internal/y;->d:Ljava/lang/String;

    .line 1267
    iput-object p5, p0, Lcom/google/android/gms/people/internal/y;->e:Ljava/lang/String;

    .line 1268
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 1292
    iget-object v0, p0, Lcom/google/android/gms/people/internal/y;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 1272
    iget-boolean v0, p0, Lcom/google/android/gms/people/internal/y;->b:Z

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1277
    iget-object v0, p0, Lcom/google/android/gms/people/internal/y;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/google/android/gms/people/internal/y;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 1287
    iget-object v0, p0, Lcom/google/android/gms/people/internal/y;->e:Ljava/lang/String;

    return-object v0
.end method
