.class final Lcom/google/android/gms/people/internal/al;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 707
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 708
    iput-object p1, p0, Lcom/google/android/gms/people/internal/al;->a:Lcom/google/android/gms/common/api/m;

    .line 709
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    .prologue
    .line 713
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 714
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "People callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nholder="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 718
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/p;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 719
    invoke-static {p3}, Lcom/google/android/gms/people/internal/p;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/people/model/k;

    move-result-object v2

    .line 720
    if-nez p3, :cond_1

    const/4 v0, 0x0

    .line 723
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/people/internal/al;->a:Lcom/google/android/gms/common/api/m;

    new-instance v4, Lcom/google/android/gms/people/internal/aq;

    invoke-direct {v4, v1, v2, v0}, Lcom/google/android/gms/people/internal/aq;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/k;Ljava/lang/String;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 724
    return-void

    .line 720
    :cond_1
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "pageToken"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
