.class public Lcom/google/android/gms/checkin/g;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field protected e:Lcom/google/android/gms/checkin/b/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 93
    return-void
.end method

.method private static a(Lcom/google/android/gms/checkin/h;Lcom/google/android/gms/checkin/b/b;J)J
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 271
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 273
    iget-object v1, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v2, "phone"

    invoke-virtual {v1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/telephony/TelephonyManager;

    .line 275
    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v5, "wifi"

    invoke-virtual {v2, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/wifi/WifiManager;

    .line 278
    iget-object v5, p0, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    invoke-static {v5, p1}, Lcom/google/android/gms/checkin/a;->a(Landroid/content/SharedPreferences;Lcom/google/android/gms/checkin/b/b;)V

    .line 279
    iget-object v5, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v5, p1}, Lcom/google/android/gms/checkin/a;->a(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 280
    invoke-static {v1, v2, v0, p1}, Lcom/google/android/gms/checkin/a;->a(Landroid/telephony/TelephonyManager;Landroid/net/wifi/WifiManager;Landroid/net/ConnectivityManager;Lcom/google/android/gms/checkin/b/b;)V

    .line 281
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->a(Ljava/util/Locale;Lcom/google/android/gms/checkin/b/b;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/checkin/i;->a(Landroid/content/Context;)J

    move-result-wide v0

    .line 283
    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v2, v0, v1, p1}, Lcom/google/android/gms/checkin/a;->a(Landroid/content/Context;JLcom/google/android/gms/checkin/b/b;)V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->c(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 285
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->a(Ljava/util/TimeZone;Lcom/google/android/gms/checkin/b/b;)V

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->d(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {p1}, Lcom/google/android/gms/checkin/a;->a(Lcom/google/android/gms/checkin/b/b;)V

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->e(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->g(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->f(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/checkin/a;->b(Landroid/content/Context;Lcom/google/android/gms/checkin/b/b;)V

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->b:Landroid/os/DropBoxManager;

    if-eqz v0, :cond_4

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 295
    iget-object v1, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/checkin/EventLogService;->b(Landroid/content/Context;)Z

    move-result v2

    .line 296
    const-string v1, "checkin_dropbox_whitelist"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 297
    new-array v1, v4, [Ljava/lang/String;

    const-string v5, "checkin_dropbox_upload:"

    aput-object v5, v1, v3

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v1

    const-string v5, "checkin_dropbox_upload:"

    invoke-static {v1, v5, v2, v6}, Lcom/google/android/gms/checkin/EventLogService;->a(Ljava/util/Map;Ljava/lang/String;ZLjava/lang/String;)Ljava/util/Map;

    move-result-object v5

    .line 305
    const-string v1, "checkin_dropbox_upload"

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 306
    if-eqz v0, :cond_1

    sget-object v1, Lcom/google/android/gsf/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v1, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-eqz v0, :cond_1

    move v1, v4

    .line 308
    :goto_0
    if-eqz v1, :cond_2

    if-nez v2, :cond_2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 309
    const-string v0, ","

    invoke-virtual {v6, v0}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    array-length v6, v4

    move v0, v3

    :goto_1
    if-ge v0, v6, :cond_2

    aget-object v3, v4, v0

    .line 310
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "checkin_dropbox_upload:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-interface {v5, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    .line 312
    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "checkin_dropbox_upload:"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v7, "true"

    invoke-interface {v5, v3, v7}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_1
    move v1, v3

    .line 306
    goto :goto_0

    .line 317
    :cond_2
    if-eqz v2, :cond_3

    .line 318
    const-string v0, "checkin_dropbox_upload"

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toLowerCase()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v5, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 322
    :cond_3
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/a/a;->e(Landroid/content/Context;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->b:Landroid/os/DropBoxManager;

    iget v3, p0, Lcom/google/android/gms/checkin/h;->g:I

    iget v4, p0, Lcom/google/android/gms/checkin/h;->f:I

    move-wide v6, p2

    move-object v8, p1

    invoke-static/range {v1 .. v8}, Lcom/google/android/gms/checkin/a;->a(ZLandroid/os/DropBoxManager;IILjava/util/Map;JLcom/google/android/gms/checkin/b/b;)J

    move-result-wide p2

    .line 328
    :cond_4
    return-wide p2
.end method

.method private static a(Lcom/google/android/gms/checkin/b/c;Lcom/google/android/gms/checkin/b/c;)Lcom/google/android/gms/checkin/b/c;
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 479
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/c;->c()I

    move-result v4

    .line 480
    invoke-virtual {p1}, Lcom/google/android/gms/checkin/b/c;->c()I

    move-result v5

    move v3, v0

    .line 481
    :goto_0
    if-ge v3, v4, :cond_2

    .line 482
    invoke-virtual {p0, v3}, Lcom/google/android/gms/checkin/b/c;->a(I)Lcom/google/android/gms/checkin/b/m;

    move-result-object v6

    .line 483
    iget-object v7, v6, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    move v1, v0

    move v2, v0

    .line 488
    :goto_1
    if-ge v1, v5, :cond_0

    if-nez v2, :cond_0

    .line 489
    invoke-virtual {p1, v1}, Lcom/google/android/gms/checkin/b/c;->a(I)Lcom/google/android/gms/checkin/b/m;

    move-result-object v2

    .line 490
    iget-object v2, v2, Lcom/google/android/gms/checkin/b/m;->b:Ljava/lang/String;

    .line 491
    invoke-virtual {v7, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 488
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 494
    :cond_0
    if-nez v2, :cond_1

    invoke-virtual {p1, v6}, Lcom/google/android/gms/checkin/b/c;->a(Lcom/google/android/gms/checkin/b/m;)Lcom/google/android/gms/checkin/b/c;

    .line 481
    :cond_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_0

    .line 496
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/c;->m:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p1, Lcom/google/android/gms/checkin/b/c;->m:Z

    if-nez v1, :cond_3

    .line 497
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/c;->n:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/checkin/b/c;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/c;

    .line 499
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/c;->o:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p1, Lcom/google/android/gms/checkin/b/c;->o:Z

    if-nez v1, :cond_4

    .line 500
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/c;->p:Ljava/lang/String;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/checkin/b/c;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/c;

    .line 502
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/c;->f()I

    move-result v1

    if-lez v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/checkin/b/c;->f()I

    move-result v1

    if-nez v1, :cond_5

    move v1, v0

    .line 503
    :goto_2
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/c;->f()I

    move-result v0

    if-ge v1, v0, :cond_5

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/c;->q:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/checkin/b/c;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/c;

    .line 503
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 507
    :cond_5
    return-object p1
.end method

.method private static a(Lcom/google/android/gms/checkin/h;Lcom/google/android/gms/http/GoogleHttpClient;Lcom/google/android/gms/checkin/b/b;)Lcom/google/android/gms/checkin/b/c;
    .locals 14

    .prologue
    .line 335
    new-instance v5, Ljava/net/URL;

    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    invoke-direct {v5, v2}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    .line 336
    const/4 v2, 0x0

    move v4, v2

    :goto_0
    const/4 v2, 0x2

    if-ge v4, v2, :cond_7

    .line 338
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/http/GoogleHttpClient;->getConnectionFactory()Lcom/google/android/gms/http/e;

    move-result-object v2

    invoke-virtual {v2, v5}, Lcom/google/android/gms/http/e;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v3

    .line 339
    const-string v2, "POST"

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    .line 340
    const-string v2, "Content-type"

    const-string v6, "application/x-protobuffer"

    invoke-virtual {v3, v2, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    const-string v2, "Content-encoding"

    const-string v6, "gzip"

    invoke-virtual {v3, v2, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 342
    const-string v2, "Accept-encoding"

    const-string v6, "gzip"

    invoke-virtual {v3, v2, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 343
    const/4 v2, 0x1

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    .line 344
    const v2, 0x1d4c0

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 345
    const v2, 0x1d4c0

    invoke-virtual {v3, v2}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 346
    instance-of v2, v3, Ljavax/net/ssl/HttpsURLConnection;

    if-eqz v2, :cond_0

    .line 347
    move-object v0, v3

    check-cast v0, Ljavax/net/ssl/HttpsURLConnection;

    move-object v2, v0

    invoke-virtual {p1}, Lcom/google/android/gms/http/GoogleHttpClient;->getSocketFactory()Ljavax/net/ssl/SSLSocketFactory;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljavax/net/ssl/HttpsURLConnection;->setSSLSocketFactory(Ljavax/net/ssl/SSLSocketFactory;)V

    .line 350
    :cond_0
    new-instance v2, Lcom/google/k/e/j;

    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v6

    invoke-direct {v2, v6}, Lcom/google/k/e/j;-><init>(Ljava/io/OutputStream;)V

    .line 351
    new-instance v6, Ljava/util/zip/GZIPOutputStream;

    invoke-direct {v6, v2}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 352
    invoke-static {v6}, Lcom/google/protobuf/a/c;->a(Ljava/io/OutputStream;)Lcom/google/protobuf/a/c;

    move-result-object v7

    .line 353
    move-object/from16 v0, p2

    invoke-virtual {v0, v7}, Lcom/google/android/gms/checkin/b/b;->a(Lcom/google/protobuf/a/c;)V

    .line 354
    invoke-virtual {v7}, Lcom/google/protobuf/a/c;->a()V

    .line 355
    invoke-virtual {v6}, Ljava/util/zip/GZIPOutputStream;->close()V

    .line 356
    invoke-virtual {v2}, Lcom/google/k/e/j;->close()V

    .line 358
    const-string v6, "CheckinTask"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Sending checkin request ("

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/k/e/j;->a()J

    move-result-wide v8

    invoke-virtual {v7, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, " bytes)"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v6, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    invoke-virtual {v3}, Ljava/net/HttpURLConnection;->connect()V

    .line 360
    invoke-static {p0, v3}, Lcom/google/android/gms/checkin/g;->a(Lcom/google/android/gms/checkin/h;Ljava/net/HttpURLConnection;)Lcom/google/android/gms/checkin/b/c;
    :try_end_0
    .catch Ljavax/net/ssl/SSLException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 372
    :goto_1
    return-object v2

    .line 361
    :catch_0
    move-exception v2

    move-object v3, v2

    .line 362
    const-string v2, "CheckinTask"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SSL error, attempting time correction: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 363
    if-nez v4, :cond_5

    .line 364
    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    const-string v6, "https:"

    invoke-virtual {v2, v6}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x0

    :goto_2
    if-nez v2, :cond_6

    throw v3

    :cond_1
    new-instance v2, Ljava/net/URL;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "http:"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, p0, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    const/4 v8, 0x6

    invoke-virtual {v7, v8}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v2, v6}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/http/GoogleHttpClient;->getConnectionFactory()Lcom/google/android/gms/http/e;

    move-result-object v6

    invoke-virtual {v6, v2}, Lcom/google/android/gms/http/e;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v2

    const-string v6, "CheckinTask"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Attempting to get time from "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getURL()Ljava/net/URL;

    move-result-object v8

    invoke-virtual {v8}, Ljava/net/URL;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v6, "POST"

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setRequestMethod(Ljava/lang/String;)V

    const-string v6, "Content-type"

    const-string v7, "application/x-protobuffer"

    invoke-virtual {v2, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "Content-encoding"

    const-string v7, "gzip"

    invoke-virtual {v2, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "Accept-encoding"

    const-string v7, "gzip"

    invoke-virtual {v2, v6, v7}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Ljava/net/HttpURLConnection;->setDoOutput(Z)V

    new-instance v6, Ljava/util/zip/GZIPOutputStream;

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/util/zip/GZIPOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-static {v6}, Lcom/google/protobuf/a/c;->a(Ljava/io/OutputStream;)Lcom/google/protobuf/a/c;

    move-result-object v7

    const/4 v8, 0x0

    iget-object v9, p0, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    invoke-static {v8, v9}, Lcom/google/android/gms/checkin/a;->a(ILcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/b;

    move-result-object v8

    invoke-virtual {v8, v7}, Lcom/google/android/gms/checkin/b/b;->a(Lcom/google/protobuf/a/c;)V

    invoke-virtual {v7}, Lcom/google/protobuf/a/c;->a()V

    invoke-virtual {v6}, Ljava/util/zip/GZIPOutputStream;->close()V

    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->connect()V

    invoke-static {p0, v2}, Lcom/google/android/gms/checkin/g;->a(Lcom/google/android/gms/checkin/h;Ljava/net/HttpURLConnection;)Lcom/google/android/gms/checkin/b/c;

    move-result-object v2

    iget-boolean v6, v2, Lcom/google/android/gms/checkin/b/c;->c:Z

    if-nez v6, :cond_2

    const-string v2, "CheckinTask"

    const-string v6, "No time of day in checkin server response"

    invoke-static {v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_2
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    iget-wide v8, v2, Lcom/google/android/gms/checkin/b/c;->d:J

    sub-long v10, v8, v6

    invoke-static {v10, v11}, Ljava/lang/Math;->abs(J)J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/gms/checkin/h;->i:J

    cmp-long v2, v10, v12

    if-gez v2, :cond_3

    const-string v2, "CheckinTask"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Server time agrees: delta "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sub-long v6, v8, v6

    invoke-static {v6, v7}, Ljava/lang/Math;->abs(J)J

    move-result-wide v6

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " msec"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_3
    iget-wide v10, p0, Lcom/google/android/gms/checkin/h;->j:J

    cmp-long v2, v8, v10

    if-gez v2, :cond_4

    const-string v2, "CheckinTask"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Server time is curiously old: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v2, 0x0

    goto/16 :goto_2

    :cond_4
    const-string v2, "CheckinTask"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Changing time from "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " to "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v6}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v2, p0, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v6, "alarm"

    invoke-virtual {v2, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/AlarmManager;

    invoke-virtual {v2, v8, v9}, Landroid/app/AlarmManager;->setTime(J)V

    const/4 v2, 0x1

    goto/16 :goto_2

    .line 367
    :cond_5
    throw v3

    .line 336
    :cond_6
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto/16 :goto_0

    .line 372
    :cond_7
    const/4 v2, 0x0

    goto/16 :goto_1
.end method

.method private static a(Lcom/google/android/gms/checkin/h;Ljava/net/HttpURLConnection;)Lcom/google/android/gms/checkin/b/c;
    .locals 4

    .prologue
    .line 425
    const-string v0, "Retry-After"

    invoke-virtual {p1, v0}, Ljava/net/HttpURLConnection;->getHeaderField(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 426
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    invoke-virtual {v1, v0}, Lcom/android/a/a;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 428
    const-string v1, "CheckinTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got Retry-After: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_0
    :goto_0
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseMessage()Ljava/lang/String;

    move-result-object v0

    .line 435
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v1

    .line 436
    const/16 v2, 0xc8

    if-eq v1, v2, :cond_2

    .line 437
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Rejected response from server: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 430
    :cond_1
    const-string v1, "CheckinTask"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Can\'t parse Retry-After: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 444
    :cond_2
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentType()Ljava/lang/String;

    move-result-object v0

    .line 445
    if-nez v0, :cond_3

    .line 446
    new-instance v0, Ljava/io/IOException;

    const-string v1, "No Content-Type header"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_3
    const-string v1, "application/x-protobuffer"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 449
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad Content-Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 452
    :cond_4
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getInputStream()Ljava/io/InputStream;

    move-result-object v1

    .line 453
    invoke-virtual {p1}, Ljava/net/HttpURLConnection;->getContentEncoding()Ljava/lang/String;

    move-result-object v0

    .line 454
    if-eqz v0, :cond_7

    const-string v2, "gzip"

    invoke-virtual {v0, v2}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 455
    new-instance v0, Ljava/util/zip/GZIPInputStream;

    invoke-direct {v0, v1}, Ljava/util/zip/GZIPInputStream;-><init>(Ljava/io/InputStream;)V

    .line 458
    :goto_1
    new-instance v1, Lcom/google/android/gms/checkin/b/c;

    invoke-direct {v1}, Lcom/google/android/gms/checkin/b/c;-><init>()V

    .line 460
    :try_start_0
    invoke-static {v0}, Lcom/google/protobuf/a/b;->a(Ljava/io/InputStream;)Lcom/google/protobuf/a/b;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/checkin/b/c;->b(Lcom/google/protobuf/a/b;)Lcom/google/android/gms/checkin/b/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 462
    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    .line 465
    iget-boolean v0, v1, Lcom/google/android/gms/checkin/b/c;->a:Z

    if-eqz v0, :cond_5

    iget-boolean v0, v1, Lcom/google/android/gms/checkin/b/c;->b:Z

    if-nez v0, :cond_6

    .line 466
    :cond_5
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Server refused checkin"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 462
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V

    throw v1

    .line 469
    :cond_6
    return-object v1

    :cond_7
    move-object v0, v1

    goto :goto_1
.end method


# virtual methods
.method protected varargs a([Lcom/google/android/gms/checkin/h;)Lcom/google/android/gms/checkin/b/c;
    .locals 19

    .prologue
    .line 202
    move-object/from16 v0, p1

    array-length v2, v0

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    new-instance v2, Ljava/lang/IllegalArgumentException;

    const-string v3, "Must be one Params object"

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 203
    :cond_0
    const/4 v2, 0x0

    aget-object v7, p1, v2

    .line 205
    const/4 v5, 0x0

    .line 206
    const/4 v4, 0x0

    .line 207
    const/4 v3, 0x0

    .line 208
    sget-object v2, Lcom/google/android/gms/common/security/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    .line 212
    const-wide/16 v8, 0x0

    .line 213
    :try_start_0
    iget-object v6, v7, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    if-eqz v6, :cond_1

    iget-object v6, v7, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    const-string v8, "CheckinTask_bookmark"

    const-wide/16 v10, 0x0

    invoke-interface {v6, v8, v10, v11}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v8

    .line 215
    :cond_1
    new-instance v6, Lcom/google/android/gms/http/GoogleHttpClient;

    iget-object v10, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "CheckinService-"

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const v12, 0x6768a8

    invoke-static {v12}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "/2.0"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-direct {v6, v10, v11, v12, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;ZZ)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_4
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 222
    :try_start_1
    iget-object v2, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v5, ""

    invoke-static {v2, v5}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-object v2, v4

    move v4, v3

    .line 223
    :goto_0
    :try_start_2
    iget v3, v7, Lcom/google/android/gms/checkin/h;->h:I

    if-ge v4, v3, :cond_c

    .line 225
    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    invoke-static {v4, v3}, Lcom/google/android/gms/checkin/a;->a(ILcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/b;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/gms/checkin/g;->e:Lcom/google/android/gms/checkin/b/b;

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/checkin/g;->e:Lcom/google/android/gms/checkin/b/b;

    invoke-static {v7, v3, v8, v9}, Lcom/google/android/gms/checkin/g;->a(Lcom/google/android/gms/checkin/h;Lcom/google/android/gms/checkin/b/b;J)J

    move-result-wide v10

    .line 227
    cmp-long v3, v8, v10

    if-nez v3, :cond_2

    if-gtz v4, :cond_c

    .line 229
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/checkin/g;->e:Lcom/google/android/gms/checkin/b/b;

    invoke-static {v7, v6, v3}, Lcom/google/android/gms/checkin/g;->a(Lcom/google/android/gms/checkin/h;Lcom/google/android/gms/http/GoogleHttpClient;Lcom/google/android/gms/checkin/b/b;)Lcom/google/android/gms/checkin/b/c;

    move-result-object v5

    .line 230
    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    if-eqz v3, :cond_3

    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    invoke-virtual {v3}, Lcom/android/a/a;->c()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_3
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 231
    :cond_3
    if-nez v2, :cond_9

    move-object v3, v5

    .line 233
    :goto_1
    :try_start_3
    iget-object v2, v7, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    if-eqz v2, :cond_6

    .line 234
    iget-object v2, v7, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 236
    const-string v8, "CheckinTask_bookmark"

    invoke-interface {v2, v8, v10, v11}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 238
    if-eqz v5, :cond_5

    iget-boolean v8, v5, Lcom/google/android/gms/checkin/b/c;->k:Z

    if-eqz v8, :cond_5

    iget-wide v8, v5, Lcom/google/android/gms/checkin/b/c;->l:J

    const-wide/16 v12, 0x0

    cmp-long v8, v8, v12

    if-eqz v8, :cond_5

    .line 241
    iget-wide v8, v5, Lcom/google/android/gms/checkin/b/c;->l:J
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :try_start_4
    iget-object v12, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    const-string v13, "com.google.android.gsf"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v12

    const-string v13, "CheckinService"

    const/4 v14, 0x0

    invoke-virtual {v12, v13, v14}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    if-eqz v13, :cond_5

    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    const-string v15, "CheckinTask_securityToken"

    const-wide/16 v16, 0x0

    move-wide/from16 v0, v16

    invoke-interface {v13, v15, v0, v1}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v16

    cmp-long v13, v8, v16

    if-eqz v13, :cond_4

    const-string v13, "CheckinTask_securityToken"

    invoke-interface {v14, v13, v8, v9}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->apply()V

    :cond_4
    invoke-virtual {v12}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v13

    new-instance v14, Ljava/io/File;

    const-string v15, "security_token"

    invoke-direct {v14, v13, v15}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v14}, Ljava/io/File;->exists()Z

    move-result v13

    if-nez v13, :cond_5

    new-instance v13, Ljava/io/DataOutputStream;

    const-string v14, "security_token"

    const/4 v15, 0x0

    invoke-virtual {v12, v14, v15}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v12

    invoke-direct {v13, v12}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v13, v8, v9}, Ljava/io/DataOutputStream;->writeLong(J)V

    invoke-virtual {v13}, Ljava/io/DataOutputStream;->close()V
    :try_end_4
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_4 .. :try_end_4} :catch_0
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 244
    :cond_5
    :goto_2
    :try_start_5
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 247
    :cond_6
    iget-object v8, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/gms/checkin/g;->e:Lcom/google/android/gms/checkin/b/b;

    const-string v2, ""

    if-eqz v9, :cond_7

    iget-boolean v12, v9, Lcom/google/android/gms/checkin/b/b;->k:Z

    if-eqz v12, :cond_7

    iget-object v9, v9, Lcom/google/android/gms/checkin/b/b;->l:Lcom/google/android/gms/checkin/b/j;

    iget-boolean v12, v9, Lcom/google/android/gms/checkin/b/j;->b:Z

    if-eqz v12, :cond_7

    iget-object v2, v9, Lcom/google/android/gms/checkin/b/j;->c:Ljava/lang/String;

    :cond_7
    invoke-static {v8, v2}, Lcom/google/android/gms/checkin/CheckinService;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 249
    if-eqz v5, :cond_8

    .line 250
    iget-object v2, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-static {v5, v2}, Lcom/google/android/gms/checkin/b;->a(Lcom/google/android/gms/checkin/b/c;Landroid/content/ContentResolver;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 223
    :cond_8
    add-int/lit8 v4, v4, 0x1

    move-wide v8, v10

    move-object v2, v3

    goto/16 :goto_0

    .line 231
    :cond_9
    :try_start_6
    invoke-static {v2, v5}, Lcom/google/android/gms/checkin/g;->a(Lcom/google/android/gms/checkin/b/c;Lcom/google/android/gms/checkin/b/c;)Lcom/google/android/gms/checkin/b/c;
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_3
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    move-result-object v3

    goto/16 :goto_1

    .line 241
    :catch_0
    move-exception v8

    :try_start_7
    const-string v8, "CheckinTask"

    const-string v9, "Cannot write token, failed to find GoogleServicesFramework package"

    invoke-static {v8, v9}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catchall {:try_start_7 .. :try_end_7} :catchall_1

    goto :goto_2

    .line 258
    :catch_1
    move-exception v2

    move-object v5, v6

    move-object/from16 v18, v3

    move-object v3, v2

    move-object/from16 v2, v18

    .line 259
    :goto_3
    :try_start_8
    const-string v6, "CheckinTask"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Checkin failed: "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v9, v7, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " (request #"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, "): "

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v6, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 261
    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    if-eqz v3, :cond_a

    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    invoke-virtual {v3}, Lcom/android/a/a;->b()V
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_2

    .line 263
    :cond_a
    if-eqz v5, :cond_b

    invoke-virtual {v5}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    .line 266
    :cond_b
    :goto_4
    return-object v2

    .line 254
    :cond_c
    :try_start_9
    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/checkin/g;->e:Lcom/google/android/gms/checkin/b/b;
    :try_end_9
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_3
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    :try_start_a
    const-string v5, "com.google.android.gsf"

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v3

    const-string v5, "CheckinService"

    const/4 v8, 0x0

    invoke-virtual {v3, v5, v8}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    if-eqz v3, :cond_d

    new-instance v5, Lcom/android/a/a;

    invoke-direct {v5, v3}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {v5}, Lcom/android/a/a;->a()V
    :try_end_a
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_a .. :try_end_a} :catch_2
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_3
    .catchall {:try_start_a .. :try_end_a} :catchall_1

    .line 256
    :cond_d
    :goto_5
    :try_start_b
    const-string v3, "CheckinTask"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "Checkin success: "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v7, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " ("

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v8, " requests sent)"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    if-eqz v3, :cond_e

    iget-object v3, v7, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    invoke-virtual {v3}, Lcom/android/a/a;->a()V
    :try_end_b
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_3
    .catchall {:try_start_b .. :try_end_b} :catchall_1

    .line 263
    :cond_e
    invoke-virtual {v6}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    goto :goto_4

    .line 254
    :catch_2
    move-exception v3

    :try_start_c
    const-string v3, "CheckinTask"

    const-string v5, "Cannot update checkin success time"

    invoke-static {v3, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_c
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_3
    .catchall {:try_start_c .. :try_end_c} :catchall_1

    goto :goto_5

    .line 258
    :catch_3
    move-exception v3

    move-object v5, v6

    goto/16 :goto_3

    .line 263
    :catchall_0
    move-exception v2

    move-object v6, v5

    :goto_6
    if-eqz v6, :cond_f

    invoke-virtual {v6}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    :cond_f
    throw v2

    :catchall_1
    move-exception v2

    goto :goto_6

    :catchall_2
    move-exception v2

    move-object v6, v5

    goto :goto_6

    .line 258
    :catch_4
    move-exception v2

    move-object/from16 v18, v2

    move-object v2, v4

    move v4, v3

    move-object/from16 v3, v18

    goto/16 :goto_3

    :catch_5
    move-exception v2

    move-object v5, v6

    move/from16 v18, v3

    move-object v3, v2

    move-object v2, v4

    move/from16 v4, v18

    goto/16 :goto_3
.end method

.method protected synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 80
    check-cast p1, [Lcom/google/android/gms/checkin/h;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/checkin/g;->a([Lcom/google/android/gms/checkin/h;)Lcom/google/android/gms/checkin/b/c;

    move-result-object v0

    return-object v0
.end method
