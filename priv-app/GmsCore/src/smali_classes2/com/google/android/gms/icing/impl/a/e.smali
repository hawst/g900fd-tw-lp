.class public final Lcom/google/android/gms/icing/impl/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/icing/impl/a/g;

.field public final c:Lcom/google/android/gms/icing/impl/q;

.field public final d:Ljava/util/Set;

.field public e:Ljava/lang/String;

.field public final f:Ljava/lang/Object;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/g;)V
    .locals 1

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    .line 77
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    .line 78
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/g;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    .line 79
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    .line 81
    return-void
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 159
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 160
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;

    move-result-object v1

    .line 162
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {p1, v1, v2}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->writeToParcel(Landroid/os/Parcel;I)V

    .line 163
    const-string v2, "App"

    invoke-virtual {v1}, Landroid/os/Parcel;->marshall()[B

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[B)Landroid/content/Intent;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    .line 167
    return-object v0

    .line 165
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    throw v0
.end method

.method private b(Lcom/google/android/gms/icing/impl/a/ac;)V
    .locals 6

    .prologue
    .line 280
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v4

    invoke-virtual {v1, v0, v2, v4, v5}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;IJ)V

    .line 286
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->j(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private n()Z
    .locals 2

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private o()Ljava/lang/String;
    .locals 4

    .prologue
    .line 486
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 490
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "last-update-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 488
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Landroid/support/v4/g/o;
    .locals 2

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->r(Ljava/lang/String;)Landroid/support/v4/g/o;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/ac;)V
    .locals 6

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 209
    const-string v0, "setGlobalSearchInfo"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 213
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/q;->i(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/q;->k(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/impl/q;->l(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    .line 218
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/ac;->a(Lcom/google/android/gms/icing/impl/a/ac;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 219
    new-instance v2, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "GlobalSearchApplicationInfo: cannot "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " when previously "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 238
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 207
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 223
    :cond_2
    :try_start_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    .line 224
    const-string v2, "setGlobalSearchInfo info changed for %s"

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 225
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/ac;->b(Lcom/google/android/gms/icing/impl/a/ac;)V

    .line 226
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/e;->b(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 238
    monitor-exit v1

    .line 240
    new-instance v1, Ljava/util/ArrayList;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 241
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 242
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/g;->a()Z

    move-result v2

    .line 243
    invoke-virtual {p0, v2}, Lcom/google/android/gms/icing/impl/a/e;->b(Z)Z

    move-result v2

    .line 244
    new-instance v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v4, v4, Lcom/google/android/gms/icing/impl/a/g;->d:Lcom/google/android/gms/icing/impl/a/z;

    invoke-interface {v4, p0}, Lcom/google/android/gms/icing/impl/a/z;->b(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/Map;

    move-result-object v4

    invoke-direct {v3, v0, v2, v4}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;-><init>(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;ZLjava/util/Map;)V

    .line 247
    if-eqz v2, :cond_3

    .line 248
    iget-object v0, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.google.android.gms.icing.GlobalSearchAppRegistered"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "AppInfo"

    invoke-virtual {v2, v4, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 249
    const-string v0, "com.google.android.gms.icing.GlobalSearchAppRegistered2"

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/impl/a/e;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 253
    :cond_3
    const-string v0, "com.google.android.gms.icing.GlobalSearchAppRegistered3"

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/impl/a/e;->a(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/GlobalSearchApplication;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 264
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/g;->b()Z

    move-result v0

    .line 265
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "gsaSigned= "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 266
    if-eqz v0, :cond_7

    .line 267
    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 268
    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending intent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 270
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_2

    .line 231
    :cond_4
    :try_start_2
    const-string v2, "setGlobalSearchInfo info unchanged for %s; updating timestamp to %d"

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 233
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;->a(J)V

    .line 234
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/e;->b(Lcom/google/android/gms/icing/impl/a/ac;)V

    .line 236
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 276
    :cond_5
    :goto_3
    return-void

    .line 257
    :cond_6
    new-instance v0, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.icing.GlobalSearchableAppUnRegistered"

    invoke-direct {v0, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 259
    const-string v2, "AppPackageName"

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 261
    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 273
    :cond_7
    const-string v0, "Not sending global search app notification. Search app not installed or not signed properly."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    goto :goto_3
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Z)V

    .line 126
    return-void
.end method

.method public final a(J)Z
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v5, 0x0

    const/4 v3, 0x0

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->h(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v3, v8

    .line 198
    :goto_0
    return v3

    .line 181
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 187
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iget v2, v4, Landroid/content/pm/ApplicationInfo;->labelRes:I

    iget v4, v4, Landroid/content/pm/ApplicationInfo;->icon:I

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    :try_start_1
    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/e;->a(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_1 .. :try_end_1} :catch_1

    :goto_1
    move v3, v8

    .line 198
    goto :goto_0

    .line 183
    :catch_0
    move-exception v0

    const-string v0, "Could not find app %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 195
    :catch_1
    move-exception v0

    const-string v0, "Implicit gsai conflict with explicit for %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 104
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 106
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Ljava/util/Set;
    .locals 2

    .prologue
    .line 113
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 114
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 115
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 494
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 495
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/e;->e:Ljava/lang/String;

    .line 496
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Z)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 303
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/q;->p(Ljava/lang/String;)I

    move-result v2

    .line 304
    if-nez p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/e;->g()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 305
    :cond_0
    if-eq v2, v0, :cond_2

    .line 307
    :cond_1
    :goto_0
    return v0

    :cond_2
    move v0, v1

    .line 305
    goto :goto_0

    .line 307
    :cond_3
    const/4 v3, 0x2

    if-eq v2, v3, :cond_1

    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 119
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Z)I
    .locals 4

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 411
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 413
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/e;->h()Ljava/lang/String;

    move-result-object v2

    .line 414
    if-nez v2, :cond_0

    .line 416
    const/4 v0, 0x3

    monitor-exit v1

    .line 431
    :goto_0
    return v0

    .line 418
    :cond_0
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 419
    if-eqz p1, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/e;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 421
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 431
    :cond_2
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 423
    :cond_3
    const/4 v0, 0x1

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 432
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 425
    :cond_4
    :try_start_2
    invoke-static {v0, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 427
    const/4 v0, 0x2

    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->g(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->n(Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public final f()Lcom/google/android/gms/icing/impl/a/ac;
    .locals 6

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 294
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/a/ac;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/q;->i(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/q;->k(Ljava/lang/String;)I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/impl/q;->l(Ljava/lang/String;)J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/icing/impl/a/ac;-><init>(Ljava/lang/Object;IJ)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 323
    if-eqz v0, :cond_1

    const-string v1, "-3p"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/e;->n()Z

    move-result v0

    goto :goto_0
.end method

.method public final h()Ljava/lang/String;
    .locals 4

    .prologue
    .line 375
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const/16 v2, 0x40

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 380
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Landroid/content/pm/PackageInfo;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 381
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "install-time-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 384
    :goto_0
    return-object v0

    .line 378
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 384
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "install-time-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-3p"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final i()V
    .locals 5

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 441
    :try_start_0
    const-string v0, "Unregistering package %s"

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/q;->d(Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/q;->o(Ljava/lang/String;)V

    .line 444
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/q;->f(Ljava/lang/String;)V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/q;->q(Ljava/lang/String;)V

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, ""

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 447
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()Landroid/content/res/Resources;
    .locals 2

    .prologue
    .line 452
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getResourcesForApplication(Ljava/lang/String;)Landroid/content/res/Resources;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 455
    :goto_0
    return-object v0

    .line 454
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Failed to get resources for client "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 455
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final k()V
    .locals 5

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/g;->b(Ljava/lang/String;)Landroid/support/v4/g/o;

    move-result-object v1

    .line 461
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iget-object v0, v1, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iget-object v0, v1, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v2, v3, v4, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;ILjava/lang/String;)V

    .line 462
    return-void
.end method

.method protected final l()V
    .locals 3

    .prologue
    .line 470
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/e;->o()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/icing/impl/q;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 472
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/e;->k()V

    .line 473
    return-void
.end method

.method public final m()Z
    .locals 2

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 478
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/e;->o()Ljava/lang/String;

    move-result-object v1

    .line 479
    if-eqz v0, :cond_0

    invoke-static {v1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 564
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "{"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
