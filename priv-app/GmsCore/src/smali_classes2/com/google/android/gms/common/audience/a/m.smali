.class public final Lcom/google/android/gms/common/audience/a/m;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.common.acl.ACTION_ONLY_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    .line 57
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "The account name is required."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v1, "EXTRA_ACCOUNT_NAME"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v1, "EXTRA_CLIENT_APPLICATION_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 59
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/audience/a/m;
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v1, "EXTRA_UPDATE_PERSON"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 103
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/m;
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v1, "EXTRA_PLUS_PAGE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 78
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/m;
    .locals 2

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/common/audience/a/m;->a:Landroid/content/Intent;

    const-string v1, "EXTRA_TARGET_CIRCLE_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 95
    return-object p0
.end method
