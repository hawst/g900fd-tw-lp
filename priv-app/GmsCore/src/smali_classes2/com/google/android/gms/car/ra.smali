.class Lcom/google/android/gms/car/ra;
.super Lcom/google/android/gms/car/rd;
.source "SourceFile"


# instance fields
.field private final l:Lcom/google/android/gms/car/au;

.field private final m:J

.field private final n:I

.field private final o:I

.field private final p:I

.field private final q:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/au;JJI)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 620
    add-long v2, p2, p4

    const/4 v0, 0x1

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/gms/car/rd;-><init>(JZ)V

    .line 621
    iput-object p1, p0, Lcom/google/android/gms/car/ra;->l:Lcom/google/android/gms/car/au;

    .line 622
    iput-wide p2, p0, Lcom/google/android/gms/car/ra;->m:J

    .line 623
    iput v1, p0, Lcom/google/android/gms/car/ra;->n:I

    .line 624
    iput p6, p0, Lcom/google/android/gms/car/ra;->o:I

    .line 625
    iput v1, p0, Lcom/google/android/gms/car/ra;->p:I

    .line 626
    iput v1, p0, Lcom/google/android/gms/car/ra;->q:I

    .line 627
    iget v0, p0, Lcom/google/android/gms/car/ra;->n:I

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/ra;->o:I

    if-eqz v0, :cond_1

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/ra;->f:[F

    iget v3, p0, Lcom/google/android/gms/car/ra;->n:I

    int-to-float v4, v3

    iget v3, p0, Lcom/google/android/gms/car/ra;->o:I

    int-to-float v5, v3

    const/4 v6, 0x0

    move v3, v1

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    .line 633
    :goto_0
    return-void

    .line 631
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v0, v1}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    goto :goto_0
.end method


# virtual methods
.method protected final b(J)Lcom/google/android/gms/car/qq;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->k:Lcom/google/android/gms/car/qq;

    iput-wide p1, v0, Lcom/google/android/gms/car/qq;->c:J

    .line 638
    iget-wide v2, p0, Lcom/google/android/gms/car/ra;->m:J

    cmp-long v0, p1, v2

    if-lez v0, :cond_0

    .line 639
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->l:Lcom/google/android/gms/car/au;

    long-to-float v2, p1

    iget-wide v4, p0, Lcom/google/android/gms/car/ra;->m:J

    long-to-float v3, v4

    iget-wide v4, p0, Lcom/google/android/gms/car/ra;->g:J

    long-to-float v4, v4

    invoke-interface {v0, v2, v3, v4}, Lcom/google/android/gms/car/au;->a(FFF)F

    move-result v0

    .line 641
    iget v2, p0, Lcom/google/android/gms/car/ra;->n:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/car/ra;->p:I

    iget v4, p0, Lcom/google/android/gms/car/ra;->n:I

    sub-int/2addr v3, v4

    int-to-float v3, v3

    mul-float/2addr v3, v0

    add-float v4, v2, v3

    .line 642
    iget v2, p0, Lcom/google/android/gms/car/ra;->o:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/gms/car/ra;->q:I

    iget v5, p0, Lcom/google/android/gms/car/ra;->o:I

    sub-int/2addr v3, v5

    int-to-float v3, v3

    mul-float/2addr v0, v3

    add-float v5, v2, v0

    .line 643
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/ra;->f:[F

    const/4 v6, 0x0

    move v3, v1

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->translateM([FI[FIFFF)V

    .line 646
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ra;->k:Lcom/google/android/gms/car/qq;

    return-object v0
.end method
