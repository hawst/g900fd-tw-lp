.class public final Lcom/google/android/gms/herrevad/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 19
    const-string v0, "herrevad.max_network_quality_uploads_per_day"

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/a/a;->a:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "herrevad.herrevad_id_lifetime_seconds"

    const-wide/32 v2, 0x93a80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/a/a;->b:Lcom/google/android/gms/common/a/d;

    .line 36
    const-string v0, "herrevad.herrevad_mso_list"

    const-string v1, "GoogleGuest,GoogleWiFi"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/a/a;->c:Lcom/google/android/gms/common/a/d;

    .line 43
    const-string v1, "herrevad.flip_captive_portal_bit"

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x15

    if-ne v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v1, v0}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/a/a;->d:Lcom/google/android/gms/common/a/d;

    return-void

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
