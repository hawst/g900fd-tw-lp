.class final Lcom/google/android/gms/http/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/http/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/http/e;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/gms/http/f;->a:Lcom/google/android/gms/http/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/http/f;->a:Lcom/google/android/gms/http/e;

    invoke-static {p2}, Lcom/google/android/gms/http/h;->a(Landroid/os/IBinder;)Lcom/google/android/gms/http/g;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/http/e;->a(Lcom/google/android/gms/http/e;Lcom/google/android/gms/http/g;)Lcom/google/android/gms/http/g;

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/http/f;->a:Lcom/google/android/gms/http/e;

    invoke-static {v0}, Lcom/google/android/gms/http/e;->a(Lcom/google/android/gms/http/e;)Ljava/util/concurrent/CountDownLatch;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 85
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/http/f;->a:Lcom/google/android/gms/http/e;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/http/e;->a(Lcom/google/android/gms/http/e;Lcom/google/android/gms/http/g;)Lcom/google/android/gms/http/g;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/http/f;->a:Lcom/google/android/gms/http/e;

    invoke-static {v0}, Lcom/google/android/gms/http/e;->b(Lcom/google/android/gms/http/e;)Z

    .line 92
    return-void
.end method
