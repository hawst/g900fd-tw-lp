.class final Lcom/google/android/gms/games/internal/h;
.super Lcom/google/android/gms/games/internal/t;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/quest/f;


# instance fields
.field private final c:Lcom/google/android/gms/games/quest/Quest;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 2940
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2941
    new-instance v1, Lcom/google/android/gms/games/quest/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2943
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 2944
    new-instance v2, Lcom/google/android/gms/games/quest/QuestEntity;

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/quest/QuestEntity;-><init>(Lcom/google/android/gms/games/quest/Quest;)V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/h;->c:Lcom/google/android/gms/games/quest/Quest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2949
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    .line 2950
    return-void

    .line 2946
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/gms/games/internal/h;->c:Lcom/google/android/gms/games/quest/Quest;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2949
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    throw v0
.end method


# virtual methods
.method public final c()Lcom/google/android/gms/games/quest/Quest;
    .locals 1

    .prologue
    .line 2955
    iget-object v0, p0, Lcom/google/android/gms/games/internal/h;->c:Lcom/google/android/gms/games/quest/Quest;

    return-object v0
.end method
