.class public final Lcom/google/android/gms/auth/frp/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/auth/frp/a;->a:Landroid/content/Context;

    .line 43
    return-void
.end method

.method private a(Lcom/google/android/gms/auth/frp/d;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 147
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.auth.frp.FRP_BIND"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "android.intent.category.DEFAULT"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 148
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 150
    :try_start_0
    new-instance v1, Lcom/google/android/gms/common/b;

    invoke-direct {v1}, Lcom/google/android/gms/common/b;-><init>()V

    .line 151
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/frp/a;->a:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v0, v1, v6}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 154
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/frp/h;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/frp/g;

    move-result-object v0

    .line 155
    invoke-interface {p1, v0}, Lcom/google/android/gms/auth/frp/d;->a(Lcom/google/android/gms/auth/frp/g;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 157
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/frp/a;->a:Landroid/content/Context;

    invoke-virtual {v4, v5, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 162
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return-object v0

    .line 157
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/frp/a;->a:Landroid/content/Context;

    invoke-virtual {v4, v5, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 160
    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    :cond_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 52
    :try_start_0
    new-instance v0, Lcom/google/android/gms/auth/frp/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/frp/b;-><init>(Lcom/google/android/gms/auth/frp/a;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/frp/a;->a(Lcom/google/android/gms/auth/frp/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 68
    :goto_0
    return v0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    :goto_1
    const-string v1, "FrpClient"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 68
    const/4 v0, 0x0

    goto :goto_0

    .line 58
    :catch_1
    move-exception v0

    goto :goto_1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 83
    :try_start_0
    new-instance v0, Lcom/google/android/gms/auth/frp/c;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/frp/c;-><init>(Lcom/google/android/gms/auth/frp/a;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/frp/a;->a(Lcom/google/android/gms/auth/frp/d;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 99
    :goto_0
    return v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    :goto_1
    const-string v1, "FrpClient"

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 99
    const/4 v0, 0x0

    goto :goto_0

    .line 89
    :catch_1
    move-exception v0

    goto :goto_1
.end method
