.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/cb;
.super Lcom/google/android/gms/games/service/statemachine/h;
.source "SourceFile"


# static fields
.field protected static final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/games/c/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->h:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/bz;Lcom/google/android/gms/games/service/c;Lcom/google/android/gms/games/internal/eg;)V
    .locals 2

    .prologue
    .line 43
    const-string v0, "RoomServiceClientStateMachine"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/h;-><init>(Ljava/lang/String;)V

    .line 44
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Ljava/lang/Object;)V

    .line 45
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->g:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->h:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->i:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->m:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->p:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->q:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iput-object p2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->d:Lcom/google/android/gms/games/service/c;

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iput-object p1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iput-object p3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->e:Lcom/google/android/gms/games/internal/eg;

    .line 49
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->h:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->b(Z)V

    .line 50
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->h:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1000

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(I)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->e()V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->g:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->h:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->i:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->m:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->o:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->p:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->q:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmEnterRoomState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->f()V

    .line 69
    return-void

    .line 50
    :cond_0
    const/16 v0, 0x80

    goto/16 :goto_0
.end method

.method protected static a(Landroid/os/RemoteException;)V
    .locals 2

    .prologue
    .line 91
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unable to communicate with Game"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 92
    return-void
.end method

.method protected static a(Lcom/google/android/gms/games/service/statemachine/roomclient/ca;Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 72
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;

    .line 75
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v2, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->a:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-interface {v2, v1}, Lcom/google/android/gms/games/internal/ed;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_1
    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v1, :cond_0

    .line 83
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->c:Lcom/google/android/gms/games/internal/dr;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->a:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->b:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/dr;->d(ILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 88
    :cond_0
    :goto_2
    return-void

    .line 75
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 77
    :catch_0
    move-exception v1

    .line 78
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 85
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Landroid/os/RemoteException;)V

    goto :goto_2
.end method


# virtual methods
.method protected final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 97
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 100
    :goto_1
    return-object v0

    .line 97
    :pswitch_0
    const-string v0, "ENTER_ROOM"

    goto :goto_0

    :pswitch_1
    const-string v0, "ON_SENT_RELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_2
    const-string v0, "ON_REAL_TIME_MESSAGE_RECEIVED"

    goto :goto_0

    :pswitch_3
    const-string v0, "ON_ROOM_CONNECTING"

    goto :goto_0

    :pswitch_4
    const-string v0, "ON_ROOM_AUTO_MATCHING"

    goto :goto_0

    :pswitch_5
    const-string v0, "ON_ROOM_CONNECTED"

    goto :goto_0

    :pswitch_6
    const-string v0, "ON_CONNECTED_TO_ROOM"

    goto :goto_0

    :pswitch_7
    const-string v0, "ON_DISCONNECTED_FROM_ROOM"

    goto :goto_0

    :pswitch_8
    const-string v0, "ON_PEER_INVITED_TO_ROOM"

    goto :goto_0

    :pswitch_9
    const-string v0, "ON_PEER_JOINED_ROOM"

    goto :goto_0

    :pswitch_a
    const-string v0, "ON_PEER_LEFT_ROOM"

    goto :goto_0

    :pswitch_b
    const-string v0, "ON_PEER_DECLINED"

    goto :goto_0

    :pswitch_c
    const-string v0, "ON_PEER_CONNECTED"

    goto :goto_0

    :pswitch_d
    const-string v0, "ON_PEER_DISCONNECTED"

    goto :goto_0

    :pswitch_e
    const-string v0, "ON_P2P_CONNECTED"

    goto :goto_0

    :pswitch_f
    const-string v0, "ON_P2P_DISCONNECTED"

    goto :goto_0

    :pswitch_10
    const-string v0, "ON_ROOM_TORN_DOWN"

    goto :goto_0

    :pswitch_11
    const-string v0, "REPORT_P2P_STATUS"

    goto :goto_0

    :pswitch_12
    const-string v0, "ON_ROOM_LEAVE_DIAGNOSTICS"

    goto :goto_0

    :pswitch_13
    const-string v0, "DATA_MANAGER_UPDATE_ROOM"

    goto :goto_0

    :pswitch_14
    const-string v0, "SERVICE_CONNECTED"

    goto :goto_0

    :pswitch_15
    const-string v0, "ROOM_SERVICE_GONE"

    goto :goto_0

    :pswitch_16
    const-string v0, "ON_DCM_INIT"

    goto :goto_0

    :pswitch_17
    const-string v0, "DONE_CLEANING"

    goto :goto_0

    :pswitch_18
    const-string v0, "ON_ROOM_ENTERED"

    goto :goto_0

    :pswitch_19
    const-string v0, "SEND_RELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_1a
    const-string v0, "SEND_UNRELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_1b
    const-string v0, "CREATE_SOCKET_CONNECTION"

    goto :goto_0

    :pswitch_1c
    const-string v0, "CREATE_NATIVE_SOCKET"

    goto :goto_0

    :pswitch_1d
    const-string v0, "REGISTER_WAITING_ROOM_LISTENER_RESTRICTED"

    goto :goto_0

    :pswitch_1e
    const-string v0, "UNREGISTER_WAITING_ROOM_LISTENER_RESTRICTED"

    goto :goto_0

    :pswitch_1f
    const-string v0, "ON_SIGN_OUT"

    goto :goto_0

    :pswitch_20
    const-string v0, "LEAVE_ROOM"

    goto :goto_0

    :pswitch_21
    const-string v0, "CONNECT_NETWORK_ON_CONNECTED"

    goto :goto_0

    :pswitch_22
    const-string v0, "ON_NETWORK_DISCONNECTED"

    goto :goto_0

    :pswitch_23
    const-string v0, "CLEANUP_LIB_JINGLE_TIMER"

    goto :goto_0

    :pswitch_24
    const-string v0, "SETUP_DONE"

    goto :goto_0

    :pswitch_25
    const-string v0, "ON_CLIENT_DISCONNECTING"

    goto :goto_0

    :pswitch_26
    const-string v0, "DISCONNECT_NETWORK"

    goto :goto_0

    :pswitch_27
    const-string v0, "CLEANUP_LIB_JINGLE"

    goto :goto_0

    :pswitch_28
    const-string v0, "GAME_DEATH_BINDER_DIED"

    goto :goto_0

    :pswitch_29
    const-string v0, "ON_CREATE_NATIVE_LIBJINGLE_SOCKET"

    goto :goto_0

    :pswitch_2a
    const-string v0, "ON_CREATE_SOCKET_CONNECTION"

    goto/16 :goto_0

    :pswitch_2b
    const-string v0, "ON_ROOM_LEFT"

    goto/16 :goto_0

    :pswitch_2c
    const-string v0, "ON_P2P_STATUS_REPORTED"

    goto/16 :goto_0

    :pswitch_2d
    const-string v0, "DISCONNECT_NETWORK_TIMER"

    goto/16 :goto_0

    :pswitch_2e
    const-string v0, "CLIENT_LEAVE_ROOM"

    goto/16 :goto_0

    :pswitch_2f
    const-string v0, "LIBJINGLE_FAIL_TIMER"

    goto/16 :goto_0

    :pswitch_30
    const-string v0, "ON_DCM_ROOM_ENTER_RESULT"

    goto/16 :goto_0

    .line 100
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 97
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_17
        :pswitch_18
        :pswitch_2e
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_21
        :pswitch_16
        :pswitch_20
        :pswitch_24
        :pswitch_25
        :pswitch_0
        :pswitch_2d
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_22
        :pswitch_23
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2f
        :pswitch_30
    .end packed-switch
.end method

.method protected final b(Landroid/os/RemoteException;)V
    .locals 3

    .prologue
    .line 105
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unable to communicate with RAS"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->i:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomclient/br;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->g:Lcom/google/android/gms/games/service/statemachine/f;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/f;->a()Lcom/google/android/gms/games/service/statemachine/g;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/br;-><init>(Lcom/google/android/gms/games/service/statemachine/g;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    const-string v1, "Room service gone"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/ed;->a(Ljava/lang/String;)V

    .line 112
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Killed RAS"

    invoke-static {v0, v1, p1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 116
    :goto_0
    return-void

    .line 113
    :catch_0
    move-exception v0

    .line 114
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "Unable to kill RAS (may already be dead)"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    return v0
.end method
