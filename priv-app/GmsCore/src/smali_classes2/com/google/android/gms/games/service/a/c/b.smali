.class public final Lcom/google/android/gms/games/service/a/c/b;
.super Lcom/google/android/gms/games/service/a/b;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/a/au;

.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/s;

.field private e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/a/s;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v1, 0x5

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/c/b;->b:Lcom/google/android/gms/games/a/au;

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/c/b;->c:Lcom/google/android/gms/games/internal/dr;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/c/b;->d:Lcom/google/android/gms/games/a/s;

    .line 32
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 37
    const/4 v0, 0x5

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Index exceeds maximum value"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/c/b;->e:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/c/b;->b:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/c/b;->d:Lcom/google/android/gms/games/a/s;

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/s;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/c/b;->e:Ljava/util/ArrayList;

    .line 41
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/c/b;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    return-object v0

    .line 37
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 46
    array-length v0, p1

    const/4 v1, 0x5

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Length should always be 5"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/c/b;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->a([Lcom/google/android/gms/common/data/DataHolder;)V

    .line 49
    return-void

    .line 46
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
