.class public final Lcom/google/android/gms/icing/h;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:[Ljava/lang/String;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:[Ljava/lang/String;

.field public g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 1368
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1369
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/h;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/h;->cachedSize:I

    .line 1370
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 1493
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1494
    iget v1, p0, Lcom/google/android/gms/icing/h;->a:I

    if-eqz v1, :cond_0

    .line 1495
    const/4 v1, 0x1

    iget v3, p0, Lcom/google/android/gms/icing/h;->a:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1498
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    move v4, v2

    .line 1501
    :goto_0
    iget-object v5, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v5, v5

    if-ge v1, v5, :cond_2

    .line 1502
    iget-object v5, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    aget-object v5, v5, v1

    .line 1503
    if-eqz v5, :cond_1

    .line 1504
    add-int/lit8 v4, v4, 0x1

    .line 1505
    invoke-static {v5}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v5

    add-int/2addr v3, v5

    .line 1501
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1509
    :cond_2
    add-int/2addr v0, v3

    .line 1510
    mul-int/lit8 v1, v4, 0x1

    add-int/2addr v0, v1

    .line 1512
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 1513
    const/4 v1, 0x3

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1516
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1517
    const/4 v1, 0x4

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1520
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 1521
    const/4 v1, 0x5

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1524
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 1525
    const/4 v1, 0x6

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1528
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    if-eqz v1, :cond_a

    iget-object v1, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_a

    move v1, v2

    move v3, v2

    .line 1531
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v4, v4

    if-ge v2, v4, :cond_9

    .line 1532
    iget-object v4, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    aget-object v4, v4, v2

    .line 1533
    if-eqz v4, :cond_8

    .line 1534
    add-int/lit8 v3, v3, 0x1

    .line 1535
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v1, v4

    .line 1531
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1539
    :cond_9
    add-int/2addr v0, v1

    .line 1540
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 1542
    :cond_a
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1386
    if-ne p1, p0, :cond_1

    .line 1432
    :cond_0
    :goto_0
    return v0

    .line 1389
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/h;

    if-nez v2, :cond_2

    move v0, v1

    .line 1390
    goto :goto_0

    .line 1392
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/h;

    .line 1393
    iget v2, p0, Lcom/google/android/gms/icing/h;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/h;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1394
    goto :goto_0

    .line 1396
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 1398
    goto :goto_0

    .line 1400
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 1401
    iget-object v2, p1, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    if-eqz v2, :cond_6

    move v0, v1

    .line 1402
    goto :goto_0

    .line 1404
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 1405
    goto :goto_0

    .line 1407
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    if-nez v2, :cond_7

    .line 1408
    iget-object v2, p1, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    if-eqz v2, :cond_8

    move v0, v1

    .line 1409
    goto :goto_0

    .line 1411
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 1412
    goto :goto_0

    .line 1414
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 1415
    iget-object v2, p1, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 1416
    goto :goto_0

    .line 1418
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 1419
    goto :goto_0

    .line 1421
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_b

    move v0, v1

    .line 1423
    goto :goto_0

    .line 1425
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    if-nez v2, :cond_c

    .line 1426
    iget-object v2, p1, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 1427
    goto :goto_0

    .line 1429
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 1430
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1437
    iget v0, p0, Lcom/google/android/gms/icing/h;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1439
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1441
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 1443
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 1445
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 1447
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 1449
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 1451
    return v0

    .line 1441
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 1443
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1

    .line 1445
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_2

    .line 1449
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1330
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/h;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    const/16 v0, 0x3a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_3

    :cond_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1457
    iget v0, p0, Lcom/google/android/gms/icing/h;->a:I

    if-eqz v0, :cond_0

    .line 1458
    const/4 v0, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/h;->a:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 1460
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    move v0, v1

    .line 1461
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 1462
    iget-object v2, p0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    aget-object v2, v2, v0

    .line 1463
    if-eqz v2, :cond_1

    .line 1464
    const/4 v3, 0x2

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1461
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1468
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 1469
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1471
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 1472
    const/4 v0, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1474
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 1475
    const/4 v0, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1477
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 1478
    const/4 v0, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1480
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_8

    .line 1481
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v0, v0

    if-ge v1, v0, :cond_8

    .line 1482
    iget-object v0, p0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    aget-object v0, v0, v1

    .line 1483
    if-eqz v0, :cond_7

    .line 1484
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 1481
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1488
    :cond_8
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1489
    return-void
.end method
