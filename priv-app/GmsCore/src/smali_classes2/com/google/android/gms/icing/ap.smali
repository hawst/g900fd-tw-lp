.class public final Lcom/google/android/gms/icing/ap;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:[I

.field public d:[Lcom/google/android/gms/icing/aq;

.field public e:I

.field public f:[I

.field public g:[B

.field public h:Lcom/google/android/gms/icing/at;

.field public i:[D


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 6263
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6264
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/ap;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-static {}, Lcom/google/android/gms/icing/aq;->a()[Lcom/google/android/gms/icing/aq;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    iput v1, p0, Lcom/google/android/gms/icing/ap;->e:I

    sget-object v0, Lcom/google/protobuf/nano/m;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->g:[B

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    sget-object v0, Lcom/google/protobuf/nano/m;->d:[D

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ap;->cachedSize:I

    .line 6265
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/ap;
    .locals 1

    .prologue
    .line 6661
    new-instance v0, Lcom/google/android/gms/icing/ap;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ap;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ap;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 6420
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6421
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6422
    const/4 v1, 0x1

    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6425
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/ap;->b:I

    if-eqz v1, :cond_1

    .line 6426
    const/4 v1, 0x2

    iget v3, p0, Lcom/google/android/gms/icing/ap;->b:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6429
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->c:[I

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v1, v1

    if-lez v1, :cond_3

    move v1, v2

    move v3, v2

    .line 6431
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 6432
    iget-object v4, p0, Lcom/google/android/gms/icing/ap;->c:[I

    aget v4, v4, v1

    .line 6433
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 6431
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 6436
    :cond_2
    add-int/2addr v0, v3

    .line 6437
    add-int/lit8 v0, v0, 0x1

    .line 6438
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 6441
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    array-length v1, v1

    if-lez v1, :cond_6

    move v1, v0

    move v0, v2

    .line 6442
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    array-length v3, v3

    if-ge v0, v3, :cond_5

    .line 6443
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    aget-object v3, v3, v0

    .line 6444
    if-eqz v3, :cond_4

    .line 6445
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v1, v3

    .line 6442
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_5
    move v0, v1

    .line 6450
    :cond_6
    iget v1, p0, Lcom/google/android/gms/icing/ap;->e:I

    if-eqz v1, :cond_7

    .line 6451
    const/4 v1, 0x5

    iget v3, p0, Lcom/google/android/gms/icing/ap;->e:I

    invoke-static {v1, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 6454
    :cond_7
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->f:[I

    if-eqz v1, :cond_9

    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v1, v1

    if-lez v1, :cond_9

    move v1, v2

    .line 6456
    :goto_2
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v3, v3

    if-ge v2, v3, :cond_8

    .line 6457
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->f:[I

    aget v3, v3, v2

    .line 6458
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v1, v3

    .line 6456
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 6461
    :cond_8
    add-int/2addr v0, v1

    .line 6462
    add-int/lit8 v0, v0, 0x1

    .line 6463
    invoke-static {v1}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 6466
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->g:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_a

    .line 6467
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->g:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 6470
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-eqz v1, :cond_b

    .line 6471
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6474
    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->i:[D

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v1, v1

    if-lez v1, :cond_c

    .line 6475
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v1, v1

    mul-int/lit8 v1, v1, 0x8

    .line 6476
    add-int/2addr v0, v1

    .line 6477
    add-int/lit8 v0, v0, 0x1

    .line 6478
    invoke-static {v1}, Lcom/google/protobuf/nano/b;->e(I)I

    move-result v1

    add-int/2addr v0, v1

    .line 6481
    :cond_c
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6283
    if-ne p1, p0, :cond_1

    .line 6331
    :cond_0
    :goto_0
    return v0

    .line 6286
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ap;

    if-nez v2, :cond_2

    move v0, v1

    .line 6287
    goto :goto_0

    .line 6289
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ap;

    .line 6290
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 6291
    iget-object v2, p1, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 6292
    goto :goto_0

    .line 6294
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 6295
    goto :goto_0

    .line 6297
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/ap;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/ap;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 6298
    goto :goto_0

    .line 6300
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 6302
    goto :goto_0

    .line 6304
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 6306
    goto :goto_0

    .line 6308
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/ap;->e:I

    iget v3, p1, Lcom/google/android/gms/icing/ap;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 6309
    goto :goto_0

    .line 6311
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->f:[I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([I[I)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 6313
    goto :goto_0

    .line 6315
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->g:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->g:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 6316
    goto :goto_0

    .line 6318
    :cond_a
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-nez v2, :cond_b

    .line 6319
    iget-object v2, p1, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-eqz v2, :cond_c

    move v0, v1

    .line 6320
    goto :goto_0

    .line 6323
    :cond_b
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/at;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_c

    move v0, v1

    .line 6324
    goto :goto_0

    .line 6327
    :cond_c
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->i:[D

    iget-object v3, p1, Lcom/google/android/gms/icing/ap;->i:[D

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([D[D)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 6329
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 6336
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 6339
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/ap;->b:I

    add-int/2addr v0, v2

    .line 6340
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 6342
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6344
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/ap;->e:I

    add-int/2addr v0, v2

    .line 6345
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/h;->a([I)I

    move-result v2

    add-int/2addr v0, v2

    .line 6347
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->g:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 6348
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 6350
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->i:[D

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([D)I

    move-result v1

    add-int/2addr v0, v1

    .line 6352
    return v0

    .line 6336
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 6348
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/at;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 5751
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ap;->b:I

    goto :goto_0

    :sswitch_3
    const/16 v0, 0x18

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_4

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    if-nez v2, :cond_6

    move v2, v1

    :goto_4
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_5

    iget-object v4, p0, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_5
    :goto_5
    array-length v4, v0

    if-ge v2, v4, :cond_7

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v2, v2

    goto :goto_4

    :cond_7
    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_5
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    if-nez v0, :cond_9

    move v0, v1

    :goto_6
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/aq;

    if-eqz v0, :cond_8

    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_8
    :goto_7
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_a

    new-instance v3, Lcom/google/android/gms/icing/aq;

    invoke-direct {v3}, Lcom/google/android/gms/icing/aq;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_7

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    array-length v0, v0

    goto :goto_6

    :cond_a
    new-instance v3, Lcom/google/android/gms/icing/aq;

    invoke-direct {v3}, Lcom/google/android/gms/icing/aq;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ap;->e:I

    goto/16 :goto_0

    :sswitch_7
    const/16 v0, 0x30

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    if-nez v0, :cond_c

    move v0, v1

    :goto_8
    add-int/2addr v2, v0

    new-array v2, v2, [I

    if-eqz v0, :cond_b

    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->f:[I

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_b
    :goto_9
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_d

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_9

    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v0, v0

    goto :goto_8

    :cond_d
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v3

    aput v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    goto/16 :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->o()I

    move-result v2

    move v0, v1

    :goto_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->m()I

    move-result v4

    if-lez v4, :cond_e

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_a

    :cond_e
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->e(I)V

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    if-nez v2, :cond_10

    move v2, v1

    :goto_b
    add-int/2addr v0, v2

    new-array v0, v0, [I

    if-eqz v2, :cond_f

    iget-object v4, p0, Lcom/google/android/gms/icing/ap;->f:[I

    invoke-static {v4, v1, v0, v1, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_f
    :goto_c
    array-length v4, v0

    if-ge v2, v4, :cond_11

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v4

    aput v4, v0, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_c

    :cond_10
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v2, v2

    goto :goto_b

    :cond_11
    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->g:[B

    goto/16 :goto_0

    :sswitch_a
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-nez v0, :cond_12

    new-instance v0, Lcom/google/android/gms/icing/at;

    invoke-direct {v0}, Lcom/google/android/gms/icing/at;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    :cond_12
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto/16 :goto_0

    :sswitch_b
    const/16 v0, 0x51

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    if-nez v0, :cond_14

    move v0, v1

    :goto_d
    add-int/2addr v2, v0

    new-array v2, v2, [D

    if-eqz v0, :cond_13

    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->i:[D

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_13
    :goto_e
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_15

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_e

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    goto :goto_d

    :cond_15
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/ap;->i:[D

    goto/16 :goto_0

    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->c(I)I

    move-result v2

    div-int/lit8 v3, v0, 0x8

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    if-nez v0, :cond_17

    move v0, v1

    :goto_f
    add-int/2addr v3, v0

    new-array v3, v3, [D

    if-eqz v0, :cond_16

    iget-object v4, p0, Lcom/google/android/gms/icing/ap;->i:[D

    invoke-static {v4, v1, v3, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_16
    :goto_10
    array-length v4, v3

    if-ge v0, v4, :cond_18

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->l()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->longBitsToDouble(J)D

    move-result-wide v4

    aput-wide v4, v3, v0

    add-int/lit8 v0, v0, 0x1

    goto :goto_10

    :cond_17
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    goto :goto_f

    :cond_18
    iput-object v3, p0, Lcom/google/android/gms/icing/ap;->i:[D

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/a;->d(I)V

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x1a -> :sswitch_4
        0x22 -> :sswitch_5
        0x28 -> :sswitch_6
        0x30 -> :sswitch_7
        0x32 -> :sswitch_8
        0x3a -> :sswitch_9
        0x4a -> :sswitch_a
        0x51 -> :sswitch_b
        0x52 -> :sswitch_c
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6358
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 6359
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 6361
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/ap;->b:I

    if-eqz v0, :cond_1

    .line 6362
    const/4 v0, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/ap;->b:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 6364
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v0, v0

    if-lez v0, :cond_3

    move v0, v1

    move v2, v1

    .line 6366
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v3, v3

    if-ge v0, v3, :cond_2

    .line 6367
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->c:[I

    aget v3, v3, v0

    .line 6368
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 6366
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6371
    :cond_2
    const/16 v0, 0x1a

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 6372
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    move v0, v1

    .line 6373
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 6374
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->c:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->a(I)V

    .line 6373
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 6377
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    array-length v0, v0

    if-lez v0, :cond_5

    move v0, v1

    .line 6378
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 6379
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    aget-object v2, v2, v0

    .line 6380
    if-eqz v2, :cond_4

    .line 6381
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6378
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 6385
    :cond_5
    iget v0, p0, Lcom/google/android/gms/icing/ap;->e:I

    if-eqz v0, :cond_6

    .line 6386
    const/4 v0, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/ap;->e:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 6388
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v0, v0

    if-lez v0, :cond_8

    move v0, v1

    move v2, v1

    .line 6390
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v3, v3

    if-ge v0, v3, :cond_7

    .line 6391
    iget-object v3, p0, Lcom/google/android/gms/icing/ap;->f:[I

    aget v3, v3, v0

    .line 6392
    invoke-static {v3}, Lcom/google/protobuf/nano/b;->b(I)I

    move-result v3

    add-int/2addr v2, v3

    .line 6390
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 6395
    :cond_7
    const/16 v0, 0x32

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 6396
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    move v0, v1

    .line 6397
    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    array-length v2, v2

    if-ge v0, v2, :cond_8

    .line 6398
    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->f:[I

    aget v2, v2, v0

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->a(I)V

    .line 6397
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 6401
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->g:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_9

    .line 6402
    const/4 v0, 0x7

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->g:[B

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 6404
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    if-eqz v0, :cond_a

    .line 6405
    const/16 v0, 0x9

    iget-object v2, p0, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6407
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    if-eqz v0, :cond_b

    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    if-lez v0, :cond_b

    .line 6408
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    mul-int/lit8 v0, v0, 0x8

    .line 6409
    const/16 v2, 0x52

    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 6410
    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/b;->d(I)V

    .line 6411
    :goto_5
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    if-ge v1, v0, :cond_b

    .line 6412
    iget-object v0, p0, Lcom/google/android/gms/icing/ap;->i:[D

    aget-wide v2, v0, v1

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/b;->a(D)V

    .line 6411
    add-int/lit8 v1, v1, 0x1

    goto :goto_5

    .line 6415
    :cond_b
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6416
    return-void
.end method
