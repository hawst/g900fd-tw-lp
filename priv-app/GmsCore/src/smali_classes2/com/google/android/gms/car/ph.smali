.class public final Lcom/google/android/gms/car/ph;
.super Lcom/google/android/car/fsm/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/pg;


# instance fields
.field private b:Z

.field private c:Z

.field private d:Lcom/google/android/gms/car/ea;

.field private e:Lcom/google/android/gms/car/ht;

.field private f:Lcom/google/android/gms/car/id;

.field private g:Lcom/google/android/gms/car/qc;

.field private h:Lcom/google/android/gms/car/b;

.field private i:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/car/fsm/g;-><init>()V

    .line 245
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ph;)Lcom/google/android/gms/car/ea;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->d:Lcom/google/android/gms/car/ea;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/ht;)Lcom/google/android/gms/car/ht;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/id;)Lcom/google/android/gms/car/id;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/car/ph;->f:Lcom/google/android/gms/car/id;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/qc;)Lcom/google/android/gms/car/qc;
    .locals 0

    .prologue
    .line 19
    iput-object p1, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    return-object p1
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 238
    if-nez p1, :cond_0

    .line 239
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_CAR_PARKED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 243
    :goto_0
    return-void

    .line 241
    :cond_0
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_CAR_STARTED_MOVING"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ph;I)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ph;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/ph;)Lcom/google/android/gms/car/ht;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    return-object v0
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 219
    iget-object v0, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    new-instance v1, Lcom/google/android/gms/car/pl;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/pl;-><init>(Lcom/google/android/gms/car/ph;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Lcom/google/android/car/fsm/e;)V

    .line 235
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/ph;)Z
    .locals 1

    .prologue
    .line 19
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->i:Z

    return v0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 119
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->i:Z

    if-eqz v0, :cond_0

    .line 125
    :goto_0
    return-void

    .line 123
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    iget-object v0, v0, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->r(Lcom/google/android/gms/car/gx;)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ph;->a(I)V

    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ph;->i:Z

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->f:Lcom/google/android/gms/car/id;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/id;->b(I)V

    .line 171
    if-eqz p1, :cond_0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    iget-object v1, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/qc;->b:Lcom/google/android/gms/car/CarInfoInternal;

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ia;->b(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/car/qc;->e:Z

    .line 176
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    iget-object v1, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/car/qc;->b:Lcom/google/android/gms/car/CarInfoInternal;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/ia;->d(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/qc;->a(Z)V

    .line 206
    return-void
.end method

.method public final b()Z
    .locals 3

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    const-string v1, "gearhead:is_device_country_whitelisted"

    iget-object v0, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 134
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    const-string v1, "device_country"

    iget-object v0, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 3

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    const-string v1, "gearhead:is_phone_blacklisted"

    iget-object v0, v0, Lcom/google/android/gms/car/qc;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 144
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "CAR.SETUP"

    const-string v1, "interrupting setup on user rejection or an error"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->c:Z

    if-nez v0, :cond_1

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ht;->a()V

    .line 149
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ph;->c:Z

    .line 151
    :cond_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ph;->c(Z)V

    .line 152
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    iget-boolean v0, v0, Lcom/google/android/gms/car/qc;->e:Z

    return v0
.end method

.method public final g()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    iget-object v1, v1, Lcom/google/android/gms/car/qc;->c:Lcom/google/android/gms/car/id;

    const-string v2, "car_only_connect_to_known_cars"

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/id;->a(Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/car/ph;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/b;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method public final i()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->h:Lcom/google/android/gms/car/b;

    invoke-virtual {v0}, Lcom/google/android/gms/car/b;->a()Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final j()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "extra_should_ask_bluetooth_pairing"

    iget-boolean v0, v0, Lcom/google/android/gms/car/qc;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    new-instance v0, Landroid/content/ComponentName;

    const-string v2, "com.google.android.projection.gearhead"

    const-string v3, "com.google.android.projection.gearhead.frx.SetupActivity"

    invoke-direct {v0, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    return-object v1

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()V
    .locals 2

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->h:Lcom/google/android/gms/car/b;

    iget-object v1, p0, Lcom/google/android/car/fsm/g;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/b;->a(Lcom/google/android/car/fsm/FsmController;)V

    .line 196
    return-void
.end method

.method public final l()Z
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->g:Lcom/google/android/gms/car/qc;

    iget-boolean v0, v0, Lcom/google/android/gms/car/qc;->g:Z

    return v0
.end method

.method public final m()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 210
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->c:Z

    if-nez v0, :cond_1

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    const-string v1, "CAR.SERVICE"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.SERVICE"

    const-string v2, "onSetupSuccess"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v1}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v1

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/gms/car/qc;->d:Z

    iget-object v0, v0, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->m(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/pc;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/pc;->a()V

    .line 212
    iput-boolean v3, p0, Lcom/google/android/gms/car/ph;->c:Z

    .line 214
    :cond_1
    iput-boolean v3, p0, Lcom/google/android/gms/car/ph;->c:Z

    .line 215
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/ph;->c(Z)V

    .line 216
    return-void
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 42
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-string v0, "CAR.SETUP"

    const-string v1, "onActivityCreated"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 45
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/car/fsm/g;->onActivityCreated(Landroid/os/Bundle;)V

    .line 47
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->b:Z

    if-eqz v0, :cond_1

    .line 96
    :goto_0
    return-void

    .line 50
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ph;->b:Z

    .line 52
    new-instance v0, Lcom/google/android/gms/car/ea;

    invoke-virtual {p0}, Lcom/google/android/gms/car/ph;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/car/pi;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/pi;-><init>(Lcom/google/android/gms/car/ph;)V

    new-instance v4, Lcom/google/android/gms/car/pj;

    invoke-direct {v4, p0}, Lcom/google/android/gms/car/pj;-><init>(Lcom/google/android/gms/car/ph;)V

    new-instance v5, Lcom/google/android/gms/car/pk;

    invoke-direct {v5, p0}, Lcom/google/android/gms/car/pk;-><init>(Lcom/google/android/gms/car/ph;)V

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ea;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/t;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ph;->d:Lcom/google/android/gms/car/ea;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->d:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->a()V

    goto :goto_0
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 1

    .prologue
    .line 36
    invoke-super {p0, p1}, Lcom/google/android/car/fsm/g;->onAttach(Landroid/app/Activity;)V

    .line 37
    new-instance v0, Lcom/google/android/gms/car/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/b;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ph;->h:Lcom/google/android/gms/car/b;

    .line 38
    return-void
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 100
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 101
    const-string v0, "CAR.SETUP"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ph;->c:Z

    if-nez v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->e:Lcom/google/android/gms/car/ht;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ht;->a()V

    .line 105
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ph;->c:Z

    .line 108
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->d:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->c_()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/car/ph;->d:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->b()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :cond_2
    :goto_0
    invoke-super {p0}, Lcom/google/android/car/fsm/g;->onDestroy()V

    .line 115
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
