.class final Lcom/google/android/gms/games/service/w;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;

.field private final c:Z

.field private final d:Lcom/google/android/gms/games/internal/dx;

.field private e:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZLcom/google/android/gms/games/internal/dx;)V
    .locals 1

    .prologue
    .line 323
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 324
    iput-object p1, p0, Lcom/google/android/gms/games/service/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 325
    iput-object p2, p0, Lcom/google/android/gms/games/service/w;->b:Ljava/lang/String;

    .line 326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/service/w;->c:Z

    .line 327
    iput-object p4, p0, Lcom/google/android/gms/games/service/w;->d:Lcom/google/android/gms/games/internal/dx;

    .line 328
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->d:Lcom/google/android/gms/games/internal/dx;

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/dx;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 395
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 395
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 396
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 395
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v1, :cond_1

    .line 396
    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_1
    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 336
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 337
    new-instance v0, Lcom/google/android/gms/games/a/av;

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/w;->c:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->g:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 347
    :goto_0
    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1

    .line 384
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    return v0

    .line 341
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/games/a/av;

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/w;->c:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/service/w;->b:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v0

    goto :goto_0

    .line 348
    :catch_0
    move-exception v0

    instance-of v0, v0, Lcom/google/android/gms/auth/ae;

    if-eqz v0, :cond_1

    .line 353
    const/16 v0, 0x3e9

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1

    .line 357
    :cond_1
    const/16 v0, 0x3e8

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1

    .line 359
    :catch_1
    move-exception v0

    .line 360
    const-string v1, "SignInIntentService"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 365
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->a()I

    move-result v1

    .line 366
    const/16 v2, 0x3ea

    if-ne v1, v2, :cond_2

    .line 368
    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1

    .line 369
    :cond_2
    const/16 v2, 0x5dc

    if-ne v1, v2, :cond_3

    .line 373
    iget-object v0, p0, Lcom/google/android/gms/games/service/w;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/games/k/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/t;)V

    .line 374
    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1

    .line 375
    :cond_3
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_4

    .line 377
    invoke-virtual {p2, p1}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;)V

    .line 378
    :cond_4
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/w;->e:Lcom/google/android/gms/common/data/DataHolder;

    goto :goto_1
.end method
