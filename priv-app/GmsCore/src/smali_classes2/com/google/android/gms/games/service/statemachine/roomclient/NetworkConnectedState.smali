.class final Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:J

.field private j:Lcom/google/android/gms/common/server/ClientContext;

.field private k:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 47
    return-void
.end method


# virtual methods
.method public final a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 1

    .prologue
    .line 54
    iput-wide p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->e:J

    .line 55
    iput-object p3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->j:Lcom/google/android/gms/common/server/ClientContext;

    .line 56
    iput-object p4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->k:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c()V

    .line 58
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 6

    .prologue
    .line 67
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 130
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->g:Z

    :goto_0
    return v0

    .line 69
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->b()V

    .line 71
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 75
    :sswitch_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/ed;->c()V

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 80
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    .line 78
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 82
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;

    .line 83
    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->e:J

    iget-wide v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;->b:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 86
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 89
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;

    .line 90
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->j:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 95
    :cond_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto :goto_0

    .line 98
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 99
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 101
    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/bx;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/bx;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;Lcom/google/android/gms/games/service/statemachine/roomclient/ag;)V

    .line 113
    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->e:J

    iget-wide v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    .line 115
    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v3, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v4

    iput-wide v4, v3, Lcom/google/android/gms/games/g/ag;->n:J

    .line 116
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->k:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v0, v2, v1, v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/bz;Lcom/google/android/gms/games/service/g;Lcom/google/android/gms/games/internal/ConnectionInfo;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->g:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->k:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitOneupEnterRoomState;->a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 119
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto/16 :goto_0

    .line 123
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 124
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/g/ag;->y:Z

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 127
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->f:Z

    goto/16 :goto_0

    .line 67
    nop

    :sswitch_data_0
    .sparse-switch
        0x17 -> :sswitch_3
        0x1c -> :sswitch_2
        0x1d -> :sswitch_4
        0x1e -> :sswitch_1
        0x27 -> :sswitch_0
        0x29 -> :sswitch_1
    .end sparse-switch
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 62
    const/16 v0, 0x1e

    const/16 v1, 0x3c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(IILjava/util/concurrent/TimeUnit;)V

    .line 63
    return-void
.end method
