.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;
.super Lcom/google/android/gms/games/realtime/network/s;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/s;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    .line 135
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 143
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 156
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->g:Z

    :goto_0
    return v0

    .line 145
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/k;

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/k;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->c(Ljava/lang/String;)V

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->c:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->b()V

    .line 148
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->f:Z

    goto :goto_0

    .line 151
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a()V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/service/statemachine/d;->b()V

    .line 153
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->f:Z

    goto :goto_0

    .line 143
    :pswitch_data_0
    .packed-switch 0x1771
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->c()V

    .line 139
    return-void
.end method
