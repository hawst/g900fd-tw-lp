.class final Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->a:Ljava/lang/String;

    .line 30
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->c()V

    .line 31
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 35
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 57
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->f:Z

    :goto_0
    return v0

    .line 37
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/f;

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    new-instance v2, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/f;->a:Ljava/lang/String;

    iget v4, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/f;->b:I

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/internal/ConnectionInfo;-><init>(Ljava/lang/String;I)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/internal/eg;->a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 41
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->f:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/f;->a:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->f:Z

    goto :goto_0

    .line 42
    :catch_0
    move-exception v0

    .line 43
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 48
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/eg;->a(Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->b()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 53
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->f:Z

    goto :goto_0

    .line 50
    :catch_1
    move-exception v0

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 35
    nop

    :pswitch_data_0
    .packed-switch 0x10
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
