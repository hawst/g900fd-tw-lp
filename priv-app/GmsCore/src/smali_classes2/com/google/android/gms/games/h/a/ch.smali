.class public final Lcom/google/android/gms/games/h/a/ch;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 56
    sput-object v0, Lcom/google/android/gms/games/h/a/ch;->b:Ljava/util/HashMap;

    const-string v1, "participantId"

    const-string v2, "participantId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 57
    sget-object v0, Lcom/google/android/gms/games/h/a/ch;->b:Ljava/util/HashMap;

    const-string v1, "placing"

    const-string v2, "placing"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    sget-object v0, Lcom/google/android/gms/games/h/a/ch;->b:Ljava/util/HashMap;

    const-string v1, "result"

    const-string v2, "result"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 59
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 73
    if-eqz p1, :cond_0

    .line 74
    const-string v0, "participantId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/ch;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :cond_0
    if-eqz p2, :cond_1

    .line 77
    const-string v0, "placing"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/ch;->a(Ljava/lang/String;I)V

    .line 79
    :cond_1
    if-eqz p3, :cond_2

    .line 80
    const-string v0, "result"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/ch;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 82
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 63
    sget-object v0, Lcom/google/android/gms/games/h/a/ch;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "participantId"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "placing"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "result"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
