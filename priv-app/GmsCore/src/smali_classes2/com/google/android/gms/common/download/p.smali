.class final Lcom/google/android/gms/common/download/p;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 69
    iput-object p1, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 70
    iput-object p2, p0, Lcom/google/android/gms/common/download/p;->b:Landroid/content/Context;

    .line 71
    return-void
.end method

.method private varargs a()Landroid/database/Cursor;
    .locals 13

    .prologue
    .line 75
    new-instance v6, Landroid/database/MatrixCursor;

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e()[Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v0}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 77
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-wide/16 v4, 0x1

    const-wide/16 v2, 0x2

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v0, v1

    const/4 v1, 0x1

    const-string v4, "Enabled"

    aput-object v4, v0, v1

    const/4 v1, 0x2

    const-string v4, ""

    aput-object v4, v0, v1

    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/common/download/p;->b:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;)[Lcom/google/android/gms/common/download/f;

    move-result-object v1

    .line 79
    if-eqz v1, :cond_0

    .line 80
    array-length v7, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v7, :cond_0

    aget-object v8, v1, v0

    .line 81
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v8, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " downloaded:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    iget-object v9, v8, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {v5, v9}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 83
    const/4 v4, 0x3

    new-array v10, v4, [Ljava/lang/Object;

    const/4 v11, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v10, v11

    const/4 v2, 0x1

    aput-object v9, v10, v2

    const/4 v2, 0x2

    iget-object v3, v8, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    aput-object v3, v10, v2

    invoke-virtual {v6, v10}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 80
    add-int/lit8 v0, v0, 0x1

    move-wide v2, v4

    goto :goto_0

    .line 90
    :cond_0
    :try_start_0
    new-instance v7, Lorg/json/JSONArray;

    sget-object v0, Lcom/google/android/gms/common/download/a/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v7, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 91
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v0

    if-lez v0, :cond_1

    .line 92
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    const/4 v1, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    :try_start_1
    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "Gservices Overrides"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 93
    const/4 v0, 0x0

    move v12, v0

    move-wide v0, v4

    move v4, v12

    :goto_1
    :try_start_2
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v2

    if-ge v4, v2, :cond_2

    .line 94
    new-instance v5, Lcom/google/android/gms/common/download/f;

    invoke-virtual {v7, v4}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    invoke-direct {v5, v2}, Lcom/google/android/gms/common/download/f;-><init>(Lorg/json/JSONObject;)V

    .line 95
    const/4 v2, 0x3

    new-array v8, v2, [Ljava/lang/Object;
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_2

    const/4 v9, 0x0

    const-wide/16 v2, 0x1

    add-long/2addr v2, v0

    :try_start_3
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v8, v9

    const/4 v0, 0x1

    iget-object v1, v5, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    aput-object v1, v8, v0

    const/4 v0, 0x2

    iget-object v1, v5, Lcom/google/android/gms/common/download/f;->b:Ljava/lang/String;

    aput-object v1, v8, v0

    invoke-virtual {v6, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_0

    .line 93
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    move-wide v0, v2

    goto :goto_1

    :cond_1
    move-wide v0, v2

    :cond_2
    move-wide v2, v0

    .line 102
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    const-string v1, "download"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 103
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v4

    .line 105
    if-eqz v4, :cond_3

    :try_start_4
    invoke-interface {v4}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_3

    .line 106
    const/4 v0, 0x3

    new-array v5, v0, [Ljava/lang/Object;

    const/4 v7, 0x0

    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v7

    const/4 v2, 0x1

    const-string v3, "DownloadManager"

    aput-object v3, v5, v2

    const/4 v2, 0x2

    const-string v3, ""

    aput-object v3, v5, v2

    invoke-virtual {v6, v5}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    move-wide v2, v0

    .line 107
    :goto_3
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 108
    const-string v0, "title"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 109
    const-string v0, "status"

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v4, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->a(I)Ljava/lang/String;

    move-result-object v7

    .line 111
    const/4 v0, 0x3

    new-array v8, v0, [Ljava/lang/Object;

    const/4 v9, 0x0

    const-wide/16 v0, 0x1

    add-long/2addr v0, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v8, v9

    const/4 v2, 0x1

    aput-object v5, v8, v2

    const/4 v2, 0x2

    aput-object v7, v8, v2

    invoke-virtual {v6, v8}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-wide v2, v0

    .line 112
    goto :goto_3

    .line 98
    :catch_0
    move-exception v0

    move-object v12, v0

    move-wide v0, v2

    move-object v2, v12

    :goto_4
    invoke-virtual {v2}, Lorg/json/JSONException;->printStackTrace()V

    move-wide v2, v0

    goto :goto_2

    .line 115
    :cond_3
    if-eqz v4, :cond_4

    .line 116
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 120
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->a(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 121
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-wide/16 v4, 0x1

    add-long/2addr v4, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "DownloadStatus"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, ""

    aput-object v2, v0, v1

    invoke-virtual {v6, v0}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 122
    sget-object v0, Lcom/google/android/gms/common/download/a;->d:Lcom/google/android/gms/common/download/e;

    iget-object v1, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->a(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->f()Lcom/google/android/gms/common/download/f;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/common/download/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    const-wide/16 v2, 0x1

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Status;

    .line 124
    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->f()Lcom/google/android/gms/common/download/f;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-virtual {v6, v1}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 128
    :cond_5
    return-object v6

    .line 115
    :catchall_0
    move-exception v0

    if-eqz v4, :cond_6

    .line 116
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v0

    .line 98
    :catch_1
    move-exception v0

    move-object v2, v0

    move-wide v0, v4

    goto :goto_4

    :catch_2
    move-exception v2

    goto :goto_4
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/common/download/p;->a()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x2

    .line 65
    check-cast p1, Landroid/database/Cursor;

    iget-object v0, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    const v1, 0x102000a

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v1

    if-nez v1, :cond_0

    new-instance v1, Landroid/support/v4/widget/ax;

    iget-object v2, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    new-array v3, v6, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e()[Ljava/lang/String;

    move-result-object v5

    aget-object v5, v5, v7

    aput-object v5, v3, v4

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->e()[Ljava/lang/String;

    move-result-object v4

    aget-object v4, v4, v6

    aput-object v4, v3, v7

    new-array v4, v6, [I

    fill-array-data v4, :array_0

    invoke-direct {v1, v2, p1, v3, v4}, Landroid/support/v4/widget/ax;-><init>(Landroid/content/Context;Landroid/database/Cursor;[Ljava/lang/String;[I)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->c(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/download/p;->a:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-static {v1}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->b(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;)Ljava/lang/Runnable;

    move-result-object v1

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void

    :cond_0
    invoke-virtual {v0}, Landroid/widget/ListView;->getAdapter()Landroid/widget/ListAdapter;

    move-result-object v0

    check-cast v0, Landroid/support/v4/widget/f;

    invoke-virtual {v0, p1}, Landroid/support/v4/widget/f;->a(Landroid/database/Cursor;)V

    goto :goto_0

    nop

    :array_0
    .array-data 4
        0x1020014
        0x1020015
    .end array-data
.end method
