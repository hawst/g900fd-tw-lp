.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/r;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 1

    .prologue
    .line 26
    const/16 v0, 0xa

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;I)V

    .line 27
    return-void
.end method


# virtual methods
.method public final b()V
    .locals 0

    .prologue
    .line 33
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->c()V

    .line 34
    return-void
.end method

.method public final b(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 38
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 47
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->g:Z

    :goto_0
    return v0

    .line 40
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b()V

    .line 41
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->f:Z

    goto :goto_0

    .line 44
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 45
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitNetworkDisconnectState;->f:Z

    goto :goto_0

    .line 38
    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_1
        0x27 -> :sswitch_0
    .end sparse-switch
.end method
