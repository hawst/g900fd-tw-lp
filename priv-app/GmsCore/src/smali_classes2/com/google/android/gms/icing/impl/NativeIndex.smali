.class public Lcom/google/android/gms/icing/impl/NativeIndex;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Z


# instance fields
.field private b:J


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 611
    :try_start_0
    const-string v0, "AppDataSearch"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 614
    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetVersionCode()I

    move-result v0

    div-int/lit16 v0, v0, 0x3e8

    .line 615
    const/16 v1, 0x1a79

    if-eq v0, v1, :cond_0

    .line 617
    new-instance v1, Ljava/lang/UnsatisfiedLinkError;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Version mismatch: lib: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " vs apk: 6777"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/UnsatisfiedLinkError;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/LinkageError; {:try_start_0 .. :try_end_0} :catch_0

    .line 622
    :catch_0
    move-exception v0

    .line 623
    const-string v1, "Native load error: %s"

    invoke-virtual {v0}, Ljava/lang/Throwable;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 624
    const/4 v0, 0x0

    .line 626
    :goto_0
    sput-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    .line 627
    return-void

    .line 621
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private constructor <init>(Ljava/io/File;Lcom/google/android/gms/icing/af;)V
    .locals 2

    .prologue
    .line 109
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 112
    invoke-virtual {p1}, Ljava/io/File;->mkdir()Z

    .line 114
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p2, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    .line 115
    invoke-virtual {p1}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v0

    .line 116
    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCreate([B[B)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    .line 117
    return-void
.end method

.method public static a(D)I
    .locals 2

    .prologue
    .line 62
    const-wide v0, 0x406fe00000000000L    # 255.0

    mul-double/2addr v0, p0

    .line 63
    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    return v0
.end method

.method public static a(Ljava/io/File;)J
    .locals 6

    .prologue
    const-wide/16 v0, -0x1

    .line 332
    sget-boolean v2, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v2, :cond_0

    .line 339
    :goto_0
    return-wide v0

    .line 335
    :cond_0
    :try_start_0
    const-string v2, "getDiskUsage: %s"

    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 336
    invoke-virtual {p0}, Ljava/io/File;->getCanonicalPath()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDiskUsage([B)J
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    goto :goto_0

    .line 337
    :catch_0
    move-exception v2

    const-string v3, "Bad path: %s"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p0, v4, v5

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static a(Ljava/io/File;Lcom/google/android/gms/icing/af;)Lcom/google/android/gms/icing/impl/NativeIndex;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 98
    sget-boolean v1, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v1, :cond_0

    .line 104
    :goto_0
    return-object v0

    .line 101
    :cond_0
    :try_start_0
    new-instance v1, Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;-><init>(Ljava/io/File;Lcom/google/android/gms/icing/af;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v0, v1

    goto :goto_0

    .line 102
    :catch_0
    move-exception v1

    .line 103
    const-string v2, "Error creating native index: %s"

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static a([B)Lcom/google/android/gms/icing/r;
    .locals 3

    .prologue
    .line 143
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/icing/r;->a([B)Lcom/google/android/gms/icing/r;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 147
    :goto_0
    return-object v0

    .line 144
    :catch_0
    move-exception v0

    const-string v1, "Failed parsing document store status"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 147
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a()V
    .locals 1

    .prologue
    .line 76
    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    .line 79
    :goto_0
    return-void

    .line 78
    :cond_0
    invoke-static {}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeUnloadExtension()V

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 70
    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 72
    :goto_0
    return v0

    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v0

    const v1, 0x6768a8

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeLoadExtension([BI)Z

    move-result v0

    goto :goto_0
.end method

.method private static b([B)Lcom/google/android/gms/icing/ax;
    .locals 3

    .prologue
    .line 151
    if-nez p0, :cond_0

    .line 152
    new-instance v0, Lcom/google/android/gms/icing/ax;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ax;-><init>()V

    .line 159
    :goto_0
    return-object v0

    .line 155
    :cond_0
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/icing/ax;->a([B)Lcom/google/android/gms/icing/ax;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 156
    :catch_0
    move-exception v0

    const-string v1, "Failed parsing suggestions"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 159
    new-instance v0, Lcom/google/android/gms/icing/ax;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ax;-><init>()V

    goto :goto_0
.end method

.method public static b(Ljava/lang/String;)Lcom/google/android/gms/icing/b;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 82
    sget-boolean v1, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-object v0

    .line 84
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetExtensionInfo([B)[B

    move-result-object v1

    .line 85
    if-eqz v1, :cond_0

    .line 89
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/icing/b;->a([B)Lcom/google/android/gms/icing/b;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 90
    :catch_0
    move-exception v1

    const-string v2, "Failed parsing extension info"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public static b(I)V
    .locals 1

    .prologue
    .line 213
    sget-boolean v0, Lcom/google/android/gms/icing/impl/NativeIndex;->a:Z

    if-nez v0, :cond_0

    .line 216
    :goto_0
    return-void

    .line 215
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSetLogPriority(I)V

    goto :goto_0
.end method

.method private static c([B)Lcom/google/android/gms/icing/ap;
    .locals 3

    .prologue
    .line 164
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/icing/ap;->a([B)Lcom/google/android/gms/icing/ap;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 168
    :goto_0
    return-object v0

    .line 165
    :catch_0
    move-exception v0

    const-string v1, "Failed parsing query response"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 168
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static c(Ljava/lang/String;)[B
    .locals 2

    .prologue
    .line 289
    :try_start_0
    const-string v0, "UTF-8"

    invoke-virtual {p0, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 291
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "UTF-8 not supported"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private static d([B)Ljava/lang/String;
    .locals 3

    .prologue
    .line 297
    :try_start_0
    new-instance v0, Ljava/lang/String;

    const-string v1, "UTF-8"

    invoke-direct {v0, p0, v1}, Ljava/lang/String;-><init>([BLjava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 304
    :goto_0
    return-object v0

    .line 300
    :catch_0
    move-exception v0

    const-string v1, "Can\'t convert byte array to String"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 302
    const-string v0, ""

    goto :goto_0
.end method

.method public static e(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 506
    sparse-switch p0, :sswitch_data_0

    .line 519
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "error internal "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 508
    :sswitch_0
    const-string v0, "ok"

    goto :goto_0

    .line 511
    :sswitch_1
    const-string v0, "ok trimmed"

    goto :goto_0

    .line 513
    :sswitch_2
    const-string v0, "ok duplicate uri replaced"

    goto :goto_0

    .line 515
    :sswitch_3
    const-string v0, "error uri not found"

    goto :goto_0

    .line 517
    :sswitch_4
    const-string v0, "error i/o"

    goto :goto_0

    .line 506
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x14 -> :sswitch_3
        0x15 -> :sswitch_4
    .end sparse-switch
.end method

.method private native nativeAddCorpus(JJI[B)Z
.end method

.method private native nativeAddPendingDeleteUsageReport(JJ[BII)V
.end method

.method private native nativeAddUsageReport(J[B)V
.end method

.method private native nativeAdvanceLastSeqno(JIJ)V
.end method

.method private native nativeApplyPendingDeleteUsageReports(J)Z
.end method

.method private native nativeClear(J)Z
.end method

.method private native nativeClearUsageReportData(J)Z
.end method

.method private native nativeClearUsageReportDataForId(JJ)Z
.end method

.method private native nativeCompact(JDI[J[I[I)Z
.end method

.method private native nativeCreate([B[B)J
.end method

.method private native nativeDeleteCorpus(JI)Z
.end method

.method private native nativeDeleteDocument(JJI[B)I
.end method

.method private native nativeDestroy(J)V
.end method

.method private native nativeExecuteQuery(J[B[BIII)[B
.end method

.method private native nativeFlush(J)V
.end method

.method private native nativeGetCompactStatus(J)[B
.end method

.method private native nativeGetDebugInfo(JI)[B
.end method

.method private static native nativeGetDiskUsage([B)J
.end method

.method private native nativeGetDocuments(J[[B[B)[B
.end method

.method static native nativeGetExtensionInfo([B)[B
.end method

.method private native nativeGetIMEUpdates(JJII[[B)[B
.end method

.method private native nativeGetLastKDocuments(JI[I)[[B
.end method

.method private native nativeGetPhraseAffinityScores(J[B)[I
.end method

.method private native nativeGetStatus(JZ)[B
.end method

.method private native nativeGetUsageReports(JJJJI[J)[B
.end method

.method private native nativeGetUsageStats(J)[B
.end method

.method private static native nativeGetVersionCode()I
.end method

.method private native nativeIndexDocument(JJ[B[I)V
.end method

.method private native nativeInit(J)[B
.end method

.method private native nativeIsIndexEmpty(J)Z
.end method

.method static native nativeLoadExtension([BI)Z
.end method

.method private native nativeMinFreeFraction(J)D
.end method

.method private native nativeNumDocuments(J)I
.end method

.method private native nativeNumPostingLists(J)I
.end method

.method private native nativeOnMaintenance(JZ)V
.end method

.method private native nativeOnSleep(J)V
.end method

.method private native nativeRebuildIndex(J)Z
.end method

.method private native nativeRestoreIndex(J)Z
.end method

.method private static native nativeSetLogPriority(I)V
.end method

.method private native nativeSuggest(J[B[II[B)[B
.end method

.method private native nativeTagDocument(JJI[B[BIZ)I
.end method

.method static native nativeUnloadExtension()V
.end method

.method private native nativeUpdateNativeConfig(J[B)Z
.end method

.method private native nativeUpgrade(JII)Z
.end method


# virtual methods
.method public final a(JILjava/lang/String;)I
    .locals 9

    .prologue
    .line 309
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v7

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteDocument(JJI[B)I

    move-result v0

    return v0
.end method

.method public final a(JILjava/lang/String;Ljava/lang/String;Z)I
    .locals 11

    .prologue
    .line 359
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p4}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v7

    invoke-static/range {p5 .. p5}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v8

    const v9, 0xfffe

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    move/from16 v10, p6

    invoke-direct/range {v1 .. v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeTagDocument(JJI[B[BIZ)I

    move-result v0

    return v0
.end method

.method public final a(JLcom/google/android/gms/icing/o;)I
    .locals 9

    .prologue
    .line 281
    const/4 v0, 0x1

    new-array v7, v0, [I

    .line 282
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v6

    move-object v1, p0

    move-wide v4, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeIndexDocument(JJ[B[I)V

    .line 283
    const/4 v0, 0x0

    aget v0, v7, v0

    return v0
.end method

.method public final a(IZ)J
    .locals 2

    .prologue
    .line 255
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetStatus(JZ)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lcom/google/android/gms/icing/r;

    move-result-object v0

    .line 257
    iget-object v1, v0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v1, v1

    if-ge p1, v1, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    aget-object v0, v0, p1

    iget-wide v0, v0, Lcom/google/android/gms/icing/s;->a:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/aj;II)Lcom/google/android/gms/icing/ap;
    .locals 9

    .prologue
    .line 444
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v4

    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v5

    const v6, 0x186a0

    move-object v1, p0

    move v7, p3

    move v8, p4

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeExecuteQuery(J[B[BIII)[B

    move-result-object v0

    .line 447
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c([B)Lcom/google/android/gms/icing/ap;

    move-result-object v0

    return-object v0
.end method

.method public final a([Ljava/lang/String;Lcom/google/android/gms/icing/ak;)Lcom/google/android/gms/icing/ap;
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 459
    array-length v1, p1

    new-array v3, v1, [[B

    .line 461
    array-length v4, p1

    move v1, v0

    :goto_0
    if-ge v0, v4, :cond_0

    aget-object v5, p1, v0

    .line 462
    add-int/lit8 v2, v1, 0x1

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v5

    aput-object v5, v3, v1

    .line 461
    add-int/lit8 v0, v0, 0x1

    move v1, v2

    goto :goto_0

    .line 465
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p2}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v3, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDocuments(J[[B[B)[B

    move-result-object v0

    .line 467
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->c([B)Lcom/google/android/gms/icing/ap;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;[II)Lcom/google/android/gms/icing/ax;
    .locals 8

    .prologue
    .line 452
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v4

    const/4 v7, 0x0

    move-object v1, p0

    move-object v5, p2

    move v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeSuggest(J[B[II[B)[B

    move-result-object v0

    .line 454
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->b([B)Lcom/google/android/gms/icing/ax;

    move-result-object v0

    return-object v0
.end method

.method public final a(JJJ[J)Lcom/google/android/gms/icing/bg;
    .locals 13

    .prologue
    .line 475
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const/16 v10, 0xa

    move-object v1, p0

    move-wide v4, p1

    move-wide/from16 v6, p3

    move-wide/from16 v8, p5

    move-object/from16 v11, p7

    invoke-direct/range {v1 .. v11}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetUsageReports(JJJJI[J)[B

    move-result-object v0

    .line 478
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/icing/bg;->a([B)Lcom/google/android/gms/icing/bg;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 481
    :goto_0
    return-object v0

    .line 479
    :catch_0
    move-exception v0

    const-string v1, "Failed to parse usage reports response."

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 481
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(JII[Lcom/google/android/gms/icing/x;)Lcom/google/android/gms/icing/z;
    .locals 13

    .prologue
    const/4 v11, 0x0

    .line 365
    move-object/from16 v0, p5

    array-length v2, v0

    new-array v10, v2, [[B

    .line 367
    move-object/from16 v0, p5

    array-length v5, v0

    move v2, v11

    move v3, v11

    :goto_0
    if-ge v2, v5, :cond_0

    aget-object v6, p5, v2

    .line 368
    add-int/lit8 v4, v3, 0x1

    invoke-static {v6}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v6

    aput-object v6, v10, v3

    .line 367
    add-int/lit8 v2, v2, 0x1

    move v3, v4

    goto :goto_0

    .line 370
    :cond_0
    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v3, p0

    move-wide v6, p1

    move/from16 v8, p3

    move/from16 v9, p4

    invoke-direct/range {v3 .. v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetIMEUpdates(JJII[[B)[B

    move-result-object v2

    .line 374
    :try_start_0
    invoke-static {v2}, Lcom/google/android/gms/icing/z;->a([B)Lcom/google/android/gms/icing/z;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 377
    :goto_1
    return-object v2

    .line 375
    :catch_0
    move-exception v2

    const-string v3, "Failed parsing ime update response"

    new-array v4, v11, [Ljava/lang/Object;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 377
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public final a(IJ)V
    .locals 6

    .prologue
    .line 261
    iget-wide v1, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v0, p0

    move v3, p1

    move-wide v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAdvanceLastSeqno(JIJ)V

    .line 262
    return-void
.end method

.method public final a(JII)V
    .locals 7

    .prologue
    .line 401
    const-string v4, ""

    move-object v1, p0

    move-wide v2, p1

    move v5, p3

    move v6, p4

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JLjava/lang/String;II)V

    .line 403
    return-void
.end method

.method public final a(JLjava/lang/String;II)V
    .locals 9

    .prologue
    .line 395
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p3}, Lcom/google/android/gms/icing/impl/NativeIndex;->c(Ljava/lang/String;)[B

    move-result-object v6

    move-object v1, p0

    move-wide v4, p1

    move v7, p4

    move v8, p5

    invoke-direct/range {v1 .. v8}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAddPendingDeleteUsageReport(JJ[BII)V

    .line 397
    return-void
.end method

.method public final a(Lcom/google/android/gms/icing/ag;)V
    .locals 3

    .prologue
    .line 382
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAddUsageReport(J[B)V

    .line 383
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 436
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeOnMaintenance(JZ)V

    .line 437
    return-void
.end method

.method public final a(DI[J[I[I)Z
    .locals 11

    .prologue
    .line 232
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    move-object v7, p4

    move-object/from16 v8, p5

    move-object/from16 v9, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JDI[J[I[I)Z

    move-result v0

    return v0
.end method

.method public final a(I)Z
    .locals 3

    .prologue
    .line 120
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const/16 v2, 0x35

    invoke-direct {p0, v0, v1, p1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeUpgrade(JII)Z

    move-result v0

    return v0
.end method

.method public final a(I[I)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    .line 224
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const-wide/16 v4, 0x0

    move-object v1, p0

    move v6, p1

    move-object v8, v7

    move-object v9, p2

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeCompact(JDI[J[I[I)Z

    move-result v0

    return v0
.end method

.method public final a(J)Z
    .locals 3

    .prologue
    .line 390
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1, p2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClearUsageReportDataForId(JJ)Z

    move-result v0

    return v0
.end method

.method public final a(JILcom/google/android/gms/icing/aa;)Z
    .locals 9

    .prologue
    .line 348
    invoke-static {p4}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v7

    .line 349
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    move-object v1, p0

    move-wide v4, p1

    move v6, p3

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeAddCorpus(JJI[B)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/icing/af;)Z
    .locals 3

    .prologue
    .line 138
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeUpdateNativeConfig(J[B)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/icing/ah;)[I
    .locals 3

    .prologue
    .line 486
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetPhraseAffinityScores(J[B)[I

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/icing/ad;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 124
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeInit(J)[B

    move-result-object v1

    .line 125
    if-nez v1, :cond_0

    .line 132
    :goto_0
    return-object v0

    .line 129
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/icing/ad;->a([B)Lcom/google/android/gms/icing/ad;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 130
    :catch_0
    move-exception v1

    const-string v2, "Failed parsing init status"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final b(I[I)[Lcom/google/android/gms/icing/o;
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 416
    iget-wide v4, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v4, v5, p1, p2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetLastKDocuments(JI[I)[[B

    move-result-object v4

    .line 417
    if-nez v4, :cond_0

    move-object v0, v2

    .line 428
    :goto_0
    return-object v0

    .line 419
    :cond_0
    array-length v0, v4

    new-array v3, v0, [Lcom/google/android/gms/icing/o;

    move v0, v1

    .line 420
    :goto_1
    array-length v5, v4

    if-ge v0, v5, :cond_1

    .line 422
    :try_start_0
    aget-object v5, v4, v0

    invoke-static {v5}, Lcom/google/android/gms/icing/o;->a([B)Lcom/google/android/gms/icing/o;

    move-result-object v5

    aput-object v5, v3, v0
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 420
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 423
    :catch_0
    move-exception v0

    const-string v3, "Failed to parse document."

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    move-object v0, v2

    .line 425
    goto :goto_0

    :cond_1
    move-object v0, v3

    .line 428
    goto :goto_0
.end method

.method public final c(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 273
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetDebugInfo(JI)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->d([B)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 172
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeRestoreIndex(J)Z

    move-result v0

    return v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 176
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeRebuildIndex(J)Z

    move-result v0

    return v0
.end method

.method public final d(I)Z
    .locals 2

    .prologue
    .line 353
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDeleteCorpus(JI)Z

    move-result v0

    return v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 180
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 181
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeDestroy(J)V

    .line 183
    :cond_0
    iput-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    .line 184
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 193
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClear(J)Z

    move-result v0

    return v0
.end method

.method public finalize()V
    .locals 0

    .prologue
    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/NativeIndex;->e()V

    .line 190
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    .line 197
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeOnSleep(J)V

    .line 198
    return-void
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 201
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeIsIndexEmpty(J)Z

    move-result v0

    return v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 209
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeFlush(J)V

    .line 210
    return-void
.end method

.method public final j()Lcom/google/android/gms/icing/f;
    .locals 3

    .prologue
    .line 237
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetCompactStatus(J)[B

    move-result-object v0

    .line 239
    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/icing/f;->a([B)Lcom/google/android/gms/icing/f;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 243
    :goto_0
    return-object v0

    .line 240
    :catch_0
    move-exception v0

    const-string v1, "Failed parsing compact status"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 243
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final k()Lcom/google/android/gms/icing/r;
    .locals 3

    .prologue
    .line 247
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetStatus(JZ)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lcom/google/android/gms/icing/r;

    move-result-object v0

    return-object v0
.end method

.method public final l()Lcom/google/android/gms/icing/r;
    .locals 3

    .prologue
    .line 251
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetStatus(JZ)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([B)Lcom/google/android/gms/icing/r;

    move-result-object v0

    return-object v0
.end method

.method public final m()I
    .locals 2

    .prologue
    .line 265
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumDocuments(J)I

    move-result v0

    return v0
.end method

.method public final n()I
    .locals 2

    .prologue
    .line 269
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeNumPostingLists(J)I

    move-result v0

    return v0
.end method

.method public final o()D
    .locals 2

    .prologue
    .line 277
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeMinFreeFraction(J)D

    move-result-wide v0

    return-wide v0
.end method

.method public final p()Lcom/google/android/gms/icing/bi;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 313
    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeGetUsageStats(J)[B

    move-result-object v1

    .line 314
    if-nez v1, :cond_0

    .line 321
    :goto_0
    return-object v0

    .line 318
    :cond_0
    :try_start_0
    invoke-static {v1}, Lcom/google/android/gms/icing/bi;->a([B)Lcom/google/android/gms/icing/bi;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 319
    :catch_0
    move-exception v1

    const-string v2, "Failed parsing usage stats"

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method public final q()Z
    .locals 2

    .prologue
    .line 386
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeClearUsageReportData(J)Z

    move-result v0

    return v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 412
    iget-wide v0, p0, Lcom/google/android/gms/icing/impl/NativeIndex;->b:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->nativeApplyPendingDeleteUsageReports(J)Z

    move-result v0

    return v0
.end method
