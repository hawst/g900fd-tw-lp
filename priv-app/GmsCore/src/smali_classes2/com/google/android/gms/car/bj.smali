.class public final Lcom/google/android/gms/car/bj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/bg;

.field private final b:Lcom/google/android/gms/car/kg;

.field private final c:[Lcom/google/android/gms/car/bo;

.field private final d:Ljava/util/LinkedList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/kg;)V
    .locals 1

    .prologue
    .line 734
    iput-object p1, p0, Lcom/google/android/gms/car/bj;->a:Lcom/google/android/gms/car/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 725
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/car/bo;

    iput-object v0, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    .line 731
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    .line 735
    iput-object p2, p0, Lcom/google/android/gms/car/bj;->b:Lcom/google/android/gms/car/kg;

    .line 736
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bj;)Lcom/google/android/gms/car/kg;
    .locals 1

    .prologue
    .line 721
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->b:Lcom/google/android/gms/car/kg;

    return-object v0
.end method

.method private c()V
    .locals 2

    .prologue
    .line 829
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 832
    :cond_1
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 833
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v1, v1, v0

    if-nez v1, :cond_0

    .line 832
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 839
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->a:Lcom/google/android/gms/car/bg;

    invoke-static {v0, p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/bj;)V

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(Lcom/google/android/gms/car/bg;ILcom/google/android/gms/car/nt;)Lcom/google/android/gms/car/bf;
    .locals 3

    .prologue
    .line 796
    monitor-enter p0

    if-eqz p2, :cond_0

    .line 797
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 796
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 799
    :cond_0
    :try_start_1
    new-instance v0, Lcom/google/android/gms/car/bf;

    iget-object v1, p0, Lcom/google/android/gms/car/bj;->a:Lcom/google/android/gms/car/bg;

    invoke-static {v1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bg;)Lcom/google/android/gms/car/gx;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->G()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, p0, p1, p3, v1}, Lcom/google/android/gms/car/bf;-><init>(Lcom/google/android/gms/car/bj;Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/nt;Landroid/content/Context;)V

    .line 801
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    invoke-virtual {v1, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 802
    monitor-exit p0

    return-object v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/bg;IIIII)Lcom/google/android/gms/car/bo;
    .locals 10

    .prologue
    .line 772
    monitor-enter p0

    const/4 v0, 0x3

    if-ne p2, v0, :cond_0

    .line 773
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 772
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 775
    :cond_0
    :try_start_1
    invoke-static {p2}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v8

    .line 776
    const/4 v0, 0x0

    .line 777
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v1, v1, v8

    if-nez v1, :cond_3

    .line 778
    const/4 v0, 0x1

    .line 786
    :cond_1
    :goto_0
    if-eqz v0, :cond_2

    .line 787
    iget-object v9, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    new-instance v0, Lcom/google/android/gms/car/bo;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/car/bo;-><init>(Lcom/google/android/gms/car/bj;Lcom/google/android/gms/car/bg;IIIII)V

    aput-object v0, v9, v8

    .line 791
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v0, v0, v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit p0

    return-object v0

    .line 780
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Lcom/google/android/gms/car/bo;->a()I

    move-result v1

    if-ne v1, p2, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v1, v1, v8

    invoke-virtual {v1}, Lcom/google/android/gms/car/bo;->b()I

    move-result v1

    if-eq v1, p3, :cond_1

    .line 782
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v0, v0, v8

    invoke-virtual {v0}, Lcom/google/android/gms/car/bo;->d()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 783
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized a(I)V
    .locals 3

    .prologue
    .line 807
    monitor-enter p0

    const/4 v0, 0x3

    if-ne p1, v0, :cond_0

    .line 808
    :try_start_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 807
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 810
    :cond_0
    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 811
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v1, v1, v0

    if-eqz v1, :cond_1

    .line 812
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->a:Lcom/google/android/gms/car/bg;

    iget-object v2, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    aget-object v2, v2, v0

    invoke-static {v1, v2}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/bo;)V

    .line 813
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    const/4 v2, 0x0

    aput-object v2, v1, v0

    .line 815
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/car/bj;->c()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 816
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/bf;)V
    .locals 1

    .prologue
    .line 820
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 825
    invoke-direct {p0}, Lcom/google/android/gms/car/bj;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 826
    monitor-exit p0

    return-void

    .line 820
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 746
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->b:Lcom/google/android/gms/car/kg;

    invoke-interface {v1}, Lcom/google/android/gms/car/kg;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 747
    const/4 v0, 0x1

    .line 750
    :goto_0
    monitor-exit p0

    return v0

    .line 746
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 750
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 755
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->b:Lcom/google/android/gms/car/kg;

    invoke-interface {v1}, Lcom/google/android/gms/car/kg;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {v1, p0, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 756
    iget-object v1, p0, Lcom/google/android/gms/car/bj;->c:[Lcom/google/android/gms/car/bo;

    array-length v2, v1

    :goto_0
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 757
    if-eqz v3, :cond_0

    .line 758
    invoke-virtual {v3}, Lcom/google/android/gms/car/bo;->e()V

    .line 756
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 761
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 762
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 763
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bf;

    .line 764
    invoke-virtual {v0}, Lcom/google/android/gms/car/bf;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 755
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 766
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->d:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 767
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized binderDied()V
    .locals 1

    .prologue
    .line 740
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/bj;->b()V

    .line 741
    iget-object v0, p0, Lcom/google/android/gms/car/bj;->a:Lcom/google/android/gms/car/bg;

    invoke-static {v0, p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/bj;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 742
    monitor-exit p0

    return-void

    .line 740
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
