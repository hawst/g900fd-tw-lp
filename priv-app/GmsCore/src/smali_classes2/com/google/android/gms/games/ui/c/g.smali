.class public Lcom/google/android/gms/games/ui/c/g;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final u:Ljava/lang/String;


# instance fields
.field protected final p:Lcom/google/android/gms/games/Player;

.field protected final q:Lcom/google/android/gms/games/Player;

.field protected r:Landroid/content/res/Resources;

.field protected s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

.field protected t:Lcom/google/android/gms/games/a/au;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/google/android/gms/games/ui/c/g;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/ui/c/g;->u:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V
    .locals 7

    .prologue
    .line 79
    const-wide/16 v4, 0xbb8

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/ui/c/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;JZ)V

    .line 80
    iput-object p3, p0, Lcom/google/android/gms/games/ui/c/g;->p:Lcom/google/android/gms/games/Player;

    .line 81
    iput-object p4, p0, Lcom/google/android/gms/games/ui/c/g;->q:Lcom/google/android/gms/games/Player;

    .line 82
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->r:Landroid/content/res/Resources;

    .line 83
    iput-object p1, p0, Lcom/google/android/gms/games/ui/c/g;->t:Lcom/google/android/gms/games/a/au;

    .line 84
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;B)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/games/ui/c/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 64
    invoke-interface {p2}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    const-string v1, "No pre-update level info!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 65
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    const-string v1, "No post-update level info!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 68
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    new-instance v0, Lcom/google/android/gms/games/ui/c/h;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/c/h;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;B)V

    .line 74
    :goto_0
    sget-object v1, Lcom/google/android/gms/games/ui/c/g;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v2, Lcom/google/android/gms/games/ui/c/g;->b:Lcom/google/android/gms/games/ui/c/e;

    invoke-virtual {v2, v5, v0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/c/e;->sendMessage(Landroid/os/Message;)Z

    .line 75
    return-void

    .line 72
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/c/g;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/c/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V

    goto :goto_0
.end method


# virtual methods
.method protected a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->q:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    .line 93
    invoke-static {v2}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->l:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 97
    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/g;->r:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->jK:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v1, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/g;->l:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->py:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 101
    sget v3, Lcom/google/android/gms/p;->jJ:I

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 106
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 107
    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/g;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/g;->q:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/c/g;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget v3, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/c/g;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;I)V

    .line 115
    return-void
.end method

.method protected final d()V
    .locals 5

    .prologue
    .line 130
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/g;->j()Landroid/content/Context;

    move-result-object v0

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/g;->t:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    .line 132
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/g;->t:Lcom/google/android/gms/games/a/au;

    iget-object v2, v2, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 133
    const-string v3, "com.google.android.gms.games.VIEW_PROFILE"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 135
    const-string v3, "com.google.android.gms.games.PLAYER"

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/g;->q:Lcom/google/android/gms/games/Player;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 136
    const-string v3, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 139
    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/ClientUiProxyActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 140
    return-void
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 119
    const/16 v0, 0x1a

    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 124
    const/16 v0, 0x1b

    return v0
.end method
