.class public Lcom/google/android/gms/search/queries/m;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/gms/icing/impl/e/g;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/QueryCall$Request;Lcom/google/android/gms/icing/impl/e/g;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 59
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 61
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/e/g;

    iput-object v0, p0, Lcom/google/android/gms/search/queries/m;->d:Lcom/google/android/gms/icing/impl/e/g;

    .line 62
    iget-object v0, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->a:Ljava/lang/String;

    iget-object v1, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->b:Ljava/lang/String;

    iget-object v2, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->c:[Ljava/lang/String;

    iget v3, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->d:I

    iget v4, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->e:I

    iget-object v5, p3, Lcom/google/android/gms/search/queries/QueryCall$Request;->f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/search/queries/m;->e:Ljava/lang/String;

    .line 64
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 23

    .prologue
    .line 90
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->e:Ljava/lang/String;

    if-eqz v3, :cond_0

    .line 91
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->e:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/aq;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    .line 278
    :goto_0
    return-object v3

    .line 93
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->d:Lcom/google/android/gms/icing/impl/e/g;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/e/g;->a()V

    .line 95
    const-string v3, "Query from %s start %d num %d"

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p2, v4, v5

    const/4 v5, 0x1

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    const/4 v5, 0x2

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 96
    const-string v3, "Query: [%s]"

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 97
    const-string v3, "Icing"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-eqz v3, :cond_2

    .line 98
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    new-array v5, v3, [Ljava/lang/String;

    .line 99
    const/4 v3, 0x0

    move v4, v3

    :goto_1
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v4, v3, :cond_1

    .line 100
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v3, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    aput-object v3, v5, v4

    .line 99
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_1

    .line 102
    :cond_1
    const-string v3, "Query semantic types [%s]"

    const-string v4, ","

    invoke-static {v4, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 104
    :cond_2
    const-string v3, "Index docs: %d pls: %d"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->m()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 107
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v8

    .line 108
    invoke-interface {v8}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    .line 109
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v3, Lcom/google/android/gms/icing/impl/a/aa;

    const/4 v4, 0x1

    move-object/from16 v0, p3

    invoke-interface {v8, v3, v0, v4}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v5

    .line 112
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    move-object/from16 v0, p3

    invoke-static {v5, v0}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 113
    if-eqz v3, :cond_3

    .line 114
    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/aq;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 279
    :catchall_0
    move-exception v3

    monitor-exit v10

    throw v3

    .line 116
    :cond_3
    :try_start_1
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-nez v3, :cond_4

    .line 117
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v3

    .line 118
    if-eqz v3, :cond_4

    .line 119
    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/aq;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v3

    monitor-exit v10

    goto/16 :goto_0

    .line 124
    :cond_4
    new-instance v9, Lcom/google/android/gms/icing/aj;

    invoke-direct {v9}, Lcom/google/android/gms/icing/aj;-><init>()V

    .line 126
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->b:Z

    iput-boolean v3, v9, Lcom/google/android/gms/icing/aj;->d:Z

    .line 128
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->e:Z

    iput-boolean v3, v9, Lcom/google/android/gms/icing/aj;->h:Z

    .line 130
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->c:Lcom/google/android/gms/icing/impl/a/h;

    iget-boolean v3, v3, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->f:I

    :goto_2
    iput v3, v9, Lcom/google/android/gms/icing/aj;->j:I

    .line 131
    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/bp;->a(I)I

    move-result v3

    iput v3, v9, Lcom/google/android/gms/icing/aj;->k:I

    .line 133
    move-object/from16 v0, p6

    iget v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->i:I

    move-object/from16 v0, p6

    iget v4, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/impl/bp;->a(II)I

    move-result v3

    iput v3, v9, Lcom/google/android/gms/icing/aj;->o:I

    .line 136
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    iput-boolean v3, v9, Lcom/google/android/gms/icing/aj;->l:Z

    .line 137
    iget v3, v9, Lcom/google/android/gms/icing/aj;->j:I

    iput v3, v9, Lcom/google/android/gms/icing/aj;->f:I

    .line 142
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-nez v3, :cond_9

    const/4 v4, 0x1

    .line 143
    :goto_3
    const/4 v3, 0x0

    .line 145
    new-instance v11, Landroid/util/SparseArray;

    invoke-direct {v11}, Landroid/util/SparseArray;-><init>()V

    .line 146
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12}, Ljava/util/ArrayList;-><init>()V

    .line 148
    new-instance v13, Ljava/util/HashSet;

    invoke-direct {v13}, Ljava/util/HashSet;-><init>()V

    .line 149
    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v14

    move v6, v4

    move-object v4, v3

    :cond_5
    :goto_4
    invoke-interface {v14}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_11

    invoke-interface {v14}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 151
    invoke-interface {v8, v3}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v15

    .line 155
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-eqz v3, :cond_a

    .line 156
    invoke-static {v15}, Lcom/google/android/gms/icing/impl/a/j;->d(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v3

    .line 157
    invoke-interface {v3}, Ljava/util/Map;->isEmpty()Z

    move-result v5

    if-nez v5, :cond_5

    move-object v5, v3

    .line 165
    :goto_5
    iget v3, v15, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v11, v3, v15}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 166
    new-instance v16, Lcom/google/android/gms/icing/ak;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/gms/icing/ak;-><init>()V

    .line 167
    iget v3, v15, Lcom/google/android/gms/icing/g;->a:I

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/gms/icing/ak;->a:I

    .line 169
    if-eqz v6, :cond_14

    .line 174
    if-nez v4, :cond_b

    move v7, v6

    move-object v6, v5

    .line 181
    :goto_6
    new-instance v17, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, v15, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v4, v15, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v3, :cond_d

    .line 183
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 185
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v19

    :cond_6
    :goto_7
    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_c

    invoke-interface/range {v19 .. v19}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/appdatasearch/Section;

    .line 186
    iget-object v4, v3, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/icing/impl/a/t;

    .line 188
    if-eqz v4, :cond_6

    .line 190
    iget-object v0, v4, Lcom/google/android/gms/icing/impl/a/t;->b:Lcom/google/android/gms/icing/av;

    move-object/from16 v20, v0

    .line 192
    new-instance v21, Lcom/google/android/gms/icing/an;

    invoke-direct/range {v21 .. v21}, Lcom/google/android/gms/icing/an;-><init>()V

    .line 194
    iget v0, v4, Lcom/google/android/gms/icing/impl/a/t;->a:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/google/android/gms/icing/an;->a:I

    .line 195
    iget-boolean v0, v3, Lcom/google/android/gms/appdatasearch/Section;->c:Z

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput-boolean v0, v1, Lcom/google/android/gms/icing/an;->b:Z

    .line 196
    iget v0, v3, Lcom/google/android/gms/appdatasearch/Section;->d:I

    move/from16 v22, v0

    move/from16 v0, v22

    move-object/from16 v1, v21

    iput v0, v1, Lcom/google/android/gms/icing/an;->d:I

    .line 197
    move-object/from16 v0, v18

    move-object/from16 v1, v21

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 200
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    move-object/from16 v21, v0

    invoke-virtual/range {v21 .. v21}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v21

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/gms/icing/av;->d:I

    move/from16 v22, v0

    move-object/from16 v0, v21

    move-object/from16 v1, v17

    move/from16 v2, v22

    invoke-virtual {v0, v1, v3, v2}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;I)I

    move-result v3

    move-object/from16 v0, v20

    iput v3, v0, Lcom/google/android/gms/icing/av;->d:I

    .line 203
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-nez v3, :cond_7

    move-object/from16 v0, v20

    iget v3, v0, Lcom/google/android/gms/icing/av;->d:I

    const/16 v21, 0x1

    move/from16 v0, v21

    if-ne v3, v0, :cond_7

    move-object/from16 v0, v20

    iget v3, v0, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v3, :cond_6

    .line 206
    :cond_7
    new-instance v3, Lcom/google/android/gms/icing/ao;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ao;-><init>()V

    .line 207
    iget v4, v4, Lcom/google/android/gms/icing/impl/a/t;->a:I

    iput v4, v3, Lcom/google/android/gms/icing/ao;->b:I

    .line 208
    iget v4, v15, Lcom/google/android/gms/icing/g;->a:I

    iput v4, v3, Lcom/google/android/gms/icing/ao;->a:I

    .line 209
    move-object/from16 v0, v20

    iget v4, v0, Lcom/google/android/gms/icing/av;->d:I

    iput v4, v3, Lcom/google/android/gms/icing/ao;->c:I

    .line 210
    move-object/from16 v0, v20

    iget v4, v0, Lcom/google/android/gms/icing/av;->l:I

    iput v4, v3, Lcom/google/android/gms/icing/ao;->d:I

    .line 211
    invoke-interface {v13, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_7

    .line 130
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_2

    .line 142
    :cond_9
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 162
    :cond_a
    invoke-static {v15}, Lcom/google/android/gms/icing/impl/a/j;->c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v3

    move-object v5, v3

    goto/16 :goto_5

    .line 176
    :cond_b
    invoke-interface {v4, v5}, Ljava/util/Map;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_14

    .line 177
    const/4 v6, 0x0

    move v7, v6

    move-object v6, v4

    goto/16 :goto_6

    .line 214
    :cond_c
    move-object/from16 v0, v16

    iget-object v3, v0, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    move-object/from16 v0, v18

    invoke-interface {v0, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/gms/icing/an;

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    .line 218
    :cond_d
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v3, :cond_e

    .line 219
    move-object/from16 v0, p6

    iget-object v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    move-object/from16 v0, v16

    iget-object v4, v0, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Ljava/lang/String;

    move-object/from16 v0, v16

    iput-object v3, v0, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    .line 222
    :cond_e
    move-object/from16 v0, v16

    invoke-interface {v12, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-virtual {v3, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/util/List;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_f
    :goto_8
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_10

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/g/o;

    .line 227
    iget-object v3, v3, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    invoke-interface {v5, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/impl/a/t;

    .line 228
    if-eqz v3, :cond_f

    .line 229
    new-instance v16, Lcom/google/android/gms/icing/ao;

    invoke-direct/range {v16 .. v16}, Lcom/google/android/gms/icing/ao;-><init>()V

    .line 230
    iget v0, v3, Lcom/google/android/gms/icing/impl/a/t;->a:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/google/android/gms/icing/ao;->b:I

    .line 231
    iget v0, v15, Lcom/google/android/gms/icing/g;->a:I

    move/from16 v17, v0

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/google/android/gms/icing/ao;->a:I

    .line 232
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, v16

    iput v0, v1, Lcom/google/android/gms/icing/ao;->c:I

    .line 233
    iget-object v3, v3, Lcom/google/android/gms/icing/impl/a/t;->b:Lcom/google/android/gms/icing/av;

    iget v3, v3, Lcom/google/android/gms/icing/av;->l:I

    move-object/from16 v0, v16

    iput v3, v0, Lcom/google/android/gms/icing/ao;->d:I

    .line 235
    move-object/from16 v0, v16

    invoke-interface {v13, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_8

    :cond_10
    move-object v4, v6

    move v6, v7

    .line 237
    goto/16 :goto_4

    .line 239
    :cond_11
    iget-object v3, v9, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    invoke-interface {v12, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/gms/icing/ak;

    iput-object v3, v9, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    .line 240
    iget-object v3, v9, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-interface {v13, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/gms/icing/ao;

    iput-object v3, v9, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    .line 242
    if-eqz v6, :cond_12

    if-eqz v4, :cond_12

    .line 243
    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v3

    new-array v3, v3, [Lcom/google/android/gms/icing/am;

    iput-object v3, v9, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    .line 244
    const/4 v3, 0x0

    .line 245
    invoke-interface {v4}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    move v5, v3

    :goto_9
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_12

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/Map$Entry;

    .line 246
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/icing/impl/a/t;

    .line 247
    new-instance v7, Lcom/google/android/gms/icing/am;

    invoke-direct {v7}, Lcom/google/android/gms/icing/am;-><init>()V

    .line 248
    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    iput-object v3, v7, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    .line 249
    iget v3, v4, Lcom/google/android/gms/icing/impl/a/t;->a:I

    iput v3, v7, Lcom/google/android/gms/icing/am;->b:I

    .line 250
    iget-object v4, v9, Lcom/google/android/gms/icing/aj;->c:[Lcom/google/android/gms/icing/am;

    add-int/lit8 v3, v5, 0x1

    aput-object v7, v4, v5

    move v5, v3

    .line 251
    goto :goto_9

    .line 255
    :cond_12
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->d:Lcom/google/android/gms/icing/impl/e/g;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/e/g;->b()V

    .line 256
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, p4

    move/from16 v2, p5

    invoke-virtual {v3, v0, v9, v1, v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aj;II)Lcom/google/android/gms/icing/ap;

    move-result-object v4

    .line 258
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->d:Lcom/google/android/gms/icing/impl/e/g;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/e/g;->c()V

    .line 260
    move-object/from16 v0, p6

    iget-boolean v3, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    invoke-static {v9, v4, v11, v3}, Lcom/google/android/gms/appdatasearch/aq;->a(Lcom/google/android/gms/icing/aj;Lcom/google/android/gms/icing/ap;Landroid/util/SparseArray;Z)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v9

    .line 263
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->d:Lcom/google/android/gms/icing/impl/e/g;

    iget v6, v4, Lcom/google/android/gms/icing/ap;->b:I

    iget v7, v4, Lcom/google/android/gms/icing/ap;->e:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/u;->H()Lcom/google/android/gms/icing/impl/bf;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/bf;->f()I

    move-result v8

    move-object/from16 v4, p1

    move/from16 v5, p5

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/gms/icing/impl/e/g;->a(Ljava/lang/String;IIII)Lcom/google/k/f/ap;

    move-result-object v4

    .line 266
    sget-object v3, Lcom/google/android/gms/icing/a/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 267
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v5, Lcom/google/android/gms/search/queries/n;

    move-object/from16 v0, p0

    invoke-direct {v5, v0, v4}, Lcom/google/android/gms/search/queries/n;-><init>(Lcom/google/android/gms/search/queries/m;Lcom/google/k/f/ap;)V

    invoke-virtual {v3, v5}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 278
    :goto_a
    monitor-exit v10

    move-object v3, v9

    goto/16 :goto_0

    .line 275
    :cond_13
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v3

    invoke-interface {v3, v4}, Lcom/google/android/gms/icing/impl/m;->a(Lcom/google/k/f/ap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_a

    :cond_14
    move v7, v6

    move-object v6, v4

    goto/16 :goto_6
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/icing/impl/e/g;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 67
    if-nez p0, :cond_0

    move v0, v1

    .line 69
    :goto_0
    new-instance v2, Lcom/google/android/gms/icing/impl/e/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(I)I

    move-result v0

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/icing/impl/e/g;-><init>(II)V

    return-object v2

    .line 67
    :cond_0
    iget v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/search/queries/m;)Lcom/google/android/gms/icing/impl/u;
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->a:Lcom/google/android/gms/icing/impl/u;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 50
    new-instance v7, Lcom/google/android/gms/search/queries/QueryCall$Response;

    invoke-direct {v7}, Lcom/google/android/gms/search/queries/QueryCall$Response;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget-object v1, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget-object v2, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget-object v3, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget v4, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->d:I

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget v5, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->e:I

    iget-object v0, p0, Lcom/google/android/gms/search/queries/m;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/QueryCall$Request;

    iget-object v6, v0, Lcom/google/android/gms/search/queries/QueryCall$Request;->f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/search/queries/m;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->b()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    :goto_0
    iput-object v0, v7, Lcom/google/android/gms/search/queries/QueryCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v1, v7, Lcom/google/android/gms/search/queries/QueryCall$Response;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v7

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method
