.class public final Lcom/google/android/gms/icing/ag;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Ljava/lang/String;

.field public c:J

.field public d:I

.field public e:I

.field public f:J

.field public g:I


# direct methods
.method public constructor <init>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, -0x1

    const-wide/16 v2, 0x0

    .line 10696
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 10697
    iput-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    iput-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    iput v4, p0, Lcom/google/android/gms/icing/ag;->d:I

    iput v4, p0, Lcom/google/android/gms/icing/ag;->e:I

    iput-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    iput v1, p0, Lcom/google/android/gms/icing/ag;->g:I

    iput v1, p0, Lcom/google/android/gms/icing/ag;->cachedSize:I

    .line 10698
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10795
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 10796
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 10797
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10800
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 10801
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 10804
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 10805
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10808
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/ag;->d:I

    if-eqz v1, :cond_3

    .line 10809
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/ag;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10812
    :cond_3
    iget v1, p0, Lcom/google/android/gms/icing/ag;->e:I

    if-eqz v1, :cond_4

    .line 10813
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/ag;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10816
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_5

    .line 10817
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 10820
    :cond_5
    iget v1, p0, Lcom/google/android/gms/icing/ag;->g:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_6

    .line 10821
    const/4 v1, 0x7

    iget v2, p0, Lcom/google/android/gms/icing/ag;->g:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 10824
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 10714
    if-ne p1, p0, :cond_1

    .line 10746
    :cond_0
    :goto_0
    return v0

    .line 10717
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ag;

    if-nez v2, :cond_2

    move v0, v1

    .line 10718
    goto :goto_0

    .line 10720
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ag;

    .line 10721
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/ag;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 10722
    goto :goto_0

    .line 10724
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 10725
    iget-object v2, p1, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 10726
    goto :goto_0

    .line 10728
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 10729
    goto :goto_0

    .line 10731
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/ag;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_6

    move v0, v1

    .line 10732
    goto :goto_0

    .line 10734
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/ag;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/ag;->d:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 10735
    goto :goto_0

    .line 10737
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/ag;->e:I

    iget v3, p1, Lcom/google/android/gms/icing/ag;->e:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 10738
    goto :goto_0

    .line 10740
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/ag;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_9

    move v0, v1

    .line 10741
    goto :goto_0

    .line 10743
    :cond_9
    iget v2, p0, Lcom/google/android/gms/icing/ag;->g:I

    iget v3, p1, Lcom/google/android/gms/icing/ag;->g:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 10744
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 10751
    iget-wide v0, p0, Lcom/google/android/gms/icing/ag;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 10754
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 10756
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/ag;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 10758
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ag;->d:I

    add-int/2addr v0, v1

    .line 10759
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ag;->e:I

    add-int/2addr v0, v1

    .line 10760
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/ag;->f:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 10762
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ag;->g:I

    add-int/2addr v0, v1

    .line 10763
    return v0

    .line 10754
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 10658
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/ag;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/ag;->c:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ag;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/ag;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/ag;->f:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ag;->g:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 10769
    iget-wide v0, p0, Lcom/google/android/gms/icing/ag;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 10770
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 10772
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 10773
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 10775
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/ag;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 10776
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 10778
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/ag;->d:I

    if-eqz v0, :cond_3

    .line 10779
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/ag;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 10781
    :cond_3
    iget v0, p0, Lcom/google/android/gms/icing/ag;->e:I

    if-eqz v0, :cond_4

    .line 10782
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/icing/ag;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10784
    :cond_4
    iget-wide v0, p0, Lcom/google/android/gms/icing/ag;->f:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_5

    .line 10785
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/ag;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 10787
    :cond_5
    iget v0, p0, Lcom/google/android/gms/icing/ag;->g:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_6

    .line 10788
    const/4 v0, 0x7

    iget v1, p0, Lcom/google/android/gms/icing/ag;->g:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 10790
    :cond_6
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 10791
    return-void
.end method
