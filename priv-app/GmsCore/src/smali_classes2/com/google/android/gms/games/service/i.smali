.class public final Lcom/google/android/gms/games/service/i;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:I

.field private static final b:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 54
    const-string v0, "GamesNotificationManage"

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    sput v0, Lcom/google/android/gms/games/service/i;->a:I

    .line 64
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    return-void
.end method

.method private static a(Ljava/lang/String;I)I
    .locals 3

    .prologue
    .line 339
    and-int/lit8 v0, p1, 0x1

    shl-int/lit8 v0, v0, 0x1f

    sget v1, Lcom/google/android/gms/games/service/i;->a:I

    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v2

    xor-int/2addr v1, v2

    const v2, 0x7fffffff

    and-int/2addr v1, v2

    or-int/2addr v0, v1

    return v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Z)Landroid/app/Notification;
    .locals 8

    .prologue
    const/4 v5, 0x0

    const/4 v3, 0x1

    .line 411
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 416
    if-eqz p6, :cond_2

    .line 418
    sget v0, Lcom/google/android/gms/n;->h:I

    new-array v1, v3, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v5

    invoke-virtual {v4, v0, v3, v1}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 420
    sget v0, Lcom/google/android/gms/h;->dg:I

    invoke-static {v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 421
    invoke-static {p0, p2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v3, v2

    move-object v7, v1

    move-object v1, v0

    move-object v0, v7

    .line 435
    :goto_0
    if-nez v0, :cond_0

    .line 436
    sget v0, Lcom/google/android/gms/h;->di:I

    invoke-static {v4, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 441
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/service/i;->c(Landroid/content/Context;)Landroid/support/v4/app/bk;

    move-result-object v4

    iput-object v0, v4, Landroid/support/v4/app/bk;->g:Landroid/graphics/Bitmap;

    invoke-virtual {v4, v3}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v2}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    iput-object p4, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    const-string v1, "1"

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {v0, p5}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v6

    iput v5, v6, Landroid/support/v4/app/bk;->j:I

    .line 454
    if-nez p6, :cond_1

    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 455
    new-instance v0, Landroid/support/v4/app/bl;

    invoke-direct {v0}, Landroid/support/v4/app/bl;-><init>()V

    .line 456
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->h()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bl;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bl;

    .line 457
    invoke-virtual {v0, p2}, Landroid/support/v4/app/bl;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bl;

    .line 458
    invoke-virtual {v6, v0}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    .line 459
    invoke-virtual {v6, p2}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    .line 463
    :cond_1
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->c()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->d()I

    move-result v3

    const/4 v4, 0x2

    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->b()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 466
    invoke-virtual {v6}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    return-object v0

    .line 423
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v0

    .line 424
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->f()Ljava/lang/String;

    move-result-object v3

    .line 425
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->g()Ljava/lang/String;

    move-result-object v2

    .line 426
    invoke-static {p1, p3}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/a;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 431
    invoke-interface {p3}, Lcom/google/android/gms/games/internal/e/a;->h()Ljava/lang/String;

    move-result-object v0

    move-object v7, v0

    move-object v0, v1

    move-object v1, v7

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    .line 605
    const-string v0, "com.google.android.gms.games.SHOW_MULTIPLAYER_INBOX_INTERNAL"

    invoke-static {p2, p3, v0}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 607
    const/high16 v1, 0x40000000    # 2.0f

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;
    .locals 3

    .prologue
    .line 674
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.ACKNOWLEDGE_NOTIFICATIONS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 675
    const-string v1, "com.google.android.gms.games.ACCOUNT_KEY"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 676
    invoke-static {p1, p2}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v1

    const/high16 v2, 0x40000000    # 2.0f

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 613
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 614
    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 615
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 616
    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 622
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 623
    const/16 v1, 0x4000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 624
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 625
    return-object v0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/images/a;Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 547
    const/4 v0, 0x0

    .line 548
    if-eqz p2, :cond_0

    .line 549
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 550
    if-eqz v1, :cond_0

    .line 552
    :try_start_0
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 558
    :goto_0
    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 559
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 560
    const v2, 0x1050006

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    float-to-int v2, v2

    .line 562
    const v3, 0x1050005

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    float-to-int v1, v1

    .line 564
    const/4 v3, 0x1

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 568
    :cond_0
    return-object v0

    .line 554
    :catch_0
    move-exception v1

    const-string v1, "GamesNotificationManage"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse image content for icon URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 683
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/e/a;->e()Landroid/net/Uri;

    move-result-object v0

    .line 684
    if-eqz v0, :cond_0

    .line 689
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/e/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ab;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)Landroid/support/v4/app/bl;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 631
    invoke-virtual {p2}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v2

    .line 632
    new-instance v3, Landroid/support/v4/app/bl;

    invoke-direct {v3}, Landroid/support/v4/app/bl;-><init>()V

    move v0, v1

    .line 633
    :goto_0
    if-ge v0, v2, :cond_2

    .line 634
    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v4

    .line 635
    invoke-interface {v4}, Lcom/google/android/gms/games/internal/e/a;->k()Z

    move-result v5

    if-nez v5, :cond_1

    .line 636
    invoke-interface {v4}, Lcom/google/android/gms/games/internal/e/a;->i()Ljava/lang/String;

    move-result-object v5

    .line 637
    if-nez v5, :cond_0

    .line 639
    const-string v5, "GamesNotificationManage"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Null coalesced text when parsing notification "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lcom/google/android/gms/games/internal/e/a;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {p0, v5, v4}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 643
    :cond_0
    invoke-static {v5}, Landroid/text/Html;->fromHtml(Ljava/lang/String;)Landroid/text/Spanned;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/bl;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bl;

    goto :goto_1

    .line 646
    :cond_1
    sub-int v0, v2, v0

    .line 647
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v4, Lcom/google/android/gms/n;->i:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v1

    invoke-virtual {v2, v4, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Landroid/support/v4/app/bl;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bl;

    .line 652
    :cond_2
    invoke-virtual {v3, p1}, Landroid/support/v4/app/bl;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bl;

    .line 653
    return-object v3
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 584
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 585
    const-string v1, "com.google"

    invoke-virtual {v0, v1}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    .line 586
    array-length v0, v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    .line 590
    :goto_0
    return-object p1

    .line 589
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 590
    sget v1, Lcom/google/android/gms/p;->jZ:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object p1

    goto :goto_0
.end method

.method private static a(ILandroid/app/NotificationManager;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 273
    sget-object v0, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 274
    if-nez v0, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 279
    :cond_1
    const/4 v4, 0x1

    move v2, v3

    .line 280
    :goto_1
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v2, v1, :cond_3

    .line 281
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/j;

    .line 282
    iget-boolean v1, v1, Lcom/google/android/gms/games/service/j;->f:Z

    if-nez v1, :cond_2

    .line 289
    :goto_2
    if-eqz v3, :cond_0

    .line 290
    invoke-virtual {p1, p0}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0

    .line 280
    :cond_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    :cond_3
    move v3, v4

    goto :goto_2
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 138
    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v0

    .line 139
    sget-object v2, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 140
    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 144
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 145
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    .line 146
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/j;

    .line 147
    new-instance v5, Lcom/google/android/gms/games/f/b;

    iget-object v6, v1, Lcom/google/android/gms/games/service/j;->d:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/games/service/j;->c:Ljava/lang/String;

    iget v1, v1, Lcom/google/android/gms/games/service/j;->b:I

    invoke-direct {v5, v6, v7, v1}, Lcom/google/android/gms/games/f/b;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 151
    :cond_2
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 152
    invoke-static {p0, p2, p3, v3}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/ArrayList;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;)V
    .locals 9

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 173
    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v8

    .line 174
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v7, v0

    check-cast v7, Landroid/app/NotificationManager;

    .line 178
    invoke-static {p0, p1, p2, v8}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 180
    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v0

    const-string v2, "com.google.android.gms.games.SHOW_LEVEL_UP_TRAMPOLINE_INTERNAL"

    invoke-static {p2, p3, v2}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const/high16 v3, 0x40000000    # 2.0f

    invoke-static {p0, v0, v2, v3}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-static {p0, p1, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-interface {p4}, Lcom/google/android/gms/games/internal/e/a;->d()I

    move-result v0

    const/16 v2, 0x10

    if-ne v0, v2, :cond_1

    move v0, v1

    :goto_0
    const-string v1, "Notification type must be level-up."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v0

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {v7, v8, v0}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 187
    :cond_0
    return-void

    :cond_1
    move v0, v6

    .line 180
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)V
    .locals 21

    .prologue
    .line 205
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v17

    .line 206
    const-string v4, "notification"

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    move-object v13, v4

    check-cast v13, Landroid/app/NotificationManager;

    .line 208
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v4

    if-gtz v4, :cond_1

    .line 209
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move/from16 v3, v17

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 269
    :cond_0
    :goto_0
    return-void

    .line 215
    :cond_1
    sget-object v4, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    move/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/ArrayList;

    .line 216
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v18

    .line 217
    new-instance v19, Ljava/util/ArrayList;

    move-object/from16 v0, v19

    move/from16 v1, v18

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 218
    const/4 v14, 0x0

    .line 219
    const/4 v15, 0x1

    .line 220
    const/4 v5, 0x0

    move/from16 v16, v5

    :goto_1
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_2

    .line 221
    move-object/from16 v0, p4

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v20

    .line 222
    new-instance v5, Lcom/google/android/gms/games/service/j;

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->a()J

    move-result-wide v6

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->b()Ljava/lang/String;

    move-result-object v8

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->c()Ljava/lang/String;

    move-result-object v9

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->h()Ljava/lang/String;

    move-result-object v10

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->d()I

    move-result v11

    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->k()Z

    move-result v12

    invoke-direct/range {v5 .. v12}, Lcom/google/android/gms/games/service/j;-><init>(JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)V

    .line 225
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 226
    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->j()Z

    move-result v5

    if-nez v5, :cond_9

    .line 227
    const/4 v5, 0x1

    .line 231
    :goto_2
    invoke-interface/range {v20 .. v20}, Lcom/google/android/gms/games/internal/e/a;->k()Z

    move-result v6

    if-nez v6, :cond_8

    .line 232
    const/4 v6, 0x0

    .line 220
    :goto_3
    add-int/lit8 v7, v16, 0x1

    move/from16 v16, v7

    move v15, v6

    move v14, v5

    goto :goto_1

    .line 240
    :cond_2
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    if-eqz v14, :cond_0

    .line 247
    if-nez v15, :cond_3

    .line 248
    move/from16 v0, v17

    invoke-static {v0, v13}, Lcom/google/android/gms/games/service/i;->a(ILandroid/app/NotificationManager;)V

    .line 253
    :cond_3
    if-eqz v15, :cond_5

    .line 254
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v5

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v5, v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v6

    sget v7, Lcom/google/android/gms/n;->h:I

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-virtual {v4, v7, v6, v8}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    sget v8, Lcom/google/android/gms/h;->dg:I

    invoke-static {v4, v8}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v4

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/games/service/i;->c(Landroid/content/Context;)Landroid/support/v4/app/bk;

    move-result-object v9

    iput-object v4, v9, Landroid/support/v4/app/bk;->g:Landroid/graphics/Bitmap;

    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    invoke-virtual {v4, v7}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    invoke-virtual {v4, v8}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    iput-object v5, v4, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/bk;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v4

    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v5}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v4

    const/4 v5, -0x2

    iput v5, v4, Landroid/support/v4/app/bk;->j:I

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)V

    invoke-virtual {v4}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v4

    .line 265
    :goto_4
    if-eqz v4, :cond_4

    .line 266
    move/from16 v0, v17

    invoke-virtual {v13, v0, v4}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 268
    :cond_4
    sget-object v4, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    move/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v4, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto/16 :goto_0

    .line 256
    :cond_5
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_7

    .line 257
    const/4 v4, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v4

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    invoke-static {v0, v4, v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v8

    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-static {v0, v1, v4}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v9

    invoke-static/range {p0 .. p0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;)Z

    move-result v4

    if-eqz v4, :cond_6

    invoke-interface {v7}, Lcom/google/android/gms/games/internal/e/a;->d()I

    move-result v4

    const/4 v5, 0x2

    if-eq v4, v5, :cond_6

    const/4 v10, 0x1

    :goto_5
    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    invoke-static/range {v4 .. v10}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Z)Landroid/app/Notification;

    move-result-object v4

    goto :goto_4

    :cond_6
    const/4 v10, 0x0

    goto :goto_5

    .line 260
    :cond_7
    invoke-static/range {p0 .. p4}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)Landroid/app/Notification;

    move-result-object v4

    goto :goto_4

    :cond_8
    move v6, v15

    goto/16 :goto_3

    :cond_9
    move v5, v14

    goto/16 :goto_2
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 81
    invoke-static {p0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    const-string v1, "useNewPlayerNotifications"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 83
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 84
    return-void
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 73
    const-string v1, "useNewPlayerNotifications"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)Landroid/app/Notification;
    .locals 13

    .prologue
    .line 471
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 472
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/internal/e/b;->c()I

    move-result v9

    .line 475
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v1

    move-object/from16 v0, p3

    invoke-static {p0, v1, p2, v0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v10

    .line 479
    invoke-static {p0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;)Z

    move-result v11

    .line 480
    sget v1, Lcom/google/android/gms/n;->h:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v2, v4

    invoke-virtual {v3, v1, v9, v2}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 484
    if-eqz v11, :cond_1

    .line 485
    invoke-static {p0, p2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 486
    sget v1, Lcom/google/android/gms/h;->dg:I

    invoke-static {v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v8, v2

    .line 497
    :goto_0
    if-nez v1, :cond_9

    .line 498
    sget v1, Lcom/google/android/gms/h;->di:I

    invoke-static {v3, v1}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v3, v1

    .line 503
    :goto_1
    const/4 v5, 0x1

    .line 504
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;I)I

    move-result v1

    sget-object v2, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    if-nez v1, :cond_2

    const/4 v2, 0x0

    move v4, v2

    :goto_2
    new-instance v12, Ljava/util/ArrayList;

    invoke-direct {v12, v4}, Ljava/util/ArrayList;-><init>(I)V

    const/4 v2, 0x0

    move v7, v2

    :goto_3
    if-ge v7, v4, :cond_3

    invoke-virtual {v1, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/service/j;

    iget-object v2, v2, Lcom/google/android/gms/games/service/j;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    invoke-virtual {v12, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto :goto_3

    .line 490
    :cond_1
    const/4 v1, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v1

    .line 491
    invoke-static {p0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v2

    .line 492
    invoke-static {p1, v1}, Lcom/google/android/gms/games/service/i;->a(Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {p0, v2, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Lcom/google/android/gms/common/images/a;Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    move-object v8, p2

    goto :goto_0

    .line 504
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v4, v2

    goto :goto_2

    .line 505
    :cond_3
    const/4 v1, 0x0

    :goto_4
    if-ge v1, v9, :cond_8

    .line 507
    move-object/from16 v0, p4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v2

    .line 508
    invoke-interface {v2}, Lcom/google/android/gms/games/internal/e/a;->j()Z

    move-result v4

    if-nez v4, :cond_5

    .line 509
    invoke-interface {v2}, Lcom/google/android/gms/games/internal/e/a;->b()Ljava/lang/String;

    move-result-object v4

    .line 514
    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 516
    invoke-interface {v2}, Lcom/google/android/gms/games/internal/e/a;->k()Z

    move-result v2

    if-nez v2, :cond_5

    .line 517
    const/4 v1, 0x0

    .line 524
    :goto_5
    invoke-static {p0}, Lcom/google/android/gms/games/service/i;->c(Landroid/content/Context;)Landroid/support/v4/app/bk;

    move-result-object v4

    iput-object v3, v4, Landroid/support/v4/app/bk;->g:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    const/4 v2, 0x0

    :goto_6
    invoke-virtual {v4, v2}, Landroid/support/v4/app/bk;->e(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    invoke-virtual {v2, v8}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    iput-object v10, v2, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-static {v9}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->d(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {p0, p1, v3}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;I)Landroid/app/PendingIntent;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/bk;->a(Landroid/app/PendingIntent;)Landroid/support/v4/app/bk;

    move-result-object v2

    if-eqz v1, :cond_7

    const/4 v1, -0x2

    :goto_7
    iput v1, v2, Landroid/support/v4/app/bk;->j:I

    .line 536
    if-nez v11, :cond_4

    .line 537
    move-object/from16 v0, p4

    invoke-static {p0, p2, v0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)Landroid/support/v4/app/bl;

    move-result-object v1

    invoke-virtual {v2, v1}, Landroid/support/v4/app/bk;->a(Landroid/support/v4/app/bv;)Landroid/support/v4/app/bk;

    .line 541
    :cond_4
    move-object/from16 v0, p4

    invoke-static {p0, p2, v0}, Lcom/google/android/gms/games/service/i;->b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)V

    .line 542
    invoke-virtual {v2}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v1

    return-object v1

    .line 505
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    :cond_6
    move-object v2, v6

    .line 524
    goto :goto_6

    :cond_7
    const/4 v1, 0x0

    goto :goto_7

    :cond_8
    move v1, v5

    goto :goto_5

    :cond_9
    move-object v3, v1

    goto/16 :goto_1
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)V
    .locals 6

    .prologue
    .line 658
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 659
    invoke-virtual {p2}, Lcom/google/android/gms/games/internal/e/b;->f()Ljava/util/Iterator;

    move-result-object v2

    .line 660
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 661
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/e/a;

    .line 662
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/e/a;->c()Ljava/lang/String;

    move-result-object v3

    .line 663
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/e/a;->b()Ljava/lang/String;

    move-result-object v4

    .line 664
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/e/a;->d()I

    move-result v0

    .line 665
    new-instance v5, Lcom/google/android/gms/games/f/b;

    invoke-direct {v5, v3, v4, v0}, Lcom/google/android/gms/games/f/b;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 668
    :cond_0
    const/4 v0, 0x2

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;ILjava/util/ArrayList;)V

    .line 670
    return-void
.end method

.method private static b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 160
    const-string v0, "GamesNotificationManage"

    const-string v1, "Clearing displayed notifications."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/4 v0, 0x5

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 165
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 167
    invoke-virtual {v0, p3}, Landroid/app/NotificationManager;->cancel(I)V

    .line 168
    sget-object v0, Lcom/google/android/gms/games/service/i;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p3}, Landroid/util/SparseArray;->remove(I)V

    .line 169
    return-void
.end method

.method public static b(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 101
    invoke-static {p0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 102
    const-string v1, "allowLevelUpNotifications"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 104
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 105
    return-void
.end method

.method public static b(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 91
    invoke-static {p0}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 92
    const-string v1, "allowLevelUpNotifications"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    return v0
.end method

.method private static c(Landroid/content/Context;)Landroid/support/v4/app/bk;
    .locals 3

    .prologue
    .line 693
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->di:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    const-string v1, "social"

    iput-object v1, v0, Landroid/support/v4/app/bk;->w:Ljava/lang/String;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->V:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Landroid/support/v4/app/bk;->y:I

    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v4/app/bk;->z:I

    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(I)Landroid/support/v4/app/bk;

    move-result-object v0

    .line 700
    return-object v0
.end method
