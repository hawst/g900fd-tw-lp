.class public final Lcom/google/android/gms/auth/trustagent/bd;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/bg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 454
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 456
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bd;
    .locals 3

    .prologue
    .line 467
    new-instance v0, Lcom/google/android/gms/auth/trustagent/bd;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/bd;-><init>()V

    .line 468
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 469
    const-string v2, "auth_trust_agent_pref_trusted_devices_preference_key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 470
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bd;->setArguments(Landroid/os/Bundle;)V

    .line 471
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/bd;)Lcom/google/android/gms/auth/trustagent/bg;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bd;->a:Lcom/google/android/gms/auth/trustagent/bg;

    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 476
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 477
    check-cast p1, Lcom/google/android/gms/auth/trustagent/bg;

    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/bd;->a:Lcom/google/android/gms/auth/trustagent/bg;

    .line 478
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 482
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bd;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_devices_preference_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 484
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bd;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 485
    sget v2, Lcom/google/android/gms/p;->cX:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bd;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 486
    sget v2, Lcom/google/android/gms/p;->cW:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/bd;->getString(I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/auth/trustagent/be;

    invoke-direct {v3, p0, v0}, Lcom/google/android/gms/auth/trustagent/be;-><init>(Lcom/google/android/gms/auth/trustagent/bd;Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 493
    sget v0, Lcom/google/android/gms/p;->cV:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bd;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/auth/trustagent/bf;

    invoke-direct {v2, p0}, Lcom/google/android/gms/auth/trustagent/bf;-><init>(Lcom/google/android/gms/auth/trustagent/bd;)V

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 500
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method
