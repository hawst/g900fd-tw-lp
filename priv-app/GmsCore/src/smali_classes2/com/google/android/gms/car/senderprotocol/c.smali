.class public final Lcom/google/android/gms/car/senderprotocol/c;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/d;

.field private b:Z

.field private volatile c:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 56
    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/c;->b:Z

    .line 57
    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/c;->c:Z

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/c;->a:Lcom/google/android/gms/car/senderprotocol/d;

    .line 62
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/c;->c:Z

    .line 73
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->a()V

    .line 74
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/c;->c:Z

    .line 67
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/az;->a(I)V

    .line 68
    return-void
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    const/4 v3, 0x3

    .line 78
    const v0, 0x8002

    if-ne p1, v0, :cond_4

    .line 79
    new-instance v0, Lcom/google/android/c/b/l;

    invoke-direct {v0}, Lcom/google/android/c/b/l;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/c;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/l;

    .line 82
    if-eqz v0, :cond_3

    .line 83
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.BT"

    const-string v2, "handlePairingResponse"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/c;->b:Z

    if-nez v1, :cond_1

    const-string v0, "CAR.BT"

    const-string v1, "Have not sent pairing request"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :goto_0
    return-void

    .line 83
    :cond_1
    iget v1, v0, Lcom/google/android/c/b/l;->a:I

    iget-boolean v0, v0, Lcom/google/android/c/b/l;->b:Z

    const-string v2, "CAR.BT"

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Got pairing response. status="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " alreadyPaired="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/c;->a:Lcom/google/android/gms/car/senderprotocol/d;

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/car/senderprotocol/d;->a(IZ)V

    goto :goto_0

    .line 85
    :cond_3
    const-string v0, "CAR.BT"

    const-string v1, "Wrong BluetoothPairingResponse message"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 87
    :cond_4
    const v0, 0x8003

    if-ne p1, v0, :cond_7

    .line 88
    new-instance v0, Lcom/google/android/c/b/j;

    invoke-direct {v0}, Lcom/google/android/c/b/j;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/c;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/j;

    .line 91
    if-eqz v0, :cond_6

    .line 92
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "handleAuthData. auth data: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/c/b/j;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/c;->a:Lcom/google/android/gms/car/senderprotocol/d;

    iget-object v0, v0, Lcom/google/android/c/b/j;->a:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/d;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 94
    :cond_6
    const-string v0, "CAR.BT"

    const-string v1, "Wrong BluetoothAuthenticationData message"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 97
    :cond_7
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrong Bluetooth message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 103
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 104
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendPairingRequest: phone address="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", pairing method="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/c;->c:Z

    if-nez v0, :cond_2

    .line 109
    const-string v0, "CAR.BT"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 110
    const-string v0, "CAR.BT"

    const-string v1, "sendPairingRequest on closed channel"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_1
    :goto_0
    return-void

    .line 117
    :cond_2
    new-instance v0, Lcom/google/android/c/b/k;

    invoke-direct {v0}, Lcom/google/android/c/b/k;-><init>()V

    .line 118
    iput-object p1, v0, Lcom/google/android/c/b/k;->a:Ljava/lang/String;

    .line 119
    iput p2, v0, Lcom/google/android/c/b/k;->b:I

    .line 120
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/c;->b:Z

    .line 121
    const v1, 0x8001

    invoke-static {v0}, Lcom/google/android/c/b/k;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/c;->a(I[B)V

    goto :goto_0
.end method
