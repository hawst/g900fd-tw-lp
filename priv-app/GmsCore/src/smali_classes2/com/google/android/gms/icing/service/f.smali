.class public final Lcom/google/android/gms/icing/service/f;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/app/AlarmManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 288
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 289
    iput-object p1, p0, Lcom/google/android/gms/icing/service/f;->a:Landroid/content/Context;

    .line 290
    iget-object v0, p0, Lcom/google/android/gms/icing/service/f;->a:Landroid/content/Context;

    const-string v1, "alarm"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    iput-object v0, p0, Lcom/google/android/gms/icing/service/f;->b:Landroid/app/AlarmManager;

    .line 291
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 7

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/icing/service/f;->b:Landroid/app/AlarmManager;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/f;->a:Landroid/content/Context;

    const/high16 v2, 0x10000000

    const-wide/16 v4, 0x0

    invoke-static {v1, v2, v4, v5}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/icing/service/f;->b:Landroid/app/AlarmManager;

    const/4 v1, 0x0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    add-long/2addr v2, p1

    iget-object v4, p0, Lcom/google/android/gms/icing/service/f;->a:Landroid/content/Context;

    const/high16 v5, 0x48000000    # 131072.0f

    invoke-static {v4, v5, p1, p2}, Lcom/google/android/gms/icing/service/IndexWorkerService;->a(Landroid/content/Context;IJ)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 301
    return-void
.end method
