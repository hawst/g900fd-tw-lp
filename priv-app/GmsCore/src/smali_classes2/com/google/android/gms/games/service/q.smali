.class final Lcom/google/android/gms/games/service/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/r;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/c;
    .locals 3

    .prologue
    .line 88
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/ads/identifier/a;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/c;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 93
    :goto_0
    return-object v0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    const-string v1, "GamesSessionRecorder"

    const-string v2, "Failed to retrieve tracking information"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/gms/ads/identifier/c;)V
    .locals 11

    .prologue
    .line 71
    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v2

    .line 74
    const/4 v8, 0x0

    .line 75
    const/4 v9, 0x0

    .line 76
    if-eqz p9, :cond_0

    .line 77
    move-object/from16 v0, p9

    iget-object v8, v0, Lcom/google/android/gms/ads/identifier/c;->a:Ljava/lang/String;

    .line 78
    move-object/from16 v0, p9

    iget-boolean v9, v0, Lcom/google/android/gms/ads/identifier/c;->b:Z

    :cond_0
    move-object v3, p4

    move-wide/from16 v4, p5

    move-wide/from16 v6, p7

    .line 80
    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)V

    .line 82
    return-void
.end method
