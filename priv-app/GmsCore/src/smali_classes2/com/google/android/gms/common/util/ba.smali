.class public final Lcom/google/android/gms/common/util/ba;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Landroid/support/v4/app/q;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/common/util/bd;

.field private e:Lcom/google/android/gms/common/util/bc;

.field private f:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/q;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/util/bd;)V
    .locals 1

    .prologue
    .line 71
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 72
    iput-object p1, p0, Lcom/google/android/gms/common/util/ba;->a:Landroid/support/v4/app/q;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/common/util/ba;->c:Ljava/lang/String;

    .line 75
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/util/bd;

    iput-object v0, p0, Lcom/google/android/gms/common/util/ba;->d:Lcom/google/android/gms/common/util/bd;

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->a:Landroid/support/v4/app/q;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/util/ba;->f:Z

    .line 77
    return-void

    .line 76
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private varargs a()Ljava/lang/String;
    .locals 3

    .prologue
    .line 97
    iget-boolean v0, p0, Lcom/google/android/gms/common/util/ba;->f:Z

    if-nez v0, :cond_0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->c:Ljava/lang/String;

    .line 112
    :goto_0
    return-object v0

    .line 102
    :cond_0
    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "weblogin:continue="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/common/util/ba;->c:Ljava/lang/String;

    invoke-static {v1}, Landroid/net/Uri;->encode(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 103
    iget-object v1, p0, Lcom/google/android/gms/common/util/ba;->a:Landroid/support/v4/app/q;

    iget-object v2, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    .line 112
    :goto_1
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "sarp"

    const-string v2, "1"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 104
    :catch_0
    move-exception v0

    .line 105
    const-string v1, "WebLoginHelper"

    const-string v2, "unable to retrieve token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/util/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/az;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 107
    :catch_1
    move-exception v0

    .line 108
    const-string v1, "WebLoginHelper"

    const-string v2, "unable to retrieve token"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/common/util/ba;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/az;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method

.method private b()V
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->e:Lcom/google/android/gms/common/util/bc;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->e:Lcom/google/android/gms/common/util/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/common/util/bc;->a()V

    .line 135
    :cond_0
    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 61
    invoke-direct {p0}, Lcom/google/android/gms/common/util/ba;->a()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final onCancelled()V
    .locals 1

    .prologue
    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/common/util/ba;->b()V

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->d:Lcom/google/android/gms/common/util/bd;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/bd;->e()V

    .line 123
    return-void
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 61
    check-cast p1, Ljava/lang/String;

    invoke-direct {p0}, Lcom/google/android/gms/common/util/ba;->b()V

    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->d:Lcom/google/android/gms/common/util/bd;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/util/bd;->a(Ljava/lang/String;)V

    return-void
.end method

.method protected final onPreExecute()V
    .locals 5

    .prologue
    .line 81
    iget-boolean v0, p0, Lcom/google/android/gms/common/util/ba;->f:Z

    if-eqz v0, :cond_0

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->a:Landroid/support/v4/app/q;

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->fm:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/common/util/ba;->b:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/google/android/gms/common/util/bc;->a(Ljava/lang/String;)Lcom/google/android/gms/common/util/bc;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/util/ba;->e:Lcom/google/android/gms/common/util/bc;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->e:Lcom/google/android/gms/common/util/bc;

    iget-object v1, p0, Lcom/google/android/gms/common/util/ba;->a:Landroid/support/v4/app/q;

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "WebLoginProgressDialog"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/util/bc;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/common/util/ba;->e:Lcom/google/android/gms/common/util/bc;

    new-instance v1, Lcom/google/android/gms/common/util/bb;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/util/bb;-><init>(Lcom/google/android/gms/common/util/ba;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/util/bc;->a(Landroid/content/DialogInterface$OnCancelListener;)V

    .line 93
    :cond_0
    return-void
.end method
