.class public final Lcom/google/android/gms/common/images/internal/LoadingImageView;
.super Landroid/widget/ImageView;
.source "SourceFile"


# static fields
.field private static a:Lcom/google/android/gms/common/images/ImageManager;


# instance fields
.field private b:Landroid/net/Uri;

.field private c:I

.field private d:Z

.field private e:Z

.field private f:Z

.field private g:I

.field private h:I

.field private i:Lcom/google/android/gms/common/images/internal/g;

.field private j:Lcom/google/android/gms/common/images/g;

.field private k:I

.field private l:F


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 83
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 84
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4

    .prologue
    const/high16 v3, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    .line 91
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/ImageView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 66
    iput v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    .line 69
    iput-boolean v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    .line 71
    iput-boolean v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    .line 73
    iput v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    .line 74
    iput v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    .line 79
    iput v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->k:I

    .line 80
    iput v3, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->l:F

    .line 93
    sget-object v0, Lcom/google/android/gms/r;->G:[I

    invoke-virtual {p1, p2, v0}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 94
    sget v1, Lcom/google/android/gms/r;->J:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->k:I

    .line 96
    sget v1, Lcom/google/android/gms/r;->I:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    iput v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->l:F

    .line 97
    sget v1, Lcom/google/android/gms/r;->H:I

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    .line 98
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Z)V

    .line 99
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 100
    return-void
.end method

.method private b(Landroid/net/Uri;I)V
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 161
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    invoke-virtual {p1, v0}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 162
    :goto_0
    if-eqz v0, :cond_3

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 165
    invoke-direct {p0, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Z)V

    .line 194
    :goto_1
    return-void

    .line 161
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 168
    :cond_2
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    if-ne v0, p2, :cond_3

    .line 170
    invoke-direct {p0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Z)V

    goto :goto_1

    .line 175
    :cond_3
    sget-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    if-nez v0, :cond_4

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 177
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "com.google.android.play.games"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 178
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/gms/common/images/ImageManager;->a(Landroid/content/Context;Z)Lcom/google/android/gms/common/images/ImageManager;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    .line 181
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->e:Z

    if-nez v0, :cond_5

    iget-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    if-eqz v0, :cond_7

    :cond_5
    move v0, v2

    .line 182
    :goto_2
    iput-boolean v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    .line 185
    new-instance v3, Lcom/google/android/gms/common/images/m;

    invoke-direct {v3, p0, p1}, Lcom/google/android/gms/common/images/m;-><init>(Landroid/widget/ImageView;Landroid/net/Uri;)V

    .line 186
    invoke-virtual {v3, p2}, Lcom/google/android/gms/common/images/k;->a(I)V

    .line 187
    iget-boolean v4, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    iput-boolean v4, v3, Lcom/google/android/gms/common/images/k;->f:Z

    .line 188
    iput-boolean v0, v3, Lcom/google/android/gms/common/images/k;->g:Z

    if-eqz v0, :cond_6

    iput-boolean v2, v3, Lcom/google/android/gms/common/images/k;->f:Z

    .line 189
    :cond_6
    iput-boolean v2, v3, Lcom/google/android/gms/common/images/k;->h:Z

    .line 190
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/k;->b(I)V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->j:Lcom/google/android/gms/common/images/g;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/k;->a(Lcom/google/android/gms/common/images/g;)V

    .line 192
    invoke-virtual {v3, v1}, Lcom/google/android/gms/common/images/k;->a(Z)V

    .line 193
    sget-object v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a:Lcom/google/android/gms/common/images/ImageManager;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/images/ImageManager;->a(Lcom/google/android/gms/common/images/k;)V

    goto :goto_1

    :cond_7
    move v0, v1

    .line 181
    goto :goto_2
.end method

.method private b(Z)V
    .locals 2

    .prologue
    .line 348
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->j:Lcom/google/android/gms/common/images/g;

    if-eqz v0, :cond_0

    .line 349
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->j:Lcom/google/android/gms/common/images/g;

    iget-object v1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/images/g;->a(Z)V

    .line 352
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 106
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Landroid/net/Uri;I)V

    .line 107
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->f:Z

    .line 108
    return-void
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 209
    iput p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    .line 210
    return-void
.end method

.method public final a(IF)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 286
    if-eqz p1, :cond_0

    if-eq p1, v2, :cond_0

    const/4 v0, 0x2

    if-ne p1, v0, :cond_1

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 289
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_2

    :goto_1
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 291
    iput p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->k:I

    .line 292
    iput p2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->l:F

    .line 293
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->requestLayout()V

    .line 294
    return-void

    :cond_1
    move v0, v1

    .line 286
    goto :goto_0

    :cond_2
    move v2, v1

    .line 289
    goto :goto_1
.end method

.method public final a(Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Landroid/net/Uri;I)V

    .line 119
    return-void
.end method

.method public final a(Landroid/net/Uri;I)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Landroid/net/Uri;I)V

    .line 131
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/images/g;)V
    .locals 0

    .prologue
    .line 201
    iput-object p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->j:Lcom/google/android/gms/common/images/g;

    .line 202
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 221
    if-eqz p1, :cond_0

    .line 222
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    or-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    .line 226
    :goto_0
    return-void

    .line 224
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    and-int/lit8 v0, v0, -0x2

    iput v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->h:I

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 205
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->c:I

    return v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 236
    const/4 v0, 0x0

    .line 238
    if-lez p1, :cond_0

    .line 239
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 240
    if-eqz v1, :cond_0

    .line 241
    invoke-virtual {v1, p1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    .line 245
    :cond_0
    iput v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/common/images/internal/a;->a:Landroid/graphics/ColorFilter;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->invalidate()V

    .line 246
    return-void

    .line 245
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_0
.end method

.method public final b(Landroid/net/Uri;)V
    .locals 0

    .prologue
    .line 197
    iput-object p1, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b:Landroid/net/Uri;

    .line 198
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 213
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->d:Z

    .line 214
    return-void
.end method

.method protected final onDraw(Landroid/graphics/Canvas;)V
    .locals 1

    .prologue
    .line 337
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->i:Lcom/google/android/gms/common/images/internal/g;

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->i:Lcom/google/android/gms/common/images/internal/g;

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getWidth()I

    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getHeight()I

    invoke-interface {v0}, Lcom/google/android/gms/common/images/internal/g;->a()Landroid/graphics/Path;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->clipPath(Landroid/graphics/Path;)Z

    .line 340
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/ImageView;->onDraw(Landroid/graphics/Canvas;)V

    .line 342
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    if-eqz v0, :cond_1

    .line 343
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->g:I

    invoke-virtual {p1, v0}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 345
    :cond_1
    return-void
.end method

.method protected final onMeasure(II)V
    .locals 3

    .prologue
    .line 311
    invoke-super {p0, p1, p2}, Landroid/widget/ImageView;->onMeasure(II)V

    .line 316
    iget v0, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->k:I

    packed-switch v0, :pswitch_data_0

    .line 333
    :goto_0
    return-void

    .line 318
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getMeasuredHeight()I

    move-result v0

    .line 319
    int-to-float v1, v0

    iget v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->l:F

    mul-float/2addr v1, v2

    float-to-int v1, v1

    .line 332
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setMeasuredDimension(II)V

    goto :goto_0

    .line 322
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->getMeasuredWidth()I

    move-result v1

    .line 323
    int-to-float v0, v1

    iget v2, p0, Lcom/google/android/gms/common/images/internal/LoadingImageView;->l:F

    div-float/2addr v0, v2

    float-to-int v0, v0

    .line 324
    goto :goto_1

    .line 316
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
