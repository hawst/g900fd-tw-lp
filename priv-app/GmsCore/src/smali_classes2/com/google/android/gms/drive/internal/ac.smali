.class final Lcom/google/android/gms/drive/internal/ac;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 271
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 272
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ac;->a:Lcom/google/android/gms/common/api/m;

    .line 273
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ac;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/v;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/v;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 284
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnContentsResponse;)V
    .locals 5

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ac;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/v;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v3, Lcom/google/android/gms/drive/internal/an;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnContentsResponse;->a()Lcom/google/android/gms/drive/Contents;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/internal/an;-><init>(Lcom/google/android/gms/drive/Contents;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/v;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 279
    return-void
.end method
