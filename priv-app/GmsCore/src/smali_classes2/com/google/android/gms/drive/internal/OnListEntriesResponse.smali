.class public Lcom/google/android/gms/drive/internal/OnListEntriesResponse;
.super Lcom/google/android/gms/drive/WriteAwareParcelable;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/common/data/DataHolder;

.field final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/internal/cs;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/cs;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/gms/drive/WriteAwareParcelable;-><init>()V

    .line 53
    iput p1, p0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->a:I

    .line 54
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->b:Lcom/google/android/gms/common/data/DataHolder;

    .line 55
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->c:Z

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Z)V
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;-><init>(ILcom/google/android/gms/common/data/DataHolder;Z)V

    .line 60
    return-void
.end method


# virtual methods
.method protected final a(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 40
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/cs;->a(Lcom/google/android/gms/drive/internal/OnListEntriesResponse;Landroid/os/Parcel;I)V

    .line 41
    return-void
.end method

.method public final b()Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->b:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->c:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x0

    return v0
.end method
