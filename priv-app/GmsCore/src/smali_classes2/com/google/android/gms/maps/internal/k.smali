.class public interface abstract Lcom/google/android/gms/maps/internal/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a()Lcom/google/android/gms/maps/model/CameraPosition;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/TileOverlayOptions;)Lcom/google/android/gms/maps/model/internal/ab;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/CircleOptions;)Lcom/google/android/gms/maps/model/internal/g;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/internal/j;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;Lcom/google/android/gms/maps/model/internal/GroundOverlayOptionsParcelable;)Lcom/google/android/gms/maps/model/internal/j;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/internal/s;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/MarkerOptions;Lcom/google/android/gms/maps/model/internal/MarkerOptionsParcelable;)Lcom/google/android/gms/maps/model/internal/s;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolygonOptions;)Lcom/google/android/gms/maps/model/internal/v;
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/PolylineOptions;)Lcom/google/android/gms/maps/model/internal/y;
.end method

.method public abstract a(I)V
.end method

.method public abstract a(IIII)V
.end method

.method public abstract a(Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/b/l;)V
.end method

.method public abstract a(Lcom/google/android/gms/b/l;ILcom/google/android/gms/maps/internal/e;)V
.end method

.method public abstract a(Lcom/google/android/gms/b/l;Lcom/google/android/gms/maps/internal/e;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/ad;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/ag;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/aj;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/ap;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/as;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/av;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/ay;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/bb;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/be;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/bh;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/bk;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/cc;Lcom/google/android/gms/b/l;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/n;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/q;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/internal/t;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;ILcom/google/android/gms/maps/internal/e;)V
.end method

.method public abstract a(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;Lcom/google/android/gms/maps/internal/e;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Z)V
.end method

.method public abstract b()F
.end method

.method public abstract b(Landroid/os/Bundle;)V
.end method

.method public abstract b(Lcom/google/android/gms/b/l;)V
.end method

.method public abstract b(Lcom/google/android/gms/maps/model/internal/CameraUpdateParcelable;)V
.end method

.method public abstract b(Z)Z
.end method

.method public abstract c()F
.end method

.method public abstract c(Z)V
.end method

.method public abstract d()V
.end method

.method public abstract d(Z)V
.end method

.method public abstract e()V
.end method

.method public abstract f()I
.end method

.method public abstract g()Z
.end method

.method public abstract h()Z
.end method

.method public abstract i()Z
.end method

.method public abstract j()Landroid/location/Location;
.end method

.method public abstract k()Lcom/google/android/gms/maps/internal/co;
.end method

.method public abstract l()Lcom/google/android/gms/maps/internal/bz;
.end method

.method public abstract m()Z
.end method

.method public abstract n()Lcom/google/android/gms/maps/model/internal/m;
.end method

.method public abstract o()V
.end method

.method public abstract p()V
.end method

.method public abstract q()V
.end method

.method public abstract r()V
.end method

.method public abstract s()Z
.end method
