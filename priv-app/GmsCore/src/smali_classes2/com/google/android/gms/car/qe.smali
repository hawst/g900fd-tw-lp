.class final Lcom/google/android/gms/car/qe;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 11
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;)V

    .line 43
    new-instance v0, Lcom/google/android/gms/car/qg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/qg;-><init>(Lcom/google/android/gms/car/qe;)V

    iput-object v0, p0, Lcom/google/android/gms/car/qe;->a:Ljava/lang/Runnable;

    .line 12
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/qe;)V
    .locals 0

    .prologue
    .line 7
    invoke-direct {p0}, Lcom/google/android/gms/car/qe;->c()V

    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 32
    if-nez v0, :cond_0

    .line 41
    :goto_0
    return-void

    .line 35
    :cond_0
    const/16 v1, 0xf06

    invoke-virtual {v0, v1}, Landroid/view/View;->setSystemUiVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 15
    iget-object v0, p0, Lcom/google/android/gms/car/pf;->b:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    .line 16
    if-nez v0, :cond_0

    .line 28
    :goto_0
    return-void

    .line 19
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/qe;->c()V

    .line 20
    new-instance v1, Lcom/google/android/gms/car/qf;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/qf;-><init>(Lcom/google/android/gms/car/qe;)V

    invoke-virtual {v0, v1}, Landroid/view/View;->setOnSystemUiVisibilityChangeListener(Landroid/view/View$OnSystemUiVisibilityChangeListener;)V

    goto :goto_0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/car/qe;->a:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/qe;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/car/qe;->a:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/car/qe;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 53
    return-void
.end method
