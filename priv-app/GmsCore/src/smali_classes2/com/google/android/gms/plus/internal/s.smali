.class public final Lcom/google/android/gms/plus/internal/s;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/plus/model/c/a;

.field private final g:Lcom/google/android/gms/plus/internal/PlusSession;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Lcom/google/android/gms/plus/internal/PlusSession;)V
    .locals 6

    .prologue
    .line 385
    invoke-virtual {p5}, Lcom/google/android/gms/plus/internal/PlusSession;->c()[Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 387
    iput-object p5, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    .line 388
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 49
    invoke-static {p1}, Lcom/google/android/gms/plus/internal/q;->a(Landroid/os/IBinder;)Lcom/google/android/gms/plus/internal/p;

    move-result-object v0

    return-object v0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 655
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 657
    const-string v0, "loaded_person"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/plus/internal/model/people/PersonEntity;->a([B)Lcom/google/android/gms/plus/internal/model/people/PersonEntity;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/plus/internal/s;->a:Lcom/google/android/gms/plus/model/c/a;

    .line 659
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 660
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/internal/PlusSession;->k()Landroid/os/Bundle;

    move-result-object v0

    .line 643
    const-string v1, "request_visible_actions"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->d()[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 644
    const-string v1, "auth_package"

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 645
    new-instance v1, Lcom/google/android/gms/common/internal/GetServiceRequest;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/plus/internal/s;->g:Lcom/google/android/gms/plus/internal/PlusSession;

    invoke-virtual {v2}, Lcom/google/android/gms/plus/internal/PlusSession;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/internal/d;->a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/d;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Lcom/google/android/gms/common/internal/ba;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/util/as;->a([Ljava/lang/String;)[Lcom/google/android/gms/common/api/Scope;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a([Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/GetServiceRequest;->a(Landroid/os/Bundle;)Lcom/google/android/gms/common/internal/GetServiceRequest;

    move-result-object v0

    .line 650
    invoke-interface {p1, p2, v0}, Lcom/google/android/gms/common/internal/bj;->b(Lcom/google/android/gms/common/internal/bg;Lcom/google/android/gms/common/internal/GetServiceRequest;)V

    .line 651
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 1

    .prologue
    .line 613
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/s;->j()V

    .line 614
    invoke-virtual {p0}, Lcom/google/android/gms/plus/internal/s;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/internal/p;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/plus/internal/p;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V

    .line 615
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 636
    const-string v0, "com.google.android.gms.plus.service.START"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 631
    const-string v0, "com.google.android.gms.plus.internal.IPlusService"

    return-object v0
.end method
