.class public final Lcom/google/android/gms/people/identity/p;
.super Lcom/google/android/gms/common/data/i;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 0

    .prologue
    .line 429
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/data/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 430
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)I
    .locals 1

    .prologue
    .line 433
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/identity/p;->c_(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 434
    const/4 v0, -0x1

    .line 436
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/i;->c(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 454
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/identity/p;->c_(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 455
    const/4 v0, 0x0

    .line 457
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/i;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Z)Z
    .locals 1

    .prologue
    .line 441
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/identity/p;->c_(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 442
    const/4 v0, 0x0

    .line 444
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/common/data/i;->d(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/people/identity/p;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
