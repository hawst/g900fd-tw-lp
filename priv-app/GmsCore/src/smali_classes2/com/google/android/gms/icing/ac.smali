.class public final Lcom/google/android/gms/icing/ac;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11781
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11782
    const/16 v0, 0x80

    iput v0, p0, Lcom/google/android/gms/icing/ac;->a:I

    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/gms/icing/ac;->b:I

    const/16 v0, 0x8

    iput v0, p0, Lcom/google/android/gms/icing/ac;->c:I

    const/16 v0, 0xc0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->d:I

    const/16 v0, 0x40

    iput v0, p0, Lcom/google/android/gms/icing/ac;->e:I

    const/16 v0, 0x3e8

    iput v0, p0, Lcom/google/android/gms/icing/ac;->f:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/ac;->cachedSize:I

    .line 11783
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11864
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 11865
    iget v1, p0, Lcom/google/android/gms/icing/ac;->a:I

    const/16 v2, 0x80

    if-eq v1, v2, :cond_0

    .line 11866
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/ac;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11869
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/ac;->b:I

    const/16 v2, 0x10

    if-eq v1, v2, :cond_1

    .line 11870
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/ac;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11873
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/ac;->c:I

    const/16 v2, 0x8

    if-eq v1, v2, :cond_2

    .line 11874
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/ac;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11877
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/ac;->d:I

    const/16 v2, 0xc0

    if-eq v1, v2, :cond_3

    .line 11878
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/ac;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11881
    :cond_3
    iget v1, p0, Lcom/google/android/gms/icing/ac;->e:I

    const/16 v2, 0x40

    if-eq v1, v2, :cond_4

    .line 11882
    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/icing/ac;->e:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11885
    :cond_4
    iget v1, p0, Lcom/google/android/gms/icing/ac;->f:I

    const/16 v2, 0x3e8

    if-eq v1, v2, :cond_5

    .line 11886
    const/4 v1, 0x6

    iget v2, p0, Lcom/google/android/gms/icing/ac;->f:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 11889
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11798
    if-ne p1, p0, :cond_1

    .line 11823
    :cond_0
    :goto_0
    return v0

    .line 11801
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/ac;

    if-nez v2, :cond_2

    move v0, v1

    .line 11802
    goto :goto_0

    .line 11804
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/ac;

    .line 11805
    iget v2, p0, Lcom/google/android/gms/icing/ac;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 11806
    goto :goto_0

    .line 11808
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/ac;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 11809
    goto :goto_0

    .line 11811
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/ac;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 11812
    goto :goto_0

    .line 11814
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/ac;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 11815
    goto :goto_0

    .line 11817
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/ac;->e:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->e:I

    if-eq v2, v3, :cond_7

    move v0, v1

    .line 11818
    goto :goto_0

    .line 11820
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/ac;->f:I

    iget v3, p1, Lcom/google/android/gms/icing/ac;->f:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 11821
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 11828
    iget v0, p0, Lcom/google/android/gms/icing/ac;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 11830
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ac;->b:I

    add-int/2addr v0, v1

    .line 11831
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ac;->c:I

    add-int/2addr v0, v1

    .line 11832
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ac;->d:I

    add-int/2addr v0, v1

    .line 11833
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ac;->e:I

    add-int/2addr v0, v1

    .line 11834
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/ac;->f:I

    add-int/2addr v0, v1

    .line 11835
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11746
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->e:I

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/ac;->f:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x28 -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11841
    iget v0, p0, Lcom/google/android/gms/icing/ac;->a:I

    const/16 v1, 0x80

    if-eq v0, v1, :cond_0

    .line 11842
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/ac;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11844
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/ac;->b:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    .line 11845
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/ac;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11847
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/ac;->c:I

    const/16 v1, 0x8

    if-eq v0, v1, :cond_2

    .line 11848
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/ac;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11850
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/ac;->d:I

    const/16 v1, 0xc0

    if-eq v0, v1, :cond_3

    .line 11851
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/ac;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11853
    :cond_3
    iget v0, p0, Lcom/google/android/gms/icing/ac;->e:I

    const/16 v1, 0x40

    if-eq v0, v1, :cond_4

    .line 11854
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/icing/ac;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11856
    :cond_4
    iget v0, p0, Lcom/google/android/gms/icing/ac;->f:I

    const/16 v1, 0x3e8

    if-eq v0, v1, :cond_5

    .line 11857
    const/4 v0, 0x6

    iget v1, p0, Lcom/google/android/gms/icing/ac;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 11859
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11860
    return-void
.end method
