.class final Lcom/google/android/gms/common/api/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/z;)V
    .locals 0

    .prologue
    .line 267
    iput-object p1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 273
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 283
    :goto_0
    return-void

    .line 277
    :cond_0
    if-eqz p1, :cond_1

    .line 278
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->k:Landroid/os/Bundle;

    invoke-virtual {v0, p1}, Landroid/os/Bundle;->putAll(Landroid/os/Bundle;)V

    .line 280
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-static {v0}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/z;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final f_(I)V
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 290
    packed-switch p1, :pswitch_data_0

    .line 324
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 325
    :goto_1
    return-void

    .line 293
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/api/z;->a(I)V

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 324
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 299
    :pswitch_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-boolean v0, v0, Lcom/google/android/gms/common/api/z;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_0

    .line 324
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 303
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/common/api/z;->f:Z

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    new-instance v1, Lcom/google/android/gms/common/api/ae;

    iget-object v2, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/ae;-><init>(Lcom/google/android/gms/common/api/z;)V

    iput-object v1, v0, Lcom/google/android/gms/common/api/z;->j:Landroid/content/BroadcastReceiver;

    .line 306
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 308
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 309
    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v2, v2, Lcom/google/android/gms/common/api/z;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-wide v2, v2, Lcom/google/android/gms/common/api/z;->g:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    iget-wide v2, v2, Lcom/google/android/gms/common/api/z;->h:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/common/api/ab;->a:Lcom/google/android/gms/common/api/z;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/z;I)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 290
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
