.class public final Lcom/google/android/gms/backup/af;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/ae/a/a;


# static fields
.field private static final a:Z


# instance fields
.field private b:Ljava/io/InputStream;

.field private c:J

.field private d:J

.field private e:J

.field private f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private g:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private h:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final i:Ljava/util/concurrent/locks/Lock;

.field private final j:Ljava/util/concurrent/locks/Condition;

.field private final k:Ljava/util/concurrent/locks/Condition;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 24
    const-string v0, "GmsBackupTransport"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/backup/af;->a:Z

    return-void
.end method

.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const-wide/16 v2, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    .line 35
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->k:Ljava/util/concurrent/locks/Condition;

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/backup/af;->b:Ljava/io/InputStream;

    .line 41
    iput-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    .line 42
    iput-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    .line 43
    iput-wide v2, p0, Lcom/google/android/gms/backup/af;->e:J

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 45
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 46
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v4}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 47
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 90
    sget-boolean v0, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GmsBackupTransport"

    const-string v1, "[FINISH] signal no more data."

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->b:Ljava/io/InputStream;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(I)I
    .locals 6

    .prologue
    const/4 v0, -0x1

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 59
    :try_start_0
    sget-boolean v1, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "GmsBackupTransport"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "[PUSH] Push "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bytes into pipe."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    int-to-long v4, p1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    .line 62
    sget-boolean v1, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v1, :cond_1

    const-string v1, "GmsBackupTransport"

    const-string v2, "[PUSH] signal data available."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 67
    :goto_0
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/backup/af;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_3

    .line 68
    sget-boolean v1, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v1, :cond_2

    const-string v1, "GmsBackupTransport"

    const-string v2, "[PUSH] Wait for data been processed."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->k:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 78
    :catch_0
    move-exception v1

    :try_start_1
    const-string v1, "GmsBackupTransport"

    const-string v2, "InterruptedException when waiting for data processed"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 79
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return v0

    .line 72
    :cond_3
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_4

    .line 73
    const-string v1, "GmsBackupTransport"

    const-string v2, "Data stream has transfer exception or unexpected http response."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 76
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const/4 v0, 0x0

    goto :goto_1

    .line 79
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a([BII)I
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 132
    sget-boolean v1, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v1, :cond_0

    const-string v1, "GmsBackupTransport"

    const-string v2, "[READ]"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    array-length v1, p1

    sub-int/2addr v1, p2

    if-ge v1, p3, :cond_1

    .line 134
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Buffer length must be greater than desired number of bytes."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 137
    :cond_1
    if-nez p3, :cond_2

    .line 176
    :goto_0
    return v0

    .line 141
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 148
    :goto_1
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/backup/af;->c:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-nez v1, :cond_4

    .line 149
    sget-boolean v1, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v1, :cond_3

    const-string v1, "GmsBackupTransport"

    const-string v2, "[READ] wait for avaible data."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 150
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 179
    :catch_0
    move-exception v0

    .line 180
    :try_start_1
    const-string v1, "GmsBackupTransport"

    const-string v2, "InterruptedException when waiting for available data in read method."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 181
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 183
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 153
    :cond_4
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 154
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Transfer cancelled by framework."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 157
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v1

    if-nez v1, :cond_6

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    .line 161
    :cond_6
    :try_start_3
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/backup/af;->d:J

    sub-long/2addr v2, v4

    long-to-int v1, v2

    invoke-static {v1, p3}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 162
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->b:Ljava/io/InputStream;

    invoke-virtual {v2, p1, p2, v1}, Ljava/io/InputStream;->read([BII)I

    move-result v1

    .line 164
    sget-boolean v2, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v2, :cond_7

    const-string v2, "GmsBackupTransport"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "[READ] Read "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " bytes data."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 165
    :cond_7
    const/4 v2, -0x1

    if-eq v1, v2, :cond_a

    .line 166
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    int-to-long v4, v1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    .line 169
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/backup/af;->d:J

    cmp-long v0, v2, v4

    if-nez v0, :cond_9

    .line 170
    sget-boolean v0, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v0, :cond_8

    const-string v0, "GmsBackupTransport"

    const-string v2, "[READ] signal data processed."

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 171
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->k:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 174
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move v0, v1

    goto/16 :goto_0

    .line 176
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto/16 :goto_0
.end method

.method public final a(J)J
    .locals 7

    .prologue
    const-wide/16 v0, 0x0

    .line 245
    sget-boolean v2, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v2, :cond_0

    const-string v2, "GmsBackupTransport"

    const-string v3, "[SKIP]"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :cond_0
    cmp-long v2, p1, v0

    if-gez v2, :cond_1

    .line 247
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Can\'t skip negative bytes."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 250
    :cond_1
    cmp-long v2, p1, v0

    if-nez v2, :cond_2

    .line 282
    :goto_0
    return-wide p1

    .line 254
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 261
    :goto_1
    :try_start_0
    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    iget-wide v4, p0, Lcom/google/android/gms/backup/af;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-nez v2, :cond_3

    .line 262
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 283
    :catch_0
    move-exception v0

    .line 284
    :try_start_1
    const-string v1, "GmsBackupTransport"

    const-string v2, "InterruptedException when waiting for available data in skip method."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 285
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 287
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 265
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 266
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Transfer cancelled by framework."

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 269
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result v2

    if-nez v2, :cond_5

    .line 270
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    move-wide p1, v0

    goto :goto_0

    .line 273
    :cond_5
    :try_start_3
    iget-wide v0, p0, Lcom/google/android/gms/backup/af;->c:J

    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->d:J

    sub-long/2addr v0, v2

    invoke-static {v0, v1, p1, p2}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v0

    .line 274
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->b:Ljava/io/InputStream;

    invoke-virtual {v2, v0, v1}, Ljava/io/InputStream;->skip(J)J

    move-result-wide p1

    .line 276
    iget-wide v0, p0, Lcom/google/android/gms/backup/af;->d:J

    add-long/2addr v0, p1

    iput-wide v0, p0, Lcom/google/android/gms/backup/af;->d:J

    .line 278
    iget-wide v0, p0, Lcom/google/android/gms/backup/af;->d:J

    iget-wide v2, p0, Lcom/google/android/gms/backup/af;->c:J

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->k:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 282
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 105
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->g:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->k:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 109
    return-void

    .line 108
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 119
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->h:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->j:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 123
    return-void

    .line 122
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 195
    return-void
.end method

.method public final e()J
    .locals 3

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 212
    :try_start_0
    iget-wide v0, p0, Lcom/google/android/gms/backup/af;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 214
    iget-object v2, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-wide v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const-wide v0, 0x7fffffffffffffffL

    return-wide v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    const-wide/16 v0, -0x1

    return-wide v0
.end method

.method public final h()Z
    .locals 3

    .prologue
    .line 304
    sget-boolean v0, Lcom/google/android/gms/backup/af;->a:Z

    if-eqz v0, :cond_0

    const-string v0, "GmsBackupTransport"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "[HAS MORE DATA ]"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 307
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 309
    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final i()V
    .locals 2

    .prologue
    .line 315
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 317
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->b:Ljava/io/InputStream;

    invoke-virtual {v0}, Ljava/io/InputStream;->close()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 320
    return-void

    .line 319
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/af;->i:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
