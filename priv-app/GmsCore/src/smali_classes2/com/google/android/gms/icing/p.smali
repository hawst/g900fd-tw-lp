.class public final Lcom/google/android/gms/icing/p;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/icing/p;


# instance fields
.field public a:I

.field public b:I

.field public c:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 4017
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4018
    iput v0, p0, Lcom/google/android/gms/icing/p;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/p;->b:I

    iput-boolean v0, p0, Lcom/google/android/gms/icing/p;->c:Z

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/p;->cachedSize:I

    .line 4019
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/p;
    .locals 2

    .prologue
    .line 3997
    sget-object v0, Lcom/google/android/gms/icing/p;->d:[Lcom/google/android/gms/icing/p;

    if-nez v0, :cond_1

    .line 3998
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4000
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/p;->d:[Lcom/google/android/gms/icing/p;

    if-nez v0, :cond_0

    .line 4001
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/p;

    sput-object v0, Lcom/google/android/gms/icing/p;->d:[Lcom/google/android/gms/icing/p;

    .line 4003
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4005
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/p;->d:[Lcom/google/android/gms/icing/p;

    return-object v0

    .line 4003
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4076
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4077
    iget v1, p0, Lcom/google/android/gms/icing/p;->a:I

    if-eqz v1, :cond_0

    .line 4078
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/p;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4081
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/p;->b:I

    if-eqz v1, :cond_1

    .line 4082
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/p;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4085
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/p;->c:Z

    if-eqz v1, :cond_2

    .line 4086
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/p;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 4089
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4031
    if-ne p1, p0, :cond_1

    .line 4047
    :cond_0
    :goto_0
    return v0

    .line 4034
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/p;

    if-nez v2, :cond_2

    move v0, v1

    .line 4035
    goto :goto_0

    .line 4037
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/p;

    .line 4038
    iget v2, p0, Lcom/google/android/gms/icing/p;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/p;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4039
    goto :goto_0

    .line 4041
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/p;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/p;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 4042
    goto :goto_0

    .line 4044
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/p;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/p;->c:Z

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 4045
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4052
    iget v0, p0, Lcom/google/android/gms/icing/p;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4054
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/p;->b:I

    add-int/2addr v0, v1

    .line 4055
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/p;->c:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 4056
    return v0

    .line 4055
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3991
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/p;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/p;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/p;->c:Z

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4062
    iget v0, p0, Lcom/google/android/gms/icing/p;->a:I

    if-eqz v0, :cond_0

    .line 4063
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/p;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4065
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/p;->b:I

    if-eqz v0, :cond_1

    .line 4066
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/p;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 4068
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/p;->c:Z

    if-eqz v0, :cond_2

    .line 4069
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/p;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 4071
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4072
    return-void
.end method
