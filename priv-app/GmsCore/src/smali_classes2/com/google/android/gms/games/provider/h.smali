.class Lcom/google/android/gms/games/provider/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field final synthetic b:Lcom/google/android/gms/games/provider/GamesContentProvider;

.field private final c:Ljava/lang/String;

.field private final d:[Ljava/lang/String;

.field private final e:Ljava/util/HashMap;

.field private final f:[Lcom/google/android/gms/games/provider/i;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 4192
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v5, v4

    move-object v6, v4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    .line 4193
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 4188
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    .line 4189
    return-void
.end method

.method constructor <init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 4179
    iput-object p1, p0, Lcom/google/android/gms/games/provider/h;->b:Lcom/google/android/gms/games/provider/GamesContentProvider;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 4180
    iput-object p3, p0, Lcom/google/android/gms/games/provider/h;->c:Ljava/lang/String;

    .line 4181
    iput-object p2, p0, Lcom/google/android/gms/games/provider/h;->a:Ljava/lang/String;

    .line 4182
    iput-object p4, p0, Lcom/google/android/gms/games/provider/h;->d:[Ljava/lang/String;

    .line 4183
    iput-object p6, p0, Lcom/google/android/gms/games/provider/h;->e:Ljava/util/HashMap;

    .line 4184
    iput-object p5, p0, Lcom/google/android/gms/games/provider/h;->f:[Lcom/google/android/gms/games/provider/i;

    .line 4185
    return-void
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/provider/ay;J)I
    .locals 12

    .prologue
    .line 4204
    iget-object v0, p1, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/az;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 4205
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->d:[Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 4207
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->b:Lcom/google/android/gms/games/provider/GamesContentProvider;

    iget-object v3, p0, Lcom/google/android/gms/games/provider/h;->a:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/provider/h;->d:[Ljava/lang/String;

    move-object v2, p1

    move-wide v4, p2

    invoke-static/range {v1 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/GamesContentProvider;Lcom/google/android/gms/games/provider/ay;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4208
    const/4 v0, 0x0

    .line 4242
    :goto_0
    return v0

    .line 4212
    :cond_0
    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v4, v1

    .line 4214
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->f:[Lcom/google/android/gms/games/provider/i;

    if-eqz v1, :cond_3

    .line 4216
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/provider/h;->f:[Lcom/google/android/gms/games/provider/i;

    array-length v9, v2

    move v8, v1

    :goto_1
    if-ge v8, v9, :cond_3

    .line 4217
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->f:[Lcom/google/android/gms/games/provider/i;

    aget-object v10, v1, v8

    .line 4218
    iget-object v1, v10, Lcom/google/android/gms/games/provider/i;->e:Ljava/lang/String;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    iget-object v5, v10, Lcom/google/android/gms/games/provider/i;->c:Ljava/lang/String;

    aput-object v5, v2, v3

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v5, v10, Lcom/google/android/gms/games/provider/i;->d:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=?"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 4221
    :goto_2
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 4222
    iget-object v2, v10, Lcom/google/android/gms/games/provider/i;->f:Lcom/google/android/gms/games/provider/h;

    if-nez v2, :cond_1

    .line 4223
    iget-object v2, v10, Lcom/google/android/gms/games/provider/i;->b:Ljava/lang/String;

    iget-object v3, v10, Lcom/google/android/gms/games/provider/i;->a:Ljava/lang/String;

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {v0, v2, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 4230
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    .line 4226
    :cond_1
    :try_start_1
    iget-object v2, v10, Lcom/google/android/gms/games/provider/i;->f:Lcom/google/android/gms/games/provider/h;

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-virtual {v2, p1, v6, v7}, Lcom/google/android/gms/games/provider/h;->a(Lcom/google/android/gms/games/provider/ay;J)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 4230
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4216
    add-int/lit8 v1, v8, 0x1

    move v8, v1

    goto :goto_1

    .line 4236
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->e:Ljava/util/HashMap;

    if-eqz v1, :cond_4

    .line 4237
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->e:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 4238
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/games/provider/h;->e:Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v5, "=?"

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_3

    .line 4242
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/provider/h;->a:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/google/android/gms/games/provider/h;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0
.end method
