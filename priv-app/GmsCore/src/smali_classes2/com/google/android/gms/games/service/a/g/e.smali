.class public final Lcom/google/android/gms/games/service/a/g/e;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;

.field private final f:J

.field private final g:J

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JJLjava/lang/String;)V
    .locals 2

    .prologue
    .line 26
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/g/e;->c:Lcom/google/android/gms/games/internal/dr;

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/g/e;->d:Lcom/google/android/gms/games/a/au;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/g/e;->e:Ljava/lang/String;

    .line 30
    iput-wide p4, p0, Lcom/google/android/gms/games/service/a/g/e;->f:J

    .line 31
    iput-wide p6, p0, Lcom/google/android/gms/games/service/a/g/e;->g:J

    .line 32
    iput-object p8, p0, Lcom/google/android/gms/games/service/a/g/e;->h:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/e;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    .line 48
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-lez v0, :cond_1

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/e;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->d(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 67
    :cond_0
    :goto_0
    return-void

    .line 56
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/e/q;

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/g/e;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/g/e;->d:Lcom/google/android/gms/games/a/au;

    iget-object v3, v3, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/e/q;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    .line 58
    invoke-virtual {v0}, Lcom/google/android/gms/games/e/q;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 62
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/e;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/dr;->d(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/e;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    const/4 v9, 0x1

    .line 39
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/g/e;->d:Lcom/google/android/gms/games/a/au;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/g/e;->e:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/games/service/a/g/e;->f:J

    iget-wide v6, p0, Lcom/google/android/gms/games/service/a/g/e;->g:J

    iget-object v8, p0, Lcom/google/android/gms/games/service/a/g/e;->h:Ljava/lang/String;

    move-object v1, p2

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/google/android/gms/games/e/q;

    move-result-object v0

    .line 41
    invoke-virtual {v0}, Lcom/google/android/gms/games/e/q;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 38
    :cond_0
    const/4 v9, 0x0

    goto :goto_0
.end method
