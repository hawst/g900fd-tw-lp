.class public final Lcom/google/android/gms/games/internal/a/cr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/multiplayer/turnbased/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 152
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;I[I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 361
    new-instance v0, Lcom/google/android/gms/games/internal/a/cx;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/cx;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;I[I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 223
    new-instance v0, Lcom/google/android/gms/games/internal/a/cv;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/cv;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 387
    new-instance v0, Lcom/google/android/gms/games/internal/a/cs;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/cs;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;[I)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 429
    new-instance v0, Lcom/google/android/gms/games/internal/a/cu;

    const/4 v4, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/cu;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I[I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;[I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 354
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0, p2}, Lcom/google/android/gms/games/internal/a/cr;->a(Lcom/google/android/gms/common/api/v;I[I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 192
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->o()V

    .line 193
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/turnbased/b;)V
    .locals 2

    .prologue
    .line 185
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 187
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->b(Lcom/google/android/gms/common/api/aj;)V

    .line 188
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/turnbased/b;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 441
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 443
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->b(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V

    .line 445
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 234
    new-instance v0, Lcom/google/android/gms/games/internal/a/cw;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/cw;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 398
    new-instance v0, Lcom/google/android/gms/games/internal/a/ct;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/ct;-><init>(Lcom/google/android/gms/games/internal/a/cr;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 244
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;I)V

    .line 246
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 409
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;Ljava/lang/String;I)V

    .line 411
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 250
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, v1}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;I)V

    .line 252
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 416
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, p2, p3, v1}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;Ljava/lang/String;I)V

    .line 418
    return-void
.end method

.method public final e(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 348
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->b(Ljava/lang/String;)V

    .line 349
    return-void
.end method

.method public final e(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 422
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/games/internal/c;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 423
    return-void
.end method

.method public final f(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 449
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->d(Ljava/lang/String;)V

    .line 450
    return-void
.end method
