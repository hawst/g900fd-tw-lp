.class final Lcom/google/android/gms/common/internal/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Z

.field final b:Lcom/google/android/gms/common/internal/ah;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/common/internal/ah;)V
    .locals 1

    .prologue
    .line 435
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/common/internal/aa;-><init>(Lcom/google/android/gms/common/internal/ah;Z)V

    .line 436
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/internal/ah;Z)V
    .locals 0

    .prologue
    .line 438
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 439
    iput-object p1, p0, Lcom/google/android/gms/common/internal/aa;->b:Lcom/google/android/gms/common/internal/ah;

    .line 440
    iput-boolean p2, p0, Lcom/google/android/gms/common/internal/aa;->a:Z

    .line 441
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/common/internal/aa;
    .locals 2

    .prologue
    .line 444
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 445
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "separator may not be empty or null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :cond_1
    new-instance v0, Lcom/google/android/gms/common/internal/aa;

    new-instance v1, Lcom/google/android/gms/common/internal/ab;

    invoke-direct {v1, p0}, Lcom/google/android/gms/common/internal/ab;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/aa;-><init>(Lcom/google/android/gms/common/internal/ah;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/internal/aa;
    .locals 3

    .prologue
    .line 480
    new-instance v0, Lcom/google/android/gms/common/internal/aa;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/aa;->b:Lcom/google/android/gms/common/internal/ah;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/internal/aa;-><init>(Lcom/google/android/gms/common/internal/ah;Z)V

    return-object v0
.end method

.method public final a(Ljava/lang/CharSequence;)Ljava/lang/Iterable;
    .locals 1

    .prologue
    .line 484
    new-instance v0, Lcom/google/android/gms/common/internal/ad;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/internal/ad;-><init>(Lcom/google/android/gms/common/internal/aa;Ljava/lang/CharSequence;)V

    return-object v0
.end method
