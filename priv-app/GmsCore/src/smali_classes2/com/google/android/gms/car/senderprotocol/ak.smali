.class public final Lcom/google/android/gms/car/senderprotocol/ak;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/al;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ey;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 55
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ak;->a:Lcom/google/android/gms/car/senderprotocol/al;

    .line 56
    return-void
.end method


# virtual methods
.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ak;->a:Lcom/google/android/gms/car/senderprotocol/al;

    if-nez v0, :cond_0

    .line 155
    :goto_0
    return-void

    .line 145
    :cond_0
    const v0, 0x8006

    if-ne p1, v0, :cond_1

    .line 146
    new-instance v0, Lcom/google/android/c/b/au;

    invoke-direct {v0}, Lcom/google/android/c/b/au;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/au;

    .line 148
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ak;->a:Lcom/google/android/gms/car/senderprotocol/al;

    iget-object v2, v0, Lcom/google/android/c/b/au;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/c/b/au;->a:Lcom/google/android/c/b/an;

    iget v0, v0, Lcom/google/android/c/b/an;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/car/senderprotocol/al;->a(Ljava/lang/String;I)V

    goto :goto_0

    .line 149
    :cond_1
    const v0, 0x8005

    if-ne p1, v0, :cond_2

    .line 150
    new-instance v0, Lcom/google/android/c/b/aw;

    invoke-direct {v0}, Lcom/google/android/c/b/aw;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ak;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/aw;

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ak;->a:Lcom/google/android/gms/car/senderprotocol/al;

    iget-object v2, v0, Lcom/google/android/c/b/aw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/c/b/aw;->a()I

    move-result v3

    invoke-virtual {v0}, Lcom/google/android/c/b/aw;->b()Z

    move-result v0

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/car/senderprotocol/al;->a(Ljava/lang/String;IZ)V

    goto :goto_0

    .line 153
    :cond_2
    const-string v0, "CAR.INST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid message type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/CarMediaBrowserListNode;)V
    .locals 4

    .prologue
    .line 99
    new-instance v1, Lcom/google/android/c/b/ay;

    invoke-direct {v1}, Lcom/google/android/c/b/ay;-><init>()V

    .line 100
    new-instance v0, Lcom/google/android/c/b/ax;

    invoke-direct {v0}, Lcom/google/android/c/b/ax;-><init>()V

    iput-object v0, v1, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    .line 101
    iget-object v0, v1, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    .line 102
    iget-object v0, v1, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->e:I

    iput v2, v0, Lcom/google/android/c/b/ax;->b:I

    .line 103
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget-object v0, v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 104
    iget-object v0, v1, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->c:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/c/b/ax;->a(Ljava/lang/String;)Lcom/google/android/c/b/ax;

    .line 106
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget-object v0, v0, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->d:[B

    if-eqz v0, :cond_1

    .line 107
    iget-object v0, v1, Lcom/google/android/c/b/ay;->a:Lcom/google/android/c/b/ax;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->b:Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->d:[B

    invoke-virtual {v0, v2}, Lcom/google/android/c/b/ax;->a([B)Lcom/google/android/c/b/ax;

    .line 109
    :cond_1
    iget v0, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->c:I

    invoke-virtual {v1, v0}, Lcom/google/android/c/b/ay;->a(I)Lcom/google/android/c/b/ay;

    .line 110
    iget v0, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->d:I

    invoke-virtual {v1, v0}, Lcom/google/android/c/b/ay;->b(I)Lcom/google/android/c/b/ay;

    .line 111
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/c/b/be;

    iput-object v0, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    .line 112
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 113
    iget-object v2, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    new-instance v3, Lcom/google/android/c/b/be;

    invoke-direct {v3}, Lcom/google/android/c/b/be;-><init>()V

    aput-object v3, v2, v0

    .line 114
    iget-object v2, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    .line 115
    iget-object v2, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->c:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    .line 116
    iget-object v2, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/be;->b(Ljava/lang/String;)Lcom/google/android/c/b/be;

    .line 117
    iget-object v2, v1, Lcom/google/android/c/b/ay;->b:[Lcom/google/android/c/b/be;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserListNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/be;->a(Ljava/lang/String;)Lcom/google/android/c/b/be;

    .line 112
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_2
    const v0, 0x8003

    invoke-static {v1}, Lcom/google/android/c/b/ay;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ak;->a(I[B)V

    .line 120
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarMediaBrowserRootNode;)V
    .locals 4

    .prologue
    .line 59
    new-instance v1, Lcom/google/android/c/b/bc;

    invoke-direct {v1}, Lcom/google/android/c/b/bc;-><init>()V

    .line 60
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->b:Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/c/b/bc;->a:Ljava/lang/String;

    .line 61
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/c/b/bg;

    iput-object v0, v1, Lcom/google/android/c/b/bc;->b:[Lcom/google/android/c/b/bg;

    .line 62
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 63
    iget-object v2, v1, Lcom/google/android/c/b/bc;->b:[Lcom/google/android/c/b/bg;

    new-instance v3, Lcom/google/android/c/b/bg;

    invoke-direct {v3}, Lcom/google/android/c/b/bg;-><init>()V

    aput-object v3, v2, v0

    .line 64
    iget-object v2, v1, Lcom/google/android/c/b/bc;->b:[Lcom/google/android/c/b/bg;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    .line 65
    iget-object v2, v1, Lcom/google/android/c/b/bc;->b:[Lcom/google/android/c/b/bg;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->c:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    .line 66
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    if-eqz v2, :cond_0

    .line 67
    iget-object v2, v1, Lcom/google/android/c/b/bc;->b:[Lcom/google/android/c/b/bg;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserRootNode;->c:[Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/bg;->a([B)Lcom/google/android/c/b/bg;

    .line 62
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 70
    :cond_1
    const v0, 0x8001

    invoke-static {v1}, Lcom/google/android/c/b/bc;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ak;->a(I[B)V

    .line 71
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarMediaBrowserSongNode;)V
    .locals 3

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/c/b/bf;

    invoke-direct {v0}, Lcom/google/android/c/b/bf;-><init>()V

    .line 124
    new-instance v1, Lcom/google/android/c/b/be;

    invoke-direct {v1}, Lcom/google/android/c/b/be;-><init>()V

    iput-object v1, v0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    .line 125
    iget-object v1, v0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->b:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/c/b/be;->a:Ljava/lang/String;

    .line 126
    iget-object v1, v0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/c/b/be;->b:Ljava/lang/String;

    .line 127
    iget-object v1, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v1, v1, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->e:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 128
    iget-object v1, v0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/c/b/be;->b(Ljava/lang/String;)Lcom/google/android/c/b/be;

    .line 130
    :cond_0
    iget-object v1, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v1, v1, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->d:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 131
    iget-object v1, v0, Lcom/google/android/c/b/bf;->a:Lcom/google/android/c/b/be;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->b:Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserListNode$CarMediaSong;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/c/b/be;->a(Ljava/lang/String;)Lcom/google/android/c/b/be;

    .line 133
    :cond_1
    iget-object v1, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->c:[B

    if-eqz v1, :cond_2

    .line 134
    iget-object v1, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->c:[B

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/bf;->a([B)Lcom/google/android/c/b/bf;

    .line 136
    :cond_2
    iget v1, p1, Lcom/google/android/gms/car/CarMediaBrowserSongNode;->d:I

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/bf;->a(I)Lcom/google/android/c/b/bf;

    .line 137
    const v1, 0x8004

    invoke-static {v0}, Lcom/google/android/c/b/bf;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/ak;->a(I[B)V

    .line 138
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarMediaBrowserSourceNode;)V
    .locals 4

    .prologue
    .line 74
    new-instance v1, Lcom/google/android/c/b/bh;

    invoke-direct {v1}, Lcom/google/android/c/b/bh;-><init>()V

    .line 75
    new-instance v0, Lcom/google/android/c/b/bg;

    invoke-direct {v0}, Lcom/google/android/c/b/bg;-><init>()V

    iput-object v0, v1, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    .line 76
    iget-object v0, v1, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->b:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/c/b/bg;->a:Ljava/lang/String;

    .line 77
    iget-object v0, v1, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->c:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/c/b/bg;->b:Ljava/lang/String;

    .line 78
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    iget-object v0, v0, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    if-eqz v0, :cond_0

    .line 79
    iget-object v0, v1, Lcom/google/android/c/b/bh;->a:Lcom/google/android/c/b/bg;

    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->b:Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserRootNode$CarMediaSource;->d:[B

    invoke-virtual {v0, v2}, Lcom/google/android/c/b/bg;->a([B)Lcom/google/android/c/b/bg;

    .line 81
    :cond_0
    iget v0, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->c:I

    invoke-virtual {v1, v0}, Lcom/google/android/c/b/bh;->a(I)Lcom/google/android/c/b/bh;

    .line 82
    iget v0, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->d:I

    invoke-virtual {v1, v0}, Lcom/google/android/c/b/bh;->b(I)Lcom/google/android/c/b/bh;

    .line 83
    iget-object v0, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/c/b/ax;

    iput-object v0, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    .line 84
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    array-length v2, v2

    if-ge v0, v2, :cond_3

    .line 85
    iget-object v2, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    new-instance v3, Lcom/google/android/c/b/ax;

    invoke-direct {v3}, Lcom/google/android/c/b/ax;-><init>()V

    aput-object v3, v2, v0

    .line 86
    iget-object v2, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->b:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/c/b/ax;->a:Ljava/lang/String;

    .line 87
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->c:Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 88
    iget-object v2, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/ax;->a(Ljava/lang/String;)Lcom/google/android/c/b/ax;

    .line 90
    :cond_1
    iget-object v2, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v3, v3, v0

    iget v3, v3, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->e:I

    iput v3, v2, Lcom/google/android/c/b/ax;->b:I

    .line 91
    iget-object v2, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->d:[B

    if-eqz v2, :cond_2

    .line 92
    iget-object v2, v1, Lcom/google/android/c/b/bh;->b:[Lcom/google/android/c/b/ax;

    aget-object v2, v2, v0

    iget-object v3, p1, Lcom/google/android/gms/car/CarMediaBrowserSourceNode;->e:[Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/car/CarMediaBrowserSourceNode$CarMediaList;->d:[B

    invoke-virtual {v2, v3}, Lcom/google/android/c/b/ax;->a([B)Lcom/google/android/c/b/ax;

    .line 84
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 95
    :cond_3
    const v0, 0x8002

    invoke-static {v1}, Lcom/google/android/c/b/bh;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ak;->a(I[B)V

    .line 96
    return-void
.end method
