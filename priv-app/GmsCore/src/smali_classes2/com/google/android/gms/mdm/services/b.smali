.class final Lcom/google/android/gms/mdm/services/b;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/mdm/services/DeviceManagerApiService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/mdm/services/DeviceManagerApiService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/mdm/services/b;->a:Lcom/google/android/gms/mdm/services/DeviceManagerApiService;

    .line 42
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 43
    return-void
.end method


# virtual methods
.method public final i(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/b;->a:Lcom/google/android/gms/mdm/services/DeviceManagerApiService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 50
    const-string v0, "Verified Package Name."

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/b;->a:Lcom/google/android/gms/mdm/services/DeviceManagerApiService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/DeviceManagerApiService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 54
    const-string v0, "Verified Package is Google Signed."

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 57
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/mdm/services/a;

    invoke-direct {v1}, Lcom/google/android/gms/mdm/services/a;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/mdm/services/a;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    return-void

    .line 61
    :catch_0
    move-exception v0

    const-string v0, "Client died while brokering service."

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->c(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
