.class public final Lcom/google/android/gms/search/queries/o;
.super Lcom/google/android/gms/search/queries/a/e;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/u;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/search/queries/a/e;-><init>()V

    .line 21
    iput-object p1, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/search/queries/o;->b:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;Lcom/google/android/gms/search/queries/a/a;)V
    .locals 7

    .prologue
    .line 53
    iget-object v6, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/queries/r;

    iget-object v2, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/queries/o;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/queries/r;-><init>(Lcom/google/android/gms/search/queries/o;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;Lcom/google/android/gms/search/queries/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 60
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;Lcom/google/android/gms/search/queries/a/a;)V
    .locals 7

    .prologue
    .line 65
    iget-object v6, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/queries/s;

    iget-object v2, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/queries/o;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/queries/s;-><init>(Lcom/google/android/gms/search/queries/o;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;Lcom/google/android/gms/search/queries/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 73
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;Lcom/google/android/gms/search/queries/a/a;)V
    .locals 7

    .prologue
    .line 41
    iget-object v6, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/queries/q;

    iget-object v2, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/queries/o;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/queries/q;-><init>(Lcom/google/android/gms/search/queries/o;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;Lcom/google/android/gms/search/queries/a/a;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 48
    return-void
.end method

.method public final a(Lcom/google/android/gms/search/queries/QueryCall$Request;Lcom/google/android/gms/search/queries/a/a;)V
    .locals 8

    .prologue
    .line 27
    iget-object v0, p1, Lcom/google/android/gms/search/queries/QueryCall$Request;->f:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static {v0}, Lcom/google/android/gms/search/queries/m;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Lcom/google/android/gms/icing/impl/e/g;

    move-result-object v5

    .line 28
    iget-object v7, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v0, Lcom/google/android/gms/search/queries/p;

    iget-object v2, p0, Lcom/google/android/gms/search/queries/o;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v3, p0, Lcom/google/android/gms/search/queries/o;->b:Ljava/lang/String;

    move-object v1, p0

    move-object v4, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/search/queries/p;-><init>(Lcom/google/android/gms/search/queries/o;Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/QueryCall$Request;Lcom/google/android/gms/icing/impl/e/g;Lcom/google/android/gms/search/queries/a/a;)V

    invoke-virtual {v7, v0}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 36
    return-void
.end method
