.class final Lcom/google/android/gms/car/jm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/jk;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/jk;)V
    .locals 0

    .prologue
    .line 572
    iput-object p1, p0, Lcom/google/android/gms/car/jm;->a:Lcom/google/android/gms/car/jk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onFailure(I)V
    .locals 3

    .prologue
    .line 581
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    const-string v0, "CAR.FIRST"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Peer discovery failed: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    :cond_0
    return-void
.end method

.method public final onSuccess()V
    .locals 2

    .prologue
    .line 575
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    const-string v0, "CAR.FIRST"

    const-string v1, "Started peer discovery successful"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 578
    :cond_0
    return-void
.end method
