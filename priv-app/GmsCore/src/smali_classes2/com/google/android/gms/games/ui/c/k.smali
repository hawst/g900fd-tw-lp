.class public final Lcom/google/android/gms/games/ui/c/k;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"


# instance fields
.field private final p:Lcom/google/android/gms/games/Player;

.field private q:J

.field private r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;)V
    .locals 7

    .prologue
    .line 51
    const-wide/16 v4, 0x7d0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/ui/c/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;JZ)V

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/games/ui/c/k;->p:Lcom/google/android/gms/games/Player;

    .line 53
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/k;->j()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/k;->t:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v0, v0

    iput-wide v0, p0, Lcom/google/android/gms/games/ui/c/k;->q:J

    .line 55
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;)V
    .locals 4

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/games/ui/c/k;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/ui/c/k;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;)V

    .line 46
    sget-object v1, Lcom/google/android/gms/games/ui/c/k;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v2, Lcom/google/android/gms/games/ui/c/k;->b:Lcom/google/android/gms/games/ui/c/e;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/c/e;->sendMessage(Landroid/os/Message;)Z

    .line 47
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 61
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/k;->j()Landroid/content/Context;

    move-result-object v2

    sget v3, Lcom/google/android/gms/p;->lG:I

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->b()Ljava/lang/String;

    move-result-object v2

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a()Z

    move-result v3

    if-nez v3, :cond_0

    if-eqz v2, :cond_0

    const-string v3, " ("

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/k;->l:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->py:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 64
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/k;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 66
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/k;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v3

    .line 67
    if-eqz v3, :cond_2

    invoke-virtual {v3}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v2

    .line 72
    :goto_0
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v0, v4}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 73
    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/widget/TextView;Ljava/lang/Boolean;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/k;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/c/k;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v1

    sget v4, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/c/k;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v4

    invoke-virtual {v0, v1, v4, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;I)V

    .line 80
    if-eqz v3, :cond_1

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)V

    .line 83
    :cond_1
    return-void

    .line 67
    :cond_2
    const/4 v2, -0x1

    goto :goto_0
.end method

.method protected final d()V
    .locals 5

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/k;->j()Landroid/content/Context;

    move-result-object v0

    .line 110
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/k;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    .line 111
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/k;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, v2, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 112
    const-string v3, "com.google.android.gms.games.VIEW_PROFILE"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 114
    const-string v3, "com.google.android.gms.games.PLAYER"

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/k;->p:Lcom/google/android/gms/games/Player;

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 115
    const-string v3, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 118
    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/ClientUiProxyActivity;->a(Landroid/content/Context;Landroid/content/Intent;)V

    .line 119
    return-void
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 98
    const/16 v0, 0x1c

    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 103
    const/16 v0, 0x1d

    return v0
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    .line 123
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/c/b;->onAnimationEnd(Landroid/view/animation/Animation;)V

    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->g:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/k;->r:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/c/k;->q:J

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(JZ)V

    .line 130
    :cond_0
    return-void
.end method
