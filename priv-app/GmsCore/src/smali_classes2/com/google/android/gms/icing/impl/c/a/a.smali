.class public Lcom/google/android/gms/icing/impl/c/a/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:I

.field private final c:[C


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 15
    invoke-virtual {p1}, Ljava/lang/String;->toCharArray()[C

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->c:[C

    .line 16
    return-void
.end method

.method protected static a(C)Z
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x30

    if-lt p0, v0, :cond_0

    const/16 v0, 0x39

    if-gt p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 117
    iget v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;I)V

    .line 118
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 125
    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/b;

    new-instance v1, Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/c/a/a;->c:[C

    invoke-direct {v1, v2}, Ljava/lang/String;-><init>([C)V

    invoke-direct {v0, p2, v1, p1}, Lcom/google/android/gms/icing/impl/c/a/b;-><init>(ILjava/lang/String;Ljava/lang/String;)V

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 27
    iget v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->c:[C

    array-length v1, v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 31
    iget v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    .line 32
    return-void
.end method

.method public final c()Lcom/google/android/gms/icing/impl/c/a/c;
    .locals 7

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    new-instance v2, Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/c/a/a;->c:[C

    iget v4, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    iget v5, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    iget v6, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    sub-int/2addr v5, v6

    invoke-direct {v2, v3, v4, v5}, Ljava/lang/String;-><init>([CII)V

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/icing/impl/c/a/c;-><init>(ILjava/lang/Object;)V

    return-object v0
.end method

.method public final d()C
    .locals 2

    .prologue
    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v0

    .line 40
    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    .line 41
    return v0
.end method

.method public final e()C
    .locals 2

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/a/a;->c:[C

    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->a:I

    aget-char v0, v0, v1

    return v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 49
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v0

    invoke-static {v0}, Ljava/lang/Character;->isWhitespace(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    goto :goto_0

    .line 52
    :cond_0
    return-void
.end method

.method public final g()Lcom/google/android/gms/icing/impl/c/a/c;
    .locals 3

    .prologue
    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->f()V

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->b()V

    .line 76
    const/4 v0, 0x0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v1

    const/16 v2, 0x2d

    if-ne v1, v2, :cond_0

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    .line 81
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/c/a/a;->a(C)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    .line 83
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 86
    :cond_1
    if-nez v0, :cond_2

    .line 87
    const-string v0, "Expected an integer"

    iget v1, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;I)V

    .line 89
    :cond_2
    new-instance v1, Lcom/google/android/gms/icing/impl/c/a/c;

    iget v2, p0, Lcom/google/android/gms/icing/impl/c/a/a;->b:I

    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/a/a;->c()Lcom/google/android/gms/icing/impl/c/a/c;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/c/a/c;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/icing/impl/c/a/c;-><init>(ILjava/lang/Object;)V

    return-object v1
.end method
