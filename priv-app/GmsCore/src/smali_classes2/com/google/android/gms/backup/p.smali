.class public final Lcom/google/android/gms/backup/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/util/HashMap;

.field c:Z


# direct methods
.method constructor <init>(J)V
    .locals 5

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    .line 26
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/backup/p;->c:Z

    .line 79
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "_tmp_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-wide/32 v2, 0x10000

    rem-long v2, p1, v2

    const/16 v1, 0x10

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/p;->a:Ljava/lang/String;

    .line 80
    return-void
.end method

.method public static a(Ljava/io/InputStream;)Lcom/google/android/gms/backup/u;
    .locals 5

    .prologue
    .line 326
    invoke-static {p0}, Lcom/google/protobuf/a/b;->a(Ljava/io/InputStream;)Lcom/google/protobuf/a/b;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/backup/u;

    invoke-direct {v1}, Lcom/google/android/gms/backup/u;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/u;->b(Lcom/google/protobuf/a/b;)Lcom/google/android/gms/backup/u;

    move-result-object v1

    .line 328
    iget-boolean v0, v1, Lcom/google/android/gms/backup/u;->a:Z

    if-nez v0, :cond_0

    .line 329
    new-instance v0, Ljava/io/IOException;

    const-string v1, "Incomplete response"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 331
    :cond_0
    iget v0, v1, Lcom/google/android/gms/backup/u;->b:I

    .line 332
    if-eqz v0, :cond_1

    .line 333
    new-instance v1, Lcom/google/android/gms/backup/r;

    invoke-direct {v1, v0}, Lcom/google/android/gms/backup/r;-><init>(I)V

    throw v1

    .line 335
    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/v;

    .line 337
    iget v3, v0, Lcom/google/android/gms/backup/v;->f:I

    .line 338
    if-eqz v3, :cond_2

    const/4 v4, 0x3

    if-eq v3, v4, :cond_2

    .line 340
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "server error in app "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 341
    iget-object v2, v0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 342
    const-string v2, ": code "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 343
    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 344
    iget-object v0, v0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 345
    const-string v4, ": "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 346
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_0

    .line 348
    :cond_3
    const/4 v0, 0x6

    if-ne v3, v0, :cond_4

    .line 349
    new-instance v0, Lcom/google/android/gms/backup/t;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/t;-><init>(Ljava/lang/String;)V

    throw v0

    .line 350
    :cond_4
    const/4 v0, 0x1

    if-eq v3, v0, :cond_5

    const/4 v0, 0x7

    if-ne v3, v0, :cond_6

    .line 352
    :cond_5
    new-instance v0, Lcom/google/android/gms/backup/s;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/backup/s;-><init>(Ljava/lang/String;)V

    throw v0

    .line 354
    :cond_6
    new-instance v0, Ljava/io/IOException;

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 359
    :cond_7
    return-object v1
.end method


# virtual methods
.method public final a()I
    .locals 5

    .prologue
    .line 126
    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v0

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 128
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/q;

    iget-object v0, v0, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :cond_0
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 129
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 130
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    add-int/lit8 v1, v1, 0x5

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    array-length v0, v0

    add-int/2addr v0, v1

    add-int/2addr v2, v0

    goto :goto_1

    :cond_1
    move v1, v2

    .line 133
    goto :goto_0

    .line 134
    :cond_2
    return v1
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/backup/q;
    .locals 4

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/q;

    .line 91
    if-nez v0, :cond_0

    .line 92
    new-instance v0, Lcom/google/android/gms/backup/q;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/backup/q;-><init>(Lcom/google/android/gms/backup/p;B)V

    .line 93
    iget-object v1, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    if-nez v1, :cond_2

    .line 96
    iput-object p2, v0, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    .line 101
    :cond_1
    return-object v0

    .line 97
    :cond_2
    if-eqz p2, :cond_1

    iget-object v1, v0, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 98
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Different API keys for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": was \""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\", now \""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\""

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public final b()Lcom/google/android/gms/backup/i;
    .locals 10

    .prologue
    .line 152
    new-instance v5, Lcom/google/android/gms/backup/i;

    invoke-direct {v5}, Lcom/google/android/gms/backup/i;-><init>()V

    .line 153
    const/4 v0, 0x2

    invoke-virtual {v5, v0}, Lcom/google/android/gms/backup/i;->b(I)Lcom/google/android/gms/backup/i;

    .line 157
    iget-boolean v0, p0, Lcom/google/android/gms/backup/p;->c:Z

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Lcom/google/android/gms/backup/n;

    invoke-direct {v0}, Lcom/google/android/gms/backup/n;-><init>()V

    invoke-virtual {v5, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/n;)Lcom/google/android/gms/backup/i;

    .line 159
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/backup/p;->c:Z

    .line 162
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v6

    :cond_1
    invoke-interface {v6}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v6}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 163
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/backup/q;

    .line 164
    const/4 v4, 0x0

    .line 165
    iget-object v2, v1, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    .line 166
    :cond_2
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 167
    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Ljava/util/Map$Entry;

    .line 168
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 170
    if-nez v4, :cond_3

    .line 171
    new-instance v4, Lcom/google/android/gms/backup/j;

    invoke-direct {v4}, Lcom/google/android/gms/backup/j;-><init>()V

    .line 172
    invoke-virtual {v5, v4}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    .line 173
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 174
    iget-object v2, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 175
    iget-object v2, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/backup/j;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 179
    :cond_3
    iget-object v2, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v2}, Ljava/util/HashSet;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_4

    .line 180
    new-instance v2, Lcom/google/android/gms/backup/k;

    invoke-direct {v2}, Lcom/google/android/gms/backup/k;-><init>()V

    const-string v8, "_tmp_"

    invoke-virtual {v2, v8}, Lcom/google/android/gms/backup/k;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/k;

    move-result-object v2

    .line 182
    invoke-virtual {v4, v2}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/k;)Lcom/google/android/gms/backup/j;

    .line 185
    :cond_4
    new-instance v8, Lcom/google/android/gms/backup/x;

    invoke-direct {v8}, Lcom/google/android/gms/backup/x;-><init>()V

    .line 186
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, p0, Lcom/google/android/gms/backup/p;->a:Ljava/lang/String;

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/gms/backup/x;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/x;

    .line 187
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [B

    invoke-static {v2}, Lcom/google/protobuf/a/a;->a([B)Lcom/google/protobuf/a/a;

    move-result-object v2

    invoke-virtual {v8, v2}, Lcom/google/android/gms/backup/x;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/backup/x;

    .line 188
    invoke-virtual {v4, v8}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/x;)Lcom/google/android/gms/backup/j;

    .line 190
    iget-object v2, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 191
    invoke-interface {v7}, Ljava/util/Iterator;->remove()V

    goto/16 :goto_0

    .line 195
    :cond_5
    return-object v5
.end method

.method public final c()Lcom/google/android/gms/backup/i;
    .locals 6

    .prologue
    .line 203
    new-instance v2, Lcom/google/android/gms/backup/i;

    invoke-direct {v2}, Lcom/google/android/gms/backup/i;-><init>()V

    .line 204
    const/4 v0, 0x2

    invoke-virtual {v2, v0}, Lcom/google/android/gms/backup/i;->b(I)Lcom/google/android/gms/backup/i;

    .line 206
    iget-object v0, p0, Lcom/google/android/gms/backup/p;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 207
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/backup/q;

    .line 210
    new-instance v4, Lcom/google/android/gms/backup/j;

    invoke-direct {v4}, Lcom/google/android/gms/backup/j;-><init>()V

    .line 211
    invoke-virtual {v2, v4}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    .line 213
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 214
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 215
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->d:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/backup/j;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 217
    :cond_0
    new-instance v0, Lcom/google/android/gms/backup/k;

    invoke-direct {v0}, Lcom/google/android/gms/backup/k;-><init>()V

    const-string v5, "_tmp_"

    invoke-virtual {v0, v5}, Lcom/google/android/gms/backup/k;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/k;

    move-result-object v0

    .line 219
    invoke-virtual {v4, v0}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/k;)Lcom/google/android/gms/backup/j;

    .line 221
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 222
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    .line 223
    iget-object v0, v1, Lcom/google/android/gms/backup/q;->c:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->clear()V

    goto :goto_0

    .line 226
    :cond_1
    return-object v2
.end method
