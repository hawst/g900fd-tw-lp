.class public Lcom/google/android/gms/feedback/FeedbackOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public final a:I

.field public b:Ljava/lang/String;

.field public c:Landroid/os/Bundle;

.field public d:Ljava/lang/String;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public e:Ljava/lang/String;

.field public f:Landroid/app/ApplicationErrorReport;

.field public g:Ljava/lang/String;

.field public h:Lcom/google/android/gms/common/data/BitmapTeleporter;

.field public i:Ljava/lang/String;

.field public j:Ljava/util/ArrayList;

.field public k:Z

.field public l:Lcom/google/android/gms/feedback/ThemeSettings;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/gms/feedback/q;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/q;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/FeedbackOptions;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method private constructor <init>()V
    .locals 13
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 127
    const/4 v1, 0x2

    new-instance v6, Landroid/app/ApplicationErrorReport;

    invoke-direct {v6}, Landroid/app/ApplicationErrorReport;-><init>()V

    const/4 v11, 0x1

    move-object v0, p0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    move-object v7, v2

    move-object v8, v2

    move-object v9, v2

    move-object v10, v2

    move-object v12, v2

    invoke-direct/range {v0 .. v12}, Lcom/google/android/gms/feedback/FeedbackOptions;-><init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/feedback/ThemeSettings;)V

    .line 129
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/gms/feedback/FeedbackOptions;-><init>()V

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/os/Bundle;Ljava/lang/String;Ljava/lang/String;Landroid/app/ApplicationErrorReport;Ljava/lang/String;Lcom/google/android/gms/common/data/BitmapTeleporter;Ljava/lang/String;Ljava/util/ArrayList;ZLcom/google/android/gms/feedback/ThemeSettings;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 111
    iput p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->a:I

    .line 112
    iput-object p2, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    .line 113
    iput-object p3, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    .line 114
    iput-object p4, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->d:Ljava/lang/String;

    .line 115
    iput-object p5, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    .line 116
    iput-object p6, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    .line 117
    iput-object p7, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    .line 118
    iput-object p8, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    .line 119
    iput-object p9, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    .line 120
    iput-object p10, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    .line 121
    iput-boolean p11, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->k:Z

    .line 122
    iput-object p12, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->l:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 123
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/app/ApplicationErrorReport$CrashInfo;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iput-object p1, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/graphics/Bitmap;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 1

    .prologue
    .line 41
    if-eqz p1, :cond_0

    new-instance v0, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/data/BitmapTeleporter;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    :cond_0
    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/os/Bundle;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Lcom/google/android/gms/feedback/ThemeSettings;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->l:Lcom/google/android/gms/feedback/ThemeSettings;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Ljava/lang/String;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Ljava/util/ArrayList;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    return-object p0
.end method

.method static synthetic a(Lcom/google/android/gms/feedback/FeedbackOptions;Z)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-boolean p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->k:Z

    return-object p0
.end method

.method static synthetic b(Lcom/google/android/gms/feedback/FeedbackOptions;Ljava/lang/String;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic c(Lcom/google/android/gms/feedback/FeedbackOptions;Ljava/lang/String;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic d(Lcom/google/android/gms/feedback/FeedbackOptions;Ljava/lang/String;)Lcom/google/android/gms/feedback/FeedbackOptions;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 455
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->c:Landroid/os/Bundle;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/feedback/ThemeSettings;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->l:Lcom/google/android/gms/feedback/ThemeSettings;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->e:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Landroid/app/ApplicationErrorReport$CrashInfo;
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    .line 471
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->f:Landroid/app/ApplicationErrorReport;

    iget-object v0, v0, Landroid/app/ApplicationErrorReport;->crashInfo:Landroid/app/ApplicationErrorReport$CrashInfo;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 476
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->g:Ljava/lang/String;

    return-object v0
.end method

.method public final g()Lcom/google/android/gms/common/data/BitmapTeleporter;
    .locals 1

    .prologue
    .line 481
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->h:Lcom/google/android/gms/common/data/BitmapTeleporter;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final i()Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 491
    iget-object v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->j:Ljava/util/ArrayList;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 496
    iget-boolean v0, p0, Lcom/google/android/gms/feedback/FeedbackOptions;->k:Z

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 94
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/feedback/q;->a(Lcom/google/android/gms/feedback/FeedbackOptions;Landroid/os/Parcel;I)V

    .line 95
    return-void
.end method
