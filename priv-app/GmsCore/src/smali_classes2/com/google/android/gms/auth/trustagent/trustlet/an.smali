.class public final Lcom/google/android/gms/auth/trustagent/trustlet/an;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/auth/trustagent/trustlet/af;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/af;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    .line 29
    return-void
.end method

.method private a(Ljava/util/Set;Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 74
    invoke-virtual {p2}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->d()[Ljava/lang/String;

    move-result-object v2

    .line 75
    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 76
    invoke-interface {p1, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    :goto_1
    return v0

    .line 75
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 82
    :cond_1
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    const-class v1, Landroid/nfc/tech/NdefFormatable;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-class v1, Landroid/nfc/tech/Ndef;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    const-class v1, Landroid/nfc/tech/IsoDep;

    invoke-virtual {v1}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 83
    array-length v4, v2

    move v1, v0

    :goto_2
    if-ge v1, v4, :cond_3

    aget-object v5, v2, v1

    .line 84
    invoke-virtual {v3, v5}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 85
    invoke-interface {p1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 86
    const/4 v0, 0x1

    goto :goto_1

    .line 83
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 91
    :cond_3
    const-string v1, "Coffee - NfcTrustletController"

    const-string v2, "no available tech code for tag"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/ao;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ao;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/an;B)V

    throw v1
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b()Ljava/util/Set;

    move-result-object v1

    .line 51
    if-nez v1, :cond_0

    .line 52
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 55
    :cond_0
    :try_start_0
    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a(Ljava/util/Set;Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z

    move-result v2

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Ljava/util/Set;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/trustlet/ao; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    if-nez v1, :cond_1

    .line 62
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z

    move-result v0

    goto :goto_0

    .line 59
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 114
    :goto_0
    return v0

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a()Ljava/util/Set;

    move-result-object v0

    .line 102
    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    .line 104
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_1
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    .line 105
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 107
    :try_start_0
    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a(Ljava/util/Set;Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/trustagent/trustlet/ao; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 110
    :catch_0
    move-exception v0

    const-string v0, "Coffee - NfcTrustletController"

    const-string v4, "something very wrong with an UnlockTag, removing"

    invoke-static {v0, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 114
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->c(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Ljava/util/Set;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x1

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method
