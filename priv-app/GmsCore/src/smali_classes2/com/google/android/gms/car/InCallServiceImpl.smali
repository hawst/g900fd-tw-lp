.class public Lcom/google/android/gms/car/InCallServiceImpl;
.super Landroid/telecom/InCallService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/telecom/InCallService;-><init>()V

    .line 74
    return-void
.end method


# virtual methods
.method public getPhone()Landroid/telecom/Phone;
    .locals 3

    .prologue
    .line 68
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "CAR.TEL.InCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "getPhone "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-super {p0}, Landroid/telecom/InCallService;->getPhone()Landroid/telecom/Phone;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    invoke-super {p0}, Landroid/telecom/InCallService;->getPhone()Landroid/telecom/Phone;

    move-result-object v0

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 40
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 41
    const-string v0, "CAR.TEL.InCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onBind action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    :cond_0
    const-string v0, "local_bind"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 44
    new-instance v0, Lcom/google/android/gms/car/nj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/nj;-><init>(Lcom/google/android/gms/car/InCallServiceImpl;)V

    .line 46
    :goto_0
    return-object v0

    :cond_1
    invoke-super {p0, p1}, Landroid/telecom/InCallService;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 24
    invoke-super {p0}, Landroid/telecom/InCallService;->onCreate()V

    .line 25
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 26
    const-string v0, "CAR.TEL.InCallService"

    const-string v1, "onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 32
    invoke-super {p0}, Landroid/telecom/InCallService;->onDestroy()V

    .line 33
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 34
    const-string v0, "CAR.TEL.InCallService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 36
    :cond_0
    return-void
.end method

.method public onPhoneCreated(Landroid/telecom/Phone;)V
    .locals 3

    .prologue
    .line 52
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "CAR.TEL.InCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPhoneCreated "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 55
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/car/do;->a(Landroid/content/Context;)Lcom/google/android/gms/car/do;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/do;->a(Landroid/telecom/Phone;)V

    .line 56
    return-void
.end method

.method public onPhoneDestroyed(Landroid/telecom/Phone;)V
    .locals 3

    .prologue
    .line 60
    const-string v0, "CAR.TEL.InCallService"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    const-string v0, "CAR.TEL.InCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPhoneDestroyed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/car/do;->a(Landroid/content/Context;)Lcom/google/android/gms/car/do;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/do;->a()V

    .line 64
    return-void
.end method
