.class final Lcom/google/android/gms/checkin/ab;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/checkin/EventLogService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/checkin/EventLogService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 171
    iput-object p1, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    iput-object p2, p0, Lcom/google/android/gms/checkin/ab;->a:Landroid/content/Context;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 13

    .prologue
    const/4 v2, 0x0

    .line 174
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v8

    .line 175
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    const-string v1, "EventLogService-AsyncTask"

    invoke-virtual {v0, v1}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    invoke-virtual {v0}, Lcom/google/android/gms/checkin/EventLogService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    const-string v1, "EventLogService"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/checkin/EventLogService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    const-string v1, "dropbox"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/checkin/EventLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/DropBoxManager;

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/checkin/EventLogService;->b(Landroid/content/Context;)Z

    move-result v7

    .line 182
    invoke-static {}, Lcom/google/android/gms/checkin/EventLogService;->a()Lcom/google/android/gms/checkin/j;

    move-result-object v11

    monitor-enter v11

    .line 183
    :try_start_0
    const-string v0, "lastLog"

    invoke-static {v10, v0}, Lcom/google/android/gms/checkin/EventLogService;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)J

    move-result-wide v2

    .line 184
    const-string v0, "lastData"

    invoke-static {v10, v0}, Lcom/google/android/gms/checkin/EventLogService;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    .line 187
    :try_start_1
    const-string v0, "EventLogService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v12, "Aggregate from "

    invoke-direct {v1, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, " (log), "

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v12, " (data)"

    invoke-virtual {v1, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    invoke-static {}, Lcom/google/android/gms/checkin/EventLogService;->a()Lcom/google/android/gms/checkin/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/checkin/ab;->a:Landroid/content/Context;

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/checkin/j;->a(Landroid/content/Context;JJLandroid/os/DropBoxManager;Z)J

    move-result-wide v0

    .line 191
    invoke-interface {v10}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 192
    const-string v3, "lastLog"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "lastData"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 196
    :goto_0
    :try_start_2
    monitor-exit v11
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 198
    const-class v1, Lcom/google/android/gms/checkin/ac;

    monitor-enter v1

    .line 199
    const/4 v0, 0x1

    :try_start_3
    new-array v0, v0, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "dumpsys:"

    aput-object v3, v0, v2

    invoke-static {v9, v0}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;[Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    const-string v2, "dumpsys:"

    const-string v3, "checkin_dumpsys_whitelist"

    invoke-static {v9, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v7, v3}, Lcom/google/android/gms/checkin/EventLogService;->a(Ljava/util/Map;Ljava/lang/String;ZLjava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    const-string v3, "dump.tmp"

    invoke-virtual {v2, v3}, Lcom/google/android/gms/checkin/EventLogService;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    invoke-static {v0, v6, v2}, Lcom/google/android/gms/checkin/ac;->a(Ljava/util/Map;Landroid/os/DropBoxManager;Ljava/io/File;)V

    .line 204
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 206
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 207
    const/4 v0, 0x0

    return-object v0

    .line 193
    :catch_0
    move-exception v0

    .line 194
    :try_start_4
    const-string v1, "EventLogService"

    const-string v2, "Can\'t aggregate logs"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    .line 196
    :catchall_0
    move-exception v0

    monitor-exit v11

    throw v0

    .line 204
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/google/android/gms/checkin/ab;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method public final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/EventLogService;->a(Lcom/google/android/gms/checkin/EventLogService;)Landroid/os/AsyncTask;

    new-instance v0, Lcom/android/a/a;

    iget-object v1, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    const-string v2, "EventLogService"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/checkin/EventLogService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {v0}, Lcom/android/a/a;->a()V

    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    invoke-static {v0}, Lcom/google/android/gms/checkin/EventLogService;->c(Landroid/content/Context;)V

    iget-object v0, p0, Lcom/google/android/gms/checkin/ab;->b:Lcom/google/android/gms/checkin/EventLogService;

    invoke-virtual {v0}, Lcom/google/android/gms/checkin/EventLogService;->stopSelf()V

    return-void
.end method
