.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/ci;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/games/g/as;

.field public b:Landroid/content/Context;

.field public c:Ljava/lang/String;

.field public d:Ljava/lang/String;

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:Ljava/util/ArrayList;

.field private final h:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->f:Z

    .line 29
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->g:Ljava/util/ArrayList;

    .line 32
    new-instance v0, Lcom/google/android/gms/games/g/as;

    invoke-direct {v0}, Lcom/google/android/gms/games/g/as;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/g/as;->b:J

    .line 34
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->h:J

    .line 35
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 4

    .prologue
    .line 66
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->h:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b:Landroid/content/Context;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->d:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
