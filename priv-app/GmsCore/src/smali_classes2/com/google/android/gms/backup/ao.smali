.class public final Lcom/google/android/gms/backup/ao;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/backup/i;

.field private b:Lcom/google/android/gms/backup/u;

.field private c:I

.field private d:Ljava/util/HashMap;


# direct methods
.method constructor <init>(J[Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 66
    array-length v0, p3

    if-nez v0, :cond_1

    .line 67
    iput-object v3, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    .line 81
    :cond_0
    iput-object v3, p0, Lcom/google/android/gms/backup/ao;->b:Lcom/google/android/gms/backup/u;

    .line 82
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/ao;->c:I

    .line 83
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/ao;->d:Ljava/util/HashMap;

    .line 84
    return-void

    .line 69
    :cond_1
    new-instance v0, Lcom/google/android/gms/backup/i;

    invoke-direct {v0}, Lcom/google/android/gms/backup/i;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    .line 70
    iget-object v0, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/backup/i;->a(J)Lcom/google/android/gms/backup/i;

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    const v1, 0x7d000

    invoke-virtual {v0, v1}, Lcom/google/android/gms/backup/i;->a(I)Lcom/google/android/gms/backup/i;

    .line 72
    const/4 v0, 0x0

    :goto_0
    array-length v1, p3

    if-ge v0, v1, :cond_0

    .line 73
    new-instance v1, Lcom/google/android/gms/backup/j;

    invoke-direct {v1}, Lcom/google/android/gms/backup/j;-><init>()V

    .line 74
    aget-object v2, p3, v0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 75
    new-instance v2, Lcom/google/android/gms/backup/l;

    invoke-direct {v2}, Lcom/google/android/gms/backup/l;-><init>()V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/l;)Lcom/google/android/gms/backup/j;

    .line 76
    iget-object v2, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/util/Map;Ljava/util/Map;)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/backup/ao;->b:Lcom/google/android/gms/backup/u;

    if-nez v0, :cond_0

    move-object v0, v1

    .line 211
    :goto_0
    return-object v0

    .line 173
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/ao;->c:I

    iget-object v2, p0, Lcom/google/android/gms/backup/ao;->b:Lcom/google/android/gms/backup/u;

    iget-object v2, v2, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    if-ge v0, v2, :cond_6

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/backup/ao;->b:Lcom/google/android/gms/backup/u;

    iget v2, p0, Lcom/google/android/gms/backup/ao;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/backup/ao;->c:I

    iget-object v0, v0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/v;

    .line 176
    iget-object v2, v0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    .line 179
    iget-boolean v3, v0, Lcom/google/android/gms/backup/v;->d:Z

    if-eqz v3, :cond_1

    .line 180
    iget-object v3, v0, Lcom/google/android/gms/backup/v;->e:Ljava/lang/String;

    invoke-interface {p2, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 187
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/backup/v;->c()I

    move-result v4

    .line 190
    if-nez v4, :cond_2

    iget-boolean v3, v0, Lcom/google/android/gms/backup/v;->d:Z

    if-eqz v3, :cond_0

    .line 191
    :cond_2
    const/4 v3, 0x0

    :goto_1
    if-ge v3, v4, :cond_5

    .line 197
    invoke-virtual {v0, v3}, Lcom/google/android/gms/backup/v;->a(I)Lcom/google/android/gms/backup/x;

    move-result-object v5

    .line 198
    iget-object v6, v5, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    .line 199
    const-string v7, "_tmp_"

    invoke-virtual {v6, v7}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_3

    .line 200
    iget-boolean v7, v5, Lcom/google/android/gms/backup/x;->b:Z

    if-eqz v7, :cond_4

    .line 202
    iget-object v5, v5, Lcom/google/android/gms/backup/x;->c:Lcom/google/protobuf/a/a;

    invoke-virtual {v5}, Lcom/google/protobuf/a/a;->b()[B

    move-result-object v5

    invoke-interface {p1, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    :cond_3
    :goto_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 204
    :cond_4
    invoke-interface {p1, v6, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    :cond_5
    move-object v0, v2

    .line 208
    goto :goto_0

    :cond_6
    move-object v0, v1

    .line 211
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/backup/u;)V
    .locals 8

    .prologue
    .line 106
    iput-object p1, p0, Lcom/google/android/gms/backup/ao;->b:Lcom/google/android/gms/backup/u;

    .line 107
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/backup/ao;->c:I

    .line 113
    const/4 v2, 0x0

    .line 114
    iget-object v0, p1, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/v;

    .line 115
    iget-object v4, v0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    .line 118
    iget-boolean v1, v0, Lcom/google/android/gms/backup/v;->b:Z

    if-eqz v1, :cond_0

    .line 119
    iget-wide v6, v0, Lcom/google/android/gms/backup/v;->c:J

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/backup/ao;->d:Ljava/util/HashMap;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    .line 121
    if-nez v1, :cond_2

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/backup/ao;->d:Ljava/util/HashMap;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    :cond_0
    iget v1, v0, Lcom/google/android/gms/backup/v;->f:I

    .line 130
    const/4 v5, 0x3

    if-ne v1, v5, :cond_5

    .line 131
    if-nez v2, :cond_4

    .line 132
    new-instance v1, Lcom/google/android/gms/backup/i;

    invoke-direct {v1}, Lcom/google/android/gms/backup/i;-><init>()V

    .line 133
    iget-object v2, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    iget-wide v6, v2, Lcom/google/android/gms/backup/i;->b:J

    invoke-virtual {v1, v6, v7}, Lcom/google/android/gms/backup/i;->a(J)Lcom/google/android/gms/backup/i;

    .line 134
    iget-object v2, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    iget v2, v2, Lcom/google/android/gms/backup/i;->e:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/i;->a(I)Lcom/google/android/gms/backup/i;

    .line 137
    :goto_1
    new-instance v2, Lcom/google/android/gms/backup/j;

    invoke-direct {v2}, Lcom/google/android/gms/backup/j;-><init>()V

    .line 138
    invoke-virtual {v1, v2}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    .line 139
    invoke-virtual {v2, v4}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 141
    new-instance v4, Lcom/google/android/gms/backup/l;

    invoke-direct {v4}, Lcom/google/android/gms/backup/l;-><init>()V

    .line 143
    invoke-virtual {v2, v4}, Lcom/google/android/gms/backup/j;->a(Lcom/google/android/gms/backup/l;)Lcom/google/android/gms/backup/j;

    .line 145
    invoke-virtual {v0}, Lcom/google/android/gms/backup/v;->c()I

    move-result v2

    .line 146
    if-lez v2, :cond_1

    .line 147
    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/backup/v;->a(I)Lcom/google/android/gms/backup/x;

    move-result-object v0

    .line 148
    iget-object v0, v0, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/backup/l;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/l;

    :cond_1
    :goto_2
    move-object v2, v1

    .line 153
    goto :goto_0

    .line 123
    :cond_2
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v1, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 124
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Application "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": Fingerprint changed from "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 155
    :cond_3
    iput-object v2, p0, Lcom/google/android/gms/backup/ao;->a:Lcom/google/android/gms/backup/i;

    .line 156
    return-void

    :cond_4
    move-object v1, v2

    goto :goto_1

    :cond_5
    move-object v1, v2

    goto :goto_2
.end method
