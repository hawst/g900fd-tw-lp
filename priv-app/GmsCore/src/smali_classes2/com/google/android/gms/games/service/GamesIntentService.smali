.class public Lcom/google/android/gms/games/service/GamesIntentService;
.super Lcom/google/android/gms/common/app/a;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 179
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 195
    sput-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 197
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 198
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 199
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 200
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 201
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 202
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 203
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 207
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    new-instance v2, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v2}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 213
    invoke-static {}, Lcom/google/android/gms/games/service/GamesIntentService;->a()Ljava/util/HashMap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/app/a;-><init>(Ljava/util/HashMap;)V

    .line 214
    return-void
.end method

.method private static a()Ljava/util/HashMap;
    .locals 5

    .prologue
    .line 217
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 218
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 220
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/4 v4, -0x1

    if-ne v3, v4, :cond_0

    .line 221
    const/4 v3, 0x4

    invoke-static {v3}, Ljava/util/concurrent/Executors;->newFixedThreadPool(I)Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 223
    :cond_0
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    invoke-virtual {v1, v0, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 226
    :cond_1
    return-object v1
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 726
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/d;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/a/d;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 727
    return-void
.end method

.method private static a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V
    .locals 3

    .prologue
    .line 230
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 231
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 232
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No operation queue found for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 233
    invoke-virtual {v0, p2}, Ljava/util/concurrent/ConcurrentLinkedQueue;->offer(Ljava/lang/Object;)Z

    .line 234
    const-string v0, "com.google.android.gms.games.service.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "intent_thread_affinity"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 235
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 18

    .prologue
    .line 247
    const/16 v17, 0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/k;

    move-object/from16 v3, p1

    move/from16 v4, p2

    move-object/from16 v5, p3

    move/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    move-object/from16 v9, p7

    move/from16 v10, p8

    move/from16 v11, p9

    move/from16 v12, p10

    move/from16 v13, p11

    move/from16 v14, p12

    move-object/from16 v15, p13

    move-object/from16 v16, p14

    invoke-direct/range {v2 .. v16}, Lcom/google/android/gms/games/service/a/k;-><init>(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZILjava/lang/String;Ljava/util/ArrayList;)V

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 251
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 707
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/h/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/a/h/e;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 708
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 256
    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/i;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/a/i;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 257
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 696
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/e/e;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/service/a/e/e;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 698
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 2

    .prologue
    .line 382
    const/4 v0, 0x7

    new-instance v1, Lcom/google/android/gms/games/service/a/f;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/service/a/f;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 384
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 388
    const/4 v0, 0x7

    new-instance v1, Lcom/google/android/gms/games/service/a/j;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/a/j;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 390
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 604
    const/4 v0, 0x5

    new-instance v1, Lcom/google/android/gms/games/service/a/k/d;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/a/k/d;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/f;Ljava/lang/String;Ljava/util/ArrayList;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 606
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V
    .locals 7

    .prologue
    .line 597
    const/4 v6, 0x5

    new-instance v0, Lcom/google/android/gms/games/service/a/k/c;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/k/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/h;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 599
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 753
    const/16 v0, 0xc

    new-instance v1, Lcom/google/android/gms/games/service/a/c/a;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/a/c/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 755
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 294
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/i/g;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/a/i/g;-><init>(Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 295
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 711
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/h/a;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/a/h/a;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 713
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 702
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/h/b;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2, p3}, Lcom/google/android/gms/games/service/a/h/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 704
    return-void
.end method

.method public static a(Landroid/content/Context;Z)V
    .locals 2

    .prologue
    .line 260
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/e;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/a/e;-><init>(Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 261
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;)V
    .locals 3

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/b;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/service/a/i/b;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 323
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;I)V
    .locals 3

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/o;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/i/o;-><init>(Lcom/google/android/gms/games/a/au;I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 318
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V
    .locals 1

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 240
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/h;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/i/h;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 266
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/d;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/i/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 329
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IILjava/util/ArrayList;)V
    .locals 8

    .prologue
    .line 779
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v7, 0x8

    new-instance v0, Lcom/google/android/gms/games/service/a/l/d;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/l/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IILjava/util/ArrayList;)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 781
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V
    .locals 8

    .prologue
    .line 675
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/e/b;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/e/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 677
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[B[Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 760
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v8, 0x8

    new-instance v0, Lcom/google/android/gms/games/service/a/l/e;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/l/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[B[Ljava/lang/String;)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 763
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 9

    .prologue
    .line 532
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v8, 0x6

    new-instance v0, Lcom/google/android/gms/games/service/a/n/c;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/n/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[Ljava/lang/String;Landroid/os/Bundle;)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 535
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 3

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/f;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/i/f;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 278
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I[I)V
    .locals 3

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/h;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/n/h;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I[I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 613
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/a/s;)V
    .locals 3

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0xc

    new-instance v2, Lcom/google/android/gms/games/service/a/c/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/c/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/a/s;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 749
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/e/g;II)V
    .locals 8

    .prologue
    .line 436
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x2

    new-instance v0, Lcom/google/android/gms/games/service/a/g/c;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/g/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/e/g;II)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 438
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;)V
    .locals 3

    .prologue
    .line 838
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x9

    new-instance v2, Lcom/google/android/gms/games/service/a/m/c;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/m/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 840
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x9

    new-instance v2, Lcom/google/android/gms/games/service/a/m/b;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/m/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 834
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V
    .locals 9

    .prologue
    .line 811
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v8, 0x9

    new-instance v0, Lcom/google/android/gms/games/service/a/m/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/m/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 813
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V
    .locals 10

    .prologue
    .line 818
    iget-object v8, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v9, 0x9

    new-instance v0, Lcom/google/android/gms/games/service/a/m/f;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/service/a/m/f;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 822
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;ZI)V
    .locals 9

    .prologue
    .line 804
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v8, 0x9

    new-instance v0, Lcom/google/android/gms/games/service/a/m/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/m/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;ZI)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 806
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 414
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 415
    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/games/service/a/g/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/g/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 417
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;II)V
    .locals 8

    .prologue
    .line 408
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x2

    new-instance v0, Lcom/google/android/gms/games/service/a/g/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/g/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;II)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 410
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V
    .locals 10

    .prologue
    .line 421
    iget-object v8, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v9, 0x2

    new-instance v0, Lcom/google/android/gms/games/service/a/g/d;

    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/service/a/g/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIII)V

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 424
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V
    .locals 7

    .prologue
    .line 457
    const/4 v6, 0x3

    new-instance v0, Lcom/google/android/gms/games/service/a/a/a;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/a/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 459
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 282
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/i/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/i/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 285
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZZ)V
    .locals 10

    .prologue
    .line 682
    iget-object v8, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v9, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/e/f;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    move/from16 v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/service/a/e/f;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZZ)V

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 686
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JLjava/lang/String;)V
    .locals 11

    .prologue
    .line 400
    iget-object v9, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v10, 0x2

    new-instance v0, Lcom/google/android/gms/games/service/a/g/e;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object/from16 v8, p5

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/games/service/a/g/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;JJLjava/lang/String;)V

    invoke-static {v9, v10, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 403
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 7

    .prologue
    .line 442
    const/4 v6, 0x3

    new-instance v0, Lcom/google/android/gms/games/service/a/a/e;

    const/4 v3, 0x2

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/a/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;ILjava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 445
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 854
    const/16 v0, 0xa

    new-instance v1, Lcom/google/android/gms/games/service/a/j/b;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/j/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 857
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 9

    .prologue
    .line 553
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v8, 0x6

    new-instance v0, Lcom/google/android/gms/games/service/a/n/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/n/j;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 556
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZLjava/lang/String;)V
    .locals 8

    .prologue
    .line 569
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x6

    new-instance v0, Lcom/google/android/gms/games/service/a/n/g;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/n/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ZLjava/lang/String;)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 572
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 8

    .prologue
    .line 561
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x6

    new-instance v0, Lcom/google/android/gms/games/service/a/n/e;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/n/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 564
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 3

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/games/service/a/i/q;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/i/q;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 366
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[II)V
    .locals 2

    .prologue
    .line 861
    const/16 v0, 0xa

    new-instance v1, Lcom/google/android/gms/games/service/a/j/c;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/j/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[II)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 863
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/h;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/i/h;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 272
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/b;)V
    .locals 3

    .prologue
    .line 826
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x9

    new-instance v2, Lcom/google/android/gms/games/service/a/m/g;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/m/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/b;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 828
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 9

    .prologue
    .line 583
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v8, 0x5

    new-instance v0, Lcom/google/android/gms/games/service/a/k/a;

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/k/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    invoke-static {v7, v8, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 586
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 3

    .prologue
    .line 590
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x5

    new-instance v2, Lcom/google/android/gms/games/service/a/k/b;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/k/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 592
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/a;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/i/a;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 334
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 493
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 494
    new-instance v1, Lcom/google/android/gms/games/internal/d/c;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/d/c;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 495
    const/16 v1, 0xb

    new-instance v2, Lcom/google/android/gms/games/service/a/d/a;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/service/a/d/a;-><init>(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)V

    invoke-static {p0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 497
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)V
    .locals 12

    .prologue
    .line 796
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v10, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/e/h;

    move-object v2, p0

    move-object v3, p1

    move-wide v4, p2

    move-wide/from16 v6, p4

    move-object/from16 v8, p6

    move/from16 v9, p7

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/games/service/a/e/h;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)V

    invoke-static {v0, v10, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 799
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 2

    .prologue
    .line 873
    const/16 v0, 0xa

    new-instance v1, Lcom/google/android/gms/games/service/a/j/d;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/service/a/j/d;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 875
    return-void
.end method

.method public static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 730
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/h;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/a/h;-><init>()V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 731
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 791
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/e/k;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/a/e/k;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 792
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 376
    const/4 v0, 0x7

    new-instance v1, Lcom/google/android/gms/games/service/a/f;

    const/4 v2, 0x0

    invoke-direct {v1, p1, p2, v2}, Lcom/google/android/gms/games/service/a/f;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 378
    return-void
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 741
    const/4 v0, 0x7

    new-instance v1, Lcom/google/android/gms/games/service/a/b/b;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/service/a/b/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 743
    return-void
.end method

.method public static b(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 716
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/g;

    const/4 v2, 0x1

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/games/service/a/g;-><init>(Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 718
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;)V
    .locals 3

    .prologue
    .line 663
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/e/j;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/service/a/e/j;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 665
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/m;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/i/m;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 347
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V
    .locals 4

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/n;

    const/4 v3, 0x0

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/google/android/gms/games/service/a/i/n;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 341
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 3

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/c;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/i/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 301
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v3, 0x0

    .line 507
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x4

    new-instance v0, Lcom/google/android/gms/games/service/a/f/c;

    move-object v1, p0

    move-object v2, p1

    move v4, v3

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/f/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZLjava/lang/String;)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 510
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;III)V
    .locals 10

    .prologue
    .line 429
    iget-object v8, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v9, 0x2

    new-instance v0, Lcom/google/android/gms/games/service/a/g/d;

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/service/a/g/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIII)V

    invoke-static {v8, v9, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 432
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V
    .locals 7

    .prologue
    .line 464
    const/4 v6, 0x3

    new-instance v0, Lcom/google/android/gms/games/service/a/a/d;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/a/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 466
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 289
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/i/i;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/i/i;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 291
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 7

    .prologue
    .line 449
    const/4 v6, 0x3

    new-instance v0, Lcom/google/android/gms/games/service/a/a/e;

    const/4 v3, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/a/e;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;ILjava/lang/String;Lcom/google/android/gms/games/internal/el;)V

    invoke-static {p0, v6, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 452
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 3

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/e/d;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/e/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 625
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 487
    const/16 v0, 0xb

    new-instance v1, Lcom/google/android/gms/games/service/a/d/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/service/a/d/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 489
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 525
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/d;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/n/d;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 527
    return-void
.end method

.method public static b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 514
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/games/service/a/f/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/f/b;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 516
    return-void
.end method

.method public static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 735
    const/4 v0, 0x7

    new-instance v1, Lcom/google/android/gms/games/service/a/b/a;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/a/b/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 737
    return-void
.end method

.method public static c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 721
    const/4 v0, -0x1

    new-instance v1, Lcom/google/android/gms/games/service/a/g;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/games/service/a/g;-><init>(Ljava/lang/String;Z)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 723
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 358
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/games/service/a/i/k;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/i/k;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 360
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V
    .locals 4

    .prologue
    .line 351
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/n;

    const/4 v3, 0x1

    invoke-direct {v2, p0, p1, p2, v3}, Lcom/google/android/gms/games/service/a/i/n;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 354
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 3

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/i/l;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/i/l;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 313
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 539
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/i;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/n/i;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 541
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 305
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/i/j;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/i/j;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 307
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 3

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/games/service/a/h/f;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/h/f;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 655
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 767
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/gms/games/service/a/l/a;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/l/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 769
    return-void
.end method

.method public static c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x4

    new-instance v2, Lcom/google/android/gms/games/service/a/f/a;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/f/a;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 522
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 394
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/h/c;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/h/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 396
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V
    .locals 3

    .prologue
    .line 785
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/gms/games/service/a/l/c;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/l/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 788
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 8

    .prologue
    .line 501
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, 0x4

    new-instance v0, Lcom/google/android/gms/games/service/a/f/c;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/f/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZLjava/lang/String;)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 503
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 545
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/a;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/n/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 548
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 370
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/i/p;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/i/p;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 372
    return-void
.end method

.method public static d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/16 v1, 0x8

    new-instance v2, Lcom/google/android/gms/games/service/a/l/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/l/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 775
    return-void
.end method

.method public static e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 470
    const/4 v0, 0x3

    new-instance v1, Lcom/google/android/gms/games/service/a/a/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/service/a/a/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 472
    return-void
.end method

.method public static e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 3

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/e/g;

    invoke-direct {v2, p0, p1, p2, p3}, Lcom/google/android/gms/games/service/a/e/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 632
    return-void
.end method

.method public static e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/b;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/n/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 578
    return-void
.end method

.method public static e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 8

    .prologue
    .line 690
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/e/i;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/e/i;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 692
    return-void
.end method

.method public static e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 867
    const/16 v0, 0xa

    new-instance v1, Lcom/google/android/gms/games/service/a/j/c;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/service/a/j/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 869
    return-void
.end method

.method public static f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 476
    const/4 v0, 0x3

    new-instance v1, Lcom/google/android/gms/games/service/a/a/b;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/service/a/a/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 478
    return-void
.end method

.method public static f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 8

    .prologue
    .line 636
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/e/a;

    const/4 v3, 0x0

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/e/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 640
    return-void
.end method

.method public static f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x6

    new-instance v2, Lcom/google/android/gms/games/service/a/n/f;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/gms/games/service/a/n/f;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 619
    return-void
.end method

.method public static g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 2

    .prologue
    .line 481
    const/16 v0, 0xb

    new-instance v1, Lcom/google/android/gms/games/service/a/d/c;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/service/a/d/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 483
    return-void
.end method

.method public static g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IZ)V
    .locals 8

    .prologue
    .line 644
    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v7, -0x1

    new-instance v0, Lcom/google/android/gms/games/service/a/e/a;

    const/4 v3, 0x1

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/e/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V

    invoke-static {v6, v7, v0}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 649
    return-void
.end method

.method public static g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 848
    const/16 v0, 0xa

    new-instance v1, Lcom/google/android/gms/games/service/a/j/a;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/games/service/a/j/a;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Lcom/google/android/gms/games/a/au;ILcom/google/android/gms/games/service/e;)V

    .line 850
    return-void
.end method

.method public static h(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 658
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x7

    new-instance v2, Lcom/google/android/gms/games/service/a/h/d;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/h/d;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 660
    return-void
.end method

.method public static i(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 3

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, -0x1

    new-instance v2, Lcom/google/android/gms/games/service/a/e/c;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/games/service/a/e/c;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;ILcom/google/android/gms/games/service/e;)V

    .line 671
    return-void
.end method


# virtual methods
.method protected final a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 879
    const-string v0, "intent_thread_affinity"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 880
    sget-object v0, Lcom/google/android/gms/games/service/GamesIntentService;->b:Ljava/util/HashMap;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 881
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No op queue for affinity "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 882
    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/e;

    .line 883
    if-nez v0, :cond_0

    .line 884
    const-string v0, "GamesIntentService"

    const-string v1, "operation missing"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 901
    :goto_0
    return-void

    .line 888
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v2

    .line 890
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    .line 894
    invoke-interface {v0, p0, v2}, Lcom/google/android/gms/games/service/e;->a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V

    .line 895
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 899
    invoke-interface {v0}, Lcom/google/android/gms/games/service/e;->a()V

    .line 900
    invoke-virtual {v2}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_0

    .line 899
    :catchall_0
    move-exception v1

    invoke-interface {v0}, Lcom/google/android/gms/games/service/e;->a()V

    .line 900
    invoke-virtual {v2}, Lcom/google/android/gms/games/a/t;->a()V

    throw v1
.end method
