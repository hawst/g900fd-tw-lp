.class final Lcom/google/android/gms/car/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ag;)V
    .locals 0

    .prologue
    .line 375
    iput-object p1, p0, Lcom/google/android/gms/car/ah;->a:Lcom/google/android/gms/car/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 379
    iget-object v0, p0, Lcom/google/android/gms/car/ah;->a:Lcom/google/android/gms/car/ag;

    iget-object v0, v0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->d(Lcom/google/android/gms/car/ae;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 388
    :goto_0
    return-void

    .line 382
    :cond_0
    const-string v0, "CAR.IME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 383
    const-string v0, "CAR.IME"

    const-string v1, "Attempting IME reconnection now."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 386
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ah;->a:Lcom/google/android/gms/car/ag;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/ag;->a(Lcom/google/android/gms/car/ag;J)J

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/car/ah;->a:Lcom/google/android/gms/car/ag;

    iget-object v0, v0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->e(Lcom/google/android/gms/car/ae;)V

    goto :goto_0
.end method
