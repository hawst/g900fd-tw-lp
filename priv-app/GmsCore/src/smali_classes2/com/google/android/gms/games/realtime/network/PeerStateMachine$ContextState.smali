.class abstract Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final a:Ljava/lang/String;

.field public final b:Ljava/lang/String;

.field protected c:I

.field protected final d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

.field final synthetic e:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->e:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput-object p2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b:Ljava/lang/String;

    .line 81
    iput-object p3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a:Ljava/lang/String;

    .line 82
    iput p4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    .line 83
    iput-object p5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    .line 84
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a:Ljava/lang/String;

    return-object v0
.end method

.method public abstract a(Landroid/os/Message;)Z
.end method

.method public final b()Ljava/lang/String;
    .locals 3

    .prologue
    .line 93
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    .line 94
    const/16 v1, 0x24

    invoke-virtual {v0, v1}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v1

    .line 95
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "-"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
