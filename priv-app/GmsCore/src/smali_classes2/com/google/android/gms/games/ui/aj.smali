.class public final Lcom/google/android/gms/games/ui/aj;
.super Lcom/google/android/gms/games/ui/z;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/c;
.implements Lcom/google/android/gms/games/ui/d/k;


# instance fields
.field private A:Lcom/google/android/gms/games/ui/az;

.field private o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;

.field private r:Lcom/google/android/gms/games/ui/ba;

.field private s:Lcom/google/android/gms/games/ui/bt;

.field private t:Ljava/util/HashMap;

.field private u:Z

.field private v:Lcom/google/android/gms/games/ui/bd;

.field private w:Z

.field private x:Landroid/os/Bundle;

.field private y:Ljava/util/ArrayList;

.field private z:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/z;-><init>()V

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->t:Ljava/util/HashMap;

    .line 137
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->z:Z

    .line 1086
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/games/ui/aj;
    .locals 2

    .prologue
    .line 143
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 144
    const-string v1, "accountName"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const-string v1, "destAppVersion"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 147
    new-instance v1, Lcom/google/android/gms/games/ui/aj;

    invoke-direct {v1}, Lcom/google/android/gms/games/ui/aj;-><init>()V

    .line 148
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/aj;->setArguments(Landroid/os/Bundle;)V

    .line 149
    return-object v1
.end method

.method private a(Lcom/google/android/gms/common/api/v;I)V
    .locals 3

    .prologue
    .line 331
    invoke-static {p2}, Lcom/google/android/gms/games/internal/b/e;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 332
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 333
    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v2, v1, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 334
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/aj;->w:Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/games/p;->a(Lcom/google/android/gms/common/api/v;ZLandroid/os/Bundle;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/ap;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/ap;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bt;->notifyDataSetInvalidated()V

    .line 342
    return-void

    .line 333
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/aj;)Z
    .locals 1

    .prologue
    .line 73
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->u:Z

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/aj;Z)Z
    .locals 0

    .prologue
    .line 73
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/aj;->u:Z

    return p1
.end method

.method static b(I)I
    .locals 3

    .prologue
    .line 607
    packed-switch p0, :pswitch_data_0

    .line 615
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 609
    :pswitch_0
    const/4 v0, 0x1

    .line 613
    :goto_0
    return v0

    .line 611
    :pswitch_1
    const/16 v0, 0x9

    goto :goto_0

    .line 613
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 607
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/aj;)Lcom/google/android/gms/games/ui/GamesSettingsActivity;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/aj;)Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/aj;)Ljava/util/ArrayList;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->y:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/aj;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->q:Ljava/lang/String;

    return-object v0
.end method

.method public static k()V
    .locals 0

    .prologue
    .line 532
    return-void
.end method

.method private l()V
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 291
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->N()Ljava/util/ArrayList;

    move-result-object v1

    .line 297
    if-eqz v1, :cond_0

    .line 298
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299
    new-instance v2, Lcom/google/android/gms/common/people/a/f;

    invoke-direct {v2}, Lcom/google/android/gms/common/people/a/f;-><init>()V

    invoke-static {v2, v1}, Lcom/google/android/gms/common/people/data/c;->a(Lcom/google/android/gms/common/people/a/f;Ljava/util/List;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/people/a/f;->g()[B

    move-result-object v1

    .line 300
    sget-object v2, Lcom/google/android/gms/games/d;->t:Lcom/google/android/gms/games/internal/game/a;

    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/internal/game/a;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/an;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/an;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    .line 313
    :cond_0
    :goto_0
    return-void

    .line 310
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    goto :goto_0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 316
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 317
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 328
    :goto_0
    return-void

    .line 320
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/k;->c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/ao;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/ao;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method private n()V
    .locals 2

    .prologue
    .line 1129
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->k:Lcom/google/android/gms/games/ui/d/t;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 1130
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->k:Lcom/google/android/gms/games/ui/d/t;

    sget v1, Lcom/google/android/gms/p;->lw:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->b(I)V

    .line 1131
    return-void
.end method


# virtual methods
.method public final G_()V
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 590
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 592
    const-string v0, "GamesSettings"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    :goto_0
    return-void

    .line 596
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/k;->d(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/as;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/as;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final M()Z
    .locals 1

    .prologue
    .line 274
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->u:Z

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 515
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    .line 519
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 520
    sget-object v1, Lcom/google/android/gms/games/d;->t:Lcom/google/android/gms/games/internal/game/a;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/internal/game/a;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/ar;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/ar;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 527
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 226
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->l()V

    .line 228
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/y;->c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/ak;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/ak;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 236
    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/p;->e(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/al;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/al;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->m()V

    .line 244
    sget-object v0, Lcom/google/android/gms/games/d;->t:Lcom/google/android/gms/games/internal/game/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/game/a;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/am;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/am;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->A:Lcom/google/android/gms/games/ui/az;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->A:Lcom/google/android/gms/games/ui/az;

    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v1, p1}, Lcom/google/android/gms/games/p;->d(Lcom/google/android/gms/common/api/v;)Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/az;->a(Z)V

    .line 256
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/aa;)V
    .locals 2

    .prologue
    .line 461
    invoke-interface {p1}, Lcom/google/android/gms/games/aa;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 462
    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 463
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->n()V

    .line 471
    :goto_0
    return-void

    .line 469
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/aa;->b()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->u:Z

    .line 470
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bt;->notifyDataSetInvalidated()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V
    .locals 4

    .prologue
    .line 425
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 426
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 442
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->t:Ljava/util/HashMap;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 435
    sget-object v1, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/p;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/aq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/aq;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 485
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 486
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/b;->c()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 488
    const/4 v2, 0x2

    if-ne v0, v2, :cond_1

    .line 489
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->n()V

    .line 490
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/aj;->z:Z

    .line 512
    :cond_0
    :goto_0
    return-void

    .line 494
    :cond_1
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/aj;->z:Z

    .line 495
    if-nez v0, :cond_0

    .line 497
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 498
    const-string v2, "pacl"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 501
    :try_start_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/common/people/data/c;->a([B)Ljava/util/List;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/aj;->y:Ljava/util/ArrayList;

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bt;->notifyDataSetInvalidated()V
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 509
    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 506
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "GamesSettings"

    const-string v2, "Unable to parse ACL data."

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 509
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/l;)V
    .locals 2

    .prologue
    .line 474
    invoke-interface {p1}, Lcom/google/android/gms/games/l;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 475
    invoke-interface {p1}, Lcom/google/android/gms/games/l;->c()Lcom/google/android/gms/games/internal/game/e;

    move-result-object v1

    .line 478
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 479
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ba;->e()V

    .line 481
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/ba;->a(Lcom/google/android/gms/common/data/d;)V

    .line 482
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/q;)V
    .locals 6

    .prologue
    const/4 v2, 0x2

    .line 535
    invoke-interface {p1}, Lcom/google/android/gms/games/q;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 536
    invoke-interface {p1}, Lcom/google/android/gms/games/q;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 538
    if-ne v0, v2, :cond_1

    .line 539
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->n()V

    .line 565
    :cond_0
    :goto_0
    return-void

    .line 540
    :cond_1
    if-nez v0, :cond_0

    .line 542
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 543
    const-string v2, "mobile_notifications_enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/aj;->w:Z

    .line 545
    const-string v2, "match_notifications_enabled"

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v2

    .line 547
    const-string v3, "quest_notifications_enabled"

    const/4 v4, 0x0

    invoke-virtual {v1, v3, v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v3

    .line 549
    const-string v4, "request_notifications_enabled"

    const/4 v5, 0x0

    invoke-virtual {v1, v4, v5, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v0

    .line 551
    iget-object v4, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    const/4 v5, 0x0

    invoke-static {v5}, Lcom/google/android/gms/games/internal/b/e;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 554
    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    const/4 v4, 0x2

    invoke-static {v4}, Lcom/google/android/gms/games/internal/b/e;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 557
    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    const/4 v3, 0x1

    invoke-static {v3}, Lcom/google/android/gms/games/internal/b/e;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/bt;->notifyDataSetChanged()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 562
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/r;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 568
    invoke-interface {p1}, Lcom/google/android/gms/games/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    .line 569
    invoke-interface {p1}, Lcom/google/android/gms/games/r;->b()Ljava/lang/String;

    move-result-object v2

    .line 570
    invoke-interface {p1}, Lcom/google/android/gms/games/r;->c()Z

    move-result v3

    .line 571
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->t:Ljava/util/HashMap;

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 573
    if-eqz v3, :cond_0

    .line 574
    const-string v0, "GamesSettings"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Application was not unmuted as it should have been. (status: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", externalGameId: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 584
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->m()V

    .line 585
    return-void

    .line 576
    :cond_0
    if-eqz v0, :cond_1

    .line 577
    sget v1, Lcom/google/android/gms/p;->mh:I

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/aj;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 578
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1, v0, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 581
    :cond_1
    const-string v0, "GamesSettings"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Display name of unmuted game with externalGameId: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " was not found!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 284
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->v:Lcom/google/android/gms/games/ui/bd;

    if-eqz v0, :cond_0

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->v:Lcom/google/android/gms/games/ui/bd;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/bd;->a(Z)V

    .line 287
    :cond_0
    return-void
.end method

.method public final d_(I)V
    .locals 7

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v4

    .line 354
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-static {v4, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 356
    const-string v0, "GamesSettings"

    const-string v1, "onListItemClick: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 360
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/bt;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    .line 361
    instance-of v0, v1, Lcom/google/android/gms/games/ui/ay;

    if-eqz v0, :cond_5

    move-object v0, v1

    .line 362
    check-cast v0, Lcom/google/android/gms/games/ui/ay;

    iget v0, v0, Lcom/google/android/gms/games/ui/ay;->b:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    goto :goto_0

    .line 370
    :pswitch_1
    invoke-direct {p0, v4, v3}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/common/api/v;I)V

    goto :goto_0

    .line 364
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/games/ui/b/e;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/b/e;-><init>()V

    .line 366
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    const-string v2, "profileVisDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0

    .line 375
    :pswitch_3
    invoke-direct {p0, v4, v5}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/common/api/v;I)V

    goto :goto_0

    .line 380
    :pswitch_4
    invoke-direct {p0, v4, v2}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/common/api/v;I)V

    goto :goto_0

    .line 385
    :pswitch_5
    check-cast v1, Lcom/google/android/gms/games/ui/az;

    sget-object v0, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    invoke-interface {v0, v4}, Lcom/google/android/gms/games/p;->d(Lcom/google/android/gms/common/api/v;)Z

    move-result v5

    sget-object v6, Lcom/google/android/gms/games/d;->p:Lcom/google/android/gms/games/p;

    if-nez v5, :cond_2

    move v0, v2

    :goto_1
    invoke-interface {v6, v4, v0}, Lcom/google/android/gms/games/p;->a(Lcom/google/android/gms/common/api/v;Z)V

    if-nez v5, :cond_3

    :goto_2
    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/az;->a(Z)V

    goto :goto_0

    :cond_2
    move v0, v3

    goto :goto_1

    :cond_3
    move v2, v3

    goto :goto_2

    .line 389
    :pswitch_6
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->z:Z

    if-eqz v0, :cond_0

    .line 392
    invoke-static {}, Lcom/google/android/gms/common/audience/a/a;->a()Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->y:Ljava/util/ArrayList;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->y:Ljava/util/ArrayList;

    .line 399
    :goto_3
    iget-object v3, p0, Lcom/google/android/gms/games/ui/aj;->p:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/android/gms/common/audience/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v1

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/audience/a/b;->a(Ljava/util/List;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v0

    const-string v1, " "

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/audience/a/b;->c(Ljava/lang/String;)Lcom/google/android/gms/common/audience/a/b;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/b;->a()Landroid/content/Intent;

    move-result-object v0

    .line 403
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/games/ui/aj;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 397
    :cond_4
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    goto :goto_3

    .line 407
    :pswitch_7
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_GOOGLE_DEBUG_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 408
    invoke-virtual {p0, v0, v5}, Lcom/google/android/gms/games/ui/aj;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 412
    :pswitch_8
    sget-object v0, Lcom/google/android/gms/games/ui/n;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 413
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 414
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/aj;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 417
    :cond_5
    instance-of v0, v1, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    if-eqz v0, :cond_0

    .line 418
    check-cast v1, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 419
    invoke-static {v0}, Lcom/google/android/gms/games/ui/bg;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)Lcom/google/android/gms/games/ui/bg;

    move-result-object v0

    .line 420
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-string v2, "unmuteDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 362
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_3
        :pswitch_2
    .end packed-switch
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->p:Ljava/lang/String;

    return-object v0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 154
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/z;->onActivityCreated(Landroid/os/Bundle;)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "accountName"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->p:Ljava/lang/String;

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "destAppVersion"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->q:Ljava/lang/String;

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->O()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->w:Z

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->P()Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    .line 167
    new-instance v3, Lcom/google/android/gms/games/ui/av;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Lcom/google/android/gms/games/ui/bd;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/ui/bd;-><init>(Lcom/google/android/gms/games/ui/aj;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->v:Lcom/google/android/gms/games/ui/bd;

    new-instance v0, Lcom/google/android/gms/games/ui/bf;

    iget-object v5, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Landroid/content/Context;)I

    move-result v5

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/games/ui/bf;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->v:Lcom/google/android/gms/games/ui/bd;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/bb;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/ui/bb;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/bb;

    invoke-direct {v0, p0, v7}, Lcom/google/android/gms/games/ui/bb;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/bb;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/ui/bb;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/az;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/az;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->A:Lcom/google/android/gms/games/ui/az;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/bc;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/bc;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Lcom/google/android/gms/games/ui/ax;

    sget v5, Lcom/google/android/gms/p;->im:I

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/games/ui/ax;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-static {v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/games/ui/au;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/au;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_0
    new-instance v0, Lcom/google/android/gms/games/ui/aw;

    sget v5, Lcom/google/android/gms/p;->ir:I

    invoke-direct {v0, p0, v5}, Lcom/google/android/gms/games/ui/aw;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-direct {v3, p0, v4}, Lcom/google/android/gms/games/ui/av;-><init>(Lcom/google/android/gms/games/ui/aj;Ljava/util/ArrayList;)V

    .line 170
    new-instance v0, Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v4

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/games/ui/ba;-><init>(Lcom/google/android/gms/games/ui/aj;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/ba;->a(Lcom/google/android/gms/games/ui/c;)V

    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ba;->d()V

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    sget v4, Lcom/google/android/gms/l;->aM:I

    const/4 v5, 0x0

    invoke-static {v0, v4, v5}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 175
    sget v4, Lcom/google/android/gms/p;->is:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    .line 176
    iget-object v4, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/ui/ba;->a(Landroid/view/View;)V

    .line 180
    new-instance v0, Lcom/google/android/gms/games/ui/av;

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    iget-object v5, p0, Lcom/google/android/gms/games/ui/aj;->q:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Lcom/google/android/gms/games/ui/aw;

    sget v6, Lcom/google/android/gms/p;->ik:I

    invoke-direct {v5, p0, v6}, Lcom/google/android/gms/games/ui/aw;-><init>(Lcom/google/android/gms/games/ui/aj;I)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/gms/games/ui/at;

    invoke-direct {v5, p0}, Lcom/google/android/gms/games/ui/at;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Lcom/google/android/gms/games/ui/GamesSettingsDebugActivity;->a()Z

    move-result v5

    if-nez v5, :cond_1

    new-instance v5, Lcom/google/android/gms/games/ui/be;

    invoke-direct {v5, p0}, Lcom/google/android/gms/games/ui/be;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_1
    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/games/ui/av;-><init>(Lcom/google/android/gms/games/ui/aj;Ljava/util/ArrayList;)V

    .line 183
    new-instance v4, Lcom/google/android/gms/games/ui/bt;

    const/4 v5, 0x3

    new-array v5, v5, [Landroid/widget/BaseAdapter;

    aput-object v3, v5, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    aput-object v2, v5, v1

    aput-object v0, v5, v7

    invoke-direct {v4, v5}, Lcom/google/android/gms/games/ui/bt;-><init>([Landroid/widget/BaseAdapter;)V

    iput-object v4, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->s:Lcom/google/android/gms/games/ui/bt;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/aj;->a(Landroid/widget/ListAdapter;)V

    .line 187
    return-void

    .line 167
    :cond_2
    const-string v0, "6.7.77 (1747363-000)"

    const-string v5, "eng"

    invoke-virtual {v0, v5}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_0
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    .line 446
    packed-switch p1, :pswitch_data_0

    .line 457
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/z;->onActivityResult(IILandroid/content/Intent;)V

    .line 458
    :cond_0
    :goto_0
    return-void

    .line 448
    :pswitch_0
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 449
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/a;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/c;

    move-result-object v0

    .line 450
    invoke-interface {v0}, Lcom/google/android/gms/common/audience/a/c;->c()Ljava/util/ArrayList;

    move-result-object v0

    .line 451
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Ljava/util/ArrayList;)V

    .line 452
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/aj;->l()V

    goto :goto_0

    .line 446
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 191
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/z;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 195
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/aj;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/g;->aE:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 198
    invoke-virtual {v1, v2, v4, v2, v4}, Landroid/view/View;->setPadding(IIII)V

    .line 200
    const/high16 v2, 0x2000000

    invoke-virtual {v1, v2}, Landroid/view/View;->setScrollBarStyle(I)V

    .line 202
    iget-object v1, p0, Lcom/google/android/gms/games/ui/aj;->k:Lcom/google/android/gms/games/ui/d/t;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 204
    return-object v0
.end method

.method public final onDestroyView()V
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->r:Lcom/google/android/gms/games/ui/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/ba;->a()V

    .line 261
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onDestroyView()V

    .line 262
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 209
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onStart()V

    .line 210
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/aj;->z:Z

    .line 211
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 215
    iget-object v0, p0, Lcom/google/android/gms/games/ui/aj;->o:Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/aj;->w:Z

    iget-object v2, p0, Lcom/google/android/gms/games/ui/aj;->x:Landroid/os/Bundle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(ZLandroid/os/Bundle;)V

    .line 216
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onStop()V

    .line 217
    return-void
.end method
