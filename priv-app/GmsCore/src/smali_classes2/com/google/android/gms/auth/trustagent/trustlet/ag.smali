.class public final Lcom/google/android/gms/auth/trustagent/trustlet/ag;
.super Lcom/google/android/gms/auth/trustagent/trustlet/ap;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

.field b:Landroid/os/Handler;

.field c:J

.field private i:Landroid/nfc/NfcAdapter;

.field private j:Landroid/content/BroadcastReceiver;

.field private k:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final l:Landroid/nfc/NfcAdapter$NfcUnlockHandler;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;-><init>()V

    .line 73
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ah;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->l:Landroid/nfc/NfcAdapter$NfcUnlockHandler;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->l()V

    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a()Ljava/util/Set;

    move-result-object v0

    .line 192
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->b(Z)V

    .line 193
    return-void

    .line 192
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected final a()Z
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 197
    const-string v0, "Coffee - NFCTrustlet"

    const-string v1, "Starting authenticating user."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    iget-wide v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->c:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0xbb8

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 206
    const-string v0, "Coffee - NFCTrustlet"

    const-string v1, "startAuthenticating with recent tag read, authenticating."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->b:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/al;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/al;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 215
    :cond_0
    iput-wide v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->c:J

    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 113
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b()V

    .line 115
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->b:Landroid/os/Handler;

    .line 117
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    new-instance v2, Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const/4 v3, 0x0

    invoke-direct {v2, v1, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/af;-><init>(Landroid/content/SharedPreferences;Lcom/google/android/gms/auth/trustagent/am;)V

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/an;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/af;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    .line 119
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->i:Landroid/nfc/NfcAdapter;

    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aj;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->j:Landroid/content/BroadcastReceiver;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->j:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.nfc.action.ADAPTER_STATE_CHANGED"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ak;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->k:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->k:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->h()V

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->l()V

    .line 122
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 167
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->c()V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->k:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->i()V

    .line 172
    return-void
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 232
    const-string v0, "NFC"

    return-object v0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 227
    const/4 v0, 0x4

    return v0
.end method

.method protected final f()V
    .locals 2

    .prologue
    .line 221
    const-string v0, "Coffee - NFCTrustlet"

    const-string v1, "Stopping authenticating user."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 222
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->c:J

    .line 223
    return-void
.end method

.method protected final g()Z
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x1

    return v0
.end method

.method final h()V
    .locals 4

    .prologue
    .line 180
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b()Ljava/util/Set;

    move-result-object v0

    .line 181
    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->i:Landroid/nfc/NfcAdapter;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->l:Landroid/nfc/NfcAdapter$NfcUnlockHandler;

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/String;

    invoke-interface {v0, v3}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/nfc/NfcAdapter;->addNfcUnlockHandler(Landroid/nfc/NfcAdapter$NfcUnlockHandler;[Ljava/lang/String;)Z

    .line 184
    :cond_0
    return-void
.end method

.method final i()V
    .locals 2

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->i:Landroid/nfc/NfcAdapter;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->l:Landroid/nfc/NfcAdapter$NfcUnlockHandler;

    invoke-virtual {v0, v1}, Landroid/nfc/NfcAdapter;->removeNfcUnlockHandler(Landroid/nfc/NfcAdapter$NfcUnlockHandler;)Z

    .line 188
    return-void
.end method
