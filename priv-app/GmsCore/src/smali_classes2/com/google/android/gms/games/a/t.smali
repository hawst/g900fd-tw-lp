.class public final Lcom/google/android/gms/games/a/t;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static x:Lcom/google/android/gms/games/a/t;

.field private static final y:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final a:Lcom/google/android/gms/games/h/b;

.field private final b:Lcom/google/android/gms/games/h/b;

.field private final c:Lcom/google/android/gms/common/server/n;

.field private final d:Lcom/google/android/gms/common/server/n;

.field private final e:Lcom/google/android/gms/common/server/n;

.field private final f:Lcom/google/android/gms/games/a/a;

.field private final g:Lcom/google/android/gms/games/a/c;

.field private final h:Lcom/google/android/gms/games/a/q;

.field private final i:Lcom/google/android/gms/games/a/u;

.field private final j:Lcom/google/android/gms/games/a/ab;

.field private final k:Lcom/google/android/gms/games/a/ay;

.field private final l:Lcom/google/android/gms/games/a/bh;

.field private final m:Lcom/google/android/gms/games/a/bj;

.field private final n:Lcom/google/android/gms/games/a/bp;

.field private final o:Lcom/google/android/gms/games/a/ca;

.field private final p:Lcom/google/android/gms/games/a/k;

.field private final q:Lcom/google/android/gms/games/a/bv;

.field private final r:Lcom/google/android/gms/games/a/cb;

.field private final s:Lcom/google/android/gms/games/a/bc;

.field private final t:Lcom/google/android/gms/games/a/bt;

.field private final u:Lcom/google/android/gms/games/a/cf;

.field private final v:Ljava/util/ArrayList;

.field private final w:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 147
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/t;->y:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x0

    .line 185
    const-string v0, "DataBroker"

    sget-object v1, Lcom/google/android/gms/games/a/t;->y:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, v6}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 186
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    .line 187
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->v:Ljava/util/ArrayList;

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    .line 189
    new-instance v2, Lcom/google/android/gms/games/h/b;

    sget-object v0, Lcom/google/android/gms/games/c/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    sget-object v0, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v4, v3, v0}, Lcom/google/android/gms/games/h/b;-><init>(Landroid/content/Context;ZZZ)V

    iput-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    .line 190
    new-instance v2, Lcom/google/android/gms/games/h/b;

    const/4 v3, 0x1

    sget-object v0, Lcom/google/android/gms/games/c/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    sget-object v0, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-direct {v2, v1, v3, v5, v0}, Lcom/google/android/gms/games/h/b;-><init>(Landroid/content/Context;ZZZ)V

    iput-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    .line 191
    invoke-static {v1}, Lcom/google/android/gms/plus/e/d;->b(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->c:Lcom/google/android/gms/common/server/n;

    .line 192
    invoke-static {v1}, Lcom/google/android/gms/plus/e/d;->a(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->d:Lcom/google/android/gms/common/server/n;

    .line 193
    new-instance v0, Lcom/google/android/gms/common/server/n;

    sget-object v2, Lcom/google/android/gms/games/c/a;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/suggest"

    sget-object v5, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v5}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->e:Lcom/google/android/gms/common/server/n;

    .line 196
    new-instance v0, Lcom/google/android/gms/drive/d/a/g;

    sget-object v1, Lcom/google/android/gms/drive/ai;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    const-string v3, "/drive/v2/"

    sget-object v1, Lcom/google/android/gms/games/c/a;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    sget-object v1, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    move-object v1, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/drive/d/a/g;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;)V

    .line 202
    new-instance v1, Lcom/google/android/gms/games/a/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/a;-><init>(Lcom/google/android/gms/games/a/bb;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/a;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    .line 203
    new-instance v1, Lcom/google/android/gms/games/a/c;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/games/a/c;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/c;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    .line 205
    new-instance v1, Lcom/google/android/gms/games/a/q;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/games/a/q;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/q;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    .line 206
    new-instance v1, Lcom/google/android/gms/games/a/u;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/games/a/u;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/u;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    .line 207
    new-instance v1, Lcom/google/android/gms/games/a/ab;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->e:Lcom/google/android/gms/common/server/n;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/gms/games/a/ab;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/ab;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    .line 209
    new-instance v1, Lcom/google/android/gms/games/a/ay;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2, v3}, Lcom/google/android/gms/games/a/ay;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/ay;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    .line 211
    new-instance v1, Lcom/google/android/gms/games/a/bh;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/bh;-><init>(Lcom/google/android/gms/games/a/bb;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/bh;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    .line 212
    new-instance v1, Lcom/google/android/gms/games/a/bj;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    iget-object v5, p0, Lcom/google/android/gms/games/a/t;->c:Lcom/google/android/gms/common/server/n;

    iget-object v6, p0, Lcom/google/android/gms/games/a/t;->d:Lcom/google/android/gms/common/server/n;

    move-object v2, p0

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/bj;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/bj;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    .line 214
    new-instance v1, Lcom/google/android/gms/games/a/bp;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-direct {v1, p0, v2, v3, v4}, Lcom/google/android/gms/games/a/bp;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/games/a/aa;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/bp;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    .line 216
    new-instance v1, Lcom/google/android/gms/games/a/ca;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/games/a/ca;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/ca;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    .line 217
    new-instance v1, Lcom/google/android/gms/games/a/k;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/games/a/k;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/k;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    .line 218
    new-instance v1, Lcom/google/android/gms/games/a/bv;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/games/a/bv;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/bv;

    iput-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    .line 219
    new-instance v1, Lcom/google/android/gms/games/a/cb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/gms/games/a/cb;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/cb;

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    .line 221
    new-instance v0, Lcom/google/android/gms/games/a/bc;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/a/bc;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bc;

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    .line 222
    new-instance v0, Lcom/google/android/gms/games/a/bt;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/a/bt;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bt;

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    .line 223
    new-instance v0, Lcom/google/android/gms/games/a/cf;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->a:Lcom/google/android/gms/games/h/b;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->b:Lcom/google/android/gms/games/h/b;

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/a/cf;-><init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/t;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/cf;

    iput-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    .line 224
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 538
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 539
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 540
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 542
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 545
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 547
    return-object v0

    .line 545
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;
    .locals 2

    .prologue
    .line 162
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 164
    sget-object v0, Lcom/google/android/gms/games/a/t;->y:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 166
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/a/t;->x:Lcom/google/android/gms/games/a/t;

    if-nez v0, :cond_0

    .line 167
    new-instance v0, Lcom/google/android/gms/games/a/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/t;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/games/a/t;->x:Lcom/google/android/gms/games/a/t;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/a/t;->y:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 172
    sget-object v0, Lcom/google/android/gms/games/a/t;->x:Lcom/google/android/gms/games/a/t;

    return-object v0

    .line 170
    :catchall_0
    move-exception v0

    sget-object v1, Lcom/google/android/gms/games/a/t;->y:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v0
.end method

.method private a(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 227
    instance-of v0, p1, Lcom/google/android/gms/games/a/ce;

    if-eqz v0, :cond_0

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->v:Ljava/util/ArrayList;

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/games/a/ce;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 230
    :cond_0
    instance-of v0, p1, Lcom/google/android/gms/games/a/ax;

    if-eqz v0, :cond_1

    .line 231
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    move-object v0, p1

    check-cast v0, Lcom/google/android/gms/games/a/ax;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 233
    :cond_1
    return-object p1
.end method

.method private a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)V
    .locals 10

    .prologue
    const/4 v8, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 4130
    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 4131
    new-array v0, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4133
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    .line 4135
    new-array v0, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v4, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 4139
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 4140
    const/16 v4, 0x5dd

    if-ne v0, v4, :cond_1

    move v0, v1

    .line 4141
    :goto_0
    new-instance v4, Lcom/google/android/gms/games/internal/e/b;

    invoke-direct {v4, v3}, Lcom/google/android/gms/games/internal/e/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 4143
    if-eqz v0, :cond_0

    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/e/b;->c()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v0

    if-eq v0, v1, :cond_2

    .line 4152
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/e/b;->w_()V

    .line 4169
    :goto_1
    return-void

    .line 4135
    :catchall_0
    move-exception v0

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    :cond_1
    move v0, v2

    .line 4140
    goto :goto_0

    .line 4147
    :cond_2
    :try_start_2
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/ui/d/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v3

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v5}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    iget-object v6, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const/4 v7, 0x0

    invoke-virtual {v4, v7}, Lcom/google/android/gms/games/internal/e/b;->b(I)Lcom/google/android/gms/games/internal/e/a;

    move-result-object v7

    invoke-static {v0, v3, v5, v6, v7}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/a;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 4152
    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/e/b;->w_()V

    .line 4157
    new-array v0, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v3, v0, v2

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v3, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4160
    :try_start_3
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v0

    const/4 v3, 0x1

    iput-boolean v3, v0, Lcom/google/android/gms/games/a/av;->g:Z

    const/4 v3, 0x0

    iput-object v3, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 4164
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    new-instance v4, Landroid/content/SyncResult;

    invoke-direct {v4}, Landroid/content/SyncResult;-><init>()V

    invoke-virtual {v3, v0, v4}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 4165
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 4166
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 4168
    new-array v0, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v3, v0, v2

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto :goto_1

    .line 4152
    :catchall_1
    move-exception v0

    invoke-virtual {v4}, Lcom/google/android/gms/games/internal/e/b;->w_()V

    throw v0

    .line 4168
    :catchall_2
    move-exception v0

    new-array v3, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v4, v3, v2

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v3, v1

    invoke-static {v3}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 3165
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 3166
    const/4 v0, 0x1

    .line 3170
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v1}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 3171
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    if-lez v1, :cond_0

    .line 3172
    const/4 v1, 0x0

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v1

    .line 3173
    const-string v3, "quest_notifications_enabled"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4, v1}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3178
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 3180
    :goto_0
    return v0

    .line 3175
    :catch_0
    move-exception v1

    .line 3176
    :try_start_1
    const-string v3, "DataBroker"

    const-string v4, "Failed to fetch contact settings"

    invoke-static {v3, v4, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3178
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method private c(Lcom/google/android/gms/games/a/au;Z)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 519
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 524
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 529
    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 531
    return v0

    .line 529
    :catchall_0
    move-exception v0

    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method private g(Landroid/content/Context;)Ljava/util/HashSet;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v2, 0x0

    .line 3429
    invoke-static {p1}, Lcom/google/android/gms/games/n;->b(Landroid/content/Context;)Ljava/util/ArrayList;

    move-result-object v3

    .line 3430
    new-instance v4, Ljava/util/HashSet;

    invoke-direct {v4}, Ljava/util/HashSet;-><init>()V

    .line 3431
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3433
    :try_start_0
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v1, v2

    :goto_0
    if-ge v1, v5, :cond_1

    .line 3434
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 3435
    iget-object v6, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 3436
    if-eqz v0, :cond_0

    .line 3437
    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3433
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 3441
    :cond_1
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v1, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3443
    return-object v4

    .line 3441
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 270
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->g()V

    .line 271
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->v:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->v:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ce;

    invoke-interface {v0}, Lcom/google/android/gms/games/a/ce;->a()V

    .line 271
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 274
    :cond_0
    return-void
.end method

.method private r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4107
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 4110
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4113
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1}, Lcom/google/android/gms/games/a/a;->a(Lcom/google/android/gms/games/a/au;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 4115
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 4118
    if-eqz v0, :cond_0

    .line 4119
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v1

    iput-object v0, v1, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    .line 4122
    invoke-virtual {v1}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object p1

    .line 4125
    :cond_0
    return-object p1

    .line 4115
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;Landroid/os/Bundle;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 974
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 980
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->a:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 982
    :try_start_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 983
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 987
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->a:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    .line 985
    :cond_0
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->a:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 990
    return v3
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 701
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 702
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 706
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 708
    return v0

    .line 706
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3679
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3680
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 3684
    if-nez v0, :cond_0

    .line 3687
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/ay;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3690
    :cond_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3692
    return v0

    .line 3690
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2431
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2432
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2435
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/ep;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2438
    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2440
    return v0

    .line 2438
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3635
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3636
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/k;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3640
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3642
    return v0

    .line 3640
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1246
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1247
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1252
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1254
    return v0

    .line 1252
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/os/Bundle;)I
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1309
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v5

    .line 1310
    iget-object v0, v5, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    move v2, v1

    .line 1339
    :goto_0
    return v2

    .line 1316
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v2

    move v4, v2

    :goto_1
    if-ge v3, v6, :cond_1

    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ax;

    .line 1318
    new-array v7, v1, [Lcom/google/android/gms/games/a/bb;

    invoke-interface {v0}, Lcom/google/android/gms/games/a/ax;->a()Lcom/google/android/gms/games/a/bb;

    move-result-object v8

    aput-object v8, v7, v2

    invoke-static {v7}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1321
    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/games/a/ax;->b()Ljava/lang/String;

    move-result-object v7

    .line 1322
    invoke-interface {v0, v5}, Lcom/google/android/gms/games/a/ax;->a(Lcom/google/android/gms/games/a/au;)I

    move-result v8

    .line 1323
    const/4 v9, 0x0

    invoke-virtual {p2, v7, v9}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v9

    .line 1324
    add-int/2addr v8, v9

    .line 1325
    invoke-virtual {p2, v7, v8}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1326
    invoke-interface {v0}, Lcom/google/android/gms/games/a/ax;->c()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v7

    or-int/2addr v4, v7

    .line 1328
    new-array v7, v1, [Lcom/google/android/gms/games/a/bb;

    invoke-interface {v0}, Lcom/google/android/gms/games/a/ax;->a()Lcom/google/android/gms/games/a/bb;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v7}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1316
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 1328
    :catchall_0
    move-exception v3

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    invoke-interface {v0}, Lcom/google/android/gms/games/a/ax;->a()Lcom/google/android/gms/games/a/bb;

    move-result-object v0

    aput-object v0, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v3

    .line 1334
    :cond_1
    invoke-virtual {p2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1335
    invoke-virtual {p2, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1336
    goto :goto_2

    .line 1337
    :cond_2
    const-string v0, "inbox_total_count"

    invoke-virtual {p2, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1338
    const-string v0, "inbox_has_new_activity"

    invoke-virtual {p2, v0, v4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3961
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3964
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3966
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3968
    return v0

    .line 3966
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3912
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3915
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3918
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3920
    return v0

    .line 3918
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3981
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3984
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3986
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3988
    return v0

    .line 3986
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1083
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1086
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1088
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1090
    return v0

    .line 1088
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2223
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2224
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2227
    if-nez p3, :cond_0

    .line 2228
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bt;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I

    .line 2233
    :goto_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2235
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2237
    return v2

    .line 2230
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2235
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1845
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1848
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)Lcom/google/android/gms/games/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1851
    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1855
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1856
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    invoke-direct {p0, p1, v2, v3, p4}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)V

    .line 1859
    :cond_0
    iget v0, v0, Lcom/google/android/gms/games/a/g;->a:I

    return v0

    .line 1851
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)I
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 338
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 339
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static/range {p1 .. p8}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 345
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 348
    return v3

    .line 345
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)I
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1795
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1797
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1800
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1804
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1805
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    invoke-direct {p0, p1, v2, v3, p3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)V

    .line 1808
    :cond_0
    iget v0, v0, Lcom/google/android/gms/games/a/g;->a:I

    return v0

    .line 1800
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)I
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2047
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2050
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)I

    move-result v0

    .line 2052
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-static {p1}, Lcom/google/android/gms/games/a/bp;->b(Lcom/google/android/gms/games/a/au;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2054
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2056
    return v0

    .line 2054
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Z)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1288
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1291
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1292
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1294
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1296
    return v0

    .line 1294
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/games/a/bb;)I
    .locals 1

    .prologue
    .line 107
    invoke-super {p0, p1}, Lcom/google/android/gms/games/a/bb;->a(Lcom/google/android/gms/games/a/bb;)I

    move-result v0

    return v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2603
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2604
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2606
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2607
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2609
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2611
    return-object v0

    .line 2609
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2380
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2382
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2384
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    return-object v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1225
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1226
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1228
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/common/server/ClientContext;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1230
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1232
    return-object v0

    .line 1230
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 386
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 387
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 389
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 391
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 393
    return-object v0

    .line 391
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 778
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 779
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 781
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 783
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 785
    return-object v0

    .line 783
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2516
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2517
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2519
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2521
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2523
    return-object v0

    .line 2521
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3811
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3812
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3814
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-static {p1, p2, p3, p4, p5}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3817
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3819
    return-object v0

    .line 3817
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1482
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/games/a/ab;->a(I)Lcom/google/android/gms/games/a/bb;

    move-result-object v1

    .line 1483
    new-array v0, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1484
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1486
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1487
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v2, v0, p2, p3, p4}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1490
    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1492
    return-object v0

    .line 1490
    :catchall_0
    move-exception v0

    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;II[B[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3728
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3729
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3731
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bv;->j()V

    .line 3732
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3734
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;ILjava/lang/Integer;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3737
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3739
    return-object v0

    .line 3737
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 888
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 889
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 891
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 892
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 893
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 898
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 900
    return-object v0

    .line 895
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 898
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->f:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;I[Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2076
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2077
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2079
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2080
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2081
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v1, p1, p2, v0, p4}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;ILjava/util/ArrayList;Landroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2084
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2086
    return-object v0

    .line 2084
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2150
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2151
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2153
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2154
    new-instance v3, Ljava/util/ArrayList;

    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 2155
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    move-object v1, p1

    move v2, p2

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/bt;->a(Lcom/google/android/gms/games/a/au;ILjava/util/ArrayList;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2158
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2160
    return-object v0

    .line 2158
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 3882
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3883
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3885
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 3886
    iget-object v0, v1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 3887
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3893
    :goto_0
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3895
    return-object v0

    .line 3889
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3893
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/e/g;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 10

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 628
    new-array v0, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v9

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 629
    invoke-static {v8}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 631
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {p2}, Lcom/google/android/gms/games/e/g;->c()Ljava/lang/String;

    move-result-object v2

    iget-object v1, p2, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v3, "time_span"

    invoke-virtual {v1, v3}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    iget-object v1, p2, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v4, "leaderboard_collection"

    invoke-virtual {v1, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    iget-object v1, p2, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v5, "page_type"

    invoke-virtual {v1, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    move-object v1, p1

    move v5, p3

    move v7, p4

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIIII)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 636
    new-array v1, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v9

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 638
    return-object v0

    .line 636
    :catchall_0
    move-exception v0

    new-array v1, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v9

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 413
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 414
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 417
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 424
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 426
    return-object v0

    .line 421
    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 424
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 569
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 570
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 572
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 575
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 577
    return-object v0

    .line 575
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 946
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bb;

    move-result-object v1

    .line 948
    new-array v0, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 949
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 951
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 952
    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 953
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 959
    :goto_0
    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 961
    return-object v0

    .line 955
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v2, v0, p2, p3, p4}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 959
    :catchall_0
    move-exception v0

    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZZZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 9

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 1678
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v8

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1679
    invoke-static {v7}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1681
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 1682
    iget-object v0, v1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 1683
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1689
    :goto_0
    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v8

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1691
    return-object v0

    .line 1685
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZZZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1689
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->k:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v8

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2198
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2199
    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2201
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2202
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/bt;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 2205
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2207
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2209
    return-object v0

    .line 2207
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3195
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3198
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 3199
    if-nez v0, :cond_0

    .line 3200
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3206
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3208
    return-object v0

    .line 3203
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3206
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 2313
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2314
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2316
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2317
    if-nez p5, :cond_0

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 2320
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2323
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2325
    return-object v0

    .line 2317
    :cond_0
    :try_start_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-static {p5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v5, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2323
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 443
    new-array v1, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 444
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 446
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    .line 449
    new-array v1, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 452
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v1

    if-lez v1, :cond_3

    move v1, v0

    move v2, v0

    :goto_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v5

    if-ge v0, v5, :cond_2

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v5

    const-string v6, "collection"

    invoke-virtual {v4, v6, v0, v5}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v6

    if-nez v6, :cond_1

    const-string v6, "player_rank"

    invoke-virtual {v4, v6, v0, v5}, Lcom/google/android/gms/common/data/DataHolder;->i(Ljava/lang/String;II)Z

    move-result v6

    if-nez v6, :cond_0

    move v2, v3

    :cond_0
    const-string v6, "player_raw_score"

    invoke-virtual {v4, v6, v0, v5}, Lcom/google/android/gms/common/data/DataHolder;->i(Ljava/lang/String;II)Z

    move-result v5

    if-nez v5, :cond_1

    move v1, v3

    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 449
    :catchall_0
    move-exception v1

    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1

    .line 452
    :cond_2
    if-eqz v1, :cond_3

    const-string v0, "DataBroker"

    const-string v1, "User has public score, checking gameplay ACL status..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/t;->c(Lcom/google/android/gms/games/a/au;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-direct {p0, p1, p2, v4}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 453
    :goto_1
    return-object v0

    :cond_3
    move-object v0, v4

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2406
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2407
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2409
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2410
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ZLjava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2413
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2415
    return-object v0

    .line 2413
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2347
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2348
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2350
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2351
    if-nez p4, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2354
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v1, p1, p2, p3, v0}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2357
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2359
    return-object v0

    .line 2351
    :cond_0
    :try_start_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p4}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2357
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3231
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3233
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 3235
    if-nez v0, :cond_0

    .line 3236
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3242
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3245
    return-object v0

    .line 3239
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3242
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 739
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 740
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 742
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 744
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 746
    return-object v0

    .line 744
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/games/service/a/m/e;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 3940
    new-array v0, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3943
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/games/service/a/m/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3946
    new-array v1, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3948
    return-object v0

    .line 3946
    :catchall_0
    move-exception v0

    new-array v1, v8, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;ZI)Lcom/google/android/gms/games/service/a/m/e;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 3858
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3861
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;ZI)Lcom/google/android/gms/games/service/a/m/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3864
    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3866
    return-object v0

    .line 3864
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/s;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3104
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3106
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/q;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/s;)Ljava/util/ArrayList;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3108
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3111
    return-object v0

    .line 3108
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a()V
    .locals 0

    .prologue
    .line 181
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->f()V

    .line 182
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3123
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3127
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3128
    return-void

    .line 3127
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->h:Lcom/google/android/gms/games/a/q;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2677
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2679
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 2680
    if-nez v0, :cond_0

    .line 2681
    const-string v0, "DataBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No account found for the given datastore! Bailing! (datastore name :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2687
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2688
    :goto_0
    return-void

    .line 2685
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    invoke-static {p1, v0, p2}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2687
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 2629
    new-array v0, v9, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v8

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2631
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/a;->b(Landroid/content/Context;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 2632
    iget-object v3, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    .line 2633
    iget-object v6, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    .line 2634
    if-eqz v3, :cond_0

    if-nez v6, :cond_1

    .line 2635
    :cond_0
    const-string v0, "DataBroker"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No account found for the given datastore! Bailing! (datastore name :"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2647
    new-array v0, v9, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v8

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2648
    :goto_0
    return-void

    .line 2640
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v8

    :goto_1
    if-ge v1, v2, :cond_2

    .line 2641
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->w:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ax;

    invoke-interface {v0, p5}, Lcom/google/android/gms/games/a/ax;->a(I)V

    .line 2640
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 2644
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    move-object v1, p1

    move-object v2, p2

    move-object v4, p3

    move-object v5, p4

    move v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2647
    new-array v0, v9, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v8

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    new-array v1, v9, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v8

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 4

    .prologue
    .line 1738
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 1740
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 1741
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/ab;->a()V

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v2, p1, v0, p2, p3}, Lcom/google/android/gms/games/a/ab;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v3, v3, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v3}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    if-eqz v2, :cond_0

    if-nez p3, :cond_0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1, v0, v2}, Lcom/google/android/gms/games/a/ab;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    :catchall_1
    move-exception v0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    throw v0

    .line 1742
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3006
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3008
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3010
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3011
    return-void

    .line 3010
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 363
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/ab;->h(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 368
    const/4 v1, 0x4

    invoke-virtual {p2, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 370
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 373
    if-eqz v0, :cond_0

    .line 374
    iget-object v0, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 376
    :cond_0
    return-void

    .line 370
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->n:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 287
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 288
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/ca;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 292
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 294
    return v0

    .line 292
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 2

    .prologue
    .line 3043
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3044
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 3048
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3050
    return v0

    .line 3048
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;J)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v0, 0x0

    .line 3314
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 3324
    :goto_0
    return v0

    .line 3317
    :cond_0
    new-array v1, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3320
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    .line 3322
    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    move v0, v1

    .line 3324
    goto :goto_0

    .line 3322
    :catchall_0
    move-exception v1

    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v3, v2, v0

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2252
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2253
    if-nez p3, :cond_0

    .line 2256
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bt;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I

    .line 2261
    :goto_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2263
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2265
    return v2

    .line 2258
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2263
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)I
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1878
    new-array v0, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v6

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1881
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move-object v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)Lcom/google/android/gms/games/a/g;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1884
    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1888
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-lez v1, :cond_0

    .line 1889
    iget-wide v2, v0, Lcom/google/android/gms/games/a/g;->b:J

    invoke-direct {p0, p1, v2, v3, p4}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;JLcom/google/android/gms/games/internal/el;)V

    .line 1892
    :cond_0
    iget v0, v0, Lcom/google/android/gms/games/a/g;->a:I

    return v0

    .line 1884
    :catchall_0
    move-exception v0

    new-array v1, v7, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v6

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1822
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1824
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;

    move-result-object v0

    iget v0, v0, Lcom/google/android/gms/games/a/g;->a:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1827
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    return v0

    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Z)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1396
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1397
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Z)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1401
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1403
    return v0

    .line 1401
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2582
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2583
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2585
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->t:Lcom/google/android/gms/games/a/bt;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/bt;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2587
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2589
    return-object v0

    .line 2587
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 719
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 720
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 722
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 724
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 726
    return-object v0

    .line 724
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 798
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 799
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 801
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->d(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 803
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 805
    return-object v0

    .line 803
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2560
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2561
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2563
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2565
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2567
    return-object v0

    .line 2565
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 916
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 917
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 919
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 920
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 921
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 926
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 928
    return-object v0

    .line 923
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 926
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 4002
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4003
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 4005
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 4007
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 4009
    return-object v0

    .line 4007
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2100
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2101
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2103
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2104
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2106
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2108
    return-object v0

    .line 2106
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 599
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v7

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 600
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 602
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 605
    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 607
    return-object v0

    .line 605
    :catchall_0
    move-exception v0

    new-array v1, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v7

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1160
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1161
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1163
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1164
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1165
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1171
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1173
    return-object v0

    .line 1167
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2, p3, p4}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1171
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3268
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3270
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 3271
    if-nez v0, :cond_0

    .line 3272
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/bp;->b(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3278
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3281
    return-object v0

    .line 3275
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3278
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2028
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2029
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2031
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2033
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2035
    return-object v0

    .line 2033
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/google/android/gms/games/e/q;
    .locals 11

    .prologue
    .line 659
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/games/a/bb;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 660
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-object/from16 v8, p7

    move/from16 v9, p8

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)Lcom/google/android/gms/games/e/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 665
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 667
    return-object v0

    .line 665
    :catchall_0
    move-exception v0

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 2700
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 261
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    .line 263
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/games/a/t;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 265
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    .line 266
    return-void

    .line 265
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    throw v0
.end method

.method public final b(Landroid/content/Context;)V
    .locals 5

    .prologue
    .line 243
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 245
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    .line 247
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/games/a/t;->j()V

    .line 248
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 249
    invoke-static {v0}, Lcom/google/android/gms/games/provider/l;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 250
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v3, 0x0

    const/4 v4, 0x0

    invoke-virtual {v2, v0, v3, v4}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 253
    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    throw v0

    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    .line 254
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 682
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 684
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/ay;->b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 685
    const/4 v0, 0x5

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 687
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 688
    return-void

    .line 687
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    .locals 10

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1557
    new-array v0, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1562
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v4

    .line 1564
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v6, v4

    .line 1565
    const-wide/16 v8, 0x0

    cmp-long v0, v4, v8

    if-lez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/games/c/a;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v4

    cmp-long v0, v6, v4

    if-gtz v0, :cond_0

    move v0, v1

    .line 1568
    :goto_0
    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1570
    return v0

    :cond_0
    move v0, v2

    .line 1565
    goto :goto_0

    .line 1568
    :catchall_0
    move-exception v0

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;I)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1059
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1060
    const/4 v0, 0x2

    .line 1062
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v1

    .line 1063
    iget-object v2, v1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-eqz v2, :cond_0

    .line 1064
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1067
    :cond_0
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1069
    return v0

    .line 1067
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3535
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3536
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3538
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3540
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3542
    return-object v0

    .line 3540
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->j:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 758
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 759
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 761
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bj;->f(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 763
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 765
    return-object v0

    .line 763
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3833
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3834
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3836
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3838
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3840
    return-object v0

    .line 3838
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1009
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1010
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1012
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1013
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1014
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1019
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1021
    return-object v0

    .line 1016
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1019
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->e:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2122
    new-array v0, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2123
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2125
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bc;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 2126
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v2, p2, v0}, Lcom/google/android/gms/games/a/cf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2129
    new-array v1, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2131
    return-object v0

    .line 2129
    :catchall_0
    move-exception v0

    new-array v1, v3, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1712
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1713
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1715
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1716
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1717
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1722
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1724
    return-object v0

    .line 1719
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v1, v0, p2, p3, p4}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1722
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->i:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3753
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3754
    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3756
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bv;->j()V

    .line 3757
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3758
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 3759
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3761
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3763
    return-object v0

    .line 3761
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c()V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 316
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bv;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 321
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 322
    return-void

    .line 321
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 304
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 306
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    invoke-static {p1}, Lcom/google/android/gms/games/a/ca;->a(Landroid/content/Context;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 308
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 309
    return-void

    .line 308
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->o:Lcom/google/android/gms/games/a/ca;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2659
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2661
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2663
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2664
    return-void

    .line 2663
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final c(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 3024
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 3025
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 3026
    const/4 v2, 0x0

    invoke-virtual {p0, p1, v0, p2, v2}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    goto :goto_0

    .line 3028
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 1606
    new-array v0, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v5

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1607
    const/4 v1, 0x0

    .line 1608
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    const/16 v2, 0x19

    const/4 v3, 0x7

    const/4 v4, 0x0

    invoke-virtual {v0, p1, v2, v3, v4}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;IIZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 1612
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 1613
    const/16 v2, 0xa

    invoke-virtual {p2, v2}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1615
    new-array v2, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v3, v3, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v3, v2, v5

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1616
    if-eqz v1, :cond_0

    .line 1617
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1621
    :cond_0
    if-eqz v0, :cond_1

    .line 1622
    iget-object v0, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1624
    :cond_1
    return-void

    .line 1615
    :catchall_0
    move-exception v0

    new-array v2, v6, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v3, v3, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v3, v2, v5

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1616
    if-eqz v1, :cond_2

    .line 1617
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_2
    throw v0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3296
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3298
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3300
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3301
    return-void

    .line 3300
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3613
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3614
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3616
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3618
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3620
    return-object v0

    .line 3618
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3657
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3658
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3660
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3662
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3664
    return-object v0

    .line 3662
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 819
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 820
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 823
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 824
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 826
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 828
    return-object v0

    .line 826
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1127
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1128
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1130
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1131
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1132
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1137
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1139
    return-object v0

    .line 1134
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1137
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1036
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1037
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1039
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1040
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1041
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1046
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1048
    return-object v0

    .line 1043
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1046
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2173
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2174
    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2176
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2177
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 2179
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2181
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2183
    return-object v0

    .line 2181
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 3584
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/bb;

    move-result-object v1

    .line 3586
    new-array v0, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3587
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3589
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3590
    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 3591
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3597
    :goto_0
    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3599
    return-object v0

    .line 3593
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v2, v0, p2, p3, p4}, Lcom/google/android/gms/games/a/bj;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3597
    :catchall_0
    move-exception v0

    new-array v2, v3, [Lcom/google/android/gms/games/a/bb;

    aput-object v1, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final d(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3777
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3778
    invoke-static {v4}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3780
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bv;->j()V

    .line 3781
    new-instance v0, Ljava/util/ArrayList;

    invoke-static {p2}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 3782
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v1, p1, v0}, Lcom/google/android/gms/games/a/bv;->b(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 3784
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3786
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3788
    return-object v0

    .line 3786
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->d()V

    return-void
.end method

.method public final d(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1183
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1185
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/ay;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 1187
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1191
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v1

    .line 1192
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v0, v0, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1194
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bj;->b()V

    .line 1195
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 1196
    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/a/t;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1197
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/a/bj;->d(Lcom/google/android/gms/games/a/au;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1201
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    .line 1187
    :catchall_1
    move-exception v0

    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    .line 1199
    :cond_0
    :try_start_2
    sget-object v0, Lcom/google/android/gms/games/internal/c/a;->b:Landroid/net/Uri;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v0, v0, Lcom/google/android/gms/games/a/bj;->k:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1205
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1207
    :try_start_3
    invoke-virtual {v1}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 1208
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/bc;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    goto :goto_1

    .line 1211
    :catchall_2
    move-exception v0

    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    :cond_1
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1212
    return-void
.end method

.method public final d(Landroid/content/Context;Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 3065
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 3066
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 3067
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    const/4 v2, 0x1

    :try_start_0
    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v0}, Lcom/google/android/gms/games/provider/t;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v3

    const-string v4, "package_name=?"

    invoke-virtual {v3, v0, v4, v2}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    throw v0

    .line 3068
    :cond_0
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1636
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1637
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1638
    new-array v0, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v5, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v5, v0, v2

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1642
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/games/a/bj;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    and-int/lit8 v5, v0, 0x1

    .line 1643
    const/16 v0, 0xb

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V

    .line 1644
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/games/a/bj;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    and-int/2addr v0, v5

    .line 1645
    const/16 v3, 0xc

    invoke-virtual {p2, v3}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1647
    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1650
    if-nez v0, :cond_0

    .line 1651
    iget-object v0, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v0, v0, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1653
    :cond_0
    return-void

    :cond_1
    move v0, v2

    .line 1642
    goto :goto_0

    :cond_2
    move v0, v2

    .line 1644
    goto :goto_1

    .line 1647
    :catchall_0
    move-exception v0

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2280
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2285
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2287
    return v0

    .line 2285
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 3490
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3491
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 3493
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3495
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3497
    return-object v0

    .line 3495
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1369
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1370
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1372
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1373
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1374
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1380
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1382
    return-object v0

    .line 1376
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v1, v0, p2, p3}, Lcom/google/android/gms/games/a/ab;->c(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1380
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->d:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/au;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 4061
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    const-string v1, "Must be GmsCore context"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 4062
    new-instance v0, Lcom/google/android/gms/games/a/av;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-boolean v2, v0, Lcom/google/android/gms/games/a/av;->a:Z

    iput-boolean v2, v0, Lcom/google/android/gms/games/a/av;->g:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method public final e(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3456
    invoke-static {p1}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 3457
    const-string v1, "forcedMetadataRefreshVersion"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 3459
    invoke-static {v0}, Lcom/google/android/gms/common/util/x;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 3479
    :goto_0
    return-void

    .line 3464
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;)Ljava/util/HashSet;

    move-result-object v0

    .line 3465
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3467
    :try_start_0
    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 3468
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/ab;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 3471
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0

    :cond_1
    new-array v0, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3475
    invoke-static {p1}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 3476
    const-string v1, "forcedMetadataRefreshVersion"

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->b()I

    move-result v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 3478
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto :goto_0
.end method

.method public final e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 3705
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3707
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/k;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3709
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3710
    return-void

    .line 3709
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->p:Lcom/google/android/gms/games/a/k;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;)V
    .locals 5

    .prologue
    .line 840
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 841
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 844
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v3

    .line 845
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v0

    .line 850
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/au;->a()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 874
    :goto_0
    return-void

    .line 856
    :cond_0
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    .line 858
    :try_start_0
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v4

    if-lez v4, :cond_1

    .line 859
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v4

    if-lez v4, :cond_1

    .line 860
    new-instance v0, Lcom/google/android/gms/games/t;

    invoke-direct {v0, v3}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 861
    const/4 v4, 0x0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 865
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 869
    if-nez v0, :cond_2

    .line 870
    const-string v0, "DataBroker"

    const-string v1, "Unable to load profile for player!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 865
    :catchall_0
    move-exception v0

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    .line 873
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/games/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1948
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1950
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 1951
    const/4 v0, 0x6

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1953
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1954
    return-void

    .line 1953
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic e()Z
    .locals 1

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v0

    return v0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1101
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v1, v1, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1102
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1105
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 1106
    const/4 v0, 0x2

    .line 1111
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1113
    return v0

    .line 1108
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/bj;->c(Lcom/google/android/gms/games/a/au;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 1111
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    iget-object v2, v2, Lcom/google/android/gms/games/a/bj;->g:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2458
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2459
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2462
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/cf;->f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 2464
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2466
    return v0

    .line 2464
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 4022
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4023
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 4025
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 4027
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 4029
    return-object v0

    .line 4027
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1511
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1512
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1514
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1516
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1518
    return-object v0

    .line 1516
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->l:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->f()V

    return-void
.end method

.method public final f(Landroid/content/Context;)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v1, 0x0

    .line 3507
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3511
    :try_start_0
    invoke-static {p1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 3512
    const-string v2, "com.google"

    invoke-virtual {v0, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    move v0, v1

    .line 3513
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 3514
    aget-object v3, v2, v0

    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 3516
    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1, v3}, Lcom/google/android/gms/games/a/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    .line 3517
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aget-object v3, v2, v0

    invoke-static {p1, v3}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Landroid/accounts/Account;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3513
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 3520
    :cond_0
    new-array v0, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3521
    return-void

    .line 3520
    :catchall_0
    move-exception v0

    new-array v2, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v3, v2, v1

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1969
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1971
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 1972
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1974
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1975
    return-void

    .line 1974
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1267
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1268
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1270
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1271
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/bj;->e(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1273
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1275
    return-object v0

    .line 1273
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1538
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1539
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1541
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1543
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1545
    return-object v0

    .line 1543
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->m:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2536
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2537
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2539
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2541
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2543
    return-object v0

    .line 2541
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic g()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->g()V

    return-void
.end method

.method public final g(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 4041
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 4043
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    invoke-static {p1, p2, p3}, Lcom/google/android/gms/games/a/cb;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4045
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 4046
    return-void

    .line 4045
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->r:Lcom/google/android/gms/games/a/cb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1991
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1993
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 1994
    const/4 v0, 0x7

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1996
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1997
    return-void

    .line 1996
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1416
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1417
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1419
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 1420
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 1421
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1426
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1428
    return-object v0

    .line 1423
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/ab;->f(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 1426
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v2, v2, Lcom/google/android/gms/games/a/ab;->h:Lcom/google/android/gms/games/a/bb;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 2484
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2485
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2487
    if-eqz p3, :cond_0

    .line 2488
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 2492
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bc;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 2493
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-static {p1, p2, v0}, Lcom/google/android/gms/games/a/bc;->a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2495
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2497
    return-object v0

    .line 2495
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final h(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2715
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2716
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2718
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2720
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2722
    return-object v0

    .line 2720
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final h(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 2

    .prologue
    .line 4083
    new-instance v0, Lcom/google/android/gms/games/a/av;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->g:Z

    .line 4086
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4087
    iput-object p3, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    .line 4089
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 4090
    iput-object p3, v0, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    .line 4094
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic h()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    return-void
.end method

.method public final h(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2757
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2759
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 2760
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2762
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2763
    return-void

    .line 2762
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/games/a/au;)I
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1438
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1439
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1}, Lcom/google/android/gms/games/a/ab;->g(Lcom/google/android/gms/games/a/au;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1443
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1445
    return v0

    .line 1443
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x2

    .line 2779
    new-array v0, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v0, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2780
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2783
    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    move v0, v1

    .line 2793
    :goto_0
    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2797
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_1

    .line 2800
    :cond_0
    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 2802
    :cond_1
    return v0

    .line 2786
    :cond_2
    :try_start_1
    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v2, :cond_3

    .line 2787
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/bv;->j()V

    .line 2789
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a/bv;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 2790
    const/16 v2, 0xd

    invoke-virtual {p2, v2}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2793
    :catchall_0
    move-exception v0

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->q:Lcom/google/android/gms/games/a/bv;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final i(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3142
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3144
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 3145
    if-nez v0, :cond_0

    .line 3146
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/a/bp;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 3151
    :goto_0
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3153
    return-object v0

    .line 3148
    :cond_0
    :try_start_1
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    goto :goto_0

    .line 3151
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final bridge synthetic i()V
    .locals 0

    .prologue
    .line 107
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    return-void
.end method

.method public final j(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I
    .locals 6

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2818
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v1, v0, v3

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2819
    :try_start_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v0, :cond_0

    .line 2822
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bc;->j()V

    .line 2825
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->u:Lcom/google/android/gms/games/a/cf;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/cf;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 2826
    const/16 v0, 0x9

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V

    .line 2827
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bc;->b(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 2828
    const/16 v1, 0xe

    invoke-virtual {p2, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2830
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2834
    const/4 v1, 0x4

    if-eq v0, v1, :cond_1

    const/4 v1, 0x3

    if-eq v0, v1, :cond_1

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_2

    .line 2837
    :cond_1
    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 2839
    :cond_2
    return v0

    .line 2830
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->s:Lcom/google/android/gms/games/a/bc;

    aput-object v2, v1, v3

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final j(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1455
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1456
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1458
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1}, Lcom/google/android/gms/games/a/ab;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1460
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1462
    return-object v0

    .line 1460
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final k(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/service/ae;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1581
    new-instance v0, Lcom/google/android/gms/games/service/ae;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/ae;-><init>()V

    .line 1582
    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1584
    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 1585
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1586
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1}, Lcom/google/android/gms/games/a/ab;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v3

    iput v3, v0, Lcom/google/android/gms/games/service/ae;->a:I

    .line 1587
    invoke-static {v1, v2}, Lcom/google/android/gms/games/i/a;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)I

    move-result v3

    iput v3, v0, Lcom/google/android/gms/games/service/ae;->b:I

    .line 1588
    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {v1, v2}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/games/service/ae;->c:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1591
    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1593
    return-object v0

    .line 1591
    :catchall_0
    move-exception v0

    new-array v1, v5, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final k(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 15

    .prologue
    .line 2857
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2860
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Ljava/util/ArrayList;

    move-result-object v6

    .line 2861
    const/16 v1, 0x11

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2863
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2867
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    .line 2918
    :goto_0
    return-void

    .line 2863
    :catchall_0
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1

    .line 2872
    :cond_0
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v7

    .line 2876
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v8

    .line 2877
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v8}, Ljava/util/ArrayList;-><init>(I)V

    .line 2878
    const/4 v1, 0x0

    move v5, v1

    :goto_1
    if-ge v5, v8, :cond_4

    .line 2879
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2882
    :try_start_1
    invoke-virtual {v6, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/ag;

    .line 2883
    iget-object v10, v1, Lcom/google/android/gms/games/a/ag;->a:Ljava/lang/String;

    .line 2884
    invoke-virtual {v7}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v2

    iput-object v10, v2, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v11

    .line 2890
    iget-object v12, v1, Lcom/google/android/gms/games/a/ag;->d:Ljava/util/HashSet;

    .line 2891
    const/4 v3, 0x1

    .line 2892
    const/4 v2, 0x0

    move v4, v2

    :goto_2
    sget-object v2, Lcom/google/android/gms/games/d/a;->c:[Ljava/lang/String;

    array-length v2, v2

    if-ge v4, v2, :cond_2

    .line 2893
    sget-object v2, Lcom/google/android/gms/games/d/a;->c:[Ljava/lang/String;

    aget-object v13, v2, v4

    .line 2894
    invoke-interface {v12, v13}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2895
    const/4 v2, -0x1

    invoke-virtual {v13}, Ljava/lang/String;->hashCode()I

    move-result v14

    sparse-switch v14, :sswitch_data_0

    :cond_1
    :goto_3
    packed-switch v2, :pswitch_data_0

    const/4 v2, 0x1

    :goto_4
    and-int/2addr v2, v3

    .line 2892
    :goto_5
    add-int/lit8 v3, v4, 0x1

    move v4, v3

    move v3, v2

    goto :goto_2

    .line 2895
    :sswitch_0
    const-string v14, "ACHIEVEMENT_DEFINITION"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v2, 0x0

    goto :goto_3

    :sswitch_1
    const-string v14, "LEADERBOARD_DEFINITION"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v2, 0x1

    goto :goto_3

    :sswitch_2
    const-string v14, "EVENT_DEFINITION"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v2, 0x2

    goto :goto_3

    :sswitch_3
    const-string v14, "ACHIEVEMENT_INSTANCE"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v2, 0x3

    goto :goto_3

    :sswitch_4
    const-string v14, "EVENT_INSTANCE"

    invoke-virtual {v13, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_1

    const/4 v2, 0x4

    goto :goto_3

    :pswitch_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v2, v11, v13}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z

    move-result v2

    const/16 v13, 0x14

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_4

    .line 2906
    :catchall_1
    move-exception v1

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1

    .line 2895
    :pswitch_1
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v2, v11, v13}, Lcom/google/android/gms/games/a/ay;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z

    move-result v2

    const/16 v13, 0x18

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/gms/games/service/ad;->a(I)V

    goto :goto_4

    :pswitch_2
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v2, v11, v13}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z

    move-result v2

    const/16 v13, 0x16

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/gms/games/service/ad;->a(I)V

    goto/16 :goto_4

    :pswitch_3
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v2, v11, v13}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z

    move-result v2

    const/16 v13, 0x15

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/gms/games/service/ad;->a(I)V

    goto/16 :goto_4

    :pswitch_4
    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v2, v11, v13}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z

    move-result v2

    const/16 v13, 0x17

    move-object/from16 v0, p2

    invoke-virtual {v0, v13}, Lcom/google/android/gms/games/service/ad;->a(I)V

    goto/16 :goto_4

    .line 2900
    :cond_2
    if-eqz v3, :cond_3

    .line 2901
    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 2906
    :goto_6
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->k:Lcom/google/android/gms/games/a/ay;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2878
    add-int/lit8 v1, v5, 0x1

    move v5, v1

    goto/16 :goto_1

    .line 2903
    :cond_3
    :try_start_3
    const-string v1, "DataBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to update game data for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    goto :goto_6

    .line 2911
    :cond_4
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2913
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v7, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, v7, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v2, v9}, Lcom/google/android/gms/games/a/ab;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)V

    .line 2915
    const/16 v1, 0x12

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_2

    .line 2917
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto/16 :goto_0

    :catchall_2
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1

    :cond_5
    move v2, v3

    goto/16 :goto_5

    .line 2895
    nop

    :sswitch_data_0
    .sparse-switch
        -0x11699b46 -> :sswitch_4
        0x30558c38 -> :sswitch_2
        0x5b182483 -> :sswitch_0
        0x661caf75 -> :sswitch_1
        0x752893c5 -> :sswitch_3
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public final l(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1903
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1904
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1906
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1908
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1910
    return-object v0

    .line 1908
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final l(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 2969
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2971
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V

    .line 2972
    const/16 v0, 0x13

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2974
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2975
    return-void

    .line 2974
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final m(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    const/4 v2, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1923
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v1, v0, v4

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 1924
    invoke-static {v3}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 1926
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/bj;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1927
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1928
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1930
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 1932
    return-object v0

    .line 1930
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->g:Lcom/google/android/gms/games/a/c;

    aput-object v2, v1, v4

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->m:Lcom/google/android/gms/games/a/bj;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final m(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    .locals 17

    .prologue
    .line 3335
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 3336
    move-object/from16 v0, p1

    iget-object v12, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 3339
    new-instance v1, Lcom/google/android/gms/games/quest/b;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 3340
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3342
    :try_start_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/a/bp;->c(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/quest/b;

    move-result-object v13

    .line 3343
    const/16 v1, 0x10

    move-object/from16 v0, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3345
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3348
    invoke-virtual {v13}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v14

    .line 3351
    if-eqz v14, :cond_0

    move-object/from16 v0, p0

    invoke-direct {v0, v12}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3352
    :cond_0
    invoke-virtual {v13}, Lcom/google/android/gms/games/quest/b;->w_()V

    .line 3389
    :cond_1
    :goto_0
    return-void

    .line 3345
    :catchall_0
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1

    .line 3357
    :cond_2
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v14}, Ljava/util/ArrayList;-><init>(I)V

    .line 3359
    const/4 v1, 0x0

    move v10, v1

    :goto_1
    if-ge v10, v14, :cond_3

    .line 3360
    :try_start_1
    invoke-virtual {v13, v10}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    move-object v9, v0

    .line 3361
    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Quest;->a()Ljava/lang/String;

    move-result-object v4

    .line 3362
    invoke-static {}, Lcom/google/android/gms/games/a/bh;->a()Ljava/lang/String;

    move-result-object v3

    .line 3364
    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 3365
    sget v2, Lcom/google/android/gms/p;->kF:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 3366
    sget v2, Lcom/google/android/gms/p;->kG:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 3367
    sget v2, Lcom/google/android/gms/p;->kE:I

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Quest;->d()Ljava/lang/String;

    move-result-object v16

    aput-object v16, v5, v8

    invoke-virtual {v1, v2, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 3370
    new-instance v1, Lcom/google/android/gms/games/a/bi;

    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    const/16 v5, 0x8

    invoke-interface {v9}, Lcom/google/android/gms/games/quest/Quest;->l()Lcom/google/android/gms/games/Game;

    move-result-object v9

    invoke-interface {v9}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v9

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/games/a/bi;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    invoke-virtual {v15, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3359
    add-int/lit8 v1, v10, 0x1

    move v10, v1

    goto :goto_1

    .line 3376
    :cond_3
    invoke-virtual {v13}, Lcom/google/android/gms/games/quest/b;->w_()V

    .line 3379
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_1

    .line 3384
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3386
    :try_start_2
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    invoke-static {v11, v12, v15}, Lcom/google/android/gms/games/a/bh;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 3388
    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/a/bb;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v3, v1, v2

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto/16 :goto_0

    .line 3376
    :catchall_1
    move-exception v1

    invoke-virtual {v13}, Lcom/google/android/gms/games/quest/b;->w_()V

    throw v1

    .line 3388
    :catchall_2
    move-exception v1

    const/4 v2, 0x1

    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/a/t;->l:Lcom/google/android/gms/games/a/bh;

    aput-object v4, v2, v3

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v1
.end method

.method public final n(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3402
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 3403
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3407
    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_2

    .line 3408
    const/4 v0, 0x2

    .line 3414
    :goto_0
    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3418
    const/4 v1, 0x4

    if-eq v0, v1, :cond_0

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/16 v1, 0x1f4

    if-ne v0, v1, :cond_1

    .line 3421
    :cond_0
    iget-object v1, p2, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 3423
    :cond_1
    return v0

    .line 3410
    :cond_2
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/bp;->d(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 3411
    const/16 v1, 0xf

    invoke-virtual {p2, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 3414
    :catchall_0
    move-exception v0

    new-array v1, v4, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->n:Lcom/google/android/gms/games/a/bp;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final n(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2008
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2009
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2011
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2013
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2015
    return-object v0

    .line 2013
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->i:Lcom/google/android/gms/games/a/u;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final o(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 2734
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v3

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2735
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    .line 2737
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/t;->r(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 2738
    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 2740
    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2742
    return-object v0

    .line 2740
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final p(Lcom/google/android/gms/games/a/au;)J
    .locals 5

    .prologue
    const/4 v2, 0x1

    const/4 v4, 0x0

    .line 2988
    new-array v0, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v1, v0, v4

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 2989
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-static {p1}, Lcom/google/android/gms/games/a/ab;->b(Lcom/google/android/gms/games/a/au;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 2993
    new-array v2, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v3, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v3, v2, v4

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 2995
    return-wide v0

    .line 2993
    :catchall_0
    move-exception v0

    new-array v1, v2, [Lcom/google/android/gms/games/a/bb;

    iget-object v2, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    aput-object v2, v1, v4

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method

.method public final q(Lcom/google/android/gms/games/a/au;)V
    .locals 4

    .prologue
    .line 3551
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->a([Lcom/google/android/gms/games/a/bb;)V

    .line 3553
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {p1}, Lcom/google/android/gms/games/a/a;->b(Lcom/google/android/gms/games/a/au;)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 3554
    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 3562
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    .line 3563
    :goto_0
    return-void

    .line 3558
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/ab;->e(Lcom/google/android/gms/games/a/au;)V

    .line 3560
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->f:Lcom/google/android/gms/games/a/a;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/a/a;->a(Lcom/google/android/gms/games/a/au;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 3562
    iget-object v0, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v0, v0, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v0}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/t;->j:Lcom/google/android/gms/games/a/ab;

    iget-object v1, v1, Lcom/google/android/gms/games/a/ab;->p:[Lcom/google/android/gms/games/a/bb;

    invoke-static {v1}, Lcom/google/android/gms/games/a/t;->b([Lcom/google/android/gms/games/a/bb;)V

    throw v0
.end method
