.class final Lcom/google/android/gms/car/senderprotocol/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/ah;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/senderprotocol/ah;)V
    .locals 0

    .prologue
    .line 194
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 199
    :try_start_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    move-wide v2, v0

    .line 200
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/aj;

    iget-wide v0, v0, Lcom/google/android/gms/car/senderprotocol/aj;->b:J

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/aj;

    .line 203
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/ah;->c(Lcom/google/android/gms/car/senderprotocol/ah;)Lcom/google/android/gms/car/gx;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/aj;->a:Lcom/google/android/c/b/ar;

    const/16 v2, 0x80

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/c/b/ar;I)V

    .line 205
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    move-wide v2, v0

    .line 206
    goto :goto_0

    .line 208
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/ah;->b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/aj;

    iget-wide v0, v0, Lcom/google/android/gms/car/senderprotocol/aj;->b:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 210
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/ai;->a:Lcom/google/android/gms/car/senderprotocol/ah;

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/ah;->d(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    move-result-object v2

    sget-object v3, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p0, v0, v1, v3}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 212
    :cond_1
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0
.end method
