.class final Lcom/google/android/gms/games/service/statemachine/roomservice/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/realtime/network/a;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/e;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/e;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 158
    return-void
.end method

.method public final a(ILjava/lang/String;Z)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 177
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_0

    .line 178
    const-string v1, "Message Send: to %s, result = %s, token = %s"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    aput-object p2, v2, v0

    const/4 v3, 0x1

    invoke-static {p3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->b()V

    .line 181
    :cond_0
    if-eqz p3, :cond_1

    .line 183
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomservice/m;

    invoke-direct {v2, p1, v0, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/m;-><init>(IILjava/lang/String;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 184
    return-void

    .line 181
    :cond_1
    const/16 v0, 0x1b59

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/realtime/a;)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/o;-><init>(Lcom/google/android/gms/games/realtime/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 168
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/f;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomservice/f;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 153
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 198
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v0, :cond_0

    .line 199
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "ReceivedBuzzNotification"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    :cond_0
    :try_start_0
    new-instance v0, Lorg/json/JSONArray;

    invoke-direct {v0, p1}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 203
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 204
    sget-boolean v1, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v1, :cond_1

    .line 205
    const-string v1, "RoomServiceStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Received notification "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    :cond_1
    const-string v1, "{"

    invoke-virtual {p2, v1}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v1

    invoke-virtual {p2, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 208
    if-eqz v0, :cond_2

    .line 209
    const-string v2, "STATUS"

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v2

    if-eqz v2, :cond_3

    .line 211
    :try_start_1
    invoke-static {v1}, Lcom/google/android/gms/games/service/RoomAndroidService;->a(Ljava/lang/String;)Lcom/google/android/gms/games/h/a/ev;

    move-result-object v0

    .line 213
    const-string v1, "RoomServiceStateMachine"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ev;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomservice/s;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/s;-><init>(Lcom/google/android/gms/games/h/a/ev;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V
    :try_end_1
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_1

    .line 226
    :cond_2
    :goto_0
    return-void

    .line 215
    :catch_0
    move-exception v0

    .line 216
    :try_start_2
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "MatchStatus parsing error for payload"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 217
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_2
    .catch Lorg/json/JSONException; {:try_start_2 .. :try_end_2} :catch_1

    .line 223
    :catch_1
    move-exception v0

    .line 224
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Json parsing error for payload"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 225
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 220
    :cond_3
    :try_start_3
    const-string v1, "RoomServiceStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid notification type :"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_3
    .catch Lorg/json/JSONException; {:try_start_3 .. :try_end_3} :catch_1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/l;

    const/4 v2, 0x1

    invoke-direct {v1, p2, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/l;-><init>([BLjava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 189
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/n;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/n;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 163
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/realtime/a;)V
    .locals 2

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/p;-><init>(Lcom/google/android/gms/games/realtime/a;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 173
    return-void
.end method

.method public final b(Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/v;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomservice/l;

    const/4 v2, 0x0

    invoke-direct {v1, p2, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/l;-><init>([BLjava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 194
    return-void
.end method
