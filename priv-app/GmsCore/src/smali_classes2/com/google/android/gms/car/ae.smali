.class public final Lcom/google/android/gms/car/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/qn;


# static fields
.field public static final a:Landroid/content/ComponentName;

.field public static final b:Landroid/content/Intent;

.field public static final c:Landroid/content/ComponentName;

.field public static final d:Landroid/content/ComponentName;

.field public static final e:Landroid/content/ComponentName;

.field public static final f:Landroid/content/ComponentName;

.field public static final g:Landroid/content/ComponentName;

.field public static final h:Landroid/content/ComponentName;

.field private static final j:Lcom/google/android/gms/car/ii;

.field private static k:Ljava/util/ArrayList;

.field private static final l:Landroid/content/ComponentName;

.field private static final m:Landroid/content/ComponentName;

.field private static final n:Landroid/content/ComponentName;

.field private static final o:Landroid/content/ComponentName;

.field private static final p:Landroid/content/ComponentName;

.field private static final q:Landroid/content/ComponentName;

.field private static final r:Landroid/content/ComponentName;

.field private static final s:Landroid/content/ComponentName;

.field private static final t:Ljava/util/Map;


# instance fields
.field private final A:Landroid/util/SparseArray;

.field private final B:Lcom/google/android/gms/car/ap;

.field private final C:Z

.field private D:Lcom/google/android/gms/car/ja;

.field private E:Lcom/google/android/gms/car/y;

.field private final F:Ljava/util/concurrent/Semaphore;

.field private final G:Ljava/util/concurrent/Semaphore;

.field private volatile H:Z

.field private I:Landroid/content/ComponentName;

.field private J:Lcom/google/android/gms/car/b/d;

.field private K:Lcom/google/android/gms/car/y;

.field private L:Landroid/content/ComponentName;

.field private M:Z

.field private N:Z

.field private O:Z

.field private final P:Ljava/util/Set;

.field private final Q:Landroid/os/Handler;

.field private final R:Landroid/os/Handler;

.field private volatile S:Z

.field private final T:Landroid/content/BroadcastReceiver;

.field private final U:Landroid/content/ServiceConnection;

.field private final V:Ljava/lang/Runnable;

.field private final W:Ljava/util/Set;

.field private final X:Landroid/os/HandlerThread;

.field private final Y:Ljava/util/List;

.field public final i:Landroid/content/ComponentName;

.field private final u:Landroid/content/Context;

.field private final v:Lcom/google/android/gms/car/gx;

.field private final w:Lcom/google/android/gms/car/senderprotocol/be;

.field private final x:Lcom/google/android/gms/car/on;

.field private final y:Ljava/util/Map;

.field private final z:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 74
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.assert_main_thread"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 86
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.apps.gmm.car"

    const-string v2, "com.google.android.apps.gmm.car.GmmCarProjectionService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->a:Landroid/content/ComponentName;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 97
    sput-object v0, Lcom/google/android/gms/car/ae;->k:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    const-string v2, "com.google.android.projection.kitchensink"

    const-string v3, "com.google.android.projection.kitchensink.KitchenSinkService"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 100
    sget-object v0, Lcom/google/android/gms/car/ae;->k:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    const-string v2, "com.google.android.projection.gearhead.receivertest"

    const-string v3, "com.google.android.projection.gearhead.receivertest.ReceiverTestService"

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 103
    sget-object v0, Lcom/google/android/gms/car/ae;->k:Ljava/util/ArrayList;

    new-instance v1, Landroid/util/Pair;

    sget-object v2, Lcom/google/android/gms/car/ae;->a:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/car/ae;->a:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MAIN"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 110
    sput-object v0, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    const-string v1, "android.intent.category.HOME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 111
    sget-object v0, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    const-string v1, "com.google.android.projection.gearhead"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 119
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.rail.RailService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->c:Landroid/content/ComponentName;

    .line 123
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.notifications.NotificationService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->l:Landroid/content/ComponentName;

    .line 127
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.notifications.GHNotificationListenerService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->m:Landroid/content/ComponentName;

    .line 132
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.googlequicksearchbox"

    const-string v2, "com.google.android.projection.SearchProjectionService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    .line 136
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.media.MediaService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->e:Landroid/content/ComponentName;

    .line 140
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.setup.DisclaimerService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->n:Landroid/content/ComponentName;

    .line 144
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.setup.TutorialService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->o:Landroid/content/ComponentName;

    .line 148
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.car.mockup"

    const-string v2, "com.google.android.car.mockup.MockupService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->p:Landroid/content/ComponentName;

    .line 152
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.input.RotaryImeService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->q:Landroid/content/ComponentName;

    .line 156
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.input.CarScreenKeyboardService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->r:Landroid/content/ComponentName;

    .line 160
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.telecom.TelecomService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->s:Landroid/content/ComponentName;

    .line 164
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.kitchensink"

    const-string v2, "com.google.android.projection.kitchensink.KitchenSinkService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->f:Landroid/content/ComponentName;

    .line 168
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead"

    const-string v2, "com.google.android.projection.gearhead.stream.StreamService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    .line 172
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.google.android.projection.gearhead.receivertest"

    const-string v2, "com.google.android.projection.gearhead.receivertest.ReceiverTestService"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ae;->h:Landroid/content/ComponentName;

    .line 182
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 185
    sput-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->c:Landroid/content/ComponentName;

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 186
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->l:Landroid/content/ComponentName;

    const/4 v2, 0x5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 188
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->n:Landroid/content/ComponentName;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 190
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->o:Landroid/content/ComponentName;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 192
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->p:Landroid/content/ComponentName;

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 194
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    const/4 v2, 0x6

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 196
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/senderprotocol/be;Lcom/google/android/gms/car/ip;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 412
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    .line 222
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    .line 253
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v4}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    .line 259
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v5}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    .line 262
    iput-boolean v4, p0, Lcom/google/android/gms/car/ae;->H:Z

    .line 264
    iput-object v6, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    .line 286
    iput-boolean v4, p0, Lcom/google/android/gms/car/ae;->O:Z

    .line 291
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->P:Ljava/util/Set;

    .line 302
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->R:Landroid/os/Handler;

    .line 306
    iput-boolean v4, p0, Lcom/google/android/gms/car/ae;->S:Z

    .line 307
    new-instance v0, Lcom/google/android/gms/car/af;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/af;-><init>(Lcom/google/android/gms/car/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->T:Landroid/content/BroadcastReceiver;

    .line 322
    new-instance v0, Lcom/google/android/gms/car/ag;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ag;-><init>(Lcom/google/android/gms/car/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->U:Landroid/content/ServiceConnection;

    .line 393
    new-instance v0, Lcom/google/android/gms/car/ai;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ai;-><init>(Lcom/google/android/gms/car/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->V:Ljava/lang/Runnable;

    .line 406
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    .line 407
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "WaitForStopThenStartClient"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->X:Landroid/os/HandlerThread;

    .line 409
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    .line 413
    iput-object p1, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    .line 414
    iput-object p2, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    .line 415
    iput-object p3, p0, Lcom/google/android/gms/car/ae;->w:Lcom/google/android/gms/car/senderprotocol/be;

    .line 416
    new-instance v0, Lcom/google/android/gms/car/on;

    invoke-direct {v0, p1, p4, p2}, Lcom/google/android/gms/car/on;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/ip;Lcom/google/android/gms/car/gx;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    .line 418
    new-instance v1, Lcom/google/android/gms/car/id;

    invoke-direct {v1, p1}, Lcom/google/android/gms/car/id;-><init>(Landroid/content/Context;)V

    .line 419
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    sget-object v2, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->C:Z

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    const-string v2, "gmm_package_name"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/gx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 422
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 423
    iput-object v6, p0, Lcom/google/android/gms/car/ae;->i:Landroid/content/ComponentName;

    .line 429
    :goto_0
    new-instance v0, Lcom/google/android/gms/car/ap;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/car/ap;-><init>(Lcom/google/android/gms/car/ae;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->B:Lcom/google/android/gms/car/ap;

    .line 430
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->B:Lcom/google/android/gms/car/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ap;->start()V

    .line 432
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 433
    const-string v2, "android.intent.action.projected.BVRA"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 434
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/car/ae;->T:Landroid/content/BroadcastReceiver;

    invoke-virtual {v2, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 435
    iput-boolean v5, p0, Lcom/google/android/gms/car/ae;->S:Z

    .line 436
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/car/ae;->m:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3, v0}, Lcom/google/android/gms/car/ae;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_0

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ":"

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :cond_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "enabled_notification_listeners"

    invoke-static {v2, v3, v0}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    const-string v0, "CAR.CAM"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.CAM"

    const-string v2, "Adding notification permission"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->X:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 439
    new-instance v0, Lcom/google/android/gms/car/pf;

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->X:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v6, v2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->Q:Landroid/os/Handler;

    .line 442
    iput-boolean v5, p0, Lcom/google/android/gms/car/ae;->H:Z

    .line 445
    invoke-virtual {v1}, Lcom/google/android/gms/car/id;->d()Z

    move-result v0

    .line 446
    if-eqz v0, :cond_2

    .line 447
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->p:Landroid/content/ComponentName;

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 449
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->C:Z

    if-eqz v0, :cond_4

    .line 450
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.IME"

    const-string v1, "initImeService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->h()I

    move-result v0

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->x()Landroid/content/ComponentName;

    move-result-object v0

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->x()Landroid/content/ComponentName;

    move-result-object v0

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    const/4 v2, 0x4

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->l()V

    .line 452
    :cond_4
    return-void

    .line 426
    :cond_5
    new-instance v2, Landroid/content/ComponentName;

    const-string v3, "com.google.android.apps.gmm.car.GmmCarProjectionService"

    invoke-direct {v2, v0, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/car/ae;->i:Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 450
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->f()Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarUiInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    sget-object v0, Lcom/google/android/gms/car/ae;->q:Landroid/content/ComponentName;

    goto :goto_1

    :cond_7
    sget-object v0, Lcom/google/android/gms/car/ae;->r:Landroid/content/ComponentName;

    goto :goto_1
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 1671
    sget-object v0, Lcom/google/android/gms/car/ae;->k:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1672
    new-instance v3, Landroid/content/ComponentName;

    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/String;

    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1674
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1675
    invoke-virtual {v0, v3}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1676
    invoke-static {p0, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1680
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/b/d;)Lcom/google/android/gms/car/b/d;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/y;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    return-object p1
.end method

.method private a(Ljava/lang/Throwable;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 1644
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1645
    const-string v0, "CAR.CAM"

    const-string v1, "Starting fallback client"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1648
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1649
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    if-nez v0, :cond_1

    .line 1652
    new-instance v0, Lcom/google/android/gms/car/ja;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ja;-><init>(Lcom/google/android/gms/car/ae;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    .line 1653
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v0, v3, v1}, Lcom/google/android/gms/car/on;->a(ILcom/google/android/gms/car/om;)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1655
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/ja;->a(Lcom/google/android/gms/car/oi;)V

    .line 1657
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ja;->b(Ljava/lang/Throwable;)V

    .line 1658
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ja;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1659
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v1, v3, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1661
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->p()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1662
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/oi;)Z

    .line 1664
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ja;->a(I)V

    .line 1665
    return-void
.end method

.method private a(ILandroid/content/ComponentName;)Z
    .locals 2

    .prologue
    .line 1909
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1910
    invoke-virtual {v0, p2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1912
    iget-boolean v1, p0, Lcom/google/android/gms/car/ae;->O:Z

    if-nez v1, :cond_0

    .line 1913
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1914
    const/4 v0, 0x0

    .line 1917
    :goto_0
    return v0

    :cond_0
    const/4 v1, 0x1

    invoke-direct {p0, v1, v0, v0}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(ILandroid/content/Intent;Landroid/content/Intent;)Z
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1483
    const-string v0, "CAR.CAM"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1485
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "startActivityManager called for intent: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " from: "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1489
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1490
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v0, :cond_1

    move v0, v2

    .line 1580
    :goto_0
    return v0

    .line 1495
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {p2}, Landroid/content/Intent;->cloneFilter()Landroid/content/Intent;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/nw;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    .line 1497
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 1498
    const-string v0, "CAR.CAM"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1499
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No matching Projection Clients found for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1502
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_3

    .line 1505
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.PACKAGE_ADDED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 1506
    const-string v1, "package"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addDataScheme(Ljava/lang/String;)V

    .line 1507
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    new-instance v3, Lcom/google/android/gms/car/al;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/al;-><init>(Lcom/google/android/gms/car/ae;)V

    invoke-virtual {v1, v3, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    :cond_3
    move v0, v2

    .line 1520
    goto :goto_0

    .line 1524
    :cond_4
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->serviceInfo:Landroid/content/pm/ServiceInfo;

    .line 1525
    iget-object v1, v0, Landroid/content/pm/ServiceInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, v1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    .line 1526
    iget-object v0, v0, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    .line 1527
    new-instance v4, Landroid/content/ComponentName;

    invoke-direct {v4, v1, v0}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1533
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1534
    if-eqz v0, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->n()Z

    move-result v1

    if-eqz v1, :cond_5

    sget-object v1, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-virtual {v1, v4}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    const/4 v1, 0x5

    if-ge p1, v1, :cond_5

    .line 1538
    invoke-virtual {v0, v5}, Lcom/google/android/gms/car/y;->a(I)V

    .line 1541
    :cond_5
    invoke-virtual {p2, v4}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1542
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1544
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/y;

    .line 1546
    sget-object v5, Lcom/google/android/gms/car/ae;->l:Landroid/content/ComponentName;

    invoke-virtual {v4, v5}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_7

    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->n()Z

    move-result v5

    if-eqz v5, :cond_7

    .line 1548
    const-string v0, "CAR.CAM"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1549
    const-string v0, "CAR.CAM"

    const-string v1, "Notification while tutorial is running, ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v0, v2

    .line 1551
    goto/16 :goto_0

    .line 1554
    :cond_7
    const/4 v2, 0x6

    if-ne p1, v2, :cond_8

    .line 1555
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->k()V

    .line 1559
    :cond_8
    if-eqz v0, :cond_c

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->n()Z

    move-result v2

    if-eqz v2, :cond_c

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 1560
    const-string v2, "CAR.CAM"

    invoke-static {v2, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 1561
    if-eqz v1, :cond_b

    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v1

    .line 1563
    :goto_1
    const-string v2, "CAR.CAM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Already a client for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", onNewIntent, lastClient "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1567
    :cond_9
    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/y;->a(Landroid/content/Intent;)V

    .line 1569
    const/4 v1, 0x5

    if-eq p1, v1, :cond_a

    .line 1570
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    .line 1572
    :cond_a
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v0, v3

    .line 1573
    goto/16 :goto_0

    .line 1561
    :cond_b
    const-string v1, ""

    goto :goto_1

    .line 1575
    :cond_c
    if-ne p1, v3, :cond_d

    .line 1576
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->R:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1578
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->B:Lcom/google/android/gms/car/ap;

    invoke-virtual {v0, v4, p1, p2, p3}, Lcom/google/android/gms/car/ap;->a(Landroid/content/ComponentName;ILandroid/content/Intent;Landroid/content/Intent;)V

    move v0, v3

    .line 1580
    goto/16 :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 1687
    invoke-virtual {p1}, Landroid/content/Intent;->cloneFilter()Landroid/content/Intent;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/gms/car/nw;->a(Landroid/content/Context;Landroid/content/Intent;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Landroid/content/Intent;I)Z
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v2, 0x0

    .line 1880
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->C()Lcom/google/android/gms/car/lb;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/car/lb;->b()Ljava/util/List;

    move-result-object v0

    .line 1882
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v1, v2

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/CarCall;

    .line 1883
    iget v0, v0, Lcom/google/android/gms/car/CarCall;->f:I

    const/4 v4, 0x7

    if-eq v0, v4, :cond_4

    .line 1884
    const/4 v0, 0x1

    :goto_1
    move v1, v0

    .line 1886
    goto :goto_0

    .line 1887
    :cond_0
    if-eqz v1, :cond_2

    .line 1890
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1891
    const-string v0, "CAR.CAM"

    const-string v1, "GSA is unavailable while there are active calls."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_1
    move v0, v2

    .line 1905
    :goto_2
    return v0

    .line 1895
    :catch_0
    move-exception v0

    .line 1896
    const-string v1, "CAR.CAM"

    const-string v2, "Unable to get CarCallService. This should never happen."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1899
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->K()Lcom/google/android/gms/car/at;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/at;->a(I)Lcom/google/android/gms/car/at;

    .line 1901
    const-string v0, "CAR.CAM"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1902
    const-string v0, "CAR.CAM"

    const-string v1, "Starting GSA"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1904
    :cond_3
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    sget-object v1, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1905
    invoke-direct {p0, v0, p1, p1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z

    move-result v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private declared-synchronized a(Landroid/content/Intent;Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 633
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 634
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 637
    const/4 v0, 0x0

    .line 648
    :goto_0
    monitor-exit p0

    return v0

    .line 639
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    .line 640
    sget-object v1, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-virtual {v1, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 644
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Intent;I)Z

    move-result v0

    goto :goto_0

    .line 646
    :cond_1
    invoke-static {p1}, Lcom/google/android/gms/car/ae;->e(Landroid/content/Intent;)I

    move-result v0

    .line 648
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private a(Landroid/view/KeyEvent;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 1838
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 1839
    sget-object v2, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1840
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 1841
    invoke-virtual {v2, v1, v0}, Landroid/content/pm/PackageManager;->queryIntentServices(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v2

    .line 1842
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 1843
    if-eqz p1, :cond_1

    .line 1844
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1845
    const-string v2, "android.intent.action.VOICE_COMMAND"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1846
    const-string v2, "KEY_CODE"

    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v3

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1847
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {v2, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1862
    :cond_0
    :goto_0
    return v0

    .line 1849
    :cond_1
    const-string v1, "CAR.CAM"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1850
    const-string v1, "CAR.CAM"

    const-string v2, "Received broadcast to start GSA but projected mode GSA not installed."

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1857
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/gms/car/ae;->O:Z

    if-nez v2, :cond_3

    .line 1858
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1862
    :cond_3
    const/4 v0, 0x2

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Intent;I)Z

    move-result v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ae;)Z
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->n()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/y;->a(I)V

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    move-result v0

    return v0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/4 v8, -0x1

    const/4 v0, 0x0

    .line 1710
    invoke-virtual {p1, p0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v4

    .line 1711
    if-ne v4, v8, :cond_1

    .line 1731
    :cond_0
    :goto_0
    return v0

    .line 1716
    :cond_1
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    .line 1717
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v2

    .line 1718
    const-string v3, ":"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;I)I

    move-result v6

    .line 1719
    const-string v3, ":"

    invoke-virtual {p1, v3, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;I)I

    move-result v7

    .line 1720
    if-ne v6, v8, :cond_4

    if-nez v4, :cond_4

    move v3, v1

    .line 1721
    :goto_1
    if-ne v7, v8, :cond_5

    add-int v8, v4, v5

    if-ne v8, v2, :cond_5

    move v2, v1

    .line 1726
    :goto_2
    if-nez v3, :cond_2

    add-int/lit8 v3, v4, -0x1

    if-ne v6, v3, :cond_0

    :cond_2
    if-nez v2, :cond_3

    add-int v2, v4, v5

    if-ne v2, v7, :cond_0

    :cond_3
    move v0, v1

    .line 1729
    goto :goto_0

    :cond_4
    move v3, v0

    .line 1720
    goto :goto_1

    :cond_5
    move v2, v0

    .line 1721
    goto :goto_2
.end method

.method public static b(Landroid/content/Context;)V
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1743
    invoke-static {p0}, Lcom/google/android/gms/car/ae;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1775
    :cond_0
    :goto_0
    return-void

    .line 1747
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/car/ae;->d(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    .line 1748
    sget-object v2, Lcom/google/android/gms/car/ae;->m:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v3

    .line 1749
    invoke-static {v3, v0}, Lcom/google/android/gms/car/ae;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1754
    const-string v2, ":"

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1755
    const/4 v0, 0x1

    .line 1756
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 1757
    array-length v6, v4

    move v2, v1

    :goto_1
    if-ge v2, v6, :cond_4

    aget-object v7, v4, v2

    .line 1758
    invoke-virtual {v7, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_2

    .line 1759
    if-eqz v0, :cond_3

    move v0, v1

    .line 1766
    :goto_2
    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1757
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1764
    :cond_3
    const-string v8, ":"

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_2

    .line 1769
    :cond_4
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_notification_listeners"

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Secure;->putString(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Z

    .line 1772
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1773
    const-string v0, "CAR.CAM"

    const-string v1, "Removing notification permission"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private b(Landroid/content/res/Configuration;I)V
    .locals 3

    .prologue
    .line 939
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 940
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onVideoConfigurationChanged, reason 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 943
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 944
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    monitor-enter v1

    .line 947
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 948
    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/y;->a(Landroid/content/res/Configuration;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 950
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/ae;)Z
    .locals 1

    .prologue
    .line 71
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/ae;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->m()V

    return-void
.end method

.method public static c(Lcom/google/android/gms/car/y;)V
    .locals 3

    .prologue
    .line 1051
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1052
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityManagerStartAborted for CarActivityManager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1056
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1057
    return-void
.end method

.method private static c(Landroid/content/Context;)Z
    .locals 3

    .prologue
    .line 1696
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/car/ae;->m:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1698
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v2, Lcom/google/android/gms/car/ae;->m:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not signed by Google"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1699
    const/4 v0, 0x0

    .line 1701
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static d(Landroid/content/Context;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1814
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "enabled_notification_listeners"

    invoke-static {v0, v1}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1816
    if-nez v0, :cond_0

    .line 1817
    const-string v0, ""

    .line 1819
    :cond_0
    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/ae;)Z
    .locals 1

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    return v0
.end method

.method private static e(Landroid/content/Intent;)I
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 2157
    invoke-virtual {p0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v2

    .line 2159
    if-eqz p0, :cond_1

    sget-object v1, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-virtual {v1}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Intent;->getPackage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v1

    sget-object v3, Lcom/google/android/gms/car/ae;->b:Landroid/content/Intent;

    invoke-virtual {v3}, Landroid/content/Intent;->getCategories()Ljava/util/Set;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->containsAll(Ljava/util/Collection;)Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v0

    :goto_0
    if-eqz v1, :cond_2

    .line 2161
    const/4 v0, 0x3

    .line 2168
    :cond_0
    :goto_1
    return v0

    .line 2159
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2163
    :cond_2
    sget-object v1, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method

.method static synthetic e(Lcom/google/android/gms/car/ae;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->l()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/car/ae;)Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/ae;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/car/ae;)V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    return-void
.end method

.method private h(Lcom/google/android/gms/car/y;)V
    .locals 3

    .prologue
    .line 1401
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1402
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1403
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    if-ne v1, p1, :cond_0

    .line 1404
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->delete(I)V

    .line 1406
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->r()V

    .line 1407
    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/on;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    return-object v0
.end method

.method private j()V
    .locals 4

    .prologue
    .line 1035
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    .line 1036
    if-nez v0, :cond_0

    .line 1037
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1043
    :goto_0
    return-void

    .line 1039
    :cond_0
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "completion semaphore has permits already "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1040
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->F:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    return-object v0
.end method

.method private declared-synchronized k()V
    .locals 3

    .prologue
    .line 1349
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1350
    const-string v0, "CAR.IME"

    const-string v1, "stopInput"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1353
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1354
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_2

    .line 1363
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1360
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->w()V

    .line 1361
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    .line 1362
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    invoke-interface {v0}, Lcom/google/android/gms/car/b/d;->a()V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    new-instance v0, Lcom/google/android/gms/car/ak;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ak;-><init>(Lcom/google/android/gms/car/ae;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 1349
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1362
    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "CAR.IME"

    const-string v2, "stopInput() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_1
.end method

.method static synthetic l(Lcom/google/android/gms/car/ae;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    return-object v0
.end method

.method private l()V
    .locals 4

    .prologue
    .line 1596
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.car.BIND_CAR_INPUT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 1597
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 1598
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->I()Lcom/google/android/gms/car/es;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/car/es;->b:Landroid/graphics/Point;

    .line 1599
    if-eqz v1, :cond_0

    .line 1600
    const-string v2, "touchpad_width"

    iget v3, v1, Landroid/graphics/Point;->x:I

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1601
    const-string v2, "touchpad_height"

    iget v1, v1, Landroid/graphics/Point;->y:I

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1603
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->U:Landroid/content/ServiceConnection;

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->M:Z

    .line 1607
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->M:Z

    if-nez v0, :cond_1

    .line 1608
    const-string v0, "CAR.IME"

    const-string v1, "Couldn\'t bind to Input Service."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1610
    :cond_1
    return-void
.end method

.method private m()V
    .locals 2

    .prologue
    .line 1627
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1628
    const-string v0, "CAR.IME"

    const-string v1, "stopImeService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1630
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->M:Z

    if-eqz v0, :cond_1

    .line 1631
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->U:Landroid/content/ServiceConnection;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 1632
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->M:Z

    .line 1634
    :cond_1
    return-void
.end method

.method static synthetic m(Lcom/google/android/gms/car/ae;)V
    .locals 4

    .prologue
    .line 71
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x1388

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;J)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CAR.CAM"

    const-string v1, "Timeout waiting for previous client to stop!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method static synthetic n(Lcom/google/android/gms/car/ae;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method private n()Z
    .locals 3

    .prologue
    .line 1823
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1825
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/car/ae;->n:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/car/ae;->o:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1829
    const/4 v0, 0x1

    .line 1831
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic o(Lcom/google/android/gms/car/ae;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->Q:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 512
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 513
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 516
    const-string v0, "CAR.CAM"

    const-string v1, "onFocusLost called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 553
    :goto_0
    monitor-exit p0

    return-void

    .line 521
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 522
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v2

    sget-object v3, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 525
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/y;->a(I)V

    .line 529
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->P()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    sget-object v2, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 532
    if-eqz v0, :cond_2

    .line 533
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 512
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 536
    :cond_3
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 541
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-eqz v0, :cond_6

    .line 542
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 543
    const-string v0, "CAR.CAM"

    const-string v1, "shut down current client connection and release semaphore"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 545
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/y;->a(I)V

    .line 546
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    .line 547
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    .line 550
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->B:Lcom/google/android/gms/car/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ap;->b()V

    .line 552
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final declared-synchronized a(IIIIIIILandroid/view/Surface;)V
    .locals 9

    .prologue
    .line 459
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 460
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    const-string v0, "CAR.CAM"

    const-string v1, "onFocusGained called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 506
    :goto_0
    monitor-exit p0

    return-void

    .line 470
    :cond_1
    :try_start_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->O:Z

    if-nez v0, :cond_2

    .line 471
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->O:Z

    .line 473
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    move v1, p1

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move/from16 v6, p7

    move-object/from16 v7, p8

    move v8, p6

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/car/on;->a(IIIIIILandroid/view/Surface;I)Z

    move-result v0

    .line 476
    if-nez v0, :cond_3

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    const/16 v1, 0xb

    const-string v2, "Starting composition failed"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 459
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 481
    :cond_3
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->C:Z

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_4
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 487
    if-eqz v0, :cond_4

    .line 488
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->i()Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->j()Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Intent;Landroid/content/Intent;)Z

    goto :goto_1

    .line 493
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 494
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/oi;)V

    goto :goto_2

    .line 498
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v2

    .line 499
    const/4 v0, 0x0

    move v1, v0

    :goto_3
    if-ge v1, v2, :cond_8

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 501
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 502
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/ae;->c(Landroid/content/Intent;)Z

    goto :goto_4

    .line 499
    :cond_7
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_3

    .line 505
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x3

    .line 564
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 565
    const-string v0, "CAR.CAM"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 566
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onVoiceAction "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; extras: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 567
    const-string v0, "CAR.CAM"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 568
    if-eqz v1, :cond_0

    .line 569
    invoke-virtual {v1}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 570
    const-string v3, "CAR.CAM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "EXTRA "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 576
    :cond_0
    const-string v0, "com.google.android.gms.car.category.CATEGORY_PROJECTION"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 579
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 581
    const-string v0, "CAR.CAM"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 582
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Looking for clients that match "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 584
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 585
    invoke-virtual {v0, p1, v7}, Landroid/content/pm/PackageManager;->queryBroadcastReceivers(Landroid/content/Intent;I)Ljava/util/List;

    move-result-object v1

    .line 586
    const-string v0, "CAR.CAM"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 587
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Found "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " matching projection clients"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    const-string v0, "CAR.CAM"

    invoke-static {v0, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 589
    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    .line 590
    const-string v3, "CAR.CAM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Found client for voice action in "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 597
    :cond_2
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_3

    .line 598
    invoke-interface {v1, v7}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ResolveInfo;

    iget-object v0, v0, Landroid/content/pm/ResolveInfo;->activityInfo:Landroid/content/pm/ActivityInfo;

    iget-object v0, v0, Landroid/content/pm/ActivityInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {v0, p1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 601
    :cond_3
    return-void
.end method

.method public final a(Landroid/content/res/Configuration;I)V
    .locals 3

    .prologue
    .line 922
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 923
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onConfigurationChanged, diff 0x"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 926
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 927
    and-int/lit16 v0, p2, 0x207

    .line 933
    if-eqz v0, :cond_1

    .line 934
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/ae;->b(Landroid/content/res/Configuration;I)V

    .line 936
    :cond_1
    return-void
.end method

.method public final declared-synchronized a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 696
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 697
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 703
    :goto_0
    monitor-exit p0

    return-void

    .line 702
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->a(Landroid/view/MotionEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 696
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/c/b/ar;I)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 725
    monitor-enter p0

    :try_start_0
    sget-object v2, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 726
    iget-boolean v2, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 809
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 731
    :cond_1
    :try_start_1
    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const/16 v3, 0x54

    if-eq v2, v3, :cond_2

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const v3, 0x10001

    if-eq v2, v3, :cond_2

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const/16 v3, 0xd1

    if-eq v2, v3, :cond_2

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const v3, 0x10002

    if-eq v2, v3, :cond_2

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const v3, 0x10004

    if-ne v2, v3, :cond_9

    .line 736
    :cond_2
    iget-boolean v0, p1, Lcom/google/android/c/b/ar;->b:Z

    if-nez v0, :cond_0

    .line 739
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->n()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 740
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 741
    const-string v0, "CAR.CAM"

    const-string v1, "Keycode to start app before accepting disclaimer, ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 725
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 745
    :cond_3
    :try_start_2
    iget v0, p1, Lcom/google/android/c/b/ar;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 774
    :cond_4
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->d()Z

    move-result v0

    if-nez v0, :cond_0

    .line 775
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->w:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->e()V

    goto :goto_0

    .line 747
    :sswitch_0
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 748
    const-string v0, "CAR.CAM"

    const-string v1, "received keycode search"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 750
    :cond_5
    invoke-static {p1, p2}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/c/b/ar;I)Landroid/view/KeyEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ae;->a(Landroid/view/KeyEvent;)Z

    goto :goto_1

    .line 754
    :sswitch_1
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 755
    const-string v0, "CAR.CAM"

    const-string v1, "Got keycode media/music."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 757
    :cond_6
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/gms/car/ae;->e:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    goto :goto_1

    .line 760
    :sswitch_2
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 761
    const-string v0, "CAR.CAM"

    const-string v1, "Got keycode for navigation."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 763
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->i:Landroid/content/ComponentName;

    if-eqz v0, :cond_4

    .line 764
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->i:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    goto :goto_1

    .line 768
    :sswitch_3
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 769
    const-string v0, "CAR.CAM"

    const-string v1, "Got keycode for tel."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 771
    :cond_8
    const/4 v0, 0x1

    sget-object v1, Lcom/google/android/gms/car/ae;->s:Landroid/content/ComponentName;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    goto :goto_1

    .line 785
    :cond_9
    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    const v3, 0xffff

    if-lt v2, v3, :cond_a

    .line 786
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 787
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dropping untranslated key: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 791
    :cond_a
    invoke-static {p1, p2}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/c/b/ar;I)Landroid/view/KeyEvent;

    move-result-object v3

    .line 792
    const-string v2, "CAR.CAM"

    const/4 v4, 0x3

    invoke-static {v2, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 793
    const-string v2, "CAR.CAM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got android key event "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 796
    :cond_b
    invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_1

    move v2, v1

    :goto_2
    if-eqz v2, :cond_c

    .line 797
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.MEDIA_BUTTON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 798
    const-string v1, "android.intent.extra.KEY_EVENT"

    invoke-virtual {v0, v1, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 799
    sget-object v1, Lcom/google/android/gms/car/ae;->e:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 800
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto/16 :goto_0

    :sswitch_4
    move v2, v0

    .line 796
    goto :goto_2

    .line 801
    :cond_c
    invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    packed-switch v2, :pswitch_data_0

    move v0, v1

    :pswitch_0
    if-eqz v0, :cond_e

    .line 802
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 803
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got phone key event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 805
    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->C()Lcom/google/android/gms/car/lb;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/dj;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/dj;->a(Landroid/view/KeyEvent;)V

    goto/16 :goto_0

    .line 807
    :cond_e
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/on;->a(Landroid/view/KeyEvent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 745
    :sswitch_data_0
    .sparse-switch
        0x54 -> :sswitch_0
        0xd1 -> :sswitch_1
        0x10001 -> :sswitch_1
        0x10002 -> :sswitch_2
        0x10004 -> :sswitch_3
    .end sparse-switch

    .line 796
    :sswitch_data_1
    .sparse-switch
        0x55 -> :sswitch_4
        0x56 -> :sswitch_4
        0x57 -> :sswitch_4
        0x58 -> :sswitch_4
        0x59 -> :sswitch_4
        0x5a -> :sswitch_4
        0x7e -> :sswitch_4
        0x7f -> :sswitch_4
        0x80 -> :sswitch_4
        0x81 -> :sswitch_4
        0x82 -> :sswitch_4
        0xde -> :sswitch_4
    .end sparse-switch

    .line 801
    :pswitch_data_0
    .packed-switch 0x5
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;Lcom/google/android/gms/car/y;)V
    .locals 3

    .prologue
    .line 1318
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1319
    const-string v0, "CAR.IME"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startInput/editorInfo.imeOptions="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p2, Landroid/view/inputmethod/EditorInfo;->imeOptions:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1323
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1324
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    if-nez v0, :cond_1

    .line 1327
    const-string v0, "CAR.IME"

    const-string v1, "No Input Service, can\'t start input."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1343
    :goto_0
    monitor-exit p0

    return-void

    .line 1330
    :cond_1
    :try_start_1
    invoke-virtual {p3}, Lcom/google/android/gms/car/y;->p()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1333
    const-string v0, "CAR.IME"

    const-string v1, "Can\'t start input on an unstarted activity"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1318
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1336
    :cond_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-eq v0, p3, :cond_3

    .line 1337
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->w()V

    .line 1339
    :cond_3
    iput-object p3, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    .line 1341
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->N:Z

    .line 1342
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    iget-boolean v1, p0, Lcom/google/android/gms/car/ae;->N:Z

    invoke-interface {v0, p1, p2, v1}, Lcom/google/android/gms/car/b/d;->a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;Z)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->N:Z

    new-instance v0, Lcom/google/android/gms/car/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/aj;-><init>(Lcom/google/android/gms/car/ae;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v1, "CAR.IME"

    const-string v2, "startInput() failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/ka;)V
    .locals 3

    .prologue
    .line 1379
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1380
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1382
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 1383
    invoke-interface {p1, v0}, Lcom/google/android/gms/car/ka;->b(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1385
    :catch_0
    move-exception v0

    .line 1386
    const-string v1, "CAR.CAM"

    const-string v2, "oNewActivityRequest failed"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1388
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->Y:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 1390
    :cond_1
    return-void
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/od;)V
    .locals 1

    .prologue
    .line 682
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 683
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 689
    :goto_0
    monitor-exit p0

    return-void

    .line 688
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/od;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 682
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/y;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 960
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 961
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityManagerConnected called "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 964
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 965
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v0, :cond_3

    .line 968
    const-string v0, "CAR.CAM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 969
    const-string v0, "CAR.CAM"

    const-string v1, "CarActivityManager not connected but tearing down"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 971
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->e()V

    .line 1003
    :cond_2
    :goto_0
    return-void

    .line 974
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 975
    if-eqz v0, :cond_2

    .line 980
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    .line 982
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v1}, Lcom/google/android/gms/car/on;->d()Z

    move-result v1

    if-nez v1, :cond_4

    .line 983
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    goto :goto_0

    .line 986
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    invoke-interface {v1, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    if-eq v1, p1, :cond_5

    .line 988
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Got duplicate activitymanager for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ignoring"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 989
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->r()V

    goto :goto_0

    .line 992
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-eq p1, v1, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/car/ae;->c:Landroid/content/ComponentName;

    invoke-virtual {v1, v2}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 994
    const-string v0, "CAR.CAM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 995
    const-string v0, "CAR.CAM"

    const-string v1, "Unknown CarActivityManager connected, ignoring."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 999
    :cond_6
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->p()Z

    move-result v1

    if-eqz v1, :cond_7

    .line 1000
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/oi;)Z

    .line 1002
    :cond_7
    invoke-virtual {p1, v3}, Lcom/google/android/gms/car/y;->a(I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/y;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/4 v1, 0x1

    .line 1216
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1217
    const-string v0, "CAR.CAM"

    const-string v1, "Crashed by a monkey: "

    invoke-static {v0, v1, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1218
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0, p2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 1220
    :cond_0
    const-string v0, "CAR.CAM"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1221
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onActivityManagerCrash called. Message: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " State: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->m()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/car/y;->b(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, p2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1223
    if-nez p2, :cond_1

    .line 1224
    const-string v0, "CAR.CAM"

    const-string v2, "No crash stack"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1226
    :cond_1
    const-string v0, "CAR.CAM"

    const-string v2, "onActivityManagerCrash called from: "

    new-instance v3, Ljava/lang/Exception;

    invoke-direct {v3}, Ljava/lang/Exception;-><init>()V

    invoke-static {v0, v2, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1229
    :cond_2
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1230
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->e()V

    .line 1233
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v0, :cond_4

    .line 1306
    :cond_3
    :goto_0
    return-void

    .line 1236
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1237
    if-eqz v0, :cond_5

    .line 1238
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/oi;)V

    .line 1241
    :cond_5
    monitor-enter p0

    .line 1242
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-ne v0, p1, :cond_6

    .line 1243
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->k()V

    .line 1245
    :cond_6
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1247
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v2

    .line 1248
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-eq v0, p1, :cond_8

    .line 1250
    :cond_7
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Crash from unknown component, ignoring."

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1245
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1254
    :cond_8
    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    sget-object v0, Lcom/google/android/gms/car/ae;->t:Ljava/util/Map;

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 1258
    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-ne p1, v3, :cond_c

    .line 1260
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    .line 1261
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    .line 1262
    sget-object v3, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    invoke-virtual {v2, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 1264
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->q()V

    .line 1265
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->h()I

    move-result v0

    const/4 v1, 0x2

    if-le v0, v1, :cond_a

    .line 1268
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/ae;->a(Ljava/lang/Throwable;)V

    goto :goto_0

    :cond_9
    move v0, v1

    .line 1254
    goto :goto_1

    .line 1270
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->R:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->V:Ljava/lang/Runnable;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->h()I

    move-result v2

    mul-int/lit8 v2, v2, 0x5

    mul-int/lit16 v2, v2, 0x3e8

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 1274
    :cond_b
    packed-switch v0, :pswitch_data_0

    .line 1291
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/y;)V

    goto/16 :goto_0

    .line 1278
    :pswitch_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->q()V

    .line 1279
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->i()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 1285
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/y;)V

    .line 1286
    sget-object v0, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    goto/16 :goto_0

    .line 1296
    :cond_c
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->p()Z

    move-result v1

    if-eqz v1, :cond_3

    if-eq v0, v4, :cond_3

    .line 1298
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/y;)V

    .line 1299
    sget-object v0, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 1300
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/ae;->a(Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 1302
    :cond_d
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->i()Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z

    goto/16 :goto_0

    .line 1274
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_2
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 2186
    const-string v0, "CarActivityManagerService "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2187
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 2188
    const-string v0, " State:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2189
    const-string v0, "mConnected="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 2190
    const-string v0, " mGearheadExists="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->C:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 2191
    const-string v0, " mInputService="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->J:Lcom/google/android/gms/car/b/d;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 2192
    const-string v0, " mLastImeClient="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/Object;)V

    .line 2193
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 2194
    const-string v0, "Active Clients: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2196
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 2197
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/y;->a(Ljava/io/PrintWriter;)V
    :try_end_0
    .catch Ljava/util/ConcurrentModificationException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 2200
    :catch_0
    move-exception v0

    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 2201
    const-string v0, "ConcurrentModificationException caught"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2203
    :cond_0
    invoke-virtual {p1}, Ljava/io/PrintWriter;->println()V

    .line 2204
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    .line 2205
    if-eqz v0, :cond_1

    .line 2206
    const-string v1, "**ProjectionWindowManager**"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2207
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->a(Ljava/io/PrintWriter;)V

    .line 2209
    :cond_1
    return-void
.end method

.method public final declared-synchronized a(I)Z
    .locals 4

    .prologue
    .line 828
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 829
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 830
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->l()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 831
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 835
    :goto_0
    monitor-exit p0

    return v0

    .line 834
    :cond_1
    :try_start_2
    monitor-exit v1

    .line 835
    const/4 v0, 0x0

    goto :goto_0

    .line 834
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 828
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Landroid/content/ComponentName;)Z
    .locals 3

    .prologue
    .line 663
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 664
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 667
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopCarActivityManager called for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 669
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->n()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 670
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/y;->a(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 671
    const/4 v0, 0x1

    .line 673
    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 663
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 609
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->d()Z

    move-result v0

    if-nez v0, :cond_1

    .line 610
    invoke-static {p1}, Lcom/google/android/gms/car/ae;->e(Landroid/content/Intent;)I

    move-result v1

    .line 611
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 612
    if-nez v0, :cond_0

    .line 613
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 614
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->z:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 617
    :cond_0
    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 619
    :cond_1
    monitor-exit p0

    return-void

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 711
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 712
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 718
    :goto_0
    monitor-exit p0

    return-void

    .line 717
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->b(Landroid/view/MotionEvent;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 711
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/ka;)V
    .locals 1

    .prologue
    .line 1393
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->P:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 1394
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/y;)V
    .locals 2

    .prologue
    .line 1016
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1017
    const-string v0, "CAR.CAM"

    const-string v1, "onActivityManagerStarted called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1020
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1021
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v0, :cond_2

    .line 1024
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->e()V

    .line 1032
    :cond_1
    :goto_0
    return-void

    .line 1028
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->d(Lcom/google/android/gms/car/oi;)V

    .line 1029
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->T()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1030
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->t()V

    goto :goto_0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 559
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->C:Z

    return v0
.end method

.method public final declared-synchronized b(I)Z
    .locals 4

    .prologue
    .line 840
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 841
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 842
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->l()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/car/ae;->I:Landroid/content/ComponentName;

    invoke-virtual {v0, v3}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 844
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 848
    :goto_0
    monitor-exit p0

    return v0

    .line 847
    :cond_1
    :try_start_2
    monitor-exit v1

    .line 848
    const/4 v0, 0x0

    goto :goto_0

    .line 847
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 840
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final c()Landroid/content/Context;
    .locals 1

    .prologue
    .line 823
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    return-object v0
.end method

.method public final declared-synchronized c(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 623
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1, p1}, Lcom/google/android/gms/car/ae;->a(Landroid/content/Intent;Landroid/content/Intent;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d()V
    .locals 3

    .prologue
    .line 855
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 856
    const-string v0, "CAR.CAM"

    const-string v1, "tearDownSynchronized()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 858
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 859
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    .line 862
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->S:Z

    if-eqz v0, :cond_1

    .line 863
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->u:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->T:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 864
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->S:Z

    .line 867
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    monitor-enter v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 868
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 869
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->r()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 872
    :catchall_0
    move-exception v0

    :try_start_2
    monitor-exit v1

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 855
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 871
    :cond_2
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 872
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 873
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->R:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->V:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 874
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    if-eqz v0, :cond_3

    .line 879
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ja;->a(I)V

    .line 882
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->m()V

    .line 883
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->b()V

    .line 884
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->B:Lcom/google/android/gms/car/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ap;->a()V

    .line 885
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->X:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 886
    monitor-exit p0

    return-void
.end method

.method public final d(Lcom/google/android/gms/car/y;)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x3

    .line 1067
    const-string v0, "CAR.CAM"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1068
    const-string v0, "CAR.CAM"

    const-string v1, "onActivityManagerFocusGained called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1071
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1072
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v0, :cond_3

    .line 1075
    const-string v0, "CAR.CAM"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1076
    const-string v0, "CAR.CAM"

    const-string v1, "CarActivityManager onFocusGained but we\'re tearing down"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1078
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->e()V

    .line 1135
    :cond_2
    :goto_0
    return-void

    .line 1081
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v1

    .line 1082
    if-eqz v1, :cond_2

    .line 1085
    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v2

    .line 1086
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 1087
    const-string v3, "CAR.CAM"

    invoke-static {v3, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 1088
    const-string v3, "CAR.CAM"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Last CarActivityManager is: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " current CarActivityManager is: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1091
    :cond_4
    if-eqz v0, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v3

    if-eqz v3, :cond_6

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/car/oi;->a()I

    move-result v3

    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v1

    if-ne v3, v1, :cond_6

    if-eq v0, p1, :cond_6

    .line 1094
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "focus gained in inconsitent CarActivityManager, stopping "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1096
    invoke-virtual {p1, v7}, Lcom/google/android/gms/car/y;->a(I)V

    .line 1097
    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->p()Z

    move-result v1

    if-nez v1, :cond_2

    .line 1098
    const-string v1, "CAR.CAM"

    invoke-static {v1, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 1099
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " resuming last CarActivityManager "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1102
    :cond_5
    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/y;->a(I)V

    goto/16 :goto_0

    .line 1106
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v0, v2, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1107
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-ne v0, p1, :cond_7

    .line 1108
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    .line 1109
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    .line 1112
    :cond_7
    instance-of v0, p1, Lcom/google/android/gms/car/ja;

    if-nez v0, :cond_9

    .line 1113
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1115
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->P:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ka;

    .line 1117
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->i()Landroid/content/Intent;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/ka;->a(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 1118
    :catch_0
    move-exception v1

    .line 1119
    const-string v4, "CAR.CAM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error calling onActivityStarted with intent: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->i()Landroid/content/Intent;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1122
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1126
    :cond_8
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/ka;

    .line 1127
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->P:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1130
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->e(Lcom/google/android/gms/car/oi;)V

    .line 1132
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1133
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->W:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method public final declared-synchronized d(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 652
    monitor-enter p0

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 653
    iget-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 656
    const/4 v0, 0x0

    .line 658
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x3

    :try_start_1
    invoke-direct {p0, v0, p1, p1}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/Intent;Landroid/content/Intent;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    goto :goto_0

    .line 652
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 892
    const-string v0, "CAR.CAM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 893
    const-string v0, "CAR.CAM"

    const-string v1, "tearDownOnCrash()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 895
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 896
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ae;->H:Z

    .line 899
    iget-object v1, p0, Lcom/google/android/gms/car/ae;->y:Ljava/util/Map;

    .line 900
    if-eqz v1, :cond_2

    .line 901
    invoke-interface {v1}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 902
    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/y;->a(I)V

    .line 903
    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->r()V

    goto :goto_0

    .line 905
    :cond_1
    invoke-interface {v1}, Ljava/util/Map;->clear()V

    .line 907
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    if-eqz v0, :cond_3

    .line 908
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->D:Lcom/google/android/gms/car/ja;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/ja;->a(I)V

    .line 910
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->m()V

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    .line 912
    if-eqz v0, :cond_4

    .line 913
    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->c()V

    .line 915
    :cond_4
    return-void
.end method

.method public final e(Lcom/google/android/gms/car/y;)V
    .locals 2

    .prologue
    .line 1142
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1143
    const-string v0, "CAR.CAM"

    const-string v1, "onStopComplete called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1146
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1147
    monitor-enter p0

    .line 1150
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-ne v0, p1, :cond_1

    .line 1151
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->k()V

    .line 1153
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1155
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-ne v0, p1, :cond_2

    .line 1156
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    .line 1157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    .line 1159
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/oi;)V

    .line 1160
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 1161
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    invoke-static {v0}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1162
    return-void

    .line 1153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final f()Lcom/google/android/gms/car/on;
    .locals 1

    .prologue
    .line 918
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/car/y;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 1172
    const-string v2, "CAR.CAM"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1173
    const-string v2, "CAR.CAM"

    const-string v3, "finish called"

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1176
    :cond_0
    sget-object v2, Lcom/google/android/gms/car/ae;->j:Lcom/google/android/gms/car/ii;

    .line 1177
    iget-boolean v2, p0, Lcom/google/android/gms/car/ae;->H:Z

    if-nez v2, :cond_2

    .line 1180
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->e()V

    .line 1209
    :cond_1
    :goto_0
    return-void

    .line 1184
    :cond_2
    monitor-enter p0

    .line 1185
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-ne v2, p1, :cond_3

    .line 1186
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->k()V

    .line 1188
    :cond_3
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1190
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    if-ne v2, p1, :cond_4

    .line 1191
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->j()V

    .line 1192
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/car/ae;->E:Lcom/google/android/gms/car/y;

    .line 1195
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v3

    .line 1196
    if-eqz v3, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->A:Landroid/util/SparseArray;

    invoke-virtual {v3}, Lcom/google/android/gms/car/oi;->a()I

    move-result v4

    invoke-virtual {v2, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_7

    move v2, v1

    .line 1198
    :goto_1
    if-eqz v2, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/car/oi;->a()I

    move-result v3

    if-ne v3, v1, :cond_5

    move v0, v1

    .line 1200
    :cond_5
    iget-object v3, p0, Lcom/google/android/gms/car/ae;->x:Lcom/google/android/gms/car/on;

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/oi;)V

    .line 1201
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/y;)V

    .line 1203
    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v2

    if-nez v2, :cond_6

    .line 1204
    iget-object v2, p0, Lcom/google/android/gms/car/ae;->G:Ljava/util/concurrent/Semaphore;

    invoke-static {v2}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;)V

    .line 1206
    :cond_6
    if-eqz v0, :cond_1

    .line 1207
    sget-object v0, Lcom/google/android/gms/car/ae;->g:Landroid/content/ComponentName;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/ae;->a(ILandroid/content/ComponentName;)Z

    goto :goto_0

    .line 1188
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_7
    move v2, v0

    .line 1196
    goto :goto_1
.end method

.method final g()Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 1457
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->v:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method public final declared-synchronized g(Lcom/google/android/gms/car/y;)V
    .locals 2

    .prologue
    .line 1366
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-eqz v0, :cond_1

    .line 1368
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->K:Lcom/google/android/gms/car/y;

    if-eq p1, v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/ae;->L:Landroid/content/ComponentName;

    invoke-virtual {v0, v1}, Landroid/content/ComponentName;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1371
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/ae;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1376
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 1373
    :cond_2
    :try_start_1
    const-string v0, "CAR.IME"

    const-string v1, "Can\'t stop input, this client didn\'t request input in the first place."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1366
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final h()V
    .locals 1

    .prologue
    .line 1464
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->w:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->f()V

    .line 1465
    return-void
.end method

.method final i()V
    .locals 1

    .prologue
    .line 1471
    iget-object v0, p0, Lcom/google/android/gms/car/ae;->w:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->e()V

    .line 1472
    return-void
.end method
