.class public Lcom/google/android/gms/common/widget/a/q;
.super Lcom/google/android/gms/common/widget/a/g;
.source "SourceFile"


# instance fields
.field final l:Landroid/view/View;

.field final m:Landroid/view/View;

.field final n:Landroid/widget/ImageView;

.field final o:Landroid/widget/TextView;

.field final p:Landroid/widget/TextView;

.field final q:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 253
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/g;-><init>(Landroid/view/View;)V

    .line 254
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/q;->l:Landroid/view/View;

    .line 255
    sget v0, Lcom/google/android/gms/common/widget/d;->a:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->m:Landroid/view/View;

    .line 256
    const v0, 0x1020006

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->n:Landroid/widget/ImageView;

    .line 257
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->o:Landroid/widget/TextView;

    .line 258
    const v0, 0x1020010

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->p:Landroid/widget/TextView;

    .line 259
    const v0, 0x1020018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->q:Landroid/view/ViewGroup;

    .line 260
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 261
    return-void
.end method

.method private static a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    .line 264
    if-nez p0, :cond_0

    .line 274
    :goto_0
    return-void

    .line 268
    :cond_0
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 269
    invoke-virtual {p0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 270
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    .line 272
    :cond_1
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 4

    .prologue
    .line 298
    instance-of v0, p1, Lcom/google/android/gms/common/widget/a/o;

    if-nez v0, :cond_0

    .line 299
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "settingItem must be SimpleSettingItem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 301
    :cond_0
    check-cast p1, Lcom/google/android/gms/common/widget/a/o;

    .line 302
    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->k()Z

    move-result v1

    .line 304
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->n:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setEnabled(Z)V

    .line 305
    iget-object v2, p0, Lcom/google/android/gms/common/widget/a/q;->m:Landroid/view/View;

    iget-object v3, p0, Lcom/google/android/gms/common/widget/a/q;->n:Landroid/widget/ImageView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->i()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    if-eqz v3, :cond_1

    if-eqz v0, :cond_2

    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v3, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    if-eqz v2, :cond_1

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 307
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->o:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 308
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->o:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->a()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/widget/a/q;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->p:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->p:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->h()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/widget/a/q;->a(Landroid/widget/TextView;Ljava/lang/CharSequence;)V

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->l:Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/view/View;->setEnabled(Z)V

    .line 314
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/q;->l:Landroid/view/View;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/o;->j()Z

    move-result v1

    if-eqz v1, :cond_3

    :goto_1
    invoke-virtual {v0, p1}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 315
    return-void

    .line 305
    :cond_2
    const/16 v0, 0x8

    goto :goto_0

    .line 314
    :cond_3
    const/4 p1, 0x0

    goto :goto_1
.end method
