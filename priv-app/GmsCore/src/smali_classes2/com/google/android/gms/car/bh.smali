.class final Lcom/google/android/gms/car/bh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/i;


# instance fields
.field final a:I

.field final b:Ljava/lang/Object;

.field c:Lcom/google/android/gms/car/bo;

.field final synthetic d:Lcom/google/android/gms/car/bg;

.field private final e:Lcom/google/android/gms/car/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/bg;ILcom/google/android/gms/car/g;)V
    .locals 1

    .prologue
    .line 850
    iput-object p1, p0, Lcom/google/android/gms/car/bh;->d:Lcom/google/android/gms/car/bg;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 846
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    .line 847
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    .line 851
    iput p2, p0, Lcom/google/android/gms/car/bh;->a:I

    .line 852
    iput-object p3, p0, Lcom/google/android/gms/car/bh;->e:Lcom/google/android/gms/car/g;

    .line 853
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/gms/car/bh;->e:Lcom/google/android/gms/car/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/g;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleFocusLoss client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bh;->e:Lcom/google/android/gms/car/g;

    invoke-virtual {v0}, Lcom/google/android/gms/car/g;->d()V

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/bh;->a(I)V

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 895
    iget-object v1, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 897
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    .line 898
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    .line 899
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 900
    if-eqz v0, :cond_0

    .line 901
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/bo;->a(I)V

    .line 903
    :cond_0
    return-void

    .line 899
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(Lcom/google/android/gms/car/bo;)V
    .locals 2

    .prologue
    .line 861
    iget-object v1, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 862
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    .line 863
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a([BII)Z
    .locals 2

    .prologue
    .line 883
    iget-object v1, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 885
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    .line 886
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 887
    if-eqz v0, :cond_0

    .line 888
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2, p3}, Lcom/google/android/gms/car/bo;->a([BIII)Z

    move-result v0

    return v0

    .line 886
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 890
    :cond_0
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no active track"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method final b()Lcom/google/android/gms/car/bo;
    .locals 2

    .prologue
    .line 867
    iget-object v1, p0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    .line 868
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 869
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
