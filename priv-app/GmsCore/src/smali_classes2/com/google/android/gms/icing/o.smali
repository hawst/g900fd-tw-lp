.class public final Lcom/google/android/gms/icing/o;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:Ljava/lang/String;

.field public c:I

.field public d:[Lcom/google/android/gms/icing/q;

.field public e:[Lcom/google/android/gms/icing/p;

.field public f:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 4167
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4168
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/o;->a:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/o;->c:I

    invoke-static {}, Lcom/google/android/gms/icing/q;->a()[Lcom/google/android/gms/icing/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    invoke-static {}, Lcom/google/android/gms/icing/p;->a()[Lcom/google/android/gms/icing/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/o;->f:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/o;->cachedSize:I

    .line 4169
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/o;
    .locals 1

    .prologue
    .line 4385
    new-instance v0, Lcom/google/android/gms/icing/o;

    invoke-direct {v0}, Lcom/google/android/gms/icing/o;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/o;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 4270
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4271
    iget v2, p0, Lcom/google/android/gms/icing/o;->a:I

    if-eqz v2, :cond_0

    .line 4272
    iget v2, p0, Lcom/google/android/gms/icing/o;->a:I

    invoke-static {v4, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4275
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 4276
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4279
    :cond_1
    iget v2, p0, Lcom/google/android/gms/icing/o;->c:I

    if-eq v2, v4, :cond_2

    .line 4280
    const/4 v2, 0x3

    iget v3, p0, Lcom/google/android/gms/icing/o;->c:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 4283
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v2, v2

    if-lez v2, :cond_5

    move v2, v0

    move v0, v1

    .line 4284
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v3, v3

    if-ge v0, v3, :cond_4

    .line 4285
    iget-object v3, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    aget-object v3, v3, v0

    .line 4286
    if-eqz v3, :cond_3

    .line 4287
    const/4 v4, 0x4

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 4284
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_4
    move v0, v2

    .line 4292
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    array-length v2, v2

    if-lez v2, :cond_7

    .line 4293
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    array-length v2, v2

    if-ge v1, v2, :cond_7

    .line 4294
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    aget-object v2, v2, v1

    .line 4295
    if-eqz v2, :cond_6

    .line 4296
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 4293
    :cond_6
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4301
    :cond_7
    iget-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_8

    .line 4302
    const/4 v1, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 4305
    :cond_8
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4184
    if-ne p1, p0, :cond_1

    .line 4215
    :cond_0
    :goto_0
    return v0

    .line 4187
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/o;

    if-nez v2, :cond_2

    move v0, v1

    .line 4188
    goto :goto_0

    .line 4190
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/o;

    .line 4191
    iget v2, p0, Lcom/google/android/gms/icing/o;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/o;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 4192
    goto :goto_0

    .line 4194
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    if-nez v2, :cond_4

    .line 4195
    iget-object v2, p1, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    if-eqz v2, :cond_5

    move v0, v1

    .line 4196
    goto :goto_0

    .line 4198
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 4199
    goto :goto_0

    .line 4201
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/o;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/o;->c:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 4202
    goto :goto_0

    .line 4204
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    iget-object v3, p1, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 4206
    goto :goto_0

    .line 4208
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    iget-object v3, p1, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 4210
    goto :goto_0

    .line 4212
    :cond_8
    iget-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/o;->f:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 4213
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 6

    .prologue
    .line 4220
    iget v0, p0, Lcom/google/android/gms/icing/o;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 4222
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 4224
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/o;->c:I

    add-int/2addr v0, v1

    .line 4225
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4227
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4229
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/o;->f:J

    const/16 v1, 0x20

    ushr-long/2addr v4, v1

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 4231
    return v0

    .line 4222
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 3772
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/o;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/o;->c:I

    goto :goto_0

    :sswitch_4
    const/16 v0, 0x22

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/q;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/q;

    invoke-direct {v3}, Lcom/google/android/gms/icing/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/q;

    invoke-direct {v3}, Lcom/google/android/gms/icing/q;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/p;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/p;

    invoke-direct {v3}, Lcom/google/android/gms/icing/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/p;

    invoke-direct {v3}, Lcom/google/android/gms/icing/p;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    goto/16 :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v1, 0x0

    .line 4237
    iget v0, p0, Lcom/google/android/gms/icing/o;->a:I

    if-eqz v0, :cond_0

    .line 4238
    iget v0, p0, Lcom/google/android/gms/icing/o;->a:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 4240
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 4241
    const/4 v0, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4243
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/o;->c:I

    if-eq v0, v3, :cond_2

    .line 4244
    const/4 v0, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/o;->c:I

    invoke-virtual {p1, v0, v2}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 4246
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v0, v0

    if-lez v0, :cond_4

    move v0, v1

    .line 4247
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v2, v2

    if-ge v0, v2, :cond_4

    .line 4248
    iget-object v2, p0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    aget-object v2, v2, v0

    .line 4249
    if-eqz v2, :cond_3

    .line 4250
    const/4 v3, 0x4

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4247
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 4254
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    array-length v0, v0

    if-lez v0, :cond_6

    .line 4255
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    array-length v0, v0

    if-ge v1, v0, :cond_6

    .line 4256
    iget-object v0, p0, Lcom/google/android/gms/icing/o;->e:[Lcom/google/android/gms/icing/p;

    aget-object v0, v0, v1

    .line 4257
    if-eqz v0, :cond_5

    .line 4258
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 4255
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 4262
    :cond_6
    iget-wide v0, p0, Lcom/google/android/gms/icing/o;->f:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_7

    .line 4263
    const/4 v0, 0x6

    iget-wide v2, p0, Lcom/google/android/gms/icing/o;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 4265
    :cond_7
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4266
    return-void
.end method
