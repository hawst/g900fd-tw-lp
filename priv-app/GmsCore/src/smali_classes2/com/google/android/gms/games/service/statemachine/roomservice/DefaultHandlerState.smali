.class final Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 50
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 6

    .prologue
    .line 55
    :try_start_0
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 159
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->g:Z

    .line 163
    :goto_0
    return v0

    .line 57
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;

    .line 58
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/d;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;I)V

    .line 59
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 62
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;

    .line 64
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v2, 0x0

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/c;->a:I

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/eg;->a(Landroid/os/ParcelFileDescriptor;I)V

    .line 65
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 68
    :pswitch_2
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Unexpected: LEAVE_ROOM"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h()V

    .line 70
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 76
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;

    .line 77
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/i;->a:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;Z)V

    .line 78
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 81
    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 82
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 85
    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 86
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 89
    :pswitch_6
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Unexpected: PEER_JOINED"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 93
    :pswitch_7
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 96
    :pswitch_8
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 99
    :pswitch_9
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 102
    :pswitch_a
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 105
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;

    .line 106
    const-string v1, "RoomServiceStateMachine"

    const-string v2, "Failed to send message %s. Message can be sent only inside a room"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 109
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/16 v2, 0x1b5c

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->a:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/q;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/eg;->a(IILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 115
    :goto_1
    :try_start_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 161
    :catch_1
    move-exception v0

    .line 162
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    .line 163
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->g:Z

    goto/16 :goto_0

    .line 118
    :pswitch_c
    :try_start_3
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Can\'t sent a message while not in room"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 120
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 123
    :pswitch_d
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Unexpected: DONE_LEAVING_ROOM"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 127
    :pswitch_e
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Unexpected: CONNECT_NETWORK"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 132
    :pswitch_f
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: NETWORK_DISCONNECTED"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0

    .line 137
    :pswitch_10
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: DCM_CONNECT"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0

    .line 142
    :pswitch_11
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: DCM_CONNECT_FAILED"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0

    .line 147
    :pswitch_12
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: DCM_CONNECT_OK"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0

    .line 151
    :pswitch_13
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: DCM_DISCONNECT"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0

    .line 155
    :pswitch_14
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/x;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1}, Lcom/google/android/gms/games/service/statemachine/d;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v1

    const-string v2, "Unexpected: DISCONNECT_NETWORK"

    invoke-direct {v0, v1, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/x;-><init>(Lcom/google/android/gms/common/util/a/a;Landroid/os/Message;Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_1

    .line 55
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_f
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_5
        :pswitch_e
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_d
    .end packed-switch
.end method
