.class public final Lcom/google/android/gms/games/service/a/h/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field private final a:Lcom/google/android/gms/games/a/au;

.field private final b:Lcom/google/android/gms/games/internal/dr;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;)V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/h/c;->a:Lcom/google/android/gms/games/a/au;

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/h/c;->b:Lcom/google/android/gms/games/internal/dr;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 43
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 3

    .prologue
    .line 33
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 34
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/h/c;->a:Lcom/google/android/gms/games/a/au;

    invoke-virtual {p2, v1, v0}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Landroid/os/Bundle;)I

    move-result v1

    .line 36
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/h/c;->b:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v2, v1, v0}, Lcom/google/android/gms/games/internal/dr;->e(ILandroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 40
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    .line 38
    const-string v1, "InboxCountsOp"

    const-string v2, "Failed to provide callback"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
