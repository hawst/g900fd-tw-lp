.class public final Lcom/google/android/gms/car/diagnostics/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/Executor;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    invoke-static {}, Ljava/util/concurrent/Executors;->newSingleThreadExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/diagnostics/b;->a:Ljava/util/concurrent/Executor;

    return-void
.end method

.method static synthetic a()V
    .locals 0

    .prologue
    .line 22
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 3

    .prologue
    .line 89
    const-string v0, "CAR.FEEDBACK"

    const-string v1, "Recording User Feedback"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    invoke-static {p0}, Lcom/google/android/gms/car/diagnostics/l;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 91
    const-string v0, "CAR.FEEDBACK"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Feedback disabled. Feedback Message:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :goto_0
    return-void

    .line 94
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/diagnostics/b;->a:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/gms/car/diagnostics/c;

    const/4 v2, 0x0

    invoke-direct {v1, p0, p1, v2, p2}, Lcom/google/android/gms/car/diagnostics/c;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;Landroid/graphics/Bitmap;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)Z
    .locals 1
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 103
    invoke-static {p0}, Lcom/google/android/gms/car/diagnostics/l;->a(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method
