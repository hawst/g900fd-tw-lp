.class public final Lcom/google/android/gms/maps/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/maps/internal/k;

.field private b:Lcom/google/android/gms/maps/u;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/maps/internal/k;)V
    .locals 1

    .prologue
    .line 450
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 451
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/maps/internal/k;

    iput-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    .line 452
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/maps/model/CameraPosition;
    .locals 2

    .prologue
    .line 468
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/k;->a()Lcom/google/android/gms/maps/model/CameraPosition;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 469
    :catch_0
    move-exception v0

    .line 470
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/f;
    .locals 2

    .prologue
    .line 686
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/model/GroundOverlayOptions;)Lcom/google/android/gms/maps/model/internal/j;

    move-result-object v1

    .line 687
    if-eqz v1, :cond_0

    .line 688
    new-instance v0, Lcom/google/android/gms/maps/model/f;

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/model/f;-><init>(Lcom/google/android/gms/maps/model/internal/j;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 690
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 692
    :catch_0
    move-exception v0

    .line 693
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/j;
    .locals 2

    .prologue
    .line 660
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v0, p1}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/model/MarkerOptions;)Lcom/google/android/gms/maps/model/internal/s;

    move-result-object v1

    .line 661
    if-eqz v1, :cond_0

    .line 662
    new-instance v0, Lcom/google/android/gms/maps/model/j;

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/model/j;-><init>(Lcom/google/android/gms/maps/model/internal/s;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 664
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 666
    :catch_0
    move-exception v0

    .line 667
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(II)V
    .locals 3

    .prologue
    .line 1397
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-interface {v0, v1, p1, v2, p2}, Lcom/google/android/gms/maps/internal/k;->a(IIII)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1400
    return-void

    .line 1398
    :catch_0
    move-exception v0

    .line 1399
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/a;)V
    .locals 2

    .prologue
    .line 512
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    iget-object v1, p1, Lcom/google/android/gms/maps/a;->a:Lcom/google/android/gms/b/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/b/l;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 515
    return-void

    .line 513
    :catch_0
    move-exception v0

    .line 514
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final a(Lcom/google/android/gms/maps/k;)V
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1239
    if-nez p1, :cond_0

    .line 1240
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/bk;)V

    .line 1257
    :goto_0
    return-void

    .line 1242
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    new-instance v1, Lcom/google/android/gms/maps/d;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/maps/d;-><init>(Lcom/google/android/gms/maps/c;Lcom/google/android/gms/maps/k;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/k;->a(Lcom/google/android/gms/maps/internal/bk;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 1255
    :catch_0
    move-exception v0

    .line 1256
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b(Lcom/google/android/gms/maps/a;)V
    .locals 2

    .prologue
    .line 529
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    iget-object v1, p1, Lcom/google/android/gms/maps/a;->a:Lcom/google/android/gms/b/l;

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/k;->b(Lcom/google/android/gms/b/l;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 532
    return-void

    .line 530
    :catch_0
    move-exception v0

    .line 531
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 874
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/gms/maps/internal/k;->b(Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    return v0

    .line 875
    :catch_0
    move-exception v0

    .line 876
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final c()Landroid/location/Location;
    .locals 2
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 954
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v0}, Lcom/google/android/gms/maps/internal/k;->j()Landroid/location/Location;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 955
    :catch_0
    move-exception v0

    .line 956
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final d()Lcom/google/android/gms/maps/u;
    .locals 2

    .prologue
    .line 1005
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->b:Lcom/google/android/gms/maps/u;

    if-nez v0, :cond_0

    .line 1006
    new-instance v0, Lcom/google/android/gms/maps/u;

    iget-object v1, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/k;->k()Lcom/google/android/gms/maps/internal/co;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/u;-><init>(Lcom/google/android/gms/maps/internal/co;)V

    iput-object v0, p0, Lcom/google/android/gms/maps/c;->b:Lcom/google/android/gms/maps/u;

    .line 1008
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/maps/c;->b:Lcom/google/android/gms/maps/u;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1009
    :catch_0
    move-exception v0

    .line 1010
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method

.method public final e()Lcom/google/android/gms/maps/o;
    .locals 2

    .prologue
    .line 1027
    :try_start_0
    new-instance v0, Lcom/google/android/gms/maps/o;

    iget-object v1, p0, Lcom/google/android/gms/maps/c;->a:Lcom/google/android/gms/maps/internal/k;

    invoke-interface {v1}, Lcom/google/android/gms/maps/internal/k;->l()Lcom/google/android/gms/maps/internal/bz;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/maps/o;-><init>(Lcom/google/android/gms/maps/internal/bz;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 1028
    :catch_0
    move-exception v0

    .line 1029
    new-instance v1, Lcom/google/android/gms/maps/model/n;

    invoke-direct {v1, v0}, Lcom/google/android/gms/maps/model/n;-><init>(Landroid/os/RemoteException;)V

    throw v1
.end method
