.class final Lcom/google/android/gms/drive/internal/au;
.super Lcom/google/android/gms/drive/internal/ai;
.source "SourceFile"


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/google/android/gms/drive/DriveId;

.field final synthetic d:Lcom/google/android/gms/drive/internal/at;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/at;Lcom/google/android/gms/common/api/v;JLcom/google/android/gms/drive/DriveId;)V
    .locals 1

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/au;->d:Lcom/google/android/gms/drive/internal/at;

    iput-wide p3, p0, Lcom/google/android/gms/drive/internal/au;->a:J

    iput-object p5, p0, Lcom/google/android/gms/drive/internal/au;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/ai;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 5

    .prologue
    .line 31
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;

    iget-wide v2, p0, Lcom/google/android/gms/drive/internal/au;->a:J

    iget-object v4, p0, Lcom/google/android/gms/drive/internal/au;->b:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;-><init>(JLcom/google/android/gms/drive/DriveId;)V

    new-instance v2, Lcom/google/android/gms/drive/internal/dh;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/dh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/AuthorizeAccessRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
