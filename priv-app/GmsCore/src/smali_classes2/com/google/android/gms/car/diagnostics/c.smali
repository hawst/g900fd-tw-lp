.class final Lcom/google/android/gms/car/diagnostics/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field private final a:Lcom/google/android/gms/car/diagnostics/a;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/graphics/Bitmap;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {}, Lcom/google/android/gms/car/diagnostics/b;->a()V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/car/diagnostics/c;->b:Landroid/content/Context;

    .line 39
    new-instance v0, Lcom/google/android/gms/car/diagnostics/a;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/diagnostics/a;-><init>(Ljava/util/Date;)V

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    iput-object p2, v0, Lcom/google/android/gms/car/diagnostics/a;->a:Ljava/lang/String;

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/a;->b:Ljava/lang/Throwable;

    .line 42
    iput-object p4, p0, Lcom/google/android/gms/car/diagnostics/c;->c:Landroid/graphics/Bitmap;

    .line 43
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->c:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/c;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/car/diagnostics/c;->c:Landroid/graphics/Bitmap;

    invoke-static {v1, v2}, Lcom/google/android/gms/car/diagnostics/a;->a(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/a;->c:Ljava/lang/String;

    .line 68
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/c;->b:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/diagnostics/a;->a(Landroid/content/Context;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/c;->a:Lcom/google/android/gms/car/diagnostics/a;

    iget-object v0, v0, Lcom/google/android/gms/car/diagnostics/a;->f:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/google/android/gms/car/diagnostics/b;->a()V

    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.car.userfeedback"

    const-string v3, "com.google.android.gms.car.diagnostics.FeedbackMarshallerService"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    new-instance v2, Ljava/io/File;

    invoke-direct {v2, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-static {v2}, Landroid/net/Uri;->fromFile(Ljava/io/File;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.car.diagnostics.FeedbackMarshallerService.MARSHAL_FEEDBACK"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/c;->b:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 74
    :goto_0
    return-void

    .line 71
    :catch_0
    move-exception v0

    .line 72
    const-string v1, "CAR.FEEDBACK"

    const-string v2, "Couldn\'t write FeedbackBundle"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method
