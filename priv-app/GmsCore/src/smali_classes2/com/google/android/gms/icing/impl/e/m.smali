.class public final Lcom/google/android/gms/icing/impl/e/m;
.super Lcom/google/android/gms/common/internal/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/Iterator;

.field private final c:Landroid/os/ParcelFileDescriptor;

.field private final d:Lcom/google/android/gms/icing/impl/e/k;


# direct methods
.method public constructor <init>(Ljava/util/Iterator;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/icing/impl/e/k;)V
    .locals 0

    .prologue
    .line 318
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/a;-><init>()V

    .line 319
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/e/m;->b:Ljava/util/Iterator;

    .line 320
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/e/m;->c:Landroid/os/ParcelFileDescriptor;

    .line 321
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    .line 322
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 311
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/m;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/m;->b:Ljava/util/Iterator;

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/bf;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    iget-object v3, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    iget-wide v8, v0, Lcom/google/android/gms/icing/bf;->b:J

    invoke-virtual {v1, v3, v8, v9}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;J)Z

    move-result v7

    if-nez v7, :cond_1

    const-string v1, "Invalid usage report: reporting package installed after report -- %s"

    invoke-static {v1, v3}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    move v1, v4

    :goto_1
    if-eqz v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_d

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v6, v0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-direct {v1, v6, v7}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v6, v3, Lcom/google/android/gms/icing/impl/e/k;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v6, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    iget-object v1, v3, Lcom/google/android/gms/icing/impl/e/k;->b:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    invoke-virtual {v3, v7}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v1

    if-nez v1, :cond_8

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v1

    :goto_2
    iget-object v8, v3, Lcom/google/android/gms/icing/impl/e/k;->b:Ljava/util/Map;

    invoke-interface {v8, v7, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :goto_3
    invoke-interface {v1, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_a

    const-string v1, "Invalid usage report: no access"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    move-object v3, v2

    :goto_4
    if-nez v3, :cond_c

    const-string v1, "UsageReport from %s ignored -- corpus not found"

    iget-object v0, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    :cond_1
    iget-wide v8, v0, Lcom/google/android/gms/icing/bf;->b:J

    invoke-virtual {v1, v6, v8, v9}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;J)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v1, "Invalid usage report: doc package installed after report -- %s"

    invoke-static {v1, v6}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    move v1, v4

    goto :goto_1

    :cond_2
    iget v7, v0, Lcom/google/android/gms/icing/bf;->g:I

    if-ne v7, v10, :cond_4

    invoke-virtual {v1, v3}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v1

    if-eqz v1, :cond_3

    iget-boolean v1, v1, Lcom/google/android/gms/icing/impl/a/h;->c:Z

    if-nez v1, :cond_4

    :cond_3
    move v1, v4

    :goto_5
    if-nez v1, :cond_5

    const-string v1, "Illegal usage type: %d from %s"

    iget v6, v0, Lcom/google/android/gms/icing/bf;->g:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v1, v6, v3}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move v1, v4

    goto :goto_1

    :cond_4
    move v1, v5

    goto :goto_5

    :cond_5
    iget-object v1, v0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    const-string v1, "Invalid usage report: pkg name mismatch -- caller %s vs %s"

    invoke-static {v1, v3, v6}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    move v1, v4

    goto/16 :goto_1

    :cond_6
    iget-object v1, v0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-nez v1, :cond_7

    const-string v1, "Invalid usage report: no corpus name and no document -- %s"

    invoke-static {v1, v3}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    move v1, v4

    goto/16 :goto_1

    :cond_7
    move v1, v5

    goto/16 :goto_1

    :cond_8
    iget-object v8, v3, Lcom/google/android/gms/icing/impl/e/k;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v8, v1, v2, v5}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v1

    goto :goto_2

    :cond_9
    iget-object v1, v3, Lcom/google/android/gms/icing/impl/e/k;->b:Ljava/util/Map;

    invoke-interface {v1, v7}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Set;

    goto :goto_3

    :cond_a
    iget-object v1, v3, Lcom/google/android/gms/icing/impl/e/k;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v1, v6}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v1

    if-nez v1, :cond_b

    const-string v1, "Invalid usage report: missing config"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    move-object v3, v2

    goto :goto_4

    :cond_b
    move-object v3, v1

    goto :goto_4

    :cond_c
    new-instance v1, Lcom/google/android/gms/icing/impl/e/l;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    iget-object v4, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v2

    invoke-direct {v1, v0, v3, v2}, Lcom/google/android/gms/icing/impl/e/l;-><init>(Lcom/google/android/gms/icing/bf;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/impl/a/aa;)V

    move-object v0, v1

    :goto_6
    return-object v0

    :cond_d
    new-instance v1, Lcom/google/android/gms/icing/impl/e/l;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    iget-object v4, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/e/k;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v3

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/icing/impl/e/l;-><init>(Lcom/google/android/gms/icing/bf;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/impl/a/aa;)V

    move-object v0, v1

    goto :goto_6

    :cond_e
    iput v10, p0, Lcom/google/android/gms/common/internal/a;->a:I

    move-object v0, v2

    goto :goto_6
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/m;->c:Landroid/os/ParcelFileDescriptor;

    if-eqz v0, :cond_0

    .line 355
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/m;->c:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 360
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/m;->d:Lcom/google/android/gms/icing/impl/e/k;

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/e/k;->a:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->clear()V

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/e/k;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 361
    return-void

    .line 357
    :catch_0
    move-exception v0

    const-string v0, "Failed to close file descriptor."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0
.end method
