.class public final Lcom/google/android/gms/icing/impl/e/h;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;


# instance fields
.field private b:Ljava/io/FileOutputStream;

.field private c:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/e/h;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/e/h;->c:Landroid/content/Context;

    .line 123
    return-void
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;)Lcom/google/android/gms/icing/bf;
    .locals 4

    .prologue
    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->a()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    .line 93
    new-instance v2, Lcom/google/android/gms/icing/bf;

    invoke-direct {v2}, Lcom/google/android/gms/icing/bf;-><init>()V

    .line 94
    iput-object p1, v2, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    .line 95
    iput-wide p2, v2, Lcom/google/android/gms/icing/bf;->b:J

    .line 97
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    .line 98
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->b()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v2, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    .line 99
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->b()J

    move-result-wide v0

    iput-wide v0, v2, Lcom/google/android/gms/icing/bf;->f:J

    .line 101
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v0

    iput v0, v2, Lcom/google/android/gms/icing/bf;->g:I

    .line 104
    iget-object v0, v2, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->d()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v1

    invoke-static {v0, p2, p3, v1}, Lcom/google/android/gms/icing/impl/e/h;->a(Ljava/lang/String;JLcom/google/android/gms/appdatasearch/DocumentContents;)Lcom/google/android/gms/icing/o;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    .line 108
    if-eqz p4, :cond_0

    .line 109
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {p4}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_0

    .line 110
    invoke-interface {p4, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/ActivityManager$RunningTaskInfo;

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    .line 111
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 112
    iput v1, v2, Lcom/google/android/gms/icing/bf;->i:I

    .line 118
    :cond_0
    return-object v2

    .line 109
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;JLcom/google/android/gms/appdatasearch/DocumentContents;)Lcom/google/android/gms/icing/o;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 50
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 78
    :goto_0
    return-object v0

    .line 52
    :cond_0
    invoke-virtual {p3}, Lcom/google/android/gms/appdatasearch/DocumentContents;->a()[Lcom/google/android/gms/appdatasearch/DocumentSection;

    move-result-object v4

    .line 55
    new-instance v2, Lcom/google/android/gms/icing/o;

    invoke-direct {v2}, Lcom/google/android/gms/icing/o;-><init>()V

    .line 56
    iput-object p0, v2, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    .line 57
    array-length v0, v4

    new-array v0, v0, [Lcom/google/android/gms/icing/q;

    iput-object v0, v2, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    move v0, v1

    .line 58
    :goto_1
    array-length v3, v4

    if-ge v0, v3, :cond_3

    .line 59
    aget-object v5, v4, v0

    .line 60
    new-instance v6, Lcom/google/android/gms/icing/q;

    invoke-direct {v6}, Lcom/google/android/gms/icing/q;-><init>()V

    .line 61
    iput v0, v6, Lcom/google/android/gms/icing/q;->a:I

    .line 63
    :try_start_0
    iget-object v3, v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->c:Ljava/lang/String;

    if-eqz v3, :cond_2

    iget-object v3, v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->c:Ljava/lang/String;

    const-string v7, "UTF-8"

    invoke-virtual {v3, v7}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v3

    :goto_2
    iput-object v3, v6, Lcom/google/android/gms/icing/q;->c:[B
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    .line 70
    invoke-virtual {v5}, Lcom/google/android/gms/appdatasearch/DocumentSection;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v3

    invoke-static {v3, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Z)Lcom/google/android/gms/icing/av;

    move-result-object v3

    iput-object v3, v6, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    .line 71
    iget v3, v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->e:I

    sget v7, Lcom/google/android/gms/appdatasearch/DocumentSection;->a:I

    if-eq v3, v7, :cond_1

    .line 73
    iget-object v3, v6, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    iget v5, v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->e:I

    iput v5, v3, Lcom/google/android/gms/icing/av;->k:I

    .line 75
    :cond_1
    iget-object v3, v2, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    aput-object v6, v3, v0

    .line 58
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 63
    :cond_2
    :try_start_1
    iget-object v3, v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->f:[B
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 66
    :catch_0
    move-exception v0

    .line 67
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 77
    :cond_3
    iput-wide p1, v2, Lcom/google/android/gms/icing/o;->f:J

    move-object v0, v2

    .line 78
    goto :goto_0
.end method

.method public static a(Landroid/os/ParcelFileDescriptor;)Ljava/util/Iterator;
    .locals 5

    .prologue
    .line 232
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    if-nez v0, :cond_1

    .line 233
    :cond_0
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .line 235
    :goto_0
    return-object v0

    :cond_1
    new-instance v0, Lcom/google/android/gms/icing/impl/e/e;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    invoke-virtual {p0}, Landroid/os/ParcelFileDescriptor;->getStatSize()J

    move-result-wide v2

    const-class v4, Lcom/google/android/gms/icing/bf;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/icing/impl/e/e;-><init>(Ljava/io/FileInputStream;JLjava/lang/Class;)V

    goto :goto_0
.end method

.method private c()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 126
    sget-object v2, Lcom/google/android/gms/icing/impl/e/h;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 127
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v1, :cond_0

    .line 129
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/h;->c:Landroid/content/Context;

    const-string v3, "appdatasearch_usage"

    const v4, 0x8000

    invoke-virtual {v1, v3, v4}, Landroid/content/Context;->openFileOutput(Ljava/lang/String;I)Ljava/io/FileOutputStream;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :cond_0
    monitor-exit v2

    .line 136
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 130
    :catch_0
    move-exception v1

    :try_start_2
    const-string v3, "Failed to open usage log file"

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1, v3, v4}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 132
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    .line 140
    sget-object v1, Lcom/google/android/gms/icing/impl/e/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 141
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 143
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 147
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;

    .line 149
    :cond_0
    monitor-exit v1

    return-void

    .line 144
    :catch_0
    move-exception v0

    const-string v2, "Failed to close usage file stream."

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Object;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 149
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 12

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "Pending usage reports:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 244
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/h;->c:Landroid/content/Context;

    const-string v1, "appdatasearch_usage"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    .line 248
    new-instance v5, Lcom/google/android/gms/icing/impl/e/e;

    new-instance v1, Ljava/io/FileInputStream;

    invoke-direct {v1, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v6

    const-class v0, Lcom/google/android/gms/icing/bf;

    invoke-direct {v5, v1, v6, v7, v0}, Lcom/google/android/gms/icing/impl/e/e;-><init>(Ljava/io/FileInputStream;JLjava/lang/Class;)V

    .line 252
    const-string v0, "Icing"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v6

    .line 253
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 254
    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/bf;

    .line 255
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "From %s: type=%d, ts=%d, has_doc=%b Doc[package=%s corpus=%s, uri=%s]"

    const/4 v1, 0x7

    new-array v9, v1, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v10, v0, Lcom/google/android/gms/icing/bf;->a:Ljava/lang/String;

    aput-object v10, v9, v1

    const/4 v1, 0x1

    iget v10, v0, Lcom/google/android/gms/icing/bf;->g:I

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v9, v1

    const/4 v1, 0x2

    iget-wide v10, v0, Lcom/google/android/gms/icing/bf;->f:J

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    aput-object v10, v9, v1

    const/4 v10, 0x3

    iget-object v1, v0, Lcom/google/android/gms/icing/bf;->h:Lcom/google/android/gms/icing/o;

    if-eqz v1, :cond_1

    move v1, v2

    :goto_1
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    aput-object v1, v9, v10

    const/4 v1, 0x4

    iget-object v10, v0, Lcom/google/android/gms/icing/bf;->c:Ljava/lang/String;

    aput-object v10, v9, v1

    const/4 v1, 0x5

    iget-object v10, v0, Lcom/google/android/gms/icing/bf;->d:Ljava/lang/String;

    aput-object v10, v9, v1

    const/4 v1, 0x6

    if-eqz v6, :cond_2

    iget-object v0, v0, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    :goto_2
    aput-object v0, v9, v1

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 265
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "<empty>"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 267
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 255
    goto :goto_1

    :cond_2
    :try_start_1
    const-string v0, "<redacted>"
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public final a([Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;Ljava/lang/Runnable;)Z
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 166
    new-instance v3, Ljava/util/ArrayList;

    array-length v0, p1

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    move v0, v1

    .line 167
    :goto_0
    array-length v4, p1

    if-ge v0, v4, :cond_3

    .line 168
    aget-object v4, p1, v0

    if-eqz v4, :cond_0

    .line 169
    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_1

    .line 170
    const-string v4, "Dropping context-only usage from %s"

    invoke-static {v4, p2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 167
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :cond_1
    aget-object v4, p1, v0

    invoke-virtual {v4}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_2

    .line 174
    const-string v4, "Dropping usage end from %s"

    invoke-static {v4, p2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_1

    .line 178
    :cond_2
    aget-object v4, p1, v0

    invoke-static {v4, p2, p3, p4, p5}, Lcom/google/android/gms/icing/impl/e/h;->a(Lcom/google/android/gms/appdatasearch/UsageInfo;Ljava/lang/String;JLjava/util/List;)Lcom/google/android/gms/icing/bf;

    move-result-object v4

    .line 181
    invoke-interface {v3, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 183
    :cond_3
    invoke-static {v3, v1}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/lang/Iterable;Z)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 186
    sget-object v3, Lcom/google/android/gms/icing/impl/e/h;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 187
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/e/h;->c()Z

    move-result v4

    if-nez v4, :cond_5

    .line 188
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    :cond_4
    :goto_2
    return v1

    .line 191
    :cond_5
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;

    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v4

    .line 192
    invoke-virtual {v4, v0}, Ljava/nio/channels/FileChannel;->write(Ljava/nio/ByteBuffer;)I

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/h;->b:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->flush()V

    .line 194
    invoke-virtual {v4}, Ljava/nio/channels/FileChannel;->size()J
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v4

    const-wide/32 v6, 0x19000

    cmp-long v0, v4, v6

    if-ltz v0, :cond_6

    move v1, v2

    :cond_6
    move v0, v1

    move v1, v2

    .line 200
    :goto_3
    :try_start_2
    monitor-exit v3
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 201
    if-eqz p6, :cond_4

    if-eqz v0, :cond_4

    .line 202
    const-string v0, "Usage file limit reached"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 203
    invoke-interface {p6}, Ljava/lang/Runnable;->run()V

    goto :goto_2

    .line 198
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_3

    .line 200
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method

.method public final b()Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 210
    sget-object v1, Lcom/google/android/gms/icing/impl/e/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 211
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/e/h;->a()V

    .line 212
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/e/h;->c:Landroid/content/Context;

    const-string v3, "appdatasearch_usage"

    invoke-virtual {v2, v3}, Landroid/content/Context;->getFileStreamPath(Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 213
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    .line 214
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 226
    :goto_0
    return-object v0

    .line 216
    :cond_0
    const/high16 v3, 0x10000000

    :try_start_1
    invoke-static {v2, v3}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 223
    :try_start_2
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    move-result v2

    if-nez v2, :cond_1

    .line 224
    const-string v2, "Failed to delete file."

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 226
    :cond_1
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 220
    :catch_0
    move-exception v2

    :try_start_3
    const-string v2, "Unexpected FileNotFoundException when reading file."

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 221
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method
