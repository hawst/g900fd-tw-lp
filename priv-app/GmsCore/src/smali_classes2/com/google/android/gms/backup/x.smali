.class public final Lcom/google/android/gms/backup/x;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Z

.field c:Lcom/google/protobuf/a/a;

.field private d:Z

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 13
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    .line 30
    sget-object v0, Lcom/google/protobuf/a/a;->a:Lcom/google/protobuf/a/a;

    iput-object v0, p0, Lcom/google/android/gms/backup/x;->c:Lcom/google/protobuf/a/a;

    .line 67
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/x;->e:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/gms/backup/x;->e:I

    if-gez v0, :cond_0

    .line 72
    invoke-virtual {p0}, Lcom/google/android/gms/backup/x;->b()I

    .line 74
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/x;->e:I

    return v0
.end method

.method public final a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/backup/x;
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/x;->b:Z

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/backup/x;->c:Lcom/google/protobuf/a/a;

    .line 36
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/backup/x;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/x;->d:Z

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    .line 19
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/x;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/x;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->f()Lcom/google/protobuf/a/a;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/x;->a(Lcom/google/protobuf/a/a;)Lcom/google/android/gms/backup/x;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 59
    iget-boolean v0, p0, Lcom/google/android/gms/backup/x;->d:Z

    if-eqz v0, :cond_0

    .line 60
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 62
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/backup/x;->b:Z

    if-eqz v0, :cond_1

    .line 63
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/backup/x;->c:Lcom/google/protobuf/a/a;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/a;)V

    .line 65
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 79
    const/4 v0, 0x0

    .line 80
    iget-boolean v1, p0, Lcom/google/android/gms/backup/x;->d:Z

    if-eqz v1, :cond_0

    .line 81
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/backup/x;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 84
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/backup/x;->b:Z

    if-eqz v1, :cond_1

    .line 85
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/backup/x;->c:Lcom/google/protobuf/a/a;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/a;)I

    move-result v1

    add-int/2addr v0, v1

    .line 88
    :cond_1
    iput v0, p0, Lcom/google/android/gms/backup/x;->e:I

    .line 89
    return v0
.end method
