.class public Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;
.super Landroid/support/v7/app/d;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/udc/ui/x;


# instance fields
.field private final a:Lcom/google/android/gms/udc/util/j;

.field private final b:Lcom/google/android/gms/udc/ui/w;

.field private c:Lcom/google/android/gms/common/api/v;

.field private d:Lcom/google/android/gms/udc/e/m;

.field private e:Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

.field private f:Lcom/google/android/gms/analytics/bv;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 41
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    .line 56
    new-instance v0, Lcom/google/android/gms/udc/util/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/util/j;-><init>(Landroid/support/v4/app/q;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a:Lcom/google/android/gms/udc/util/j;

    .line 57
    new-instance v0, Lcom/google/android/gms/udc/ui/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/ui/w;-><init>(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->b:Lcom/google/android/gms/udc/ui/w;

    .line 228
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/udc/e/m;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 69
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 70
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 71
    const-string v1, "UdcSettingConfig"

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/udc/util/f;->a(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 72
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->c:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;Lcom/google/android/gms/udc/e/m;)Lcom/google/android/gms/udc/e/m;
    .locals 0

    .prologue
    .line 41
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/udc/e/m;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/analytics/bv;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->e:Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    return-object v0
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 193
    const/16 v0, 0x8

    if-eq p1, v0, :cond_0

    const/4 v0, 0x7

    if-ne p1, v0, :cond_1

    .line 195
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x2

    new-instance v2, Lcom/google/android/gms/udc/ui/v;

    invoke-direct {v2, p0, p1}, Lcom/google/android/gms/udc/ui/v;-><init>(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;I)V

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 198
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/m;)V
    .locals 4

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    const-string v1, "ChangeSetting"

    iget-object v2, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v2, v2, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v3, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 162
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/n;)V
    .locals 4

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    const-string v1, "ChangeDeviceSetting"

    iget v2, p1, Lcom/google/android/gms/udc/e/n;->c:I

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 189
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/o;)V
    .locals 4

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    const-string v1, "ChangeSetting"

    iget-object v2, p1, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v2, v2, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v3, p1, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 171
    return-void
.end method

.method public final b(Lcom/google/android/gms/udc/e/m;)V
    .locals 4

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    const-string v1, "ManageHistory"

    iget-object v2, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v2, v2, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v3, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 180
    return-void
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 151
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->setResult(I)V

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->b:Lcom/google/android/gms/udc/ui/w;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/util/j;->b(ILandroid/support/v4/app/av;)V

    .line 153
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 84
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 85
    const-string v1, "UdcAccountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->g:Ljava/lang/String;

    .line 86
    if-eqz p1, :cond_0

    .line 87
    const-string v0, "udc.OverviewConfig"

    new-instance v1, Lcom/google/android/gms/udc/e/m;

    invoke-direct {v1}, Lcom/google/android/gms/udc/e/m;-><init>()V

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/udc/util/f;->b(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/m;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    .line 94
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    iget-object v0, v0, Lcom/google/android/gms/udc/e/m;->c:Lcom/google/android/gms/udc/e/s;

    invoke-static {v0}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v0

    .line 96
    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 98
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 99
    sget v1, Lcom/google/android/gms/l;->fO:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->setContentView(I)V

    .line 101
    const-string v1, "AccountHistoryDetail"

    invoke-static {p0, v1}, Lcom/google/android/gms/udc/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->f:Lcom/google/android/gms/analytics/bv;

    .line 103
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    .line 104
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/support/v7/app/a;->a(Z)V

    .line 105
    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->a(Ljava/lang/CharSequence;)V

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->g:Ljava/lang/String;

    invoke-virtual {v1, v0}, Landroid/support/v7/app/a;->b(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->gp:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/v;->a(I)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->e:Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->e:Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->g:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Ljava/lang/String;)V

    .line 111
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->e:Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Lcom/google/android/gms/udc/e/m;)V

    .line 113
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/udc/c;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/lockbox/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {v0, p0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->c:Lcom/google/android/gms/common/api/v;

    .line 119
    return-void

    .line 90
    :cond_0
    const-string v1, "UdcSettingConfig"

    new-instance v2, Lcom/google/android/gms/udc/e/m;

    invoke-direct {v2}, Lcom/google/android/gms/udc/e/m;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/udc/util/f;->b(Landroid/content/Intent;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/m;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a:Lcom/google/android/gms/udc/util/j;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/util/j;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 136
    invoke-super {p0}, Landroid/support/v7/app/d;->onDestroy()V

    .line 137
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 141
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 146
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 143
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->finish()V

    .line 144
    const/4 v0, 0x1

    goto :goto_0

    .line 141
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 77
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 78
    const-string v0, "udc.OverviewConfig"

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d:Lcom/google/android/gms/udc/e/m;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 79
    return-void
.end method

.method protected onStart()V
    .locals 1

    .prologue
    .line 123
    invoke-super {p0}, Landroid/support/v7/app/d;->onStart()V

    .line 124
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/app/Activity;)V

    .line 125
    return-void
.end method

.method protected onStop()V
    .locals 1

    .prologue
    .line 129
    invoke-super {p0}, Landroid/support/v7/app/d;->onStop()V

    .line 130
    invoke-static {p0}, Lcom/google/android/gms/analytics/ax;->a(Landroid/content/Context;)Lcom/google/android/gms/analytics/ax;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/analytics/ax;->c(Landroid/app/Activity;)V

    .line 131
    return-void
.end method
