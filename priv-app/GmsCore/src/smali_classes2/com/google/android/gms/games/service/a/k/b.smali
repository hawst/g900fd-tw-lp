.class public final Lcom/google/android/gms/games/service/a/k/b;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/service/g;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;

.field private final f:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 2

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/k/b;->c:Lcom/google/android/gms/games/service/g;

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/k/b;->d:Lcom/google/android/gms/games/a/au;

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/k/b;->e:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/k/b;->f:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/b;->c:Lcom/google/android/gms/games/service/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/g;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 40
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/b;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k/b;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k/b;->f:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
