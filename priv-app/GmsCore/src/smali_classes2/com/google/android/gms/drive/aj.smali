.class public abstract Lcom/google/android/gms/drive/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/u;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;
.end method

.method public final a()Ljava/util/Map;
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->c:Lcom/google/android/gms/drive/metadata/internal/a/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    .line 77
    if-nez v0, :cond_0

    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;->b()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->d:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 90
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->a:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final f()Ljava/util/Date;
    .locals 1

    .prologue
    .line 122
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->b:Lcom/google/android/gms/drive/metadata/internal/a/n;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public final g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 127
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->r:Lcom/google/android/gms/drive/metadata/internal/a/e;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final h()Ljava/util/Date;
    .locals 1

    .prologue
    .line 136
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->d:Lcom/google/android/gms/drive/metadata/internal/a/o;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public final i()Ljava/util/Date;
    .locals 1

    .prologue
    .line 143
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->c:Lcom/google/android/gms/drive/metadata/internal/a/p;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public final j()Z
    .locals 1

    .prologue
    .line 203
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->m:Lcom/google/android/gms/drive/metadata/internal/a/d;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 204
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final k()Ljava/util/Date;
    .locals 1

    .prologue
    .line 220
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/l;->e:Lcom/google/android/gms/drive/metadata/internal/a/q;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Date;

    return-object v0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 225
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->A:Lcom/google/android/gms/drive/metadata/internal/a/i;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final m()Z
    .locals 1

    .prologue
    .line 251
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->h:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 252
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 263
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->j:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 264
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 289
    const-string v0, "application/vnd.google-apps.folder"

    invoke-virtual {p0}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final p()Z
    .locals 1

    .prologue
    .line 304
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->o:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 305
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final q()Z
    .locals 1

    .prologue
    .line 312
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->y:Lcom/google/android/gms/drive/metadata/internal/a/h;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    .line 313
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    goto :goto_0
.end method

.method public final r()Ljava/lang/String;
    .locals 1

    .prologue
    .line 382
    sget-object v0, Lcom/google/android/gms/drive/metadata/internal/a/a;->E:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/aj;->a(Lcom/google/android/gms/drive/metadata/f;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
