.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/games/realtime/a;

.field private h:Lcom/google/android/gms/games/realtime/network/aa;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/realtime/a;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 6

    .prologue
    .line 610
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p6

    .line 611
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 612
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->f:Ljava/lang/String;

    .line 613
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/a;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->g:Lcom/google/android/gms/games/realtime/a;

    .line 614
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 615
    return-void
.end method

.method private c()V
    .locals 2

    .prologue
    .line 725
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->f(Ljava/lang/String;)V

    .line 726
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    if-eqz v0, :cond_0

    .line 728
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/aa;->a()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 732
    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    .line 734
    :cond_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x0

    const/4 v2, 0x1

    .line 619
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    move v0, v1

    .line 720
    :goto_0
    return v0

    .line 621
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/j;

    .line 622
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/aa;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 623
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/j;->b:[B

    :try_start_0
    iget-object v3, v1, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    invoke-virtual {v3}, Landroid/net/LocalSocket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/io/OutputStream;->write([B)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    move v0, v2

    .line 627
    goto :goto_0

    .line 623
    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/games/realtime/network/aa;->a()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_2
    const-string v3, "SocketProxy"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "IOException writing data to socket."

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/games/internal/ej;->b(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, v1, Lcom/google/android/gms/games/realtime/network/aa;->a:Lcom/google/android/gms/games/realtime/network/b;

    new-instance v3, Lcom/google/android/gms/games/realtime/network/o;

    iget-object v4, v1, Lcom/google/android/gms/games/realtime/network/aa;->c:Ljava/lang/String;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/games/realtime/network/o;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/realtime/network/aa;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/realtime/network/b;->a(Lcom/google/android/gms/games/realtime/network/o;)V

    goto :goto_1

    :catch_1
    move-exception v3

    const-string v4, "SocketProxy"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Error closing socket:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/games/internal/ej;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 625
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/j;->b:[B

    invoke-interface {v1, v3, v0}, Lcom/google/android/gms/games/realtime/network/a;->b(Ljava/lang/String;[B)V

    goto :goto_1

    .line 630
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/g;

    .line 631
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    iget v3, v0, Lcom/google/android/gms/games/jingle/g;->a:I

    iget-object v4, v0, Lcom/google/android/gms/games/jingle/g;->b:Ljava/lang/String;

    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/g;->c:Z

    invoke-interface {v1, v3, v4, v0}, Lcom/google/android/gms/games/realtime/network/a;->a(ILjava/lang/String;Z)V

    move v0, v2

    .line 632
    goto :goto_0

    .line 635
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/f;

    .line 636
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v3, v0, Lcom/google/android/gms/games/jingle/f;->a:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/f;->b:[B

    invoke-interface {v1, v3, v0}, Lcom/google/android/gms/games/realtime/network/a;->a(Ljava/lang/String;[B)V

    move v0, v2

    .line 637
    goto/16 :goto_0

    .line 640
    :sswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/i;

    .line 641
    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/i;->b:Z

    if-nez v0, :cond_1

    .line 644
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    new-instance v3, Lcom/google/android/gms/games/realtime/a;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->b:Ljava/lang/String;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v5, "P2P_FAILED"

    invoke-direct {v3, v4, v1, v5}, Lcom/google/android/gms/games/realtime/a;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/realtime/network/a;->a(Lcom/google/android/gms/games/realtime/a;)V

    :cond_1
    move v0, v2

    .line 650
    goto/16 :goto_0

    .line 653
    :sswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/d;

    .line 654
    iget v0, v0, Lcom/google/android/gms/games/jingle/d;->d:I

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    move v0, v2

    .line 662
    goto/16 :goto_0

    .line 658
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->c()V

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    move v0, v2

    .line 660
    goto/16 :goto_0

    .line 666
    :sswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/m;

    .line 667
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v3, v0, Lcom/google/android/gms/games/realtime/network/m;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/games/realtime/network/m;->a:[B

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;[B)I

    move-result v1

    .line 669
    sget v3, Lcom/google/android/gms/games/jingle/Libjingle;->FAILURE_OPS_ID:I

    if-ne v1, v3, :cond_2

    .line 670
    const/4 v1, -0x1

    .line 672
    :cond_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/m;->a(Ljava/lang/Object;)V

    move v0, v2

    .line 673
    goto/16 :goto_0

    .line 676
    :sswitch_6
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/n;

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->f:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/n;->a:[B

    invoke-virtual {v1, v3, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;[B)V

    move v0, v2

    .line 678
    goto/16 :goto_0

    .line 681
    :sswitch_7
    invoke-direct {p0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->c()V

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->b:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v1, v3, v4, v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v1, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    move v0, v2

    .line 683
    goto/16 :goto_0

    .line 686
    :sswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/g;

    .line 687
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v3, v0, Lcom/google/android/gms/games/realtime/network/g;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/jingle/Libjingle;->b(Ljava/lang/String;)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/g;->a(Ljava/lang/Object;)V

    move v0, v2

    .line 688
    goto/16 :goto_0

    .line 691
    :sswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/o;

    .line 692
    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/o;->a:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 693
    iput-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    :cond_3
    move v0, v2

    .line 695
    goto/16 :goto_0

    .line 698
    :sswitch_a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/h;

    .line 700
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    if-nez v1, :cond_4

    .line 701
    iget-object v1, v0, Lcom/google/android/gms/games/realtime/network/h;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/realtime/network/h;->b:Lcom/google/android/gms/games/realtime/network/b;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "com.android.games"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Landroid/net/LocalServerSocket;

    invoke-direct {v5, v4}, Landroid/net/LocalServerSocket;-><init>(Ljava/lang/String;)V

    new-instance v6, Lcom/google/android/gms/games/realtime/network/aa;

    invoke-direct {v6, v1, v3, v4, v5}, Lcom/google/android/gms/games/realtime/network/aa;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/realtime/network/b;Ljava/lang/String;Landroid/net/LocalServerSocket;)V

    invoke-virtual {v6}, Lcom/google/android/gms/games/realtime/network/aa;->b()V

    iput-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    .line 703
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->h:Lcom/google/android/gms/games/realtime/network/aa;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/aa;->d:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/h;->a(Ljava/lang/Object;)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_3

    :goto_3
    move v0, v2

    .line 711
    goto/16 :goto_0

    .line 704
    :catch_2
    move-exception v1

    .line 705
    const-string v3, "PeerStateMachine"

    const-string v4, "Unable to create socket proxy."

    invoke-static {v3, v4, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 706
    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/realtime/network/h;->a(Ljava/lang/Object;)V

    goto :goto_3

    .line 707
    :catch_3
    move-exception v1

    .line 708
    const-string v3, "PeerStateMachine"

    const-string v4, "Unable to create socket proxy."

    invoke-static {v3, v4, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 709
    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/realtime/network/h;->a(Ljava/lang/Object;)V

    goto :goto_3

    .line 716
    :sswitch_b
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;->g:Lcom/google/android/gms/games/realtime/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/realtime/network/a;->b(Lcom/google/android/gms/games/realtime/a;)V

    move v0, v2

    .line 717
    goto/16 :goto_0

    .line 619
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x65 -> :sswitch_2
        0x66 -> :sswitch_1
        0x12d -> :sswitch_3
        0x12e -> :sswitch_0
        0x1775 -> :sswitch_8
        0x1776 -> :sswitch_a
        0x1777 -> :sswitch_5
        0x1778 -> :sswitch_6
        0x1779 -> :sswitch_b
        0x177a -> :sswitch_7
        0x177d -> :sswitch_9
    .end sparse-switch

    .line 654
    :pswitch_data_0
    .packed-switch 0xc
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
