.class final Lcom/google/android/gms/car/ek;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/t;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarHomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/CarHomeActivity;)V
    .locals 0

    .prologue
    .line 63
    iput-object p1, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 66
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 67
    const-string v0, "CAR.HOME"

    const-string v1, "onCarConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "CAR.HOME"

    const-string v1, "onCarDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->c(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/hs;)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/car/ek;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarHomeActivity;->finish()V

    .line 79
    return-void
.end method
