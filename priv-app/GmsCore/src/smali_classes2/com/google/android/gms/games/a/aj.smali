.class final Lcom/google/android/gms/games/a/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/af;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/a/ab;

.field private final b:Landroid/content/Context;

.field private final c:Lcom/google/android/gms/common/server/ClientContext;

.field private final d:Ljava/lang/String;

.field private final e:Z

.field private f:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V
    .locals 0

    .prologue
    .line 1919
    iput-object p1, p0, Lcom/google/android/gms/games/a/aj;->a:Lcom/google/android/gms/games/a/ab;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1920
    iput-object p2, p0, Lcom/google/android/gms/games/a/aj;->b:Landroid/content/Context;

    .line 1921
    iput-object p3, p0, Lcom/google/android/gms/games/a/aj;->c:Lcom/google/android/gms/common/server/ClientContext;

    .line 1922
    iput-object p4, p0, Lcom/google/android/gms/games/a/aj;->d:Ljava/lang/String;

    .line 1923
    iput-boolean p5, p0, Lcom/google/android/gms/games/a/aj;->e:Z

    .line 1924
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;ZB)V
    .locals 0

    .prologue
    .line 1901
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/games/a/aj;-><init>(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)V

    return-void
.end method


# virtual methods
.method public final a(Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 1928
    iput-object p1, p0, Lcom/google/android/gms/games/a/aj;->f:Ljava/util/ArrayList;

    .line 1929
    new-instance v0, Ljava/lang/Thread;

    invoke-direct {v0, p0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 1930
    return-void
.end method

.method public final run()V
    .locals 13

    .prologue
    const/4 v0, 0x0

    .line 1935
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1939
    iget-object v1, p0, Lcom/google/android/gms/games/a/aj;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/aj;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/games/a/aj;->d:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v10

    .line 1941
    const-wide/16 v2, -0x1

    cmp-long v1, v10, v2

    if-nez v1, :cond_0

    .line 1942
    const-string v0, "GameAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to find record for player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/aj;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when storing game collection."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1980
    :goto_0
    return-void

    .line 1948
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/games/a/aj;->e:Z

    if-eqz v1, :cond_1

    .line 1949
    iget-object v1, p0, Lcom/google/android/gms/games/a/aj;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "muted"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1955
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/aj;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v9, v0

    .line 1956
    :goto_1
    if-ge v9, v12, :cond_4

    .line 1957
    iget-object v0, p0, Lcom/google/android/gms/games/a/aj;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/bg;

    .line 1958
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bg;->getGamesData()Lcom/google/android/gms/games/h/a/u;

    move-result-object v4

    .line 1959
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/bg;->getMarketData()Lcom/google/android/gms/games/h/a/ce;

    move-result-object v5

    .line 1960
    if-nez v4, :cond_2

    if-nez v5, :cond_2

    .line 1962
    const-string v0, "GameAgent"

    const-string v1, "Received application with no app data and no Market data!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1956
    :goto_2
    add-int/lit8 v0, v9, 0x1

    move v9, v0

    goto :goto_1

    .line 1968
    :cond_2
    if-eqz v4, :cond_3

    iget-boolean v0, p0, Lcom/google/android/gms/games/a/aj;->e:Z

    if-eqz v0, :cond_3

    .line 1969
    iget-object v0, v4, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v1, "muted"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1974
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/a/aj;->a:Lcom/google/android/gms/games/a/ab;

    iget-object v1, p0, Lcom/google/android/gms/games/a/aj;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/aj;->c:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    iget-boolean v7, p0, Lcom/google/android/gms/games/a/aj;->e:Z

    invoke-static/range {v0 .. v8}, Lcom/google/android/gms/games/a/ab;->a(Lcom/google/android/gms/games/a/ab;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bg;Lcom/google/android/gms/games/h/a/u;Lcom/google/android/gms/games/h/a/ce;Ljava/lang/Long;ZLjava/util/ArrayList;)J

    goto :goto_2

    .line 1978
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/a/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "GameAgent"

    invoke-static {v0, v8, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_0
.end method
