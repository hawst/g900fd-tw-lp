.class final Lcom/google/android/gms/car/gp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/gn;

.field private final b:Lcom/google/android/gms/car/na;

.field private final c:Landroid/util/SparseBooleanArray;

.field private volatile d:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/na;)V
    .locals 1

    .prologue
    .line 665
    iput-object p1, p0, Lcom/google/android/gms/car/gp;->a:Lcom/google/android/gms/car/gn;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 660
    new-instance v0, Landroid/util/SparseBooleanArray;

    invoke-direct {v0}, Landroid/util/SparseBooleanArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    .line 663
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/gp;->d:Z

    .line 666
    iput-object p2, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    .line 667
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/gp;)Lcom/google/android/gms/car/na;
    .locals 1

    .prologue
    .line 657
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    return-object v0
.end method


# virtual methods
.method final a()I
    .locals 1

    .prologue
    .line 691
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    return v0
.end method

.method final a(I)V
    .locals 2

    .prologue
    .line 683
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    const/4 v1, 0x1

    invoke-virtual {v0, p1, v1}, Landroid/util/SparseBooleanArray;->put(IZ)V

    .line 684
    return-void
.end method

.method final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 2

    .prologue
    .line 717
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/gp;->d:Z

    if-eqz v0, :cond_1

    .line 718
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/na;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 727
    :cond_0
    :goto_0
    return-void

    .line 720
    :cond_1
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 721
    const-string v0, "CAR.SENSOR"

    const-string v1, "sensor update while client is already released"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 727
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final a(Landroid/os/IBinder;)Z
    .locals 1

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v0}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final b(I)V
    .locals 1

    .prologue
    .line 687
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseBooleanArray;->delete(I)V

    .line 688
    return-void
.end method

.method final b()[I
    .locals 3

    .prologue
    .line 695
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->size()I

    move-result v0

    new-array v1, v0, [I

    .line 696
    array-length v0, v1

    add-int/lit8 v0, v0, -0x1

    :goto_0
    if-ltz v0, :cond_0

    .line 697
    iget-object v2, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseBooleanArray;->keyAt(I)I

    move-result v2

    aput v2, v1, v0

    .line 696
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 699
    :cond_0
    return-object v1
.end method

.method public final binderDied()V
    .locals 2

    .prologue
    .line 711
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v0}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 712
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->a:Lcom/google/android/gms/car/gn;

    invoke-static {v0, p0}, Lcom/google/android/gms/car/gn;->a(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gp;)V

    .line 713
    return-void
.end method

.method final c()Lcom/google/android/gms/car/na;
    .locals 1

    .prologue
    .line 703
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    return-object v0
.end method

.method final d()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 730
    iget-boolean v0, p0, Lcom/google/android/gms/car/gp;->d:Z

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v0}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 732
    iget-object v0, p0, Lcom/google/android/gms/car/gp;->c:Landroid/util/SparseBooleanArray;

    invoke-virtual {v0}, Landroid/util/SparseBooleanArray;->clear()V

    .line 733
    iput-boolean v1, p0, Lcom/google/android/gms/car/gp;->d:Z

    .line 735
    :cond_0
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 671
    instance-of v0, p1, Lcom/google/android/gms/car/gp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v0}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    check-cast p1, Lcom/google/android/gms/car/gp;

    iget-object v1, p1, Lcom/google/android/gms/car/gp;->b:Lcom/google/android/gms/car/na;

    invoke-interface {v1}, Lcom/google/android/gms/car/na;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-ne v0, v1, :cond_0

    .line 673
    const/4 v0, 0x1

    .line 675
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
