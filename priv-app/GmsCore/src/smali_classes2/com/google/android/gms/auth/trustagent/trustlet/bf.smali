.class public final Lcom/google/android/gms/auth/trustagent/trustlet/bf;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/bo;


# static fields
.field private static a:Lcom/google/android/gms/auth/trustagent/trustlet/bf;


# instance fields
.field private final b:Landroid/content/Context;

.field private final c:Ljava/lang/Object;

.field private d:Z

.field private e:Z


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b:Landroid/content/Context;

    .line 53
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c:Ljava/lang/Object;

    .line 54
    return-void
.end method

.method public static declared-synchronized a()Lcom/google/android/gms/auth/trustagent/trustlet/bf;
    .locals 3

    .prologue
    .line 40
    const-class v1, Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    if-nez v0, :cond_0

    .line 41
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    .line 43
    :cond_0
    sget-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bf;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 0

    .prologue
    .line 76
    return-void
.end method

.method public final a(ZZLjava/lang/String;)V
    .locals 6

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->e:Z

    if-eq p1, v0, :cond_0

    const-string v0, "Coffee - TrustStateMonitor"

    const-string v2, "Trusted state changed from %b to %b."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->e:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iput-boolean p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->e:Z

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.auth.trustagent.TRUST_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "is_trusted"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b:Landroid/content/Context;

    const-string v3, "com.google.android.gms.auth.trustagent.permission.TRUSTAGENT_STATE"

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Z)V
    .locals 6

    .prologue
    .line 89
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 90
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->d:Z

    if-eq p1, v0, :cond_0

    .line 91
    const-string v0, "Coffee - TrustStateMonitor"

    const-string v2, "TrustAgent enabled state changed from %b to %b."

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-boolean v5, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->d:Z

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    iput-boolean p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->d:Z

    .line 98
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms.auth.trustagent.TRUST_AGENT_STATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v2, "is_enabled"

    invoke-virtual {v0, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 101
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b:Landroid/content/Context;

    const-string v3, "com.google.android.gms.auth.trustagent.permission.TRUSTAGENT_STATE"

    invoke-virtual {v2, v0, v3}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    .line 103
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 57
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 58
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->d:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 64
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->e:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
