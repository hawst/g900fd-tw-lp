.class final Lcom/google/android/gms/car/ef;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarErrorDisplayActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/CarErrorDisplayActivity;)V
    .locals 0

    .prologue
    .line 40
    iput-object p1, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->a(Lcom/google/android/gms/car/CarErrorDisplayActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v0

    .line 44
    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->c()I

    move-result v0

    .line 45
    if-nez v0, :cond_1

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->finish()V

    .line 52
    :cond_0
    :goto_0
    return-void

    .line 51
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->a(Lcom/google/android/gms/car/CarErrorDisplayActivity;I)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->isFinishing()Z

    move-result v0

    if-nez v0, :cond_0

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/car/ef;->a:Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarErrorDisplayActivity;->finish()V

    .line 59
    :cond_0
    return-void
.end method
