.class public final Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;
.super Lcom/google/android/gms/games/ui/client/quests/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/quests/k;


# static fields
.field private static final n:I


# instance fields
.field private o:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget v0, Lcom/google/android/gms/l;->bA:I

    sput v0, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    .line 28
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/j;->qg:I

    sget v2, Lcom/google/android/gms/p;->kB:I

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/ui/client/quests/a;-><init>(III)V

    .line 29
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 56
    const/16 v0, 0x10

    return v0
.end method

.method public final V()[I
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->o:[I

    return-object v0
.end method

.method protected final a(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/quests/a;->a(Landroid/content/Intent;)V

    .line 47
    const-string v0, "com.google.android.gms.games.QUEST_STATES"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getIntArrayExtra(Ljava/lang/String;)[I

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->o:[I

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->o:[I

    if-nez v0, :cond_0

    .line 49
    const-string v0, "ClientQuestListAct"

    const-string v1, "EXTRA_QUEST_STATES extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->finish()V

    .line 52
    :cond_0
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x1

    return v0
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 38
    sget v0, Lcom/google/android/gms/games/ui/client/quests/ClientQuestListActivity;->n:I

    return v0
.end method
