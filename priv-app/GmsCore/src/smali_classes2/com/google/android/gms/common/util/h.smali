.class public final Lcom/google/android/gms/common/util/h;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a([Ljava/lang/Object;Lcom/google/android/gms/common/util/ap;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 420
    if-eqz p0, :cond_0

    array-length v1, p0

    if-nez v1, :cond_1

    .line 436
    :cond_0
    return v0

    .line 425
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 426
    aget-object v3, p0, v1

    invoke-interface {p1, v3}, Lcom/google/android/gms/common/util/ap;->a(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 428
    if-eq v0, v1, :cond_2

    .line 429
    aget-object v3, p0, v0

    .line 430
    aget-object v4, p0, v1

    aput-object v4, p0, v0

    .line 431
    aput-object v3, p0, v1

    .line 433
    :cond_2
    add-int/lit8 v0, v0, 0x1

    .line 425
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;)Ljava/util/ArrayList;
    .locals 4

    .prologue
    .line 446
    array-length v1, p0

    .line 447
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 448
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 449
    aget-object v3, p0, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 448
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 451
    :cond_0
    return-object v2
.end method

.method public static a(Ljava/lang/StringBuilder;[D)V
    .locals 4

    .prologue
    .line 230
    array-length v1, p1

    .line 231
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 232
    if-eqz v0, :cond_0

    .line 233
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 235
    :cond_0
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/Double;->toString(D)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 231
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 237
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[F)V
    .locals 3

    .prologue
    .line 220
    array-length v1, p1

    .line 221
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 222
    if-eqz v0, :cond_0

    .line 223
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 225
    :cond_0
    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Float;->toString(F)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 221
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 227
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[I)V
    .locals 3

    .prologue
    .line 200
    array-length v1, p1

    .line 201
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 202
    if-eqz v0, :cond_0

    .line 203
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 205
    :cond_0
    aget v2, p1, v0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 201
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 207
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[J)V
    .locals 4

    .prologue
    .line 210
    array-length v1, p1

    .line 211
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 212
    if-eqz v0, :cond_0

    .line 213
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    :cond_0
    aget-wide v2, p1, v0

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 217
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 190
    array-length v1, p1

    .line 191
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 192
    if-eqz v0, :cond_0

    .line 193
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 195
    :cond_0
    aget-object v2, p1, v0

    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 191
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 197
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 250
    array-length v1, p1

    .line 251
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 252
    if-eqz v0, :cond_0

    .line 253
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 255
    :cond_0
    const-string v2, "\""

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    aget-object v3, p1, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 257
    :cond_1
    return-void
.end method

.method public static a(Ljava/lang/StringBuilder;[Z)V
    .locals 3

    .prologue
    .line 240
    array-length v1, p1

    .line 241
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 242
    if-eqz v0, :cond_0

    .line 243
    const-string v2, ","

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 245
    :cond_0
    aget-boolean v2, p1, v0

    invoke-static {v2}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 247
    :cond_1
    return-void
.end method

.method public static a([II)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 99
    if-nez p0, :cond_1

    .line 107
    :cond_0
    :goto_0
    return v0

    .line 102
    :cond_1
    array-length v2, p0

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_0

    aget v3, p0, v1

    .line 103
    if-ne v3, p1, :cond_2

    .line 104
    const/4 v0, 0x1

    goto :goto_0

    .line 102
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public static a([Ljava/lang/Object;Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 47
    if-eqz p0, :cond_1

    array-length v0, p0

    :goto_0
    move v2, v1

    :goto_1
    if-ge v2, v0, :cond_3

    aget-object v3, p0, v2

    invoke-static {v3, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    move v0, v2

    :goto_2
    if-ltz v0, :cond_0

    const/4 v1, 0x1

    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_3
    const/4 v0, -0x1

    goto :goto_2
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;)Z
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 137
    if-nez p0, :cond_1

    .line 148
    :cond_0
    :goto_0
    return v0

    .line 140
    :cond_1
    array-length v3, p0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_0

    aget-object v4, p0, v2

    .line 141
    if-ne v4, p1, :cond_2

    move v0, v1

    .line 142
    goto :goto_0

    .line 144
    :cond_2
    if-eqz v4, :cond_3

    invoke-virtual {v4, p1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_3

    move v0, v1

    .line 145
    goto :goto_0

    .line 140
    :cond_3
    add-int/lit8 v2, v2, 0x1

    goto :goto_1
.end method

.method public static varargs a([I[I)[I
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 357
    if-nez p0, :cond_0

    .line 358
    const/4 v0, 0x0

    .line 380
    :goto_0
    return-object v0

    .line 360
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 361
    :cond_1
    array-length v0, p0

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto :goto_0

    .line 363
    :cond_2
    array-length v0, p0

    new-array v2, v0, [I

    .line 367
    array-length v0, p1

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 368
    array-length v5, p0

    move v4, v3

    move v1, v3

    :goto_1
    if-ge v4, v5, :cond_4

    aget v6, p0, v4

    .line 369
    aget v0, p1, v3

    if-eq v0, v6, :cond_7

    .line 370
    add-int/lit8 v0, v1, 0x1

    aput v6, v2, v1

    .line 368
    :goto_2
    add-int/lit8 v1, v4, 0x1

    move v4, v1

    move v1, v0

    goto :goto_1

    .line 374
    :cond_3
    array-length v4, p0

    move v1, v3

    :goto_3
    if-ge v3, v4, :cond_4

    aget v5, p0, v3

    .line 375
    invoke-static {p1, v5}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-nez v0, :cond_6

    .line 376
    add-int/lit8 v0, v1, 0x1

    aput v5, v2, v1

    .line 374
    :goto_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_3

    .line 380
    :cond_4
    array-length v0, v2

    if-eq v1, v0, :cond_5

    invoke-static {v2, v1}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto :goto_0

    :cond_5
    move-object v0, v2

    goto :goto_0

    :cond_6
    move v0, v1

    goto :goto_4

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method public static a([I)[Ljava/lang/Integer;
    .locals 4

    .prologue
    .line 174
    if-nez p0, :cond_1

    .line 175
    const/4 v0, 0x0

    .line 182
    :cond_0
    return-object v0

    .line 177
    :cond_1
    array-length v2, p0

    .line 178
    new-array v0, v2, [Ljava/lang/Integer;

    .line 179
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 180
    aget v3, p0, v1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v0, v1

    .line 179
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static a([Ljava/lang/Object;I)[Ljava/lang/Object;
    .locals 1

    .prologue
    .line 388
    if-nez p0, :cond_1

    .line 389
    const/4 p0, 0x0

    .line 395
    :cond_0
    :goto_0
    return-object p0

    .line 392
    :cond_1
    array-length v0, p0

    if-eq p1, v0, :cond_0

    .line 393
    invoke-static {p0, p1}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p0

    goto :goto_0
.end method

.method public static varargs a([Ljava/lang/Object;[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 325
    if-nez p0, :cond_0

    .line 326
    const/4 v0, 0x0

    .line 349
    :goto_0
    return-object v0

    .line 328
    :cond_0
    if-eqz p1, :cond_1

    array-length v0, p1

    if-nez v0, :cond_2

    .line 329
    :cond_1
    array-length v0, p0

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 332
    :cond_2
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getComponentType()Ljava/lang/Class;

    move-result-object v0

    array-length v1, p0

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 336
    array-length v1, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 337
    array-length v5, p0

    move v4, v3

    move v2, v3

    :goto_1
    if-ge v4, v5, :cond_4

    aget-object v6, p0, v4

    .line 338
    aget-object v1, p1, v3

    invoke-static {v1, v6}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_6

    .line 339
    add-int/lit8 v1, v2, 0x1

    aput-object v6, v0, v2

    .line 337
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    move v2, v1

    goto :goto_1

    .line 343
    :cond_3
    array-length v4, p0

    move v2, v3

    :goto_3
    if-ge v3, v4, :cond_4

    aget-object v5, p0, v3

    .line 344
    invoke-static {p1, v5}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_5

    .line 345
    add-int/lit8 v1, v2, 0x1

    aput-object v5, v0, v2

    .line 343
    :goto_4
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    move v2, v1

    goto :goto_3

    .line 349
    :cond_4
    invoke-static {v0, v2}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    :cond_5
    move v1, v2

    goto :goto_4

    :cond_6
    move v1, v2

    goto :goto_2
.end method

.method public static varargs a([[Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 263
    array-length v0, p0

    if-nez v0, :cond_0

    .line 264
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 281
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 269
    :goto_1
    array-length v3, p0

    if-ge v0, v3, :cond_1

    .line 270
    aget-object v3, p0, v0

    array-length v3, v3

    add-int/2addr v2, v3

    .line 269
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 274
    :cond_1
    aget-object v0, p0, v1

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v3

    .line 275
    aget-object v0, p0, v1

    array-length v2, v0

    .line 276
    const/4 v0, 0x1

    :goto_2
    array-length v4, p0

    if-ge v0, v4, :cond_2

    .line 277
    aget-object v4, p0, v0

    .line 278
    array-length v5, v4

    invoke-static {v4, v1, v3, v2, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 279
    array-length v4, v4

    add-int/2addr v2, v4

    .line 276
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    move-object v0, v3

    .line 281
    goto :goto_0
.end method

.method public static b([II)[I
    .locals 2

    .prologue
    .line 311
    if-eqz p0, :cond_0

    array-length v0, p0

    if-nez v0, :cond_1

    .line 312
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 316
    :goto_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput p1, v0, v1

    .line 317
    return-object v0

    .line 314
    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([II)[I

    move-result-object v0

    goto :goto_0
.end method

.method public static b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;
    .locals 2

    .prologue
    .line 292
    if-nez p0, :cond_0

    if-nez p1, :cond_0

    .line 293
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot generate array of generic type w/o class info"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 295
    :cond_0
    if-nez p0, :cond_1

    .line 296
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    .line 300
    :goto_0
    array-length v1, v0

    add-int/lit8 v1, v1, -0x1

    aput-object p1, v0, v1

    .line 301
    return-object v0

    .line 298
    :cond_1
    array-length v0, p0

    add-int/lit8 v0, v0, 0x1

    invoke-static {p0, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
