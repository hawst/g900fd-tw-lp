.class public abstract Lcom/google/android/gms/icing/b/d;
.super Lcom/google/android/gms/icing/b/a;
.source "SourceFile"


# instance fields
.field private final g:Ljava/util/concurrent/ScheduledExecutorService;

.field private final h:Ljava/util/concurrent/ScheduledExecutorService;


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/icing/b/a;-><init>()V

    .line 91
    new-instance v0, Lcom/google/android/gms/icing/b/e;

    const/4 v1, 0x4

    const-string v2, "Icing-Pool"

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/e;-><init>(ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/d;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 93
    new-instance v0, Lcom/google/android/gms/icing/b/e;

    const/4 v1, 0x1

    const-string v2, "Icing-Worker"

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/e;-><init>(ILjava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/b/d;->h:Ljava/util/concurrent/ScheduledExecutorService;

    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/icing/b/c;J)V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p1, Lcom/google/android/gms/icing/b/c;->a:Lcom/google/android/gms/icing/b/h;

    iget v0, v0, Lcom/google/android/gms/icing/b/h;->i:I

    .line 100
    packed-switch v0, :pswitch_data_0

    .line 108
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid task scheduling: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 102
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/icing/b/d;->g:Ljava/util/concurrent/ScheduledExecutorService;

    .line 110
    :goto_0
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, p1, p2, p3, v1}, Ljava/util/concurrent/ScheduledExecutorService;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    .line 111
    return-void

    .line 105
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/b/d;->h:Ljava/util/concurrent/ScheduledExecutorService;

    goto :goto_0

    .line 100
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
