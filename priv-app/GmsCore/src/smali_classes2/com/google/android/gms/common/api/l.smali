.class public abstract Lcom/google/android/gms/common/api/l;
.super Lcom/google/android/gms/common/api/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ah;
.implements Lcom/google/android/gms/common/api/m;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/j;

.field private b:Ljava/util/concurrent/atomic/AtomicReference;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/j;Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 100
    invoke-interface {p2}, Lcom/google/android/gms/common/api/v;->a()Landroid/os/Looper;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/a;-><init>(Landroid/os/Looper;)V

    .line 94
    new-instance v0, Ljava/util/concurrent/atomic/AtomicReference;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicReference;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/l;->b:Ljava/util/concurrent/atomic/AtomicReference;

    .line 101
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/j;

    iput-object v0, p0, Lcom/google/android/gms/common/api/l;->a:Lcom/google/android/gms/common/api/j;

    .line 102
    return-void
.end method

.method private a(Landroid/os/RemoteException;)V
    .locals 4

    .prologue
    .line 174
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-virtual {p1}, Landroid/os/RemoteException;->getLocalizedMessage()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 176
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/l;->b(Lcom/google/android/gms/common/api/Status;)V

    .line 177
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/af;)V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->b:Ljava/util/concurrent/atomic/AtomicReference;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/atomic/AtomicReference;->set(Ljava/lang/Object;)V

    .line 149
    return-void
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/h;)V
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 89
    check-cast p1, Lcom/google/android/gms/common/api/ap;

    invoke-super {p0, p1}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/ap;)V

    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/Status;)V
    .locals 2

    .prologue
    .line 139
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Failed result must not be success"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 140
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 141
    return-void

    .line 139
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/h;)V
    .locals 1

    .prologue
    .line 124
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/l;->a(Lcom/google/android/gms/common/api/h;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 132
    :goto_0
    return-void

    .line 125
    :catch_0
    move-exception v0

    .line 126
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/l;->a(Landroid/os/RemoteException;)V

    .line 128
    throw v0

    .line 129
    :catch_1
    move-exception v0

    .line 130
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/l;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method protected final d()V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->b:Ljava/util/concurrent/atomic/AtomicReference;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicReference;->getAndSet(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/af;

    .line 168
    if-eqz v0, :cond_0

    .line 169
    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/af;->a(Lcom/google/android/gms/common/api/ah;)V

    .line 171
    :cond_0
    return-void
.end method

.method public final f()Lcom/google/android/gms/common/api/j;
    .locals 1

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/common/api/l;->a:Lcom/google/android/gms/common/api/j;

    return-object v0
.end method

.method public g()I
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x0

    return v0
.end method
