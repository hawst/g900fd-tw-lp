.class public final Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;
.super Lcom/google/android/gms/games/ui/headless/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/b/a/d;
.implements Lcom/google/android/gms/games/ui/b/a/j;
.implements Lcom/google/android/gms/games/ui/common/matches/ab;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# static fields
.field private static final i:I

.field private static final j:I


# instance fields
.field private k:Lcom/google/android/gms/games/ui/headless/matches/a;

.field private l:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private m:Ljava/lang/String;

.field private n:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    sget v0, Lcom/google/android/gms/l;->bx:I

    sput v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->i:I

    .line 30
    sget v0, Lcom/google/android/gms/m;->m:I

    sput v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->j:I

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    sget v0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->i:I

    sget v1, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->j:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/headless/a;-><init>(II)V

    .line 40
    return-void
.end method


# virtual methods
.method public final T()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final U()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;
    .locals 1

    .prologue
    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->l:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final W()Ljava/lang/String;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/ui/b/a/c;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/games/ui/b/a/i;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 44
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/headless/a;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->isFinishing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 57
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->l:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 50
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->m:Ljava/lang/String;

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->n:Ljava/lang/String;

    .line 53
    new-instance v0, Lcom/google/android/gms/games/ui/headless/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/headless/matches/a;-><init>(Lcom/google/android/gms/games/ui/headless/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->k:Lcom/google/android/gms/games/ui/headless/matches/a;

    .line 55
    sget v0, Lcom/google/android/gms/p;->iN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->setTitle(I)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->l:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/matches/HeadlessPublicInvitationActivity;->a(Ljava/lang/CharSequence;)V

    goto :goto_0
.end method
