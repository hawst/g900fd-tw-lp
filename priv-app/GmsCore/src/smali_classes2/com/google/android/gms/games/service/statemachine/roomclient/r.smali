.class abstract Lcom/google/android/gms/games/service/statemachine/roomclient/r;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# instance fields
.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 22
    iput p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->e:I

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 34
    iget v0, p1, Landroid/os/Message;->what:I

    const/16 v1, 0x30

    if-ne v0, v1, :cond_0

    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Libjingle failure in state:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " no response after "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " seconds"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->g()V

    .line 39
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    const-string v2, "RoomServiceClientStateMachine"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->b()V

    .line 42
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->f:Z

    .line 44
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->b(Landroid/os/Message;)Z

    move-result v0

    goto :goto_0
.end method

.method abstract b(Landroid/os/Message;)Z
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 27
    const/16 v0, 0x30

    iget v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->e:I

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;->a(IILjava/util/concurrent/TimeUnit;)V

    .line 28
    return-void
.end method
