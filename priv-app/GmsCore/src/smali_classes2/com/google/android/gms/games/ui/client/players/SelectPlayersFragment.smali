.class public final Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;
.super Lcom/google/android/gms/games/ui/p;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/widget/bp;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/android/gms/games/ui/client/players/g;
.implements Lcom/google/android/gms/games/ui/cr;


# instance fields
.field private d:Lcom/google/android/gms/games/ui/client/a;

.field private e:Lcom/google/android/gms/games/ui/client/players/w;

.field private f:Lcom/google/android/gms/games/ui/client/players/p;

.field private g:Lcom/google/android/gms/games/Player;

.field private h:Lcom/google/android/gms/games/ui/d/ah;

.field private i:I

.field private j:Ljava/util/HashMap;

.field private k:Landroid/os/Bundle;

.field private l:Landroid/os/Handler;

.field private m:Lcom/google/android/gms/games/ui/client/players/q;

.field private n:Landroid/widget/FrameLayout;

.field private o:Landroid/widget/LinearLayout;

.field private p:Landroid/widget/ListView;

.field private q:Lcom/google/android/gms/games/ui/client/players/a;

.field private r:Landroid/view/View;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/MenuItem;

.field private u:Landroid/widget/ProgressBar;

.field private v:Z

.field private w:Landroid/view/LayoutInflater;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 140
    sget v0, Lcom/google/android/gms/l;->bE:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/p;-><init>(I)V

    .line 141
    return-void
.end method

.method private D()V
    .locals 4

    .prologue
    .line 307
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->getCount()I

    move-result v1

    .line 310
    sget v0, Lcom/google/android/gms/k;->s:I

    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    .line 311
    if-le v1, v0, :cond_0

    .line 316
    :goto_0
    sget v1, Lcom/google/android/gms/g;->ay:I

    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 317
    sget v3, Lcom/google/android/gms/g;->az:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 319
    add-float v3, v1, v2

    int-to-float v0, v0

    mul-float/2addr v0, v3

    .line 323
    add-float/2addr v1, v2

    const/high16 v2, 0x40800000    # 4.0f

    div-float/2addr v1, v2

    add-float/2addr v0, v1

    .line 325
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    invoke-virtual {v1}, Landroid/widget/ListView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 326
    float-to-int v0, v0

    iput v0, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 328
    return-void

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private E()V
    .locals 1

    .prologue
    .line 857
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->d()V

    .line 858
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->notifyDataSetChanged()V

    .line 859
    return-void
.end method

.method private F()Z
    .locals 2

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ah;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/2addr v0, v1

    .line 868
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private G()V
    .locals 3

    .prologue
    .line 963
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 966
    const-string v0, "SelectPlayersFrag"

    const-string v1, "doSearch: no room to add more players!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 983
    :goto_0
    return-void

    .line 970
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 971
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-nez v1, :cond_1

    .line 972
    const-string v0, "SelectPlayersFrag"

    const-string v1, "GoogleApiClient not connected (yet); ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 979
    :cond_1
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/a;->Q()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 982
    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/players/q;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/FrameLayout;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->n:Landroid/widget/FrameLayout;

    return-object v0
.end method

.method private b(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 1011
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 1012
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1013
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/util/Pair;

    .line 1014
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerEntity;->q()Lcom/google/android/gms/games/Player;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1015
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1018
    :cond_0
    const-string v0, "savedStateSelectedPlayers"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1019
    const-string v0, "savedStatedPlayerSources"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->putIntegerArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 1020
    const-string v0, "savedStateNumOfAutoMatchPlayers"

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 1021
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    if-eqz v0, :cond_1

    .line 1022
    const-string v0, "savedStateSearchNearbyPlayers"

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/q;->p()Z

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 1028
    :goto_1
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    .line 1029
    return-void

    .line 1025
    :cond_1
    const-string v0, "savedStateSearchNearbyPlayers"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_1
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/LinearLayout;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->o:Landroid/widget/LinearLayout;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/a;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Lcom/google/android/gms/games/ui/client/players/a;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)Landroid/widget/ListView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    return-object v0
.end method


# virtual methods
.method public final A()V
    .locals 2

    .prologue
    .line 1033
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v:Z

    .line 1034
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1035
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1037
    :cond_0
    return-void
.end method

.method public final B()V
    .locals 2

    .prologue
    .line 1041
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v:Z

    .line 1042
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    if-eqz v0, :cond_0

    .line 1043
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 1045
    :cond_0
    return-void
.end method

.method final C()V
    .locals 2

    .prologue
    .line 1048
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->o()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Z)V

    .line 1050
    return-void

    .line 1048
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final I_()V
    .locals 2

    .prologue
    .line 839
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    if-nez v0, :cond_0

    .line 840
    const-string v0, "SelectPlayersFrag"

    const-string v1, "onRemoveAutoMatchPlayer: no auto-match players to remove!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 853
    :goto_0
    return-void

    .line 844
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    .line 849
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->b()V

    .line 852
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    goto :goto_0
.end method

.method public final J_()Z
    .locals 1

    .prologue
    .line 932
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->G()V

    .line 933
    const/4 v0, 0x0

    return v0
.end method

.method public final a()V
    .locals 3

    .prologue
    .line 943
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->gf:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 944
    const-string v1, ""

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 946
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Landroid/os/Bundle;)V

    .line 948
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/ui/client/players/q;->c(ZZ)V

    .line 952
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    .line 953
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 386
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Player;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:Lcom/google/android/gms/games/Player;

    .line 387
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_0

    .line 389
    const-string v0, "SelectPlayersFrag"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 396
    :goto_0
    return-void

    .line 395
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 817
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 818
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0, v1, v1}, Lcom/google/android/gms/games/ui/client/players/q;->c(ZZ)V

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 826
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Ljava/lang/String;)V

    .line 829
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    .line 830
    return-void
.end method

.method final a(Lcom/google/android/gms/games/Player;I)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 587
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v3

    .line 588
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 595
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:Lcom/google/android/gms/games/Player;

    if-nez v0, :cond_1

    .line 596
    const-string v0, "SelectPlayersFrag"

    const-string v1, "don\'t have mCurrentPlayer yet"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 619
    :goto_1
    return v2

    :cond_0
    move v0, v2

    .line 588
    goto :goto_0

    .line 599
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->g:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 600
    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "ignoring current player "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 607
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/d/ah;->a(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    iget-object v2, v0, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    :cond_3
    :goto_2
    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/d/ah;->a(Ljava/lang/Object;)Z

    move-result v2

    .line 608
    if-nez v2, :cond_7

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 618
    :goto_3
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v4

    if-eqz v2, :cond_8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v2, v0, v3, v4}, Lcom/google/android/gms/games/ui/client/players/a;->a(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V

    :cond_4
    :goto_4
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->E()V

    move v2, v1

    .line 619
    goto :goto_1

    .line 607
    :cond_5
    iget v4, v0, Lcom/google/android/gms/games/ui/d/ah;->b:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_6

    iget-object v4, v0, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    iget v5, v0, Lcom/google/android/gms/games/ui/d/ah;->b:I

    if-lt v4, v5, :cond_6

    move v2, v1

    :cond_6
    if-nez v2, :cond_3

    iget-object v2, v0, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v2, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 611
    :cond_7
    new-instance v4, Landroid/util/Pair;

    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-direct {v4, v0, v5}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 613
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_3

    .line 618
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/client/players/a;->b(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Ljava/lang/String;)V

    goto :goto_4
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 1059
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/a;->a(I)V

    .line 1060
    return-void
.end method

.method final b(Lcom/google/android/gms/games/Player;I)V
    .locals 3

    .prologue
    .line 634
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->F()Z

    move-result v0

    if-nez v0, :cond_1

    .line 637
    const-string v0, "SelectPlayersFrag"

    const-string v1, "addPlayer: no room to add more players!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 655
    :cond_0
    :goto_0
    return-void

    .line 642
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    .line 643
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/d/ah;->a(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 644
    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addPlayer: ignoring already-selected player "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 652
    :cond_2
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/Player;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/d/ah;->a(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 252
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->onActivityCreated(Landroid/os/Bundle;)V

    .line 254
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/a;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/players/w;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/w;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    instance-of v0, v0, Lcom/google/android/gms/games/ui/client/players/p;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/p;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Lcom/google/android/gms/games/ui/client/players/p;

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->supportInvalidateOptionsMenu()V

    .line 265
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v0

    .line 268
    new-instance v1, Lcom/google/android/gms/games/ui/client/players/a;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/players/a;-><init>(Lcom/google/android/gms/games/ui/p;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 270
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    new-instance v2, Lcom/google/android/gms/games/ui/client/players/n;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/ui/client/players/n;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/f;)V

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r:Landroid/view/View;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Landroid/view/View;I)V

    .line 289
    new-instance v1, Lcom/google/android/gms/games/ui/d/ah;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/ui/d/ah;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    .line 290
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    .line 291
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    .line 295
    if-eqz p1, :cond_0

    .line 296
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    .line 299
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->D()V

    .line 300
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v2, -0x1

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 406
    packed-switch p1, :pswitch_data_0

    .line 453
    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onActivityResult: unhandled request code: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/p;->onActivityResult(IILandroid/content/Intent;)V

    .line 455
    :cond_0
    :goto_0
    return-void

    .line 411
    :pswitch_0
    if-ne p2, v2, :cond_0

    .line 413
    invoke-virtual {p3}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    .line 419
    const-string v2, "player_search_results"

    invoke-virtual {p3, v2}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v2

    .line 422
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 423
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_2

    .line 425
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 427
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 428
    if-eqz v0, :cond_0

    .line 429
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;I)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 422
    goto :goto_1

    .line 432
    :cond_2
    const-string v0, "SelectPlayersFrag"

    const-string v1, "REQUEST_PLAYER_SEARCH: RESULT_OK, but empty result"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 440
    :pswitch_1
    if-ne p2, v2, :cond_0

    .line 443
    invoke-static {p3}, Lcom/google/android/gms/common/audience/a/i;->a(Landroid/content/Intent;)Lcom/google/android/gms/common/audience/a/j;

    move-result-object v3

    .line 444
    invoke-interface {v3}, Lcom/google/android/gms/common/audience/a/j;->i()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_5

    move v2, v0

    .line 445
    :goto_2
    invoke-interface {v3}, Lcom/google/android/gms/common/audience/a/j;->g()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_3

    invoke-interface {v3}, Lcom/google/android/gms/common/audience/a/j;->h()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_4

    :cond_3
    move v1, v0

    .line 447
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/ui/client/players/q;->b(ZZ)V

    goto :goto_0

    :cond_5
    move v2, v1

    .line 444
    goto :goto_2

    .line 406
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final onCreateOptionsMenu(Landroid/view/Menu;Landroid/view/MenuInflater;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 881
    sget v0, Lcom/google/android/gms/m;->f:I

    invoke-virtual {p2, v0, p1}, Landroid/view/MenuInflater;->inflate(ILandroid/view/Menu;)V

    .line 885
    sget v0, Lcom/google/android/gms/j;->lW:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t:Landroid/view/MenuItem;

    .line 886
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t:Landroid/view/MenuItem;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 888
    sget v0, Lcom/google/android/gms/j;->lU:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v2

    .line 889
    invoke-static {v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 892
    sget v0, Lcom/google/android/gms/j;->lS:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->findItem(I)Landroid/view/MenuItem;

    move-result-object v0

    .line 893
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 894
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    if-eqz v3, :cond_0

    .line 895
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v3}, Lcom/google/android/gms/games/ui/client/players/w;->W()Z

    move-result v3

    invoke-interface {v0, v3}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 898
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->w:Landroid/view/LayoutInflater;

    sget v3, Lcom/google/android/gms/l;->c:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v3

    .line 900
    new-instance v0, Landroid/view/ViewGroup$LayoutParams;

    const/4 v4, -0x2

    const/4 v5, -0x1

    invoke-direct {v0, v4, v5}, Landroid/view/ViewGroup$LayoutParams;-><init>(II)V

    .line 903
    invoke-virtual {v3, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 904
    sget v0, Lcom/google/android/gms/j;->pR:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    .line 905
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u:Landroid/widget/ProgressBar;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v:Z

    if-eqz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-virtual {v4, v0}, Landroid/widget/ProgressBar;->setVisibility(I)V

    .line 907
    invoke-static {v2, v3}, Landroid/support/v4/view/ai;->a(Landroid/view/MenuItem;Landroid/view/View;)Landroid/view/MenuItem;

    .line 908
    return-void

    .line 905
    :cond_1
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 146
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/p;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v2

    .line 148
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->l:Landroid/os/Handler;

    .line 149
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->w:Landroid/view/LayoutInflater;

    .line 150
    sget v0, Lcom/google/android/gms/j;->cK:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    .line 152
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    invoke-virtual {v0, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->p:Landroid/widget/ListView;

    invoke-virtual {v0, p0}, Landroid/widget/ListView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 161
    sget v0, Lcom/google/android/gms/j;->rc:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->n:Landroid/widget/FrameLayout;

    .line 166
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/q;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/client/players/q;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->d()V

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/client/players/q;->a(Landroid/support/v4/widget/bp;)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getChildFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v0

    .line 171
    sget v1, Lcom/google/android/gms/j;->rd:I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0, v1, v3}, Landroid/support/v4/app/aj;->b(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/aj;

    .line 172
    invoke-virtual {v0}, Landroid/support/v4/app/aj;->a()I

    .line 173
    sget v0, Lcom/google/android/gms/j;->gS:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->o:Landroid/widget/LinearLayout;

    .line 175
    sget v0, Lcom/google/android/gms/j;->rb:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->s:Landroid/widget/TextView;

    .line 177
    sget v0, Lcom/google/android/gms/j;->gT:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 181
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v3, Lcom/google/android/gms/e;->b:I

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v1

    .line 183
    if-nez v1, :cond_0

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->s:Landroid/widget/TextView;

    const/16 v3, 0x8

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    :cond_0
    sget v1, Lcom/google/android/gms/l;->aC:I

    const/4 v3, 0x0

    invoke-virtual {p1, v1, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r:Landroid/view/View;

    .line 189
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->gf:I

    invoke-virtual {v1, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 190
    new-instance v3, Lcom/google/android/gms/games/ui/client/players/k;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/ui/client/players/k;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->addTextChangedListener(Landroid/text/TextWatcher;)V

    .line 225
    new-instance v3, Lcom/google/android/gms/games/ui/client/players/l;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/ui/client/players/l;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;)V

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 237
    new-instance v3, Lcom/google/android/gms/games/ui/client/players/m;

    invoke-direct {v3, p0, v1}, Lcom/google/android/gms/games/ui/client/players/m;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/widget/TextView;)V

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 245
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->setHasOptionsMenu(Z)V

    .line 247
    return-object v2
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 913
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 914
    sget v2, Lcom/google/android/gms/j;->lV:I

    if-ne v1, v2, :cond_0

    .line 915
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->A()V

    .line 916
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a()V

    .line 925
    :goto_0
    return v0

    .line 918
    :cond_0
    sget v2, Lcom/google/android/gms/j;->lW:I

    if-ne v1, v2, :cond_1

    .line 919
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->G()V

    goto :goto_0

    .line 921
    :cond_1
    sget v2, Lcom/google/android/gms/j;->lS:I

    if-ne v1, v2, :cond_2

    .line 922
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->d(Landroid/content/Context;)V

    goto :goto_0

    .line 925
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onResume()V
    .locals 0

    .prologue
    .line 370
    invoke-super {p0}, Lcom/google/android/gms/games/ui/p;->onResume()V

    .line 374
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    .line 375
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 379
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/p;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 380
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Landroid/os/Bundle;)V

    .line 381
    return-void
.end method

.method public final onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 6

    .prologue
    .line 468
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->cK:I

    if-ne v0, v1, :cond_1

    .line 473
    invoke-virtual {p2}, Landroid/view/MotionEvent;->getAction()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 474
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->E()V

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ah;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 481
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->r:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->gf:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 483
    invoke-virtual {v0}, Landroid/widget/TextView;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 486
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->l:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/games/ui/client/players/o;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/client/players/o;-><init>(Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/widget/TextView;)V

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 516
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 512
    :cond_1
    const-string v0, "SelectPlayersFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onTouch: unexpected view "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final t()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    if-eqz v0, :cond_3

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    iput v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->c()V

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    const-string v1, "savedStateSelectedPlayers"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 341
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    const-string v1, "savedStatedPlayerSources"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getIntegerArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v5

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    const-string v1, "savedStateNumOfAutoMatchPlayers"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 344
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 347
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_1
    if-ge v3, v7, :cond_1

    .line 348
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(Lcom/google/android/gms/games/Player;I)V

    .line 347
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 344
    goto :goto_0

    :cond_1
    move v0, v2

    .line 352
    :goto_2
    if-ge v0, v6, :cond_2

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->w()V

    .line 352
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 357
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    const-string v1, "savedStateSearchNearbyPlayers"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    .line 358
    if-eqz v0, :cond_4

    .line 359
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Z)V

    .line 364
    :cond_3
    :goto_3
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->k:Landroid/os/Bundle;

    .line 365
    return-void

    .line 361
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->l()V

    goto :goto_3
.end method

.method final u()Lcom/google/android/gms/games/ui/d/ah;
    .locals 1

    .prologue
    .line 459
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    return-object v0
.end method

.method final v()I
    .locals 1

    .prologue
    .line 463
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    return v0
.end method

.method final w()V
    .locals 2

    .prologue
    .line 664
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->F()Z

    move-result v0

    if-nez v0, :cond_0

    .line 667
    const-string v0, "SelectPlayersFrag"

    const-string v1, "addAutoMatchPlayer: no room to add more players!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 683
    :goto_0
    return-void

    .line 671
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    .line 674
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->a()V

    .line 677
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    .line 682
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->E()V

    goto :goto_0
.end method

.method final x()V
    .locals 2

    .prologue
    .line 695
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 696
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    if-nez v0, :cond_1

    .line 697
    const-string v0, "SelectPlayersFrag"

    const-string v1, "removeAutoMatchPlayer: nobody to remove!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 713
    :goto_1
    return-void

    .line 695
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 701
    :cond_1
    iget v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    .line 704
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->q:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/a;->b()V

    .line 707
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->y()V

    .line 712
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->E()V

    goto :goto_1
.end method

.method final y()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 722
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ah;->a()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/2addr v3, v0

    .line 724
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 726
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->Y()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 733
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->z()I

    move-result v5

    .line 734
    if-ltz v5, :cond_4

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 736
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v0

    if-ne v5, v0, :cond_6

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->T()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    .line 739
    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v5}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    .line 740
    if-ne v5, v0, :cond_5

    .line 741
    sget v5, Lcom/google/android/gms/p;->lj:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v2

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 754
    :goto_1
    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->d:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/games/ui/client/a;->a(Ljava/lang/CharSequence;)V

    .line 761
    :cond_0
    add-int/lit8 v0, v3, 0x1

    .line 762
    sget v3, Lcom/google/android/gms/n;->j:I

    new-array v5, v1, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v2

    invoke-virtual {v4, v3, v0, v5}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 765
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->s:Landroid/widget/TextView;

    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 770
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v0

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    sub-int v3, v0, v3

    .line 771
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    iget-object v0, v4, Lcom/google/android/gms/games/ui/d/ah;->a:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ge v3, v0, :cond_1

    const/4 v0, -0x1

    if-ne v3, v0, :cond_8

    :cond_1
    move v0, v1

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    iput v3, v4, Lcom/google/android/gms/games/ui/d/ah;->b:I

    .line 774
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->F()Z

    move-result v0

    .line 775
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/client/players/q;->b(Z)V

    .line 777
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t:Landroid/view/MenuItem;

    if-eqz v3, :cond_2

    .line 781
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t:Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setVisible(Z)Landroid/view/MenuItem;

    .line 782
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t:Landroid/view/MenuItem;

    invoke-interface {v3, v0}, Landroid/view/MenuItem;->setEnabled(Z)Landroid/view/MenuItem;

    .line 788
    :cond_2
    if-nez v0, :cond_9

    iget v3, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    if-lez v3, :cond_9

    .line 789
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/games/ui/client/players/q;->d(ZZ)V

    .line 792
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Lcom/google/android/gms/games/ui/client/players/p;

    if-eqz v0, :cond_3

    .line 793
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->f:Lcom/google/android/gms/games/ui/client/players/p;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->j:Ljava/util/HashMap;

    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/ui/client/players/p;->a(Ljava/util/HashMap;I)V

    .line 797
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->j()V

    .line 802
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->m:Lcom/google/android/gms/games/ui/client/players/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->m()V

    .line 804
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->D()V

    .line 805
    return-void

    :cond_4
    move v0, v2

    .line 734
    goto/16 :goto_0

    .line 744
    :cond_5
    sget v6, Lcom/google/android/gms/p;->lk:I

    const/4 v7, 0x2

    new-array v7, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v2

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v7, v1

    invoke-virtual {v4, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 747
    :cond_6
    if-lez v5, :cond_7

    .line 748
    sget v0, Lcom/google/android/gms/n;->l:I

    new-array v6, v1, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v2

    invoke-virtual {v4, v0, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    .line 752
    :cond_7
    sget v0, Lcom/google/android/gms/p;->li:I

    invoke-virtual {v4, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_1

    :cond_8
    move v0, v2

    .line 771
    goto :goto_2

    :cond_9
    move v1, v2

    .line 788
    goto :goto_3
.end method

.method final z()I
    .locals 2

    .prologue
    .line 872
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->h:Lcom/google/android/gms/games/ui/d/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/d/ah;->a()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->i:I

    add-int/2addr v0, v1

    .line 873
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/client/players/w;->U()I

    move-result v1

    sub-int v0, v1, v0

    return v0
.end method
