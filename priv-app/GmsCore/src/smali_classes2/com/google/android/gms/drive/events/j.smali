.class final Lcom/google/android/gms/drive/events/j;
.super Lcom/google/android/gms/drive/internal/ce;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/events/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/events/g;)V
    .locals 0

    .prologue
    .line 139
    iput-object p1, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/ce;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/internal/OnEventResponse;)V
    .locals 4

    .prologue
    .line 142
    iget-object v1, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    monitor-enter v1

    .line 143
    :try_start_0
    const-string v0, "DriveEventService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onEvent: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    invoke-static {v0}, Lcom/google/android/gms/drive/events/g;->a(Lcom/google/android/gms/drive/events/g;)V

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    if-eqz v0, :cond_0

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    iget-object v0, v0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    invoke-static {v0, p1}, Lcom/google/android/gms/drive/events/i;->a(Lcom/google/android/gms/drive/events/i;Lcom/google/android/gms/drive/internal/OnEventResponse;)Landroid/os/Message;

    move-result-object v0

    .line 147
    iget-object v2, p0, Lcom/google/android/gms/drive/events/j;->a:Lcom/google/android/gms/drive/events/g;

    iget-object v2, v2, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/drive/events/i;->sendMessage(Landroid/os/Message;)Z

    .line 153
    :goto_0
    monitor-exit v1

    return-void

    .line 151
    :cond_0
    const-string v0, "DriveEventService"

    const-string v2, "Receiving event before initialize is completed."

    invoke-static {v0, v2}, Lcom/google/android/gms/drive/internal/be;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
