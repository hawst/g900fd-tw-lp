.class final Lcom/google/android/gms/games/internal/ay;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 1329
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 1330
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ay;->a:Lcom/google/android/gms/common/api/m;

    .line 1331
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 1335
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ay;->a:Lcom/google/android/gms/common/api/m;

    invoke-static {p1}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 1336
    return-void
.end method
