.class public final Lcom/google/android/gms/people/model/k;
.super Lcom/google/android/gms/people/model/f;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/people/internal/a/d;

.field private final c:Lcom/google/android/gms/people/internal/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/people/internal/a/d;Lcom/google/android/gms/people/internal/a/c;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/model/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/people/model/k;->b:Lcom/google/android/gms/people/internal/a/d;

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/people/model/k;->c:Lcom/google/android/gms/people/internal/a/c;

    .line 27
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/people/model/k;->b(I)Lcom/google/android/gms/people/model/j;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/gms/people/model/j;
    .locals 6

    .prologue
    .line 31
    new-instance v0, Lcom/google/android/gms/people/internal/bc;

    iget-object v1, p0, Lcom/google/android/gms/people/model/k;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {p0}, Lcom/google/android/gms/people/model/k;->e()Landroid/os/Bundle;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/people/model/k;->b:Lcom/google/android/gms/people/internal/a/d;

    iget-object v5, p0, Lcom/google/android/gms/people/model/k;->c:Lcom/google/android/gms/people/internal/a/c;

    move v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/internal/bc;-><init>(Lcom/google/android/gms/common/data/DataHolder;ILandroid/os/Bundle;Lcom/google/android/gms/people/internal/a/d;Lcom/google/android/gms/people/internal/a/c;)V

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 36
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "People:size="

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/people/model/k;->c()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
