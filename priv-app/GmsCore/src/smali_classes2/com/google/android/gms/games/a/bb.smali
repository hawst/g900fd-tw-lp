.class Lcom/google/android/gms/games/a/bb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# static fields
.field private static final a:Ljava/util/concurrent/atomic/AtomicInteger;


# instance fields
.field private final b:Ljava/lang/String;

.field private final c:Ljava/util/concurrent/locks/ReentrantLock;

.field private final d:Ljava/util/ArrayList;

.field private final e:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/games/a/bb;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 56
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    .line 58
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/locks/ReentrantLock;

    iput-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    .line 59
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    .line 60
    sget-object v0, Lcom/google/android/gms/games/a/bb;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->getAndAdd(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/a/bb;->e:I

    .line 62
    if-eqz p3, :cond_0

    .line 63
    invoke-virtual {p3}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    iget-object v0, p3, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    iget-object v0, p3, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v0, p3, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-le v0, v1, :cond_0

    iget-object v0, p3, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    iget-object v1, p3, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bb;

    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/bb;->a(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/games/a/bb;)V

    .line 65
    :cond_0
    return-void

    .line 63
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->lock()V

    .line 187
    return-void

    .line 185
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/games/a/bb;)V
    .locals 3

    .prologue
    .line 97
    iget v0, p0, Lcom/google/android/gms/games/a/bb;->e:I

    iget v1, p1, Lcom/google/android/gms/games/a/bb;->e:I

    if-gt v0, v1, :cond_0

    .line 98
    const-string v0, "Lockable"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid lock ordering: locking "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/games/a/bb;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") after "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/gms/games/a/bb;->e:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Invalid lock ordering!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 103
    :cond_0
    return-void
.end method

.method private static a(Ljava/util/List;)V
    .locals 4

    .prologue
    .line 86
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v3

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_1

    .line 87
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bb;

    .line 88
    if-lez v2, :cond_0

    .line 89
    add-int/lit8 v1, v2, -0x1

    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/bb;

    .line 90
    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/bb;->a(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/games/a/bb;)V

    .line 92
    :cond_0
    invoke-direct {v0}, Lcom/google/android/gms/games/a/bb;->a()V

    .line 86
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 94
    :cond_1
    return-void
.end method

.method public static varargs a([Lcom/google/android/gms/games/a/bb;)V
    .locals 1

    .prologue
    .line 78
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 79
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 80
    invoke-static {v0}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 81
    invoke-static {v0}, Lcom/google/android/gms/games/a/bb;->a(Ljava/util/List;)V

    .line 82
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 196
    return-void
.end method

.method private static b(Ljava/util/List;)V
    .locals 3

    .prologue
    .line 120
    const/4 v0, 0x0

    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    .line 121
    invoke-interface {p0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bb;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/bb;->b()V

    .line 120
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 123
    :cond_0
    return-void
.end method

.method public static varargs b([Lcom/google/android/gms/games/a/bb;)V
    .locals 1

    .prologue
    .line 113
    if-eqz p0, :cond_0

    array-length v0, p0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 114
    invoke-static {p0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    .line 115
    invoke-static {v0}, Lcom/google/android/gms/games/a/bb;->b(Ljava/util/List;)V

    .line 116
    return-void

    .line 113
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/games/a/bb;)I
    .locals 2

    .prologue
    .line 235
    iget v0, p0, Lcom/google/android/gms/games/a/bb;->e:I

    iget v1, p1, Lcom/google/android/gms/games/a/bb;->e:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public synthetic compareTo(Ljava/lang/Object;)I
    .locals 1

    .prologue
    .line 21
    check-cast p1, Lcom/google/android/gms/games/a/bb;

    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/a/bb;->a(Lcom/google/android/gms/games/a/bb;)I

    move-result v0

    return v0
.end method

.method public d()V
    .locals 3

    .prologue
    .line 210
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Lock "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not held by thread!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 211
    return-void
.end method

.method public e()Z
    .locals 1

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->c:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-virtual {v0}, Ljava/util/concurrent/locks/ReentrantLock;->isHeldByCurrentThread()Z

    move-result v0

    return v0
.end method

.method public f()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 168
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->a()V

    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_2

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bb;

    .line 172
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v3

    if-nez v3, :cond_1

    move v3, v1

    :goto_2
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Lock still held: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_1

    :cond_0
    move v0, v2

    .line 167
    goto :goto_0

    :cond_1
    move v3, v2

    .line 172
    goto :goto_2

    .line 176
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    .line 177
    return-void

    .line 176
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    throw v0
.end method

.method public g()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 154
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->a()V

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_1
    if-ge v1, v2, :cond_1

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/bb;

    .line 158
    invoke-virtual {v0}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Lock not held: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/games/a/bb;->b:Ljava/lang/String;

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 156
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_0
    move v0, v1

    .line 153
    goto :goto_0

    .line 162
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    .line 163
    return-void

    .line 162
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    throw v0
.end method

.method public h()V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 144
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->a()V

    .line 146
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/games/a/bb;->b(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    .line 149
    return-void

    .line 143
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    throw v0
.end method

.method public i()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 131
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->a()V

    .line 133
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bb;->d:Ljava/util/ArrayList;

    invoke-static {v0}, Lcom/google/android/gms/games/a/bb;->a(Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 135
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    .line 136
    return-void

    .line 130
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 135
    :catchall_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/games/a/bb;->b()V

    throw v0
.end method
