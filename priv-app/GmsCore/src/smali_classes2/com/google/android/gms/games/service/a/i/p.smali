.class public final Lcom/google/android/gms/games/service/a/i/p;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:Z

.field private final g:Lcom/google/android/gms/games/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i/p;->c:Lcom/google/android/gms/games/internal/dr;

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/i/p;->d:Ljava/lang/String;

    .line 27
    iput p4, p0, Lcom/google/android/gms/games/service/a/i/p;->e:I

    .line 28
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/a/i/p;->f:Z

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/p;->g:Lcom/google/android/gms/games/a/au;

    .line 30
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/p;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->e(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 41
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/p;->g:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/i/p;->d:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/games/service/a/i/p;->e:I

    iget-boolean v3, p0, Lcom/google/android/gms/games/service/a/i/p;->f:Z

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
