.class public final Lcom/google/android/gms/common/server/y;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 189
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.plus.service.default.INTENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "isLoggingIntent"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.plus.service.DefaultIntentService"

    invoke-virtual {v0, p1, v1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/server/y;-><init>(Landroid/content/Intent;)V

    .line 192
    return-void
.end method

.method public constructor <init>(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p1, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    .line 196
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "startView"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 223
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ActionTargetEntity;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "actionTarget"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 291
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/plus/service/v1whitelisted/models/ClientActionDataEntity;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 276
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "clientActionData"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 277
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "callingPackage"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 201
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "startView"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "endView"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 234
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "accountName"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 212
    return-object p0
.end method

.method public final c()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "endView"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 245
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/common/server/y;
    .locals 2

    .prologue
    .line 310
    if-eqz p1, :cond_0

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "plusPageId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 313
    :cond_0
    return-object p0
.end method

.method public final d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    const-string v1, "action"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    return-object v0
.end method

.method public final e()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 322
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 323
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->b()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 324
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    if-nez v0, :cond_1

    .line 325
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->c()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 330
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/y;->a:Landroid/content/Intent;

    return-object v0

    .line 326
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->c()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    if-nez v0, :cond_0

    .line 327
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/y;->d()Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method
