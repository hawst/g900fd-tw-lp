.class public final Lcom/google/android/gms/drive/events/ChangeEvent;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/drive/events/ResourceEvent;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/drive/events/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/events/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/events/ChangeEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;I)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput p1, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->a:I

    .line 60
    iput-object p2, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->b:Lcom/google/android/gms/drive/DriveId;

    .line 61
    iput p3, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->c:I

    .line 62
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;I)V
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/events/ChangeEvent;-><init>(ILcom/google/android/gms/drive/DriveId;I)V

    .line 69
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x1

    return v0
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 5

    .prologue
    .line 108
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v1, "ChangeEvent [id=%s,changeFlags=%x]"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    iget v4, p0, Lcom/google/android/gms/drive/events/ChangeEvent;->c:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v0, v1, v2}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/events/d;->a(Lcom/google/android/gms/drive/events/ChangeEvent;Landroid/os/Parcel;I)V

    .line 79
    return-void
.end method
