.class public final Lcom/google/android/gms/auth/trustagent/bk;
.super Landroid/app/DialogFragment;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/bn;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 521
    invoke-direct {p0}, Landroid/app/DialogFragment;-><init>()V

    .line 523
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bk;
    .locals 3

    .prologue
    .line 534
    new-instance v0, Lcom/google/android/gms/auth/trustagent/bk;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/bk;-><init>()V

    .line 535
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 536
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_key"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_title"

    invoke-virtual {v1, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v2, "auth_trust_agent_pref_trusted_places_preference_summary"

    invoke-virtual {v1, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bk;->setArguments(Landroid/os/Bundle;)V

    .line 540
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/bk;)Lcom/google/android/gms/auth/trustagent/bn;
    .locals 1

    .prologue
    .line 521
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bk;->a:Lcom/google/android/gms/auth/trustagent/bn;

    return-object v0
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 0

    .prologue
    .line 545
    invoke-super {p0, p1}, Landroid/app/DialogFragment;->onAttach(Landroid/app/Activity;)V

    .line 546
    check-cast p1, Lcom/google/android/gms/auth/trustagent/bn;

    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/bk;->a:Lcom/google/android/gms/auth/trustagent/bn;

    .line 547
    return-void
.end method

.method public final onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 7

    .prologue
    .line 551
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bk;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_places_preference_key"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 552
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bk;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trusted_places_preference_title"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 553
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bk;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v3, "auth_trust_agent_pref_trusted_places_preference_summary"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 554
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-direct {v4, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 555
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bk;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    .line 556
    sget v5, Lcom/google/android/gms/l;->fx:I

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v5

    .line 557
    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 558
    sget v0, Lcom/google/android/gms/j;->tr:I

    invoke-virtual {v5, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    .line 560
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 561
    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 566
    :goto_0
    sget v1, Lcom/google/android/gms/j;->tq:I

    invoke-virtual {v5, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/EditText;

    .line 568
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 569
    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 574
    :goto_1
    sget v3, Lcom/google/android/gms/p;->df:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/trustagent/bk;->getString(I)Ljava/lang/String;

    move-result-object v3

    new-instance v5, Lcom/google/android/gms/auth/trustagent/bl;

    invoke-direct {v5, p0, v0, v2, v1}, Lcom/google/android/gms/auth/trustagent/bl;-><init>(Lcom/google/android/gms/auth/trustagent/bk;Landroid/widget/EditText;Ljava/lang/String;Landroid/widget/EditText;)V

    invoke-virtual {v4, v3, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 586
    sget v0, Lcom/google/android/gms/p;->dd:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bk;->getString(I)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/auth/trustagent/bm;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/bm;-><init>(Lcom/google/android/gms/auth/trustagent/bk;)V

    invoke-virtual {v4, v0, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 593
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0

    .line 563
    :cond_0
    sget v1, Lcom/google/android/gms/p;->dh:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bk;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 571
    :cond_1
    sget v3, Lcom/google/android/gms/p;->dg:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/auth/trustagent/bk;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Landroid/widget/EditText;->setHint(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method
