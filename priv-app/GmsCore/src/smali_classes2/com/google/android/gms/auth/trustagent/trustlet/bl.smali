.class final Lcom/google/android/gms/auth/trustagent/trustlet/bl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/w;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V
    .locals 0

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->f:Z

    if-eqz v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->f:Z

    .line 91
    const-string v0, "Coffee - TrustletManager"

    const-string v1, "Device active, revalidating trust."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    .line 94
    :cond_0
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 98
    const-string v0, "Coffee - TrustletManager"

    const-string v1, "Device became inactive, revoking trust."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->f:Z

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    .line 101
    return-void
.end method
