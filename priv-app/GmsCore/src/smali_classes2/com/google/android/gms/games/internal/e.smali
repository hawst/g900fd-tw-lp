.class abstract Lcom/google/android/gms/games/internal/e;
.super Lcom/google/android/gms/games/internal/g;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1898
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/g;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 1895
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/e;->a:Ljava/util/ArrayList;

    .line 1900
    const/4 v0, 0x0

    array-length v1, p2

    :goto_0
    if-ge v0, v1, :cond_0

    .line 1901
    iget-object v2, p0, Lcom/google/android/gms/games/internal/e;->a:Ljava/util/ArrayList;

    aget-object v3, p2, v0

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1900
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1903
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/multiplayer/realtime/f;Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1907
    iget-object v0, p0, Lcom/google/android/gms/games/internal/e;->a:Ljava/util/ArrayList;

    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/internal/e;->b(Lcom/google/android/gms/games/multiplayer/realtime/f;Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 1908
    return-void
.end method

.method protected abstract b(Lcom/google/android/gms/games/multiplayer/realtime/f;Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
.end method
