.class final Lcom/google/android/gms/common/widget/a/k;
.super Lcom/google/android/gms/common/widget/a/g;
.source "SourceFile"


# instance fields
.field private final l:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 206
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/g;-><init>(Landroid/view/View;)V

    .line 207
    const v0, 0x1020016

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/k;->l:Landroid/widget/TextView;

    .line 208
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusable(Z)V

    .line 209
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setFocusableInTouchMode(Z)V

    .line 210
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 2

    .prologue
    .line 214
    instance-of v0, p1, Lcom/google/android/gms/common/widget/a/i;

    if-nez v0, :cond_0

    .line 215
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "settingItem must be SettingsCategory"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 217
    :cond_0
    check-cast p1, Lcom/google/android/gms/common/widget/a/i;

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/k;->l:Landroid/widget/TextView;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/i;->a()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 219
    return-void
.end method
