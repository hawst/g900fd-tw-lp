.class final Lcom/google/android/gms/car/hr;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/o;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/gx;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 1789
    iput-object p1, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/gx;B)V
    .locals 0

    .prologue
    .line 1789
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/hr;-><init>(Lcom/google/android/gms/car/gx;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1872
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/fh;

    invoke-direct {v1}, Lcom/google/android/gms/car/fh;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fh;)Lcom/google/android/gms/car/fh;

    .line 1873
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->B(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/fh;->a()V

    .line 1874
    return-void
.end method

.method public final a(IILcom/google/android/c/b/br;)V
    .locals 2

    .prologue
    .line 1865
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/fu;

    invoke-direct {v1}, Lcom/google/android/gms/car/fu;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fu;)Lcom/google/android/gms/car/fu;

    .line 1866
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->A(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fu;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/car/fu;->a(IILcom/google/android/c/b/br;)V

    .line 1868
    return-void
.end method

.method public final a(II[Lcom/google/android/c/b/f;)V
    .locals 4

    .prologue
    .line 1802
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->u(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/bg;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1803
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/bg;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->v(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/aw;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/bg;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/bg;)Lcom/google/android/gms/car/bg;

    .line 1806
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->u(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/bg;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/car/bg;->a(II[Lcom/google/android/c/b/f;)V

    .line 1807
    return-void
.end method

.method public final a(Lcom/google/android/c/b/f;)V
    .locals 4

    .prologue
    .line 1812
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->u(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/bg;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1813
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/bg;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->v(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/aw;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/bg;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/bg;)Lcom/google/android/gms/car/bg;

    .line 1816
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->u(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/bg;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/c/b/f;)V

    .line 1817
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V
    .locals 9

    .prologue
    .line 1913
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->i(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 1914
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->d(Lcom/google/android/gms/car/gx;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1915
    const-string v0, "CAR.SERVICE"

    const-string v2, "car disconnected while service discovery"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1916
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1927
    :goto_0
    return-void

    .line 1918
    :cond_0
    monitor-exit v1

    .line 1919
    iget-object v8, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v0, Lcom/google/android/gms/car/CarInfoInternal;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/car/CarInfoInternal;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V

    invoke-static {v8, v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;

    .line 1922
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1923
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onCarInfo "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->p(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1925
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/qc;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->z(Lcom/google/android/gms/car/gx;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->p(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v4}, Lcom/google/android/gms/car/gx;->b(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/id;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/car/qc;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;Lcom/google/android/gms/car/id;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/qc;)Lcom/google/android/gms/car/qc;

    .line 1926
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->I(Lcom/google/android/gms/car/gx;)V

    goto :goto_0

    .line 1918
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;[I)V
    .locals 4

    .prologue
    .line 1854
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/br;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->z(Lcom/google/android/gms/car/gx;)Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/gms/car/qc;->g:Z

    if-eqz v3, :cond_0

    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3}, Lcom/google/android/gms/car/gx;->o(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/qc;

    move-result-object v3

    iget-boolean v3, v3, Lcom/google/android/gms/car/qc;->g:Z

    invoke-direct {v1, v2, p1, p2}, Lcom/google/android/gms/car/br;-><init>(Landroid/content/Context;Ljava/lang/String;[I)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/br;

    .line 1860
    return-void

    .line 1854
    :cond_0
    const-string p1, "SKIP_THIS_BLUETOOTH"

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;[Ljava/lang/String;[B)V
    .locals 3

    .prologue
    .line 1892
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->E(Lcom/google/android/gms/car/gx;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 1893
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->F(Lcom/google/android/gms/car/gx;)Ljava/util/HashMap;

    move-result-object v0

    new-instance v2, Landroid/util/Pair;

    invoke-direct {v2, p2, p3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-virtual {v0, p1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1895
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z[ILandroid/graphics/Point;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1835
    .line 1836
    array-length v2, p2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget v3, p2, v1

    .line 1837
    const/high16 v4, 0x10000

    if-ne v3, v4, :cond_2

    .line 1838
    const/4 v0, 0x1

    .line 1842
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v2, Lcom/google/android/gms/car/CarUiInfo;

    invoke-direct {v2, v0, p1}, Lcom/google/android/gms/car/CarUiInfo;-><init>(ZZ)V

    invoke-static {v1, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/CarUiInfo;)Lcom/google/android/gms/car/CarUiInfo;

    .line 1843
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1844
    const-string v0, "CAR.SERVICE"

    iget-object v1, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v1}, Lcom/google/android/gms/car/gx;->x(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/car/CarUiInfo;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1847
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/es;

    invoke-direct {v1}, Lcom/google/android/gms/car/es;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/es;)Lcom/google/android/gms/car/es;

    .line 1848
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->y(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/es;

    move-result-object v0

    iput-object p2, v0, Lcom/google/android/gms/car/es;->a:[I

    iput-object p3, v0, Lcom/google/android/gms/car/es;->b:Landroid/graphics/Point;

    .line 1849
    return-void

    .line 1836
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public final a([Lcom/google/android/c/b/cn;)V
    .locals 3

    .prologue
    .line 1821
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/gn;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/gn;-><init>(Lcom/google/android/gms/car/gx;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/gn;)Lcom/google/android/gms/car/gn;

    .line 1822
    array-length v0, p1

    new-array v1, v0, [I

    .line 1823
    const/4 v0, 0x0

    :goto_0
    array-length v2, p1

    if-ge v0, v2, :cond_0

    .line 1824
    aget-object v2, p1, v0

    iget v2, v2, Lcom/google/android/c/b/cn;->a:I

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/ba;->d(I)I

    move-result v2

    aput v2, v1, v0

    .line 1823
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1827
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->w(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/gn;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/gn;->a([I)V

    .line 1828
    return-void
.end method

.method public final a([Lcom/google/android/c/b/cz;)V
    .locals 6

    .prologue
    .line 1793
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->t(Lcom/google/android/gms/car/gx;)Landroid/graphics/Point;

    move-result-object v0

    .line 1794
    iget-object v1, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v2, Lcom/google/android/gms/car/ip;

    iget-object v3, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    iget-object v4, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    iget v5, v0, Landroid/graphics/Point;->x:I

    iget v0, v0, Landroid/graphics/Point;->y:I

    invoke-direct {v2, v3, v4, v5, v0}, Lcom/google/android/gms/car/ip;-><init>(Lcom/google/android/gms/car/is;Lcom/google/android/gms/car/gx;II)V

    invoke-static {v1, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/ip;

    .line 1796
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ip;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ip;->a([Lcom/google/android/c/b/cz;)V

    .line 1797
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 1878
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/ey;

    invoke-direct {v1}, Lcom/google/android/gms/car/ey;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ey;)Lcom/google/android/gms/car/ey;

    .line 1879
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->C(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ey;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ey;->a()V

    .line 1880
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1884
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/ga;

    iget-object v2, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/ga;-><init>(Lcom/google/android/gms/car/CarService;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/ga;)Lcom/google/android/gms/car/ga;

    .line 1885
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->D(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/ga;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ga;->a()V

    .line 1886
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 1900
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    new-instance v1, Lcom/google/android/gms/car/fx;

    invoke-direct {v1}, Lcom/google/android/gms/car/fx;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/fx;)Lcom/google/android/gms/car/fx;

    .line 1901
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->G(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/fx;

    const-string v0, "CAR.NOTIFICATION"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.NOTIFICATION"

    const-string v1, "onServiceDiscovery"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1902
    :cond_0
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1906
    iget-object v0, p0, Lcom/google/android/gms/car/hr;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->H(Lcom/google/android/gms/car/gx;)V

    .line 1907
    return-void
.end method
