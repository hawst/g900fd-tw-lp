.class final Lcom/google/android/gms/icing/impl/h;
.super Lcom/google/android/gms/icing/impl/j;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:I

.field final synthetic d:Ljava/lang/String;

.field final synthetic e:Z

.field final synthetic f:J

.field final synthetic g:Lcom/google/android/gms/icing/impl/e;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/e;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZJ)V
    .locals 1

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/h;->g:Lcom/google/android/gms/icing/impl/e;

    iput-object p2, p0, Lcom/google/android/gms/icing/impl/h;->a:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/gms/icing/impl/h;->b:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/gms/icing/impl/h;->c:I

    iput-object p5, p0, Lcom/google/android/gms/icing/impl/h;->d:Ljava/lang/String;

    iput-boolean p6, p0, Lcom/google/android/gms/icing/impl/h;->e:Z

    iput-wide p7, p0, Lcom/google/android/gms/icing/impl/h;->f:J

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/j;-><init>(Lcom/google/android/gms/icing/impl/e;)V

    return-void
.end method

.method private a(Ljava/lang/String;)[Ljava/lang/String;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/h;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/e;->e:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/j;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v2

    .line 226
    if-nez v2, :cond_1

    .line 227
    new-array v0, v1, [Ljava/lang/String;

    .line 234
    :cond_0
    return-object v0

    .line 229
    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v3, v0

    .line 230
    new-array v0, v3, [Ljava/lang/String;

    .line 231
    :goto_0
    if-ge v1, v3, :cond_0

    .line 232
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "section_"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    .line 231
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final synthetic a()Ljava/lang/Object;
    .locals 8

    .prologue
    .line 211
    const-string v0, "Indexing %s from %s"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/h;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/h;->b:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/h;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/h;->a:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/icing/impl/h;->c:I

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/h;->d:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/gms/icing/impl/h;->e:Z

    new-instance v5, Lcom/google/android/gms/icing/impl/d;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/h;->g:Lcom/google/android/gms/icing/impl/e;

    iget-object v6, v6, Lcom/google/android/gms/icing/impl/e;->a:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/h;->a:Ljava/lang/String;

    invoke-direct {p0, v7}, Lcom/google/android/gms/icing/impl/h;->a(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v7

    invoke-direct {v5, v6, v7}, Lcom/google/android/gms/icing/impl/d;-><init>(Landroid/content/ContentResolver;[Ljava/lang/String;)V

    iget-wide v6, p0, Lcom/google/android/gms/icing/impl/h;->f:J

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)V

    const/4 v0, 0x0

    return-object v0
.end method
