.class final Lcom/google/android/gms/games/internal/ao;
.super Lcom/google/android/gms/games/internal/t;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/aa;


# instance fields
.field private final c:Z

.field private final d:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 2547
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/internal/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2549
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-lez v0, :cond_0

    .line 2550
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 2551
    const-string v1, "profile_visible"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/gms/games/internal/ao;->c:Z

    .line 2553
    const-string v1, "profile_visibility_explicitly_set"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/ao;->d:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2561
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 2562
    return-void

    .line 2557
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/ao;->c:Z

    .line 2558
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/ao;->d:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2561
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2567
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ao;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 2572
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/ao;->c:Z

    return v0
.end method
