.class public final Lcom/google/android/gms/googlehelp/GoogleHelp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:Ljava/lang/String;

.field c:Landroid/accounts/Account;

.field d:Landroid/os/Bundle;

.field e:Z

.field f:Z

.field g:Ljava/util/List;

.field h:Landroid/os/Bundle;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field i:Landroid/graphics/Bitmap;
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field j:[B
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field k:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field l:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field m:Ljava/lang/String;

.field n:Landroid/net/Uri;

.field o:Ljava/util/List;

.field p:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field q:Lcom/google/android/gms/feedback/ThemeSettings;

.field r:Ljava/util/List;

.field s:Z

.field t:Lcom/google/android/gms/feedback/ErrorReport;

.field private u:Landroid/graphics/Bitmap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/google/android/gms/googlehelp/a;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/GoogleHelp;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILcom/google/android/gms/feedback/ThemeSettings;Ljava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V
    .locals 3

    .prologue
    .line 236
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 208
    new-instance v1, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Lcom/google/android/gms/feedback/ErrorReport;

    .line 237
    iput p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:I

    .line 240
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->b:Ljava/lang/String;

    .line 241
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    .line 242
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Landroid/os/Bundle;

    .line 245
    iput-boolean p5, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->e:Z

    .line 246
    iput-boolean p6, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->f:Z

    .line 249
    iput-object p7, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->g:Ljava/util/List;

    .line 252
    iput-object p8, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->h:Landroid/os/Bundle;

    .line 253
    iput-object p9, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Landroid/graphics/Bitmap;

    .line 254
    iput-object p10, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->j:[B

    .line 255
    iput p11, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:I

    .line 256
    iput p12, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:I

    .line 259
    move-object/from16 v0, p13

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->m:Ljava/lang/String;

    .line 262
    move-object/from16 v0, p14

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    .line 263
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->o:Ljava/util/List;

    .line 264
    iget v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->a:I

    const/4 v2, 0x4

    if-ge v1, v2, :cond_0

    .line 265
    new-instance v1, Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-direct {v1}, Lcom/google/android/gms/feedback/ThemeSettings;-><init>()V

    move/from16 v0, p16

    invoke-virtual {v1, v0}, Lcom/google/android/gms/feedback/ThemeSettings;->a(I)Lcom/google/android/gms/feedback/ThemeSettings;

    move-result-object p17

    move-object v1, p0

    .line 267
    :goto_0
    move-object/from16 v0, p17

    iput-object v0, v1, Lcom/google/android/gms/googlehelp/GoogleHelp;->q:Lcom/google/android/gms/feedback/ThemeSettings;

    .line 269
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->r:Ljava/util/List;

    .line 270
    move/from16 v0, p19

    iput-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Z

    .line 271
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Lcom/google/android/gms/feedback/ErrorReport;

    .line 272
    return-void

    .line 267
    :cond_0
    if-nez p17, :cond_1

    new-instance p17, Lcom/google/android/gms/feedback/ThemeSettings;

    invoke-direct/range {p17 .. p17}, Lcom/google/android/gms/feedback/ThemeSettings;-><init>()V

    move-object v1, p0

    goto :goto_0

    :cond_1
    move-object v1, p0

    goto :goto_0
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 21

    .prologue
    .line 280
    const/4 v1, 0x4

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x1

    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15}, Ljava/util/ArrayList;-><init>()V

    const/16 v16, 0x0

    const/16 v17, 0x0

    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    const/16 v19, 0x0

    new-instance v20, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct/range {v20 .. v20}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    move-object/from16 v0, p0

    move-object/from16 v2, p1

    invoke-direct/range {v0 .. v20}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(ILjava/lang/String;Landroid/accounts/Account;Landroid/os/Bundle;ZZLjava/util/List;Landroid/os/Bundle;Landroid/graphics/Bitmap;[BIILjava/lang/String;Landroid/net/Uri;Ljava/util/List;ILcom/google/android/gms/feedback/ThemeSettings;Ljava/util/List;ZLcom/google/android/gms/feedback/ErrorReport;)V

    .line 301
    return-void
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 1

    .prologue
    .line 310
    new-instance v0, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v0, p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 495
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.googlehelp.HELP"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "EXTRA_GOOGLE_HELP"

    invoke-virtual {v0, v1, p0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    .line 319
    return-object p0
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    .prologue
    .line 396
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    .line 397
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/googlehelp/GoogleHelp;
    .locals 0

    .prologue
    .line 323
    iput-object p1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Landroid/os/Bundle;

    .line 324
    return-object p0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 501
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 504
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->c:Landroid/accounts/Account;

    return-object v0
.end method

.method public final d()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 507
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->d:Landroid/os/Bundle;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 582
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Z
    .locals 1

    .prologue
    .line 510
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->e:Z

    return v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 513
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->f:Z

    return v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->g:Ljava/util/List;

    return-object v0
.end method

.method public final h()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 519
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->h:Landroid/os/Bundle;

    return-object v0
.end method

.method public final i()[B
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->j:[B

    return-object v0
.end method

.method public final j()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->i:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public final k()I
    .locals 1

    .prologue
    .line 543
    iget v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->k:I

    return v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 551
    iget v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->l:I

    return v0
.end method

.method public final m()Ljava/lang/String;
    .locals 1

    .prologue
    .line 555
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->m:Ljava/lang/String;

    return-object v0
.end method

.method public final n()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 558
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->n:Landroid/net/Uri;

    return-object v0
.end method

.method public final o()Ljava/util/List;
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->o:Ljava/util/List;

    return-object v0
.end method

.method public final p()Lcom/google/android/gms/feedback/ThemeSettings;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->q:Lcom/google/android/gms/feedback/ThemeSettings;

    return-object v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 569
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->r:Ljava/util/List;

    return-object v0
.end method

.method public final r()Z
    .locals 1

    .prologue
    .line 572
    iget-boolean v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->s:Z

    return v0
.end method

.method public final s()Lcom/google/android/gms/feedback/ErrorReport;
    .locals 1

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Lcom/google/android/gms/feedback/ErrorReport;

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 2

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->u:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 590
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->t:Lcom/google/android/gms/feedback/ErrorReport;

    iget-object v1, p0, Lcom/google/android/gms/googlehelp/GoogleHelp;->u:Landroid/graphics/Bitmap;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/feedback/ErrorReport;->a(Landroid/graphics/Bitmap;)V

    .line 592
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/googlehelp/a;->a(Lcom/google/android/gms/googlehelp/GoogleHelp;Landroid/os/Parcel;I)V

    .line 593
    return-void
.end method
