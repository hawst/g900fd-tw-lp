.class final Lcom/google/android/gms/icing/impl/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/a/i;


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V
    .locals 3

    .prologue
    .line 273
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/u;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 275
    const-string v0, "Skipping register from self resource"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 279
    :goto_0
    return-void

    .line 278
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/u;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 255
    const-string v0, "Skipping unregister from self resource"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 259
    :goto_0
    return-void

    .line 258
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 263
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-boolean v0, v0, Lcom/google/android/gms/icing/impl/u;->n:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 264
    const-string v0, "Skipping remove pkg from self resource"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 268
    :goto_0
    return-void

    .line 267
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/v;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/u;->g(Ljava/lang/String;)V

    goto :goto_0
.end method
