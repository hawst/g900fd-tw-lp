.class public final Lcom/google/android/gms/games/service/a/i/d;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;I)V
    .locals 1

    .prologue
    .line 24
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 25
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i/d;->c:Lcom/google/android/gms/games/internal/dr;

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/d;->d:Lcom/google/android/gms/games/a/au;

    .line 27
    iput p3, p0, Lcom/google/android/gms/games/service/a/i/d;->e:I

    .line 28
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/d;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->e(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 39
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/d;->d:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/i/d;->e:I

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
