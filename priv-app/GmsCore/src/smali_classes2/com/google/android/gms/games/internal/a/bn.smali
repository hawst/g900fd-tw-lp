.class public final Lcom/google/android/gms/games/internal/a/bn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/quest/e;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 136
    new-instance v0, Lcom/google/android/gms/games/internal/a/bo;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bo;-><init>(Lcom/google/android/gms/games/internal/a/bn;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[IZ)Lcom/google/android/gms/common/api/am;
    .locals 8

    .prologue
    .line 190
    new-instance v0, Lcom/google/android/gms/games/internal/a/br;

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move v7, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/br;-><init>(Lcom/google/android/gms/games/internal/a/bn;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[IIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/gms/games/internal/a/bs;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v6, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/internal/a/bs;-><init>(Lcom/google/android/gms/games/internal/a/bn;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;[IZ)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 158
    new-instance v0, Lcom/google/android/gms/games/internal/a/bp;

    const/4 v4, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/bp;-><init>(Lcom/google/android/gms/games/internal/a/bn;Lcom/google/android/gms/common/api/v;[IIZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final varargs a(Lcom/google/android/gms/common/api/v;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 169
    new-instance v0, Lcom/google/android/gms/games/internal/a/bq;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1, p2}, Lcom/google/android/gms/games/internal/a/bq;-><init>(Lcom/google/android/gms/games/internal/a/bn;Lcom/google/android/gms/common/api/v;Z[Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 130
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->p()V

    .line 131
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/quest/d;)V
    .locals 2

    .prologue
    .line 124
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 125
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->c(Lcom/google/android/gms/common/api/aj;)V

    .line 126
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/quest/d;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 215
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 216
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->c(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V

    .line 218
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 223
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->e(Ljava/lang/String;)V

    .line 224
    return-void
.end method
