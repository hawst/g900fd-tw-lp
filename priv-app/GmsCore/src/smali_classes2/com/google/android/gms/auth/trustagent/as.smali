.class public abstract Lcom/google/android/gms/auth/trustagent/as;
.super Landroid/support/v7/app/d;
.source "SourceFile"


# instance fields
.field a:Landroid/preference/PreferenceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Landroid/support/v7/app/d;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract e()Landroid/preference/PreferenceFragment;
.end method

.method protected f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 17
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 22
    invoke-super {p0, p1}, Landroid/support/v7/app/d;->onCreate(Landroid/os/Bundle;)V

    .line 23
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/as;->e()Landroid/preference/PreferenceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/as;->a:Landroid/preference/PreferenceFragment;

    .line 24
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/as;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    const v1, 0x1020002

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/as;->a:Landroid/preference/PreferenceFragment;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/as;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v1, v2, v3}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    .line 26
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 27
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 1

    .prologue
    .line 35
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 40
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 37
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/as;->onBackPressed()V

    .line 38
    const/4 v0, 0x1

    goto :goto_0

    .line 35
    :pswitch_data_0
    .packed-switch 0x102002c
        :pswitch_0
    .end packed-switch
.end method
