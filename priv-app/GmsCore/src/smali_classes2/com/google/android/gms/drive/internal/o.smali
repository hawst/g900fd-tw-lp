.class public final Lcom/google/android/gms/drive/internal/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/h;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 442
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 56
    const/high16 v0, 0x20000000

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/drive/internal/o;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 62
    new-instance v0, Lcom/google/android/gms/drive/internal/q;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/q;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 42
    if-nez p2, :cond_0

    .line 43
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Query must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 45
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/internal/p;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/p;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/gms/drive/internal/r;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/r;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/util/Set;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 87
    new-instance v0, Lcom/google/android/gms/drive/internal/s;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/drive/internal/s;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;Ljava/util/Set;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;
    .locals 2

    .prologue
    .line 99
    if-nez p2, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Id must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 103
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Client must be connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 105
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/internal/ar;

    invoke-direct {v0, p2}, Lcom/google/android/gms/drive/internal/ar;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 151
    new-instance v0, Lcom/google/android/gms/drive/internal/u;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/drive/internal/u;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;
    .locals 2

    .prologue
    .line 121
    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 122
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Client must be connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 124
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/b;->a:Lcom/google/android/gms/common/api/j;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/aj;

    .line 125
    new-instance v1, Lcom/google/android/gms/drive/internal/av;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/aj;->g()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/drive/internal/av;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    return-object v1
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/q;
    .locals 2

    .prologue
    .line 110
    if-nez p2, :cond_0

    .line 111
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Id must be provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 113
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 114
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Client must be connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 116
    :cond_1
    new-instance v0, Lcom/google/android/gms/drive/internal/av;

    invoke-direct {v0, p2}, Lcom/google/android/gms/drive/internal/av;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;
    .locals 2

    .prologue
    .line 130
    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Client must be connected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 133
    :cond_0
    sget-object v0, Lcom/google/android/gms/drive/b;->a:Lcom/google/android/gms/common/api/j;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/aj;

    .line 134
    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/aj;->l()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 135
    if-eqz v1, :cond_1

    new-instance v0, Lcom/google/android/gms/drive/internal/av;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/internal/av;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 140
    new-instance v0, Lcom/google/android/gms/drive/internal/t;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/t;-><init>(Lcom/google/android/gms/drive/internal/o;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
