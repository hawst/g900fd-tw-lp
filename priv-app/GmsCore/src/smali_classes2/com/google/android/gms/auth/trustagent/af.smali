.class public final Lcom/google/android/gms/auth/trustagent/af;
.super Lcom/google/android/gms/auth/trustagent/a;
.source "SourceFile"


# instance fields
.field private c:Landroid/widget/LinearLayout;

.field private d:Landroid/preference/Preference;

.field private e:Landroid/preference/Preference;

.field private f:Landroid/preference/Preference;

.field private g:Landroid/preference/CheckBoxPreference;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/af;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->d()V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/af;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/af;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->i:Z

    return v0
.end method

.method private d()V
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 161
    invoke-static {}, Landroid/bluetooth/BluetoothAdapter;->getDefaultAdapter()Landroid/bluetooth/BluetoothAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->h:Z

    .line 163
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Landroid/nfc/NfcAdapter;->getDefaultAdapter(Landroid/content/Context;)Landroid/nfc/NfcAdapter;

    move-result-object v0

    if-eqz v0, :cond_1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    :goto_1
    iput-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/af;->i:Z

    .line 165
    return-void

    :cond_0
    move v0, v2

    .line 161
    goto :goto_0

    :cond_1
    move v1, v2

    .line 163
    goto :goto_1
.end method

.method static synthetic d(Lcom/google/android/gms/auth/trustagent/af;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->e()V

    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "location"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    .line 257
    const-string v1, "gps"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    const-string v1, "network"

    invoke-virtual {v0, v1}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->j:Z

    .line 259
    return-void

    .line 257
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic e(Lcom/google/android/gms/auth/trustagent/af;)Z
    .locals 1

    .prologue
    .line 39
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->j:Z

    return v0
.end method

.method private f()Ljava/lang/String;
    .locals 6

    .prologue
    .line 388
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    .line 389
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    .line 390
    if-eqz v2, :cond_2

    iget-object v0, v2, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 392
    :goto_0
    if-eqz v0, :cond_3

    .line 393
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 394
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 395
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-eqz v0, :cond_1

    .line 397
    const-string v0, ", "

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 399
    :cond_1
    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v4, ""

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 390
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 406
    :cond_3
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_4

    .line 407
    sget v0, Lcom/google/android/gms/p;->cM:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 410
    :goto_2
    return-object v0

    :cond_4
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2
.end method

.method static synthetic f(Lcom/google/android/gms/auth/trustagent/af;)Z
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->i()Z

    move-result v0

    return v0
.end method

.method private g()Ljava/lang/String;
    .locals 1

    .prologue
    .line 414
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/g;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 415
    sget v0, Lcom/google/android/gms/p;->cw:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 419
    :goto_0
    return-object v0

    .line 416
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->i()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 417
    sget v0, Lcom/google/android/gms/p;->cD:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 419
    :cond_1
    sget v0, Lcom/google/android/gms/p;->cC:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private h()Z
    .locals 2

    .prologue
    .line 424
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    const-string v1, "device_policy"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/admin/DevicePolicyManager;

    .line 426
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/app/admin/DevicePolicyManager;->getKeyguardDisabledFeatures(Landroid/content/ComponentName;)I

    move-result v0

    .line 427
    and-int/lit8 v0, v0, 0x10

    const/16 v1, 0x10

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Z
    .locals 2

    .prologue
    .line 445
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    if-nez v0, :cond_0

    .line 446
    const/4 v0, 0x0

    .line 448
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trustlet_enabled_FaceUnlockTrustlet"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final b()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->h()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    sget v1, Lcom/google/android/gms/p;->cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 124
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    if-eqz v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->h()Z

    move-result v0

    if-eqz v0, :cond_8

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    sget v1, Lcom/google/android/gms/p;->cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 125
    :cond_1
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/g;->c(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->k:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/g;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v3}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 126
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    if-eqz v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->h()Z

    move-result v0

    if-eqz v0, :cond_a

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    sget v1, Lcom/google/android/gms/p;->cw:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v3}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    .line 127
    :cond_3
    :goto_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0}, Landroid/preference/PreferenceScreen;->getPreferenceCount()I

    move-result v0

    if-nez v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->c:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v3}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 128
    :goto_4
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->h:Z

    if-nez v0, :cond_4

    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->i:Z

    if-eqz v0, :cond_10

    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->h:Z

    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_5
    :goto_5
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "auth_trust_agent_pref_trusted_bluetooth_title"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    const-string v5, ""

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_14

    const/16 v1, 0x2d

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_6
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, ", "

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 123
    :cond_7
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->d()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/ag;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/ag;-><init>(Lcom/google/android/gms/auth/trustagent/af;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_0

    .line 124
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->e()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/ah;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/ah;-><init>(Lcom/google/android/gms/auth/trustagent/af;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_1

    .line 125
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->k:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v2}, Landroid/preference/Preference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/ai;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/ai;-><init>(Lcom/google/android/gms/auth/trustagent/af;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    goto/16 :goto_2

    .line 126
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v2}, Landroid/preference/CheckBoxPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    if-eqz v0, :cond_b

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/am;->a()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    const-string v2, "auth_trust_agent_pref_trustlet_enabled_com.google.android.gms.auth.trustagent.trustlet.PhonePositionTrustlet"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v0

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/aj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/aj;-><init>(Lcom/google/android/gms/auth/trustagent/af;)V

    invoke-virtual {v0, v1}, Landroid/preference/CheckBoxPreference;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    goto/16 :goto_3

    .line 127
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    goto/16 :goto_4

    .line 128
    :cond_d
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->i:Z

    if-eqz v0, :cond_f

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a(Lcom/google/android/gms/auth/trustagent/am;)Lcom/google/android/gms/auth/trustagent/trustlet/af;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_7
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    if-eqz v4, :cond_e

    const-string v4, ", "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_e
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    :cond_f
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v0

    if-nez v0, :cond_13

    sget v0, Lcom/google/android/gms/p;->cK:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_8
    invoke-virtual {v2, v0}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_10
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->j:Z

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->f()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    :cond_11
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/af;->k:Z

    if-eqz v0, :cond_12

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/af;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 129
    :cond_12
    return-void

    .line 128
    :cond_13
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_8

    :cond_14
    move-object v0, v1

    goto/16 :goto_6
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 433
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/auth/trustagent/a;->onActivityResult(IILandroid/content/Intent;)V

    .line 434
    if-nez p1, :cond_0

    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trustlet_enabled_FaceUnlockTrustlet"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 440
    new-instance v0, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFaceUnlockSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->startActivity(Landroid/content/Intent;)V

    .line 442
    :cond_0
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 81
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/a;->onCreate(Landroid/os/Bundle;)V

    .line 82
    sget v0, Lcom/google/android/gms/s;->d:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->addPreferencesFromResource(I)V

    .line 83
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/l;->t:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->setContentView(I)V

    .line 84
    const-string v0, "auth_trust_agent_pref_trusted_devices_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    sget-object v0, Lcom/google/android/gms/auth/b/a;->W:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->X:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/google/android/gms/auth/trustagent/af;->d:Landroid/preference/Preference;

    :cond_0
    const-string v0, "auth_trust_agent_pref_trusted_places_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    sget-object v0, Lcom/google/android/gms/auth/b/a;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/google/android/gms/auth/trustagent/af;->e:Landroid/preference/Preference;

    :cond_1
    const-string v0, "auth_trust_agent_pref_face_unlock_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/g;->a(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/google/android/gms/auth/trustagent/af;->f:Landroid/preference/Preference;

    :cond_2
    const-string v0, "auth_trust_agent_pref_activity_recognition_unlock_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/af;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/CheckBoxPreference;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    sget-object v0, Lcom/google/android/gms/auth/b/a;->Z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->removePreference(Landroid/preference/Preference;)Z

    iput-object v2, p0, Lcom/google/android/gms/auth/trustagent/af;->g:Landroid/preference/CheckBoxPreference;

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->mC:I

    invoke-virtual {v0, v1}, Landroid/app/Activity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->c:Landroid/widget/LinearLayout;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/af;->c:Landroid/widget/LinearLayout;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 85
    return-void
.end method
