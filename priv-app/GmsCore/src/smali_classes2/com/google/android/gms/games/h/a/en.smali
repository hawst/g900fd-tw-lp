.class public final Lcom/google/android/gms/games/h/a/en;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 68
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 70
    sput-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "autoMatchingCriteria"

    const-string v2, "autoMatchingCriteria"

    const-class v3, Lcom/google/android/gms/games/h/a/el;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "capabilities"

    const-string v2, "capabilities"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "clientAddress"

    const-string v2, "clientAddress"

    const-class v3, Lcom/google/android/gms/games/h/a/em;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "invitedPlayerIds"

    const-string v2, "invitedPlayerIds"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->h(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "networkDiagnostics"

    const-string v2, "networkDiagnostics"

    const-class v3, Lcom/google/android/gms/games/h/a/cm;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "requestId"

    const-string v2, "requestId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    const-string v1, "variant"

    const-string v2, "variant"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 83
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    .line 90
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/h/a/el;Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/em;Ljava/util/ArrayList;Lcom/google/android/gms/games/h/a/cm;Ljava/lang/Long;Ljava/lang/Integer;)V
    .locals 4

    .prologue
    .line 100
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 124
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    .line 101
    if-eqz p1, :cond_0

    .line 102
    const-string v0, "autoMatchingCriteria"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/en;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 104
    :cond_0
    if-eqz p2, :cond_1

    .line 105
    const-string v0, "capabilities"

    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/games/h/a/en;->i(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 107
    :cond_1
    if-eqz p3, :cond_2

    .line 108
    const-string v0, "clientAddress"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/en;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 110
    :cond_2
    if-eqz p4, :cond_3

    .line 111
    const-string v0, "invitedPlayerIds"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/en;->i(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 113
    :cond_3
    if-eqz p5, :cond_4

    .line 114
    const-string v0, "networkDiagnostics"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/games/h/a/en;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 116
    :cond_4
    if-eqz p6, :cond_5

    .line 117
    const-string v0, "requestId"

    invoke-virtual {p6}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/en;->a(Ljava/lang/String;J)V

    .line 119
    :cond_5
    if-eqz p7, :cond_6

    .line 120
    const-string v0, "variant"

    invoke-virtual {p7}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/en;->a(Ljava/lang/String;I)V

    .line 122
    :cond_6
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 87
    sget-object v0, Lcom/google/android/gms/games/h/a/en;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    return-void
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getAutoMatchingCriteria()Lcom/google/android/gms/games/h/a/el;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    const-string v1, "autoMatchingCriteria"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/el;

    return-object v0
.end method

.method public final getClientAddress()Lcom/google/android/gms/games/h/a/em;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    const-string v1, "clientAddress"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/em;

    return-object v0
.end method

.method public final getNetworkDiagnostics()Lcom/google/android/gms/games/h/a/cm;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/en;->c:Ljava/util/HashMap;

    const-string v1, "networkDiagnostics"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cm;

    return-object v0
.end method
