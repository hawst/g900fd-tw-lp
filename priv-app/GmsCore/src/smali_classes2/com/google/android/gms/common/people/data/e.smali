.class public final Lcom/google/android/gms/common/people/data/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/people/model/d;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 2

    .prologue
    .line 225
    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->e()I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 226
    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    .line 228
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->c()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/people/model/d;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/people/model/j;)Lcom/google/android/gms/common/people/data/AudienceMember;
    .locals 3

    .prologue
    .line 217
    invoke-interface {p0}, Lcom/google/android/gms/people/model/j;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0}, Lcom/google/android/gms/people/model/j;->b()Ljava/lang/String;

    move-result-object v1

    invoke-interface {p0}, Lcom/google/android/gms/people/model/j;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/people/data/AudienceMember;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/people/data/AudienceMember;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/people/model/j;
    .locals 1

    .prologue
    .line 212
    new-instance v0, Lcom/google/android/gms/common/people/data/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/people/data/f;-><init>(Lcom/google/android/gms/common/people/data/AudienceMember;)V

    return-object v0
.end method
