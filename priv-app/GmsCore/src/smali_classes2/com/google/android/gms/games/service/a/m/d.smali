.class public final Lcom/google/android/gms/games/service/a/m/d;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/service/b;

.field private final e:Lcom/google/android/gms/games/a/au;

.field private final f:Ljava/lang/String;

.field private final g:Z

.field private final h:I

.field private i:Lcom/google/android/gms/games/service/a/m/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;ZI)V
    .locals 1

    .prologue
    .line 32
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 33
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/d;->c:Lcom/google/android/gms/games/internal/dr;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/m/d;->d:Lcom/google/android/gms/games/service/b;

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/d;->e:Lcom/google/android/gms/games/a/au;

    .line 36
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/m/d;->f:Ljava/lang/String;

    .line 37
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/a/m/d;->g:Z

    .line 38
    iput p6, p0, Lcom/google/android/gms/games/service/a/m/d;->h:I

    .line 39
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 71
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    .prologue
    .line 51
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 52
    const/16 v1, 0xfa4

    if-ne v0, v1, :cond_0

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->c:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/a/m/e;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v3, v3, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v4, v4, Lcom/google/android/gms/games/service/a/m/e;->d:Lcom/google/android/gms/drive/Contents;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v5, v5, Lcom/google/android/gms/games/service/a/m/e;->e:Lcom/google/android/gms/drive/Contents;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/Contents;)V

    .line 66
    :goto_0
    return-void

    .line 59
    :cond_0
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    if-eqz v1, :cond_1

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object p1, v0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    .line 64
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/d;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/Contents;)V

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/d;->e:Lcom/google/android/gms/games/a/au;

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/m/d;->f:Ljava/lang/String;

    iget-boolean v4, p0, Lcom/google/android/gms/games/service/a/m/d;->g:Z

    iget v5, p0, Lcom/google/android/gms/games/service/a/m/d;->h:I

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;ZI)Lcom/google/android/gms/games/service/a/m/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/d;->i:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method
