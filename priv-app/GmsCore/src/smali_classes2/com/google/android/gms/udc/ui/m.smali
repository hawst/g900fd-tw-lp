.class final Lcom/google/android/gms/udc/ui/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

.field private b:Landroid/support/v4/app/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)V
    .locals 1

    .prologue
    .line 343
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 344
    invoke-virtual {p1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/t;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/m;->b:Landroid/support/v4/app/m;

    .line 345
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 411
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->b:Landroid/support/v4/app/m;

    if-eqz v0, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->b:Landroid/support/v4/app/m;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/util/j;->b(Landroid/support/v4/app/m;)V

    .line 413
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/m;->b:Landroid/support/v4/app/m;

    .line 415
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 350
    new-instance v0, Lcom/google/android/gms/udc/ui/u;

    invoke-direct {v0}, Lcom/google/android/gms/udc/ui/u;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->zb:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/u;->a(Ljava/lang/String;)Lcom/google/android/gms/udc/ui/u;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/u;->a(Landroid/support/v4/app/v;)Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/m;->b:Landroid/support/v4/app/m;

    .line 353
    new-instance v0, Lcom/google/android/gms/udc/c/y;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->i(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/x;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/udc/c/y;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/udc/e/x;)V

    .line 355
    const-wide/16 v2, 0xf

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/udc/c/y;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 356
    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 376
    invoke-direct {p0}, Lcom/google/android/gms/udc/ui/m;->a()V

    .line 377
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 338
    check-cast p2, Lcom/google/android/gms/udc/f;

    invoke-direct {p0}, Lcom/google/android/gms/udc/ui/m;->a()V

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    const/4 v1, -0x1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->j(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->finish()V

    :goto_0
    return-void

    :cond_0
    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    const-string v1, "UdcConsent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error writing the consent:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    const-string v0, "UdcConsent"

    const-string v2, "Unknown statuscode:%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->e(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v2

    const-string v3, "WriteConsent"

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;I)V

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->k(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/e/x;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Lcom/google/android/gms/udc/ui/UdcConsentActivity;)Lcom/google/android/gms/udc/util/j;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/udc/ui/g;

    invoke-direct {v2}, Lcom/google/android/gms/udc/ui/g;-><init>()V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/udc/ui/g;->b(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    const v3, 0x104000a

    invoke-virtual {v2, v3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/g;->c(Ljava/lang/CharSequence;)Lcom/google/android/gms/udc/ui/g;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/g;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/udc/util/j;->a(Landroid/support/v4/app/m;)V

    goto :goto_0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->zd:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yZ:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yO:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/m;->a:Lcom/google/android/gms/udc/ui/UdcConsentActivity;

    sget v2, Lcom/google/android/gms/p;->yU:I

    invoke-virtual {v0, v2}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    nop

    :pswitch_data_0
    .packed-switch 0x1196
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
