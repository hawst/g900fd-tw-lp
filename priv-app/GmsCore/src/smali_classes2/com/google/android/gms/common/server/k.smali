.class public abstract Lcom/google/android/gms/common/server/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/util/ArrayList;

.field private final b:Ljava/util/HashMap;

.field private c:Ljava/lang/String;

.field private final d:Lcom/google/android/gms/common/server/l;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/k;->a:Ljava/util/ArrayList;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/server/k;->b:Ljava/util/HashMap;

    .line 84
    new-instance v0, Lcom/google/android/gms/common/server/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/l;-><init>(Lcom/google/android/gms/common/server/k;)V

    iput-object v0, p0, Lcom/google/android/gms/common/server/k;->d:Lcom/google/android/gms/common/server/l;

    .line 86
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Lcom/google/android/gms/common/server/k;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/google/android/gms/common/server/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 166
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 224
    const-string v0, "prettyPrint"

    invoke-static {}, Lcom/google/android/gms/common/ew;->b()Z

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Z)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/common/server/k;->c:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 228
    const-string v1, "trace"

    iget-object v2, p0, Lcom/google/android/gms/common/server/k;->c:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 231
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/server/k;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 232
    const-string v1, "fields"

    const-string v2, ","

    iget-object v3, p0, Lcom/google/android/gms/common/server/k;->a:Ljava/util/ArrayList;

    invoke-interface {v3}, Ljava/util/List;->toArray()[Ljava/lang/Object;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/server/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 235
    :cond_1
    return-object v0
.end method
