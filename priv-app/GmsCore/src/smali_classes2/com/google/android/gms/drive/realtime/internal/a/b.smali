.class public abstract Lcom/google/android/gms/drive/realtime/internal/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:[Ljava/lang/String;

.field private final b:I


# direct methods
.method public constructor <init>([Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/internal/a/b;->a:[Ljava/lang/String;

    .line 27
    array-length v0, p1

    iput v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/b;->b:I

    .line 28
    return-void
.end method

.method private final a(I)Landroid/database/CursorWindow;
    .locals 2

    .prologue
    .line 130
    new-instance v0, Landroid/database/CursorWindow;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/database/CursorWindow;-><init>(Z)V

    .line 131
    iget v1, p0, Lcom/google/android/gms/drive/realtime/internal/a/b;->b:I

    invoke-virtual {v0, v1}, Landroid/database/CursorWindow;->setNumColumns(I)Z

    .line 132
    invoke-virtual {v0, p1}, Landroid/database/CursorWindow;->setStartPosition(I)V

    .line 133
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/realtime/internal/a/b;Ljava/util/List;Ljava/lang/Object;I)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(Ljava/util/List;Ljava/lang/Object;I)V

    return-void
.end method

.method private final a(Ljava/util/List;Ljava/lang/Object;I)V
    .locals 3

    .prologue
    .line 90
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    .line 92
    if-nez v0, :cond_0

    .line 93
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(I)Landroid/database/CursorWindow;

    move-result-object v0

    .line 94
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    :goto_0
    invoke-virtual {v0}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v1

    if-nez v1, :cond_1

    .line 102
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(I)Landroid/database/CursorWindow;

    move-result-object v0

    .line 103
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-virtual {v0}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v1

    if-nez v1, :cond_1

    .line 105
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot allocate window space."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 96
    :cond_0
    add-int/lit8 v0, v0, -0x1

    invoke-interface {p1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/CursorWindow;

    goto :goto_0

    .line 109
    :cond_1
    invoke-virtual {p0, v0, p3, p2}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(Landroid/database/CursorWindow;ILjava/lang/Object;)Z

    move-result v1

    .line 110
    if-eqz v1, :cond_3

    .line 127
    :cond_2
    return-void

    .line 115
    :cond_3
    invoke-virtual {v0}, Landroid/database/CursorWindow;->freeLastRow()V

    .line 116
    invoke-direct {p0, p3}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(I)Landroid/database/CursorWindow;

    move-result-object v0

    .line 117
    invoke-interface {p1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 118
    invoke-virtual {v0}, Landroid/database/CursorWindow;->allocRow()Z

    move-result v1

    if-nez v1, :cond_4

    .line 119
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot allocate window space."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 122
    :cond_4
    invoke-virtual {p0, v0, p3, p2}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(Landroid/database/CursorWindow;ILjava/lang/Object;)Z

    move-result v0

    .line 123
    if-nez v0, :cond_2

    .line 125
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Single row exceeds window size: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private final b(Ljava/lang/Iterable;)[Landroid/database/CursorWindow;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 69
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 70
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 71
    new-array v0, v0, [Landroid/database/CursorWindow;

    .line 86
    :goto_0
    return-object v0

    .line 73
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 76
    :try_start_0
    invoke-interface {p1}, Ljava/lang/Iterable;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    .line 77
    add-int/lit8 v1, v0, 0x1

    invoke-direct {p0, v2, v4, v0}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(Ljava/util/List;Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 78
    goto :goto_1

    .line 79
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 81
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/CursorWindow;

    .line 82
    invoke-virtual {v0}, Landroid/database/CursorWindow;->close()V

    goto :goto_2

    .line 84
    :cond_1
    throw v1

    .line 86
    :cond_2
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Landroid/database/CursorWindow;

    invoke-interface {v2, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/CursorWindow;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/Iterable;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/internal/a/b;->b(Ljava/lang/Iterable;)[Landroid/database/CursorWindow;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a([Landroid/database/CursorWindow;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method final a([Landroid/database/CursorWindow;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 65
    new-instance v0, Lcom/google/android/gms/common/data/DataHolder;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/internal/a/b;->a:[Ljava/lang/String;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>([Ljava/lang/String;[Landroid/database/CursorWindow;ILandroid/os/Bundle;)V

    return-object v0
.end method

.method protected abstract a(Landroid/database/CursorWindow;ILjava/lang/Object;)Z
.end method
