.class public final Lcom/google/android/gms/games/a/bf;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "REPLACE_ME"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/a/bf;->a:[Ljava/lang/String;

    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I
    .locals 17

    .prologue
    .line 185
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/bi;->getRoom()Lcom/google/android/gms/games/h/a/ej;

    move-result-object v13

    .line 186
    if-nez v13, :cond_1

    .line 187
    const/4 v6, -0x1

    .line 288
    :cond_0
    :goto_0
    return v6

    .line 189
    :cond_1
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->getCreationDetails()Lcom/google/android/gms/games/h/a/er;

    move-result-object v4

    .line 190
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->getLastUpdateDetails()Lcom/google/android/gms/games/h/a/er;

    move-result-object v5

    .line 191
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->d()Ljava/lang/String;

    move-result-object v2

    .line 192
    if-nez v2, :cond_b

    .line 193
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/er;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 195
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/er;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    .line 196
    if-nez v5, :cond_2

    move-wide v6, v8

    .line 200
    :goto_2
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->getParticipants()Ljava/util/ArrayList;

    move-result-object v4

    .line 201
    if-nez v4, :cond_3

    .line 202
    const-string v2, "MultiplayerUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No participants found for invitation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 203
    const/4 v6, -0x1

    goto :goto_0

    .line 196
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/er;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    move-wide v6, v4

    goto :goto_2

    .line 208
    :cond_3
    const/4 v11, 0x0

    .line 209
    const/4 v10, 0x0

    .line 210
    const/4 v5, 0x0

    .line 211
    const/4 v2, 0x0

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v14

    move v12, v2

    :goto_3
    if-ge v12, v14, :cond_a

    .line 212
    invoke-virtual {v4, v12}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/eu;

    .line 213
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v15

    .line 214
    invoke-virtual {v15, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_6

    .line 215
    const/4 v10, 0x1

    .line 216
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/eu;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v12

    .line 217
    if-eqz v12, :cond_5

    .line 218
    invoke-virtual {v12}, Lcom/google/android/gms/games/h/a/cs;->b()Ljava/lang/String;

    move-result-object v5

    .line 219
    invoke-virtual {v12}, Lcom/google/android/gms/games/h/a/cs;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move/from16 v16, v10

    move-object v10, v5

    move/from16 v5, v16

    .line 228
    :goto_4
    if-eqz v5, :cond_4

    if-nez v10, :cond_7

    .line 229
    :cond_4
    const-string v2, "MultiplayerUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No inviting player found for external ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    const/4 v6, -0x1

    goto/16 :goto_0

    .line 220
    :cond_5
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/eu;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v12

    if-eqz v12, :cond_a

    .line 221
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/eu;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/s;->c()Ljava/lang/String;

    move-result-object v2

    move/from16 v16, v5

    move v5, v10

    move-object v10, v2

    move/from16 v2, v16

    goto :goto_4

    .line 211
    :cond_6
    add-int/lit8 v2, v12, 0x1

    move v12, v2

    goto :goto_3

    .line 234
    :cond_7
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 235
    const-string v10, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 236
    const-string v10, "external_invitation_id"

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v5, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    const-string v10, "external_inviter_id"

    invoke-virtual {v5, v10, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    const-string v3, "creation_timestamp"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v5, v3, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 239
    const-string v3, "last_modified_timestamp"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 240
    const-string v3, "description"

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->c()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    const-string v3, "type"

    const/4 v6, 0x0

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 242
    const-string v3, "inviter_in_circles"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 243
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->g()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 244
    const-string v2, "variant"

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->g()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248
    :cond_8
    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->getAutoMatchingCriteria()Lcom/google/android/gms/games/h/a/el;

    move-result-object v2

    .line 249
    if-eqz v2, :cond_9

    .line 250
    const-string v3, "has_automatch_criteria"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 251
    const-string v3, "automatch_min_players"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/el;->d()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 253
    const-string v3, "automatch_max_players"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/el;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 262
    :goto_5
    const-string v2, "MultiplayerUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Adding invitation for room "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 263
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 264
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 271
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "invitation_id=?"

    sget-object v5, Lcom/google/android/gms/games/a/bf;->a:[Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 277
    const-string v5, "invitation_id"

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;ILcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)V

    .line 281
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v9

    invoke-virtual {v13}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object/from16 v7, p0

    move-object/from16 v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v7 .. v13}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bj;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 283
    if-eqz v2, :cond_0

    .line 284
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 256
    :cond_9
    const-string v2, "has_automatch_criteria"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 257
    const-string v2, "automatch_min_players"

    invoke-virtual {v5, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 258
    const-string v2, "automatch_max_players"

    invoke-virtual {v5, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_5

    :cond_a
    move v2, v5

    move v5, v10

    move-object v10, v11

    goto/16 :goto_4

    :cond_b
    move-object v3, v2

    goto/16 :goto_1
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I
    .locals 13

    .prologue
    .line 518
    if-nez p2, :cond_1

    .line 519
    const/4 v5, -0x1

    .line 635
    :cond_0
    :goto_0
    return v5

    .line 524
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    iget-object v1, p2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v2, v1}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 525
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v3

    .line 526
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getCreationDetails()Lcom/google/android/gms/games/h/a/fp;

    move-result-object v4

    .line 527
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fp;->c()Ljava/lang/String;

    move-result-object v9

    .line 528
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getLastUpdateDetails()Lcom/google/android/gms/games/h/a/fp;

    move-result-object v5

    .line 529
    if-nez v5, :cond_2

    const/4 v1, 0x0

    move-object v8, v1

    .line 531
    :goto_1
    const-string v1, "pending_participant_external"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 534
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    .line 535
    if-nez v11, :cond_3

    .line 536
    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "No participants found for match "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 537
    const/4 v5, -0x1

    goto :goto_0

    .line 529
    :cond_2
    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/fp;->c()Ljava/lang/String;

    move-result-object v1

    move-object v8, v1

    goto :goto_1

    .line 542
    :cond_3
    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 544
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 547
    const-string v1, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 548
    const-string v1, "creator_external"

    invoke-virtual {v2, v1, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 549
    const-string v1, "creation_timestamp"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fp;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 550
    const-string v1, "last_updater_external"

    invoke-virtual {v2, v1, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 551
    if-eqz v5, :cond_4

    .line 552
    const-string v1, "last_updated_timestamp"

    invoke-virtual {v5}, Lcom/google/android/gms/games/h/a/fp;->b()Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 555
    :cond_4
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 556
    const-string v1, "pending_participant_external"

    invoke-virtual {v2, v1, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getData()Lcom/google/android/gms/games/h/a/fn;

    move-result-object v1

    .line 558
    if-eqz v1, :cond_5

    .line 559
    const-string v3, "data"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 561
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getPreviousMatchData()Lcom/google/android/gms/games/h/a/fn;

    move-result-object v1

    .line 562
    if-eqz v1, :cond_6

    .line 563
    const-string v3, "previous_match_data"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fn;->b()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 572
    :goto_2
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/fl;->getAutoMatchingCriteria()Lcom/google/android/gms/games/h/a/fk;

    move-result-object v1

    .line 573
    if-eqz v1, :cond_7

    .line 574
    const-string v3, "has_automatch_criteria"

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 575
    const-string v3, "automatch_min_players"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fk;->d()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 576
    const-string v3, "automatch_max_players"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fk;->c()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 577
    const-string v3, "automatch_bit_mask"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fk;->b()Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 586
    :goto_3
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 587
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 594
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "match_id=?"

    sget-object v3, Lcom/google/android/gms/games/a/bf;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    move-object/from16 v0, p6

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 601
    const-string v4, "match_id"

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;Ljava/lang/String;ILcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)V

    .line 603
    const/4 v2, 0x0

    .line 604
    const/4 v3, 0x0

    .line 605
    const/4 v4, 0x0

    .line 606
    const/4 v1, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v6, v1

    :goto_4
    if-ge v6, v7, :cond_8

    .line 607
    invoke-virtual {v11, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/h/a/fq;

    .line 608
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/fq;->b()Ljava/lang/String;

    move-result-object v12

    .line 609
    invoke-virtual {v12, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_d

    .line 610
    const/4 v1, 0x1

    .line 612
    :goto_5
    invoke-virtual {v12, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 613
    const/4 v2, 0x1

    .line 615
    :goto_6
    invoke-virtual {v12, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_b

    .line 616
    const/4 v3, 0x1

    .line 606
    :goto_7
    add-int/lit8 v4, v6, 0x1

    move v6, v4

    move v4, v3

    move v3, v2

    move v2, v1

    goto :goto_4

    .line 568
    :cond_6
    const-string v1, "previous_match_data"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 579
    :cond_7
    const-string v1, "has_automatch_criteria"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v2, v1, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 580
    const-string v1, "automatch_min_players"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 581
    const-string v1, "automatch_max_players"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 582
    const-string v1, "automatch_bit_mask"

    invoke-virtual {v2, v1}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_3

    .line 621
    :cond_8
    if-nez v2, :cond_9

    .line 622
    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No match-creating player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 623
    const/4 v5, -0x1

    goto/16 :goto_0

    .line 625
    :cond_9
    if-eqz v8, :cond_a

    if-nez v3, :cond_a

    .line 626
    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No match-updating player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 627
    const/4 v5, -0x1

    goto/16 :goto_0

    .line 629
    :cond_a
    if-eqz v10, :cond_0

    if-nez v4, :cond_0

    .line 630
    const-string v1, "MultiplayerUtils"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No pending player found for external ID "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 631
    const/4 v5, -0x1

    goto/16 :goto_0

    :cond_b
    move v3, v4

    goto :goto_7

    :cond_c
    move v2, v3

    goto/16 :goto_6

    :cond_d
    move v1, v2

    goto/16 :goto_5
.end method

.method static a(Ljava/lang/String;Lcom/android/volley/ac;I)I
    .locals 2

    .prologue
    .line 126
    .line 127
    invoke-static {p1, p0}, Lcom/google/android/gms/common/server/b/c;->b(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/a;

    move-result-object v0

    .line 128
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v1

    if-nez v1, :cond_1

    .line 166
    :cond_0
    :goto_0
    return p2

    .line 133
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/b/a;->b()Ljava/lang/String;

    move-result-object v0

    .line 134
    const-string v1, "MatchCreationNotAllowed"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 135
    const/16 p2, 0x1770

    goto :goto_0

    .line 136
    :cond_2
    const-string v1, "TrustedTestersOnly"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 137
    const/16 p2, 0x1771

    goto :goto_0

    .line 138
    :cond_3
    const-string v1, "InvalidMatchType"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 139
    const/16 p2, 0x1772

    goto :goto_0

    .line 140
    :cond_4
    const-string v1, "MultiplayerDisabled"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 141
    const/16 p2, 0x1773

    goto :goto_0

    .line 142
    :cond_5
    const-string v1, "InactiveRoom"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 143
    const/16 p2, 0x1b5d

    goto :goto_0

    .line 144
    :cond_6
    const-string v1, "InvalidParticipantState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 145
    const/16 p2, 0x1964

    goto :goto_0

    .line 146
    :cond_7
    const-string v1, "InactiveMatch"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 147
    const/16 p2, 0x1965

    goto :goto_0

    .line 148
    :cond_8
    const-string v1, "InvalidState"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_9

    .line 149
    const/16 p2, 0x1966

    goto :goto_0

    .line 150
    :cond_9
    const-string v1, "OutOfDateVersion"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_a

    .line 151
    const/16 p2, 0x1967

    goto :goto_0

    .line 152
    :cond_a
    const-string v1, "InvalidMatchResults"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 153
    const/16 p2, 0x1968

    goto :goto_0

    .line 154
    :cond_b
    const-string v1, "AlreadyRematched"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 155
    const/16 p2, 0x1969

    goto :goto_0

    .line 158
    :cond_c
    const/16 v0, 0x190

    invoke-static {p1, v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 160
    const/16 p2, 0x1774

    goto/16 :goto_0

    .line 161
    :cond_d
    invoke-static {p1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 163
    const/4 p2, 0x6

    goto/16 :goto_0
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/eu;)Landroid/content/ContentValues;
    .locals 6

    .prologue
    .line 899
    iget-object v0, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 900
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/eu;->getClientAddress()Lcom/google/android/gms/games/h/a/em;

    move-result-object v1

    .line 904
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/eu;->b()Ljava/lang/Boolean;

    move-result-object v2

    if-nez v2, :cond_0

    .line 905
    const-string v2, "connected"

    invoke-virtual {v0, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 908
    :cond_0
    if-eqz v1, :cond_1

    .line 909
    const-string v2, "client_address"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/em;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 912
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/eu;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v1

    .line 913
    if-eqz v1, :cond_3

    .line 914
    const-string v2, "default_display_name"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/s;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 915
    const-string v2, "default_display_image_url"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 926
    :cond_2
    :goto_0
    return-object v0

    .line 919
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/eu;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v1

    .line 920
    if-eqz v1, :cond_2

    .line 921
    iget-object v1, v1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 922
    const-string v2, "last_updated"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 923
    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/h/a/ev;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 83
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 84
    const-string v1, "RoomStatusJson"

    invoke-virtual {p0}, Lcom/google/android/gms/games/h/a/ev;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const-string v2, "RoomStatusJson"

    aput-object v2, v1, v3

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;)Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 93
    const-string v0, "RoomStatusJson"

    invoke-virtual {p0, v0, v1, v1}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/ej;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 20

    .prologue
    .line 765
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->f()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x4

    if-ne v4, v5, :cond_0

    .line 767
    const-string v4, "MultiplayerUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Room is already deleted : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 768
    const/4 v4, 0x0

    .line 894
    :goto_0
    return-object v4

    .line 770
    :cond_0
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 773
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v4

    .line 774
    const-wide/16 v8, -0x1

    cmp-long v4, v4, v8

    if-nez v4, :cond_1

    .line 775
    const-string v4, "MultiplayerUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No game found matching external game ID "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 776
    const/4 v4, 0x0

    goto :goto_0

    .line 779
    :cond_1
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 780
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->e()Ljava/lang/String;

    move-result-object v5

    .line 783
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->getCreationDetails()Lcom/google/android/gms/games/h/a/er;

    move-result-object v6

    .line 784
    const-string v4, "Creation Details cannot be null for room with status: %d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->f()Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-static {v4, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v6, v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 787
    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/er;->c()Ljava/lang/String;

    move-result-object v10

    .line 788
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->getLastUpdateDetails()Lcom/google/android/gms/games/h/a/er;

    move-result-object v8

    .line 789
    if-nez v8, :cond_2

    const/4 v4, 0x0

    .line 793
    :goto_1
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    .line 794
    if-nez v11, :cond_3

    .line 795
    const-string v4, "MultiplayerUtils"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "No participants found for room "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 796
    const/4 v4, 0x0

    goto :goto_0

    .line 789
    :cond_2
    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/er;->c()Ljava/lang/String;

    move-result-object v4

    goto :goto_1

    .line 800
    :cond_3
    const-string v5, "creator_external"

    invoke-virtual {v9, v5, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 801
    const-string v5, "creation_timestamp"

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/er;->b()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v9, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 802
    const-string v5, "last_updater_external"

    invoke-virtual {v9, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    if-eqz v8, :cond_4

    .line 804
    const-string v4, "last_updated_timestamp"

    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/er;->b()Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v9, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 809
    :cond_4
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->getAutoMatchingCriteria()Lcom/google/android/gms/games/h/a/el;

    move-result-object v4

    .line 810
    if-eqz v4, :cond_5

    .line 811
    const-string v5, "has_automatch_criteria"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v9, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 812
    const-string v5, "automatch_min_players"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/el;->d()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 813
    const-string v5, "automatch_max_players"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/el;->c()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v9, v5, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 814
    const-string v5, "automatch_bit_mask"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/el;->b()Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v9, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 822
    :goto_2
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/ej;->getAutoMatchingStatus()Lcom/google/android/gms/games/h/a/ek;

    move-result-object v4

    .line 823
    if-eqz v4, :cond_6

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/ek;->b()Ljava/lang/Integer;

    move-result-object v5

    if-eqz v5, :cond_6

    .line 824
    const-string v5, "automatch_wait_estimate_sec"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/ek;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v9, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 832
    :goto_3
    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    .line 833
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v13

    .line 834
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 835
    new-instance v15, Ljava/util/ArrayList;

    invoke-direct {v15, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 836
    new-instance v16, Ljava/util/ArrayList;

    move-object/from16 v0, v16

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 837
    new-instance v17, Ljava/util/ArrayList;

    move-object/from16 v0, v17

    invoke-direct {v0, v12}, Ljava/util/ArrayList;-><init>(I)V

    .line 838
    new-instance v18, Ljava/util/ArrayList;

    invoke-direct/range {v18 .. v18}, Ljava/util/ArrayList;-><init>()V

    .line 839
    const/4 v6, 0x0

    .line 840
    const/4 v4, 0x0

    move v8, v4

    :goto_4
    if-ge v8, v12, :cond_7

    .line 841
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/h/a/eu;

    .line 842
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v5

    .line 843
    invoke-virtual {v5, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 844
    const/4 v5, 0x1

    .line 848
    :goto_5
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 849
    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 850
    move-object/from16 v0, p0

    invoke-static {v0, v4}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/eu;)Landroid/content/ContentValues;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 854
    const-string v19, "default_display_image_url"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 855
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v13, v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 856
    const-string v19, "default_display_hi_res_image_url"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 858
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v13, v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 860
    const-string v19, "profile_icon_image_url"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 861
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v13, v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 862
    const-string v19, "profile_hi_res_image_url"

    move-object/from16 v0, v19

    invoke-virtual {v6, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 863
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-static {v13, v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/net/Uri;Ljava/lang/String;Ljava/util/ArrayList;)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 866
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v4, v6}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 840
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    move v6, v5

    goto/16 :goto_4

    .line 816
    :cond_5
    const-string v4, "has_automatch_criteria"

    const/4 v5, 0x0

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v9, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 817
    const-string v4, "automatch_min_players"

    invoke-virtual {v9, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 818
    const-string v4, "automatch_max_players"

    invoke-virtual {v9, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 819
    const-string v4, "automatch_bit_mask"

    invoke-virtual {v9, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 827
    :cond_6
    const-string v4, "automatch_wait_estimate_sec"

    const/4 v5, -0x1

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v9, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto/16 :goto_3

    .line 868
    :cond_7
    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v12, :cond_8

    const/4 v4, 0x1

    :goto_6
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 869
    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v12, :cond_9

    const/4 v4, 0x1

    :goto_7
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 870
    invoke-virtual/range {v16 .. v16}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v12, :cond_a

    const/4 v4, 0x1

    :goto_8
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 871
    invoke-virtual/range {v17 .. v17}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ne v4, v12, :cond_b

    const/4 v4, 0x1

    :goto_9
    invoke-static {v4}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 874
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "MultiplayerUtils"

    move-object/from16 v0, v18

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v9

    .line 876
    const/4 v4, 0x0

    move v8, v4

    :goto_a
    if-ge v8, v12, :cond_c

    .line 877
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/h/a/eu;

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v4

    .line 878
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/content/ContentValues;

    .line 879
    const-string v13, "default_display_image_url"

    const-string v18, "default_display_image_uri"

    invoke-virtual {v14, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-static {v4, v13, v0, v9, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 881
    const-string v13, "default_display_hi_res_image_url"

    const-string v18, "default_display_hi_res_image_uri"

    invoke-virtual {v15, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-static {v4, v13, v0, v9, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 883
    const-string v13, "profile_icon_image_url"

    const-string v18, "profile_icon_image_uri"

    move-object/from16 v0, v16

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-static {v4, v13, v0, v9, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 885
    const-string v13, "profile_hi_res_image_url"

    const-string v18, "profile_hi_res_image_uri"

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/Integer;

    move-object/from16 v0, v18

    invoke-static {v4, v13, v0, v9, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Integer;)V

    .line 876
    add-int/lit8 v4, v8, 0x1

    move v8, v4

    goto :goto_a

    .line 868
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_6

    .line 869
    :cond_9
    const/4 v4, 0x0

    goto :goto_7

    .line 870
    :cond_a
    const/4 v4, 0x0

    goto :goto_8

    .line 871
    :cond_b
    const/4 v4, 0x0

    goto :goto_9

    .line 890
    :cond_c
    if-nez v6, :cond_d

    .line 891
    const-string v4, "MultiplayerUtils"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "No room-creating player found for external ID "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    const/4 v4, 0x0

    goto/16 :goto_0

    :cond_d
    move-object v4, v7

    .line 894
    goto/16 :goto_0

    :cond_e
    move v5, v6

    goto/16 :goto_5
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 992
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 993
    sget-object v0, Lcom/google/android/gms/games/c/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 994
    const-wide/16 v2, -0x1

    .line 996
    const-string v8, "3"

    .line 999
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1000
    new-instance v4, Lcom/google/android/gms/common/e/b;

    invoke-direct {v4, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1001
    const-string v0, "user_match_status"

    invoke-virtual {v4, v0, v8}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1002
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v4, Lcom/google/android/gms/games/a/bg;->a:[Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v4, "game_id,last_updated_timestamp DESC"

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v9

    move v0, v1

    .line 1008
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v9}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1009
    const/4 v4, 0x1

    invoke-interface {v9, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1010
    cmp-long v10, v4, v2

    if-eqz v10, :cond_1

    move v0, v1

    move-wide v2, v4

    .line 1015
    :cond_1
    if-ge v0, v7, :cond_2

    .line 1016
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1018
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v6, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 1019
    const/4 v10, 0x2

    invoke-interface {v9, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 1020
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1025
    :catchall_0
    move-exception v0

    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 1030
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 1031
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1032
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1033
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1034
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v0

    .line 1035
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1036
    const-string v7, "user_match_status"

    invoke-virtual {v1, v7, v8}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1037
    const-string v7, "last_updated_timestamp"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "<=?"

    invoke-virtual {v1, v7, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1038
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v4

    iget-object v1, v1, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v0, v4, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1044
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1045
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "MultiplayerUtils"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1047
    :cond_5
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;Ljava/lang/String;ILcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)V
    .locals 14

    .prologue
    .line 654
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/fl;->getParticipants()Ljava/util/ArrayList;

    move-result-object v8

    .line 655
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v9

    .line 656
    const/4 v2, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v7, v2

    :goto_0
    if-ge v7, v10, :cond_d

    .line 657
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/fq;

    .line 658
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->b()Ljava/lang/String;

    move-result-object v5

    .line 659
    invoke-static {v5}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 660
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/fl;->getResults()Ljava/util/ArrayList;

    move-result-object v6

    if-eqz v6, :cond_0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_5

    :cond_0
    const/4 v3, 0x0

    .line 663
    :cond_1
    :goto_1
    const/4 v4, -0x1

    .line 664
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v5

    .line 665
    if-eqz v5, :cond_2

    .line 666
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v4

    .line 667
    iget-object v5, v5, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v12

    invoke-static {p1, v5, v12, v13}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v5

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    :cond_2
    iget-object v6, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    if-eqz v3, :cond_3

    const-string v11, "result_type"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/ch;->d()Ljava/lang/String;

    move-result-object v5

    const-string v12, "MATCH_RESULT_WIN"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_7

    const/4 v5, 0x0

    :goto_2
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v6, v11, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v5, "placing"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/ch;->c()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v6, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v2

    if-eqz v2, :cond_e

    const-string v3, "default_display_name"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/s;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "default_display_image_url"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v6, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v2

    const-string v3, "default_display_hi_res_image_url"

    invoke-static {p0, v6, v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v2

    .line 673
    :goto_3
    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    move-object/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    .line 677
    const/4 v3, -0x1

    if-eq v4, v3, :cond_4

    .line 678
    const-string v3, "player_id"

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 680
    :cond_4
    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 656
    add-int/lit8 v2, v7, 0x1

    move v7, v2

    goto/16 :goto_0

    .line 660
    :cond_5
    const/4 v3, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v4, v3

    :goto_4
    if-ge v4, v11, :cond_6

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/h/a/ch;

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/ch;->b()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v12, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_1

    add-int/lit8 v3, v4, 0x1

    move v4, v3

    goto :goto_4

    :cond_6
    const/4 v3, 0x0

    goto/16 :goto_1

    .line 672
    :cond_7
    const-string v12, "MATCH_RESULT_LOSS"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_8

    const/4 v5, 0x1

    goto :goto_2

    :cond_8
    const-string v12, "MATCH_RESULT_TIE"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_9

    const/4 v5, 0x2

    goto/16 :goto_2

    :cond_9
    const-string v12, "MATCH_RESULT_NONE"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_a

    const/4 v5, 0x3

    goto/16 :goto_2

    :cond_a
    const-string v12, "MATCH_RESULT_DISCONNECT"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_b

    const/4 v5, 0x4

    goto/16 :goto_2

    :cond_b
    const-string v12, "MATCH_RESULT_DISAGREED"

    invoke-virtual {v5, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_c

    const/4 v5, 0x5

    goto/16 :goto_2

    :cond_c
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unknown result string: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 682
    :cond_d
    return-void

    :cond_e
    move-object v2, v6

    goto/16 :goto_3
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/ArrayList;Ljava/lang/String;ILcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)V
    .locals 8

    .prologue
    .line 307
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    .line 308
    const/4 v0, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v0

    :goto_0
    if-ge v3, v5, :cond_3

    .line 309
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/eu;

    .line 310
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v1

    .line 311
    invoke-static {v1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 314
    const/4 v1, -0x1

    .line 315
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v2

    .line 316
    if-eqz v2, :cond_0

    .line 317
    invoke-virtual {p6}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 318
    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-interface {p5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {p1, v2, v6, v7}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {p6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 324
    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->getClientAddress()Lcom/google/android/gms/games/h/a/em;

    move-result-object v6

    if-eqz v6, :cond_1

    const-string v7, "client_address"

    invoke-virtual {v6}, Lcom/google/android/gms/games/h/a/em;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v7, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v0

    if-eqz v0, :cond_4

    const-string v6, "default_display_name"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/s;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "default_display_image_url"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/s;->b()Ljava/lang/String;

    move-result-object v0

    const-string v6, "default_display_hi_res_image_url"

    invoke-static {p0, v2, v0, v6}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 326
    :goto_1
    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, p3, p4}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 330
    const/4 v2, -0x1

    if-eq v1, v2, :cond_2

    .line 331
    const-string v2, "player_id"

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    .line 333
    :cond_2
    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p6, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 308
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 335
    :cond_3
    return-void

    :cond_4
    move-object v0, v2

    goto :goto_1
.end method

.method static a(Landroid/content/Context;Ljava/util/HashMap;Landroid/content/ContentValues;Lcom/google/android/gms/games/h/a/ev;)V
    .locals 11

    .prologue
    .line 946
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->b()Ljava/lang/String;

    move-result-object v4

    .line 947
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->c()Ljava/lang/Integer;

    move-result-object v5

    .line 948
    const/4 v0, -0x1

    .line 953
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->getAutoMatchingStatus()Lcom/google/android/gms/games/h/a/ek;

    move-result-object v1

    .line 954
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/ek;->b()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_2

    .line 955
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/a/ek;->b()Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    move v2, v0

    .line 958
    :goto_0
    invoke-virtual {p3}, Lcom/google/android/gms/games/h/a/ev;->getParticipants()Ljava/util/ArrayList;

    move-result-object v6

    .line 959
    const/4 v0, 0x0

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v0

    :goto_1
    if-ge v3, v7, :cond_1

    .line 960
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/eu;

    .line 961
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/content/ContentValues;

    .line 962
    if-eqz v1, :cond_0

    .line 963
    const-string v8, "status"

    invoke-virtual {v1, v8, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 964
    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/eu;)Landroid/content/ContentValues;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 979
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eu;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 959
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_1

    .line 967
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 968
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 969
    invoke-virtual {v8, p2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 970
    const-string v9, "external_match_id"

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 971
    const-string v9, "status"

    invoke-virtual {v8, v9, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 972
    const-string v9, "automatch_wait_estimate_sec"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-virtual {v8, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 974
    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 975
    invoke-static {p0, v0}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/games/h/a/eu;)Landroid/content/ContentValues;

    move-result-object v8

    invoke-virtual {v1, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    goto :goto_2

    .line 981
    :cond_1
    return-void

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 494
    invoke-static {p0, p1}, Lcom/google/android/gms/games/provider/ak;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 495
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 496
    invoke-static {p0, p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 498
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 501
    return-void
.end method

.method static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I
    .locals 15

    .prologue
    .line 353
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v4

    .line 354
    if-nez v4, :cond_1

    .line 355
    const/4 v6, -0x1

    .line 453
    :cond_0
    :goto_0
    return v6

    .line 357
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->getCreationDetails()Lcom/google/android/gms/games/h/a/fp;

    move-result-object v9

    .line 358
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->getLastUpdateDetails()Lcom/google/android/gms/games/h/a/fp;

    move-result-object v10

    .line 359
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->c()Ljava/lang/String;

    move-result-object v2

    .line 360
    if-nez v2, :cond_a

    .line 361
    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/fp;->c()Ljava/lang/String;

    move-result-object v2

    move-object v3, v2

    .line 365
    :goto_1
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->getParticipants()Ljava/util/ArrayList;

    move-result-object v11

    .line 366
    if-nez v11, :cond_2

    .line 367
    const-string v2, "MultiplayerUtils"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "No participants found for invitation "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 368
    const/4 v6, -0x1

    goto :goto_0

    .line 373
    :cond_2
    const/4 v7, 0x0

    .line 374
    const/4 v6, 0x0

    .line 375
    const/4 v5, 0x0

    .line 376
    const/4 v2, 0x0

    invoke-virtual {v11}, Ljava/util/ArrayList;->size()I

    move-result v12

    move v8, v2

    :goto_2
    if-ge v8, v12, :cond_9

    .line 377
    invoke-virtual {v11, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/fq;

    .line 378
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->b()Ljava/lang/String;

    move-result-object v13

    .line 379
    invoke-virtual {v13, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 380
    const/4 v6, 0x1

    .line 381
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->getPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v8

    .line 382
    if-eqz v8, :cond_4

    .line 383
    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/cs;->b()Ljava/lang/String;

    move-result-object v5

    .line 384
    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/cs;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    move v14, v6

    move-object v6, v5

    move v5, v14

    .line 393
    :goto_3
    if-eqz v5, :cond_3

    if-nez v6, :cond_6

    .line 394
    :cond_3
    const-string v2, "MultiplayerUtils"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "No inviting player found for external ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const/4 v6, -0x1

    goto :goto_0

    .line 385
    :cond_4
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v8

    if-eqz v8, :cond_9

    .line 386
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fq;->getAutoMatchedPlayer()Lcom/google/android/gms/games/h/a/s;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/s;->c()Ljava/lang/String;

    move-result-object v2

    move v14, v5

    move v5, v6

    move-object v6, v2

    move v2, v14

    goto :goto_3

    .line 376
    :cond_5
    add-int/lit8 v2, v8, 0x1

    move v8, v2

    goto :goto_2

    .line 399
    :cond_6
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 400
    const-string v6, "game_id"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 401
    const-string v6, "external_invitation_id"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 402
    const-string v6, "external_inviter_id"

    invoke-virtual {v5, v6, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 403
    const-string v3, "creation_timestamp"

    invoke-virtual {v9}, Lcom/google/android/gms/games/h/a/fp;->b()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 405
    const-string v3, "last_modified_timestamp"

    invoke-virtual {v10}, Lcom/google/android/gms/games/h/a/fp;->b()Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 407
    const-string v3, "type"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 408
    const-string v3, "inviter_in_circles"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 409
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->h()Ljava/lang/Integer;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 410
    const-string v2, "variant"

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->h()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 414
    :cond_7
    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->getAutoMatchingCriteria()Lcom/google/android/gms/games/h/a/fk;

    move-result-object v2

    .line 415
    if-eqz v2, :cond_8

    .line 416
    const-string v3, "has_automatch_criteria"

    const/4 v6, 0x1

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 417
    const-string v3, "automatch_min_players"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fk;->d()Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 419
    const-string v3, "automatch_max_players"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fk;->c()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v5, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 428
    :goto_4
    invoke-virtual/range {p6 .. p6}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 429
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ae;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 436
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/ao;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const-string v3, "invitation_id=?"

    sget-object v5, Lcom/google/android/gms/games/a/bf;->a:[Ljava/lang/String;

    invoke-virtual {v2, v3, v5}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v6}, Landroid/content/ContentProviderOperation$Builder;->withSelectionBackReference(II)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 442
    const-string v5, "invitation_id"

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-static/range {v2 .. v8}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;Ljava/lang/String;ILcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)V

    .line 446
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v9

    invoke-virtual {v4}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object v7, p0

    move-object/from16 v8, p1

    move-wide/from16 v10, p3

    invoke-static/range {v7 .. v13}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bj;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v2

    .line 448
    if-eqz v2, :cond_0

    .line 449
    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 422
    :cond_8
    const-string v2, "has_automatch_criteria"

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v5, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 423
    const-string v2, "automatch_min_players"

    invoke-virtual {v5, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 424
    const-string v2, "automatch_max_players"

    invoke-virtual {v5, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    goto :goto_4

    :cond_9
    move v2, v5

    move v5, v6

    move-object v6, v7

    goto/16 :goto_3

    :cond_a
    move-object v3, v2

    goto/16 :goto_1
.end method

.method static c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bi;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I
    .locals 9

    .prologue
    .line 471
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/bi;->getTurnBasedMatch()Lcom/google/android/gms/games/h/a/fl;

    move-result-object v3

    move-object v1, p0

    move-object v2, p1

    move-wide v4, p3

    move-object v6, p5

    move-object v7, p6

    .line 472
    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/bf;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/fl;JLcom/google/android/gms/common/util/p;Ljava/util/ArrayList;)I

    move-result v0

    .line 476
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 477
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/bi;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v4

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/fl;->d()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x2

    move-object v1, p0

    move-object v2, p1

    move-object v3, v4

    move-wide v4, p3

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bj;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v1

    .line 480
    invoke-virtual {p6, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 482
    :cond_0
    return v0
.end method
