.class final Lcom/google/android/gms/car/hh;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/lh;

.field final synthetic b:Lcom/google/android/gms/car/gx;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/lh;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/google/android/gms/car/hh;->b:Lcom/google/android/gms/car/gx;

    iput-object p2, p0, Lcom/google/android/gms/car/hh;->a:Lcom/google/android/gms/car/lh;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final binderDied()V
    .locals 2

    .prologue
    .line 365
    const-string v0, "CAR.SERVICE"

    const-string v1, "a car connection listener died, remove from list"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 366
    iget-object v0, p0, Lcom/google/android/gms/car/hh;->a:Lcom/google/android/gms/car/lh;

    invoke-interface {v0}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/car/hh;->b:Lcom/google/android/gms/car/gx;

    iget-object v1, p0, Lcom/google/android/gms/car/hh;->a:Lcom/google/android/gms/car/lh;

    invoke-interface {v1}, Lcom/google/android/gms/car/lh;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Landroid/os/IBinder;)V

    .line 368
    return-void
.end method
