.class public abstract Lcom/google/android/gms/udc/c/d;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/udc/c/c;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/udc/c/d;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/udc/c/c;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/udc/c/c;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/udc/c/c;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/udc/c/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/udc/c/e;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 126
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 48
    goto :goto_0

    .line 52
    :sswitch_1
    const-string v2, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 55
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 61
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 62
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/udc/c/d;->a(Lcom/google/android/gms/common/api/Status;[B)V

    move v0, v1

    .line 63
    goto :goto_0

    .line 67
    :sswitch_2
    const-string v2, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 70
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 76
    :cond_1
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 77
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/udc/c/d;->b(Lcom/google/android/gms/common/api/Status;[B)V

    move v0, v1

    .line 78
    goto :goto_0

    .line 82
    :sswitch_3
    const-string v2, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 84
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_2

    .line 85
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 91
    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 92
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/udc/c/d;->c(Lcom/google/android/gms/common/api/Status;[B)V

    move v0, v1

    .line 93
    goto :goto_0

    .line 97
    :sswitch_4
    const-string v2, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 99
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_3

    .line 100
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 106
    :cond_3
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v2

    .line 107
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/udc/c/d;->d(Lcom/google/android/gms/common/api/Status;[B)V

    move v0, v1

    .line 108
    goto :goto_0

    .line 112
    :sswitch_5
    const-string v2, "com.google.android.gms.udc.internal.IUdcCallbacks"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_4

    .line 115
    sget-object v0, Lcom/google/android/gms/common/api/Status;->CREATOR:Lcom/google/android/gms/common/api/as;

    invoke-static {p2}, Lcom/google/android/gms/common/api/as;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 121
    :cond_4
    sget-object v2, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->createTypedArrayList(Landroid/os/Parcelable$Creator;)Ljava/util/ArrayList;

    move-result-object v2

    .line 122
    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/udc/c/d;->a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V

    move v0, v1

    .line 123
    goto/16 :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
