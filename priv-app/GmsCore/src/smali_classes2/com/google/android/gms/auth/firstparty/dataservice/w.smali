.class public final Lcom/google/android/gms/auth/firstparty/dataservice/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/firstparty/dataservice/u;


# instance fields
.field public final a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    .line 82
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;
    .locals 1

    .prologue
    .line 284
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ac;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ac;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryDataRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/AccountRecoveryData;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;
    .locals 1

    .prologue
    .line 432
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ak;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ak;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/CheckFactoryResetPolicyComplianceResponse;

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/x;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/GoogleAccountData;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/OtpRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/OtpResponse;
    .locals 1

    .prologue
    .line 419
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/aj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/aj;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/OtpRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/OtpResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    .prologue
    .line 207
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/z;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/z;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/AccountSignInRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    .prologue
    .line 239
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/aa;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/aa;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/ConfirmCredentialsRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;
    .locals 1

    .prologue
    .line 182
    const-string v0, "TokenRequest cannot be null!"

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 183
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/ao;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ao;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Lcom/google/android/gms/auth/firstparty/dataservice/TokenRequest;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/firstparty/dataservice/TokenResponse;

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;
    .locals 7

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 493
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "com.google.android.gms.auth.DATA_PROXY"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "android.intent.category.DEFAULT"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/auth/r;->b:Ljava/lang/String;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 512
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 514
    :try_start_0
    new-instance v1, Lcom/google/android/gms/common/b;

    invoke-direct {v1}, Lcom/google/android/gms/common/b;-><init>()V

    .line 515
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    const/4 v6, 0x1

    invoke-virtual {v4, v5, v0, v1, v6}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-eqz v0, :cond_0

    .line 518
    :try_start_1
    invoke-virtual {v1}, Lcom/google/android/gms/common/b;->a()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/aw;->a(Landroid/os/IBinder;)Lcom/google/android/gms/auth/firstparty/dataservice/av;

    move-result-object v0

    .line 520
    invoke-interface {p1, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ap;->a(Lcom/google/android/gms/auth/firstparty/dataservice/av;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v0

    .line 528
    :try_start_2
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    invoke-virtual {v4, v5, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 533
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    :goto_0
    return-object v0

    .line 521
    :catch_0
    move-exception v0

    .line 522
    :try_start_3
    const-string v4, "GoogleAccountDataServiceClient"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "[GoogleAccountDataServiceClient]  Interrupted when getting service: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/InterruptedException;->getMessage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/aq;

    invoke-direct {v4, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/aq;-><init>(Ljava/lang/InterruptedException;)V

    throw v4
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 528
    :catchall_0
    move-exception v0

    :try_start_4
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a:Landroid/content/Context;

    invoke-virtual {v4, v5, v1}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 531
    :catchall_1
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0

    .line 524
    :catch_1
    move-exception v0

    .line 525
    :try_start_5
    const-string v4, "GoogleAccountDataServiceClient"

    const-string v5, "[GoogleAccountDataServiceClient]  RemoteException when executing call!"

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 526
    new-instance v4, Lcom/google/android/gms/auth/firstparty/dataservice/ar;

    invoke-direct {v4, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/ar;-><init>(Landroid/os/RemoteException;)V

    throw v4
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 531
    :cond_0
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 449
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/al;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/firstparty/dataservice/al;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    .line 458
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 170
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/an;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/an;-><init>(Lcom/google/android/gms/auth/firstparty/dataservice/w;Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/firstparty/dataservice/w;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ap;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method
