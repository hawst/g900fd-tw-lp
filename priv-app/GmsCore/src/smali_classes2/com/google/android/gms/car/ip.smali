.class public final Lcom/google/android/gms/car/ip;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;


# static fields
.field private static final v:Landroid/util/Pair;

.field private static final w:Landroid/util/Pair;


# instance fields
.field private final a:Lcom/google/android/gms/car/iu;

.field private final b:Landroid/graphics/Point;

.field private final c:I

.field private final d:I

.field private e:I

.field private f:I

.field private g:I

.field private h:I

.field private i:I

.field private j:I

.field private k:I

.field private final l:Lcom/google/android/gms/car/is;

.field private m:Lcom/google/android/gms/car/senderprotocol/be;

.field private n:Lcom/google/android/gms/car/jv;

.field private volatile o:Lcom/google/android/gms/car/iv;

.field private p:[Lcom/google/android/c/b/cz;

.field private final q:Lcom/google/android/gms/car/gx;

.field private final r:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private s:Z

.field private t:Z

.field private final u:Ljava/util/concurrent/Semaphore;

.field private final x:Ljava/util/concurrent/LinkedBlockingQueue;

.field private final y:Ljava/lang/Thread;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 80
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/car/ip;->v:Landroid/util/Pair;

    .line 82
    new-instance v0, Landroid/util/Pair;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, v2}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    sput-object v0, Lcom/google/android/gms/car/ip;->w:Landroid/util/Pair;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/is;Lcom/google/android/gms/car/gx;II)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 145
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    new-instance v0, Landroid/graphics/Point;

    invoke-direct {v0}, Landroid/graphics/Point;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    .line 71
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 75
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->u:Ljava/util/concurrent/Semaphore;

    .line 85
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->x:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 87
    new-instance v0, Ljava/lang/Thread;

    new-instance v1, Lcom/google/android/gms/car/iq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/iq;-><init>(Lcom/google/android/gms/car/ip;)V

    const-string v2, "VideoFocusHandling"

    invoke-direct {v0, v1, v2}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->y:Ljava/lang/Thread;

    .line 146
    iput p3, p0, Lcom/google/android/gms/car/ip;->c:I

    .line 147
    iput p4, p0, Lcom/google/android/gms/car/ip;->d:I

    .line 148
    iput-object p1, p0, Lcom/google/android/gms/car/ip;->l:Lcom/google/android/gms/car/is;

    .line 149
    iput-object p2, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    .line 150
    new-instance v0, Lcom/google/android/gms/car/iu;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/iu;-><init>(Lcom/google/android/gms/car/ip;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->a:Lcom/google/android/gms/car/iu;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->y:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 152
    return-void
.end method

.method private a(I)I
    .locals 10

    .prologue
    const/4 v0, 0x3

    .line 380
    invoke-static {}, Lcom/google/android/gms/car/jv;->c()Lcom/google/android/gms/car/jw;

    move-result-object v1

    .line 381
    iget v2, p0, Lcom/google/android/gms/car/ip;->c:I

    iget v3, v1, Lcom/google/android/gms/car/jw;->a:I

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    .line 382
    iget v3, p0, Lcom/google/android/gms/car/ip;->d:I

    iget v4, v1, Lcom/google/android/gms/car/jw;->b:I

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 383
    const-string v4, "CAR.VIDEO"

    invoke-static {v4, v0}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 384
    const-string v4, "CAR.VIDEO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "disp "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v6, p0, Lcom/google/android/gms/car/ip;->c:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p0, Lcom/google/android/gms/car/ip;->d:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " codec "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/google/android/gms/car/jw;->a:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ","

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/google/android/gms/car/jw;->b:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " codec fps "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v1, Lcom/google/android/gms/car/jw;->c:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_0
    iget v4, v1, Lcom/google/android/gms/car/jw;->a:I

    iget v5, v1, Lcom/google/android/gms/car/jw;->b:I

    mul-int/2addr v4, v5

    iget v1, v1, Lcom/google/android/gms/car/jw;->c:I

    mul-int/2addr v1, v4

    int-to-long v4, v1

    .line 390
    const/16 v1, 0x780

    if-lt v2, v1, :cond_1

    const/16 v1, 0x438

    if-lt v3, v1, :cond_1

    const-wide/32 v6, 0x1fa400

    div-long v6, v4, v6

    int-to-long v8, p1

    cmp-long v1, v6, v8

    if-ltz v1, :cond_1

    .line 397
    :goto_0
    return v0

    .line 393
    :cond_1
    const/16 v0, 0x500

    if-lt v2, v0, :cond_2

    const/16 v0, 0x2d0

    if-lt v3, v0, :cond_2

    const-wide/32 v0, 0xe1000

    div-long v0, v4, v0

    int-to-long v2, p1

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 395
    const/4 v0, 0x2

    goto :goto_0

    .line 397
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static a(Lcom/google/android/c/b/cz;)I
    .locals 2

    .prologue
    .line 325
    invoke-virtual {p0}, Lcom/google/android/c/b/cz;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/c/b/cz;->c()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 326
    const/16 v0, 0x3c

    .line 328
    :goto_0
    return v0

    :cond_0
    const/16 v0, 0x1e

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ip;)Ljava/util/concurrent/LinkedBlockingQueue;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->x:Ljava/util/concurrent/LinkedBlockingQueue;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ip;Z)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ip;->a(Z)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ip;[I)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ip;->b([I)V

    return-void
.end method

.method private declared-synchronized a(Z)V
    .locals 2

    .prologue
    .line 347
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 348
    const-string v0, "CAR.VIDEO"

    const-string v1, "doHandleVideoFocusLoss"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    if-eqz v0, :cond_1

    .line 351
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ip;->b(Z)V

    .line 352
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->l_()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 354
    :cond_1
    monitor-exit p0

    return-void

    .line 347
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static a(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 304
    invoke-virtual {p0}, Lcom/google/android/c/b/cz;->b()I

    move-result v1

    const/4 v2, 0x3

    if-ne v1, v2, :cond_0

    .line 305
    const/16 v1, 0x780

    const/16 v2, 0x438

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    .line 314
    :goto_0
    return v0

    .line 307
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/c/b/cz;->b()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 308
    const/16 v1, 0x500

    const/16 v2, 0x2d0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 310
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/c/b/cz;->b()I

    move-result v1

    if-ne v1, v0, :cond_2

    .line 311
    const/16 v1, 0x320

    const/16 v2, 0x1e0

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Point;->set(II)V

    goto :goto_0

    .line 314
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/senderprotocol/be;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    return-object v0
.end method

.method private b(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-static {p1, v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)Z

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v0, Landroid/graphics/Point;->x:I

    invoke-virtual {p1}, Lcom/google/android/c/b/cz;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/c/b/cz;->e()I

    move-result v0

    :goto_0
    sub-int v0, v2, v0

    .line 320
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {p1}, Lcom/google/android/c/b/cz;->h()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {p1}, Lcom/google/android/c/b/cz;->g()I

    move-result v1

    :cond_0
    sub-int v1, v2, v1

    .line 321
    invoke-virtual {p2, v0, v1}, Landroid/graphics/Point;->set(II)V

    .line 322
    return-void

    :cond_1
    move v0, v1

    .line 319
    goto :goto_0
.end method

.method private declared-synchronized b(Z)V
    .locals 4

    .prologue
    .line 480
    monitor-enter p0

    if-eqz p1, :cond_0

    .line 481
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->a:Lcom/google/android/gms/car/iu;

    invoke-virtual {v0}, Lcom/google/android/gms/car/iu;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 483
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->u:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x7530

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    const/16 v1, 0x9

    const-string v2, "handling display removal timed-out"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 492
    :cond_0
    :goto_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    if-eqz v0, :cond_1

    .line 493
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    invoke-virtual {v0}, Lcom/google/android/gms/car/jv;->b()V

    .line 494
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 496
    :cond_1
    monitor-exit p0

    return-void

    .line 480
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized b([I)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 227
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 228
    const-string v0, "CAR.VIDEO"

    const-string v1, "doHandleVideoFocusGained"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 231
    :cond_0
    const/16 v0, 0x3c

    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ip;->a(I)I

    .line 232
    const/16 v0, 0x1e

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ip;->a(I)I

    .line 233
    sget-object v0, Lcom/google/android/gms/car/hz;->c:Lcom/google/android/gms/car/io;

    .line 234
    const/4 v3, 0x0

    .line 242
    const/4 v1, -0x1

    .line 243
    array-length v6, p1

    const/4 v0, 0x0

    move v5, v0

    :goto_0
    if-ge v5, v6, :cond_9

    aget v0, p1, v5

    .line 244
    if-ltz v0, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/car/ip;->p:[Lcom/google/android/c/b/cz;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/car/ip;->p:[Lcom/google/android/c/b/cz;

    array-length v2, v2

    if-lt v0, v2, :cond_2

    .line 246
    :cond_1
    const-string v1, "CAR.VIDEO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "car sent wrong configuration index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 247
    new-instance v1, Lcom/google/android/gms/car/ed;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "wrong index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catch Lcom/google/android/gms/car/ed; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 294
    :catch_0
    move-exception v0

    .line 295
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 297
    :goto_1
    monitor-exit p0

    return-void

    .line 250
    :cond_2
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->p:[Lcom/google/android/c/b/cz;

    aget-object v2, v2, v0

    .line 251
    const-string v4, "CAR.VIDEO"

    const/4 v7, 0x3

    invoke-static {v4, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 252
    const-string v7, "CAR.VIDEO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v8, "Checcking video config at index "

    invoke-direct {v4, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " codec:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->b()I

    move-result v8

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v8, " fps:"

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->c()I

    move-result v4

    if-ne v4, v9, :cond_4

    const-string v4, "60"

    :goto_2
    invoke-virtual {v8, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v7, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_3
    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->c()I

    .line 259
    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->b()I

    move-result v4

    if-gt v4, v9, :cond_5

    move v1, v0

    move-object v0, v2

    .line 267
    :goto_3
    if-nez v0, :cond_6

    .line 268
    const-string v0, "CAR.VIDEO"

    const-string v1, "No working configuration"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    new-instance v0, Lcom/google/android/gms/car/ed;

    const-string v1, "no working configuration"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Lcom/google/android/gms/car/ed; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 252
    :cond_4
    :try_start_4
    const-string v4, "30"

    goto :goto_2

    .line 243
    :cond_5
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto/16 :goto_0

    .line 272
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)Z

    .line 273
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/google/android/gms/car/ip;->e:I

    .line 274
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/google/android/gms/car/ip;->f:I

    .line 275
    invoke-static {v0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/car/ip;->g:I

    .line 276
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)V

    .line 277
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    iput v2, p0, Lcom/google/android/gms/car/ip;->h:I

    .line 278
    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    iput v2, p0, Lcom/google/android/gms/car/ip;->i:I

    .line 279
    invoke-virtual {v0}, Lcom/google/android/c/b/cz;->i()I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/car/ip;->j:I

    .line 281
    invoke-virtual {v0}, Lcom/google/android/c/b/cz;->l()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-virtual {v0}, Lcom/google/android/c/b/cz;->k()I

    move-result v0

    :goto_4
    iput v0, p0, Lcom/google/android/gms/car/ip;->k:I

    .line 284
    const-string v0, "CAR.VIDEO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 285
    const-string v0, "CAR.VIDEO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Accessory display sink available: config index "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", codecWidth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->e:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", codecHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->f:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dispWidth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->h:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", dispHeight="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->i:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", densityDpi="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->j:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", fps "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->g:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", decoder depth="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/ip;->k:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/be;->b(I)V

    .line 293
    invoke-direct {p0}, Lcom/google/android/gms/car/ip;->g()V
    :try_end_4
    .catch Lcom/google/android/gms/car/ed; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_1

    .line 281
    :cond_8
    const/4 v0, 0x4

    goto :goto_4

    :cond_9
    move-object v0, v3

    goto/16 :goto_3
.end method

.method static synthetic c(Lcom/google/android/gms/car/ip;)Lcom/google/android/gms/car/iv;
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->o:Lcom/google/android/gms/car/iv;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/car/it;)V
    .locals 9

    .prologue
    .line 523
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->l:Lcom/google/android/gms/car/is;

    iget v1, p1, Lcom/google/android/gms/car/it;->a:I

    iget v2, p1, Lcom/google/android/gms/car/it;->b:I

    iget v3, p1, Lcom/google/android/gms/car/it;->c:I

    iget v4, p1, Lcom/google/android/gms/car/it;->d:I

    iget v5, p1, Lcom/google/android/gms/car/it;->e:I

    iget v6, p1, Lcom/google/android/gms/car/it;->f:I

    iget v7, p1, Lcom/google/android/gms/car/it;->g:I

    iget-object v8, p1, Lcom/google/android/gms/car/it;->h:Landroid/view/Surface;

    invoke-interface/range {v0 .. v8}, Lcom/google/android/gms/car/is;->a(IIIIIIILandroid/view/Surface;)V

    .line 532
    return-void
.end method

.method private declared-synchronized g()V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 402
    monitor-enter p0

    const/4 v0, 0x0

    .line 403
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/car/id;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 404
    new-instance v0, Lcom/google/android/gms/car/ns;

    iget-object v1, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->F()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ns;-><init>(Landroid/content/Context;)V

    .line 406
    :cond_0
    new-instance v1, Lcom/google/android/gms/car/jv;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/jv;-><init>(Lcom/google/android/gms/car/ns;)V

    iput-object v1, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    iget v1, p0, Lcom/google/android/gms/car/ip;->e:I

    iget v2, p0, Lcom/google/android/gms/car/ip;->f:I

    iget v3, p0, Lcom/google/android/gms/car/ip;->g:I

    iget-object v4, v0, Lcom/google/android/gms/car/jv;->a:Lcom/google/android/gms/car/jw;

    iget v4, v4, Lcom/google/android/gms/car/jw;->a:I

    if-gt v1, v4, :cond_1

    iget-object v4, v0, Lcom/google/android/gms/car/jv;->a:Lcom/google/android/gms/car/jw;

    iget v4, v4, Lcom/google/android/gms/car/jw;->b:I

    if-le v2, v4, :cond_2

    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "width "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " height "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too big"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 402
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 407
    :cond_2
    :try_start_1
    iget-boolean v4, v0, Lcom/google/android/gms/car/np;->i:Z

    if-eqz v4, :cond_3

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "configure called while encoding is on-going"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    iput v1, v0, Lcom/google/android/gms/car/jv;->b:I

    iput v2, v0, Lcom/google/android/gms/car/jv;->c:I

    iput v3, v0, Lcom/google/android/gms/car/jv;->e:I

    iget v1, v0, Lcom/google/android/gms/car/jv;->c:I

    iget v2, v0, Lcom/google/android/gms/car/jv;->b:I

    invoke-static {v1, v2}, Ljava/lang/Math;->max(II)I

    move-result v1

    const/16 v2, 0x780

    if-lt v1, v2, :cond_6

    const v1, 0x7a1200

    iput v1, v0, Lcom/google/android/gms/car/jv;->d:I

    :goto_0
    iget v1, v0, Lcom/google/android/gms/car/jv;->e:I

    const/16 v2, 0x3c

    if-ne v1, v2, :cond_4

    iget v1, v0, Lcom/google/android/gms/car/jv;->d:I

    mul-int/lit8 v1, v1, 0x2

    iput v1, v0, Lcom/google/android/gms/car/jv;->d:I

    :cond_4
    const-string v1, "CAR.VIDEO"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    const-string v1, "CAR.VIDEO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Configuring codec with width: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/gms/car/jv;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " height: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, v0, Lcom/google/android/gms/car/jv;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " bit rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/gms/car/jv;->d:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 408
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    new-instance v1, Lcom/google/android/gms/car/ir;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ir;-><init>(Lcom/google/android/gms/car/ip;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/jv;->a(Lcom/google/android/gms/car/nr;)Z

    move-result v0

    .line 455
    if-nez v0, :cond_9

    .line 456
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->l:Lcom/google/android/gms/car/is;

    const-string v1, "cannot start video encoding"

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/is;->c(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 460
    :goto_1
    monitor-exit p0

    return-void

    .line 407
    :cond_6
    const/16 v2, 0x500

    if-lt v1, v2, :cond_7

    const v1, 0x5b8d80

    :try_start_2
    iput v1, v0, Lcom/google/android/gms/car/jv;->d:I

    goto :goto_0

    :cond_7
    const/16 v2, 0x320

    if-lt v1, v2, :cond_8

    const v1, 0x3d0900

    iput v1, v0, Lcom/google/android/gms/car/jv;->d:I

    goto :goto_0

    :cond_8
    const v1, 0x1e8480

    iput v1, v0, Lcom/google/android/gms/car/jv;->d:I

    goto :goto_0

    .line 459
    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->a:Lcom/google/android/gms/car/iu;

    invoke-direct {p0}, Lcom/google/android/gms/car/ip;->h()Lcom/google/android/gms/car/it;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/iu;->a(Lcom/google/android/gms/car/it;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method private h()Lcom/google/android/gms/car/it;
    .locals 2

    .prologue
    .line 463
    new-instance v0, Lcom/google/android/gms/car/it;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/it;-><init>(Lcom/google/android/gms/car/ip;)V

    .line 464
    iget v1, p0, Lcom/google/android/gms/car/ip;->e:I

    iput v1, v0, Lcom/google/android/gms/car/it;->a:I

    .line 465
    iget v1, p0, Lcom/google/android/gms/car/ip;->f:I

    iput v1, v0, Lcom/google/android/gms/car/it;->b:I

    .line 466
    iget v1, p0, Lcom/google/android/gms/car/ip;->g:I

    iput v1, v0, Lcom/google/android/gms/car/it;->c:I

    .line 467
    iget v1, p0, Lcom/google/android/gms/car/ip;->h:I

    iput v1, v0, Lcom/google/android/gms/car/it;->d:I

    .line 468
    iget v1, p0, Lcom/google/android/gms/car/ip;->i:I

    iput v1, v0, Lcom/google/android/gms/car/it;->e:I

    .line 469
    iget v1, p0, Lcom/google/android/gms/car/ip;->j:I

    iput v1, v0, Lcom/google/android/gms/car/it;->f:I

    .line 470
    iget v1, p0, Lcom/google/android/gms/car/ip;->k:I

    iput v1, v0, Lcom/google/android/gms/car/it;->g:I

    .line 473
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    if-eqz v1, :cond_0

    .line 474
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->n:Lcom/google/android/gms/car/jv;

    iget-object v1, v1, Lcom/google/android/gms/car/jv;->f:Landroid/view/Surface;

    iput-object v1, v0, Lcom/google/android/gms/car/it;->h:Landroid/view/Surface;

    .line 476
    :cond_0
    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 207
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->x:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v1, Lcom/google/android/gms/car/ip;->w:Landroid/util/Pair;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 210
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->y:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/car/it;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 506
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->s:Z

    if-eqz v0, :cond_1

    .line 507
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 508
    const-string v0, "CAR.VIDEO"

    const-string v1, "sink already added"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 520
    :cond_0
    :goto_0
    return-void

    .line 512
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ip;->s:Z

    .line 513
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->t:Z

    if-nez v0, :cond_2

    .line 514
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 515
    const-string v0, "CAR.VIDEO"

    const-string v1, "sink added while display not enabled yet."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 519
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ip;->c(Lcom/google/android/gms/car/it;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/iv;)V
    .locals 0

    .prologue
    .line 155
    iput-object p1, p0, Lcom/google/android/gms/car/ip;->o:Lcom/google/android/gms/car/iv;

    .line 156
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 202
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/be;

    iput-object p1, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    .line 203
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 11

    .prologue
    .line 581
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "codecW:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/ip;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " codecH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/ip;->f:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dispW:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/ip;->h:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dispH:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/ip;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " dpi:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/ip;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " fps:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/ip;->g:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 584
    const-string v0, "**Video Configs**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 585
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->p:[Lcom/google/android/c/b/cz;

    .line 586
    if-eqz v1, :cond_1

    .line 587
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 588
    if-eqz v3, :cond_0

    .line 589
    iget-object v4, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)Z

    iget-object v4, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v4, v4, Landroid/graphics/Point;->x:I

    iget-object v5, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v5, v5, Landroid/graphics/Point;->y:I

    invoke-static {v3}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;)I

    move-result v6

    iget-object v7, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-direct {p0, v3, v7}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)V

    iget-object v7, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v7, v7, Landroid/graphics/Point;->x:I

    iget-object v8, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v8, v8, Landroid/graphics/Point;->y:I

    invoke-virtual {v3}, Lcom/google/android/c/b/cz;->i()I

    move-result v3

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, " codecW:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v9, " codecH:"

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " dispW:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " dispH:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " dpi:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " fps:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, " disp enabled:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v4, p0, Lcom/google/android/gms/car/ip;->t:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " disp added:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-boolean v4, p0, Lcom/google/android/gms/car/ip;->s:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 587
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_0

    .line 591
    :cond_0
    const-string v3, "null config"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 595
    :cond_1
    const-string v0, "null video configs"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 597
    :cond_2
    return-void
.end method

.method public final a([I)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 218
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const-string v0, "CAR.VIDEO"

    const-string v1, "Video focus gained."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 221
    :cond_0
    new-instance v0, Landroid/util/Pair;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 223
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->x:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 224
    return-void
.end method

.method public final a([Lcom/google/android/c/b/cz;)V
    .locals 5

    .prologue
    .line 160
    if-nez p1, :cond_0

    .line 161
    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/ed;

    const-string v1, "no video config"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/car/ed; {:try_start_0 .. :try_end_0} :catch_0

    .line 168
    :catch_0
    move-exception v0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->a()I

    move-result v2

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 171
    :goto_0
    return-void

    .line 164
    :cond_0
    :try_start_1
    array-length v1, p1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_7

    aget-object v2, p1, v0

    .line 165
    iget-object v3, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)Z

    move-result v3

    if-nez v3, :cond_1

    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "wrong codec resolution "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/car/ip;->b(Lcom/google/android/c/b/cz;Landroid/graphics/Point;)V

    iget-object v3, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v4, v3, Landroid/graphics/Point;->x:I

    if-lez v4, :cond_2

    iget v3, v3, Landroid/graphics/Point;->y:I

    if-gtz v3, :cond_3

    :cond_2
    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong disp resolution "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->x:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ","

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/ip;->b:Landroid/graphics/Point;

    iget v2, v2, Landroid/graphics/Point;->y:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_3
    invoke-static {v2}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/c/b/cz;)I

    move-result v3

    const/16 v4, 0x1e

    if-eq v3, v4, :cond_4

    const/16 v4, 0x3c

    if-eq v3, v4, :cond_4

    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong FPS "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_4
    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->j()Z

    move-result v3

    if-nez v3, :cond_5

    new-instance v0, Lcom/google/android/gms/car/ed;

    const-string v1, "density missing"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_5
    invoke-virtual {v2}, Lcom/google/android/c/b/cz;->i()I

    move-result v2

    if-gtz v2, :cond_6

    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "wrong density "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 167
    :cond_7
    iput-object p1, p0, Lcom/google/android/gms/car/ip;->p:[Lcom/google/android/c/b/cz;
    :try_end_1
    .catch Lcom/google/android/gms/car/ed; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 339
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 340
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 341
    const-string v0, "CAR.VIDEO"

    const-string v1, "Video focus lost."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->x:Ljava/util/concurrent/LinkedBlockingQueue;

    sget-object v1, Lcom/google/android/gms/car/ip;->v:Landroid/util/Pair;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 344
    return-void
.end method

.method final b(Lcom/google/android/gms/car/it;)V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 554
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->t:Z

    if-eqz v0, :cond_1

    .line 555
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 556
    const-string v0, "CAR.VIDEO"

    const-string v1, "display already enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 560
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ip;->t:Z

    .line 561
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->s:Z

    if-eqz v0, :cond_3

    .line 562
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 563
    const-string v0, "CAR.VIDEO"

    const-string v1, "display enabled after sink added, start notification"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 565
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ip;->c(Lcom/google/android/gms/car/it;)V

    .line 567
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->q:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->h()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 568
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 569
    const-string v0, "CAR.VIDEO"

    const-string v1, "Requesting video focus, assuming legacy car behavior."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/car/ip;->d()V

    goto :goto_0

    .line 573
    :cond_5
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 574
    const-string v0, "CAR.VIDEO"

    const-string v1, "Not taking video focus, assuming new car behavior."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 357
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->r:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    .line 359
    if-eqz v0, :cond_0

    .line 360
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/be;->c(I)V

    .line 363
    :cond_0
    return-void
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 366
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->m:Lcom/google/android/gms/car/senderprotocol/be;

    .line 367
    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/be;->e()V

    .line 370
    :cond_0
    return-void
.end method

.method public final declared-synchronized e()V
    .locals 2

    .prologue
    .line 502
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->a:Lcom/google/android/gms/car/iu;

    invoke-direct {p0}, Lcom/google/android/gms/car/ip;->h()Lcom/google/android/gms/car/it;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/iu;->b(Lcom/google/android/gms/car/it;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 503
    monitor-exit p0

    return-void

    .line 502
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final f()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 536
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->s:Z

    if-nez v0, :cond_1

    .line 537
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 538
    const-string v0, "CAR.VIDEO"

    const-string v1, "sink not added"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 551
    :cond_0
    :goto_0
    return-void

    .line 542
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ip;->s:Z

    .line 543
    iget-boolean v0, p0, Lcom/google/android/gms/car/ip;->t:Z

    if-nez v0, :cond_2

    .line 544
    const-string v0, "CAR.VIDEO"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 545
    const-string v0, "CAR.VIDEO"

    const-string v1, "sink removed while not enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 549
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->l:Lcom/google/android/gms/car/is;

    invoke-interface {v0}, Lcom/google/android/gms/car/is;->z()V

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/car/ip;->u:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0
.end method
