.class final Lcom/google/android/gms/icing/impl/ak;
.super Lcom/google/android/gms/icing/impl/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:J

.field final synthetic b:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/impl/u;J)V
    .locals 0

    .prologue
    .line 2073
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/ak;->b:Lcom/google/android/gms/icing/impl/u;

    iput-wide p2, p0, Lcom/google/android/gms/icing/impl/ak;->a:J

    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/ay;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    return-void
.end method


# virtual methods
.method public final c()V
    .locals 5

    .prologue
    .line 2076
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ak;->b:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->l()Z

    move-result v0

    if-nez v0, :cond_0

    .line 2077
    const-string v0, "Indexing retry skipped because init failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2081
    :goto_0
    return-void

    .line 2080
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/ak;->b:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/u;->k:Lcom/google/android/gms/icing/impl/e;

    iget-wide v2, p0, Lcom/google/android/gms/icing/impl/ak;->a:J

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    new-instance v4, Lcom/google/android/gms/icing/impl/g;

    invoke-direct {v4, v0, v2, v3}, Lcom/google/android/gms/icing/impl/g;-><init>(Lcom/google/android/gms/icing/impl/e;J)V

    const-wide/16 v2, 0x0

    invoke-virtual {v1, v4, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0
.end method
