.class public final Lcom/google/android/gms/games/h/a/ay;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 38
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 40
    sput-object v0, Lcom/google/android/gms/games/h/a/ay;->b:Ljava/util/HashMap;

    const-string v1, "definitionId"

    const-string v2, "definitionId"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 41
    sget-object v0, Lcom/google/android/gms/games/h/a/ay;->b:Ljava/util/HashMap;

    const-string v1, "updateCount"

    const-string v2, "updateCount"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->c(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Long;)V
    .locals 4

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 55
    if-eqz p1, :cond_0

    .line 56
    const-string v0, "definitionId"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/ay;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 58
    :cond_0
    if-eqz p2, :cond_1

    .line 59
    const-string v0, "updateCount"

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/games/h/a/ay;->a(Ljava/lang/String;J)V

    .line 61
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/google/android/gms/games/h/a/ay;->b:Ljava/util/HashMap;

    return-object v0
.end method
