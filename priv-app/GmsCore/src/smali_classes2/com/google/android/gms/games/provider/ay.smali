.class final Lcom/google/android/gms/games/provider/ay;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field final b:Lcom/google/android/gms/games/provider/az;

.field c:Lcom/google/android/gms/games/provider/bg;

.field volatile d:Ljava/util/concurrent/CountDownLatch;

.field volatile e:Ljava/util/concurrent/CountDownLatch;

.field f:I

.field private final g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/provider/ay;->a:Ljava/lang/String;

    .line 51
    new-instance v0, Lcom/google/android/gms/games/provider/az;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/ay;->a:Ljava/lang/String;

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/provider/az;-><init>(Lcom/google/android/gms/games/provider/ay;Landroid/content/Context;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    .line 52
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "games.data_store_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/ay;->g:Ljava/lang/String;

    .line 54
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    .line 55
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v0, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    .line 56
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/provider/ay;->f:I

    .line 58
    invoke-static {p1, p2}, Lcom/google/android/gms/games/n;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 59
    return-void
.end method

.method static a(Ljava/util/concurrent/CountDownLatch;)V
    .locals 1

    .prologue
    .line 154
    if-nez p0, :cond_0

    .line 161
    :goto_0
    return-void

    .line 163
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 160
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)Landroid/content/SharedPreferences;
    .locals 2

    .prologue
    .line 74
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/provider/ay;->g:Ljava/lang/String;

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final b(Landroid/content/Context;)Z
    .locals 6

    .prologue
    const/4 v5, 0x0

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 99
    iget-object v2, p0, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    if-nez v2, :cond_0

    move v2, v0

    :goto_0
    if-eqz v2, :cond_1

    .line 117
    :goto_1
    return v0

    :cond_0
    move v2, v1

    .line 99
    goto :goto_0

    .line 104
    :cond_1
    invoke-virtual {p1}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/provider/ay;->a:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/bg;->a(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v2

    .line 105
    if-nez v2, :cond_2

    .line 106
    iget v0, p0, Lcom/google/android/gms/games/provider/ay;->f:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/games/provider/ay;->f:I

    move v0, v1

    .line 107
    goto :goto_1

    .line 111
    :cond_2
    new-instance v3, Lcom/google/android/gms/games/provider/bg;

    iget-object v4, p0, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-direct {v3, v2, v4}, Lcom/google/android/gms/games/provider/bg;-><init>(Ljava/io/File;Lcom/google/android/gms/games/provider/az;)V

    iput-object v3, p0, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    .line 112
    iget-object v2, p0, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 113
    iput-object v5, p0, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    .line 114
    iget-object v2, p0, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v2}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 115
    iput-object v5, p0, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    .line 116
    iput v1, p0, Lcom/google/android/gms/games/provider/ay;->f:I

    goto :goto_1
.end method
