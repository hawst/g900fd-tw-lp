.class public final Lcom/google/android/gms/people/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/people/d;


# instance fields
.field private b:I

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/gms/people/d;

    invoke-direct {v0}, Lcom/google/android/gms/people/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/d;->a:Lcom/google/android/gms/people/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 131
    const/16 v0, -0x3e7

    iput v0, p0, Lcom/google/android/gms/people/d;->b:I

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/people/d;
    .locals 0

    .prologue
    .line 199
    iput p1, p0, Lcom/google/android/gms/people/d;->b:I

    .line 200
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/people/d;
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/gms/people/d;->d:Ljava/lang/String;

    .line 171
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/people/d;
    .locals 0

    .prologue
    .line 186
    iput-boolean p1, p0, Lcom/google/android/gms/people/d;->e:Z

    .line 187
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/gms/people/d;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/people/d;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 175
    iget-boolean v0, p0, Lcom/google/android/gms/people/d;->e:Z

    return v0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 191
    iget v0, p0, Lcom/google/android/gms/people/d;->b:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 141
    const/16 v0, 0x8

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "mFilterCircleType"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/people/d;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mFilterCircleId"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/d;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mFilterCircleNamePrefix"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/people/d;->d:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "mGetVisibility"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    iget-boolean v2, p0, Lcom/google/android/gms/people/d;->e:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/people/internal/n;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
