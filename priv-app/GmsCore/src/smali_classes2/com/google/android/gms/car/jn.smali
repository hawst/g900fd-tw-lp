.class final Lcom/google/android/gms/car/jn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/net/wifi/p2p/WifiP2pManager$PeerListListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/jk;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/jk;)V
    .locals 0

    .prologue
    .line 587
    iput-object p1, p0, Lcom/google/android/gms/car/jn;->a:Lcom/google/android/gms/car/jk;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPeersAvailable(Landroid/net/wifi/p2p/WifiP2pDeviceList;)V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x0

    .line 590
    invoke-virtual {p1}, Landroid/net/wifi/p2p/WifiP2pDeviceList;->getDeviceList()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/p2p/WifiP2pDevice;

    .line 591
    const-string v2, "CAR.FIRST"

    invoke-static {v2, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 592
    const-string v2, "CAR.FIRST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", status: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", isGO? "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Landroid/net/wifi/p2p/WifiP2pDevice;->isGroupOwner()Z

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 596
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/jn;->a:Lcom/google/android/gms/car/jk;

    invoke-static {v2}, Lcom/google/android/gms/car/jk;->a(Lcom/google/android/gms/car/jk;)Ljava/util/HashSet;

    move-result-object v2

    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 597
    iget v2, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->status:I

    if-eqz v2, :cond_0

    .line 600
    const-string v2, "CAR.FIRST"

    invoke-static {v2, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 604
    const-string v2, "CAR.FIRST"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Inviting device: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 606
    :cond_2
    new-instance v2, Landroid/net/wifi/p2p/WifiP2pConfig;

    invoke-direct {v2}, Landroid/net/wifi/p2p/WifiP2pConfig;-><init>()V

    .line 607
    iget-object v3, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    iput-object v3, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->deviceAddress:Ljava/lang/String;

    .line 608
    iput v5, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->groupOwnerIntent:I

    .line 609
    iget-object v3, v2, Landroid/net/wifi/p2p/WifiP2pConfig;->wps:Landroid/net/wifi/WpsInfo;

    iput v5, v3, Landroid/net/wifi/WpsInfo;->setup:I

    .line 610
    iget-object v3, p0, Lcom/google/android/gms/car/jn;->a:Lcom/google/android/gms/car/jk;

    invoke-static {v3}, Lcom/google/android/gms/car/jk;->a(Lcom/google/android/gms/car/jk;)Ljava/util/HashSet;

    move-result-object v3

    iget-object v0, v0, Landroid/net/wifi/p2p/WifiP2pDevice;->deviceAddress:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 611
    iget-object v0, p0, Lcom/google/android/gms/car/jn;->a:Lcom/google/android/gms/car/jk;

    iget-object v0, v0, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->o(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/car/jn;->a:Lcom/google/android/gms/car/jk;

    iget-object v3, v3, Lcom/google/android/gms/car/jk;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v3}, Lcom/google/android/gms/car/FirstActivity;->n(Lcom/google/android/gms/car/FirstActivity;)Landroid/net/wifi/p2p/WifiP2pManager$Channel;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v2, v4}, Landroid/net/wifi/p2p/WifiP2pManager;->connect(Landroid/net/wifi/p2p/WifiP2pManager$Channel;Landroid/net/wifi/p2p/WifiP2pConfig;Landroid/net/wifi/p2p/WifiP2pManager$ActionListener;)V

    goto/16 :goto_0

    .line 613
    :cond_3
    return-void
.end method
