.class public final Lcom/google/android/gms/games/a/am;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/games/a/am;

.field private static final c:[I


# instance fields
.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/am;->a:Ljava/lang/Object;

    .line 37
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0x1
        0x2
        0x3
        0x4
    .end array-data
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/am;->e:Ljava/util/ArrayList;

    .line 68
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/am;->d:Ljava/util/ArrayList;

    .line 69
    const/4 v0, 0x0

    :goto_0
    sget-object v1, Lcom/google/android/gms/games/a/am;->c:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 70
    iget-object v1, p0, Lcom/google/android/gms/games/a/am;->d:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 69
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 72
    :cond_0
    return-void
.end method

.method public static a()Lcom/google/android/gms/games/a/am;
    .locals 2

    .prologue
    .line 58
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 59
    sget-object v1, Lcom/google/android/gms/games/a/am;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 60
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/a/am;->b:Lcom/google/android/gms/games/a/am;

    if-nez v0, :cond_0

    .line 61
    new-instance v0, Lcom/google/android/gms/games/a/am;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/am;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/am;->b:Lcom/google/android/gms/games/a/am;

    .line 63
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    sget-object v0, Lcom/google/android/gms/games/a/am;->b:Lcom/google/android/gms/games/a/am;

    return-object v0

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(I)Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 222
    if-ltz p1, :cond_0

    sget-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    array-length v0, v0

    if-lt p1, v0, :cond_1

    .line 223
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown event type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/a/am;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    return-object v0
.end method

.method private a(ILjava/lang/String;Ljava/io/PrintWriter;)V
    .locals 8

    .prologue
    .line 95
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 96
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ar;

    .line 97
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    .line 98
    if-eqz v1, :cond_0

    .line 99
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "  "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v4, " for %s [%d]:"

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v0, v0, Lcom/google/android/gms/games/a/ar;->b:Ljava/lang/String;

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v2, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 101
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_0

    .line 102
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/at;

    .line 103
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "    Listener "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v6, v0, Lcom/google/android/gms/games/a/at;->a:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 101
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 107
    :cond_1
    return-void
.end method

.method private a(JLcom/google/android/gms/games/a/ar;I)V
    .locals 9

    .prologue
    .line 524
    invoke-direct {p0, p4}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v3

    .line 525
    invoke-virtual {v3, p3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 526
    if-nez v0, :cond_1

    .line 545
    :cond_0
    :goto_0
    return-void

    .line 531
    :cond_1
    const/4 v1, 0x0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    .line 532
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/at;

    .line 533
    iget-wide v6, v1, Lcom/google/android/gms/games/a/at;->a:J

    cmp-long v1, v6, p1

    if-nez v1, :cond_3

    .line 534
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Unregistering "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " for "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v4, p3, Lcom/google/android/gms/games/a/ar;->b:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "GameEventRegistry"

    invoke-static {v4, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 535
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 541
    :cond_2
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 542
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "All type "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " listeners removed for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p3, Lcom/google/android/gms/games/a/ar;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    invoke-virtual {v3, p3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 531
    :cond_3
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JI)V
    .locals 1

    .prologue
    .line 519
    new-instance v0, Lcom/google/android/gms/games/a/ar;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/a/ar;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    invoke-direct {p0, p3, p4, v0, p5}, Lcom/google/android/gms/games/a/am;->a(JLcom/google/android/gms/games/a/ar;I)V

    .line 521
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;I)V
    .locals 9

    .prologue
    .line 192
    invoke-direct {p0, p6}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v1

    .line 193
    new-instance v3, Lcom/google/android/gms/games/a/ar;

    invoke-direct {v3, p1, p2}, Lcom/google/android/gms/games/a/ar;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 194
    invoke-virtual {v1, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 195
    if-nez v0, :cond_2

    .line 196
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 197
    invoke-virtual {v1, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v1, v0

    .line 201
    :goto_0
    const/4 v0, 0x0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_0

    .line 202
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/at;

    .line 203
    iget-wide v6, v0, Lcom/google/android/gms/games/a/at;->a:J

    cmp-long v0, v6, p3

    if-nez v0, :cond_1

    .line 204
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, "Removing listener "

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, " from key "

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v4, "GameEventRegistry"

    invoke-static {v4, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 205
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 211
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Added listener "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " for key "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, "GameEventRegistry"

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 212
    new-instance v0, Lcom/google/android/gms/games/a/at;

    invoke-direct {v0, p3, p4, p5}, Lcom/google/android/gms/games/a/at;-><init>(JLcom/google/android/gms/games/internal/dr;)V

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 213
    return-void

    .line 201
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move-object v1, v0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 287
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v2

    .line 288
    new-instance v0, Lcom/google/android/gms/games/a/ar;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/a/ar;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 289
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 290
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    move v0, v1

    .line 298
    :goto_0
    return v0

    .line 296
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/a/ar;

    const/4 v3, 0x0

    invoke-direct {v0, p1, v3}, Lcom/google/android/gms/games/a/ar;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 297
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 298
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    move v0, v1

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    .locals 1

    .prologue
    .line 408
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z

    move-result v0

    .line 409
    if-nez v0, :cond_0

    .line 410
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0, p3, p4}, Lcom/google/android/gms/games/a/am;->b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z

    move-result v0

    .line 413
    :cond_0
    return v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 418
    invoke-direct {p0, p4}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v0

    .line 419
    new-instance v1, Lcom/google/android/gms/games/a/ar;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/a/ar;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 421
    if-nez v0, :cond_0

    move v0, v2

    .line 451
    :goto_0
    return v0

    .line 427
    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    move v3, v1

    :goto_1
    if-ltz v3, :cond_2

    .line 428
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/a/at;

    .line 429
    iget-object v4, v1, Lcom/google/android/gms/games/a/at;->b:Lcom/google/android/gms/games/internal/dr;

    .line 430
    if-nez v4, :cond_1

    .line 431
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 427
    :goto_2
    add-int/lit8 v1, v3, -0x1

    move v3, v1

    goto :goto_1

    .line 436
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Delivering to: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v1, Lcom/google/android/gms/games/a/at;->a:J

    invoke-virtual {v5, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "GameEventRegistry"

    invoke-static {v6, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    :try_start_0
    invoke-virtual {p3, v4}, Lcom/google/android/gms/games/a/as;->a(Lcom/google/android/gms/games/internal/dr;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Delivered to: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, v1, Lcom/google/android/gms/games/a/at;->a:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 447
    const/4 v0, 0x1

    goto :goto_0

    .line 440
    :catch_0
    move-exception v4

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 441
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Remote exception delivering to: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v6, v1, Lcom/google/android/gms/games/a/at;->a:J

    invoke-virtual {v4, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v4, "GameEventRegistry"

    invoke-static {v4, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    :cond_2
    move v0, v2

    .line 451
    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a(J)V
    .locals 5

    .prologue
    .line 548
    monitor-enter p0

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    array-length v0, v0

    if-ge v1, v0, :cond_1

    .line 549
    sget-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    aget v2, v0, v1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ar;

    invoke-direct {p0, p1, p2, v0, v2}, Lcom/google/android/gms/games/a/am;->a(JLcom/google/android/gms/games/a/ar;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 548
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 551
    :cond_1
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 80
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    const-string v1, "Invitation listeners"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/am;->a(ILjava/lang/String;Ljava/io/PrintWriter;)V

    .line 81
    const/4 v0, 0x1

    const-string v1, "Match update listeners"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/am;->a(ILjava/lang/String;Ljava/io/PrintWriter;)V

    .line 82
    const/4 v0, 0x3

    const-string v1, "Quest update listeners"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/am;->a(ILjava/lang/String;Ljava/io/PrintWriter;)V

    .line 83
    const/4 v0, 0x2

    const-string v1, "Request listeners"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/am;->a(ILjava/lang/String;Ljava/io/PrintWriter;)V

    .line 84
    const/4 v0, 0x4

    const-string v1, "Nearby player listeners"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/am;->a(ILjava/lang/String;Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    monitor-exit p0

    return-void

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 569
    monitor-enter p0

    move v3, v2

    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    array-length v0, v0

    if-ge v3, v0, :cond_4

    .line 570
    sget-object v0, Lcom/google/android/gms/games/a/am;->c:[I

    aget v0, v0, v3

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/am;->a(I)Ljava/util/HashMap;

    move-result-object v4

    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ar;

    iget-object v6, v0, Lcom/google/android/gms/games/a/ar;->a:Ljava/lang/String;

    invoke-virtual {v6, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 569
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 570
    :cond_1
    :try_start_1
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v1, v2

    :goto_2
    if-ge v1, v6, :cond_3

    invoke-virtual {v5, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/a/ar;

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Clearing  listeners for "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v8, v0, Lcom/google/android/gms/games/a/ar;->b:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const-string v8, "GameEventRegistry"

    invoke-static {v8, v7}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 569
    :cond_3
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 572
    :cond_4
    monitor-exit p0

    return-void
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 463
    monitor-enter p0

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    :try_start_0
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 464
    monitor-exit p0

    return-void

    .line 463
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V
    .locals 9

    .prologue
    .line 121
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Registering invitation listener "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 122
    const/4 v7, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 123
    monitor-exit p0

    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 237
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;)Z
    .locals 2

    .prologue
    .line 367
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/a/ap;

    invoke-direct {v0, p0, p4, p3}, Lcom/google/android/gms/games/a/ap;-><init>(Lcom/google/android/gms/games/a/am;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;)V

    .line 374
    const/4 v1, 0x3

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 367
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    .locals 2

    .prologue
    .line 314
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/a/an;

    invoke-direct {v0, p0, p4, p3, p5}, Lcom/google/android/gms/games/a/an;-><init>(Lcom/google/android/gms/games/a/am;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Z)V

    .line 325
    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 314
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 476
    monitor-enter p0

    const/4 v6, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    :try_start_0
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 477
    monitor-exit p0

    return-void

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V
    .locals 9

    .prologue
    .line 137
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Registering match update listener"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 139
    monitor-exit p0

    return-void

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 248
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    .locals 2

    .prologue
    .line 341
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/a/ao;

    invoke-direct {v0, p0, p4, p3, p5}, Lcom/google/android/gms/games/a/ao;-><init>(Lcom/google/android/gms/games/a/am;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Z)V

    .line 352
    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 341
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 489
    monitor-enter p0

    const/4 v6, 0x3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    :try_start_0
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 490
    monitor-exit p0

    return-void

    .line 489
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V
    .locals 9

    .prologue
    .line 153
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Registering quest update listener "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 154
    const/4 v7, 0x3

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 155
    monitor-exit p0

    return-void

    .line 153
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 260
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    .locals 2

    .prologue
    .line 390
    monitor-enter p0

    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/a/aq;

    invoke-direct {v0, p0, p4, p3, p5}, Lcom/google/android/gms/games/a/aq;-><init>(Lcom/google/android/gms/games/a/am;Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Z)V

    .line 401
    const/4 v1, 0x2

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/a/as;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 390
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;Ljava/lang/String;J)V
    .locals 7

    .prologue
    .line 501
    monitor-enter p0

    const/4 v6, 0x2

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    :try_start_0
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JI)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 502
    monitor-exit p0

    return-void

    .line 501
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;)V
    .locals 9

    .prologue
    .line 169
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Registering request listener "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " for "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "GameEventRegistry"

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const/4 v7, 0x2

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move-object v6, p5

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;JLcom/google/android/gms/games/internal/dr;I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    monitor-exit p0

    return-void

    .line 169
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized d(Ljava/lang/String;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 271
    monitor-enter p0

    const/4 v0, 0x2

    :try_start_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/am;->a(Ljava/lang/String;Ljava/lang/String;I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
