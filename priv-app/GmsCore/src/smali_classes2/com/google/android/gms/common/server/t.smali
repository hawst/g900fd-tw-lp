.class public final Lcom/google/android/gms/common/server/t;
.super Lcom/android/volley/toolbox/g;
.source "SourceFile"


# static fields
.field private static final b:Lorg/apache/http/HttpEntity;


# instance fields
.field private final c:Lcom/google/android/gms/http/GoogleHttpClient;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 27
    new-instance v0, Lorg/apache/http/entity/ByteArrayEntity;

    const/4 v1, 0x0

    new-array v1, v1, [B

    invoke-direct {v0, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    sput-object v0, Lcom/google/android/gms/common/server/t;->b:Lorg/apache/http/HttpEntity;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Z)V
    .locals 3

    .prologue
    .line 36
    new-instance v0, Lcom/google/android/gms/http/GoogleHttpClient;

    const-string v1, "unused/0"

    const/4 v2, 0x1

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/server/t;-><init>(Lcom/google/android/gms/http/GoogleHttpClient;Z)V

    .line 38
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/http/GoogleHttpClient;Z)V
    .locals 2

    .prologue
    .line 41
    invoke-direct {p0, p1}, Lcom/android/volley/toolbox/g;-><init>(Lorg/apache/http/client/HttpClient;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/common/server/t;->c:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 43
    if-eqz p2, :cond_0

    .line 44
    sget-object v0, Lcom/android/volley/ad;->a:Ljava/lang/String;

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/http/GoogleHttpClient;->enableCurlLogging(Ljava/lang/String;I)V

    .line 46
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/p;Ljava/util/Map;)Lorg/apache/http/HttpResponse;
    .locals 2

    .prologue
    .line 59
    invoke-super {p0, p1, p2}, Lcom/android/volley/toolbox/g;->a(Lcom/android/volley/p;Ljava/util/Map;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    if-nez v1, :cond_0

    instance-of v1, p1, Lcom/google/android/gms/common/server/v;

    if-eqz v1, :cond_0

    .line 63
    sget-object v1, Lcom/google/android/gms/common/server/t;->b:Lorg/apache/http/HttpEntity;

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 64
    const/16 v1, 0xc8

    invoke-interface {v0, v1}, Lorg/apache/http/HttpResponse;->setStatusCode(I)V

    .line 66
    :cond_0
    return-object v0
.end method
