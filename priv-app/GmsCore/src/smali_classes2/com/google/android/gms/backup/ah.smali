.class final Lcom/google/android/gms/backup/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/backup/aj;


# static fields
.field private static final a:Lcom/google/android/gms/auth/d/a;


# instance fields
.field private final b:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SharedPrefsBackupAccountManager"

    aput-object v4, v2, v3

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/auth/d/a;-><init>(Ljava/lang/String;[Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    .line 41
    return-void
.end method

.method private b(Landroid/accounts/Account;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v2

    .line 107
    array-length v1, v2

    if-nez v1, :cond_1

    .line 108
    sget-object v1, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "No google accounts found!"

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 116
    :cond_0
    :goto_0
    return v0

    .line 111
    :cond_1
    array-length v3, v2

    move v1, v0

    :goto_1
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 112
    invoke-virtual {v4, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 113
    const/4 v0, 0x1

    goto :goto_0

    .line 111
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method


# virtual methods
.method public final a()Landroid/accounts/Account;
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    const-string v2, "BackupAccount"

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 47
    const-string v2, "accountName"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 48
    const-string v3, "accountType"

    invoke-interface {v0, v3, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 49
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    :cond_0
    move-object v0, v1

    .line 61
    :cond_1
    :goto_0
    return-object v0

    .line 53
    :cond_2
    new-instance v0, Landroid/accounts/Account;

    invoke-direct {v0, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 54
    invoke-direct {p0, v0}, Lcom/google/android/gms/backup/ah;->b(Landroid/accounts/Account;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 58
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Backup account was not valid."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    const-string v2, "BackupAccount"

    invoke-virtual {v0, v2, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "accountName"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "accountType"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_3

    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v2, "Fail to write gms backup account shared preference."

    invoke-virtual {v0, v2}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 60
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-object v0, v1

    .line 61
    goto :goto_0
.end method

.method public final a(Landroid/accounts/Account;)V
    .locals 3

    .prologue
    .line 67
    if-nez p1, :cond_0

    .line 68
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Cannot set null backup account."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    .line 91
    :goto_0
    return-void

    .line 71
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/backup/ah;->a()Landroid/accounts/Account;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Backup account not changed."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 75
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/backup/ah;->b(Landroid/accounts/Account;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 76
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting backup account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->b(Ljava/lang/String;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    const-string v1, "BackupAccount"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 79
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accountName"

    iget-object v2, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "accountType"

    iget-object v2, p1, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 82
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_2

    .line 83
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Fail to write gms backup account shared preference."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->e(Ljava/lang/String;)V

    .line 85
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/gms/backup/BackupAccountNotifierService;->b(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/backup/ah;->b:Landroid/content/Context;

    invoke-static {v0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.gms.backup.BackupAccountChanged"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/Intent;)Z

    goto :goto_0

    .line 89
    :cond_3
    sget-object v0, Lcom/google/android/gms/backup/ah;->a:Lcom/google/android/gms/auth/d/a;

    const-string v1, "Cannot set invalid backup account."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/d/a;->d(Ljava/lang/String;)V

    goto :goto_0
.end method
