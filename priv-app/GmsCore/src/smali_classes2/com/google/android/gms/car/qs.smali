.class final Lcom/google/android/gms/car/qs;
.super Lcom/google/android/gms/car/qp;
.source "SourceFile"


# instance fields
.field private final l:I

.field private final m:I

.field private final n:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ok;I)V
    .locals 4

    .prologue
    const-wide/16 v2, 0xfa

    .line 667
    invoke-static {v2, v3, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/car/qp;-><init>(JZ)V

    .line 669
    const/16 v0, 0x10

    invoke-static {p2, v0}, Lcom/google/android/gms/car/qo;->a(II)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/qs;->l:I

    .line 670
    iget v0, p1, Lcom/google/android/gms/car/ok;->a:I

    iget v1, p1, Lcom/google/android/gms/car/ok;->e:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/qs;->m:I

    .line 671
    iget v0, p1, Lcom/google/android/gms/car/ok;->b:I

    iget v1, p1, Lcom/google/android/gms/car/ok;->f:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/qs;->n:I

    .line 672
    return-void
.end method


# virtual methods
.method protected final a(ZJ)Lcom/google/android/gms/car/qq;
    .locals 10

    .prologue
    const/high16 v5, 0x437a0000    # 250.0f

    const/4 v1, 0x0

    const/high16 v6, 0x3f800000    # 1.0f

    const/4 v9, 0x0

    .line 677
    sget-object v0, Lcom/google/android/gms/car/qs;->a:Lcom/google/android/gms/car/pd;

    long-to-float v2, p2

    invoke-virtual {v0, v2, v9, v5}, Lcom/google/android/gms/car/pd;->a(FFF)F

    move-result v0

    .line 679
    sget-object v2, Lcom/google/android/gms/car/qs;->d:Lcom/google/android/gms/car/nm;

    long-to-float v3, p2

    invoke-virtual {v2, v3, v9, v5}, Lcom/google/android/gms/car/nm;->a(FFF)F

    move-result v7

    .line 681
    sget-object v2, Lcom/google/android/gms/car/qs;->c:Lcom/google/android/gms/car/pd;

    long-to-float v3, p2

    const/high16 v4, 0x42c80000    # 100.0f

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/gms/car/pd;->a(FFF)F

    move-result v8

    .line 683
    const v2, 0x3f6b851f    # 0.92f

    const v3, 0x3da3d708    # 0.07999998f

    sub-float v0, v6, v0

    mul-float/2addr v0, v3

    add-float v4, v2, v0

    .line 685
    iget-object v0, p0, Lcom/google/android/gms/car/qs;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/qs;->f:[F

    move v3, v1

    move v5, v4

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->scaleM([FI[FIFFF)V

    .line 686
    sub-float v0, v6, v4

    iget v2, p0, Lcom/google/android/gms/car/qs;->m:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 687
    sub-float v2, v6, v4

    iget v3, p0, Lcom/google/android/gms/car/qs;->n:I

    int-to-float v3, v3

    mul-float/2addr v2, v3

    .line 688
    iget-object v3, p0, Lcom/google/android/gms/car/qs;->k:Lcom/google/android/gms/car/qq;

    iget-object v3, v3, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v3, v1, v0, v2, v9}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 690
    neg-float v0, v7

    iget v2, p0, Lcom/google/android/gms/car/qs;->l:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 691
    iget-object v2, p0, Lcom/google/android/gms/car/qs;->k:Lcom/google/android/gms/car/qq;

    iget-object v2, v2, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v2, v1, v9, v0, v9}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 692
    iget-object v0, p0, Lcom/google/android/gms/car/qs;->k:Lcom/google/android/gms/car/qq;

    sub-float v1, v6, v8

    iput v1, v0, Lcom/google/android/gms/car/qq;->b:F

    .line 698
    iget-object v0, p0, Lcom/google/android/gms/car/qs;->k:Lcom/google/android/gms/car/qq;

    return-object v0
.end method
