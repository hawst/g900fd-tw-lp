.class public final Lcom/google/android/gms/icing/impl/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/icing/impl/e/b;

.field public final b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/e/a;-><init>(B)V

    .line 55
    return-void
.end method

.method private constructor <init>(B)V
    .locals 2

    .prologue
    .line 57
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    new-instance v0, Lcom/google/android/gms/icing/impl/e/b;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/e/b;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/a;->a:Lcom/google/android/gms/icing/impl/e/b;

    .line 59
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/icing/impl/e/a;->b:I

    .line 60
    return-void
.end method


# virtual methods
.method public final a(Landroid/accounts/Account;)Ljava/util/List;
    .locals 10

    .prologue
    const/4 v9, 0x3

    .line 78
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/a;->a:Lcom/google/android/gms/icing/impl/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/b;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 81
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 82
    if-nez p1, :cond_2

    .line 83
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Collection;

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 101
    :cond_1
    new-instance v0, Lcom/google/android/gms/icing/impl/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/e/c;-><init>()V

    invoke-static {v3, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 102
    invoke-interface {v3}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 103
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v4

    if-eq v9, v4, :cond_0

    .line 104
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 85
    :cond_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/UsageInfo;

    .line 87
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->d()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v5

    .line 88
    if-eqz v5, :cond_3

    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/DocumentContents;->e:Landroid/accounts/Account;

    if-eqz v6, :cond_3

    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/DocumentContents;->e:Landroid/accounts/Account;

    invoke-virtual {v6, p1}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 89
    const-string v6, "Drop usageinfo account mismatch (%s) filter (%s): %s"

    new-array v7, v9, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget-object v5, v5, Lcom/google/android/gms/appdatasearch/DocumentContents;->e:Landroid/accounts/Account;

    aput-object v5, v7, v8

    const/4 v5, 0x1

    aput-object p1, v7, v5

    const/4 v5, 0x2

    aput-object v0, v7, v5

    invoke-static {v6, v7}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1

    .line 93
    :cond_3
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 112
    :cond_4
    new-instance v0, Lcom/google/android/gms/icing/impl/e/c;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/e/c;-><init>()V

    invoke-static {v1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 113
    return-object v1
.end method
