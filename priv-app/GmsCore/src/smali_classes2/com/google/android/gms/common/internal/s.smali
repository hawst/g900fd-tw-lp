.class final Lcom/google/android/gms/common/internal/s;
.super Lcom/google/android/gms/common/internal/g;
.source "SourceFile"


# instance fields
.field p:Ljava/util/List;


# direct methods
.method constructor <init>(Ljava/util/List;)V
    .locals 0

    .prologue
    .line 593
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/g;-><init>()V

    .line 594
    iput-object p1, p0, Lcom/google/android/gms/common/internal/s;->p:Ljava/util/List;

    .line 595
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/g;)Lcom/google/android/gms/common/internal/g;
    .locals 2

    .prologue
    .line 607
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/s;->p:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 608
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 609
    new-instance v1, Lcom/google/android/gms/common/internal/s;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/internal/s;-><init>(Ljava/util/List;)V

    return-object v1
.end method

.method public final a(C)Z
    .locals 2

    .prologue
    .line 598
    iget-object v0, p0, Lcom/google/android/gms/common/internal/s;->p:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/g;

    .line 599
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/g;->a(C)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 600
    const/4 v0, 0x1

    .line 603
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
