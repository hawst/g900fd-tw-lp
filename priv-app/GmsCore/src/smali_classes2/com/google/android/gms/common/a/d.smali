.class public abstract Lcom/google/android/gms/common/a/d;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field static a:Lcom/google/android/gms/common/a/k;

.field private static final d:Ljava/lang/Object;


# instance fields
.field protected final b:Ljava/lang/String;

.field protected final c:Ljava/lang/Object;

.field private e:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/a/d;->d:Ljava/lang/Object;

    .line 30
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/common/a/d;->a:Lcom/google/android/gms/common/a/k;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/a/d;->e:Ljava/lang/Object;

    .line 116
    iput-object p1, p0, Lcom/google/android/gms/common/a/d;->b:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/google/android/gms/common/a/d;->c:Ljava/lang/Object;

    .line 118
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Double;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 226
    new-instance v0, Lcom/google/android/gms/common/a/h;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/a/h;-><init>(Ljava/lang/String;Ljava/lang/Double;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 236
    new-instance v0, Lcom/google/android/gms/common/a/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/a/i;-><init>(Ljava/lang/String;Ljava/lang/Float;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/gms/common/a/g;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/a/g;-><init>(Ljava/lang/String;Ljava/lang/Integer;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 208
    new-instance v0, Lcom/google/android/gms/common/a/f;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/a/f;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;
    .locals 1

    .prologue
    .line 245
    new-instance v0, Lcom/google/android/gms/common/a/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/a/j;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;
    .locals 2

    .prologue
    .line 199
    new-instance v0, Lcom/google/android/gms/common/a/e;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/common/a/e;-><init>(Ljava/lang/String;Ljava/lang/Boolean;)V

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 36
    sget-object v1, Lcom/google/android/gms/common/a/d;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 37
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/a/d;->a:Lcom/google/android/gms/common/a/k;

    if-nez v0, :cond_0

    .line 38
    new-instance v0, Lcom/google/android/gms/common/a/l;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/a/l;-><init>(Landroid/content/ContentResolver;)V

    sput-object v0, Lcom/google/android/gms/common/a/d;->a:Lcom/google/android/gms/common/a/k;

    .line 40
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a()Z
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/common/a/d;->a:Lcom/google/android/gms/common/a/k;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Ljava/lang/String;)Ljava/lang/Object;
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/common/a/d;->e:Ljava/lang/Object;

    if-eqz v0, :cond_0

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/common/a/d;->e:Ljava/lang/Object;

    .line 177
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/a/d;->b:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/lang/Object;
    .locals 3

    .prologue
    .line 186
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v0

    .line 188
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    .line 190
    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    return-object v2

    :catchall_0
    move-exception v2

    invoke-static {v0, v1}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v2
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/common/a/d;->b:Ljava/lang/String;

    return-object v0
.end method
