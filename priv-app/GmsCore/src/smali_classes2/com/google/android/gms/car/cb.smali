.class final Lcom/google/android/gms/car/cb;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/Runnable;

.field final synthetic b:Lcom/google/android/gms/car/br;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/br;Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 264
    iput-object p1, p0, Lcom/google/android/gms/car/cb;->b:Lcom/google/android/gms/car/br;

    iput-object p2, p0, Lcom/google/android/gms/car/cb;->a:Ljava/lang/Runnable;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 267
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleIncomingMessage on main thread. handler="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/cb;->a:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/cb;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 283
    :goto_0
    return-void

    .line 273
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/cb;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->e(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/senderprotocol/c;

    move-result-object v0

    if-nez v0, :cond_2

    .line 275
    const-string v0, "CAR.BT"

    const-string v1, "handleIncomingMessage: Channel not established yet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 278
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/cb;->b:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->b(Lcom/google/android/gms/car/br;)I

    move-result v0

    if-eqz v0, :cond_3

    .line 279
    const-string v0, "CAR.BT"

    const-string v1, "handleIncomingMessage: Service not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 282
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/cb;->a:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method
