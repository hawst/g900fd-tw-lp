.class final Lcom/google/android/gms/games/ui/c/h;
.super Lcom/google/android/gms/games/ui/c/g;
.source "SourceFile"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0xb
.end annotation


# instance fields
.field private final u:Landroid/view/animation/Animation;

.field private v:Landroid/widget/TextView;

.field private w:F

.field private x:[F

.field private y:[F

.field private z:[I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V
    .locals 6

    .prologue
    .line 166
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/c/g;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;B)V

    .line 170
    const/high16 v0, -0x40800000    # -1.0f

    iput v0, p0, Lcom/google/android/gms/games/ui/c/h;->w:F

    .line 172
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/h;->j()Landroid/content/Context;

    move-result-object v0

    sget v1, Lcom/google/android/gms/b;->l:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->u:Landroid/view/animation/Animation;

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->u:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 175
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;B)V
    .locals 0

    .prologue
    .line 147
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/games/ui/c/h;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/Player;)V

    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    .line 179
    invoke-super {p0}, Lcom/google/android/gms/games/ui/c/g;->a()V

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    sget v1, Lcom/google/android/gms/j;->bF:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->v:Landroid/widget/TextView;

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    .line 185
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Lcom/google/android/gms/games/PlayerLevelInfo;)V

    .line 190
    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->l:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/c/h;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v2

    sget v3, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/c/h;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;I)V

    .line 195
    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 9

    .prologue
    const/4 v8, 0x3

    const/4 v7, 0x0

    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 199
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/c/g;->onAnimationEnd(Landroid/view/animation/Animation;)V

    .line 202
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->g:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->v:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->u:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->r:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/k;->e:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3, v4}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(JZ)V

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->p:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    .line 209
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->q:Lcom/google/android/gms/games/Player;

    invoke-interface {v1}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v1

    .line 214
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->r:Landroid/content/res/Resources;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v0

    .line 216
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->r:Landroid/content/res/Resources;

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/res/Resources;I)I

    move-result v1

    .line 220
    new-array v2, v6, [F

    iput-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    .line 221
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    invoke-static {v0}, Landroid/graphics/Color;->alpha(I)I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v7

    .line 222
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    invoke-static {v0}, Landroid/graphics/Color;->red(I)I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v4

    .line 223
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    invoke-static {v0}, Landroid/graphics/Color;->green(I)I

    move-result v3

    int-to-float v3, v3

    aput v3, v2, v5

    .line 224
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    invoke-static {v0}, Landroid/graphics/Color;->blue(I)I

    move-result v0

    int-to-float v0, v0

    aput v0, v2, v8

    .line 226
    new-array v0, v6, [F

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    invoke-static {v1}, Landroid/graphics/Color;->alpha(I)I

    move-result v2

    int-to-float v2, v2

    aput v2, v0, v7

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    invoke-static {v1}, Landroid/graphics/Color;->red(I)I

    move-result v2

    int-to-float v2, v2

    aput v2, v0, v4

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    invoke-static {v1}, Landroid/graphics/Color;->green(I)I

    move-result v2

    int-to-float v2, v2

    aput v2, v0, v5

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    invoke-static {v1}, Landroid/graphics/Color;->blue(I)I

    move-result v1

    int-to-float v1, v1

    aput v1, v0, v8

    .line 232
    new-array v0, v6, [I

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    .line 235
    new-array v0, v5, [F

    fill-array-data v0, :array_0

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofFloat([F)Landroid/animation/ValueAnimator;

    move-result-object v0

    .line 236
    invoke-virtual {v0, p0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 237
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->r:Landroid/content/res/Resources;

    sget v2, Lcom/google/android/gms/k;->f:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 239
    new-instance v1, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 240
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/gms/games/ui/c/i;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/games/ui/c/i;-><init>(Lcom/google/android/gms/games/ui/c/h;Landroid/animation/ValueAnimator;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->r:Landroid/content/res/Resources;

    sget v3, Lcom/google/android/gms/k;->e:I

    invoke-virtual {v0, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    int-to-long v4, v0

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 247
    :cond_0
    return-void

    .line 235
    :array_0
    .array-data 4
        0x0
        0x3f800000    # 1.0f
    .end array-data
.end method

.method public final onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 252
    iget v0, p0, Lcom/google/android/gms/games/ui/c/h;->w:F

    const/high16 v2, -0x40800000    # -1.0f

    cmpl-float v0, v0, v2

    if-nez v0, :cond_0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->q:Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->n()Lcom/google/android/gms/games/PlayerLevelInfo;

    move-result-object v0

    .line 254
    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevelInfo;->d()Lcom/google/android/gms/games/PlayerLevel;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerLevel;->b()I

    move-result v0

    .line 255
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->v:Landroid/widget/TextView;

    invoke-static {v0}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :cond_0
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/c/h;->w:F

    move v0, v1

    .line 260
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 261
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    aget v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/h;->y:[F

    aget v4, v4, v0

    iget-object v5, p0, Lcom/google/android/gms/games/ui/c/h;->x:[F

    aget v5, v5, v0

    sub-float/2addr v4, v5

    iget v5, p0, Lcom/google/android/gms/games/ui/c/h;->w:F

    mul-float/2addr v4, v5

    add-float/2addr v3, v4

    float-to-int v3, v3

    aput v3, v2, v0

    .line 260
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 264
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    aget v0, v0, v1

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    const/4 v2, 0x1

    aget v1, v1, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    const/4 v3, 0x2

    aget v2, v2, v3

    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/h;->z:[I

    const/4 v4, 0x3

    aget v3, v3, v4

    invoke-static {v0, v1, v2, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v0

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/h;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->f(I)V

    .line 266
    return-void
.end method
