.class final Lcom/google/android/gms/car/hn;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/v;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/gx;

.field private b:Lcom/google/android/gms/car/senderprotocol/q;

.field private volatile c:Z

.field private final d:Ljava/lang/Object;

.field private e:Landroid/os/Handler;

.field private f:Ljava/lang/Runnable;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    .line 1936
    iput-object p1, p0, Lcom/google/android/gms/car/hn;->a:Lcom/google/android/gms/car/gx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1944
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/hn;->c:Z

    .line 1945
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/hn;->d:Ljava/lang/Object;

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/gx;B)V
    .locals 0

    .prologue
    .line 1936
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/hn;-><init>(Lcom/google/android/gms/car/gx;)V

    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 1979
    iget-object v1, p0, Lcom/google/android/gms/car/hn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1980
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->e:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/hn;->f:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 1981
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/car/hn;->f:Ljava/lang/Runnable;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 1983
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final a(I)V
    .locals 6

    .prologue
    .line 1957
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->d(Lcom/google/android/gms/car/gx;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1960
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->b:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/senderprotocol/q;->b(I)V

    .line 1961
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/hn;->c:Z

    .line 1962
    iget-object v1, p0, Lcom/google/android/gms/car/hn;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1963
    :try_start_0
    new-instance v0, Lcom/google/android/gms/car/hv;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/car/hv;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/hn;->e:Landroid/os/Handler;

    .line 1964
    new-instance v0, Lcom/google/android/gms/car/ho;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ho;-><init>(Lcom/google/android/gms/car/hn;)V

    iput-object v0, p0, Lcom/google/android/gms/car/hn;->f:Ljava/lang/Runnable;

    .line 1973
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->e:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/gms/car/hn;->f:Ljava/lang/Runnable;

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1974
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1976
    :cond_0
    return-void

    .line 1974
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/q;)V
    .locals 0

    .prologue
    .line 1993
    iput-object p1, p0, Lcom/google/android/gms/car/hn;->b:Lcom/google/android/gms/car/senderprotocol/q;

    .line 1994
    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 1998
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/hn;->b:Lcom/google/android/gms/car/senderprotocol/q;

    .line 1999
    return-void
.end method

.method public final b(I)V
    .locals 4

    .prologue
    .line 2003
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->b:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/q;->d()V

    .line 2004
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->a:Lcom/google/android/gms/car/gx;

    const/4 v1, 0x4

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "reason:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 2005
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 2009
    iget-boolean v0, p0, Lcom/google/android/gms/car/hn;->c:Z

    if-eqz v0, :cond_0

    .line 2010
    invoke-virtual {p0}, Lcom/google/android/gms/car/hn;->a()V

    .line 2011
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->a:Lcom/google/android/gms/car/gx;

    const/4 v1, 0x5

    const-string v2, "user request"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 2018
    :goto_0
    return-void

    .line 2015
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/hn;->a:Lcom/google/android/gms/car/gx;

    const/4 v1, 0x6

    const-string v2, "got ByeByeResponse without request"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_0
.end method
