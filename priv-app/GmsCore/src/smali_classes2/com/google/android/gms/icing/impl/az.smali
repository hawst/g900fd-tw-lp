.class public final Lcom/google/android/gms/icing/impl/az;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:[Lcom/google/android/gms/icing/k;

.field b:J

.field private final c:Ljava/util/Map;


# direct methods
.method public constructor <init>([Lcom/google/android/gms/icing/k;J)V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    .line 34
    iput-wide p2, p0, Lcom/google/android/gms/icing/impl/az;->b:J

    .line 35
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    array-length v4, v2

    move v1, v3

    :goto_1
    if-ge v1, v4, :cond_3

    aget-object v5, v2, v1

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    iget-object v6, v5, Lcom/google/android/gms/icing/k;->a:Ljava/lang/String;

    invoke-interface {v0, v6}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    .line 57
    if-eqz v0, :cond_2

    .line 58
    iget v6, v5, Lcom/google/android/gms/icing/k;->b:I

    aget v0, v0, v3

    add-int/2addr v0, v6

    iput v0, v5, Lcom/google/android/gms/icing/k;->b:I

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    iget-object v5, v5, Lcom/google/android/gms/icing/k;->a:Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    :cond_2
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 63
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    array-length v1, v0

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    array-length v2, v2

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v4}, Ljava/util/Map;->size()I

    move-result v4

    add-int/2addr v2, v4

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/k;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v2, v1

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v1, v0

    check-cast v1, Ljava/util/Map$Entry;

    .line 68
    new-instance v5, Lcom/google/android/gms/icing/k;

    invoke-direct {v5}, Lcom/google/android/gms/icing/k;-><init>()V

    .line 69
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/gms/icing/k;->a:Ljava/lang/String;

    .line 70
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v0, v0, v3

    iput v0, v5, Lcom/google/android/gms/icing/k;->b:I

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    add-int/lit8 v0, v2, 0x1

    aput-object v5, v1, v2

    move v2, v0

    .line 72
    goto :goto_2

    .line 75
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/icing/impl/ba;->a:Lcom/google/android/gms/icing/impl/ba;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    goto/16 :goto_0
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    new-array v1, v3, [I

    aput v3, v1, v2

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 39
    :goto_0
    return-void

    .line 38
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/az;->c:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [I

    aget v1, v0, v2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v2

    goto :goto_0
.end method
