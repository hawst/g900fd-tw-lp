.class public final Lcom/google/android/gms/fitness/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Landroid/util/SparseArray;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 85
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0x6d

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 92
    sput-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x9

    const-string v2, "aerobics"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 100
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xa

    const-string v2, "badminton"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 108
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xb

    const-string v2, "baseball"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 116
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string v2, "basketball"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 124
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xd

    const-string v2, "biathlon"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 132
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x1

    const-string v2, "biking"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 140
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xe

    const-string v2, "biking.hand"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 148
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0xf

    const-string v2, "biking.mountain"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 156
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x10

    const-string v2, "biking.road"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 164
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x11

    const-string v2, "biking.spinning"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 172
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x12

    const-string v2, "biking.stationary"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 180
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x13

    const-string v2, "biking.utility"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 188
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x14

    const-string v2, "boxing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 196
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x15

    const-string v2, "calisthenics"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 207
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x16

    const-string v2, "circuit_training"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 215
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x17

    const-string v2, "cricket"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 223
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x6a

    const-string v2, "curling"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 231
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x18

    const-string v2, "dancing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 239
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x66

    const-string v2, "diving"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 247
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x19

    const-string v2, "elliptical"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 255
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x67

    const-string v2, "ergometer"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 269
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v2, "exiting_vehicle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 277
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1a

    const-string v2, "fencing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 285
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1b

    const-string v2, "football.american"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 293
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1c

    const-string v2, "football.australian"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 301
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1d

    const-string v2, "football.soccer"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 309
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1e

    const-string v2, "frisbee_disc"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 317
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x1f

    const-string v2, "gardening"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 325
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x20

    const-string v2, "golf"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 333
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x21

    const-string v2, "gymnastics"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 341
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x22

    const-string v2, "handball"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 349
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x23

    const-string v2, "hiking"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 357
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x24

    const-string v2, "hockey"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 365
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x25

    const-string v2, "horseback_riding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 373
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x26

    const-string v2, "housework"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 381
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x68

    const-string v2, "ice_skating"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 389
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "in_vehicle"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 397
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x27

    const-string v2, "jump_rope"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 405
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x28

    const-string v2, "kayaking"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 413
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x29

    const-string v2, "kettlebell_training"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 421
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x6b

    const-string v2, "kick_scooter"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 429
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2a

    const-string v2, "kickboxing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 437
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2b

    const-string v2, "kitesurfing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 445
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2c

    const-string v2, "martial_arts"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 453
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2d

    const-string v2, "meditation"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 461
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2e

    const-string v2, "martial_arts.mixed"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 472
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "on_foot"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 492
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x6c

    const-string v2, "other"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 500
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x2f

    const-string v2, "p90x"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 508
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x30

    const-string v2, "paragliding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 516
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x31

    const-string v2, "pilates"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 524
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x32

    const-string v2, "polo"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 532
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x33

    const-string v2, "racquetball"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 540
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x34

    const-string v2, "rock_climbing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 548
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x35

    const-string v2, "rowing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 556
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x36

    const-string v2, "rowing.machine"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 564
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x37

    const-string v2, "rugby"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 572
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string v2, "running"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 580
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x38

    const-string v2, "running.jogging"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 588
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x39

    const-string v2, "running.sand"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 596
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3a

    const-string v2, "running.treadmill"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 604
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3b

    const-string v2, "sailing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 612
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3c

    const-string v2, "scuba_diving"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 620
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3d

    const-string v2, "skateboarding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 628
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3e

    const-string v2, "skating"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 636
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x3f

    const-string v2, "skating.cross"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 644
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x69

    const-string v2, "skating.indoor"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 652
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x40

    const-string v2, "skating.inline"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 660
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x41

    const-string v2, "skiing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 668
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x42

    const-string v2, "skiing.back_country"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 676
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x43

    const-string v2, "skiing.cross_country"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 684
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x44

    const-string v2, "skiing.downhill"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 692
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x45

    const-string v2, "skiing.kite"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 700
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x46

    const-string v2, "skiing.roller"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 708
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x47

    const-string v2, "sledding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 716
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x48

    const-string v2, "sleep"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 724
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x49

    const-string v2, "snowboarding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 732
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4a

    const-string v2, "snowmobile"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 740
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4b

    const-string v2, "snowshoeing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 748
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4c

    const-string v2, "squash"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 756
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4d

    const-string v2, "stair_climbing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 764
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4e

    const-string v2, "stair_climbing.machine"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 772
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x4f

    const-string v2, "standup_paddleboarding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 780
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x3

    const-string v2, "still"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 788
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x50

    const-string v2, "strength_training"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 796
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x51

    const-string v2, "surfing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 804
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x52

    const-string v2, "swimming"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 812
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x53

    const-string v2, "swimming.pool"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 820
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x54

    const-string v2, "swimming.open_water"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 828
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x55

    const-string v2, "table_tennis"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 836
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x56

    const-string v2, "team_sports"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 844
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x57

    const-string v2, "tennis"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 858
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x5

    const-string v2, "tilting"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 866
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x58

    const-string v2, "treadmill"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 877
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x4

    const-string v2, "unknown"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 885
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x59

    const-string v2, "volleyball"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 893
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5a

    const-string v2, "volleyball.beach"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 901
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5b

    const-string v2, "volleyball.indoor"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 909
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5c

    const-string v2, "wakeboarding"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 917
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/4 v1, 0x7

    const-string v2, "walking"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 925
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5d

    const-string v2, "walking.fitness"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 933
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5e

    const-string v2, "walking.nordic"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 941
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x5f

    const-string v2, "walking.treadmill"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 949
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x60

    const-string v2, "water_polo"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 957
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x61

    const-string v2, "weightlifting"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 965
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x62

    const-string v2, "wheelchair"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 973
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x63

    const-string v2, "windsurfing"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 981
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x64

    const-string v2, "yoga"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 989
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    const/16 v1, 0x65

    const-string v2, "zumba"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 990
    return-void
.end method

.method public static a(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v2, 0x4

    .line 1024
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1025
    :goto_1
    if-gez v1, :cond_3

    move v0, v2

    .line 1037
    :cond_0
    :goto_2
    return v0

    .line 1024
    :cond_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_2
    const/4 v1, -0x1

    goto :goto_1

    .line 1029
    :cond_3
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 1030
    const/16 v1, 0x6c

    if-le v0, v1, :cond_0

    move v0, v2

    .line 1035
    goto :goto_2
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1064
    sget-object v0, Lcom/google/android/gms/fitness/e;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1065
    if-nez v0, :cond_0

    .line 1066
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown activity "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1068
    :cond_0
    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1080
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "vnd.google.fitness.activity/"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
