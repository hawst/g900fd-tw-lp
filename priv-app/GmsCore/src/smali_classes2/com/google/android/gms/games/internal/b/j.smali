.class public final Lcom/google/android/gms/games/internal/b/j;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 30
    packed-switch p0, :pswitch_data_0

    .line 44
    const-string v0, "TurnBasedMatchStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown match turn status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 45
    const-string v0, "UNKNOWN_STATUS"

    :goto_0
    return-object v0

    .line 32
    :pswitch_0
    const-string v0, "MATCH_AUTO_MATCHING"

    goto :goto_0

    .line 34
    :pswitch_1
    const-string v0, "MATCH_ACTIVE"

    goto :goto_0

    .line 36
    :pswitch_2
    const-string v0, "MATCH_COMPLETE"

    goto :goto_0

    .line 38
    :pswitch_3
    const-string v0, "MATCH_EXPIRED"

    goto :goto_0

    .line 40
    :pswitch_4
    const-string v0, "MATCH_CANCELED"

    goto :goto_0

    .line 42
    :pswitch_5
    const-string v0, "MATCH_DELETED"

    goto :goto_0

    .line 30
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch
.end method
