.class public Lcom/google/android/gms/car/CarHomeActivity;
.super Landroid/app/Activity;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/car/ea;

.field private b:Lcom/google/android/gms/car/en;

.field private c:Lcom/google/android/gms/car/eu;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/android/gms/car/hs;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 34
    new-instance v0, Lcom/google/android/gms/car/ej;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ej;-><init>(Lcom/google/android/gms/car/CarHomeActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->f:Lcom/google/android/gms/car/hs;

    return-void
.end method

.method private a()V
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->d:Z

    .line 198
    iget-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->e:Z

    if-nez v0, :cond_1

    .line 210
    :cond_0
    :goto_0
    return-void

    .line 202
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->c:Lcom/google/android/gms/car/eu;

    invoke-virtual {v0}, Lcom/google/android/gms/car/eu;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 206
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarHomeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dw:I

    iget-object v2, p0, Lcom/google/android/gms/car/CarHomeActivity;->c:Lcom/google/android/gms/car/eu;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/CarHomeActivity;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->a()V

    return-void
.end method

.method private b()V
    .locals 3

    .prologue
    .line 216
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->d:Z

    .line 218
    iget-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->e:Z

    if-nez v0, :cond_1

    .line 230
    :cond_0
    :goto_0
    return-void

    .line 222
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->b:Lcom/google/android/gms/car/en;

    invoke-virtual {v0}, Lcom/google/android/gms/car/en;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarHomeActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v0

    sget v1, Lcom/google/android/gms/j;->dw:I

    iget-object v2, p0, Lcom/google/android/gms/car/CarHomeActivity;->b:Lcom/google/android/gms/car/en;

    invoke-virtual {v0, v1, v2}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentTransaction;->commit()I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/CarHomeActivity;)V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->b()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/ea;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->a:Lcom/google/android/gms/car/ea;

    return-object v0
.end method

.method private c()V
    .locals 3

    .prologue
    const/16 v1, 0x400

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/car/CarHomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    .line 235
    new-instance v0, Lcom/google/android/gms/car/qe;

    invoke-virtual {p0}, Lcom/google/android/gms/car/CarHomeActivity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    const v2, 0x1020002

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/qe;-><init>(Landroid/view/View;)V

    .line 237
    invoke-virtual {v0}, Lcom/google/android/gms/car/qe;->a()V

    .line 238
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/hs;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->f:Lcom/google/android/gms/car/hs;

    return-object v0
.end method


# virtual methods
.method public dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1

    .prologue
    .line 181
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 187
    invoke-super {p0, p1}, Landroid/app/Activity;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 189
    :goto_0
    return v0

    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x18
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public onBackPressed()V
    .locals 0

    .prologue
    .line 177
    return-void
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 49
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 50
    const-string v0, "CAR.HOME"

    const-string v1, "CarHomeActivity::onCreate"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarHomeActivity;->requestWindowFeature(I)Z

    .line 54
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->c()V

    .line 56
    new-instance v0, Lcom/google/android/gms/car/en;

    invoke-direct {v0}, Lcom/google/android/gms/car/en;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->b:Lcom/google/android/gms/car/en;

    .line 57
    new-instance v0, Lcom/google/android/gms/car/eu;

    invoke-direct {v0}, Lcom/google/android/gms/car/eu;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->c:Lcom/google/android/gms/car/eu;

    .line 59
    sget v0, Lcom/google/android/gms/l;->L:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/CarHomeActivity;->setContentView(I)V

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->a()V

    .line 62
    new-instance v0, Lcom/google/android/gms/car/ea;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/car/ek;

    invoke-direct {v3, p0}, Lcom/google/android/gms/car/ek;-><init>(Lcom/google/android/gms/car/CarHomeActivity;)V

    new-instance v4, Lcom/google/android/gms/car/el;

    invoke-direct {v4, p0}, Lcom/google/android/gms/car/el;-><init>(Lcom/google/android/gms/car/CarHomeActivity;)V

    new-instance v5, Lcom/google/android/gms/car/em;

    invoke-direct {v5, p0}, Lcom/google/android/gms/car/em;-><init>(Lcom/google/android/gms/car/CarHomeActivity;)V

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ea;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/car/t;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->a:Lcom/google/android/gms/car/ea;

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->a:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->a()V

    .line 134
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 138
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    const-string v0, "CAR.HOME"

    const-string v1, "CarHomeActivity::onDestroy"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->a:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->c_()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->a:Lcom/google/android/gms/car/ea;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->b()V

    .line 146
    :cond_1
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 147
    return-void
.end method

.method protected onPause()V
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->e:Z

    .line 171
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 172
    return-void
.end method

.method protected onResume()V
    .locals 2

    .prologue
    .line 151
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 152
    const-string v0, "CAR.HOME"

    const-string v1, "CarHomeActivity::onResume"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 156
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->e:Z

    .line 158
    iget-boolean v0, p0, Lcom/google/android/gms/car/CarHomeActivity;->d:Z

    if-eqz v0, :cond_1

    .line 159
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->a()V

    .line 164
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->c()V

    .line 165
    return-void

    .line 161
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/car/CarHomeActivity;->b()V

    goto :goto_0
.end method
