.class public final Lcom/google/android/gms/drive/internal/be;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/internal/az;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/common/internal/az;

    const-string v1, "GmsDrive"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/az;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 82
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    .line 83
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 70
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/gms/common/internal/az;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 71
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/az;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 75
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 38
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 39
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 66
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p2, p1}, Lcom/google/android/gms/common/internal/az;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 67
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 54
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 62
    sget-object v0, Lcom/google/android/gms/drive/internal/be;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 63
    return-void
.end method
