.class public final Lcom/google/android/gms/games/h/a/ep;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 75
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 77
    sput-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "androidNetworkSubtype"

    const-string v2, "androidNetworkSubtype"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 78
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "androidNetworkType"

    const-string v2, "androidNetworkType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "iosNetworkType"

    const-string v2, "iosNetworkType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "networkOperatorCode"

    const-string v2, "networkOperatorCode"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "networkOperatorName"

    const-string v2, "networkOperatorName"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 82
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "peerSession"

    const-string v2, "peerSession"

    const-class v3, Lcom/google/android/gms/games/h/a/cp;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    const-string v1, "socketsUsed"

    const-string v2, "socketsUsed"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 85
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 92
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/ep;->c:Ljava/util/HashMap;

    .line 92
    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/String;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 102
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 126
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/ep;->c:Ljava/util/HashMap;

    .line 103
    if-eqz p1, :cond_0

    .line 104
    const-string v0, "androidNetworkSubtype"

    invoke-virtual {p1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;I)V

    .line 106
    :cond_0
    if-eqz p2, :cond_1

    .line 107
    const-string v0, "androidNetworkType"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;I)V

    .line 109
    :cond_1
    if-eqz p3, :cond_2

    .line 113
    const-string v0, "networkOperatorCode"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 115
    :cond_2
    if-eqz p4, :cond_3

    .line 116
    const-string v0, "networkOperatorName"

    invoke-virtual {p0, v0, p4}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 118
    :cond_3
    if-eqz p5, :cond_4

    .line 119
    const-string v0, "peerSession"

    invoke-virtual {p0, v0, p5}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 121
    :cond_4
    if-eqz p6, :cond_5

    .line 122
    const-string v0, "socketsUsed"

    invoke-virtual {p6}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/ep;->a(Ljava/lang/String;Z)V

    .line 124
    :cond_5
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 89
    sget-object v0, Lcom/google/android/gms/games/h/a/ep;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/ep;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    return-void
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/ep;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getPeerSession()Ljava/util/ArrayList;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/ep;->c:Ljava/util/HashMap;

    const-string v1, "peerSession"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
