.class public Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:J

.field final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    new-instance v0, Lcom/google/android/gms/drive/internal/co;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/co;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IJJ)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    iput p1, p0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->a:I

    .line 53
    iput-wide p2, p0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->b:J

    .line 54
    iput-wide p4, p0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->c:J

    .line 55
    return-void
.end method

.method public constructor <init>(JJ)V
    .locals 7

    .prologue
    .line 58
    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;-><init>(IJJ)V

    .line 59
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 62
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->b:J

    return-wide v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 66
    iget-wide v0, p0, Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;->c:J

    return-wide v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 39
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/internal/co;->a(Lcom/google/android/gms/drive/internal/OnDownloadProgressResponse;Landroid/os/Parcel;)V

    .line 40
    return-void
.end method
