.class public final Lcom/google/android/gms/icing/af;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/ac;

.field public b:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 11629
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 11630
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    const-string v0, "en"

    iput-object v0, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/af;->cachedSize:I

    .line 11631
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 11692
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 11693
    iget-object v1, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-eqz v1, :cond_0

    .line 11694
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11697
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    const-string v2, "en"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 11698
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 11701
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 11642
    if-ne p1, p0, :cond_1

    .line 11665
    :cond_0
    :goto_0
    return v0

    .line 11645
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/af;

    if-nez v2, :cond_2

    move v0, v1

    .line 11646
    goto :goto_0

    .line 11648
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/af;

    .line 11649
    iget-object v2, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-nez v2, :cond_3

    .line 11650
    iget-object v2, p1, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-eqz v2, :cond_4

    move v0, v1

    .line 11651
    goto :goto_0

    .line 11654
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    iget-object v3, p1, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/ac;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 11655
    goto :goto_0

    .line 11658
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    if-nez v2, :cond_5

    .line 11659
    iget-object v2, p1, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 11660
    goto :goto_0

    .line 11662
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 11663
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 11670
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 11673
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 11675
    return v0

    .line 11670
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/ac;->hashCode()I

    move-result v0

    goto :goto_0

    .line 11673
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 11606
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/ac;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ac;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 11681
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    if-eqz v0, :cond_0

    .line 11682
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 11684
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    const-string v1, "en"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 11685
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/af;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 11687
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 11688
    return-void
.end method
