.class public final Lcom/google/android/gms/car/senderprotocol/a;
.super Lcom/google/android/gms/car/senderprotocol/aq;
.source "SourceFile"


# instance fields
.field private f:Lcom/google/android/gms/car/senderprotocol/b;


# direct methods
.method public constructor <init>(ILcom/google/android/gms/car/gw;Lcom/google/android/gms/car/senderprotocol/b;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p2, p4}, Lcom/google/android/gms/car/senderprotocol/aq;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 25
    iput p1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->a:I

    .line 26
    iput-object p3, p0, Lcom/google/android/gms/car/senderprotocol/a;->f:Lcom/google/android/gms/car/senderprotocol/b;

    .line 27
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/c/b/ct;)V
    .locals 0

    .prologue
    .line 48
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/ct;)V

    .line 49
    return-void
.end method

.method protected final a(Lcom/google/android/c/b/t;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/t;)V

    .line 39
    iget v0, p1, Lcom/google/android/c/b/t;->a:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 40
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/a;->f:Lcom/google/android/gms/car/senderprotocol/b;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/b;->b()V

    .line 44
    :goto_0
    return-void

    .line 42
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/a;->f:Lcom/google/android/gms/car/senderprotocol/b;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/b;->c()V

    goto :goto_0
.end method

.method public final a_(I)V
    .locals 2

    .prologue
    .line 30
    new-instance v0, Lcom/google/android/c/b/ct;

    invoke-direct {v0}, Lcom/google/android/c/b/ct;-><init>()V

    .line 31
    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/a;->c:I

    iput v1, v0, Lcom/google/android/c/b/ct;->a:I

    .line 32
    iput p1, v0, Lcom/google/android/c/b/ct;->b:I

    .line 33
    invoke-super {p0, v0}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/ct;)V

    .line 34
    return-void
.end method

.method public final l_()V
    .locals 1

    .prologue
    .line 53
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/aq;->l_()V

    .line 55
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/a;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/a;->c:I

    .line 56
    return-void
.end method
