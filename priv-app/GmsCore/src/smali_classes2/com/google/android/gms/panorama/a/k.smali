.class final Lcom/google/android/gms/panorama/a/k;
.super Lcom/google/android/gms/panorama/a/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 137
    invoke-direct {p0}, Lcom/google/android/gms/panorama/a/c;-><init>()V

    .line 138
    iput-object p1, p0, Lcom/google/android/gms/panorama/a/k;->a:Lcom/google/android/gms/common/api/m;

    .line 139
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;ILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 144
    .line 145
    if-eqz p2, :cond_0

    .line 146
    const-string v0, "pendingIntent"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 148
    :goto_0
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, p1, v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/panorama/a/k;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/panorama/a/a;

    invoke-direct {v1, v2, p4, p3}, Lcom/google/android/gms/panorama/a/a;-><init>(Lcom/google/android/gms/common/api/Status;Landroid/content/Intent;I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 150
    return-void

    :cond_0
    move-object v0, v1

    goto :goto_0
.end method
