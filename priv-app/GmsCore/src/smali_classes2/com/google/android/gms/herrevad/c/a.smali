.class public final Lcom/google/android/gms/herrevad/c/a;
.super Lcom/google/android/gms/mdm/d/g;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/mdm/d/g;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    .line 57
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/k/f/ai;
    .locals 11
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const/4 v6, -0x1

    const/16 v10, 0x11

    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 165
    new-instance v5, Lcom/google/k/f/ai;

    invoke-direct {v5}, Lcom/google/k/f/ai;-><init>()V

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v0

    if-nez v0, :cond_1

    move v0, v6

    .line 171
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 265
    :cond_0
    :goto_1
    :pswitch_0
    return-object v3

    .line 169
    :cond_1
    invoke-virtual {v0}, Landroid/net/NetworkInfo;->getType()I

    move-result v0

    goto :goto_0

    .line 173
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v1, v10, :cond_9

    move-object v1, v3

    :cond_2
    :goto_2
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v10, :cond_e

    move v0, v4

    :goto_3
    if-eqz v0, :cond_0

    .line 198
    :cond_3
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2a

    .line 200
    invoke-virtual {p1}, Ljava/lang/String;->hashCode()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    :cond_4
    move v0, v6

    :goto_4
    packed-switch v0, :pswitch_data_1

    .line 218
    iput-object p1, v5, Lcom/google/k/f/ai;->a:Ljava/lang/String;

    .line 227
    :goto_5
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 228
    if-eqz v0, :cond_5

    .line 229
    iput-object v0, v5, Lcom/google/k/f/ai;->c:Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 236
    :cond_5
    const-string v0, "latency_micros"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 237
    const-string v0, "latency_micros"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, v5, Lcom/google/k/f/ai;->p:I

    .line 240
    :cond_6
    const-string v0, "latency_bps"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 241
    const-string v0, "latency_bps"

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v0

    iput-wide v0, v5, Lcom/google/k/f/ai;->q:J

    .line 249
    :cond_7
    new-instance v1, Lcom/google/android/gms/herrevad/d/b;

    invoke-direct {v1}, Lcom/google/android/gms/herrevad/d/b;-><init>()V

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/16 v8, 0x3e8

    div-long/2addr v2, v8

    iput-wide v2, v1, Lcom/google/android/gms/herrevad/d/b;->a:J

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->f:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/herrevad/d/b;->c:Ljava/lang/Integer;

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->g:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/herrevad/d/b;->d:Ljava/lang/Long;

    sget-object v0, Lcom/google/android/gms/herrevad/a/a;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/gms/herrevad/d/b;->b:J

    invoke-virtual {v1}, Lcom/google/android/gms/herrevad/d/b;->a()I

    move-result v0

    iput v0, v5, Lcom/google/k/f/ai;->r:I

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->a()Landroid/location/Location;

    move-result-object v0

    .line 253
    if-eqz v0, :cond_8

    .line 254
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    if-lt v1, v10, :cond_2b

    .line 255
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    invoke-static {v0, v5, v2, v3, v7}, Lcom/google/android/gms/herrevad/c/a;->a(Landroid/location/Location;Lcom/google/k/f/ai;JZ)V

    :cond_8
    :goto_6
    move-object v3, v5

    .line 265
    goto/16 :goto_1

    .line 173
    :cond_9
    const-string v1, "phone"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getAllCellInfo()Ljava/util/List;

    move-result-object v0

    if-nez v0, :cond_a

    move-object v1, v3

    goto/16 :goto_2

    :cond_a
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move-object v1, v3

    move v2, v4

    :goto_7
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/CellInfo;

    invoke-virtual {v0}, Landroid/telephony/CellInfo;->isRegistered()Z

    move-result v9

    if-eqz v9, :cond_c

    add-int/lit8 v1, v2, 0x1

    if-le v1, v7, :cond_d

    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_b

    const-string v0, "Herrevad"

    const-string v1, "more than one registered CellInfo.  Can\'t tell which is active.  Skipping."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    move-object v1, v3

    goto/16 :goto_2

    :cond_c
    move-object v0, v1

    move v1, v2

    :cond_d
    move v2, v1

    move-object v1, v0

    goto :goto_7

    :cond_e
    instance-of v0, v1, Landroid/telephony/CellInfoCdma;

    if-eqz v0, :cond_10

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoCdma;->getCellIdentity()Landroid/telephony/CellIdentityCdma;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoCdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoCdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthCdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthCdma;->getDbm()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->f:I

    move-object v1, v0

    :goto_8
    if-eqz v1, :cond_1d

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    if-ge v0, v10, :cond_15

    move-object v2, v3

    :goto_9
    if-eqz v2, :cond_1b

    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "phone"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v8, "connectivity"

    invoke-virtual {v1, v8}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getNetworkType()I

    move-result v8

    iput v8, v2, Lcom/google/k/f/ab;->e:I

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->isNetworkRoaming()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/k/f/ab;->f:Z

    invoke-virtual {v1, v4}, Landroid/net/ConnectivityManager;->getNetworkInfo(I)Landroid/net/NetworkInfo;

    move-result-object v0

    if-eqz v0, :cond_f

    invoke-virtual {v0}, Landroid/net/NetworkInfo;->isRoaming()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/k/f/ab;->g:Z

    :cond_f
    iput-object v2, v5, Lcom/google/k/f/ai;->d:Lcom/google/k/f/ab;

    move v0, v7

    goto/16 :goto_3

    :cond_10
    instance-of v0, v1, Landroid/telephony/CellInfoGsm;

    if-eqz v0, :cond_11

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v0}, Landroid/telephony/CellInfoGsm;->getCellIdentity()Landroid/telephony/CellIdentityGsm;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoGsm;

    invoke-virtual {v1}, Landroid/telephony/CellInfoGsm;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthGsm;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthGsm;->getDbm()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->f:I

    move-object v1, v0

    goto :goto_8

    :cond_11
    instance-of v0, v1, Landroid/telephony/CellInfoLte;

    if-eqz v0, :cond_12

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoLte;

    invoke-virtual {v0}, Landroid/telephony/CellInfoLte;->getCellIdentity()Landroid/telephony/CellIdentityLte;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoLte;

    invoke-virtual {v1}, Landroid/telephony/CellInfoLte;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthLte;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthLte;->getDbm()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->f:I

    move-object v1, v0

    goto :goto_8

    :cond_12
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v2, 0x12

    if-lt v0, v2, :cond_13

    instance-of v0, v1, Landroid/telephony/CellInfoWcdma;

    if-eqz v0, :cond_13

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v0}, Landroid/telephony/CellInfoWcdma;->getCellIdentity()Landroid/telephony/CellIdentityWcdma;

    move-result-object v0

    check-cast v1, Landroid/telephony/CellInfoWcdma;

    invoke-virtual {v1}, Landroid/telephony/CellInfoWcdma;->getCellSignalStrength()Landroid/telephony/CellSignalStrengthWcdma;

    move-result-object v1

    invoke-virtual {v1}, Landroid/telephony/CellSignalStrengthWcdma;->getDbm()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->f:I

    move-object v1, v0

    goto/16 :goto_8

    :cond_13
    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_14

    const-string v0, "Herrevad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v8, "Registered cellinfo is unrecognized type "

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_14
    move v0, v4

    goto/16 :goto_3

    :cond_15
    new-instance v2, Lcom/google/k/f/ab;

    invoke-direct {v2}, Lcom/google/k/f/ab;-><init>()V

    instance-of v0, v1, Landroid/telephony/CellIdentityCdma;

    if-eqz v0, :cond_16

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityCdma;

    new-instance v8, Lcom/google/k/f/aa;

    invoke-direct {v8}, Lcom/google/k/f/aa;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getNetworkId()I

    move-result v9

    iput v9, v8, Lcom/google/k/f/aa;->a:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getSystemId()I

    move-result v9

    iput v9, v8, Lcom/google/k/f/aa;->b:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityCdma;->getBasestationId()I

    move-result v0

    iput v0, v8, Lcom/google/k/f/aa;->c:I

    iput-object v8, v2, Lcom/google/k/f/ab;->a:Lcom/google/k/f/aa;

    goto/16 :goto_9

    :cond_16
    instance-of v0, v1, Landroid/telephony/CellIdentityGsm;

    if-eqz v0, :cond_17

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityGsm;

    new-instance v8, Lcom/google/k/f/ac;

    invoke-direct {v8}, Lcom/google/k/f/ac;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getMcc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ac;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getMnc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ac;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getLac()I

    move-result v9

    iput v9, v8, Lcom/google/k/f/ac;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityGsm;->getCid()I

    move-result v0

    iput v0, v8, Lcom/google/k/f/ac;->d:I

    iput-object v8, v2, Lcom/google/k/f/ab;->b:Lcom/google/k/f/ac;

    goto/16 :goto_9

    :cond_17
    instance-of v0, v1, Landroid/telephony/CellIdentityLte;

    if-eqz v0, :cond_18

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityLte;

    new-instance v8, Lcom/google/k/f/ad;

    invoke-direct {v8}, Lcom/google/k/f/ad;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getMcc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ad;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getMnc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ad;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getTac()I

    move-result v9

    iput v9, v8, Lcom/google/k/f/ad;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityLte;->getCi()I

    move-result v0

    iput v0, v8, Lcom/google/k/f/ad;->d:I

    iput-object v8, v2, Lcom/google/k/f/ab;->c:Lcom/google/k/f/ad;

    goto/16 :goto_9

    :cond_18
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v8, 0x12

    if-lt v0, v8, :cond_19

    instance-of v0, v1, Landroid/telephony/CellIdentityWcdma;

    if-eqz v0, :cond_19

    move-object v0, v1

    check-cast v0, Landroid/telephony/CellIdentityWcdma;

    new-instance v8, Lcom/google/k/f/ae;

    invoke-direct {v8}, Lcom/google/k/f/ae;-><init>()V

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getMcc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ae;->a:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getMnc()I

    move-result v9

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v8, Lcom/google/k/f/ae;->b:Ljava/lang/String;

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getLac()I

    move-result v9

    iput v9, v8, Lcom/google/k/f/ae;->c:I

    invoke-virtual {v0}, Landroid/telephony/CellIdentityWcdma;->getCid()I

    move-result v0

    iput v0, v8, Lcom/google/k/f/ae;->d:I

    iput-object v8, v2, Lcom/google/k/f/ab;->d:Lcom/google/k/f/ae;

    goto/16 :goto_9

    :cond_19
    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_1a

    const-string v0, "Herrevad"

    const-string v2, "Registered cellinfo is unrecognized"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1a
    move-object v2, v3

    goto/16 :goto_9

    :cond_1b
    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_1c

    const-string v0, "Herrevad"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v8, "unable to create cell identity proto from cellid: "

    invoke-direct {v2, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1c
    move v0, v4

    goto/16 :goto_3

    :cond_1d
    const-string v0, "Herrevad"

    const-string v1, "mobile connection type with no cell id"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    goto/16 :goto_3

    .line 176
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/herrevad/d/d;->c(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_20

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/herrevad/d/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1e

    new-instance v1, Lcom/google/k/f/af;

    invoke-direct {v1}, Lcom/google/k/f/af;-><init>()V

    iput-object v0, v1, Lcom/google/k/f/af;->b:Ljava/lang/String;

    iput-boolean v7, v1, Lcom/google/k/f/af;->c:Z

    iput-object v1, v5, Lcom/google/k/f/ai;->e:Lcom/google/k/f/af;

    :goto_a
    if-eqz v7, :cond_1f

    move-object v3, v5

    .line 179
    goto/16 :goto_1

    .line 178
    :cond_1e
    const-string v0, "Herrevad"

    const-string v1, "hashedBssid null, can\'t report that it\'s nomap"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v7, v4

    goto :goto_a

    .line 181
    :cond_1f
    const-string v0, "Herrevad"

    const-string v1, "Unable to create nomap event"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 186
    :cond_20
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/herrevad/d/d;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->d(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_23

    const-string v0, "Herrevad"

    const-string v1, "Looking up Wifi info on a non-wifi device"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_21
    move-object v1, v3

    :goto_b
    if-eqz v1, :cond_22

    if-nez v2, :cond_24

    :cond_22
    const-string v0, "Herrevad"

    const-string v1, "hashedBssid or hashedSsidBssid null, can\'t report on this"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v4

    :goto_c
    if-nez v0, :cond_3

    goto/16 :goto_1

    :cond_23
    const-string v1, "wifi"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/wifi/WifiManager;

    invoke-virtual {v0}, Landroid/net/wifi/WifiManager;->getConnectionInfo()Landroid/net/wifi/WifiInfo;

    move-result-object v0

    if-eqz v0, :cond_21

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_21

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getSSID()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/net/wifi/WifiInfo;->getBSSID()Ljava/lang/String;

    move-result-object v0

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v10}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v9

    invoke-static {v1, v9}, Lcom/google/android/gms/herrevad/d/d;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/d/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    goto :goto_b

    :cond_24
    new-instance v8, Lcom/google/k/f/af;

    invoke-direct {v8}, Lcom/google/k/f/af;-><init>()V

    iput-object v1, v8, Lcom/google/k/f/af;->a:Ljava/lang/String;

    iput-object v2, v8, Lcom/google/k/f/af;->b:Ljava/lang/String;

    iput-boolean v4, v8, Lcom/google/k/f/af;->c:Z

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->c:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_25

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->d:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_25

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->e:Lcom/google/android/gms/common/a/r;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/k/f/af;->e:Z

    :cond_25
    iput-object v8, v5, Lcom/google/k/f/ai;->e:Lcom/google/k/f/af;

    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    const-string v1, "connectivity"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    invoke-static {v0}, Landroid/support/v4/d/a;->a(Landroid/net/ConnectivityManager;)Z

    move-result v0

    iput-boolean v0, v8, Lcom/google/k/f/af;->f:Z

    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/herrevad/d/d;->a(Landroid/content/Context;)Lcom/google/android/gms/herrevad/d/e;

    move-result-object v0

    if-eqz v0, :cond_29

    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->d:Ljava/lang/Integer;

    if-eqz v1, :cond_26

    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->d:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->f:I

    :cond_26
    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->a:Ljava/lang/Integer;

    if-eqz v1, :cond_27

    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->a:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v5, Lcom/google/k/f/ai;->s:I

    :cond_27
    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->b:Lcom/google/k/f/ag;

    if-eqz v1, :cond_28

    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->b:Lcom/google/k/f/ag;

    iput-object v1, v8, Lcom/google/k/f/af;->g:Lcom/google/k/f/ag;

    :cond_28
    iget-object v1, v0, Lcom/google/android/gms/herrevad/d/e;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_29

    iget-object v0, v0, Lcom/google/android/gms/herrevad/d/e;->c:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v8, Lcom/google/k/f/af;->h:I

    :cond_29
    move v0, v7

    goto/16 :goto_c

    .line 200
    :sswitch_0
    const-string v0, "com.google.android.youtube"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v4

    goto/16 :goto_4

    :sswitch_1
    const-string v0, "com.google.android.apps.gmm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v7

    goto/16 :goto_4

    :sswitch_2
    const-string v0, "com.google.android.apps.maps"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x2

    goto/16 :goto_4

    :sswitch_3
    const-string v0, "com.google.android.gm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x3

    goto/16 :goto_4

    :sswitch_4
    const-string v0, "com.google.android.videos"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x4

    goto/16 :goto_4

    :sswitch_5
    const-string v0, "com.google.android.apps.tycho"

    invoke-virtual {p1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x5

    goto/16 :goto_4

    .line 202
    :pswitch_3
    const/4 v0, 0x2

    iput v0, v5, Lcom/google/k/f/ai;->b:I

    goto/16 :goto_5

    .line 206
    :pswitch_4
    iput v7, v5, Lcom/google/k/f/ai;->b:I

    goto/16 :goto_5

    .line 209
    :pswitch_5
    const/4 v0, 0x3

    iput v0, v5, Lcom/google/k/f/ai;->b:I

    goto/16 :goto_5

    .line 212
    :pswitch_6
    const/4 v0, 0x4

    iput v0, v5, Lcom/google/k/f/ai;->b:I

    goto/16 :goto_5

    .line 215
    :pswitch_7
    const/4 v0, 0x6

    iput v0, v5, Lcom/google/k/f/ai;->b:I

    goto/16 :goto_5

    .line 222
    :cond_2a
    const-string v0, "Herrevad"

    const-string v1, "No source package sent.  Not logging this event."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 232
    :catch_0
    move-exception v0

    const-string v0, "Herrevad"

    const-string v1, "Package name not recognized after it was verified"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1

    .line 258
    :cond_2b
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v6, 0x989680

    mul-long/2addr v2, v6

    invoke-static {v0, v5, v2, v3, v4}, Lcom/google/android/gms/herrevad/c/a;->a(Landroid/location/Location;Lcom/google/k/f/ai;JZ)V

    goto/16 :goto_6

    .line 171
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 200
    :sswitch_data_0
    .sparse-switch
        -0x7bb8dc04 -> :sswitch_0
        -0x2067cf93 -> :sswitch_3
        0x26d532c -> :sswitch_2
        0x178f835f -> :sswitch_4
        0x4baa6e10 -> :sswitch_5
        0x73b0dd12 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method private static a(Landroid/location/Location;Lcom/google/k/f/ai;JZ)V
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0x11
    .end annotation

    .prologue
    const-wide v4, 0x412e848000000000L    # 1000000.0

    .line 289
    if-eqz p4, :cond_5

    .line 290
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-lt v0, v1, :cond_4

    .line 291
    invoke-virtual {p0}, Landroid/location/Location;->getElapsedRealtimeNanos()J

    move-result-wide v0

    sub-long v0, p2, v0

    .line 302
    :goto_0
    long-to-double v0, v0

    const-wide v2, 0x41cdcd6500000000L    # 1.0E9

    div-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    .line 303
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p1, Lcom/google/k/f/ai;->g:I

    .line 304
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v1, v2

    iput v1, p1, Lcom/google/k/f/ai;->h:I

    .line 305
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    invoke-static {v1}, Ljava/lang/Math;->round(F)I

    move-result v1

    iput v1, p1, Lcom/google/k/f/ai;->i:I

    .line 306
    iput v0, p1, Lcom/google/k/f/ai;->j:I

    .line 307
    invoke-virtual {p0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 308
    invoke-virtual {p0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x4024000000000000L    # 10.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p1, Lcom/google/k/f/ai;->m:I

    .line 310
    :cond_0
    invoke-virtual {p0}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 311
    invoke-virtual {p0}, Landroid/location/Location;->getBearing()F

    move-result v0

    float-to-double v0, v0

    const-wide/high16 v2, 0x4059000000000000L    # 100.0

    mul-double/2addr v0, v2

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p1, Lcom/google/k/f/ai;->n:I

    .line 314
    :cond_1
    invoke-static {p0}, Lcom/google/android/location/n/z;->a(Landroid/location/Location;)Ljava/lang/String;

    move-result-object v0

    .line 315
    if-eqz v0, :cond_2

    iput-object v0, p1, Lcom/google/k/f/ai;->k:Ljava/lang/String;

    .line 316
    :cond_2
    invoke-static {p0}, Lcom/google/android/location/n/z;->b(Landroid/location/Location;)Ljava/lang/Integer;

    move-result-object v0

    .line 317
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p1, Lcom/google/k/f/ai;->l:I

    .line 318
    :cond_3
    :goto_1
    return-void

    .line 294
    :cond_4
    const-string v0, "Herrevad"

    const-string v1, "called appendLocationInfo with isElapsed=true on API < 17. Should call with isElapsed=false instead."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 299
    :cond_5
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v0

    const-wide/32 v2, 0xf4240

    mul-long/2addr v0, v2

    sub-long v0, p2, v0

    goto/16 :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/herrevad/c/a;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    const/4 v7, 0x6

    const/4 v2, 0x0

    .line 50
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-ne v0, v3, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Can\'t call reportNetworkQualityBlocking on main thread"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/SecurityException; {:try_start_1 .. :try_end_1} :catch_1

    invoke-static {p2}, Lcom/google/k/a/ah;->a(Ljava/lang/Object;)Ljava/lang/Object;

    sget-object v3, Lcom/google/android/gms/herrevad/b/a;->a:Lcom/google/android/gms/common/a/r;

    sget-object v4, Lcom/google/android/gms/herrevad/b/a;->b:Lcom/google/android/gms/common/a/r;

    sget-object v0, Lcom/google/android/gms/herrevad/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    invoke-virtual {v0, v7}, Ljava/util/Calendar;->get(I)I

    move-result v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/herrevad/d/c;->a(Lcom/google/android/gms/common/a/r;Lcom/google/android/gms/common/a/r;I)V

    if-ltz v5, :cond_1

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge v0, v5, :cond_3

    :cond_1
    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "Herrevad"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "over daily limit of "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v2, Lcom/google/android/gms/herrevad/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " per day, skipping this upload"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    :goto_1
    return-void

    :catch_0
    move-exception v0

    const-string v1, "Herrevad"

    const-string v2, "Could not verify the package name of the caller.  Dropping log event."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    :catch_1
    move-exception v0

    const-string v1, "Herrevad"

    const-string v2, "Calling package is not allowed to use."

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    throw v0

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/herrevad/c/a;->a(Ljava/lang/String;Landroid/os/Bundle;)Lcom/google/k/f/ai;

    move-result-object v3

    if-nez v3, :cond_5

    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_2

    const-string v0, "Herrevad"

    const-string v1, "could not create proto from information provided by client app.  Not uploading."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_5
    if-eqz p3, :cond_9

    iget-object v0, p0, Lcom/google/android/gms/herrevad/c/a;->a:Lcom/google/android/gms/mdm/services/NetworkQualityAndroidService;

    invoke-static {v0}, Lcom/google/android/gms/herrevad/d/d;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_9

    invoke-static {p3}, Lcom/google/android/gms/herrevad/c/a;->a(Landroid/os/Bundle;)[Ljava/lang/String;

    move-result-object v0

    :goto_2
    sget-boolean v4, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v4, :cond_6

    const-string v4, "Herrevad"

    const-string v5, "successfully built payload, queue event for upload"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    new-instance v4, Lcom/google/android/gms/playlog/a;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v5

    const/16 v6, 0xd

    invoke-direct {v4, v5, v6, v1, v2}, Lcom/google/android/gms/playlog/a;-><init>(Landroid/content/Context;ILjava/lang/String;Z)V

    if-eqz v0, :cond_8

    const-string v1, "network_quality"

    invoke-static {v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v2

    invoke-virtual {v4, v1, v2, v0}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    :goto_3
    sget-boolean v0, Lcom/google/android/gms/herrevad/d/a;->a:Z

    if-eqz v0, :cond_7

    const-string v0, "Herrevad"

    const-string v1, "NetworkQualityService - send to playlog"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    invoke-virtual {v4}, Lcom/google/android/gms/playlog/a;->a()V

    sget-object v1, Lcom/google/android/gms/herrevad/b/a;->a:Lcom/google/android/gms/common/a/r;

    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->b:Lcom/google/android/gms/common/a/r;

    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    invoke-virtual {v2, v7}, Ljava/util/Calendar;->get(I)I

    move-result v2

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/herrevad/d/c;->a(Lcom/google/android/gms/common/a/r;Lcom/google/android/gms/common/a/r;I)V

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/r;->a()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/a/r;->a(Ljava/lang/Object;)V

    goto/16 :goto_1

    :cond_8
    const-string v0, "network_quality"

    invoke-static {v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/gms/playlog/a;->a(Ljava/lang/String;[B[Ljava/lang/String;)V

    goto :goto_3

    :cond_9
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Landroid/os/Bundle;)[Ljava/lang/String;
    .locals 6

    .prologue
    .line 454
    if-nez p0, :cond_0

    .line 455
    const-string v0, "Herrevad"

    const-string v1, "null extraStats, returning null."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 456
    const/4 v0, 0x0

    .line 476
    :goto_0
    return-object v0

    .line 458
    :cond_0
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 459
    invoke-virtual {p0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 460
    if-nez v0, :cond_1

    .line 461
    const-string v0, "Herrevad"

    const-string v3, "null key, skipping"

    invoke-static {v0, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 464
    :cond_1
    invoke-virtual {p0, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    .line 465
    if-nez v3, :cond_2

    .line 466
    const-string v3, "Herrevad"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "null value for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v4, ", skipping."

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 470
    :cond_2
    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 473
    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 476
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/herrevad/c/b;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/herrevad/c/b;-><init>(Lcom/google/android/gms/herrevad/c/a;Ljava/lang/String;Landroid/os/Bundle;Landroid/os/Bundle;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/herrevad/c/b;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 77
    return-void
.end method
