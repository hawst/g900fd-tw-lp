.class public abstract Lcom/google/android/gms/games/ui/c/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;


# static fields
.field protected static final a:Ljava/util/ArrayList;

.field protected static final b:Lcom/google/android/gms/games/ui/c/e;

.field private static final p:Landroid/graphics/Rect;

.field private static final q:Landroid/graphics/Rect;


# instance fields
.field protected final c:Lcom/google/android/gms/games/a/au;

.field protected final d:J

.field protected final e:Lcom/google/android/gms/common/images/a;

.field protected final f:Landroid/widget/FrameLayout;

.field protected g:Landroid/view/animation/Animation;

.field protected final h:Landroid/view/animation/Animation;

.field protected final i:Landroid/view/animation/Animation;

.field protected final j:Landroid/view/animation/Animation;

.field protected k:Landroid/view/animation/Animation;

.field protected l:Landroid/view/View;

.field protected m:Landroid/view/View;

.field protected n:Landroid/view/View;

.field protected o:Z

.field private final r:Lcom/google/android/gms/games/internal/el;

.field private s:Z


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 64
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    .line 87
    new-instance v0, Lcom/google/android/gms/games/ui/c/e;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/ui/c/e;-><init>(Landroid/os/Looper;B)V

    sput-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    .line 138
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    .line 139
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/ui/c/b;->q:Landroid/graphics/Rect;

    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;JZ)V
    .locals 5

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 218
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/c/b;->s:Z

    .line 222
    iput-object p1, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    .line 223
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 224
    invoke-static {v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->e:Lcom/google/android/gms/common/images/a;

    .line 226
    new-instance v1, Landroid/widget/FrameLayout;

    invoke-direct {v1, v0}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    .line 228
    iput-object p2, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    .line 229
    iput-wide p3, p0, Lcom/google/android/gms/games/ui/c/b;->d:J

    .line 230
    iput-boolean p5, p0, Lcom/google/android/gms/games/ui/c/b;->o:Z

    .line 234
    sget v1, Lcom/google/android/gms/b;->p:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    .line 235
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 236
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 238
    sget v1, Lcom/google/android/gms/b;->j:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->h:Landroid/view/animation/Animation;

    .line 239
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->h:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 241
    sget v1, Lcom/google/android/gms/b;->n:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->i:Landroid/view/animation/Animation;

    .line 243
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v1, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 245
    new-instance v1, Landroid/view/animation/AlphaAnimation;

    const/4 v2, 0x0

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-direct {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->j:Landroid/view/animation/Animation;

    .line 246
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->j:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->getDuration()J

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->j:Landroid/view/animation/Animation;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/b;->i:Landroid/view/animation/Animation;

    invoke-virtual {v2}, Landroid/view/animation/Animation;->getInterpolator()Landroid/view/animation/Interpolator;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 251
    sget v1, Lcom/google/android/gms/b;->k:I

    invoke-static {v0, v1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->k:Landroid/view/animation/Animation;

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->k:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 253
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/c/b;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget-object v0, v0, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/games/ui/c/f;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/ui/c/f;-><init>(B)V

    const/4 v1, 0x1

    new-array v1, v1, [Lcom/google/android/gms/games/ui/c/b;

    aput-object p0, v1, v2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/f;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    :cond_0
    return-void
.end method

.method private b(I)V
    .locals 4

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/au;->e()Ljava/lang/String;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    .line 554
    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, v2, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    .line 555
    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v3, v3, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3, v1, v0, v2, p1}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 556
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/c/b;)V
    .locals 3

    .prologue
    .line 51
    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    sget-object v2, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;->k()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/c/b;)V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;->l()V

    return-void
.end method

.method public static i()V
    .locals 5

    .prologue
    .line 333
    const/16 v0, 0xc

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 335
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    .line 337
    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/b;->m()V

    .line 340
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    const/4 v4, 0x2

    invoke-virtual {v3, v4, v0}, Lcom/google/android/gms/games/ui/c/e;->removeMessages(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 344
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 343
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 344
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 346
    :cond_1
    return-void
.end method

.method private k()V
    .locals 8
    .annotation build Landroid/annotation/TargetApi;
        value = 0xc
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v1, v1, Lcom/google/android/gms/games/internal/el;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/c/a/a;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    .line 361
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Landroid/view/WindowManager;

    .line 363
    new-instance v7, Landroid/view/WindowManager$LayoutParams;

    invoke-direct {v7}, Landroid/view/WindowManager$LayoutParams;-><init>()V

    .line 364
    const/16 v0, 0x8

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 365
    const/4 v0, -0x3

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->format:I

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0, v2, v2}, Landroid/widget/FrameLayout;->measure(II)V

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v1

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v2

    .line 372
    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->width:I

    .line 373
    iput v2, v7, Landroid/view/WindowManager$LayoutParams;->height:I

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v0, v0, Lcom/google/android/gms/games/internal/el;->b:I

    .line 377
    if-nez v0, :cond_0

    .line 378
    const/16 v0, 0x31

    .line 381
    :cond_0
    const/16 v3, 0xc

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 382
    const/16 v3, 0x3e8

    iput v3, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 383
    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget-object v3, v3, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    iput-object v3, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 387
    const/16 v3, 0x33

    iput v3, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    .line 390
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v4, v4, Lcom/google/android/gms/games/internal/el;->d:I

    iput v4, v3, Landroid/graphics/Rect;->left:I

    .line 391
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v4, v4, Lcom/google/android/gms/games/internal/el;->e:I

    iput v4, v3, Landroid/graphics/Rect;->top:I

    .line 392
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v4, v4, Lcom/google/android/gms/games/internal/el;->g:I

    iput v4, v3, Landroid/graphics/Rect;->bottom:I

    .line 393
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v4, v4, Lcom/google/android/gms/games/internal/el;->f:I

    iput v4, v3, Landroid/graphics/Rect;->right:I

    .line 396
    const/16 v3, 0x11

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-virtual {v3}, Landroid/widget/FrameLayout;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/gms/games/ui/c/b;->q:Landroid/graphics/Rect;

    invoke-virtual {v5}, Landroid/content/res/Configuration;->getLayoutDirection()I

    move-result v5

    invoke-static/range {v0 .. v5}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;I)V

    .line 397
    :goto_0
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->x:I

    .line 398
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->q:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->top:I

    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->y:I

    .line 407
    :goto_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-interface {v6, v0, v7}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V
    :try_end_0
    .catch Landroid/view/WindowManager$BadTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 417
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/c/b;->s:Z

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 419
    :goto_2
    return-void

    .line 396
    :cond_1
    sget-object v3, Lcom/google/android/gms/games/ui/c/b;->p:Landroid/graphics/Rect;

    sget-object v4, Lcom/google/android/gms/games/ui/c/b;->q:Landroid/graphics/Rect;

    invoke-static {v0, v1, v2, v3, v4}, Landroid/view/Gravity;->apply(IIILandroid/graphics/Rect;Landroid/graphics/Rect;)V

    goto :goto_0

    .line 400
    :cond_2
    const/16 v1, 0x7d5

    iput v1, v7, Landroid/view/WindowManager$LayoutParams;->type:I

    .line 401
    new-instance v1, Landroid/os/Binder;

    invoke-direct {v1}, Landroid/os/Binder;-><init>()V

    iput-object v1, v7, Landroid/view/WindowManager$LayoutParams;->token:Landroid/os/IBinder;

    .line 403
    iput v0, v7, Landroid/view/WindowManager$LayoutParams;->gravity:I

    goto :goto_1

    .line 409
    :catch_0
    move-exception v0

    const-string v0, "BasePopup"

    const-string v1, "Cannot show the popup as the given window token is not valid. Either the given view is not attached to a window or you tried to connect the GoogleApiClient in the same lifecycle step as the creation of the GoogleApiClient. See GoogleApiClient.Builder.create() and  GoogleApiClient.connect() for more information."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 414
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;->l()V

    goto :goto_2
.end method

.method private l()V
    .locals 3

    .prologue
    .line 433
    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 434
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/c/b;->m()V

    .line 436
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 437
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 438
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/c/b;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/c/b;->k()V

    .line 440
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private m()V
    .locals 2

    .prologue
    .line 478
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/c/b;->s:Z

    if-eqz v0, :cond_0

    .line 480
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->r:Lcom/google/android/gms/games/internal/el;

    iget v1, v1, Lcom/google/android/gms/games/internal/el;->c:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/c/a/a;->a(Landroid/content/Context;I)Landroid/content/Context;

    move-result-object v0

    .line 483
    const-string v1, "window"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    .line 485
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/c/b;->s:Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 493
    :cond_0
    :goto_0
    return-void

    .line 491
    :catch_0
    move-exception v0

    const-string v0, "BasePopup"

    const-string v1, "Popup is not attached to a window, so not attempting to remove it."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Landroid/net/Uri;)Landroid/graphics/Bitmap;
    .locals 4

    .prologue
    .line 446
    const/4 v0, 0x0

    .line 447
    if-eqz p1, :cond_0

    .line 448
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->e:Lcom/google/android/gms/common/images/a;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, v2, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v1

    .line 449
    if-eqz v1, :cond_0

    .line 451
    :try_start_0
    invoke-virtual {v1}, Landroid/content/res/AssetFileDescriptor;->createInputStream()Ljava/io/FileInputStream;

    move-result-object v1

    invoke-static {v1}, Landroid/graphics/BitmapFactory;->decodeStream(Ljava/io/InputStream;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 452
    invoke-static {v0}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 459
    :cond_0
    :goto_0
    return-object v0

    .line 454
    :catch_0
    move-exception v1

    const-string v1, "BasePopup"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unable to parse image content for icon URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(I)Landroid/graphics/drawable/Drawable;
    .locals 2

    .prologue
    .line 463
    if-eqz p1, :cond_0

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 465
    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/common/images/internal/f;->a(Landroid/content/res/Resources;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 468
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected abstract a()V
.end method

.method protected b()V
    .locals 2

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->m:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->h:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 271
    return-void
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->l:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/b;->k:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 278
    return-void
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 545
    return-void
.end method

.method protected abstract e()I
.end method

.method protected abstract f()I
.end method

.method protected final g()V
    .locals 4

    .prologue
    .line 281
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 283
    sget v1, Lcom/google/android/gms/l;->aU:I

    iget-object v2, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    const/4 v3, 0x1

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/gms/j;->ko:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->l:Landroid/view/View;

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/gms/j;->kp:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->m:Landroid/view/View;

    .line 287
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->f:Landroid/widget/FrameLayout;

    sget v1, Lcom/google/android/gms/j;->kn:I

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->n:Landroid/view/View;

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->n:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->n:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 293
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;)V

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/b;->e()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/c/b;->b(I)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/b;->a()V

    .line 297
    return-void
.end method

.method protected final h()Z
    .locals 1

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->l:Landroid/view/View;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final j()Landroid/content/Context;
    .locals 1

    .prologue
    .line 349
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    return-object v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4

    .prologue
    const/4 v2, 0x2

    .line 500
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/c/b;->o:Z

    if-eqz v0, :cond_3

    .line 501
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 502
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    const-wide/16 v2, 0x5dc

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/c/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 529
    :cond_0
    :goto_0
    return-void

    .line 504
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->h:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_2

    .line 505
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/c/b;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/c/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 507
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->m:Landroid/view/View;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 508
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->k:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 509
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    new-instance v1, Lcom/google/android/gms/games/ui/c/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/c/c;-><init>(Lcom/google/android/gms/games/ui/c/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/e;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 517
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->g:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_4

    .line 518
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v1, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    invoke-virtual {v1, v2, p0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    iget-wide v2, p0, Lcom/google/android/gms/games/ui/c/b;->d:J

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/c/e;->sendMessageDelayed(Landroid/os/Message;J)Z

    goto :goto_0

    .line 520
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/b;->k:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 521
    sget-object v0, Lcom/google/android/gms/games/ui/c/b;->b:Lcom/google/android/gms/games/ui/c/e;

    new-instance v1, Lcom/google/android/gms/games/ui/c/d;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/c/d;-><init>(Lcom/google/android/gms/games/ui/c/b;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/c/e;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 532
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 496
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1

    .prologue
    .line 537
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/b;->f()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/c/b;->b(I)V

    .line 538
    sget-object v0, Lcom/google/android/gms/games/c/a;->V:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 539
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/b;->d()V

    .line 541
    :cond_0
    return-void
.end method
