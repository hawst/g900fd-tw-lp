.class public final Lcom/google/android/gms/games/ui/client/snapshots/a;
.super Lcom/google/android/gms/games/ui/b/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 236
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3

    .prologue
    .line 241
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->ic:I

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v0

    .line 243
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 244
    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 245
    sget v0, Lcom/google/android/gms/p;->ib:I

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(I)Landroid/app/AlertDialog$Builder;

    .line 246
    const v0, 0x104000a

    invoke-virtual {v1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 247
    const/high16 v0, 0x1040000

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 248
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/snapshots/a;->a(Z)V

    .line 249
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 254
    packed-switch p2, :pswitch_data_0

    .line 277
    const-string v0, "SnapshotActivity"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    :goto_0
    :pswitch_0
    return-void

    .line 256
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/a;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.SNAPSHOT_METADATA"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    .line 258
    if-eqz v0, :cond_1

    .line 259
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/snapshots/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;

    .line 261
    if-eqz v1, :cond_0

    .line 262
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/snapshots/ClientSnapshotListActivity;->c(Lcom/google/android/gms/games/snapshot/SnapshotMetadata;)V

    goto :goto_0

    .line 265
    :cond_0
    const-string v0, "SnapshotActivity"

    const-string v1, "delete button: no parent; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 268
    :cond_1
    const-string v0, "SnapshotActivity"

    const-string v1, "ConfirmDeleteDialogFragment.onClick: snapshot is null!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 254
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
