.class public final Lcom/google/android/gms/icing/service/p;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final c:Ljava/util/Map;


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/icing/impl/u;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/Context;

.field private final f:Lcom/google/android/gms/icing/b/a;

.field private final g:Lcom/google/android/gms/icing/impl/aw;

.field private h:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/service/p;->c:Ljava/util/Map;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/aw;)V
    .locals 1

    .prologue
    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 35
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/service/p;->a:Ljava/lang/Object;

    .line 140
    iput-object p1, p0, Lcom/google/android/gms/icing/service/p;->d:Ljava/lang/String;

    .line 141
    iput-object p2, p0, Lcom/google/android/gms/icing/service/p;->e:Landroid/content/Context;

    .line 142
    iput-object p3, p0, Lcom/google/android/gms/icing/service/p;->f:Lcom/google/android/gms/icing/b/a;

    .line 143
    iput-object p4, p0, Lcom/google/android/gms/icing/service/p;->g:Lcom/google/android/gms/icing/impl/aw;

    .line 144
    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/app/Service;)Lcom/google/android/gms/icing/service/p;
    .locals 5

    .prologue
    .line 52
    sget-object v1, Lcom/google/android/gms/icing/service/p;->c:Ljava/util/Map;

    monitor-enter v1

    .line 53
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/service/p;->c:Ljava/util/Map;

    invoke-interface {v0, p0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/service/p;

    .line 54
    if-nez v0, :cond_0

    .line 55
    invoke-virtual {p1}, Landroid/app/Service;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    .line 56
    new-instance v0, Landroid/content/Intent;

    const-class v3, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v3, "com.google.android.gms.icing.START_STICKY"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 58
    new-instance v3, Lcom/google/android/gms/icing/service/o;

    invoke-direct {v3, v2, p0, v0}, Lcom/google/android/gms/icing/service/o;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/content/Intent;)V

    .line 59
    new-instance v0, Lcom/google/android/gms/icing/service/p;

    new-instance v4, Lcom/google/android/gms/icing/impl/aw;

    invoke-direct {v4}, Lcom/google/android/gms/icing/impl/aw;-><init>()V

    invoke-direct {v0, p0, v2, v3, v4}, Lcom/google/android/gms/icing/service/p;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/aw;)V

    .line 61
    sget-object v2, Lcom/google/android/gms/icing/service/p;->c:Ljava/util/Map;

    invoke-interface {v2, p0, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 63
    :cond_0
    invoke-static {}, Lcom/google/android/gms/icing/service/p;->e()V

    iget v2, v0, Lcom/google/android/gms/icing/service/p;->h:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v0, Lcom/google/android/gms/icing/service/p;->h:I

    const-string v2, "onCreate count=%d"

    iget v3, v0, Lcom/google/android/gms/icing/service/p;->h:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    invoke-static {}, Lcom/google/android/gms/icing/c;->a()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->b(I)V

    .line 64
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static e()V
    .locals 2

    .prologue
    .line 69
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v1

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 70
    return-void

    .line 69
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/gms/icing/service/p;->e()V

    .line 115
    iget v0, p0, Lcom/google/android/gms/icing/service/p;->h:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/service/p;->h:I

    .line 116
    iget v0, p0, Lcom/google/android/gms/icing/service/p;->h:I

    if-ltz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    const-string v1, "More calls to onDestroy than onCreate"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 117
    const-string v0, "onDestroy count=%d"

    iget v1, p0, Lcom/google/android/gms/icing/service/p;->h:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 118
    iget v0, p0, Lcom/google/android/gms/icing/service/p;->h:I

    if-nez v0, :cond_1

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/icing/service/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 120
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/p;->b:Lcom/google/android/gms/icing/impl/u;

    if-eqz v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/icing/service/p;->b:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/m;->a()V

    .line 127
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_1
    return-void

    .line 116
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 127
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Lcom/google/android/gms/icing/b/a;
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/icing/service/p;->f:Lcom/google/android/gms/icing/b/a;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/icing/impl/u;
    .locals 6

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/android/gms/icing/service/p;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 160
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/p;->b:Lcom/google/android/gms/icing/impl/u;

    .line 161
    if-nez v0, :cond_0

    .line 163
    new-instance v0, Lcom/google/android/gms/icing/impl/u;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/p;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/icing/service/p;->f:Lcom/google/android/gms/icing/b/a;

    iget-object v4, p0, Lcom/google/android/gms/icing/service/p;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/icing/service/p;->g:Lcom/google/android/gms/icing/impl/aw;

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/gms/icing/impl/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/b/a;Ljava/lang/String;Lcom/google/android/gms/icing/impl/aw;)V

    .line 165
    const-string v2, "%s: Starting asynchronous initialization"

    iget-object v3, p0, Lcom/google/android/gms/icing/service/p;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 166
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/u;->a(Z)Lcom/google/android/gms/icing/b/j;

    .line 167
    iput-object v0, p0, Lcom/google/android/gms/icing/service/p;->b:Lcom/google/android/gms/icing/impl/u;

    .line 174
    new-instance v2, Lcom/google/android/gms/icing/service/q;

    invoke-direct {v2, p0, v0}, Lcom/google/android/gms/icing/service/q;-><init>(Lcom/google/android/gms/icing/service/p;Lcom/google/android/gms/icing/impl/u;)V

    .line 194
    new-instance v3, Ljava/lang/Thread;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "index-service-init-watch-"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/icing/service/p;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v2, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    invoke-virtual {v3}, Ljava/lang/Thread;->start()V

    .line 198
    :goto_0
    monitor-exit v1

    .line 199
    return-object v0

    .line 196
    :cond_0
    const-string v2, "%s: Re-using cached"

    iget-object v3, p0, Lcom/google/android/gms/icing/service/p;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 4

    .prologue
    .line 206
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 207
    iget-object v0, p0, Lcom/google/android/gms/icing/service/p;->e:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/p;->e:Landroid/content/Context;

    const-class v3, Lcom/google/android/gms/icing/proxy/ProxySmsProviderService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 209
    :cond_0
    return-void
.end method
