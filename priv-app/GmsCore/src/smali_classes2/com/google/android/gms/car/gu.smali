.class final Lcom/google/android/gms/car/gu;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarService;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/CarService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 302
    iput-object p1, p0, Lcom/google/android/gms/car/gu;->a:Lcom/google/android/gms/car/CarService;

    .line 303
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 304
    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/16 v2, 0x10

    const/4 v1, 0x0

    .line 313
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v3, 0x13

    if-ge v0, v3, :cond_0

    move-object v0, v1

    move v1, v2

    .line 340
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/gu;->b:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    invoke-static {v2, v3, p3}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;ILjava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 342
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Invalid calling package"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 317
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/gu;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {p4}, Lcom/google/android/gms/car/CarService;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/car/gu;->b:Landroid/content/Context;

    sget-object v3, Lcom/google/android/gms/car/nx;->d:Lcom/google/android/gms/car/nx;

    iget-object v4, p0, Lcom/google/android/gms/car/gu;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v4}, Lcom/google/android/gms/car/CarService;->b(Lcom/google/android/gms/car/CarService;)Lcom/google/android/gms/car/no;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v4

    invoke-static {v0, p3, v3, v4}, Lcom/google/android/gms/car/gc;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/car/nx;Landroid/os/Looper;)Z

    move-result v3

    .line 322
    const-string v0, "CAR.SERVICE"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    const-string v4, "CAR.SERVICE"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v0, "Package "

    invoke-direct {v5, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v3, :cond_2

    const-string v0, ""

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, "in whitelist; name: "

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 327
    :cond_1
    if-eqz v3, :cond_3

    .line 328
    const/4 v1, 0x0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/car/gu;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {v0}, Lcom/google/android/gms/car/CarService;->a(Lcom/google/android/gms/car/CarService;)Lcom/google/android/gms/car/gx;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    goto :goto_0

    .line 323
    :cond_2
    const-string v0, "NOT "

    goto :goto_1

    :cond_3
    move-object v0, v1

    move v1, v2

    .line 334
    goto :goto_0

    .line 336
    :cond_4
    const/4 v0, 0x2

    move-object v6, v1

    move v1, v0

    move-object v0, v6

    .line 337
    goto :goto_0

    .line 344
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/car/gu;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/car/CarService;->a(Lcom/google/android/gms/common/internal/bg;ILandroid/os/IBinder;)V

    .line 345
    return-void
.end method
