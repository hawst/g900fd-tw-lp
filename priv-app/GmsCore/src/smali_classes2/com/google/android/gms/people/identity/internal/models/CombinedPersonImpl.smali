.class public Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/people/identity/models/Person;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;


# instance fields
.field A:Ljava/util/List;

.field B:Ljava/lang/String;

.field C:Ljava/util/List;

.field D:Ljava/util/List;

.field E:Ljava/util/List;

.field F:Ljava/util/List;

.field G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

.field H:Ljava/util/List;

.field I:Ljava/util/List;

.field final a:Ljava/util/Set;

.field final b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field e:Ljava/lang/String;

.field f:Ljava/util/List;

.field g:Ljava/util/List;

.field h:Ljava/util/List;

.field i:Ljava/util/List;

.field j:Ljava/util/List;

.field k:Ljava/lang/String;

.field l:Ljava/util/List;

.field m:Ljava/util/List;

.field n:Ljava/lang/String;

.field o:Ljava/util/List;

.field p:Ljava/util/List;

.field q:Ljava/lang/String;

.field r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

.field s:Ljava/util/List;

.field t:Ljava/util/List;

.field u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

.field v:Ljava/util/List;

.field w:Ljava/util/List;

.field x:Ljava/util/List;

.field y:Ljava/util/List;

.field z:Ljava/util/List;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/people/identity/internal/models/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/models/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 243
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->b:I

    .line 245
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    .line 246
    return-void
.end method

.method constructor <init>(Ljava/util/Set;ILjava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;Ljava/util/List;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 285
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 286
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    .line 287
    iput p2, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->b:I

    .line 288
    iput-object p3, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->c:Ljava/util/List;

    .line 289
    iput-object p4, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    .line 290
    iput-object p5, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->e:Ljava/lang/String;

    .line 291
    iput-object p6, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->f:Ljava/util/List;

    .line 292
    iput-object p7, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->g:Ljava/util/List;

    .line 293
    iput-object p8, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->h:Ljava/util/List;

    .line 294
    iput-object p9, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->i:Ljava/util/List;

    .line 295
    iput-object p10, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    .line 296
    iput-object p11, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->k:Ljava/lang/String;

    .line 297
    iput-object p12, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    .line 298
    iput-object p13, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->m:Ljava/util/List;

    .line 299
    iput-object p14, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->n:Ljava/lang/String;

    .line 300
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    .line 301
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    .line 302
    move-object/from16 v0, p17

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->q:Ljava/lang/String;

    .line 303
    move-object/from16 v0, p18

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    .line 304
    move-object/from16 v0, p19

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->s:Ljava/util/List;

    .line 305
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    .line 306
    move-object/from16 v0, p21

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    .line 307
    move-object/from16 v0, p22

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    .line 308
    move-object/from16 v0, p23

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    .line 309
    move-object/from16 v0, p24

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->x:Ljava/util/List;

    .line 310
    move-object/from16 v0, p25

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    .line 311
    move-object/from16 v0, p26

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    .line 312
    move-object/from16 v0, p27

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->A:Ljava/util/List;

    .line 313
    move-object/from16 v0, p28

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->B:Ljava/lang/String;

    .line 314
    move-object/from16 v0, p29

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    .line 315
    move-object/from16 v0, p30

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->D:Ljava/util/List;

    .line 316
    move-object/from16 v0, p31

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->E:Ljava/util/List;

    .line 317
    move-object/from16 v0, p32

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->F:Ljava/util/List;

    .line 318
    move-object/from16 v0, p33

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    .line 319
    move-object/from16 v0, p34

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    .line 320
    move-object/from16 v0, p35

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    .line 321
    return-void
.end method


# virtual methods
.method public final A()Ljava/util/List;
    .locals 1

    .prologue
    .line 841
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    return-object v0
.end method

.method public final B()Z
    .locals 2

    .prologue
    .line 849
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final C()Ljava/util/List;
    .locals 1

    .prologue
    .line 858
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->A:Ljava/util/List;

    return-object v0
.end method

.method public final D()Z
    .locals 2

    .prologue
    .line 866
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final E()Ljava/util/List;
    .locals 1

    .prologue
    .line 892
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    return-object v0
.end method

.method public final F()Z
    .locals 2

    .prologue
    .line 900
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final G()Ljava/util/List;
    .locals 1

    .prologue
    .line 977
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    return-object v0
.end method

.method public final H()Z
    .locals 2

    .prologue
    .line 985
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final I()Ljava/util/List;
    .locals 1

    .prologue
    .line 994
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    return-object v0
.end method

.method public final J()Z
    .locals 2

    .prologue
    .line 1002
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Addresses;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1043
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1044
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    .line 1046
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1047
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1048
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Emails;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1175
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1176
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    .line 1178
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1179
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1180
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Events;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1209
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1210
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    .line 1212
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1213
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1214
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Images;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1267
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1268
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    .line 1270
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1271
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1272
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$InstantMessaging;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1290
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1291
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    .line 1293
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1294
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1295
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Memberships;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1360
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1361
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    .line 1363
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1364
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1365
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1373
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    .line 1374
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1375
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Names;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1394
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1395
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    .line 1397
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1398
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1399
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Nicknames;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1417
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1418
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    .line 1420
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1421
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1422
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Organizations;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1463
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1464
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    .line 1466
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1467
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1468
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$PhoneNumbers;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1486
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1487
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    .line 1489
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1490
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1491
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Relations;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1543
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1544
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    .line 1546
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1547
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1548
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Taglines;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1646
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1647
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    .line 1649
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1650
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1651
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Urls;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1669
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    if-nez v0, :cond_0

    .line 1670
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    .line 1672
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1673
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1674
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 399
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->b()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->c:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 400
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->d()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 401
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->g()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->f()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->e:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 402
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->k()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->h()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->f:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x5

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 403
    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->m()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->l()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->g:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x6

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 404
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->o()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->n()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->h:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 405
    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->q()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->p()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->i:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 406
    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->s()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->r()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 407
    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->u()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->t()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->k:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 408
    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->w()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->v()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 409
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->y()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->x()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->m:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xc

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 410
    :cond_a
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->A()Z

    move-result v0

    if-eqz v0, :cond_b

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    .line 411
    :cond_b
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->C()Z

    move-result v0

    if-eqz v0, :cond_c

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->B()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 412
    :cond_c
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->E()Z

    move-result v0

    if-eqz v0, :cond_d

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->D()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 413
    :cond_d
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->G()Z

    move-result v0

    if-eqz v0, :cond_e

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->F()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->q:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x11

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 414
    :cond_e
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->I()Z

    move-result v0

    if-eqz v0, :cond_f

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->H()Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->r:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$LegacyFields;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x12

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 415
    :cond_f
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->K()Z

    move-result v0

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->J()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 416
    :cond_10
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->M()Z

    move-result v0

    if-eqz v0, :cond_11

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->L()Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a(Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;

    .line 417
    :cond_11
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->O()Z

    move-result v0

    if-eqz v0, :cond_12

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->N()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 418
    :cond_12
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->Q()Z

    move-result v0

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->P()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 419
    :cond_13
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->S()Z

    move-result v0

    if-eqz v0, :cond_14

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->R()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->x:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x18

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 420
    :cond_14
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->U()Z

    move-result v0

    if-eqz v0, :cond_15

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->T()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 421
    :cond_15
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->W()Z

    move-result v0

    if-eqz v0, :cond_16

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->V()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->z:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1a

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 422
    :cond_16
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->Y()Z

    move-result v0

    if-eqz v0, :cond_17

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->X()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->A:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1b

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 423
    :cond_17
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->aa()Z

    move-result v0

    if-eqz v0, :cond_18

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->Z()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->B:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1c

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 424
    :cond_18
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ac()Z

    move-result v0

    if-eqz v0, :cond_19

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ab()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->C:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1d

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 425
    :cond_19
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ae()Z

    move-result v0

    if-eqz v0, :cond_1a

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ad()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->D:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 426
    :cond_1a
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ag()Z

    move-result v0

    if-eqz v0, :cond_1b

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->af()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->E:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x1f

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 427
    :cond_1b
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ai()Z

    move-result v0

    if-eqz v0, :cond_1c

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ah()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->F:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x20

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 428
    :cond_1c
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ak()Z

    move-result v0

    if-eqz v0, :cond_1d

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->aj()Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->G:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$SortKeys;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x21

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 429
    :cond_1d
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->am()Z

    move-result v0

    if-eqz v0, :cond_1e

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->al()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->H:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x22

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 430
    :cond_1e
    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->ao()Z

    move-result v0

    if-eqz v0, :cond_1f

    invoke-virtual {p1}, Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl;->an()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->I:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x23

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 432
    :cond_1f
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;
    .locals 2

    .prologue
    .line 1246
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->n:Ljava/lang/String;

    .line 1247
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1248
    return-object p0
.end method

.method public final a()Ljava/util/List;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->c:Ljava/util/List;

    return-object v0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/util/List;
    .locals 1

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->d:Ljava/util/List;

    return-object v0
.end method

.method public final d()Z
    .locals 2

    .prologue
    .line 469
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 1679
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/util/List;
    .locals 1

    .prologue
    .line 534
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->h:Ljava/util/List;

    return-object v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/4 v1, 0x7

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 568
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->j:Ljava/util/List;

    return-object v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final i()Ljava/util/List;
    .locals 1

    .prologue
    .line 602
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->l:Ljava/util/List;

    return-object v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 610
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final k()Ljava/lang/String;
    .locals 1

    .prologue
    .line 637
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->n:Ljava/lang/String;

    return-object v0
.end method

.method public final l()Z
    .locals 2

    .prologue
    .line 645
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xd

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final m()Ljava/util/List;
    .locals 1

    .prologue
    .line 654
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->o:Ljava/util/List;

    return-object v0
.end method

.method public final n()Z
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final o()Ljava/util/List;
    .locals 1

    .prologue
    .line 671
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->p:Ljava/util/List;

    return-object v0
.end method

.method public final p()Z
    .locals 2

    .prologue
    .line 679
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0xf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final q()Ljava/util/List;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->t:Ljava/util/List;

    return-object v0
.end method

.method public final r()Z
    .locals 2

    .prologue
    .line 747
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x14

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final s()Lcom/google/android/gms/people/identity/models/r;
    .locals 1

    .prologue
    .line 756
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->u:Lcom/google/android/gms/people/identity/internal/models/DefaultPersonImpl$Metadata;

    return-object v0
.end method

.method public final t()Z
    .locals 2

    .prologue
    .line 764
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x15

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final u()Ljava/util/List;
    .locals 1

    .prologue
    .line 773
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->v:Ljava/util/List;

    return-object v0
.end method

.method public final v()Z
    .locals 2

    .prologue
    .line 781
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x16

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final w()Ljava/util/List;
    .locals 1

    .prologue
    .line 790
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->w:Ljava/util/List;

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 1684
    sget-object v0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->CREATOR:Lcom/google/android/gms/people/identity/internal/models/a;

    invoke-static {p0, p1, p2}, Lcom/google/android/gms/people/identity/internal/models/a;->a(Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;Landroid/os/Parcel;I)V

    .line 1685
    return-void
.end method

.method public final x()Z
    .locals 2

    .prologue
    .line 798
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x17

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final y()Ljava/util/List;
    .locals 1

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->y:Ljava/util/List;

    return-object v0
.end method

.method public final z()Z
    .locals 2

    .prologue
    .line 832
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/models/CombinedPersonImpl;->a:Ljava/util/Set;

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
