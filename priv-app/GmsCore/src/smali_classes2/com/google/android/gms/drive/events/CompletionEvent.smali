.class public final Lcom/google/android/gms/drive/events/CompletionEvent;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/drive/events/ResourceEvent;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:Ljava/lang/String;

.field final d:Landroid/os/ParcelFileDescriptor;

.field final e:Landroid/os/ParcelFileDescriptor;

.field final f:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

.field final g:Ljava/util/List;

.field final h:I

.field final i:Landroid/os/IBinder;

.field private j:Z

.field private k:Z

.field private l:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 153
    new-instance v0, Lcom/google/android/gms/drive/events/e;

    invoke-direct {v0}, Lcom/google/android/gms/drive/events/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/events/CompletionEvent;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Ljava/util/List;ILandroid/os/IBinder;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 206
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 187
    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->j:Z

    .line 188
    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->k:Z

    .line 189
    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->l:Z

    .line 207
    iput p1, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->a:I

    .line 208
    iput-object p2, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->b:Lcom/google/android/gms/drive/DriveId;

    .line 209
    iput-object p3, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->c:Ljava/lang/String;

    .line 210
    iput-object p4, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->d:Landroid/os/ParcelFileDescriptor;

    .line 211
    iput-object p5, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->e:Landroid/os/ParcelFileDescriptor;

    .line 212
    iput-object p6, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->f:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    .line 213
    iput-object p7, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->g:Ljava/util/List;

    .line 214
    iput p8, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->h:I

    .line 215
    iput-object p9, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->i:Landroid/os/IBinder;

    .line 216
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Landroid/content/Context;Ljava/util/List;ILandroid/os/IBinder;)V
    .locals 10

    .prologue
    .line 231
    const/4 v1, 0x1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p7

    move/from16 v8, p8

    move-object/from16 v9, p9

    invoke-direct/range {v0 .. v9}, Lcom/google/android/gms/drive/events/CompletionEvent;-><init>(ILcom/google/android/gms/drive/DriveId;Ljava/lang/String;Landroid/os/ParcelFileDescriptor;Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;Ljava/util/List;ILandroid/os/IBinder;)V

    .line 234
    if-eqz p5, :cond_0

    .line 235
    invoke-virtual/range {p5 .. p6}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Landroid/content/Context;)V

    .line 237
    :cond_0
    return-void
.end method

.method private h()V
    .locals 2

    .prologue
    .line 418
    iget-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->l:Z

    if-eqz v0, :cond_0

    .line 419
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Event has already been dismissed or snoozed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 421
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x2

    return v0
.end method

.method public final b()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 257
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 269
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 270
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/io/InputStream;
    .locals 2

    .prologue
    .line 319
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->e:Landroid/os/ParcelFileDescriptor;

    if-nez v0, :cond_0

    .line 321
    const/4 v0, 0x0

    .line 328
    :goto_0
    return-object v0

    .line 323
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->k:Z

    if-eqz v0, :cond_1

    .line 324
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getModifiedInputStream() can only be called once per CompletionEvent instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 327
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->k:Z

    .line 328
    new-instance v0, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->e:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x0

    return v0
.end method

.method public final dismiss()V
    .locals 4

    .prologue
    .line 383
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->l:Z

    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->e:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Landroid/os/ParcelFileDescriptor;)V

    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->i:Landroid/os/IBinder;

    if-nez v0, :cond_0

    const-string v0, "CompletionEvent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No callback on dismiss"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    :goto_0
    return-void

    .line 383
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->i:Landroid/os/IBinder;

    invoke-static {v0}, Lcom/google/android/gms/drive/internal/ch;->a(Landroid/os/IBinder;)Lcom/google/android/gms/drive/internal/cg;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/drive/internal/cg;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v1, "CompletionEvent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "RemoteException on dismiss: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/drive/internal/be;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final e()Lcom/google/android/gms/drive/am;
    .locals 2

    .prologue
    .line 346
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 347
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->f:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/am;

    iget-object v1, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->f:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/am;-><init>(Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f()Ljava/util/List;
    .locals 2

    .prologue
    .line 363
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 364
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->g:Ljava/util/List;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 373
    invoke-direct {p0}, Lcom/google/android/gms/drive/events/CompletionEvent;->h()V

    .line 374
    iget v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->h:I

    return v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 6

    .prologue
    .line 426
    iget-object v0, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->g:Ljava/util/List;

    if-nez v0, :cond_0

    const-string v0, "<null>"

    .line 428
    :goto_0
    sget-object v1, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v2, "CompletionEvent [id=%s, status=%s, trackingTag=%s]"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->b:Lcom/google/android/gms/drive/DriveId;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget v5, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->h:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    aput-object v0, v3, v4

    invoke-static {v1, v2, v3}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 426
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "\',\'"

    iget-object v2, p0, Lcom/google/android/gms/drive/events/CompletionEvent;->g:Ljava/util/List;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;Ljava/lang/Iterable;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 246
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/events/e;->a(Lcom/google/android/gms/drive/events/CompletionEvent;Landroid/os/Parcel;I)V

    .line 247
    return-void
.end method
