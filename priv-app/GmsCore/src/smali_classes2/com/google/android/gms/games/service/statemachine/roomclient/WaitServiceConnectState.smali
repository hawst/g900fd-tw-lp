.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 32
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 43
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->g:Z

    :goto_0
    return v0

    .line 34
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bu;

    .line 35
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bu;->b:Landroid/os/IBinder;

    invoke-static {v0}, Lcom/google/android/gms/games/internal/ee;->a(Landroid/os/IBinder;)Lcom/google/android/gms/games/internal/ed;

    move-result-object v0

    iput-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->h:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceIpcState;->b()V

    .line 37
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->f:Z

    goto :goto_0

    .line 40
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 41
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->f:Z

    goto :goto_0

    .line 32
    :sswitch_data_0
    .sparse-switch
        0xc -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceConnectState;->c()V

    .line 28
    return-void
.end method
