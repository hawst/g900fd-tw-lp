.class public final Lcom/google/android/gms/icing/impl/a/ab;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Lcom/google/android/gms/icing/impl/a/f;

.field private final c:Lcom/google/android/gms/icing/impl/a/i;

.field private d:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a/f;Lcom/google/android/gms/icing/impl/a/i;)V
    .locals 3

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->d:Ljava/util/Set;

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/ab;->a:Landroid/content/Context;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    .line 40
    sget-object v0, Lcom/google/android/gms/icing/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_0

    new-instance v1, Ljava/util/HashSet;

    const-string v2, ","

    invoke-virtual {v0, v2}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/a/ab;->d:Ljava/util/Set;

    .line 41
    :cond_0
    return-void
.end method

.method private a(Landroid/content/pm/PackageInfo;)V
    .locals 4

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v1

    .line 111
    if-eqz v1, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/e;->g()Z

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    .line 112
    :goto_0
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v2, :cond_4

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v3, "com.google.android.gms.appdatasearch"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    if-nez v0, :cond_4

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 119
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->m()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const-string v2, "com.google.android.gms"

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 122
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: need to parse "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 123
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/icing/impl/a/ab;->a(Lcom/google/android/gms/icing/impl/a/e;Landroid/content/pm/PackageInfo;)V

    .line 149
    :cond_1
    :goto_1
    return-void

    .line 111
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 125
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: up to date:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    goto :goto_1

    .line 128
    :cond_4
    if-eqz v0, :cond_5

    .line 129
    const-string v0, "Package %s is not allowed to use icing"

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    .line 133
    :goto_2
    if-eqz v1, :cond_1

    .line 134
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/a/e;->b(Ljava/lang/String;)V

    .line 135
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "updateResources: resources removed:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 137
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/ab;->a(Lcom/google/android/gms/icing/impl/a/e;)V

    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/ab;->c()Lcom/google/android/gms/icing/impl/a/z;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v3, p1, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/a/f;->a(Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/icing/impl/a/z;->a(Lcom/google/android/gms/icing/impl/a/aa;)Ljava/util/Set;

    move-result-object v0

    .line 141
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 142
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/icing/impl/a/ab;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V

    goto :goto_3

    .line 131
    :cond_5
    const-string v0, "Package %s has no appdatasearch metadata"

    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_2

    .line 146
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/e;->l()V

    goto :goto_1
.end method

.method private static a(Lcom/google/android/gms/icing/impl/a/e;)V
    .locals 4

    .prologue
    .line 225
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/e;->f()Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 226
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Clearing GSAI for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "; no longer in resources"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    .line 227
    const/4 v0, 0x0

    const-wide/16 v2, 0x0

    :try_start_0
    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;->b(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/e;->a(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_0 .. :try_end_0} :catch_0

    .line 235
    :cond_0
    return-void

    .line 232
    :catch_0
    move-exception v0

    new-instance v0, Lcom/google/android/gms/icing/impl/b/c;

    const-string v1, "Could not clear GSAI"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/google/android/gms/icing/impl/a/e;Landroid/content/pm/PackageInfo;)V
    .locals 10

    .prologue
    .line 158
    iget-object v0, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/icing/impl/a/b;->a(Landroid/content/pm/PackageManager;Landroid/content/Context;Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/b;

    move-result-object v0

    .line 159
    if-eqz v0, :cond_2

    .line 161
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {p1, v1}, Lcom/google/android/gms/icing/impl/a/e;->b(Ljava/lang/String;)V

    .line 162
    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/a/b;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/a;

    move-result-object v1

    .line 164
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ab;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    .line 165
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/ab;->c()Lcom/google/android/gms/icing/impl/a/z;

    move-result-object v0

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v4, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/a/f;->a(Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v3

    invoke-interface {v0, v3}, Lcom/google/android/gms/icing/impl/a/z;->a(Lcom/google/android/gms/icing/impl/a/aa;)Ljava/util/Set;

    move-result-object v3

    .line 168
    const/4 v0, 0x0

    :goto_0
    iget-object v4, v1, Lcom/google/android/gms/icing/impl/a/a;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    array-length v4, v4

    if-ge v0, v4, :cond_3

    .line 169
    iget-object v4, v1, Lcom/google/android/gms/icing/impl/a/a;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    aget-object v4, v4, v0

    .line 171
    if-eqz v2, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/ab;->d:Ljava/util/Set;

    iget-object v6, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 172
    :cond_0
    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-interface {v3, v4}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 173
    iget-object v4, v1, Lcom/google/android/gms/icing/impl/a/a;->a:[Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    aget-object v4, v4, v0
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_0 .. :try_end_0} :catch_2

    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    iget-object v6, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v7, p2, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    invoke-virtual {v6, v7}, Lcom/google/android/gms/icing/impl/a/f;->a(Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v6

    iget-wide v8, p2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-interface {v5, v6, v4, v8, v9}, Lcom/google/android/gms/icing/impl/a/i;->a(Lcom/google/android/gms/icing/impl/a/aa;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;J)V
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_1 .. :try_end_1} :catch_1

    .line 168
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 173
    :catch_0
    move-exception v0

    :try_start_2
    new-instance v1, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "From "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p2, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " resources: problem with corpus "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_2 .. :try_end_2} :catch_2

    .line 197
    :catch_1
    move-exception v0

    .line 198
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/d;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 199
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/d;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/a/e;->b(Ljava/lang/String;)V

    .line 205
    :cond_2
    :goto_1
    return-void

    .line 175
    :cond_3
    :try_start_3
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 177
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/a/ab;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V
    :try_end_3
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_3 .. :try_end_3} :catch_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_2

    .line 200
    :catch_2
    move-exception v0

    .line 201
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 202
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/b/a;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/a/e;->b(Ljava/lang/String;)V

    goto :goto_1

    .line 182
    :cond_4
    :try_start_4
    iget-object v0, v1, Lcom/google/android/gms/icing/impl/a/a;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 183
    if-eqz v0, :cond_5

    .line 184
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting GSAI for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ": "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I
    :try_end_4
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_4 .. :try_end_4} :catch_2

    .line 186
    :try_start_5
    iget-wide v2, p2, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;->b(Ljava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/a/e;->a(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_5
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_5 .. :try_end_5} :catch_3
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_5 .. :try_end_5} :catch_1

    .line 196
    :goto_3
    :try_start_6
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->l()V

    goto :goto_1

    .line 190
    :catch_3
    move-exception v0

    new-instance v0, Lcom/google/android/gms/icing/impl/b/c;

    const-string v1, "Could not set GSAI from resources"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/String;)V

    throw v0

    .line 193
    :cond_5
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/a/ab;->a(Lcom/google/android/gms/icing/impl/a/e;)V
    :try_end_6
    .catch Lcom/google/android/gms/icing/impl/a/d; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_6 .. :try_end_6} :catch_2

    goto :goto_3
.end method

.method private a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/icing/impl/a/i;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)V

    .line 222
    return-void
.end method

.method private final c()Lcom/google/android/gms/icing/impl/a/z;
    .locals 1

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->d:Lcom/google/android/gms/icing/impl/a/z;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 5

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledPackages(I)Ljava/util/List;

    move-result-object v2

    .line 61
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "updateResources: found "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " total apps"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;)I

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->c()Ljava/util/Set;

    move-result-object v3

    .line 63
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 64
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    .line 65
    iget-object v4, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v4, v4, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v4, :cond_0

    .line 66
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 67
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/ab;->a(Landroid/content/pm/PackageInfo;)V

    .line 63
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 69
    :cond_0
    const-string v4, "Package %s is disabled"

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v4, v0}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_1

    .line 73
    :cond_1
    const-string v0, "Apps that are now uninstalled (%d): %s"

    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 74
    invoke-interface {v3}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/a/i;->a(Ljava/lang/String;)V

    goto :goto_2

    .line 77
    :cond_2
    return-void
.end method

.method public final a(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 95
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    const/16 v1, 0x80

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 97
    iget-object v1, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget-boolean v1, v1, Landroid/content/pm/ApplicationInfo;->enabled:Z

    if-eqz v1, :cond_0

    .line 98
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/ab;->a(Landroid/content/pm/PackageInfo;)V

    .line 103
    :goto_0
    const/4 v0, 0x1

    .line 105
    :goto_1
    return v0

    .line 100
    :cond_0
    const-string v1, "Package %s is disabled"

    iget-object v2, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;)I

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/icing/impl/a/i;->a(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/ab;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/f;->c()Ljava/util/Set;

    move-result-object v0

    .line 81
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 82
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/ab;->a(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 84
    const-string v2, "Package %s no longer installed"

    invoke-static {v2, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 85
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/ab;->c:Lcom/google/android/gms/icing/impl/a/i;

    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/a/i;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 88
    :cond_1
    return-void
.end method
