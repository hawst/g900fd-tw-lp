.class public final Lcom/google/android/gms/games/service/a/e/a;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:I

.field private final e:I

.field private final f:Z

.field private final g:Lcom/google/android/gms/games/a/au;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IIZ)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/a;->c:Lcom/google/android/gms/games/internal/dr;

    .line 30
    iput p3, p0, Lcom/google/android/gms/games/service/a/e/a;->d:I

    .line 31
    iput p4, p0, Lcom/google/android/gms/games/service/a/e/a;->e:I

    .line 32
    iput-boolean p5, p0, Lcom/google/android/gms/games/service/a/e/a;->f:Z

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/e/a;->g:Lcom/google/android/gms/games/a/au;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/a;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->h(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 53
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/games/service/a/e/a;->d:I

    packed-switch v0, :pswitch_data_0

    .line 46
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    .line 41
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/a;->g:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/e/a;->e:I

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/e/a;->f:Z

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->f(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 43
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/a;->g:Lcom/google/android/gms/games/a/au;

    iget v1, p0, Lcom/google/android/gms/games/service/a/e/a;->e:I

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/e/a;->f:Z

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->g(Lcom/google/android/gms/games/a/au;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 39
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
