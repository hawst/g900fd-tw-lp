.class final Lcom/google/android/gms/games/ui/client/matches/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    .prologue
    .line 1564
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 1567
    const-string v0, "return to game sequence not on main thread"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/String;)V

    .line 1568
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->i(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 1574
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V

    .line 1578
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    .line 1579
    return-void

    .line 1576
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/i;->a:Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;

    const/4 v1, -0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V

    goto :goto_0
.end method
