.class public final Lcom/google/android/gms/people/internal/b;
.super Lcom/google/android/gms/common/data/i;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/model/c;


# instance fields
.field private final c:Landroid/os/Bundle;

.field private final d:Lcom/google/android/gms/people/model/b;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/model/b;Lcom/google/android/gms/common/data/DataHolder;ILandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 146
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/common/data/i;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 147
    iput-object p4, p0, Lcom/google/android/gms/people/internal/b;->c:Landroid/os/Bundle;

    .line 148
    iput-object p1, p0, Lcom/google/android/gms/people/internal/b;->d:Lcom/google/android/gms/people/model/b;

    .line 149
    return-void
.end method


# virtual methods
.method public final c()I
    .locals 1

    .prologue
    .line 182
    const-string v0, "data_source"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/internal/b;->c(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 266
    const-string v0, "primary_logging_id_sorted"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/internal/b;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
