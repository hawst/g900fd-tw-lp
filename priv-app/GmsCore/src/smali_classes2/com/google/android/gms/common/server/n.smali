.class public Lcom/google/android/gms/common/server/n;
.super Lcom/google/android/gms/common/server/a;
.source "SourceFile"


# instance fields
.field protected final c:Ljava/lang/String;

.field protected final d:Lcom/android/volley/s;

.field protected final e:Ljava/lang/ThreadLocal;

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 9

    .prologue
    .line 85
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move-object v6, p6

    move-object/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 104
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/common/server/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 51
    new-instance v0, Lcom/google/android/gms/common/server/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/server/o;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/common/server/n;->e:Ljava/lang/ThreadLocal;

    .line 107
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/server/n;->d:Lcom/android/volley/s;

    .line 108
    iput-object p8, p0, Lcom/google/android/gms/common/server/n;->c:Ljava/lang/String;

    .line 109
    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/common/server/n;
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 66
    new-instance v0, Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v3, v2

    move-object v6, v2

    move-object v7, v2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/n;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 12

    .prologue
    .line 375
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v7

    .line 376
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v8

    .line 377
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v9

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v7}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v11

    .line 379
    invoke-static {p2, v11}, Lcom/google/android/gms/common/server/n;->a(ILjava/util/Map;)I

    move-result v2

    .line 380
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/common/server/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v0, p0

    move-object v1, p1

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v10, v9

    invoke-virtual/range {v0 .. v11}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/util/HashMap;)Lcom/google/android/gms/common/server/h;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 384
    :try_start_0
    invoke-virtual {v9}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 386
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 387
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 388
    :catch_1
    move-exception v0

    .line 389
    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/common/server/n;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    .line 390
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 8

    .prologue
    .line 547
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    .line 548
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v4

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    .line 550
    invoke-static {p2, v7}, Lcom/google/android/gms/common/server/n;->a(ILjava/util/Map;)I

    move-result v1

    .line 551
    new-instance v0, Lcom/google/android/gms/common/server/v;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p3}, Lcom/google/android/gms/common/server/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v3, p4

    move-object v5, v4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/v;-><init>(ILjava/lang/String;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/util/HashMap;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 554
    :try_start_0
    invoke-virtual {v4}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 561
    return-void

    .line 556
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 557
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 558
    :catch_1
    move-exception v0

    .line 559
    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/common/server/n;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    .line 560
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private f()Lcom/google/android/gms/common/server/c;
    .locals 3

    .prologue
    .line 690
    invoke-direct {p0}, Lcom/google/android/gms/common/server/n;->g()Z

    move-result v0

    const-string v1, "Not currently in an Apiary batch."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 691
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/p;

    .line 693
    iget-object v1, v0, Lcom/google/android/gms/common/server/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_1

    .line 694
    const/4 v0, 0x0

    .line 700
    :goto_1
    return-object v0

    .line 693
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 697
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->a()Ljava/lang/String;

    move-result-object v2

    new-instance v1, Lcom/google/android/gms/common/server/c;

    iget-object v0, v0, Lcom/google/android/gms/common/server/p;->a:Ljava/util/ArrayList;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/server/c;-><init>(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 698
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->d:Lcom/android/volley/s;

    invoke-virtual {v0, v1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    move-object v0, v1

    .line 700
    goto :goto_1
.end method

.method private g()Z
    .locals 1

    .prologue
    .line 722
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Ljava/lang/String;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/util/HashMap;)Lcom/google/android/gms/common/server/h;
    .locals 12

    .prologue
    .line 498
    new-instance v0, Lcom/google/android/gms/common/server/h;

    iget-boolean v10, p0, Lcom/google/android/gms/common/server/n;->b:Z

    move v1, p2

    move-object v2, p3

    move-object/from16 v3, p4

    move-object/from16 v4, p5

    move-object/from16 v5, p6

    move-object/from16 v6, p9

    move-object/from16 v7, p10

    move-object/from16 v8, p7

    move-object/from16 v9, p8

    move-object/from16 v11, p11

    invoke-direct/range {v0 .. v11}, Lcom/google/android/gms/common/server/h;-><init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    return-object v0
.end method

.method public a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 7

    .prologue
    .line 324
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 2

    .prologue
    .line 345
    :try_start_0
    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 350
    :goto_0
    return-object v0

    .line 347
    :catch_0
    move-exception v0

    .line 348
    invoke-static {v0}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    invoke-direct/range {p0 .. p6}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    goto :goto_0

    .line 353
    :cond_0
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 6

    .prologue
    .line 305
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[Lcom/google/android/gms/common/server/g;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 11

    .prologue
    .line 398
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v7

    .line 399
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v8

    .line 400
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v7}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v10

    .line 402
    new-instance v0, Lcom/google/android/gms/common/server/f;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/common/server/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    iget-boolean v9, p0, Lcom/google/android/gms/common/server/n;->b:Z

    move-object v2, p3

    move-object v3, p4

    move-object v6, v5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/common/server/f;-><init>(Ljava/lang/String;[Lcom/google/android/gms/common/server/g;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    .line 405
    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 407
    :try_start_0
    invoke-virtual {v5}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 409
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 410
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 411
    :catch_1
    move-exception v0

    .line 412
    invoke-virtual {p0, v0, v7}, Lcom/google/android/gms/common/server/n;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    .line 413
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;
    .locals 3

    .prologue
    .line 420
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/server/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 421
    iget-object v1, p0, Lcom/google/android/gms/common/server/n;->c:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 423
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->e()Ljava/lang/String;

    move-result-object v1

    .line 424
    iget-object v2, p0, Lcom/google/android/gms/common/server/n;->f:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 426
    iput-object v1, p0, Lcom/google/android/gms/common/server/n;->f:Ljava/lang/String;

    .line 427
    iget-object v2, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-static {v2, v1}, Lcom/google/android/gms/common/util/e;->h(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/common/server/n;->g:Ljava/lang/String;

    .line 430
    :cond_0
    const-string v1, "X-Android-Package"

    iget-object v2, p0, Lcom/google/android/gms/common/server/n;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    const-string v1, "X-Android-Cert"

    iget-object v2, p0, Lcom/google/android/gms/common/server/n;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 433
    :cond_1
    return-object v0
.end method

.method public a(Lcom/android/volley/p;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 704
    invoke-virtual {p0, p1, p3}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;Ljava/lang/String;)V

    .line 706
    invoke-direct {p0}, Lcom/google/android/gms/common/server/n;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    const-string v0, "Non-batchable request in batch"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 708
    instance-of v0, p1, Lcom/google/android/gms/common/server/m;

    const-string v1, "Non-batchable request in batch"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 711
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/p;

    .line 712
    check-cast p1, Lcom/google/android/gms/common/server/m;

    iget-object v0, v0, Lcom/google/android/gms/common/server/p;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 716
    :goto_0
    return-void

    .line 714
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->d:Lcom/android/volley/s;

    invoke-virtual {v0, p1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 534
    :try_start_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 542
    :goto_0
    return-void

    .line 535
    :catch_0
    move-exception v0

    .line 536
    invoke-static {v0}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/server/n;->b(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    goto :goto_0

    .line 540
    :cond_0
    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 13

    .prologue
    .line 190
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v9

    .line 191
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v10

    .line 192
    if-eqz v9, :cond_0

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p1, v9}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v12

    .line 194
    const/4 v1, 0x0

    invoke-static {v1, v12}, Lcom/google/android/gms/common/server/n;->a(ILjava/util/Map;)I

    move-result v2

    .line 195
    new-instance v1, Lcom/google/android/gms/common/server/h;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->b()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p3

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/common/server/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    iget-boolean v11, p0, Lcom/google/android/gms/common/server/n;->b:Z

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move-object/from16 v8, p8

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/common/server/h;-><init>(ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    const/4 v2, 0x1

    invoke-virtual {p0, v1, v2, v9}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 201
    :goto_0
    return-void

    .line 199
    :cond_0
    new-instance v1, Lcom/android/volley/ac;

    const-string v2, "Unable to obtain auth token - is the device online?"

    invoke-direct {v1, v2}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p8

    invoke-interface {v0, v1}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 8

    .prologue
    .line 238
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v6

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->a:Landroid/content/Context;

    invoke-virtual {p0, v0, p1, v6}, Lcom/google/android/gms/common/server/n;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v7

    .line 240
    const/4 v0, 0x1

    invoke-static {v0, v7}, Lcom/google/android/gms/common/server/n;->a(ILjava/util/Map;)I

    move-result v1

    .line 241
    if-eqz v6, :cond_0

    .line 242
    new-instance v0, Lcom/google/android/gms/common/server/v;

    invoke-virtual {p0}, Lcom/google/android/gms/common/server/n;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, p2}, Lcom/google/android/gms/common/server/n;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    move-object v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/server/v;-><init>(ILjava/lang/String;Ljava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/util/HashMap;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    new-instance v0, Lcom/android/volley/ac;

    const-string v1, "Unable to obtain auth token - is the device online?"

    invoke-direct {v0, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    invoke-interface {p4, v0}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Class;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 169
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v5, p3

    move-object v6, v4

    move-object v7, p4

    move-object v8, p5

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;Lcom/google/android/gms/common/server/response/FieldMappingDictionary;Lcom/android/volley/x;Lcom/android/volley/w;)V

    .line 171
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Object;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 517
    const/4 v0, -0x1

    invoke-virtual {p0, p1, v0, p2, p3}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V

    .line 519
    return-void
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Z
    .locals 1

    .prologue
    .line 681
    invoke-direct {p0}, Lcom/google/android/gms/common/server/n;->f()Lcom/google/android/gms/common/server/c;

    move-result-object v0

    .line 682
    if-eqz v0, :cond_0

    .line 683
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/server/c;->a(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 685
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)[B
    .locals 7

    .prologue
    .line 598
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 599
    new-instance v0, Lcom/google/android/gms/common/server/u;

    iget-boolean v4, p0, Lcom/google/android/gms/common/server/n;->b:Z

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/u;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;)V

    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 602
    :try_start_0
    invoke-virtual {v5}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 604
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 605
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 606
    :catch_1
    move-exception v0

    .line 607
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public final a(Landroid/content/Context;Ljava/lang/String;)[B
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 573
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 574
    new-instance v0, Lcom/google/android/gms/common/server/u;

    iget-boolean v4, p0, Lcom/google/android/gms/common/server/n;->b:Z

    move-object v1, p1

    move-object v3, p2

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/u;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 577
    :try_start_0
    invoke-virtual {v5}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    return-object v0

    .line 579
    :catch_0
    move-exception v0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 580
    new-instance v0, Lcom/google/android/gms/common/server/ac;

    invoke-direct {v0}, Lcom/google/android/gms/common/server/ac;-><init>()V

    throw v0

    .line 581
    :catch_1
    move-exception v0

    .line 582
    new-instance v1, Lcom/android/volley/ac;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error executing network request for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public bridge synthetic b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 438
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/common/server/a;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->c:Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 446
    const-string v0, "?"

    invoke-virtual {v1, v0}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/16 v0, 0x26

    .line 447
    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/server/n;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 449
    :goto_1
    return-object v0

    .line 446
    :cond_0
    const/16 v0, 0x3f

    goto :goto_0

    :cond_1
    move-object v0, v1

    goto :goto_1
.end method

.method public final b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x0

    .line 620
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v5

    .line 621
    new-instance v0, Lcom/google/android/gms/common/server/u;

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/u;-><init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;)V

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v4, v1}, Lcom/google/android/gms/common/server/n;->a(Lcom/android/volley/p;ZLjava/lang/String;)V

    .line 623
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 655
    iget-object v0, p0, Lcom/google/android/gms/common/server/n;->e:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Stack;

    new-instance v1, Lcom/google/android/gms/common/server/p;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/server/p;-><init>(B)V

    invoke-virtual {v0, v1}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 656
    return-void
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 662
    invoke-direct {p0}, Lcom/google/android/gms/common/server/n;->f()Lcom/google/android/gms/common/server/c;

    .line 663
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 670
    invoke-direct {p0}, Lcom/google/android/gms/common/server/n;->f()Lcom/google/android/gms/common/server/c;

    move-result-object v0

    .line 671
    if-eqz v0, :cond_0

    .line 672
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/c;->t()V

    .line 674
    :cond_0
    return-void
.end method
