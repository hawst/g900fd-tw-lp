.class public final Lcom/google/android/gms/common/api/s;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 288
    sparse-switch p0, :sswitch_data_0

    .line 362
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unknown status code: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 290
    :sswitch_0
    const-string v0, "SUCCESS_CACHE"

    goto :goto_0

    .line 293
    :sswitch_1
    const-string v0, "SUCCESS"

    goto :goto_0

    .line 296
    :sswitch_2
    const-string v0, "SERVICE_MISSING"

    goto :goto_0

    .line 299
    :sswitch_3
    const-string v0, "SERVICE_VERSION_UPDATE_REQUIRED"

    goto :goto_0

    .line 302
    :sswitch_4
    const-string v0, "SERVICE_DISABLED"

    goto :goto_0

    .line 305
    :sswitch_5
    const-string v0, "SIGN_IN_REQUIRED"

    goto :goto_0

    .line 308
    :sswitch_6
    const-string v0, "INVALID_ACCOUNT"

    goto :goto_0

    .line 311
    :sswitch_7
    const-string v0, "RESOLUTION_REQUIRED"

    goto :goto_0

    .line 314
    :sswitch_8
    const-string v0, "NETWORK_ERROR"

    goto :goto_0

    .line 317
    :sswitch_9
    const-string v0, "INTERNAL_ERROR"

    goto :goto_0

    .line 320
    :sswitch_a
    const-string v0, "SERVICE_INVALID"

    goto :goto_0

    .line 323
    :sswitch_b
    const-string v0, "DEVELOPER_ERROR"

    goto :goto_0

    .line 326
    :sswitch_c
    const-string v0, "LICENSE_CHECK_FAILED"

    goto :goto_0

    .line 332
    :sswitch_d
    const-string v0, "ERROR_OPERATION_FAILED"

    goto :goto_0

    .line 335
    :sswitch_e
    const-string v0, "INTERRUPTED"

    goto :goto_0

    .line 338
    :sswitch_f
    const-string v0, "TIMEOUT"

    goto :goto_0

    .line 341
    :sswitch_10
    const-string v0, "CANCELED"

    goto :goto_0

    .line 344
    :sswitch_11
    const-string v0, "AUTH_API_INVALID_CREDENTIALS"

    goto :goto_0

    .line 347
    :sswitch_12
    const-string v0, "AUTH_API_ACCESS_FORBIDDEN"

    goto :goto_0

    .line 350
    :sswitch_13
    const-string v0, "AUTH_API_CLIENT_ERROR"

    goto :goto_0

    .line 353
    :sswitch_14
    const-string v0, "AUTH_API_SERVER_ERROR"

    goto :goto_0

    .line 356
    :sswitch_15
    const-string v0, "AUTH_TOKEN_ERROR"

    goto :goto_0

    .line 359
    :sswitch_16
    const-string v0, "AUTH_URL_RESOLUTION"

    goto :goto_0

    .line 288
    :sswitch_data_0
    .sparse-switch
        -0x1 -> :sswitch_0
        0x0 -> :sswitch_1
        0x1 -> :sswitch_2
        0x2 -> :sswitch_3
        0x3 -> :sswitch_4
        0x4 -> :sswitch_5
        0x5 -> :sswitch_6
        0x6 -> :sswitch_7
        0x7 -> :sswitch_8
        0x8 -> :sswitch_9
        0x9 -> :sswitch_a
        0xa -> :sswitch_b
        0xb -> :sswitch_c
        0xd -> :sswitch_d
        0xe -> :sswitch_e
        0xf -> :sswitch_f
        0x10 -> :sswitch_10
        0xbb8 -> :sswitch_11
        0xbb9 -> :sswitch_12
        0xbba -> :sswitch_13
        0xbbb -> :sswitch_14
        0xbbc -> :sswitch_15
        0xbbd -> :sswitch_16
    .end sparse-switch
.end method
