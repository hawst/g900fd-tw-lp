.class public Lcom/google/android/gms/checkin/EventLogService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/checkin/j;

.field private static b:Landroid/os/PowerManager$WakeLock;


# instance fields
.field private c:Landroid/os/PowerManager$WakeLock;

.field private d:Landroid/os/AsyncTask;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 63
    new-instance v0, Lcom/google/android/gms/checkin/j;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/checkin/EventLogService;->a:Lcom/google/android/gms/checkin/j;

    .line 64
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 66
    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    .line 67
    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->d:Landroid/os/AsyncTask;

    .line 72
    return-void
.end method

.method static synthetic a(Landroid/content/SharedPreferences;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 55
    const-wide/16 v0, 0x0

    invoke-static {p0, p1, v0, v1}, Lcom/google/android/gms/checkin/EventLogService;->a(Landroid/content/SharedPreferences;Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/content/SharedPreferences;Ljava/lang/String;J)J
    .locals 5

    .prologue
    const-wide/16 v0, 0x0

    .line 104
    const-wide/16 v2, 0x0

    :try_start_0
    invoke-interface {p0, p1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 108
    :goto_0
    return-wide v0

    .line 106
    :catch_0
    move-exception v2

    const-string v2, "EventLogService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Non-long value in sharedPrefs. key: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/checkin/EventLogService;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->d:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic a()Lcom/google/android/gms/checkin/j;
    .locals 1

    .prologue
    .line 55
    sget-object v0, Lcom/google/android/gms/checkin/EventLogService;->a:Lcom/google/android/gms/checkin/j;

    return-object v0
.end method

.method public static a(Ljava/util/Map;Ljava/lang/String;ZLjava/lang/String;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 233
    if-eqz p2, :cond_0

    .line 247
    :goto_0
    return-object p0

    .line 236
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 237
    if-nez p3, :cond_1

    move-object p0, v0

    .line 238
    goto :goto_0

    .line 240
    :cond_1
    const-string v1, ","

    invoke-virtual {p3, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 241
    array-length v3, v2

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 242
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 243
    invoke-interface {p0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 244
    invoke-interface {p0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    invoke-virtual {v0, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 241
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    move-object p0, v0

    .line 247
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 10

    .prologue
    .line 83
    const-string v0, "EventLogService"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v8

    .line 84
    const-string v0, "dropbox"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/os/DropBoxManager;

    .line 86
    sget-object v9, Lcom/google/android/gms/checkin/EventLogService;->a:Lcom/google/android/gms/checkin/j;

    monitor-enter v9

    .line 87
    :try_start_0
    const-string v0, "lastLog"

    const-wide/16 v2, 0x0

    invoke-static {v8, v0, v2, v3}, Lcom/google/android/gms/checkin/EventLogService;->a(Landroid/content/SharedPreferences;Ljava/lang/String;J)J

    move-result-wide v2

    .line 88
    const-string v0, "EventLogService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Accumulating logs since "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    :try_start_1
    sget-object v0, Lcom/google/android/gms/checkin/EventLogService;->a:Lcom/google/android/gms/checkin/j;

    const-wide/16 v4, -0x1

    invoke-static {p0}, Lcom/google/android/gms/checkin/EventLogService;->b(Landroid/content/Context;)Z

    move-result v7

    move-object v1, p0

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/checkin/j;->a(Landroid/content/Context;JJLandroid/os/DropBoxManager;Z)J

    move-result-wide v0

    .line 93
    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "lastLog"

    invoke-interface {v2, v3, v0, v1}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 97
    :goto_0
    :try_start_2
    monitor-exit v9

    return-void

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v1, "EventLogService"

    const-string v2, "Can\'t capture logs"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 97
    :catchall_0
    move-exception v0

    monitor-exit v9

    throw v0
.end method

.method static b(Landroid/content/Context;)Z
    .locals 8

    .prologue
    const-wide/16 v6, 0x5

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 112
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v3, Lcom/google/android/gms/usagereporting/a;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v3, v6, v7, v0}, Lcom/google/android/gms/common/api/v;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    sget-object v0, Lcom/google/android/gms/usagereporting/a;->b:Lcom/google/android/gms/usagereporting/d;

    invoke-interface {v0, v3}, Lcom/google/android/gms/usagereporting/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v6, v7, v4}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/usagereporting/g;

    if-eqz v0, :cond_3

    invoke-interface {v0}, Lcom/google/android/gms/usagereporting/g;->b()Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "EventLogService"

    const-string v4, "Opted in for usage reporting"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    :goto_0
    invoke-interface {v3}, Lcom/google/android/gms/common/api/v;->d()V

    .line 113
    if-eqz v0, :cond_1

    .line 119
    :cond_0
    :goto_1
    return v1

    .line 116
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v3, "checkin_optedin_for_usage_reporting"

    invoke-static {v0, v3}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 119
    if-eqz v0, :cond_2

    sget-object v3, Lcom/google/android/gsf/f;->c:Ljava/util/regex/Pattern;

    invoke-virtual {v3, v0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v0

    if-nez v0, :cond_0

    :cond_2
    move v1, v2

    goto :goto_1

    :cond_3
    move v0, v2

    goto :goto_0
.end method

.method static synthetic c(Landroid/content/Context;)V
    .locals 12

    .prologue
    const/4 v1, 0x0

    .line 55
    const-string v0, "EventLogService"

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    new-instance v2, Lcom/android/a/a;

    invoke-direct {v2, v0}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-instance v4, Lcom/android/a/b;

    invoke-direct {v4}, Lcom/android/a/b;-><init>()V

    const-string v3, "aggregation_interval_seconds"

    const/16 v5, 0x708

    invoke-static {v0, v3, v5}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    mul-int/lit16 v0, v0, 0x3e8

    int-to-long v6, v0

    iput-wide v6, v4, Lcom/android/a/b;->f:J

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v2, v4}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v2

    cmp-long v0, v2, v6

    if-gtz v0, :cond_2

    const-class v1, Lcom/google/android/gms/checkin/EventLogService;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    const-string v0, "power"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v2, 0x1

    const-string v3, "Event Log Handoff"

    invoke-virtual {v0, v2, v3}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/checkin/EventLogService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    :cond_1
    :goto_0
    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    const-wide v6, 0x7fffffffffffffffL

    cmp-long v0, v2, v6

    if-gez v0, :cond_1

    const-string v0, "alarm"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    new-instance v5, Landroid/content/Intent;

    const-class v6, Lcom/google/android/gms/checkin/EventLogService$Receiver;

    invoke-direct {v5, p0, v6}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-static {p0, v1, v5, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v6

    iget-wide v8, v4, Lcom/android/a/b;->f:J

    const-wide/16 v10, 0x0

    cmp-long v5, v8, v10

    if-lez v5, :cond_3

    iget-wide v4, v4, Lcom/android/a/b;->f:J

    invoke-virtual/range {v0 .. v6}, Landroid/app/AlarmManager;->setRepeating(IJJLandroid/app/PendingIntent;)V

    goto :goto_0

    :cond_3
    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_0

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 146
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    .line 148
    :cond_0
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    if-nez v0, :cond_0

    .line 152
    const-string v0, "power"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/EventLogService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    .line 153
    const-string v1, "Event Log Service"

    invoke-virtual {v0, v4, v1}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->c:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 157
    :cond_0
    const-class v1, Lcom/google/android/gms/checkin/EventLogService;

    monitor-enter v1

    .line 158
    :try_start_0
    sget-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    if-eqz v0, :cond_1

    .line 159
    sget-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 160
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/checkin/EventLogService;->b:Landroid/os/PowerManager$WakeLock;

    .line 162
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_2

    .line 222
    :goto_0
    return v4

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 171
    :cond_2
    new-instance v0, Lcom/google/android/gms/checkin/ab;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/checkin/ab;-><init>(Lcom/google/android/gms/checkin/EventLogService;Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/checkin/EventLogService;->d:Landroid/os/AsyncTask;

    .line 219
    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/checkin/EventLogService;->d:Landroid/os/AsyncTask;

    invoke-static {}, Lcom/google/android/gms/a/a;->a()Lcom/google/android/gms/a/a;

    invoke-static {}, Lcom/google/android/gms/a/a;->k()Ljava/util/concurrent/Executor;

    move-result-object v2

    const/4 v3, 0x0

    new-array v3, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/a/a;->a(Landroid/os/AsyncTask;Ljava/util/concurrent/Executor;[Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method
