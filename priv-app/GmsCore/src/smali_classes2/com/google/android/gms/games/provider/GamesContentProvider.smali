.class public Lcom/google/android/gms/games/provider/GamesContentProvider;
.super Lcom/google/android/gms/common/e/a;
.source "SourceFile"


# static fields
.field private static final H:Lcom/android/a/a/a;

.field private static final I:Ljava/lang/String;

.field private static final J:Ljava/lang/String;

.field private static final K:Ljava/lang/String;

.field private static final L:Lcom/android/a/a/a;

.field private static final M:Ljava/lang/String;

.field private static final N:Lcom/android/a/a/a;

.field private static final O:Ljava/lang/String;

.field private static final P:Lcom/android/a/a/a;

.field private static final Q:Ljava/lang/String;

.field private static final R:Ljava/lang/String;

.field private static final S:Ljava/lang/String;

.field private static final T:Lcom/android/a/a/a;

.field private static final U:Lcom/android/a/a/a;

.field private static final V:Lcom/android/a/a/a;

.field private static final W:Ljava/lang/String;

.field private static final X:Ljava/lang/String;

.field private static final Y:Ljava/lang/String;

.field private static final Z:Lcom/android/a/a/a;

.field private static final a:Landroid/content/UriMatcher;

.field private static final aa:Ljava/lang/String;

.field private static final ab:Ljava/lang/String;

.field private static final ac:Ljava/lang/String;

.field private static final ad:Ljava/lang/String;

.field private static final ae:Ljava/lang/String;

.field private static final af:Ljava/lang/String;

.field private static final ag:Ljava/lang/String;

.field private static final ah:Lcom/android/a/a/a;

.field private static final ai:Ljava/lang/String;

.field private static final aj:Ljava/lang/String;

.field private static final ak:Ljava/lang/String;

.field private static final al:Ljava/lang/String;

.field private static final am:Lcom/android/a/a/a;

.field private static final an:Ljava/lang/String;

.field private static final ao:Ljava/lang/String;

.field private static final ap:Ljava/lang/String;

.field private static final aq:Lcom/android/a/a/a;

.field private static final ar:Ljava/lang/String;

.field private static final as:Lcom/android/a/a/a;

.field private static final at:Ljava/lang/String;

.field private static final au:Ljava/lang/String;

.field private static final av:Ljava/lang/String;

.field private static final aw:Ljava/lang/String;

.field private static final ax:Ljava/lang/String;

.field private static final ay:Ljava/lang/String;

.field private static final az:Lcom/google/android/gms/games/provider/a;


# instance fields
.field private A:Lcom/google/android/gms/games/provider/h;

.field private B:Lcom/google/android/gms/games/provider/h;

.field private C:Lcom/google/android/gms/games/provider/h;

.field private D:Lcom/google/android/gms/games/provider/h;

.field private E:Lcom/google/android/gms/games/provider/h;

.field private F:Lcom/google/android/gms/games/provider/h;

.field private G:Lcom/google/android/gms/games/provider/h;

.field private b:Landroid/os/HandlerThread;

.field private c:Landroid/os/Handler;

.field private d:J

.field private e:Lcom/google/android/gms/common/util/p;

.field private final f:Ljava/util/HashMap;

.field private final g:Ljava/util/Map;

.field private final h:Ljava/lang/Object;

.field private i:Lcom/google/android/gms/games/provider/h;

.field private j:Lcom/google/android/gms/games/provider/h;

.field private k:Lcom/google/android/gms/games/provider/h;

.field private l:Lcom/google/android/gms/games/provider/h;

.field private m:Lcom/google/android/gms/games/provider/h;

.field private n:Lcom/google/android/gms/games/provider/h;

.field private o:Lcom/google/android/gms/games/provider/h;

.field private p:Lcom/google/android/gms/games/provider/h;

.field private q:Lcom/google/android/gms/games/provider/h;

.field private r:Lcom/google/android/gms/games/provider/h;

.field private s:Lcom/google/android/gms/games/provider/h;

.field private t:Lcom/google/android/gms/games/provider/h;

.field private u:Lcom/google/android/gms/games/provider/h;

.field private v:Lcom/google/android/gms/games/provider/h;

.field private w:Lcom/google/android/gms/games/provider/h;

.field private x:Lcom/google/android/gms/games/provider/h;

.field private y:Lcom/google/android/gms/games/provider/h;

.field private z:Lcom/google/android/gms/games/provider/h;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    .line 94
    new-instance v0, Landroid/content/UriMatcher;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Landroid/content/UriMatcher;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    .line 159
    invoke-static {}, Lcom/google/android/gms/games/provider/k;->values()[Lcom/google/android/gms/games/provider/k;

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 160
    sget-object v4, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    const-string v5, "com.google.android.gms.games.background"

    invoke-virtual {v3}, Lcom/google/android/gms/games/provider/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3}, Ljava/lang/Enum;->ordinal()I

    move-result v3

    invoke-virtual {v4, v5, v6, v3}, Landroid/content/UriMatcher;->addURI(Ljava/lang/String;Ljava/lang/String;I)V

    .line 159
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 365
    :cond_0
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_count"

    const-string v2, "COUNT(*)"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->H:Lcom/android/a/a/a;

    .line 443
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_badges AS Badges"

    const-string v2, "games._id"

    const-string v3, "Badges._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->I:Ljava/lang/String;

    .line 469
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "game_badges"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "badge_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->J:Ljava/lang/String;

    .line 491
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "game_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "instance_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->K:Ljava/lang/String;

    .line 519
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/ap;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->L:Lcom/android/a/a/a;

    .line 552
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "achievement_definitions"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ljava/lang/String;

    .line 560
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "achievement_pending_ops._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/p;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->N:Lcom/android/a/a/a;

    .line 567
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "achievement_pending_ops"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->O:Ljava/lang/String;

    .line 575
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "request_pending_ops._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/au;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->P:Lcom/android/a/a/a;

    .line 583
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "request_pending_ops"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Q:Ljava/lang/String;

    .line 591
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "application_sessions"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->R:Ljava/lang/String;

    .line 633
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "achievement_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "achievement_definitions"

    const-string v2, "definition_id"

    const-string v3, "achievement_definitions._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->S:Ljava/lang/String;

    .line 642
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "application_sessions._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/s;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->T:Lcom/android/a/a/a;

    .line 649
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "app_content._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/r;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->U:Lcom/android/a/a/a;

    .line 655
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "client_contexts._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->V:Lcom/android/a/a/a;

    .line 661
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "event_definitions"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "event_definitions_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->W:Ljava/lang/String;

    .line 691
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "event_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_definitions"

    const-string v2, "definition_id"

    const-string v3, "event_definitions._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "event_definitions_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->X:Ljava/lang/String;

    .line 731
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "event_pending_ops"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_instances"

    const-string v2, "instance_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_definitions"

    const-string v2, "definition_id"

    const-string v3, "event_definitions._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "event_definitions_game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Y:Ljava/lang/String;

    .line 763
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "images._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/ad;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Lcom/android/a/a/a;

    .line 769
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "game_icon_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aa:Ljava/lang/String;

    .line 775
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "game_hi_res_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ab:Ljava/lang/String;

    .line 781
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "images"

    const-string v2, "featured_image_id"

    const-string v3, "images._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ac:Ljava/lang/String;

    .line 809
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "experience_events"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ad:Ljava/lang/String;

    .line 829
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "leaderboards"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ae:Ljava/lang/String;

    .line 860
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "leaderboard_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "leaderboards"

    const-string v2, "leaderboard_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->af:Ljava/lang/String;

    .line 900
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "leaderboard_scores"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "leaderboard_instances"

    const-string v2, "instance_id"

    const-string v3, "leaderboard_instances._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "leaderboards"

    const-string v2, "leaderboard_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ag:Ljava/lang/String;

    .line 912
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "leaderboard_pending_scores._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/ag;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ah:Lcom/android/a/a/a;

    .line 920
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "leaderboard_pending_scores"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ai:Ljava/lang/String;

    .line 927
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "quests"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aj:Ljava/lang/String;

    .line 959
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "milestones"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "quests"

    const-string v2, "quest_id"

    const-string v3, "quests._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_instances"

    const-string v2, "event_instance_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ak:Ljava/lang/String;

    .line 981
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "quests"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "milestones"

    const-string v2, "quest_id"

    const-string v3, "quests._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_instances"

    const-string v2, "event_instance_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "event_definitions"

    const-string v2, "definition_id"

    const-string v3, "event_definitions._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->al:Ljava/lang/String;

    .line 1021
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "requests._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/aw;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->am:Lcom/android/a/a/a;

    .line 1027
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "requests"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->an:Ljava/lang/String;

    .line 1039
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->an:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "request_recipients"

    const-string v2, "request_id"

    const-string v3, "requests._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players AS RequestRecipientPlayer"

    const-string v2, "player_id"

    const-string v3, "RequestRecipientPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players AS RequestSenderPlayer"

    const-string v2, "sender_id"

    const-string v3, "RequestSenderPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ao:Ljava/lang/String;

    .line 1228
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "request_recipients"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ap:Ljava/lang/String;

    .line 1237
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "matches._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/ak;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aq:Lcom/android/a/a/a;

    .line 1243
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "matches"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ar:Ljava/lang/String;

    .line 1249
    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v0

    const-string v1, "_id"

    const-string v2, "matches_pending_ops._id"

    invoke-virtual {v0, v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/al;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v0

    invoke-virtual {v0}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->as:Lcom/android/a/a/a;

    .line 1257
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "matches_pending_ops"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "client_contexts"

    const-string v2, "client_context_id"

    const-string v3, "client_contexts._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->at:Ljava/lang/String;

    .line 1349
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->ar:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "participants"

    const-string v2, "match_id"

    const-string v3, "matches._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players AS ParticipantPlayer"

    const-string v2, "player_id"

    const-string v3, "ParticipantPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->au:Ljava/lang/String;

    .line 1457
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "invitations"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "participants"

    const-string v2, "invitation_id"

    const-string v3, "invitations._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players AS ParticipantPlayer"

    const-string v2, "player_id"

    const-string v3, "ParticipantPlayer._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->av:Ljava/lang/String;

    .line 1478
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "notifications"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aw:Ljava/lang/String;

    .line 1515
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "participants"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "player_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ax:Ljava/lang/String;

    .line 1560
    new-instance v0, Lcom/google/android/gms/common/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/e/e;-><init>()V

    const-string v1, "snapshots"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "games"

    const-string v2, "game_id"

    const-string v3, "games._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "players"

    const-string v2, "owner_id"

    const-string v3, "players._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    const-string v1, "game_instances AS TargetInstance"

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/common/e/e;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/e/e;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/e/e;->a()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ay:Ljava/lang/String;

    .line 1569
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "account_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "match_sync_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "last_package_scan_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "quest_sync_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "quest_sync_metadata_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "request_sync_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "xp_sync_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "cover_photo_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "cover_photo_image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "cover_photo_image_url"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->az:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 1596
    invoke-direct {p0}, Lcom/google/android/gms/common/e/a;-><init>()V

    .line 105
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:J

    .line 123
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    .line 126
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->h:Ljava/lang/Object;

    .line 1597
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:Lcom/google/android/gms/common/util/p;

    .line 1598
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    .line 1599
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 3837
    const-string v0, "%s IN (SELECT %s FROM %s WHERE %s=?)"

    const/4 v1, 0x4

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p3, v1, v3

    const-string v2, "_id"

    aput-object v2, v1, v4

    const/4 v2, 0x2

    aput-object p4, v1, v2

    const/4 v2, 0x3

    aput-object p5, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 3839
    new-array v1, v4, [Ljava/lang/String;

    aput-object p1, v1, v3

    invoke-virtual {p0, p2, p6, v0, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 4492
    .line 4493
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v7

    .line 4494
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    aput-object p4, v2, v6

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    move v0, v6

    .line 4497
    :goto_0
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4498
    const/4 v2, 0x0

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-virtual {p5, v7, v2, v3}, Lcom/google/android/gms/games/provider/h;->a(Lcom/google/android/gms/games/provider/ay;J)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    add-int/2addr v0, v2

    goto :goto_0

    .line 4501
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 4503
    return v0

    .line 4501
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/games/provider/bg;JLandroid/content/ContentValues;)I
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 4993
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 4994
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "No image ID specified for image data update"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4996
    :cond_0
    const-string v0, "image_data"

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 4997
    const-string v0, "image_data"

    invoke-virtual {p4, v0}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v3

    .line 4998
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    move-object v0, p1

    move-wide v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/bg;->a(J[BJ)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4999
    const/4 v0, 0x1

    .line 5005
    :goto_0
    return v0

    .line 5001
    :cond_1
    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to update image data for image ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2, p3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 5002
    goto :goto_0

    :cond_2
    move v0, v6

    .line 5005
    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J
    .locals 4

    .prologue
    .line 4612
    const-wide/16 v0, -0x1

    .line 4613
    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v2

    .line 4614
    sget-object v3, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v2}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v2

    aget v2, v3, v2

    sparse-switch v2, :sswitch_data_0

    .line 4683
    :goto_0
    return-wide v0

    .line 4616
    :sswitch_0
    const-string v0, "games"

    const-string v1, "game_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4621
    :sswitch_1
    const-string v0, "external_game_id=?"

    .line 4622
    const-string v1, "games"

    const-string v2, "game_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1, v1, v2, v3, v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4627
    :sswitch_2
    const-string v0, "games"

    const-string v1, "game_hi_res_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4632
    :sswitch_3
    const-string v0, "games"

    const-string v1, "featured_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4637
    :sswitch_4
    const-string v0, "game_badges"

    const-string v1, "badge_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4642
    :sswitch_5
    const-string v0, "players"

    const-string v1, "profile_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4647
    :sswitch_6
    const-string v0, "players"

    const-string v1, "profile_hi_res_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4652
    :sswitch_7
    const-string v0, "quests"

    const-string v1, "quest_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto :goto_0

    .line 4657
    :sswitch_8
    const-string v0, "quests"

    const-string v1, "quest_banner_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 4662
    :sswitch_9
    const-string v0, "achievement_definitions"

    const-string v1, "unlocked_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 4668
    :sswitch_a
    const-string v0, "achievement_definitions"

    const-string v1, "revealed_icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 4675
    :sswitch_b
    const-string v0, "event_definitions"

    const-string v1, "icon_image_id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 4682
    :sswitch_c
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    goto/16 :goto_0

    .line 4614
    :sswitch_data_0
    .sparse-switch
        0x3 -> :sswitch_0
        0x4 -> :sswitch_1
        0x5 -> :sswitch_2
        0x6 -> :sswitch_3
        0x46 -> :sswitch_c
        0x87 -> :sswitch_4
        0x88 -> :sswitch_5
        0x89 -> :sswitch_6
        0x8a -> :sswitch_7
        0x8b -> :sswitch_8
        0x8c -> :sswitch_9
        0x8d -> :sswitch_a
        0x8e -> :sswitch_b
    .end sparse-switch
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J
    .locals 10

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    const/4 v6, 0x0

    .line 4849
    const-wide/16 v8, -0x1

    .line 4850
    const-string v1, "images"

    new-array v2, v4, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v6

    const-string v3, "url=?"

    new-array v4, v4, [Ljava/lang/String;

    aput-object p1, v4, v6

    move-object v0, p0

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4853
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 4854
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v0

    .line 4857
    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 4859
    return-wide v0

    .line 4857
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    move-wide v0, v8

    goto :goto_0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 4704
    const-string v0, "_id=?"

    .line 4705
    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v5, 0x0

    const/4 v0, 0x0

    .line 4721
    new-array v2, v1, [Ljava/lang/String;

    aput-object p2, v2, v0

    new-array v4, v1, [Ljava/lang/String;

    aput-object p3, v4, v0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p4

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 4724
    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 4725
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Landroid/database/Cursor;->isNull(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_0

    .line 4726
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    const-wide/16 v0, -0x1

    .line 4735
    :goto_0
    return-wide v0

    .line 4728
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-interface {v2, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    .line 4735
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 4731
    :cond_1
    :try_start_2
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No image found for non-existent "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " row with ID "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4735
    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private static a(Lcom/google/android/gms/games/provider/bg;J)Landroid/content/res/AssetFileDescriptor;
    .locals 7

    .prologue
    .line 4749
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/provider/bg;->a(J)Lcom/google/android/gms/games/provider/bh;

    move-result-object v4

    .line 4750
    if-eqz v4, :cond_0

    .line 4752
    :try_start_0
    new-instance v0, Landroid/content/res/AssetFileDescriptor;

    new-instance v1, Ljava/io/File;

    iget-object v2, v4, Lcom/google/android/gms/games/provider/bh;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    const/high16 v2, 0x10000000

    invoke-static {v1, v2}, Landroid/os/ParcelFileDescriptor;->open(Ljava/io/File;I)Landroid/os/ParcelFileDescriptor;

    move-result-object v1

    const-wide/16 v2, 0x0

    iget-wide v4, v4, Lcom/google/android/gms/games/provider/bh;->b:J

    invoke-direct/range {v0 .. v5}, Landroid/content/res/AssetFileDescriptor;-><init>(Landroid/os/ParcelFileDescriptor;JJ)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 4756
    :catch_0
    move-exception v0

    throw v0

    .line 4765
    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image found for ID "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/google/android/gms/games/provider/ay;)Lcom/google/android/gms/games/provider/j;
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 1739
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/games/provider/ay;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1740
    const/4 v0, 0x0

    .line 1774
    :goto_0
    return-object v0

    .line 1743
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/provider/j;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/provider/j;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Lcom/google/android/gms/games/provider/ay;)V

    .line 1746
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    const-string v2, "cover_photo_image_id"

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 1748
    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 1749
    iget-object v1, v0, Lcom/google/android/gms/games/provider/j;->c:Ljava/util/HashSet;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 1753
    :cond_1
    const-string v1, "games"

    const-string v2, "game_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1754
    const-string v1, "games"

    const-string v2, "game_hi_res_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1755
    const-string v1, "games"

    const-string v2, "featured_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1756
    const-string v1, "game_badges"

    const-string v2, "badge_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1757
    const-string v1, "players"

    const-string v2, "profile_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1758
    const-string v1, "players"

    const-string v2, "profile_hi_res_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1759
    const-string v1, "event_definitions"

    const-string v2, "icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1760
    const-string v1, "achievement_definitions"

    const-string v2, "unlocked_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1762
    const-string v1, "achievement_definitions"

    const-string v2, "revealed_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1764
    const-string v1, "experience_events"

    const-string v2, "icon_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1765
    const-string v1, "leaderboards"

    const-string v2, "board_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1766
    const-string v1, "leaderboard_scores"

    const-string v2, "default_display_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1768
    const-string v1, "notifications"

    const-string v2, "image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1769
    const-string v1, "participants"

    const-string v2, "default_display_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1770
    const-string v1, "participants"

    const-string v2, "default_display_hi_res_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1771
    const-string v1, "quests"

    const-string v2, "quest_banner_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1772
    const-string v1, "quests"

    const-string v2, "quest_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1773
    const-string v1, "snapshots"

    const-string v2, "cover_icon_image_id"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/j;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 1593
    const-string v0, "(SELECT url FROM images innerimage WHERE innerimage._id=%s.%s LIMIT 1)"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private a(ILjava/lang/Object;J)V
    .locals 3

    .prologue
    .line 1666
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v1, v2, p2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1, p3, p4}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 1667
    return-void
.end method

.method private static a(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 4905
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    .line 4906
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-wide/16 v2, -0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    .line 4907
    const-string v0, "GamesContentProvider"

    const-string v1, "Attempting to insert an invalid image ID"

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 4908
    invoke-virtual {p0, p1}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 4910
    :cond_0
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5310
    const-string v0, "player_levels"

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->L:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5311
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5294
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->I:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "games._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/y;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "badge_icon_image_uri"

    const-string v3, "badge_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "availability"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "owned"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "achievement_unlocked_count"

    const-string v3, "(SELECT COUNT(1) FROM achievement_instances, achievement_definitions, games AS SubGame WHERE SubGame._id=games._id AND games._id=achievement_definitions.game_id AND achievement_definitions._id=achievement_instances.definition_id AND 0=achievement_instances.state)"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "price_micros"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "formatted_price"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "full_price_micros"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "formatted_full_price"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5295
    return-void
.end method

.method private static a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V
    .locals 0

    .prologue
    .line 5452
    invoke-virtual {p0, p1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 5453
    invoke-virtual {p0, p2}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 5454
    return-void
.end method

.method private a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4913
    const-string v0, "profile_icon_image_url"

    const-string v1, "profile_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4915
    const-string v0, "profile_hi_res_image_url"

    const-string v1, "profile_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4917
    const-string v0, "most_recent_game_icon_url"

    const-string v1, "most_recent_game_icon_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4919
    const-string v0, "most_recent_game_hi_res_url"

    const-string v1, "most_recent_game_hi_res_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4921
    const-string v0, "most_recent_game_featured_url"

    const-string v1, "most_recent_game_featured_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4923
    return-void
.end method

.method private a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 4812
    iget-object v0, p1, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    .line 4813
    iget-object v1, p1, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v1}, Lcom/google/android/gms/games/provider/az;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 4815
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 4816
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 4817
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 4818
    invoke-virtual {p2, p4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 4834
    :goto_0
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 4838
    :cond_0
    invoke-static {p2, p4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 4839
    return-void

    .line 4820
    :cond_1
    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)J

    move-result-wide v4

    .line 4821
    const-wide/16 v6, -0x1

    cmp-long v1, v4, v6

    if-eqz v1, :cond_2

    .line 4823
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 4826
    :cond_2
    const/4 v1, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/games/provider/bg;->a([BLjava/lang/String;J)J

    move-result-wide v0

    .line 4827
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    .line 4828
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_0

    .line 4830
    :cond_3
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Unable to store image."

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method private a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 4783
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 4784
    invoke-virtual {p2, p4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 4785
    iget-object v0, p1, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->getAsByteArray(Ljava/lang/String;)[B

    move-result-object v1

    array-length v2, v1

    if-nez v2, :cond_2

    invoke-virtual {p2, p5}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :goto_1
    invoke-virtual {p2, p3}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    :cond_0
    invoke-static {p2, p5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/content/ContentValues;Ljava/lang/String;)V

    .line 4789
    :goto_2
    return-void

    .line 4784
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 4785
    :cond_2
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->e:Lcom/google/android/gms/common/util/p;

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v0, v1, v2, v4, v5}, Lcom/google/android/gms/games/provider/bg;->a([BLjava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-eqz v2, :cond_3

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p2, p5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    :cond_3
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    const-string v1, "Unable to store image."

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4787
    :cond_4
    invoke-direct {p0, p1, p2, p4, p5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method private declared-synchronized a(Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 1690
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/ay;

    .line 1691
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/provider/ay;->b(Landroid/content/Context;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_0

    .line 1700
    :goto_0
    monitor-exit p0

    return-void

    .line 1696
    :cond_0
    :try_start_1
    iget v0, v0, Lcom/google/android/gms/games/provider/ay;->f:I

    const/4 v2, 0x3

    if-ge v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_2

    .line 1697
    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Retrying data store creation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1698
    const/4 v0, 0x0

    const-wide/16 v2, 0xc8

    invoke-direct {p0, v0, p1, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(ILjava/lang/Object;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1690
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move v0, v1

    .line 1696
    goto :goto_1

    .line 1704
    :cond_2
    :try_start_2
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to initialize data store "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/provider/GamesContentProvider;Lcom/google/android/gms/games/provider/ay;Ljava/lang/String;J[Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 89
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Ljava/lang/String;J[Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method private varargs a(Lcom/google/android/gms/games/provider/ay;Ljava/lang/String;J[Ljava/lang/String;)Z
    .locals 13

    .prologue
    const/4 v11, 0x1

    const/4 v7, 0x0

    const/4 v10, 0x0

    .line 4521
    iget-object v2, p1, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v2}, Lcom/google/android/gms/games/provider/az;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 4522
    const-string v5, "_id=?"

    new-array v6, v11, [Ljava/lang/String;

    invoke-static/range {p3 .. p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v6, v10

    move-object v3, p2

    move-object/from16 v4, p5

    move-object v8, v7

    move-object v9, v7

    invoke-virtual/range {v2 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 4525
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->getCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 4526
    const-string v2, "GamesContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Attempting to delete a row from "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " that doesn\'t exist.  ID: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-wide/from16 v0, p3

    invoke-virtual {v4, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 4528
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v2, v10

    .line 4540
    :goto_0
    return v2

    .line 4530
    :cond_0
    :try_start_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    .line 4531
    :goto_1
    move-object/from16 v0, p5

    array-length v2, v0

    if-ge v10, v2, :cond_3

    .line 4532
    invoke-interface {v3, v10}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    .line 4533
    iget-object v4, p1, Lcom/google/android/gms/games/provider/ay;->a:Ljava/lang/String;

    invoke-interface {v3, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->h:Ljava/lang/Object;

    monitor-enter v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    invoke-interface {v2, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Set;

    if-nez v2, :cond_1

    new-instance v2, Ljava/util/HashSet;

    invoke-direct {v2}, Ljava/util/HashSet;-><init>()V

    iget-object v8, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    invoke-interface {v8, v4, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_1
    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 4531
    :cond_2
    add-int/lit8 v10, v10, 0x1

    goto :goto_1

    .line 4533
    :catchall_0
    move-exception v2

    :try_start_3
    monitor-exit v5

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 4538
    :catchall_1
    move-exception v2

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_3
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    move v2, v11

    .line 4540
    goto :goto_0
.end method

.method private static b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 1584
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "\'"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Lcom/google/android/gms/games/provider/ad;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/\'||"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    .line 1589
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private declared-synchronized b()V
    .locals 6

    .prologue
    .line 1712
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    if-nez v0, :cond_1

    .line 1713
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "GamesContentProvider"

    const-string v2, "No data stores when running cleanAllImageStores() background task"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1726
    :cond_0
    monitor-exit p0

    return-void

    .line 1717
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1718
    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/provider/ay;

    .line 1719
    if-nez v1, :cond_3

    .line 1720
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v3, "GamesContentProvider"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Null data store found for store name "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v3, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 1712
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 1724
    :cond_3
    :try_start_2
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;)Lcom/google/android/gms/games/provider/j;

    move-result-object v0

    if-eqz v0, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/j;->a()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static b(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5321
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->O:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->N:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5323
    return-void
.end method

.method private static b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5298
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->J:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "game_badges._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/y;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "badge_icon_image_uri"

    const-string v3, "badge_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5299
    return-void
.end method

.method private b(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 4926
    const-string v3, "icon_image_bytes"

    const-string v4, "game_icon_image_url"

    const-string v5, "game_icon_image_id"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4928
    const-string v0, "game_hi_res_image_url"

    const-string v1, "game_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4929
    const-string v0, "featured_image_url"

    const-string v1, "featured_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4930
    return-void
.end method

.method private static c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 359
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->a:Landroid/content/UriMatcher;

    invoke-virtual {v0, p0}, Landroid/content/UriMatcher;->match(Landroid/net/Uri;)I

    move-result v3

    .line 360
    if-ltz v3, :cond_0

    move v0, v1

    :goto_0
    const-string v4, "Unrecognized URI: %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p0, v1, v2

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 361
    const-class v0, Lcom/google/android/gms/games/provider/k;

    invoke-virtual {v0}, Ljava/lang/Class;->getEnumConstants()[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/games/provider/k;

    aget-object v0, v0, v3

    return-object v0

    :cond_0
    move v0, v2

    .line 360
    goto :goto_0
.end method

.method private static c(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5326
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Q:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->P:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5328
    return-void
.end method

.method private static c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5302
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->K:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "game_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/z;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5303
    return-void
.end method

.method private c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 4933
    const-string v3, "badge_icon_image_bytes"

    const-string v4, "badge_icon_image_url"

    const-string v5, "badge_icon_image_id"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4935
    return-void
.end method

.method private declared-synchronized d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;
    .locals 6

    .prologue
    .line 1638
    monitor-enter p0

    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/ar;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 1639
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/ay;

    .line 1640
    if-nez v0, :cond_0

    .line 1641
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v2

    new-instance v0, Lcom/google/android/gms/games/provider/ay;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/games/provider/ay;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 1642
    iget-object v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    iget-object v3, v0, Lcom/google/android/gms/games/provider/ay;->a:Ljava/lang/String;

    invoke-virtual {v2, v3, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1643
    const/4 v2, 0x0

    const-wide/16 v4, 0x0

    invoke-direct {p0, v2, v1, v4, v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(ILjava/lang/Object;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1645
    :cond_0
    monitor-exit p0

    return-object v0

    .line 1638
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private static d(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5336
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->R:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->T:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5337
    return-void
.end method

.method private static d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5306
    const-string v0, "players"

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5307
    return-void
.end method

.method private d(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4938
    const-string v0, "unlocked_icon_image_url"

    const-string v1, "unlocked_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4940
    const-string v0, "revealed_icon_image_url"

    const-string v1, "revealed_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4942
    return-void
.end method

.method private static e(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5344
    const-string v0, "client_contexts"

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->V:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5345
    return-void
.end method

.method private static e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5315
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->M:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "achievement_definitions._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/n;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "unlocked_icon_image_uri"

    const-string v3, "unlocked_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "unlocked_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "unlocked_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "revealed_icon_image_uri"

    const-string v3, "revealed_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "revealed_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "revealed_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/z;->b:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5317
    return-void
.end method

.method private e(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4945
    const-string v0, "icon_url"

    const-string v1, "icon_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4947
    return-void
.end method

.method private static f(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 4

    .prologue
    .line 5359
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->Y:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_definitions._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_pending_ops._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_event_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/w;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/t;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_game_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_player_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5361
    return-void
.end method

.method private static f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5331
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->S:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "achievement_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/o;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/n;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "unlocked_icon_image_uri"

    const-string v3, "unlocked_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "unlocked_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "unlocked_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "revealed_icon_image_uri"

    const-string v3, "revealed_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "revealed_icon_image_url"

    const-string v3, "achievement_definitions"

    const-string v4, "revealed_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5333
    return-void
.end method

.method private f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4950
    const-string v0, "board_icon_image_url"

    const-string v1, "board_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4952
    return-void
.end method

.method private static g(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5364
    const-string v0, "images"

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5365
    return-void
.end method

.method private static g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5349
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->W:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_definitions._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/u;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/z;->b:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_image_uri"

    const-string v3, "icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_image_url"

    const-string v3, "event_definitions"

    const-string v4, "icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5351
    return-void
.end method

.method private g(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4955
    const-string v0, "icon_image_url"

    const-string v1, "icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4957
    return-void
.end method

.method private static h(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5368
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aa:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5369
    return-void
.end method

.method private static h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5354
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->X:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/v;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/u;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "value"

    const-string v3, "(   SELECT sum( total )    FROM (           SELECT ei2.value AS total            FROM event_instances AS ei2           WHERE ei2._id = event_instances._id          UNION ALL          SELECT sum( event_pending_ops.increment ) AS total            FROM event_pending_ops           WHERE event_pending_ops.instance_id = event_instances._id      )     )"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_image_uri"

    const-string v3, "icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_image_url"

    const-string v3, "event_definitions"

    const-string v4, "icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5356
    return-void
.end method

.method private h(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4960
    const-string v0, "quest_icon_image_url"

    const-string v1, "quest_icon_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4961
    const-string v0, "quest_banner_image_url"

    const-string v1, "quest_banner_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4962
    return-void
.end method

.method private static i(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5403
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->an:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->am:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5404
    return-void
.end method

.method private static i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5380
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->av:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "invitations._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ae;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_invitation"

    const-string v3, "(SELECT MAX(last_modified_timestamp) FROM invitations innerinvite WHERE innerinvite.external_inviter_id=invitations.external_inviter_id)"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ao;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_player_id"

    const-string v3, "ParticipantPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_name"

    const-string v3, "ParticipantPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_id"

    const-string v3, "ParticipantPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_id"

    const-string v3, "ParticipantPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "last_updated"

    const-string v3, "ParticipantPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "is_in_circles"

    const-string v3, "ParticipantPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_xp_total"

    const-string v3, "ParticipantPlayer.current_xp_total"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level"

    const-string v3, "ParticipantPlayer.current_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level_min_xp"

    const-string v3, "ParticipantPlayer.current_level_min_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level_max_xp"

    const-string v3, "ParticipantPlayer.current_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "next_level"

    const-string v3, "ParticipantPlayer.next_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "next_level_max_xp"

    const-string v3, "ParticipantPlayer.next_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "last_level_up_timestamp"

    const-string v3, "ParticipantPlayer.last_level_up_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "player_title"

    const-string v3, "ParticipantPlayer.player_title"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "has_all_public_acls"

    const-string v3, "ParticipantPlayer.has_all_public_acls"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "is_profile_visible"

    const-string v3, "ParticipantPlayer.is_profile_visible"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_external_game_id"

    const-string v3, "ParticipantPlayer.most_recent_external_game_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_name"

    const-string v3, "ParticipantPlayer.most_recent_game_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_activity_timestamp"

    const-string v3, "ParticipantPlayer.most_recent_activity_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_id"

    const-string v3, "ParticipantPlayer.most_recent_game_icon_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_id"

    const-string v3, "ParticipantPlayer.most_recent_game_hi_res_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_id"

    const-string v3, "ParticipantPlayer.most_recent_game_featured_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5382
    return-void
.end method

.method private i(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4965
    const-string v0, "default_display_image_url"

    const-string v1, "default_display_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4967
    return-void
.end method

.method private static j(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5416
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ar:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->aq:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5417
    return-void
.end method

.method private static j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5385
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ad:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "experience_events._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/x;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_uri"

    const-string v3, "icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "icon_url"

    const-string v3, "experience_events"

    const-string v4, "icon_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/z;->b:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5386
    return-void
.end method

.method private j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 2

    .prologue
    .line 4970
    const-string v0, "default_display_image_url"

    const-string v1, "default_display_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4972
    const-string v0, "default_display_hi_res_image_url"

    const-string v1, "default_display_hi_res_image_id"

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 4974
    return-void
.end method

.method private static k(Landroid/database/sqlite/SQLiteQueryBuilder;)V
    .locals 2

    .prologue
    .line 5420
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->at:Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->as:Lcom/android/a/a/a;

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5421
    return-void
.end method

.method private static k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5389
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ae:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboards._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ai;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "board_icon_image_uri"

    const-string v3, "board_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "board_icon_image_url"

    const-string v3, "leaderboards"

    const-string v4, "board_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5390
    return-void
.end method

.method private k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V
    .locals 6

    .prologue
    .line 4977
    const-string v3, "cover_icon_image_bytes"

    const-string v4, "cover_icon_image_url"

    const-string v5, "cover_icon_image_id"

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 4979
    return-void
.end method

.method private static l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5393
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->af:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboard_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/af;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ai;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "board_icon_image_uri"

    const-string v3, "board_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "board_icon_image_url"

    const-string v3, "leaderboards"

    const-string v4, "board_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/z;->b:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5395
    return-void
.end method

.method private static m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5398
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ag:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "leaderboard_scores._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ah;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "leaderboard_scores"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5400
    return-void
.end method

.method private static n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5407
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ap:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "request_recipients._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/av;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5409
    return-void
.end method

.method private static o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5412
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ao:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/av;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aw;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "next_expiring_request"

    const-string v3, "(SELECT MIN(expiration_timestamp) FROM requests innerrequests WHERE innerrequests.sender_id=requests.sender_id)"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_external_player_id"

    const-string v3, "RequestSenderPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_name"

    const-string v3, "RequestSenderPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_id"

    const-string v3, "RequestSenderPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_url"

    const-string v3, "RequestSenderPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_id"

    const-string v3, "RequestSenderPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_url"

    const-string v3, "RequestSenderPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_last_updated"

    const-string v3, "RequestSenderPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_is_in_circles"

    const-string v3, "RequestSenderPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_icon_image_uri"

    const-string v3, "RequestSenderPlayer.profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_profile_hi_res_image_uri"

    const-string v3, "RequestSenderPlayer.profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_current_xp_total"

    const-string v3, "RequestSenderPlayer.current_xp_total"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_current_level"

    const-string v3, "RequestSenderPlayer.current_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_current_level_min_xp"

    const-string v3, "RequestSenderPlayer.current_level_min_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_current_level_max_xp"

    const-string v3, "RequestSenderPlayer.current_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_next_level"

    const-string v3, "RequestSenderPlayer.next_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_next_level_max_xp"

    const-string v3, "RequestSenderPlayer.next_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_last_level_up_timestamp"

    const-string v3, "RequestSenderPlayer.last_level_up_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_player_title"

    const-string v3, "RequestSenderPlayer.player_title"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_has_all_public_acls"

    const-string v3, "RequestSenderPlayer.has_all_public_acls"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_is_profile_visible"

    const-string v3, "RequestSenderPlayer.is_profile_visible"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_external_game_id"

    const-string v3, "RequestSenderPlayer.most_recent_external_game_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_name"

    const-string v3, "RequestSenderPlayer.most_recent_game_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_activity_timestamp"

    const-string v3, "RequestSenderPlayer.most_recent_activity_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_icon_id"

    const-string v3, "RequestSenderPlayer.most_recent_game_icon_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_icon_uri"

    const-string v3, "RequestSenderPlayer.most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_hi_res_id"

    const-string v3, "RequestSenderPlayer.most_recent_game_hi_res_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_hi_res_uri"

    const-string v3, "RequestSenderPlayer.most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_featured_id"

    const-string v3, "RequestSenderPlayer.most_recent_game_featured_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "sender_most_recent_game_featured_uri"

    const-string v3, "RequestSenderPlayer.most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_external_player_id"

    const-string v3, "RequestRecipientPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_name"

    const-string v3, "RequestRecipientPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_id"

    const-string v3, "RequestRecipientPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_url"

    const-string v3, "RequestRecipientPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_url"

    const-string v3, "RequestRecipientPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_id"

    const-string v3, "RequestRecipientPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_last_updated"

    const-string v3, "RequestRecipientPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_is_in_circles"

    const-string v3, "RequestRecipientPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_icon_image_uri"

    const-string v3, "RequestRecipientPlayer.profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_profile_hi_res_image_uri"

    const-string v3, "RequestRecipientPlayer.profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_current_xp_total"

    const-string v3, "RequestRecipientPlayer.current_xp_total"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_current_level"

    const-string v3, "RequestRecipientPlayer.current_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_current_level_min_xp"

    const-string v3, "RequestRecipientPlayer.current_level_min_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_current_level_max_xp"

    const-string v3, "RequestRecipientPlayer.current_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_next_level"

    const-string v3, "RequestRecipientPlayer.next_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_next_level_max_xp"

    const-string v3, "RequestRecipientPlayer.next_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_last_level_up_timestamp"

    const-string v3, "RequestRecipientPlayer.last_level_up_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_player_title"

    const-string v3, "RequestRecipientPlayer.player_title"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_has_all_public_acls"

    const-string v3, "RequestRecipientPlayer.has_all_public_acls"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_is_profile_visible"

    const-string v3, "RequestRecipientPlayer.is_profile_visible"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_external_game_id"

    const-string v3, "RequestRecipientPlayer.most_recent_external_game_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_name"

    const-string v3, "RequestRecipientPlayer.most_recent_game_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_activity_timestamp"

    const-string v3, "RequestRecipientPlayer.most_recent_activity_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_icon_id"

    const-string v3, "RequestRecipientPlayer.most_recent_game_icon_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_icon_uri"

    const-string v3, "RequestRecipientPlayer.most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_hi_res_id"

    const-string v3, "RequestRecipientPlayer.most_recent_game_hi_res_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_hi_res_uri"

    const-string v3, "RequestRecipientPlayer.most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_featured_id"

    const-string v3, "RequestRecipientPlayer.most_recent_game_featured_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "recipient_most_recent_game_featured_uri"

    const-string v3, "RequestRecipientPlayer.most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5413
    return-void
.end method

.method private static p(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5424
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->au:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ao;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ak;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "target_instance"

    const-string v3, "TargetInstance._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_player_id"

    const-string v3, "ParticipantPlayer.external_player_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_name"

    const-string v3, "ParticipantPlayer.profile_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_id"

    const-string v3, "ParticipantPlayer.profile_icon_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_id"

    const-string v3, "ParticipantPlayer.profile_hi_res_image_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "ParticipantPlayer"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "last_updated"

    const-string v3, "ParticipantPlayer.last_updated"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "is_in_circles"

    const-string v3, "ParticipantPlayer.is_in_circles"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_xp_total"

    const-string v3, "ParticipantPlayer.current_xp_total"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level"

    const-string v3, "ParticipantPlayer.current_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level_min_xp"

    const-string v3, "ParticipantPlayer.current_level_min_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_level_max_xp"

    const-string v3, "ParticipantPlayer.current_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "next_level"

    const-string v3, "ParticipantPlayer.next_level"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "next_level_max_xp"

    const-string v3, "ParticipantPlayer.next_level_max_xp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "last_level_up_timestamp"

    const-string v3, "ParticipantPlayer.last_level_up_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "player_title"

    const-string v3, "ParticipantPlayer.player_title"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "has_all_public_acls"

    const-string v3, "ParticipantPlayer.has_all_public_acls"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "is_profile_visible"

    const-string v3, "ParticipantPlayer.is_profile_visible"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_external_game_id"

    const-string v3, "ParticipantPlayer.most_recent_external_game_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_name"

    const-string v3, "ParticipantPlayer.most_recent_game_name"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_activity_timestamp"

    const-string v3, "ParticipantPlayer.most_recent_activity_timestamp"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_id"

    const-string v3, "ParticipantPlayer.most_recent_game_icon_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_id"

    const-string v3, "ParticipantPlayer.most_recent_game_hi_res_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_id"

    const-string v3, "ParticipantPlayer.most_recent_game_featured_id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5425
    return-void
.end method

.method private static q(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 4

    .prologue
    .line 5428
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aw:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "notifications._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/an;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "image_uri"

    const-string v3, "image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_game_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5429
    return-void
.end method

.method private static r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5432
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ax:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "participants._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ao;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_uri"

    const-string v3, "default_display_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_uri"

    const-string v3, "default_display_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "default_display_hi_res_image_url"

    const-string v3, "participants"

    const-string v4, "default_display_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5433
    return-void
.end method

.method private static s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5436
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ak:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "milestones._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "event_instances._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_value"

    const-string v3, "(   SELECT sum( total )    FROM (           SELECT ei2.value AS total            FROM event_instances AS ei2           WHERE ei2._id = event_instances._id          UNION ALL          SELECT sum( event_pending_ops.increment ) AS total            FROM event_pending_ops           WHERE event_pending_ops.instance_id = event_instances._id      )     )"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/am;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ar;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_uri"

    const-string v3, "quest_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_url"

    const-string v3, "quests"

    const-string v4, "quest_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_uri"

    const-string v3, "quest_banner_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_url"

    const-string v3, "quests"

    const-string v4, "quest_banner_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5437
    return-void
.end method

.method private static t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5440
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->aj:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "quests._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ar;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_uri"

    const-string v3, "quest_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_url"

    const-string v3, "quests"

    const-string v4, "quest_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_uri"

    const-string v3, "quest_banner_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_url"

    const-string v3, "quests"

    const-string v4, "quest_banner_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5441
    return-void
.end method

.method private static u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5444
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->al:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "quests._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ar;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "external_event_id"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "milestones._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "current_value"

    const-string v3, "(   SELECT sum( total )    FROM (           SELECT ei2.value AS total            FROM event_instances AS ei2           WHERE ei2._id = event_instances._id          UNION ALL          SELECT sum( event_pending_ops.increment ) AS total            FROM event_pending_ops           WHERE event_pending_ops.instance_id = event_instances._id      )     )"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/am;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_uri"

    const-string v3, "quest_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_icon_image_url"

    const-string v3, "quests"

    const-string v4, "quest_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_uri"

    const-string v3, "quest_banner_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "quest_banner_image_url"

    const-string v3, "quests"

    const-string v4, "quest_banner_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5445
    return-void
.end method

.method private static v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V
    .locals 5

    .prologue
    .line 5448
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->ay:Ljava/lang/String;

    invoke-static {}, Lcom/android/a/a/a;->a()Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "_id"

    const-string v3, "snapshots._id"

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ax;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "cover_icon_image_uri"

    const-string v3, "cover_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "cover_icon_image_url"

    const-string v3, "snapshots"

    const-string v4, "cover_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/ab;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_uri"

    const-string v3, "game_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_icon_image_url"

    const-string v3, "games"

    const-string v4, "game_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_uri"

    const-string v3, "game_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "game_hi_res_image_url"

    const-string v3, "games"

    const-string v4, "game_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_uri"

    const-string v3, "featured_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "featured_image_url"

    const-string v3, "games"

    const-string v4, "featured_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "installed"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "package_name"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "real_time_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "turn_based_support"

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a(Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/aq;->a:[Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/android/a/a/b;->a([Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_uri"

    const-string v3, "profile_icon_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_icon_image_url"

    const-string v3, "players"

    const-string v4, "profile_icon_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_uri"

    const-string v3, "profile_hi_res_image_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "profile_hi_res_image_url"

    const-string v3, "players"

    const-string v4, "profile_hi_res_image_id"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_icon_uri"

    const-string v3, "most_recent_game_icon_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_hi_res_uri"

    const-string v3, "most_recent_game_hi_res_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    const-string v2, "most_recent_game_featured_uri"

    const-string v3, "most_recent_game_featured_id"

    invoke-static {p1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/android/a/a/b;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/android/a/a/b;

    move-result-object v1

    invoke-virtual {v1}, Lcom/android/a/a/b;->a()Lcom/android/a/a/a;

    move-result-object v1

    invoke-static {p0, v0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    .line 5449
    return-void
.end method


# virtual methods
.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 8

    .prologue
    const/4 v2, 0x1

    .line 3176
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v0

    .line 3177
    iget-object v1, v0, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ay;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 3178
    iget-object v1, v0, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    .line 3181
    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v3

    .line 3182
    sget-object v4, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v3}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 3816
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid update URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3184
    :pswitch_1
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3185
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3186
    const-string v0, "games"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 3818
    :goto_0
    return v0

    .line 3191
    :pswitch_2
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3192
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3193
    const-string v0, "games"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3198
    :pswitch_3
    const-string v1, "external_game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3199
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3200
    const-string v0, "games"

    const-string v1, "external_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3205
    :pswitch_4
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3206
    const-string v0, "game_badges"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3211
    :pswitch_5
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3212
    const-string v0, "game_badges"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3217
    :pswitch_6
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3218
    const-string v0, "game_badges"

    const-string v1, "badge_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3223
    :pswitch_7
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3224
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, "game_badges"

    const-string v3, "badge_game_id"

    const-string v4, "games"

    const-string v5, "external_game_id"

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3231
    :pswitch_8
    const-string v0, "game_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3236
    :pswitch_9
    const-string v0, "game_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3241
    :pswitch_a
    const-string v0, "game_instances"

    const-string v1, "instance_game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto :goto_0

    .line 3246
    :pswitch_b
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, "game_instances"

    const-string v3, "instance_game_id"

    const-string v4, "games"

    const-string v5, "external_game_id"

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3253
    :pswitch_c
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3254
    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3255
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3256
    const-string v0, "players"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3261
    :pswitch_d
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3262
    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3263
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3264
    const-string v0, "players"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3269
    :pswitch_e
    const-string v0, "level_value"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3270
    const-string v0, "player_levels"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3275
    :pswitch_f
    const-string v0, "level_value"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3276
    const-string v0, "player_levels"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3281
    :pswitch_10
    const-string v0, "level_value"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3282
    const-string v0, "player_levels"

    const-string v1, "level_value"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3287
    :pswitch_11
    const-string v1, "external_player_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3288
    const-string v1, "profile_name"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3289
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3290
    const-string v0, "players"

    const-string v1, "external_player_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3297
    :pswitch_12
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 3298
    invoke-virtual {p3}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v0

    .line 3299
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 3300
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 3301
    sget-object v5, Lcom/google/android/gms/games/provider/GamesContentProvider;->az:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v5, v1}, Lcom/google/android/gms/games/provider/a;->a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;

    move-result-object v5

    .line 3302
    sget-object v6, Lcom/google/android/gms/games/provider/g;->a:[I

    invoke-virtual {v5}, Lcom/google/android/gms/games/provider/d;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_1

    .line 3310
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3304
    :pswitch_13
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 3307
    :pswitch_14
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v3, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_1

    .line 3312
    :cond_0
    invoke-static {v3}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    move v0, v2

    .line 3315
    goto/16 :goto_0

    .line 3319
    :pswitch_15
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3321
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3322
    const-string v0, "achievement_definitions"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3328
    :pswitch_16
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3330
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3331
    const-string v0, "achievement_definitions"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3337
    :pswitch_17
    const-string v1, "external_achievement_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3339
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3340
    const-string v0, "achievement_definitions"

    const-string v1, "external_achievement_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3346
    :pswitch_18
    const-string v0, "achievement_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3351
    :pswitch_19
    const-string v0, "achievement_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3357
    :pswitch_1a
    const-string v0, "achievement_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3362
    :pswitch_1b
    const-string v0, "achievement_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3368
    :pswitch_1c
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3369
    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3370
    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3371
    const-string v0, "client_contexts"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3376
    :pswitch_1d
    const-string v0, "package_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3377
    const-string v0, "package_uid"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3378
    const-string v0, "account_name"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3379
    const-string v0, "client_contexts"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3384
    :pswitch_1e
    const-string v1, "external_event_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3386
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3387
    const-string v0, "event_definitions"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3393
    :pswitch_1f
    const-string v1, "external_event_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3395
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3396
    const-string v0, "event_definitions"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3402
    :pswitch_20
    const-string v1, "external_event_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3404
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3405
    const-string v0, "event_definitions"

    const-string v1, "external_event_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3411
    :pswitch_21
    const-string v0, "event_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3416
    :pswitch_22
    const-string v0, "event_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3422
    :pswitch_23
    const-string v0, "event_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3427
    :pswitch_24
    const-string v0, "event_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3433
    :pswitch_25
    const-string v1, "external_experience_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3435
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3436
    const-string v0, "experience_events"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3441
    :pswitch_26
    const-string v1, "external_experience_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3443
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3444
    const-string v0, "experience_events"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3449
    :pswitch_27
    const-string v1, "external_experience_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3451
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3452
    const-string v0, "experience_events"

    const-string v1, "external_experience_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3458
    :pswitch_28
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3459
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3460
    const-string v0, "leaderboards"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3465
    :pswitch_29
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3466
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3467
    const-string v0, "leaderboards"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3472
    :pswitch_2a
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3473
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3474
    const-string v0, "leaderboards"

    const-string v1, "external_leaderboard_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3480
    :pswitch_2b
    const-string v1, "external_leaderboard_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3481
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3482
    const-string v0, "leaderboards"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3487
    :pswitch_2c
    const-string v0, "leaderboard_instances"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3492
    :pswitch_2d
    const-string v0, "leaderboard_instances"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3498
    :pswitch_2e
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3499
    const-string v0, "leaderboard_scores"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3504
    :pswitch_2f
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3505
    const-string v0, "leaderboard_scores"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3511
    :pswitch_30
    const-string v0, "leaderboard_pending_scores"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3517
    :pswitch_31
    const-string v0, "_id"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/bg;JLandroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3522
    :pswitch_32
    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-direct {p0, v1, v2, v3, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/bg;JLandroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3527
    :pswitch_33
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3528
    const-string v0, "invitations"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3533
    :pswitch_34
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3534
    const-string v0, "invitations"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3539
    :pswitch_35
    const-string v0, "external_invitation_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3540
    const-string v0, "invitations"

    const-string v1, "external_invitation_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3546
    :pswitch_36
    const-string v0, "request_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3552
    :pswitch_37
    const-string v0, "request_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3558
    :pswitch_38
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3560
    const-string v0, "request_pending_ops"

    const-string v1, "external_request_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3566
    :pswitch_39
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3567
    const-string v0, "requests"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3572
    :pswitch_3a
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3573
    const-string v0, "requests"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3578
    :pswitch_3b
    const-string v0, "external_request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3579
    const-string v0, "requests"

    const-string v1, "external_request_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3585
    :pswitch_3c
    const-string v0, "player_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3586
    const-string v0, "request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3587
    const-string v0, "request_recipients"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3592
    :pswitch_3d
    const-string v0, "player_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3593
    const-string v0, "request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3594
    const-string v0, "request_recipients"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3600
    :pswitch_3e
    const-string v0, "request_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3601
    const-string v0, "player_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3602
    const-string v0, "request_recipients"

    const-string v1, "request_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3608
    :pswitch_3f
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3609
    const-string v0, "matches"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3614
    :pswitch_40
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3615
    const-string v0, "matches"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3620
    :pswitch_41
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3621
    const-string v0, "matches"

    const-string v1, "external_match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3627
    :pswitch_42
    const-string v0, "external_match_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3628
    const-string v0, "matches"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3633
    :pswitch_43
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 3634
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3635
    invoke-static {v0}, Lcom/google/android/gms/games/d/c;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3637
    :cond_1
    const-string v0, "matches_pending_ops"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3642
    :pswitch_44
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 3643
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3644
    invoke-static {v0}, Lcom/google/android/gms/games/d/c;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3646
    :cond_2
    const-string v0, "matches_pending_ops"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3652
    :pswitch_45
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 3653
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3654
    invoke-static {v0}, Lcom/google/android/gms/games/d/c;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3656
    :cond_3
    const-string v0, "matches_pending_ops"

    const-string v1, "external_match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3662
    :pswitch_46
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3663
    const-string v0, "participants"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3668
    :pswitch_47
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3669
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3670
    const-string v0, "participants"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3675
    :pswitch_48
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3676
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3677
    const-string v0, "participants"

    const-string v1, "external_participant_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3683
    :pswitch_49
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3684
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3685
    const-string v0, "participants"

    const-string v1, "match_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3690
    :pswitch_4a
    const-string v1, "external_participant_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3691
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3692
    const-string v0, "participants"

    const-string v1, "invitation_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3698
    :pswitch_4b
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3699
    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3700
    const-string v0, "type"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3701
    const-string v0, "notifications"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3706
    :pswitch_4c
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3707
    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3708
    const-string v0, "type"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3709
    const-string v0, "notifications"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3714
    :pswitch_4d
    const-string v0, "game_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3715
    const-string v0, "external_sub_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3716
    const-string v0, "type"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3717
    const-string v0, "notifications"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3722
    :pswitch_4e
    const-string v0, "external_milestone_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3723
    const-string v0, "milestones_sorting_rank"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3724
    const-string v0, "quest_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3725
    const-string v0, "milestones"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3730
    :pswitch_4f
    const-string v0, "external_milestone_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3731
    const-string v0, "milestones_sorting_rank"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3732
    const-string v0, "quest_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3733
    const-string v0, "milestones"

    const-string v1, "external_milestone_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3739
    :pswitch_50
    const-string v0, "external_milestone_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3740
    const-string v0, "milestones_sorting_rank"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3741
    const-string v0, "quest_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3742
    const-string v0, "milestones"

    const-string v1, "quest_id=? AND external_milestone_id=?"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "quest_id"

    invoke-virtual {p3, v5}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const-string v4, "milestones_sorting_rank"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v2

    invoke-virtual {p1, v0, p3, v1, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3750
    :pswitch_51
    const-string v0, "external_quest_id"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3751
    const-string v0, "accepted_ts"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3752
    const-string v0, "notification_ts"

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3753
    const-string v0, "quests"

    const-string v1, "external_quest_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3759
    :pswitch_52
    const-string v1, "external_quest_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3760
    const-string v1, "accepted_ts"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3761
    const-string v1, "notification_ts"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3762
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3763
    const-string v0, "quests"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3769
    :pswitch_53
    const-string v1, "game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3770
    const-string v1, "owner_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3771
    const-string v1, "external_snapshot_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3772
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3773
    const-string v0, "snapshots"

    invoke-virtual {p1, v0, p3, p4, p5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3778
    :pswitch_54
    const-string v1, "game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3779
    const-string v1, "owner_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3780
    const-string v1, "external_snapshot_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3781
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3782
    const-string v0, "snapshots"

    const-string v1, "_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3787
    :pswitch_55
    const-string v1, "game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3788
    const-string v1, "owner_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3789
    const-string v1, "external_snapshot_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3790
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3791
    const-string v0, "snapshots"

    const-string v1, "external_snapshot_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3797
    :pswitch_56
    const-string v1, "game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3798
    const-string v1, "owner_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3799
    const-string v1, "external_snapshot_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3800
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3801
    const-string v0, "snapshots"

    const-string v1, "game_id"

    invoke-static {p1, p2, v0, v1, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3806
    :pswitch_57
    const-string v1, "game_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3807
    const-string v1, "owner_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3808
    const-string v1, "external_snapshot_id"

    invoke-static {v1, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3809
    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3810
    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v1

    const-string v2, "snapshots"

    const-string v3, "game_id"

    const-string v4, "games"

    const-string v5, "external_game_id"

    move-object v0, p1

    move-object v6, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I

    move-result v0

    goto/16 :goto_0

    .line 3182
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_11
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_0
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_0
        :pswitch_0
        :pswitch_1e
        :pswitch_1f
        :pswitch_0
        :pswitch_20
        :pswitch_0
        :pswitch_21
        :pswitch_22
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_23
        :pswitch_24
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1c
        :pswitch_1d
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_0
        :pswitch_2c
        :pswitch_2d
        :pswitch_0
        :pswitch_0
        :pswitch_2e
        :pswitch_2f
        :pswitch_0
        :pswitch_0
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_0
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_0
        :pswitch_3f
        :pswitch_40
        :pswitch_0
        :pswitch_41
        :pswitch_42
        :pswitch_0
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_0
        :pswitch_52
        :pswitch_0
        :pswitch_0
        :pswitch_51
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_0
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_0
        :pswitch_30
    .end packed-switch

    .line 3302
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_13
        :pswitch_14
    .end packed-switch
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 3847
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v2

    .line 3848
    iget-object v3, v2, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/ay;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 3849
    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v3

    .line 3850
    sget-object v4, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v3}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 4137
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid delete URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3854
    :pswitch_1
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->s:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    .line 4133
    :cond_0
    :goto_0
    return v0

    .line 3861
    :pswitch_2
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->t:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto :goto_0

    .line 3865
    :pswitch_3
    const-string v0, "game_instances"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3869
    :pswitch_4
    const-string v2, "game_instances"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3874
    :pswitch_5
    const-string v2, "game_instances"

    const-string v3, "instance_game_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3879
    :pswitch_6
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t delete game instances by external id!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3885
    :pswitch_7
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->B:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto :goto_0

    .line 3889
    :pswitch_8
    const-string v0, "player_levels"

    invoke-virtual {p1, v0, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3893
    :pswitch_9
    const-string v2, "player_levels"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3898
    :pswitch_a
    const-string v2, "player_levels"

    const-string v3, "level_value=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 3904
    :pswitch_b
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 3905
    const-string v3, "account_name"

    invoke-interface {v2, v3}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v3

    .line 3906
    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 3907
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    .line 3908
    invoke-static {v2}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 3909
    if-nez v3, :cond_0

    move v0, v1

    goto/16 :goto_0

    .line 3917
    :pswitch_c
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->i:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3922
    :pswitch_d
    const-string v2, "achievement_instances"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3928
    :pswitch_e
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->k:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3935
    :pswitch_f
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->j:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3941
    :pswitch_10
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->m:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3946
    :pswitch_11
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->l:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3952
    :pswitch_12
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->n:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3961
    :pswitch_13
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->o:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3966
    :pswitch_14
    const-string v2, "event_instances"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 3974
    :pswitch_15
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->p:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3982
    :pswitch_16
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->q:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3989
    :pswitch_17
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3998
    :pswitch_18
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->y:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4006
    :pswitch_19
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->x:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4013
    :pswitch_1a
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->w:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4018
    :pswitch_1b
    const-string v2, "leaderboard_pending_scores"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4025
    :pswitch_1c
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->u:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4031
    :pswitch_1d
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->v:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4038
    :pswitch_1e
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->F:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4045
    :pswitch_1f
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->E:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4053
    :pswitch_20
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->z:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4059
    :pswitch_21
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->A:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4064
    :pswitch_22
    const-string v0, "participants"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4068
    :pswitch_23
    const-string v2, "participants"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4073
    :pswitch_24
    const-string v2, "participants"

    const-string v3, "external_participant_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4078
    :pswitch_25
    const-string v2, "participants"

    const-string v3, "match_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4083
    :pswitch_26
    const-string v2, "participants"

    const-string v3, "invitation_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4094
    :pswitch_27
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->D:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4102
    :pswitch_28
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->C:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 4107
    :pswitch_29
    const-string v0, "notifications"

    invoke-virtual {p1, v0, p3, p4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4111
    :pswitch_2a
    const-string v2, "notifications"

    const-string v3, "_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4116
    :pswitch_2b
    const-string v2, "notifications"

    const-string v3, "game_id=?"

    new-array v0, v0, [Ljava/lang/String;

    invoke-static {p2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v0, v1

    invoke-virtual {p1, v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    goto/16 :goto_0

    .line 4121
    :pswitch_2c
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Can\'t delete notifications by external id!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4125
    :pswitch_2d
    iget-object v0, v2, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/az;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "achievement_definitions"

    invoke-virtual {v0, v1, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    const-string v3, "achievement_instances"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "games"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "game_badges"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "game_instances"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "invitations"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboards"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboard_instances"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "leaderboard_scores"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "matches"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "notifications"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "participants"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v3

    add-int/2addr v1, v3

    const-string v3, "snapshots"

    invoke-virtual {v0, v3, v6, v6}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    add-int/2addr v0, v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "match_sync_token"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    const-string v2, "request_sync_token"

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-static {v1}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_0

    .line 4133
    :pswitch_2e
    const-string v4, "_id"

    iget-object v5, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lcom/google/android/gms/games/provider/h;

    move-object v0, p0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)I

    move-result v0

    goto/16 :goto_0

    .line 3850
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_e
        :pswitch_e
        :pswitch_f
        :pswitch_d
        :pswitch_f
        :pswitch_f
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_13
        :pswitch_15
        :pswitch_14
        :pswitch_15
        :pswitch_15
        :pswitch_15
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_10
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_12
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_18
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_19
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
        :pswitch_0
        :pswitch_1c
        :pswitch_1c
        :pswitch_1d
        :pswitch_1d
        :pswitch_0
        :pswitch_1d
        :pswitch_1e
        :pswitch_1e
        :pswitch_1e
        :pswitch_1f
        :pswitch_1f
        :pswitch_1f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_20
        :pswitch_20
        :pswitch_0
        :pswitch_20
        :pswitch_20
        :pswitch_20
        :pswitch_21
        :pswitch_21
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_27
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_28
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_2e
        :pswitch_0
        :pswitch_1b
        :pswitch_27
        :pswitch_27
        :pswitch_2d
    .end packed-switch
.end method

.method protected final a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 6

    .prologue
    .line 4588
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v0

    .line 4589
    const-string v1, "r"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 4590
    iget-object v1, v0, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/ay;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 4595
    iget-object v1, v0, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;

    invoke-virtual {v1}, Lcom/google/android/gms/games/provider/az;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    invoke-direct {p0, v1, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/bg;J)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    return-object v0

    .line 4592
    :cond_0
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Mode "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " not supported."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 4595
    :cond_1
    new-instance v0, Ljava/io/FileNotFoundException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No image found for URI "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/FileNotFoundException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 9

    .prologue
    .line 1981
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v1

    .line 1982
    iget-object v0, v1, Lcom/google/android/gms/games/provider/ay;->d:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/ay;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 1984
    new-instance v0, Landroid/database/sqlite/SQLiteQueryBuilder;

    invoke-direct {v0}, Landroid/database/sqlite/SQLiteQueryBuilder;-><init>()V

    .line 1985
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, p2, p4, p5}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 1986
    invoke-static {p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v3

    .line 1991
    sget-object v4, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v3}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 2779
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid query URI: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1993
    :pswitch_0
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    .line 2782
    :goto_0
    if-eqz p3, :cond_0

    array-length v1, p3

    const/4 v3, 0x1

    if-ne v1, v3, :cond_0

    const-string v1, "_count"

    const/4 v3, 0x0

    aget-object v3, p3, v3

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2784
    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->H:Lcom/android/a/a/a;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    .line 2788
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    .line 2789
    iget-object v4, v2, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    .line 2792
    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v8, 0x0

    move-object v1, p1

    move-object v2, p3

    move-object v7, p6

    invoke-virtual/range {v0 .. v8}, Landroid/database/sqlite/SQLiteQueryBuilder;->query(Landroid/database/sqlite/SQLiteDatabase;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 2796
    instance-of v1, v0, Landroid/database/AbstractWindowedCursor;

    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 2798
    if-eqz v0, :cond_1

    .line 2799
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    sget-object v2, Lcom/google/android/gms/games/provider/l;->a:Landroid/net/Uri;

    invoke-interface {v0, v1, v2}, Landroid/database/Cursor;->setNotificationUri(Landroid/content/ContentResolver;Landroid/net/Uri;)V

    .line 2802
    :cond_1
    :goto_1
    return-object v0

    .line 1998
    :pswitch_1
    const-string v1, "games._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 1999
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto :goto_0

    .line 2004
    :pswitch_2
    const-string v1, "games._id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 2005
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto :goto_0

    .line 2010
    :pswitch_3
    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->d(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v1

    .line 2011
    const-string v3, "external_game_id"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2012
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto :goto_0

    .line 2017
    :pswitch_4
    const-string v1, "games._id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 2018
    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->ab:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Lcom/android/a/a/a;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    goto :goto_0

    .line 2023
    :pswitch_5
    const-string v1, "games._id"

    invoke-static {p2}, Lcom/google/android/gms/games/ui/d/ar;->c(Landroid/net/Uri;)J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 2024
    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->ac:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/provider/GamesContentProvider;->Z:Lcom/android/a/a/a;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    goto :goto_0

    .line 2029
    :pswitch_6
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2030
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2035
    :pswitch_7
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2040
    :pswitch_8
    const-string v1, "game_badges._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2041
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2046
    :pswitch_9
    const-string v1, "badge_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2047
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2052
    :pswitch_a
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2053
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2058
    :pswitch_b
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2063
    :pswitch_c
    const-string v1, "game_instances._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2064
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2069
    :pswitch_d
    const-string v1, "instance_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2070
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2075
    :pswitch_e
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2076
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2081
    :pswitch_f
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2086
    :pswitch_10
    const-string v1, "_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2087
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2092
    :pswitch_11
    const-string v1, "external_player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2093
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2099
    :pswitch_12
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2104
    :pswitch_13
    const-string v1, "_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2105
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2110
    :pswitch_14
    const-string v1, "level_value"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2111
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2117
    :pswitch_15
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 2118
    if-eqz p3, :cond_3

    .line 2120
    :goto_2
    array-length v0, p3

    new-array v3, v0, [Ljava/lang/Object;

    .line 2121
    const/4 v1, 0x0

    .line 2122
    const/4 v0, 0x0

    :goto_3
    array-length v4, p3

    if-ge v0, v4, :cond_5

    .line 2123
    aget-object v4, p3, v0

    invoke-interface {v2, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v4

    or-int/2addr v1, v4

    .line 2126
    const-string v4, "cover_photo_image_uri"

    aget-object v5, p3, v0

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 2127
    const-string v4, "cover_photo_image_id"

    const-wide/16 v6, -0x1

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    .line 2129
    const-wide/16 v6, -0x1

    cmp-long v6, v4, v6

    if-nez v6, :cond_4

    .line 2130
    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 2136
    :cond_2
    :goto_4
    sget-object v4, Lcom/google/android/gms/games/provider/GamesContentProvider;->az:Lcom/google/android/gms/games/provider/a;

    aget-object v5, p3, v0

    invoke-virtual {v4, v5}, Lcom/google/android/gms/games/provider/a;->a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;

    move-result-object v4

    .line 2137
    sget-object v5, Lcom/google/android/gms/games/provider/g;->a:[I

    invoke-virtual {v4}, Lcom/google/android/gms/games/provider/d;->ordinal()I

    move-result v4

    aget v4, v5, v4

    packed-switch v4, :pswitch_data_1

    .line 2145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2118
    :cond_3
    sget-object v0, Lcom/google/android/gms/games/provider/GamesContentProvider;->az:Lcom/google/android/gms/games/provider/a;

    iget-object p3, v0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    goto :goto_2

    .line 2132
    :cond_4
    invoke-static {p2}, Lcom/google/android/gms/games/provider/ad;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v6}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_4

    .line 2139
    :pswitch_16
    aget-object v4, p3, v0

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v3, v0

    .line 2122
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 2142
    :pswitch_17
    aget-object v4, p3, v0

    const-wide/16 v6, -0x1

    invoke-interface {v2, v4, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v3, v0

    goto :goto_5

    .line 2150
    :cond_5
    new-instance v0, Landroid/database/MatrixCursor;

    invoke-direct {v0, p3}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 2151
    if-eqz v1, :cond_1

    .line 2152
    invoke-virtual {v0, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    goto/16 :goto_1

    .line 2158
    :pswitch_18
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2163
    :pswitch_19
    const-string v1, "achievement_definitions._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2164
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2169
    :pswitch_1a
    const-string v1, "external_achievement_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2170
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2175
    :pswitch_1b
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2176
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2181
    :pswitch_1c
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2182
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2187
    :pswitch_1d
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2192
    :pswitch_1e
    const-string v1, "achievement_pending_ops._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2193
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2198
    :pswitch_1f
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2203
    :pswitch_20
    const-string v1, "achievement_instances._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2204
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2209
    :pswitch_21
    const-string v1, "player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2210
    const-string v1, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2211
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2216
    :pswitch_22
    const-string v1, "external_player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2217
    const-string v1, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2218
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2223
    :pswitch_23
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2228
    :pswitch_24
    const-string v1, "event_definitions._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2229
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2234
    :pswitch_25
    const-string v1, "event_definitions_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2235
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2240
    :pswitch_26
    const-string v1, "external_event_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2241
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2246
    :pswitch_27
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2247
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2252
    :pswitch_28
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2257
    :pswitch_29
    const-string v1, "event_instances._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2258
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2263
    :pswitch_2a
    const-string v1, "player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2264
    const-string v1, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2265
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2270
    :pswitch_2b
    const-string v1, "external_player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2271
    const-string v1, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2272
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2277
    :pswitch_2c
    const-string v1, "external_event_id"

    const-string v3, "external_event_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2279
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2284
    :pswitch_2d
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2289
    :pswitch_2e
    const-string v1, "event_pending_ops._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2290
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2295
    :pswitch_2f
    const-string v1, "external_event_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2296
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2301
    :pswitch_30
    const-string v1, "external_player_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2302
    const-string v1, "external_game_id"

    const-string v3, "external_game_id"

    invoke-virtual {v2, v1, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 2303
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2308
    :pswitch_31
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2313
    :pswitch_32
    const-string v1, "application_sessions._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2314
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2319
    :pswitch_33
    const-string v1, "app_content"

    sget-object v3, Lcom/google/android/gms/games/provider/GamesContentProvider;->U:Lcom/android/a/a/a;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteQueryBuilder;Ljava/lang/String;Lcom/android/a/a/a;)V

    goto/16 :goto_0

    .line 2324
    :pswitch_34
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2329
    :pswitch_35
    const-string v1, "client_contexts._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2330
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2335
    :pswitch_36
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2340
    :pswitch_37
    const-string v1, "experience_events._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2341
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2346
    :pswitch_38
    const-string v1, "external_experience_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2347
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2352
    :pswitch_39
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2357
    :pswitch_3a
    const-string v1, "leaderboards._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2358
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2363
    :pswitch_3b
    const-string v1, "external_leaderboard_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2364
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2369
    :pswitch_3c
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2370
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2375
    :pswitch_3d
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2376
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2381
    :pswitch_3e
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2386
    :pswitch_3f
    const-string v1, "leaderboard_instances._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2387
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2392
    :pswitch_40
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2393
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2398
    :pswitch_41
    const-string v1, "external_leaderboard_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2399
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->l(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2404
    :pswitch_42
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2409
    :pswitch_43
    const-string v1, "leaderboard_scores._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2410
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2415
    :pswitch_44
    const-string v1, "leaderboard_instances._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2416
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->m(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2421
    :pswitch_45
    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->ai:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    .line 2422
    sget-object v1, Lcom/google/android/gms/games/provider/GamesContentProvider;->ah:Lcom/android/a/a/a;

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setProjectionMap(Ljava/util/Map;)V

    goto/16 :goto_0

    .line 2427
    :pswitch_46
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2432
    :pswitch_47
    const-string v1, "_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2433
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2438
    :pswitch_48
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2443
    :pswitch_49
    const-string v1, "invitations._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2444
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2450
    :pswitch_4a
    const-string v1, "_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2451
    const-string v1, "invitations"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2456
    :pswitch_4b
    const-string v1, "external_invitation_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2457
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2462
    :pswitch_4c
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2467
    :pswitch_4d
    const-string v1, "request_pending_ops._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2468
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2473
    :pswitch_4e
    const-string v1, "external_request_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2474
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2479
    :pswitch_4f
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2484
    :pswitch_50
    const-string v1, "requests._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2485
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2490
    :pswitch_51
    const-string v1, "external_request_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2491
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2496
    :pswitch_52
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2501
    :pswitch_53
    const-string v1, "request_recipients._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2502
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2507
    :pswitch_54
    const-string v1, "request_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2508
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->n(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2513
    :pswitch_55
    const-string v1, "requests._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2514
    const-string v1, "requests"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2519
    :pswitch_56
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2524
    :pswitch_57
    const-string v1, "matches._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2525
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2530
    :pswitch_58
    const-string v1, "matches._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2531
    const-string v1, "matches"

    invoke-virtual {v0, v1}, Landroid/database/sqlite/SQLiteQueryBuilder;->setTables(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 2536
    :pswitch_59
    const-string v1, "external_match_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2537
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2542
    :pswitch_5a
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2543
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2548
    :pswitch_5b
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2549
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2554
    :pswitch_5c
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2559
    :pswitch_5d
    const-string v1, "matches_pending_ops._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2560
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2565
    :pswitch_5e
    const-string v1, "external_match_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2566
    invoke-static {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Landroid/database/sqlite/SQLiteQueryBuilder;)V

    goto/16 :goto_0

    .line 2571
    :pswitch_5f
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2576
    :pswitch_60
    const-string v1, "participants._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2577
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2582
    :pswitch_61
    const-string v1, "external_participant_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2583
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2588
    :pswitch_62
    const-string v1, "match_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2589
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2594
    :pswitch_63
    const-string v1, "invitation_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2595
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->r(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2600
    :pswitch_64
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2605
    :pswitch_65
    const-string v1, "quests._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2606
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2611
    :pswitch_66
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2612
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2617
    :pswitch_67
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2618
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2623
    :pswitch_68
    const-string v1, "external_quest_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2624
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->t(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2629
    :pswitch_69
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2634
    :pswitch_6a
    const-string v1, "quests._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2635
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2640
    :pswitch_6b
    const-string v1, "external_quest_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2641
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2646
    :pswitch_6c
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2647
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2652
    :pswitch_6d
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2653
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2658
    :pswitch_6e
    const-string v1, "external_milestone_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2659
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->u(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2664
    :pswitch_6f
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2669
    :pswitch_70
    const-string v1, "milestones._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2670
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2675
    :pswitch_71
    const-string v1, "external_milestone_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2676
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2681
    :pswitch_72
    const-string v1, "quest_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2682
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2687
    :pswitch_73
    const-string v1, "external_quest_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2688
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->s(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2693
    :pswitch_74
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2698
    :pswitch_75
    const-string v1, "requests._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2699
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2704
    :pswitch_76
    const-string v1, "external_request_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2705
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->o(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2710
    :pswitch_77
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->p(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2715
    :pswitch_78
    const-string v1, "matches._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2716
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->p(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2721
    :pswitch_79
    const-string v1, "external_match_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2722
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->p(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2727
    :pswitch_7a
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->q(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2732
    :pswitch_7b
    const-string v1, "notifications._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2733
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->q(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2738
    :pswitch_7c
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2739
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->q(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2744
    :pswitch_7d
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2745
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->q(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2750
    :pswitch_7e
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2755
    :pswitch_7f
    const-string v1, "snapshots._id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2756
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2761
    :pswitch_80
    const-string v1, "external_snapshot_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2762
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2767
    :pswitch_81
    const-string v1, "game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;)V

    .line 2768
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 2773
    :pswitch_82
    const-string v1, "external_game_id"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;)V

    .line 2774
    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->v(Landroid/database/sqlite/SQLiteQueryBuilder;Landroid/net/Uri;)V

    goto/16 :goto_0

    .line 1991
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
        :pswitch_34
        :pswitch_35
        :pswitch_36
        :pswitch_37
        :pswitch_38
        :pswitch_39
        :pswitch_3a
        :pswitch_3b
        :pswitch_3c
        :pswitch_3d
        :pswitch_3e
        :pswitch_3f
        :pswitch_40
        :pswitch_41
        :pswitch_42
        :pswitch_43
        :pswitch_44
        :pswitch_45
        :pswitch_46
        :pswitch_47
        :pswitch_48
        :pswitch_49
        :pswitch_4a
        :pswitch_4b
        :pswitch_4c
        :pswitch_4d
        :pswitch_4e
        :pswitch_4f
        :pswitch_50
        :pswitch_51
        :pswitch_52
        :pswitch_53
        :pswitch_54
        :pswitch_55
        :pswitch_56
        :pswitch_57
        :pswitch_58
        :pswitch_59
        :pswitch_5a
        :pswitch_5b
        :pswitch_5c
        :pswitch_5d
        :pswitch_5e
        :pswitch_5f
        :pswitch_60
        :pswitch_61
        :pswitch_62
        :pswitch_63
        :pswitch_64
        :pswitch_65
        :pswitch_66
        :pswitch_67
        :pswitch_68
        :pswitch_69
        :pswitch_6a
        :pswitch_6b
        :pswitch_6c
        :pswitch_6d
        :pswitch_6e
        :pswitch_6f
        :pswitch_70
        :pswitch_71
        :pswitch_72
        :pswitch_73
        :pswitch_74
        :pswitch_75
        :pswitch_76
        :pswitch_77
        :pswitch_78
        :pswitch_79
        :pswitch_7a
        :pswitch_7b
        :pswitch_7c
        :pswitch_7d
        :pswitch_7e
        :pswitch_7f
        :pswitch_80
        :pswitch_81
        :pswitch_82
    .end packed-switch

    .line 2137
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_16
        :pswitch_17
    .end packed-switch
.end method

.method protected final declared-synchronized a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;
    .locals 1

    .prologue
    .line 1633
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v0

    .line 1634
    iget-object v0, v0, Lcom/google/android/gms/games/provider/ay;->b:Lcom/google/android/gms/games/provider/az;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 1633
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 12

    .prologue
    .line 2807
    move-object v2, p2

    move-object v1, p1

    move-object v0, p0

    :goto_0
    invoke-direct {v0, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/ay;

    move-result-object v6

    .line 2808
    iget-object v3, v6, Lcom/google/android/gms/games/provider/ay;->e:Ljava/util/concurrent/CountDownLatch;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/ay;->a(Ljava/util/concurrent/CountDownLatch;)V

    .line 2809
    iget-object v3, v6, Lcom/google/android/gms/games/provider/ay;->c:Lcom/google/android/gms/games/provider/bg;

    .line 2811
    const-wide/16 v4, -0x1

    .line 2812
    invoke-static {v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v7

    .line 2813
    sget-object v8, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v7}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v9

    aget v8, v8, v9

    sparse-switch v8, :sswitch_data_0

    .line 3165
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Invalid insert URI: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2815
    :sswitch_0
    const-string v3, "external_game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2817
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2818
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ab;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2819
    const-string v4, "games"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    .line 3170
    :goto_1
    return-object v2

    .line 2829
    :sswitch_1
    :try_start_0
    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;)J
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v4

    .line 2835
    :goto_2
    const-wide/16 v6, -0x1

    cmp-long v3, v4, v6

    if-nez v3, :cond_0

    .line 2836
    const-string v0, "GamesContentProvider"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t find image to write to for URI "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move-wide v0, v4

    .line 3167
    :goto_3
    const-wide/16 v4, 0x0

    cmp-long v3, v0, v4

    if-gez v3, :cond_6

    .line 3168
    new-instance v0, Landroid/database/sqlite/SQLiteException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Error occured while inserting "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " to uri "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/sqlite/SQLiteException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2841
    :cond_0
    invoke-static {v2}, Lcom/google/android/gms/games/provider/ad;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_0

    .line 2845
    :sswitch_2
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2846
    const-string v0, "game_badges"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_3

    .line 2851
    :sswitch_3
    const-string v0, "game_instances"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto :goto_3

    .line 2856
    :sswitch_4
    const-string v3, "external_player_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2858
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2859
    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v4

    invoke-static {v4, p3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/content/ContentValues;)V

    .line 2860
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/aq;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2861
    const-string v4, "players"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto :goto_1

    .line 2867
    :sswitch_5
    const-string v3, "level_value"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 2868
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ap;->a(Landroid/net/Uri;I)Landroid/net/Uri;

    move-result-object v3

    .line 2869
    const-string v4, "player_levels"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2874
    :sswitch_6
    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 2875
    invoke-virtual {p3}, Landroid/content/ContentValues;->valueSet()Ljava/util/Set;

    move-result-object v0

    .line 2876
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_4
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2877
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2878
    sget-object v5, Lcom/google/android/gms/games/provider/GamesContentProvider;->az:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v5, v1}, Lcom/google/android/gms/games/provider/a;->a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;

    move-result-object v5

    .line 2879
    sget-object v6, Lcom/google/android/gms/games/provider/g;->a:[I

    invoke-virtual {v5}, Lcom/google/android/gms/games/provider/d;->ordinal()I

    move-result v5

    aget v5, v6, v5

    packed-switch v5, :pswitch_data_0

    .line 2887
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Unsupported metadata column type"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 2881
    :pswitch_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    .line 2884
    :pswitch_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-interface {v3, v1, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    goto :goto_4

    .line 2889
    :cond_1
    invoke-static {v3}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    goto/16 :goto_1

    .line 2895
    :sswitch_7
    const-string v1, "cover_photo_image_url"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "Must provide an image URL when inserting"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2898
    const-string v1, "cover_photo_image_url"

    const-string v3, "cover_photo_image_id"

    invoke-direct {v0, v6, p3, v1, v3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 2900
    const-string v1, "cover_photo_image_id"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 2903
    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 2904
    const-string v1, "cover_photo_image_id"

    invoke-interface {v0, v1, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    .line 2905
    invoke-static {v0}, Lcom/android/a/c;->a(Landroid/content/SharedPreferences$Editor;)V

    .line 2908
    invoke-static {v2}, Lcom/google/android/gms/games/provider/ad;->a(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2912
    :sswitch_8
    const-string v3, "external_achievement_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2914
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->d(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2915
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/n;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2917
    const-string v4, "achievement_definitions"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2922
    :sswitch_9
    const-string v0, "achievement_pending_ops"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2927
    :sswitch_a
    const-string v0, "achievement_instances"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2932
    :sswitch_b
    const-string v0, "application_sessions"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2937
    :sswitch_c
    const-string v0, "app_content"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2943
    :sswitch_d
    const-string v3, "package_name"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2945
    const-string v4, "package_uid"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 2946
    if-lez v5, :cond_2

    const/4 v4, 0x1

    :goto_5
    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 2947
    const-string v4, "account_name"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    .line 2951
    new-instance v6, Lcom/google/android/gms/common/e/b;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 2952
    const-string v7, "package_name"

    invoke-virtual {v6, v7, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2953
    const-string v3, "package_uid"

    int-to-long v8, v5

    invoke-virtual {v6, v3, v8, v9}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 2954
    const-string v3, "account_name"

    invoke-virtual {v6, v3, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 2955
    const-string v4, "client_contexts"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2946
    :cond_2
    const/4 v4, 0x0

    goto :goto_5

    .line 2960
    :sswitch_e
    const-string v3, "external_event_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2962
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->g(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2963
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/u;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2965
    const-string v4, "event_definitions"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2970
    :sswitch_f
    const-string v0, "event_instances"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2975
    :sswitch_10
    const-string v0, "event_pending_ops"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 2980
    :sswitch_11
    const-string v3, "external_experience_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2982
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->e(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2983
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/x;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2984
    const-string v4, "experience_events"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2989
    :sswitch_12
    const-string v3, "external_leaderboard_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 2991
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->f(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 2992
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ai;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 2994
    const-string v4, "leaderboards"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 2999
    :sswitch_13
    const-string v3, "leaderboard_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3000
    const-wide/16 v6, 0x0

    cmp-long v3, v4, v6

    if-lez v3, :cond_3

    const/4 v3, 0x1

    :goto_6
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3001
    const-string v3, "collection"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3002
    const-string v3, "collection"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 3003
    const-string v6, "timespan"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3004
    const-string v6, "timespan"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    .line 3007
    new-instance v8, Lcom/google/android/gms/common/e/b;

    const/4 v7, 0x0

    const/4 v9, 0x0

    invoke-direct {v8, v2, v7, v9}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 3008
    const-string v7, "leaderboard_id"

    invoke-virtual {v8, v7, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3009
    const-string v4, "collection"

    int-to-long v10, v3

    invoke-virtual {v8, v4, v10, v11}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3010
    const-string v3, "timespan"

    int-to-long v4, v6

    invoke-virtual {v8, v3, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3011
    const-string v4, "leaderboard_instances"

    const/4 v5, 0x0

    invoke-virtual {v8}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v8, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3000
    :cond_3
    const/4 v3, 0x0

    goto :goto_6

    .line 3016
    :sswitch_14
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->i(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3017
    const-string v0, "leaderboard_scores"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 3022
    :sswitch_15
    const-string v0, "leaderboard_pending_scores"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 3028
    :sswitch_16
    const-string v1, "url"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v4, "Must provide an image URL when inserting"

    invoke-static {v1, v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 3030
    const-string v1, "url"

    const-string v4, "_id"

    invoke-direct {v0, v6, p3, v1, v4}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 3031
    const-string v1, "_id"

    invoke-virtual {p3, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3033
    invoke-direct {v0, v3, v4, v5, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/bg;JLandroid/content/ContentValues;)I

    move-wide v0, v4

    .line 3034
    goto/16 :goto_3

    .line 3038
    :sswitch_17
    const-string v3, "external_invitation_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3040
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ae;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3041
    const-string v4, "invitations"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3045
    :sswitch_18
    const-string v3, "external_match_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3047
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ak;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3048
    const-string v4, "matches"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3053
    :sswitch_19
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3054
    const-string v0, "type"

    invoke-virtual {p3, v0}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 3055
    invoke-static {v0}, Lcom/google/android/gms/games/d/c;->a(I)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3058
    const-string v0, "matches_pending_ops"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 3064
    :sswitch_1a
    const-string v3, "game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3065
    const-string v3, "game_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3066
    const-string v3, "external_sub_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3067
    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3068
    const-string v6, "type"

    invoke-virtual {p3, v6}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v6

    invoke-static {v6}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 3071
    new-instance v6, Lcom/google/android/gms/common/e/b;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 3072
    const-string v7, "game_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3073
    const-string v4, "external_sub_id"

    invoke-virtual {v6, v4, v3}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 3076
    const-string v4, "notifications"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3082
    :sswitch_1b
    const-string v3, "placing"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_4

    .line 3083
    const-string v3, "result_type"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 3085
    :cond_4
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->j(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3086
    const-string v0, "participants"

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v3, p3}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v0

    goto/16 :goto_3

    .line 3091
    :sswitch_1c
    const-string v3, "accepted_ts"

    invoke-static {v3, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3092
    const-string v3, "notification_ts"

    invoke-static {v3, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3093
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3094
    const-string v4, "quests"

    move-object v3, v2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3098
    :sswitch_1d
    const-string v3, "external_quest_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3100
    const-string v4, "accepted_ts"

    invoke-static {v4, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3101
    const-string v4, "notification_ts"

    invoke-static {v4, p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Landroid/content/ContentValues;)V

    .line 3102
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->h(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3103
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ar;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3104
    const-string v4, "quests"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3108
    :sswitch_1e
    const-string v4, "milestones"

    move-object v3, v2

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3113
    :sswitch_1f
    const-string v3, "milestones_sorting_rank"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3114
    const-string v3, "quest_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3115
    const-string v3, "external_milestone_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3117
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/am;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3118
    const-string v4, "milestones"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3123
    :sswitch_20
    const-string v3, "external_request_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 3125
    const-string v4, "external_game_id"

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3126
    const-string v4, "client_context_id"

    invoke-static {v4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 3127
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/au;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3129
    const-string v4, "request_pending_ops"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3134
    :sswitch_21
    const-string v3, "external_request_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3136
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/aw;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3137
    const-string v4, "requests"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3141
    :sswitch_22
    const-string v3, "request_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 3143
    const-string v4, "player_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_5

    .line 3144
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Lcom/google/android/gms/games/provider/av;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    .line 3145
    const-string v4, "request_recipients"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3147
    :cond_5
    const-string v4, "player_id"

    invoke-virtual {p3, v4}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 3148
    new-instance v6, Lcom/google/android/gms/common/e/b;

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-direct {v6, v2, v7, v8}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    .line 3149
    const-string v7, "player_id"

    invoke-virtual {v6, v7, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3150
    const-string v4, "request_id"

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {v6, v4, v8, v9}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;J)V

    .line 3151
    const-string v4, "request_recipients"

    const/4 v5, 0x0

    invoke-virtual {v6}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, v6, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    move-object v3, v2

    move-object v6, p3

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3157
    :sswitch_23
    const-string v3, "external_snapshot_id"

    invoke-virtual {p3, v3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 3159
    invoke-direct {v0, v6, p3}, Lcom/google/android/gms/games/provider/GamesContentProvider;->k(Lcom/google/android/gms/games/provider/ay;Landroid/content/ContentValues;)V

    .line 3160
    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ax;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 3161
    const-string v4, "snapshots"

    move-object v5, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    .line 3170
    :cond_6
    invoke-static {v2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    goto/16 :goto_1

    :catch_0
    move-exception v3

    goto/16 :goto_2

    .line 2813
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_1
        0x4 -> :sswitch_1
        0x5 -> :sswitch_1
        0x6 -> :sswitch_1
        0x8 -> :sswitch_2
        0xc -> :sswitch_3
        0x10 -> :sswitch_4
        0x13 -> :sswitch_5
        0x14 -> :sswitch_5
        0x15 -> :sswitch_5
        0x16 -> :sswitch_6
        0x17 -> :sswitch_8
        0x1c -> :sswitch_9
        0x1e -> :sswitch_a
        0x22 -> :sswitch_e
        0x27 -> :sswitch_f
        0x2c -> :sswitch_10
        0x30 -> :sswitch_b
        0x32 -> :sswitch_c
        0x33 -> :sswitch_d
        0x35 -> :sswitch_11
        0x38 -> :sswitch_12
        0x3d -> :sswitch_13
        0x41 -> :sswitch_14
        0x44 -> :sswitch_15
        0x45 -> :sswitch_16
        0x46 -> :sswitch_16
        0x47 -> :sswitch_17
        0x4b -> :sswitch_20
        0x4e -> :sswitch_21
        0x51 -> :sswitch_22
        0x55 -> :sswitch_18
        0x5b -> :sswitch_19
        0x5e -> :sswitch_1b
        0x63 -> :sswitch_1d
        0x64 -> :sswitch_1c
        0x6e -> :sswitch_1f
        0x6f -> :sswitch_1e
        0x79 -> :sswitch_1a
        0x7d -> :sswitch_23
        0x82 -> :sswitch_7
    .end sparse-switch

    .line 2879
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final a()V
    .locals 4

    .prologue
    .line 5010
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/provider/l;->a:Landroid/net/Uri;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->notifyChange(Landroid/net/Uri;Landroid/database/ContentObserver;Z)V

    .line 5011
    return-void
.end method

.method protected final a(ILjava/lang/Object;)V
    .locals 6

    .prologue
    .line 1670
    packed-switch p1, :pswitch_data_0

    .line 1687
    :cond_0
    :goto_0
    return-void

    .line 1672
    :pswitch_0
    check-cast p2, Ljava/lang/String;

    invoke-direct {p0, p2}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 1679
    :pswitch_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1680
    iget-wide v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:J

    sub-long v2, v0, v2

    const-wide/32 v4, 0x36ee80

    cmp-long v2, v2, v4

    if-lez v2, :cond_0

    .line 1681
    iput-wide v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->d:J

    .line 1682
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->b()V

    goto :goto_0

    .line 1670
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1654
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/ar;->a(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected final c()V
    .locals 7

    .prologue
    .line 1956
    invoke-super {p0}, Lcom/google/android/gms/common/e/a;->c()V

    .line 1957
    iget-object v2, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->h:Ljava/lang/Object;

    monitor-enter v2

    .line 1958
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1960
    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->f:Ljava/util/HashMap;

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/provider/ay;

    .line 1961
    if-nez v1, :cond_1

    .line 1962
    invoke-virtual {p0}, Lcom/google/android/gms/games/provider/GamesContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    const-string v4, "GamesContentProvider"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Null data store found for store name "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v4, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1975
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1968
    :cond_1
    :try_start_1
    iget-object v4, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    .line 1969
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->a(Lcom/google/android/gms/games/provider/ay;)Lcom/google/android/gms/games/provider/j;

    move-result-object v1

    .line 1970
    if-eqz v1, :cond_0

    .line 1971
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/provider/j;->a(Ljava/util/Set;)V

    goto :goto_0

    .line 1974
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->g:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 1975
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 5020
    invoke-static {p1}, Lcom/google/android/gms/games/provider/GamesContentProvider;->c(Landroid/net/Uri;)Lcom/google/android/gms/games/provider/k;

    move-result-object v0

    .line 5021
    sget-object v1, Lcom/google/android/gms/games/provider/g;->b:[I

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/k;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 5289
    :pswitch_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown URI: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " - "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 5024
    :pswitch_1
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.games"

    .line 5285
    :goto_0
    return-object v0

    .line 5028
    :pswitch_2
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game"

    goto :goto_0

    .line 5034
    :pswitch_3
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.game_badges"

    goto :goto_0

    .line 5038
    :pswitch_4
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game_badge"

    goto :goto_0

    .line 5044
    :pswitch_5
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.game_instances"

    goto :goto_0

    .line 5048
    :pswitch_6
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.game_instance"

    goto :goto_0

    .line 5052
    :pswitch_7
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.players"

    goto :goto_0

    .line 5057
    :pswitch_8
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.player"

    goto :goto_0

    .line 5061
    :pswitch_9
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.player_levels"

    goto :goto_0

    .line 5066
    :pswitch_a
    const-string v0, "vnd.android.cursor.item/and.google.android.games.player_level"

    goto :goto_0

    .line 5070
    :pswitch_b
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.account_metadata"

    goto :goto_0

    .line 5077
    :pswitch_c
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievements"

    goto :goto_0

    .line 5081
    :pswitch_d
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement"

    goto :goto_0

    .line 5085
    :pswitch_e
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievement_pending_ops"

    goto :goto_0

    .line 5089
    :pswitch_f
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement_pending_op"

    goto :goto_0

    .line 5095
    :pswitch_10
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.achievement_states"

    goto :goto_0

    .line 5099
    :pswitch_11
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.achievement_state"

    goto :goto_0

    .line 5103
    :pswitch_12
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.request_pending_ops"

    goto :goto_0

    .line 5107
    :pswitch_13
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.request_pending_op"

    goto :goto_0

    .line 5111
    :pswitch_14
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.client_contexts"

    goto :goto_0

    .line 5115
    :pswitch_15
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.client_context"

    goto :goto_0

    .line 5123
    :pswitch_16
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.event"

    goto :goto_0

    .line 5130
    :pswitch_17
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.event_instances"

    goto :goto_0

    .line 5134
    :pswitch_18
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.event_pending_ops"

    goto :goto_0

    .line 5138
    :pswitch_19
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.invitations"

    goto :goto_0

    .line 5144
    :pswitch_1a
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.invitation"

    goto :goto_0

    .line 5148
    :pswitch_1b
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.experience_events"

    goto :goto_0

    .line 5152
    :pswitch_1c
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.experience_event"

    goto :goto_0

    .line 5158
    :pswitch_1d
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboards"

    goto :goto_0

    .line 5163
    :pswitch_1e
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard"

    goto :goto_0

    .line 5169
    :pswitch_1f
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_instances"

    goto :goto_0

    .line 5173
    :pswitch_20
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_instance"

    goto :goto_0

    .line 5178
    :pswitch_21
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_scores"

    goto :goto_0

    .line 5182
    :pswitch_22
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_score"

    goto :goto_0

    .line 5186
    :pswitch_23
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.leaderboard_pending_scores"

    goto :goto_0

    .line 5190
    :pswitch_24
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.leaderboard_pending_score"

    goto :goto_0

    .line 5206
    :pswitch_25
    const-string v0, "image/png"

    goto :goto_0

    .line 5210
    :pswitch_26
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.images"

    goto :goto_0

    .line 5216
    :pswitch_27
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.matches"

    goto :goto_0

    .line 5221
    :pswitch_28
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.match"

    goto :goto_0

    .line 5225
    :pswitch_29
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.match_pending_ops"

    goto :goto_0

    .line 5230
    :pswitch_2a
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.match_pending_op"

    goto :goto_0

    .line 5236
    :pswitch_2b
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.notifications"

    goto :goto_0

    .line 5240
    :pswitch_2c
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.notification"

    goto/16 :goto_0

    .line 5246
    :pswitch_2d
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.participants"

    goto/16 :goto_0

    .line 5251
    :pswitch_2e
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.participant"

    goto/16 :goto_0

    .line 5258
    :pswitch_2f
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.quests"

    goto/16 :goto_0

    .line 5263
    :pswitch_30
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.quest"

    goto/16 :goto_0

    .line 5270
    :pswitch_31
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.milestones"

    goto/16 :goto_0

    .line 5274
    :pswitch_32
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.milestone"

    goto/16 :goto_0

    .line 5279
    :pswitch_33
    const-string v0, "vnd.android.cursor.item/vnd.google.android.games.snapshot"

    goto/16 :goto_0

    .line 5285
    :pswitch_34
    const-string v0, "vnd.android.cursor.dir/vnd.google.android.games.snapshots"

    goto/16 :goto_0

    .line 5021
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_3
        :pswitch_3
        :pswitch_5
        :pswitch_6
        :pswitch_5
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_c
        :pswitch_c
        :pswitch_c
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_10
        :pswitch_10
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_16
        :pswitch_17
        :pswitch_0
        :pswitch_17
        :pswitch_17
        :pswitch_17
        :pswitch_18
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_15
        :pswitch_1b
        :pswitch_1c
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1e
        :pswitch_1d
        :pswitch_1d
        :pswitch_1f
        :pswitch_20
        :pswitch_1f
        :pswitch_1f
        :pswitch_21
        :pswitch_22
        :pswitch_21
        :pswitch_23
        :pswitch_26
        :pswitch_25
        :pswitch_19
        :pswitch_1a
        :pswitch_1a
        :pswitch_1a
        :pswitch_12
        :pswitch_0
        :pswitch_13
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_27
        :pswitch_28
        :pswitch_0
        :pswitch_28
        :pswitch_27
        :pswitch_27
        :pswitch_29
        :pswitch_2a
        :pswitch_2a
        :pswitch_2d
        :pswitch_2e
        :pswitch_2e
        :pswitch_2d
        :pswitch_2d
        :pswitch_2f
        :pswitch_30
        :pswitch_2f
        :pswitch_30
        :pswitch_2f
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_31
        :pswitch_32
        :pswitch_31
        :pswitch_31
        :pswitch_31
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_2b
        :pswitch_2c
        :pswitch_2b
        :pswitch_2b
        :pswitch_33
        :pswitch_34
        :pswitch_33
        :pswitch_34
        :pswitch_34
        :pswitch_0
        :pswitch_24
        :pswitch_2f
        :pswitch_0
        :pswitch_0
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
        :pswitch_25
    .end packed-switch
.end method

.method public onCreate()Z
    .locals 13

    .prologue
    .line 1603
    invoke-super {p0}, Lcom/google/android/gms/common/e/a;->onCreate()Z

    .line 1604
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "GamesProviderWorker"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:Landroid/os/HandlerThread;

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    new-instance v0, Lcom/google/android/gms/games/provider/e;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/provider/e;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/Handler;

    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 1605
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "achievement_instances"

    const-string v1, "definition_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "achievement_definitions"

    const-string v3, "_id"

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "unlocked_icon_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "revealed_icon_image_id"

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->i:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "achievement_instances"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->j:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "achievement_pending_ops"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->k:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "app_content"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->l:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "application_sessions"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->m:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "achievement_pending_ops"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "application_sessions"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "event_pending_ops"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "leaderboard_pending_scores"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "matches_pending_ops"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "request_pending_ops"

    const-string v1, "client_context_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "client_contexts"

    const-string v3, "_id"

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->n:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "event_instances"

    const-string v1, "definition_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "event_definitions"

    const-string v3, "_id"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "icon_image_id"

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->o:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "event_instances"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->p:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "event_pending_ops"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->q:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "experience_events"

    const-string v2, "_id"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "icon_id"

    aput-object v5, v3, v4

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->r:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "game_badges"

    const-string v2, "_id"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "badge_icon_image_id"

    aput-object v5, v3, v4

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->t:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/provider/f;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->u:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "leaderboard_scores"

    const-string v2, "_id"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "default_display_image_id"

    aput-object v5, v3, v4

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->w:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "leaderboard_instances"

    const-string v3, "_id"

    const/4 v4, 0x0

    const/4 v1, 0x1

    new-array v5, v1, [Lcom/google/android/gms/games/provider/i;

    const/4 v1, 0x0

    new-instance v6, Lcom/google/android/gms/games/provider/i;

    const-string v7, "leaderboard_scores"

    const-string v8, "_id"

    const-string v9, "instance_id"

    iget-object v10, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->w:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v6, v5, v1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->x:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "leaderboards"

    const-string v3, "_id"

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "board_icon_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    new-array v5, v1, [Lcom/google/android/gms/games/provider/i;

    const/4 v1, 0x0

    new-instance v6, Lcom/google/android/gms/games/provider/i;

    const-string v7, "leaderboard_instances"

    const-string v8, "_id"

    const-string v9, "leaderboard_id"

    iget-object v10, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->x:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v6, v5, v1

    const/4 v6, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->y:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "participants"

    const-string v1, "match_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lcom/google/android/gms/games/provider/h;

    const-string v8, "matches"

    const-string v9, "_id"

    const/4 v10, 0x0

    const/4 v0, 0x1

    new-array v11, v0, [Lcom/google/android/gms/games/provider/i;

    const/4 v12, 0x0

    new-instance v0, Lcom/google/android/gms/games/provider/i;

    const-string v1, "matches"

    const-string v2, "external_match_id"

    const-string v3, "_id"

    const-string v4, "notifications"

    const-string v5, "external_sub_id=?"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v11, v12

    move-object v0, v7

    move-object v1, p0

    move-object v2, v8

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v7, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->z:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "matches_pending_ops"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->A:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "participants"

    const-string v1, "invitation_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lcom/google/android/gms/games/provider/h;

    const-string v8, "invitations"

    const-string v9, "_id"

    const/4 v10, 0x0

    const/4 v0, 0x1

    new-array v11, v0, [Lcom/google/android/gms/games/provider/i;

    const/4 v12, 0x0

    new-instance v0, Lcom/google/android/gms/games/provider/i;

    const-string v1, "invitations"

    const-string v2, "external_invitation_id"

    const-string v3, "_id"

    const-string v4, "notifications"

    const-string v5, "external_sub_id=?"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v11, v12

    move-object v0, v7

    move-object v1, p0

    move-object v2, v8

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v7, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->v:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "milestones"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->C:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "milestones"

    const-string v1, "quest_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "quests"

    const-string v3, "_id"

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "quest_icon_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "quest_banner_image_id"

    aput-object v5, v4, v1

    const/4 v5, 0x0

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->D:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "request_recipients"

    const-string v1, "request_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v7, Lcom/google/android/gms/games/provider/h;

    const-string v8, "requests"

    const-string v9, "_id"

    const/4 v10, 0x0

    const/4 v0, 0x1

    new-array v11, v0, [Lcom/google/android/gms/games/provider/i;

    const/4 v12, 0x0

    new-instance v0, Lcom/google/android/gms/games/provider/i;

    const-string v1, "requests"

    const-string v2, "external_request_id"

    const-string v3, "_id"

    const-string v4, "notifications"

    const-string v5, "external_sub_id=?"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    aput-object v0, v11, v12

    move-object v0, v7

    move-object v1, p0

    move-object v2, v8

    move-object v3, v9

    move-object v4, v10

    move-object v5, v11

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v7, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->E:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "request_pending_ops"

    const-string v2, "_id"

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->F:Lcom/google/android/gms/games/provider/h;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v1, "snapshots"

    const-string v2, "_id"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "cover_icon_image_id"

    aput-object v5, v3, v4

    invoke-direct {v0, p0, v1, v2, v3}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "leaderboard_scores"

    const-string v1, "player_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "achievement_instances"

    const-string v1, "player_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "players"

    const-string v3, "_id"

    const/4 v1, 0x2

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "profile_icon_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "profile_hi_res_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    new-array v5, v1, [Lcom/google/android/gms/games/provider/i;

    const/4 v1, 0x0

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "snapshots"

    const-string v9, "_id"

    const-string v10, "owner_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->B:Lcom/google/android/gms/games/provider/h;

    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    const-string v0, "game_instances"

    const-string v1, "instance_game_id"

    invoke-virtual {v6, v0, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    new-instance v0, Lcom/google/android/gms/games/provider/h;

    const-string v2, "games"

    const-string v3, "_id"

    const/4 v1, 0x3

    new-array v4, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v5, "game_icon_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x1

    const-string v5, "game_hi_res_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x2

    const-string v5, "featured_image_id"

    aput-object v5, v4, v1

    const/4 v1, 0x7

    new-array v5, v1, [Lcom/google/android/gms/games/provider/i;

    const/4 v1, 0x0

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "game_badges"

    const-string v9, "_id"

    const-string v10, "badge_game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->t:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x1

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "achievement_definitions"

    const-string v9, "_id"

    const-string v10, "game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->i:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x2

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "leaderboards"

    const-string v9, "_id"

    const-string v10, "game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->y:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x3

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "matches"

    const-string v9, "_id"

    const-string v10, "game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->z:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x4

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "quests"

    const-string v9, "_id"

    const-string v10, "game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->D:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x5

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "snapshots"

    const-string v9, "_id"

    const-string v10, "game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->G:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    const/4 v1, 0x6

    new-instance v7, Lcom/google/android/gms/games/provider/i;

    const-string v8, "event_definitions"

    const-string v9, "_id"

    const-string v10, "event_definitions_game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->o:Lcom/google/android/gms/games/provider/h;

    invoke-direct {v7, v8, v9, v10, v11}, Lcom/google/android/gms/games/provider/i;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/provider/h;)V

    aput-object v7, v5, v1

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/provider/h;-><init>(Lcom/google/android/gms/games/provider/GamesContentProvider;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;[Lcom/google/android/gms/games/provider/i;Ljava/util/HashMap;)V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->s:Lcom/google/android/gms/games/provider/h;

    .line 1606
    const/4 v0, 0x1

    return v0
.end method

.method public shutdown()V
    .locals 1

    .prologue
    .line 1627
    iget-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->c:Landroid/os/Handler;

    invoke-virtual {v0}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 1628
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/provider/GamesContentProvider;->b:Landroid/os/HandlerThread;

    .line 1629
    return-void
.end method
