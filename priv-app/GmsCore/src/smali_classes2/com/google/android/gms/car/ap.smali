.class final Lcom/google/android/gms/car/ap;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ae;

.field private final b:Landroid/util/SparseArray;

.field private final c:Ljava/util/concurrent/ArrayBlockingQueue;

.field private final d:Ljava/lang/Object;

.field private volatile e:Z


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/ae;)V
    .locals 2

    .prologue
    .line 1926
    iput-object p1, p0, Lcom/google/android/gms/car/ap;->a:Lcom/google/android/gms/car/ae;

    invoke-direct {p0}, Ljava/lang/Thread;-><init>()V

    .line 1932
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ap;->b:Landroid/util/SparseArray;

    .line 1942
    new-instance v0, Ljava/util/concurrent/ArrayBlockingQueue;

    const/16 v1, 0xa

    invoke-direct {v0, v1}, Ljava/util/concurrent/ArrayBlockingQueue;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/ap;->c:Ljava/util/concurrent/ArrayBlockingQueue;

    .line 1944
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ap;->d:Ljava/lang/Object;

    .line 1946
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/ap;->e:Z

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/ae;B)V
    .locals 0

    .prologue
    .line 1926
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ap;-><init>(Lcom/google/android/gms/car/ae;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 1949
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ap;->e:Z

    .line 1950
    invoke-virtual {p0}, Lcom/google/android/gms/car/ap;->interrupt()V

    .line 1951
    return-void
.end method

.method public final a(Landroid/content/ComponentName;ILandroid/content/Intent;Landroid/content/Intent;)V
    .locals 7

    .prologue
    .line 1989
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1990
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connectToClient called for component "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1992
    :cond_0
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 1993
    iget-object v1, p0, Lcom/google/android/gms/car/ap;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 1994
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/ap;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_1

    .line 1995
    iget-object v2, p0, Lcom/google/android/gms/car/ap;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    new-instance v4, Lcom/google/android/gms/car/am;

    iget-object v5, p0, Lcom/google/android/gms/car/ap;->a:Lcom/google/android/gms/car/ae;

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/car/am;-><init>(Lcom/google/android/gms/car/ae;B)V

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 1998
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/ap;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/am;

    .line 2000
    iput-object p1, v0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    .line 2001
    iput p2, v0, Lcom/google/android/gms/car/am;->b:I

    .line 2002
    iput-object p3, v0, Lcom/google/android/gms/car/am;->c:Landroid/content/Intent;

    .line 2003
    iput-object p4, v0, Lcom/google/android/gms/car/am;->d:Landroid/content/Intent;

    .line 2004
    iget-object v0, p0, Lcom/google/android/gms/car/ap;->c:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ArrayBlockingQueue;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 2005
    const-string v0, "CAR.CAM"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 2006
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Adding layer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to queue"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2008
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/ap;->c:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ArrayBlockingQueue;->add(Ljava/lang/Object;)Z

    .line 2010
    :cond_3
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 2018
    iget-object v1, p0, Lcom/google/android/gms/car/ap;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 2019
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ap;->c:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->clear()V

    .line 2020
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final run()V
    .locals 4

    .prologue
    .line 1955
    :cond_0
    :goto_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/ap;->e:Z

    if-eqz v0, :cond_2

    .line 1956
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1959
    const-string v0, "CAR.CAM"

    const-string v1, "Waiting to grab the next runnable in the queue"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1962
    :cond_1
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ap;->c:Ljava/util/concurrent/ArrayBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ArrayBlockingQueue;->take()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1963
    iget-object v1, p0, Lcom/google/android/gms/car/ap;->d:Ljava/lang/Object;

    monitor-enter v1
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1964
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/ap;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/am;

    .line 1965
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1966
    :try_start_2
    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 1967
    iget-object v1, p0, Lcom/google/android/gms/car/ap;->a:Lcom/google/android/gms/car/ae;

    invoke-static {v1}, Lcom/google/android/gms/car/ae;->g(Lcom/google/android/gms/car/ae;)Ljava/util/concurrent/Semaphore;

    move-result-object v1

    const-wide/16 v2, 0x1388

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/car/qh;->a(Ljava/util/concurrent/Semaphore;J)Z

    move-result v1

    .line 1969
    if-nez v1, :cond_0

    .line 1970
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Timeout in ClientConnectionThread while launching "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 1974
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/ap;->e:Z

    .line 1977
    :cond_2
    return-void

    .line 1965
    :catchall_0
    move-exception v0

    :try_start_3
    monitor-exit v1

    throw v0
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
.end method
