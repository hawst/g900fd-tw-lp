.class public final Lcom/google/android/gms/games/h/e/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/server/response/s;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic a(Lcom/google/android/gms/common/server/response/FastJsonResponse;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    .locals 5

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/gms/games/h/a/ff;

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "drive_resource_id_string"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    const-string v2, "drive_resolved_id_string"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ff;->getCoverImage()Lcom/google/android/gms/games/h/a/fg;

    move-result-object v2

    if-eqz v2, :cond_1

    const-string v3, "cover_icon_image_height"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fg;->b()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "cover_icon_image_width"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fg;->d()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "cover_icon_image_url"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/fg;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_1
    return-object p1
.end method
