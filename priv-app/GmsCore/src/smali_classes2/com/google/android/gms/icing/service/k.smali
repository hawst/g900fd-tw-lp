.class final Lcom/google/android/gms/icing/service/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;

.field final synthetic b:Lcom/google/android/gms/appdatasearch/a/h;

.field final synthetic c:Lcom/google/android/gms/icing/service/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/service/g;Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;Lcom/google/android/gms/appdatasearch/a/h;)V
    .locals 0

    .prologue
    .line 452
    iput-object p1, p0, Lcom/google/android/gms/icing/service/k;->c:Lcom/google/android/gms/icing/service/g;

    iput-object p2, p0, Lcom/google/android/gms/icing/service/k;->a:Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;

    iput-object p3, p0, Lcom/google/android/gms/icing/service/k;->b:Lcom/google/android/gms/appdatasearch/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 455
    new-instance v0, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;-><init>()V

    .line 456
    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v1, v0, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    .line 457
    iget-object v1, p0, Lcom/google/android/gms/icing/service/k;->c:Lcom/google/android/gms/icing/service/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/service/g;->b:Lcom/google/android/gms/icing/impl/e/a;

    iget-object v2, p0, Lcom/google/android/gms/icing/service/k;->a:Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Request;->b:Landroid/accounts/Account;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/e/a;->a(Landroid/accounts/Account;)Ljava/util/List;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;->b:Ljava/util/List;

    .line 459
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/icing/service/k;->b:Lcom/google/android/gms/appdatasearch/a/h;

    invoke-interface {v1, v0}, Lcom/google/android/gms/appdatasearch/a/h;->a(Lcom/google/android/gms/appdatasearch/GetRecentContextCall$Response;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 463
    :goto_0
    return-void

    .line 460
    :catch_0
    move-exception v0

    const-string v1, "Client died during getRecentContext"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
