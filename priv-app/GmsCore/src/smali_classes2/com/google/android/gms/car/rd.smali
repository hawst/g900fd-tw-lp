.class public abstract Lcom/google/android/gms/car/rd;
.super Lcom/google/android/gms/car/qp;
.source "SourceFile"


# instance fields
.field private final l:J

.field private m:Z


# direct methods
.method public constructor <init>(JZ)V
    .locals 3

    .prologue
    .line 504
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/car/qp;-><init>(JZ)V

    .line 501
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/rd;->m:Z

    .line 506
    iget-wide v0, p0, Lcom/google/android/gms/car/rd;->h:J

    iput-wide v0, p0, Lcom/google/android/gms/car/rd;->l:J

    .line 507
    return-void
.end method


# virtual methods
.method protected final a(ZJ)Lcom/google/android/gms/car/qq;
    .locals 10

    .prologue
    const-wide/16 v0, 0x0

    const/4 v8, 0x1

    .line 514
    if-eqz p1, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/car/rd;->m:Z

    if-nez v2, :cond_0

    .line 515
    iput-boolean v8, p0, Lcom/google/android/gms/car/rd;->m:Z

    .line 516
    invoke-virtual {p0}, Lcom/google/android/gms/car/rd;->a()V

    move-wide p2, v0

    .line 520
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/rd;->m:Z

    if-nez v2, :cond_3

    .line 521
    iget-wide v2, p0, Lcom/google/android/gms/car/rd;->h:J

    add-long/2addr v2, p2

    .line 522
    const-wide/16 v4, 0x3e8

    iget-wide v6, p0, Lcom/google/android/gms/car/rd;->l:J

    add-long/2addr v4, v6

    cmp-long v4, v2, v4

    if-lez v4, :cond_2

    .line 523
    const-string v4, "CAR.WM.ANIM"

    const/4 v5, 0x4

    invoke-static {v4, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 524
    const-string v4, "CAR.WM.ANIM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "timeout while waiting for initial rendering, now:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " real start time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v6, p0, Lcom/google/android/gms/car/rd;->l:J

    invoke-virtual {v2, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 527
    :cond_1
    iput-boolean v8, p0, Lcom/google/android/gms/car/rd;->m:Z

    .line 528
    invoke-virtual {p0}, Lcom/google/android/gms/car/rd;->a()V

    .line 533
    :cond_2
    :goto_0
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/rd;->b(J)Lcom/google/android/gms/car/qq;

    move-result-object v0

    return-object v0

    :cond_3
    move-wide v0, p2

    goto :goto_0
.end method

.method protected final a(J)Z
    .locals 1

    .prologue
    .line 538
    iget-boolean v0, p0, Lcom/google/android/gms/car/rd;->m:Z

    if-nez v0, :cond_0

    .line 539
    const/4 v0, 0x0

    .line 541
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/car/qp;->a(J)Z

    move-result v0

    goto :goto_0
.end method

.method protected abstract b(J)Lcom/google/android/gms/car/qq;
.end method
