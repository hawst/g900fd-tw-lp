.class public final Lcom/google/android/gms/car/ij;
.super Lcom/google/android/gms/car/nb;
.source "SourceFile"


# static fields
.field private static final a:[I


# instance fields
.field private final b:Lcom/google/android/gms/car/qd;

.field private c:Z

.field private d:Z

.field private e:Lcom/google/android/gms/car/jq;

.field private final f:Lcom/google/android/gms/car/no;

.field private final g:Lcom/google/android/gms/car/gn;

.field private final h:Lcom/google/android/gms/car/gx;

.field private final i:Landroid/os/Handler;

.field private j:Z

.field private k:Z

.field private l:Landroid/location/Location;

.field private m:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private n:J

.field private final o:Ljava/util/Random;

.field private final p:Lcom/google/android/gms/car/gi;

.field private final q:Lcom/google/android/gms/car/gg;

.field private final r:Lcom/google/android/gms/car/gh;

.field private final s:Lcom/google/android/gms/car/gf;

.field private final t:Lcom/google/android/gms/car/gj;

.field private u:Landroid/location/Location;

.field private final v:Ljava/lang/Runnable;

.field private final w:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x5

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/car/ij;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0xb
        0x9
        0x2
        0x7
        0x6
    .end array-data
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/car/nb;-><init>()V

    .line 44
    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->c:Z

    .line 45
    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->d:Z

    .line 46
    iput-object v0, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    .line 52
    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->j:Z

    .line 53
    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->k:Z

    .line 54
    iput-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    .line 56
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 57
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/car/ij;->n:J

    .line 58
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->o:Ljava/util/Random;

    .line 61
    new-instance v0, Lcom/google/android/gms/car/gi;

    invoke-direct {v0}, Lcom/google/android/gms/car/gi;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->p:Lcom/google/android/gms/car/gi;

    .line 62
    new-instance v0, Lcom/google/android/gms/car/gg;

    invoke-direct {v0}, Lcom/google/android/gms/car/gg;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->q:Lcom/google/android/gms/car/gg;

    .line 63
    new-instance v0, Lcom/google/android/gms/car/gh;

    invoke-direct {v0}, Lcom/google/android/gms/car/gh;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->r:Lcom/google/android/gms/car/gh;

    .line 64
    new-instance v0, Lcom/google/android/gms/car/gf;

    invoke-direct {v0}, Lcom/google/android/gms/car/gf;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->s:Lcom/google/android/gms/car/gf;

    .line 65
    new-instance v0, Lcom/google/android/gms/car/gj;

    invoke-direct {v0}, Lcom/google/android/gms/car/gj;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->t:Lcom/google/android/gms/car/gj;

    .line 66
    new-instance v0, Landroid/location/Location;

    const-string v1, "Car-GPS"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->u:Landroid/location/Location;

    .line 401
    new-instance v0, Lcom/google/android/gms/car/im;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/im;-><init>(Lcom/google/android/gms/car/ij;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->v:Ljava/lang/Runnable;

    .line 414
    new-instance v0, Lcom/google/android/gms/car/in;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/in;-><init>(Lcom/google/android/gms/car/ij;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->w:Ljava/lang/Runnable;

    .line 70
    iget-object v0, p2, Lcom/google/android/gms/car/gx;->b:Lcom/google/android/gms/car/qd;

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->b:Lcom/google/android/gms/car/qd;

    .line 71
    iget-object v0, p2, Lcom/google/android/gms/car/gx;->c:Lcom/google/android/gms/car/no;

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->f:Lcom/google/android/gms/car/no;

    .line 72
    iput-object p1, p0, Lcom/google/android/gms/car/ij;->g:Lcom/google/android/gms/car/gn;

    .line 73
    iput-object p2, p0, Lcom/google/android/gms/car/ij;->h:Lcom/google/android/gms/car/gx;

    .line 74
    new-instance v0, Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->f:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/ij;->i:Landroid/os/Handler;

    .line 75
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ij;J)J
    .locals 1

    .prologue
    .line 27
    iput-wide p1, p0, Lcom/google/android/gms/car/ij;->n:J

    return-wide p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->h:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ij;Lcom/google/android/gms/car/jq;)Lcom/google/android/gms/car/jq;
    .locals 0

    .prologue
    .line 27
    iput-object p1, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    return-object p1
.end method

.method private a(Landroid/location/Location;)V
    .locals 3

    .prologue
    .line 352
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 353
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "injecting location "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 355
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    iget-boolean v1, v0, Lcom/google/android/gms/car/jq;->a:Z

    if-eqz v1, :cond_2

    :try_start_0
    sget-object v1, Lcom/google/android/gms/location/p;->b:Lcom/google/android/gms/location/d;

    iget-object v0, v0, Lcom/google/android/gms/car/jq;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1, v0, p1}, Lcom/google/android/gms/location/d;->a(Lcom/google/android/gms/common/api/v;Landroid/location/Location;)Lcom/google/android/gms/common/api/am;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 359
    :cond_1
    :goto_0
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 360
    return-void

    .line 356
    :catch_0
    move-exception v0

    const-string v0, "CAR.SENSOR"

    const-string v1, "location service is dead"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/google/android/gms/car/jq;->c:I

    add-int/lit8 v1, v1, 0x1

    iput v1, v0, Lcom/google/android/gms/car/jq;->c:I

    iget v0, v0, Lcom/google/android/gms/car/jq;->c:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    const-string v0, "CAR.SENSOR"

    const-string v1, "gps data available but location service not connected"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/ij;Landroid/location/Location;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x2

    const/high16 v8, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 27
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    cmpg-float v0, v0, v4

    if-ltz v0, :cond_0

    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    const/high16 v3, 0x43b40000    # 360.0f

    cmpl-float v0, v0, v3

    if-lez v0, :cond_2

    :cond_0
    invoke-virtual {p1}, Landroid/location/Location;->getBearing()F

    move-result v0

    const/high16 v3, 0x41200000    # 10.0f

    mul-float/2addr v0, v3

    float-to-int v0, v0

    rem-int/lit16 v0, v0, 0xe10

    if-gez v0, :cond_f

    add-int/lit16 v0, v0, 0xe10

    :cond_1
    :goto_0
    int-to-float v0, v0

    const/high16 v3, 0x41200000    # 10.0f

    div-float/2addr v0, v3

    invoke-virtual {p1, v0}, Landroid/location/Location;->setBearing(F)V

    :cond_2
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    cmpg-float v0, v0, v8

    if-gtz v0, :cond_5

    :cond_3
    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "CAR.SENSOR"

    const-string v3, "Intergation error: GPS bearing is set but we are not moving."

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    invoke-virtual {p1}, Landroid/location/Location;->removeBearing()V

    :cond_5
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_6

    iget-boolean v0, p0, Lcom/google/android/gms/car/ij;->j:Z

    if-eqz v0, :cond_10

    invoke-virtual {p1}, Landroid/location/Location;->removeSpeed()V

    :cond_6
    :goto_1
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-boolean v0, p0, Lcom/google/android/gms/car/ij;->k:Z

    if-eqz v0, :cond_13

    invoke-virtual {p1}, Landroid/location/Location;->removeAccuracy()V

    :cond_7
    :goto_2
    const-string v0, "CAR.SENSOR"

    invoke-static {v0, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    const-string v0, "CAR.SENSOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cleaned location: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_8
    invoke-virtual {p1}, Landroid/location/Location;->hasAccuracy()Z

    move-result v0

    if-nez v0, :cond_9

    const v0, 0x40b570a4    # 5.67f

    invoke-virtual {p1, v0}, Landroid/location/Location;->setAccuracy(F)V

    :cond_9
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    if-eqz v0, :cond_16

    move v0, v1

    :goto_3
    const-wide/16 v4, 0x0

    if-eqz v0, :cond_a

    invoke-virtual {p1}, Landroid/location/Location;->getTime()J

    move-result-wide v4

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getTime()J

    move-result-wide v6

    sub-long/2addr v4, v6

    const-wide/16 v6, 0x1f4

    cmp-long v0, v4, v6

    if-ltz v0, :cond_17

    const-wide/16 v6, 0x7d0

    cmp-long v0, v4, v6

    if-gez v0, :cond_17

    move v0, v1

    :cond_a
    :goto_4
    if-eqz v0, :cond_d

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v0, p1}, Landroid/location/Location;->distanceTo(Landroid/location/Location;)F

    move-result v3

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v0, p1}, Landroid/location/Location;->bearingTo(Landroid/location/Location;)F

    move-result v6

    const/high16 v0, 0x40000000    # 2.0f

    cmpl-float v0, v3, v0

    if-lez v0, :cond_18

    move v0, v1

    :goto_5
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v7

    if-eqz v7, :cond_19

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v7

    cmpg-float v7, v7, v8

    if-gez v7, :cond_19

    :goto_6
    const-string v2, "CAR.SENSOR"

    invoke-static {v2, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_b

    const-string v2, "CAR.SENSOR"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Location status: distanceMeters = "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " m; direction = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v6}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " deg; maybeMoving = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "; stationary = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    invoke-virtual {p1}, Landroid/location/Location;->hasBearing()Z

    move-result v2

    if-nez v2, :cond_c

    if-nez v1, :cond_1a

    if-eqz v0, :cond_1a

    invoke-virtual {p1, v6}, Landroid/location/Location;->setBearing(F)V

    :cond_c
    :goto_7
    invoke-virtual {p1}, Landroid/location/Location;->hasSpeed()Z

    move-result v1

    if-nez v1, :cond_d

    if-eqz v0, :cond_d

    long-to-float v0, v4

    div-float v0, v3, v0

    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v0, v1

    invoke-virtual {p1, v0}, Landroid/location/Location;->setSpeed(F)V

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    iput-object p1, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    if-nez v0, :cond_e

    new-instance v0, Landroid/location/Location;

    const-string v1, "Car-GPS"

    invoke-direct {v0, v1}, Landroid/location/Location;-><init>(Ljava/lang/String;)V

    :cond_e
    iput-object v0, p0, Lcom/google/android/gms/car/ij;->u:Landroid/location/Location;

    return-void

    :cond_f
    if-gtz v0, :cond_1

    const/16 v0, 0xe10

    goto/16 :goto_0

    :cond_10
    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    const/high16 v3, 0x428c0000    # 70.0f

    cmpl-float v0, v0, v3

    if-gez v0, :cond_11

    invoke-virtual {p1}, Landroid/location/Location;->getSpeed()F

    move-result v0

    cmpg-float v0, v0, v4

    if-gez v0, :cond_6

    :cond_11
    const-string v0, "CAR.SENSOR"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_12

    const-string v0, "CAR.SENSOR"

    const-string v3, "Intergation error: GPS speed seems invalid. Disabling."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_12
    invoke-virtual {p1}, Landroid/location/Location;->removeSpeed()V

    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->j:Z

    goto/16 :goto_1

    :cond_13
    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    cmpg-float v0, v0, v8

    if-lez v0, :cond_14

    invoke-virtual {p1}, Landroid/location/Location;->getAccuracy()F

    move-result v0

    const/high16 v3, 0x43fa0000    # 500.0f

    cmpl-float v0, v0, v3

    if-ltz v0, :cond_7

    :cond_14
    const-string v0, "CAR.SENSOR"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_15

    const-string v0, "CAR.SENSOR"

    const-string v3, "Intergation error: GPS accuracy is suspicious. Disabling."

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_15
    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->k:Z

    invoke-virtual {p1}, Landroid/location/Location;->removeAccuracy()V

    goto/16 :goto_2

    :cond_16
    move v0, v2

    goto/16 :goto_3

    :cond_17
    move v0, v2

    goto/16 :goto_4

    :cond_18
    move v0, v2

    goto/16 :goto_5

    :cond_19
    move v1, v2

    goto/16 :goto_6

    :cond_1a
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->hasBearing()Z

    move-result v1

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v1}, Landroid/location/Location;->getBearing()F

    move-result v1

    invoke-virtual {p1, v1}, Landroid/location/Location;->setBearing(F)V

    goto/16 :goto_7
.end method

.method static synthetic b(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gn;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->g:Lcom/google/android/gms/car/gn;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/ij;Landroid/location/Location;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ij;->a(Landroid/location/Location;)V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/ij;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->u:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/ij;)Landroid/location/Location;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    return-object v0
.end method

.method static synthetic d()[I
    .locals 1

    .prologue
    .line 27
    sget-object v0, Lcom/google/android/gms/car/ij;->a:[I

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/ij;)Ljava/util/concurrent/atomic/AtomicBoolean;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-object v0
.end method

.method private e()Z
    .locals 4

    .prologue
    .line 365
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 366
    iget-wide v2, p0, Lcom/google/android/gms/car/ij;->n:J

    sub-long/2addr v0, v2

    const-wide/16 v2, 0x708

    cmp-long v0, v0, v2

    if-ltz v0, :cond_2

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->removeSpeed()V

    .line 369
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    const v1, 0x40fc7ae1    # 7.89f

    invoke-virtual {v0, v1}, Landroid/location/Location;->setAccuracy(F)V

    .line 370
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setElapsedRealtimeNanos(J)V

    .line 371
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-static {}, Landroid/os/SystemClock;->currentThreadTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Landroid/location/Location;->setTime(J)V

    .line 372
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Force location update: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->l:Landroid/location/Location;

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/ij;->a(Landroid/location/Location;)V

    .line 377
    :cond_1
    const/4 v0, 0x1

    .line 382
    :goto_0
    return v0

    .line 379
    :cond_2
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 380
    const-string v0, "CAR.SENSOR"

    const-string v1, "Fake location inject fired but it was up to date."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 382
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method static synthetic f(Lcom/google/android/gms/car/ij;)V
    .locals 4

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->i:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->i:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->v:Ljava/lang/Runnable;

    iget-object v2, p0, Lcom/google/android/gms/car/ij;->o:Ljava/util/Random;

    const/16 v3, 0x190

    invoke-virtual {v2, v3}, Ljava/util/Random;->nextInt(I)I

    move-result v2

    add-int/lit16 v2, v2, 0x708

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/car/ij;)J
    .locals 2

    .prologue
    .line 27
    iget-wide v0, p0, Lcom/google/android/gms/car/ij;->n:J

    return-wide v0
.end method

.method static synthetic h(Lcom/google/android/gms/car/ij;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->w:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/ij;)Z
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/car/ij;->e()Z

    move-result v0

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/ij;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->i:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->f:Lcom/google/android/gms/car/no;

    new-instance v1, Lcom/google/android/gms/car/ik;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ik;-><init>(Lcom/google/android/gms/car/ij;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/no;->a(Ljava/lang/Runnable;)V

    .line 112
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 5

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x1

    .line 116
    iget v1, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    packed-switch v1, :pswitch_data_0

    .line 142
    :pswitch_0
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unrequested event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/car/CarSensorEvent;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->f:Lcom/google/android/gms/car/no;

    new-instance v1, Lcom/google/android/gms/car/il;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/il;-><init>(Lcom/google/android/gms/car/ij;Lcom/google/android/gms/car/CarSensorEvent;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/no;->a(Ljava/lang/Runnable;)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 122
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->p:Lcom/google/android/gms/car/gi;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/gi;)Lcom/google/android/gms/car/gi;

    .line 123
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->p:Lcom/google/android/gms/car/gi;

    iget-boolean v1, v1, Lcom/google/android/gms/car/gi;->b:Z

    iget-boolean v2, p0, Lcom/google/android/gms/car/ij;->d:Z

    if-ne v2, v1, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/car/ij;->c:Z

    if-nez v2, :cond_0

    :cond_1
    const-string v2, "CAR.SENSOR"

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "CAR.SENSOR"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleDayNightEvent "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iput-boolean v0, p0, Lcom/google/android/gms/car/ij;->c:Z

    iget-object v0, p0, Lcom/google/android/gms/car/ij;->b:Lcom/google/android/gms/car/qd;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/qd;->a(Z)V

    iput-boolean v1, p0, Lcom/google/android/gms/car/ij;->d:Z

    goto :goto_0

    .line 126
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->q:Lcom/google/android/gms/car/gg;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/gg;)Lcom/google/android/gms/car/gg;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->q:Lcom/google/android/gms/car/gg;

    iget-byte v0, v0, Lcom/google/android/gms/car/gg;->a:B

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->h:Lcom/google/android/gms/car/gx;

    const-string v2, "CAR.SERVICE"

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "CAR.SERVICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "new driving status "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    new-instance v2, Lcom/google/android/gms/car/he;

    invoke-direct {v2, v1, v0}, Lcom/google/android/gms/car/he;-><init>(Lcom/google/android/gms/car/gx;I)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0, v2}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto :goto_0

    .line 130
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->s:Lcom/google/android/gms/car/gf;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/gf;)Lcom/google/android/gms/car/gf;

    .line 131
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->s:Lcom/google/android/gms/car/gf;

    iget v1, v1, Lcom/google/android/gms/car/gf;->b:F

    iget-object v2, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    const/high16 v3, 0x3f000000    # 0.5f

    cmpg-float v3, v1, v3

    if-gez v3, :cond_4

    const/high16 v3, -0x41000000    # -0.5f

    cmpl-float v1, v1, v3

    if-lez v1, :cond_4

    :goto_1
    invoke-virtual {v2, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 134
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->r:Lcom/google/android/gms/car/gh;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/gh;)Lcom/google/android/gms/car/gh;

    .line 135
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->r:Lcom/google/android/gms/car/gh;

    iget v1, v1, Lcom/google/android/gms/car/gh;->b:I

    const/16 v2, 0x65

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    .line 138
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->t:Lcom/google/android/gms/car/gj;

    invoke-virtual {p1, v1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Lcom/google/android/gms/car/gj;)Lcom/google/android/gms/car/gj;

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/car/ij;->t:Lcom/google/android/gms/car/gj;

    iget-boolean v1, v1, Lcom/google/android/gms/car/gj;->b:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/ij;->m:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    goto/16 :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_6
        :pswitch_5
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_3
    .end packed-switch
.end method

.method public final b()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->i:Landroid/os/Handler;

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 151
    sget-object v2, Lcom/google/android/gms/car/ij;->a:[I

    array-length v3, v2

    move v0, v1

    :goto_0
    if-ge v0, v3, :cond_0

    aget v4, v2, v0

    .line 152
    iget-object v5, p0, Lcom/google/android/gms/car/ij;->g:Lcom/google/android/gms/car/gn;

    invoke-virtual {v5, v4, p0}, Lcom/google/android/gms/car/gn;->a(ILcom/google/android/gms/car/na;)V

    .line 151
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 155
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    if-eqz v0, :cond_1

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->g:Lcom/google/android/gms/car/gn;

    const/16 v2, 0xa

    invoke-virtual {v0, v2, p0}, Lcom/google/android/gms/car/gn;->a(ILcom/google/android/gms/car/na;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    iget-boolean v2, v0, Lcom/google/android/gms/car/jq;->a:Z

    if-eqz v2, :cond_1

    iput-boolean v1, v0, Lcom/google/android/gms/car/jq;->a:Z

    iget-object v0, v0, Lcom/google/android/gms/car/jq;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 159
    :cond_1
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 395
    iget-object v0, p0, Lcom/google/android/gms/car/ij;->e:Lcom/google/android/gms/car/jq;

    .line 396
    return-void
.end method
