.class public final Lcom/google/android/gms/games/service/a/n/b;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 22
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/n/b;->b:Lcom/google/android/gms/games/internal/dr;

    .line 23
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/n/b;->c:Lcom/google/android/gms/games/a/au;

    .line 24
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/n/b;->d:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/b;->b:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/b;->b:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/b;->d:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/dr;->c(ILjava/lang/String;)V

    .line 38
    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/b;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/b;->d:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
