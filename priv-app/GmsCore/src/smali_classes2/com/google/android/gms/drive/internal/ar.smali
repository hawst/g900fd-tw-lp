.class public final Lcom/google/android/gms/drive/internal/ar;
.super Lcom/google/android/gms/drive/internal/bg;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/internal/bg;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 68
    const/high16 v0, 0x10000000

    if-eq p2, v0, :cond_0

    const/high16 v0, 0x20000000

    if-eq p2, v0, :cond_0

    const/high16 v0, 0x30000000

    if-eq p2, v0, :cond_0

    .line 69
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid mode provided."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 71
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/internal/as;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/drive/internal/as;-><init>(Lcom/google/android/gms/drive/internal/ar;Lcom/google/android/gms/common/api/v;ILcom/google/android/gms/drive/o;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
