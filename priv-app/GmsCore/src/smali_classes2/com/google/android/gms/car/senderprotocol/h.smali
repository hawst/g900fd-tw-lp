.class final Lcom/google/android/gms/car/senderprotocol/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/j;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/e;

.field final synthetic b:Lcom/google/android/gms/car/senderprotocol/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/senderprotocol/g;Lcom/google/android/gms/car/senderprotocol/e;)V
    .locals 0

    .prologue
    .line 292
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/h;->a:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 295
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->a(Lcom/google/android/gms/car/senderprotocol/e;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_0

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->b(Lcom/google/android/gms/car/senderprotocol/e;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->c(Lcom/google/android/gms/car/senderprotocol/e;)Z

    .line 308
    :cond_0
    :try_start_0
    new-instance v0, Lcom/google/android/c/b/r;

    invoke-direct {v0}, Lcom/google/android/c/b/r;-><init>()V

    .line 309
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v3

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/c/b/r;->mergeFrom(Lcom/google/protobuf/nano/j;[BII)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 319
    iget v0, v0, Lcom/google/android/c/b/r;->a:I

    if-nez v0, :cond_2

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/senderprotocol/e;->a(Lcom/google/android/gms/car/senderprotocol/e;I)I

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->f(Lcom/google/android/gms/car/senderprotocol/e;)Lcom/google/android/gms/car/senderprotocol/f;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/f;->a()V

    .line 328
    :goto_0
    return-void

    .line 299
    :cond_1
    const-string v0, "CAR.GAL"

    const-string v1, "Received unexpected channel control message"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->e(Lcom/google/android/gms/car/senderprotocol/e;)Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/e;->d(Lcom/google/android/gms/car/senderprotocol/e;)I

    move-result v1

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v2, v2, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {}, Lcom/google/android/gms/car/senderprotocol/e;->i()Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3, v3}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILjava/nio/ByteBuffer;ZZ)V

    goto :goto_0

    .line 312
    :catch_0
    move-exception v0

    const-string v0, "CAR.GAL"

    const-string v1, "Received invalid protocol buffer"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 325
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/e;->e(Lcom/google/android/gms/car/senderprotocol/e;)Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v1, v1, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/e;->d(Lcom/google/android/gms/car/senderprotocol/e;)I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/k;->a(I)V

    .line 326
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/h;->b:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->h()V

    goto :goto_0
.end method
