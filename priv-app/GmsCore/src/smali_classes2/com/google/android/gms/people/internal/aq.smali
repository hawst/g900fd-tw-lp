.class final Lcom/google/android/gms/people/internal/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/u;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Lcom/google/android/gms/people/model/k;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/people/model/k;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 732
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 733
    iput-object p1, p0, Lcom/google/android/gms/people/internal/aq;->a:Lcom/google/android/gms/common/api/Status;

    .line 734
    iput-object p2, p0, Lcom/google/android/gms/people/internal/aq;->b:Lcom/google/android/gms/people/model/k;

    .line 735
    iput-object p3, p0, Lcom/google/android/gms/people/internal/aq;->c:Ljava/lang/String;

    .line 736
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/gms/people/internal/aq;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final c()Lcom/google/android/gms/people/model/k;
    .locals 1

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/gms/people/internal/aq;->b:Lcom/google/android/gms/people/model/k;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 740
    iget-object v0, p0, Lcom/google/android/gms/people/internal/aq;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 755
    iget-object v0, p0, Lcom/google/android/gms/people/internal/aq;->b:Lcom/google/android/gms/people/model/k;

    if-eqz v0, :cond_0

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/people/internal/aq;->b:Lcom/google/android/gms/people/model/k;

    invoke-virtual {v0}, Lcom/google/android/gms/people/model/k;->d()V

    .line 758
    :cond_0
    return-void
.end method
