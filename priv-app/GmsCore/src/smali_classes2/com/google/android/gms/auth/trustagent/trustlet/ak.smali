.class final Lcom/google/android/gms/auth/trustagent/trustlet/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V
    .locals 0

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 145
    const-string v0, "nfc_trustlet_tech_codes"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_2

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->h()V

    .line 155
    :cond_0
    :goto_1
    return-void

    .line 146
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->i()V

    goto :goto_1

    .line 151
    :cond_3
    const-string v0, "auth_trust_agent_pref_trusted_nfc_"

    invoke-virtual {p2, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ak;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V

    goto :goto_1
.end method
