.class public final Lcom/google/android/gms/icing/as;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile b:[Lcom/google/android/gms/icing/as;


# instance fields
.field public a:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 5958
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 5959
    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/as;->a:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/as;->cachedSize:I

    .line 5960
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/as;
    .locals 2

    .prologue
    .line 5944
    sget-object v0, Lcom/google/android/gms/icing/as;->b:[Lcom/google/android/gms/icing/as;

    if-nez v0, :cond_1

    .line 5945
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 5947
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/as;->b:[Lcom/google/android/gms/icing/as;

    if-nez v0, :cond_0

    .line 5948
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/as;

    sput-object v0, Lcom/google/android/gms/icing/as;->b:[Lcom/google/android/gms/icing/as;

    .line 5950
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 5952
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/as;->b:[Lcom/google/android/gms/icing/as;

    return-object v0

    .line 5950
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 6001
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6002
    iget-object v1, p0, Lcom/google/android/gms/icing/as;->a:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_0

    .line 6003
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/as;->a:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 6006
    :cond_0
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 5970
    if-ne p1, p0, :cond_1

    .line 5980
    :cond_0
    :goto_0
    return v0

    .line 5973
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/as;

    if-nez v2, :cond_2

    move v0, v1

    .line 5974
    goto :goto_0

    .line 5976
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/as;

    .line 5977
    iget-object v2, p0, Lcom/google/android/gms/icing/as;->a:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/as;->a:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 5978
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 5985
    iget-object v0, p0, Lcom/google/android/gms/icing/as;->a:[B

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([B)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 5987
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 5938
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/as;->a:[B

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 5993
    iget-object v0, p0, Lcom/google/android/gms/icing/as;->a:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 5994
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/as;->a:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 5996
    :cond_0
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 5997
    return-void
.end method
