.class public final Lcom/google/android/gms/drive/internal/an;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/m;


# instance fields
.field final a:Lcom/google/android/gms/drive/Contents;

.field private b:Z

.field private c:Z

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/drive/Contents;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    .line 37
    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->c:Z

    .line 38
    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->d:Z

    .line 41
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/Contents;

    iput-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    .line 42
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 129
    if-nez p3, :cond_0

    .line 130
    new-instance v0, Lcom/google/android/gms/drive/ae;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ae;-><init>()V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object p3

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->d()I

    move-result v0

    const/high16 v1, 0x10000000

    if-ne v0, v1, :cond_1

    .line 133
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Cannot commit contents opened with MODE_READ_ONLY"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 135
    :cond_1
    iget v0, p3, Lcom/google/android/gms/drive/ad;->c:I

    invoke-static {v0}, Lcom/google/android/gms/drive/ad;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 137
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DriveContents must be valid for conflict detection."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 140
    :cond_2
    invoke-static {p1, p3}, Lcom/google/android/gms/drive/ad;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/ad;)V

    .line 141
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    if-eqz v0, :cond_3

    .line 142
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DriveContents already closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    if-nez v0, :cond_4

    .line 145
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Only DriveContents obtained through DriveFile.open can be committed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :cond_4
    if-eqz p2, :cond_5

    .line 152
    :goto_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    .line 153
    new-instance v0, Lcom/google/android/gms/drive/internal/ao;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/drive/internal/ao;-><init>(Lcom/google/android/gms/drive/internal/an;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0

    .line 149
    :cond_5
    sget-object p2, Lcom/google/android/gms/drive/am;->a:Lcom/google/android/gms/drive/am;

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    if-eqz v0, :cond_0

    .line 168
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "DriveContents already closed."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    .line 171
    new-instance v0, Lcom/google/android/gms/drive/internal/aq;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/aq;-><init>(Lcom/google/android/gms/drive/internal/an;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/internal/aq;

    new-instance v1, Lcom/google/android/gms/drive/internal/ap;

    invoke-direct {v1, p0}, Lcom/google/android/gms/drive/internal/ap;-><init>(Lcom/google/android/gms/drive/internal/an;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/internal/aq;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 187
    return-void
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->d()I

    move-result v0

    return v0
.end method

.method public final c()Landroid/os/ParcelFileDescriptor;
    .locals 2

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    if-eqz v0, :cond_0

    .line 57
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Contents have been closed, cannot access the output stream."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->a()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    return-object v0
.end method

.method public final d()Ljava/io/OutputStream;
    .locals 2

    .prologue
    .line 83
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    if-eqz v0, :cond_0

    .line 84
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Contents have been closed, cannot access the output stream."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 87
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->d()I

    move-result v0

    const/high16 v1, 0x20000000

    if-eq v0, v1, :cond_1

    .line 88
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getOutputStream() can only be used with contents opened with MODE_WRITE_ONLY."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->d:Z

    if-eqz v0, :cond_2

    .line 92
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getOutputStream() can only be called once per Contents instance."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 95
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->d:Z

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/Contents;->c()Ljava/io/OutputStream;

    move-result-object v0

    return-object v0
.end method

.method public final e()Lcom/google/android/gms/drive/Contents;
    .locals 1

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/an;->a:Lcom/google/android/gms/drive/Contents;

    return-object v0
.end method

.method public final f()V
    .locals 1

    .prologue
    .line 196
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    .line 197
    return-void
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 201
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/an;->b:Z

    return v0
.end method
