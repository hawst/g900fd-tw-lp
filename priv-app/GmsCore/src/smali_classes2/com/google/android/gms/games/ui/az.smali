.class final Lcom/google/android/gms/games/ui/az;
.super Lcom/google/android/gms/games/ui/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/aj;

.field private final g:I

.field private h:Landroid/widget/CheckBox;

.field private i:Ljava/lang/Boolean;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/aj;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    const/4 v0, 0x1

    .line 787
    iput-object p1, p0, Lcom/google/android/gms/games/ui/az;->a:Lcom/google/android/gms/games/ui/aj;

    .line 788
    invoke-direct {p0, p1, v1, v0, v0}, Lcom/google/android/gms/games/ui/ay;-><init>(Lcom/google/android/gms/games/ui/aj;IIZ)V

    .line 789
    iput v1, p0, Lcom/google/android/gms/games/ui/az;->g:I

    .line 790
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/az;->i:Ljava/lang/Boolean;

    .line 791
    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 795
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 796
    sget v1, Lcom/google/android/gms/j;->sl:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 797
    sget v2, Lcom/google/android/gms/j;->ui:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 798
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 799
    sget v2, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/CheckBox;

    iput-object v2, p0, Lcom/google/android/gms/games/ui/az;->h:Landroid/widget/CheckBox;

    .line 800
    iget-object v2, p0, Lcom/google/android/gms/games/ui/az;->i:Ljava/lang/Boolean;

    if-eqz v2, :cond_0

    .line 801
    iget-object v2, p0, Lcom/google/android/gms/games/ui/az;->i:Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/az;->a(Z)V

    .line 802
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/games/ui/az;->i:Ljava/lang/Boolean;

    .line 806
    :cond_0
    iget v2, p0, Lcom/google/android/gms/games/ui/az;->g:I

    packed-switch v2, :pswitch_data_0

    .line 813
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown notification ID: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/ui/az;->g:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 808
    :pswitch_0
    sget v2, Lcom/google/android/gms/p;->in:I

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(I)V

    .line 809
    sget v0, Lcom/google/android/gms/p;->io:I

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(I)V

    .line 811
    return-void

    .line 806
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 820
    iget-object v0, p0, Lcom/google/android/gms/games/ui/az;->h:Landroid/widget/CheckBox;

    if-eqz v0, :cond_0

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/games/ui/az;->h:Landroid/widget/CheckBox;

    invoke-virtual {v0, p1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 827
    :goto_0
    return-void

    .line 823
    :cond_0
    const-string v0, "GamesSettings"

    const-string v1, "LocalNotificationListItem setChecked was called before onBindView, saving requested checked state for later."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 825
    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/az;->i:Ljava/lang/Boolean;

    goto :goto_0
.end method
