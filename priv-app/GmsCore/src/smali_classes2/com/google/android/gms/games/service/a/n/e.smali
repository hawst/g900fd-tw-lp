.class public final Lcom/google/android/gms/games/service/a/n/e;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;

.field private final f:[B

.field private final g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/n/e;->c:Lcom/google/android/gms/games/internal/dr;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/n/e;->d:Lcom/google/android/gms/games/a/au;

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/n/e;->e:Ljava/lang/String;

    .line 29
    if-nez p4, :cond_0

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/n/e;->f:[B

    .line 35
    :goto_0
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/n/e;->g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    .line 36
    return-void

    .line 32
    :cond_0
    array-length v0, p4

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/n/e;->f:[B

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/e;->f:[B

    array-length v1, p4

    invoke-static {p4, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/e;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->p(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 47
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/e;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/e;->e:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/n/e;->f:[B

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/n/e;->g:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
