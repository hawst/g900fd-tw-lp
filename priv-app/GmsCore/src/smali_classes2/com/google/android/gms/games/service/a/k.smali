.class public final Lcom/google/android/gms/games/service/a/k;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field private final a:Lcom/google/android/gms/common/internal/bg;

.field private final b:I

.field private final c:Ljava/lang/String;

.field private final d:I

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[Ljava/lang/String;

.field private final h:Z

.field private final i:Z

.field private final j:I

.field private final k:Z

.field private final l:I

.field private final m:Ljava/lang/String;

.field private final n:Ljava/util/ArrayList;

.field private o:Ljava/lang/String;

.field private p:Ljava/lang/String;

.field private q:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZILjava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 112
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/k;->a:Lcom/google/android/gms/common/internal/bg;

    .line 114
    iput p2, p0, Lcom/google/android/gms/games/service/a/k;->b:I

    .line 115
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/k;->c:Ljava/lang/String;

    .line 116
    iput p4, p0, Lcom/google/android/gms/games/service/a/k;->d:I

    .line 117
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    .line 118
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    .line 119
    iput-object p7, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    .line 120
    iput-boolean p8, p0, Lcom/google/android/gms/games/service/a/k;->h:Z

    .line 121
    iput-boolean p9, p0, Lcom/google/android/gms/games/service/a/k;->i:Z

    .line 122
    iput p10, p0, Lcom/google/android/gms/games/service/a/k;->j:I

    .line 123
    iput-boolean p11, p0, Lcom/google/android/gms/games/service/a/k;->k:Z

    .line 124
    iput p12, p0, Lcom/google/android/gms/games/service/a/k;->l:I

    .line 125
    iput-object p13, p0, Lcom/google/android/gms/games/service/a/k;->m:Ljava/lang/String;

    .line 126
    iput-object p14, p0, Lcom/google/android/gms/games/service/a/k;->n:Ljava/util/ArrayList;

    .line 127
    return-void
.end method

.method private a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3

    .prologue
    .line 596
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 597
    const-string v1, "com.google.android.gms.games.GAME_ID"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 598
    const-string v1, "com.google.android.gms.games.SCOPES"

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 599
    const-string v1, "com.google.android.gms.games.SHOW_CONNECTING_POPUP"

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/k;->i:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 600
    const-string v1, "com.google.android.gms.games.CONNECTING_POPUP_GRAVITY"

    iget v2, p0, Lcom/google/android/gms/games/service/a/k;->j:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 601
    const-string v1, "com.google.android.gms.games.RETRYING_SIGN_IN"

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/k;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 602
    if-eqz p3, :cond_0

    const-string v1, "<<default account>>"

    invoke-virtual {p3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 604
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v1, p3}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string v1, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 610
    :goto_0
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 612
    return-object v0

    .line 607
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p1, v1, v2}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    const-string v1, "com.google.android.gms.games.USE_DESIRED_ACCOUNT_NAME"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;Ljava/lang/String;I)Lcom/google/android/gms/common/server/ClientContext;
    .locals 7

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x4

    const/4 v2, 0x0

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->m:Ljava/lang/String;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/games/a/t;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    if-nez v0, :cond_1

    .line 411
    invoke-direct {p0, p1, v5, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 515
    :goto_0
    return-object v0

    .line 415
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 417
    if-nez v0, :cond_1

    const-string v1, "<<default account>>"

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 421
    invoke-direct {p0, p1, v5, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 422
    goto :goto_0

    .line 428
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 429
    if-nez v3, :cond_2

    .line 431
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 432
    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 434
    goto :goto_0

    .line 435
    :cond_2
    if-eqz v0, :cond_3

    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 437
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/gms/games/i/a;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 438
    const-string v1, "ValidateServiceOp"

    const-string v3, "Stored account different from desired account"

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 439
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/k;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 440
    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 442
    goto :goto_0

    .line 446
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 447
    invoke-virtual {v3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 449
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/games/i/a;->d(Landroid/content/Context;Ljava/lang/String;)V

    .line 450
    const-string v0, "ValidateServiceOp"

    const-string v1, "Selected account does not match sign-in cache"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    invoke-direct {p0, p1, p3, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-direct {p0, p1, v4, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 453
    goto :goto_0

    .line 463
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-static {p1, p4, v3, v0}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 465
    if-eqz v1, :cond_5

    .line 467
    const/4 v0, 0x0

    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    array-length v4, v4

    if-ge v0, v4, :cond_5

    .line 468
    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/server/ClientContext;->b(Ljava/lang/String;)Z

    move-result v4

    if-nez v4, :cond_6

    move-object v1, v2

    .line 479
    :cond_5
    if-nez v1, :cond_7

    .line 480
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-direct {v0, p4, v3, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_2

    .line 485
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 486
    new-instance v1, Lcom/google/android/gms/common/server/a/a;

    const/4 v3, 0x1

    invoke-direct {v1, v0, v3}, Lcom/google/android/gms/common/server/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 488
    invoke-interface {v1, p1}, Lcom/google/android/gms/common/server/a/c;->b(Landroid/content/Context;)Ljava/lang/String;

    .line 492
    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/server/ClientContext;->a(Landroid/content/Context;)V
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_1 .. :try_end_1} :catch_2

    goto/16 :goto_0

    .line 494
    :catch_0
    move-exception v1

    move-object v6, v1

    move-object v1, v0

    move-object v0, v6

    .line 495
    :goto_2
    invoke-direct {p0, p1, v1, p3, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/auth/q;)V

    move-object v0, v2

    .line 496
    goto/16 :goto_0

    .line 467
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 498
    :catch_1
    move-exception v0

    const/4 v0, 0x7

    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 499
    goto/16 :goto_0

    .line 500
    :catch_2
    move-exception v0

    move-object v1, v0

    .line 501
    const-string v0, "ValidateServiceOp"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 502
    const/16 v0, 0x8

    .line 503
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 511
    :goto_3
    invoke-direct {p0, p1, v0, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    move-object v0, v2

    .line 512
    goto/16 :goto_0

    .line 505
    :sswitch_0
    const/16 v0, 0xb

    .line 506
    goto :goto_3

    .line 508
    :sswitch_1
    const/16 v0, 0xa

    goto :goto_3

    .line 494
    :catch_3
    move-exception v0

    goto :goto_2

    :cond_7
    move-object v0, v1

    goto/16 :goto_0

    .line 503
    nop

    :sswitch_data_0
    .sparse-switch
        0x7 -> :sswitch_0
        0x3ea -> :sswitch_1
    .end sparse-switch
.end method

.method private static a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/Game;
    .locals 3

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 706
    new-instance v1, Lcom/google/android/gms/games/a;

    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 709
    const/4 v0, 0x0

    .line 711
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 712
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 715
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    .line 717
    return-object v0

    .line 715
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    throw v0
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 520
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 523
    :goto_0
    return-object p1

    :cond_0
    iget-object p1, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    goto :goto_0
.end method

.method private a(Landroid/content/Context;I)V
    .locals 6

    .prologue
    .line 759
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->o:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->q:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k;->p:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/games/service/a/k;->d:I

    move-object v0, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 761
    return-void
.end method

.method private a(Landroid/content/Context;ILandroid/content/Intent;)V
    .locals 6

    .prologue
    .line 722
    const/4 v3, 0x0

    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 723
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/auth/q;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 529
    invoke-virtual {p2, p1}, Lcom/google/android/gms/common/server/ClientContext;->b(Landroid/content/Context;)V

    .line 532
    instance-of v0, p4, Lcom/google/android/gms/auth/ae;

    if-eqz v0, :cond_0

    .line 533
    const/4 v0, 0x4

    invoke-direct {p0, p1, p3, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 541
    :goto_0
    return-void

    .line 540
    :cond_0
    const/16 v0, 0x8

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/service/l;Ljava/util/Map;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 377
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/l;->D()Lcom/google/android/gms/games/service/b;

    move-result-object v0

    .line 378
    if-nez v0, :cond_0

    move v0, v1

    .line 398
    :goto_0
    return v0

    .line 384
    :cond_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    .line 385
    invoke-interface {v2}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    move v0, v1

    .line 386
    goto :goto_0

    .line 391
    :cond_1
    invoke-interface {p1}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/c;

    .line 392
    invoke-interface {v2, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/c;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 393
    goto :goto_0

    .line 398
    :cond_3
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 329
    return-void
.end method

.method final a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 727
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 730
    sparse-switch p2, :sswitch_data_0

    .line 740
    :goto_0
    if-eqz p4, :cond_0

    .line 745
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    .line 746
    const/high16 v1, 0x8000000

    invoke-static {p1, v0, p4, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    .line 748
    const-string v1, "pendingIntent"

    invoke-virtual {p5, v1, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 751
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->a:Lcom/google/android/gms/common/internal/bg;

    invoke-interface {v0, p2, p3, p5}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 756
    :goto_1
    return-void

    .line 732
    :sswitch_0
    const/4 v0, 0x2

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 735
    :sswitch_1
    const/4 v0, 0x3

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;I)V

    goto :goto_0

    .line 752
    :catch_0
    move-exception v0

    .line 753
    const-string v1, "ValidateServiceOp"

    const-string v2, "When providing result "

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 730
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_1
        0x4 -> :sswitch_0
    .end sparse-switch
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 10

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    const-string v1, "com.google.android.gms.games.APP_ID"

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 134
    if-nez v6, :cond_4

    .line 135
    const-string v6, ""

    .line 153
    :goto_0
    iput-object v6, p0, Lcom/google/android/gms/games/service/a/k;->q:Ljava/lang/String;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->o:Ljava/lang/String;

    .line 155
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "593950602418"

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->o:Ljava/lang/String;

    .line 163
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->p:Ljava/lang/String;

    .line 164
    const-string v0, "<<default account>>"

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 165
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->p:Ljava/lang/String;

    .line 171
    :cond_2
    :goto_1
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;I)V

    .line 177
    invoke-static {p1}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 178
    new-instance v0, Lcom/google/android/gms/common/ui/i;

    sget v1, Lcom/google/android/gms/p;->eu:I

    sget v2, Lcom/google/android/gms/p;->eF:I

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/common/ui/i;-><init>(Landroid/content/Context;II)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/ui/i;->a()Lcom/google/android/gms/common/ui/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/common/ui/i;->a:Landroid/content/Intent;

    .line 183
    const/4 v1, 0x6

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    .line 326
    :cond_3
    :goto_2
    return-void

    .line 140
    :cond_4
    :try_start_0
    invoke-static {v6}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 142
    :catch_0
    move-exception v0

    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Application ID ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") must be a numeric value. Please verify that your manifest refers to the correct project ID."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_2

    .line 166
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->m:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->p:Ljava/lang/String;

    goto :goto_1

    .line 189
    :cond_6
    iget v0, p0, Lcom/google/android/gms/games/service/a/k;->b:I

    .line 190
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 191
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    .line 196
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    const-string v3, "https://www.googleapis.com/auth/games.firstparty"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 197
    if-nez v1, :cond_7

    .line 198
    const-string v0, "ValidateServiceOp"

    const-string v1, "External caller attempting to use the 1P scope"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 199
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_2

    .line 206
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    .line 207
    if-nez v1, :cond_8

    .line 208
    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Caller attempting to connect as another package (calling package: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", attempting to connect as: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 217
    :cond_8
    :try_start_1
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 219
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 229
    :cond_9
    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    if-nez v1, :cond_a

    .line 230
    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Using Google Play games services requires a metadata tag with the name \"com.google.android.gms.games.APP_ID\" in the application tag of the manifest for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 233
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 220
    :catch_1
    move-exception v0

    .line 221
    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 222
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 238
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/gms/games/service/a/k;->h:Z

    if-eqz v1, :cond_e

    .line 239
    iget v1, p0, Lcom/google/android/gms/games/service/a/k;->b:I

    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v2

    if-eq v1, v2, :cond_b

    const-string v0, "ValidateServiceOp"

    const-string v1, "Headless Client must be started from your process."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    if-eqz v1, :cond_c

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_d

    :cond_c
    const-string v0, "ValidateServiceOp"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Headless Client requires a valid account (account: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x5

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    const/4 v0, 0x0

    :goto_3
    move-object v8, v0

    .line 244
    :goto_4
    if-eqz v8, :cond_3

    .line 249
    invoke-virtual {v8}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/k;->p:Ljava/lang/String;

    .line 250
    iget v0, p0, Lcom/google/android/gms/games/service/a/k;->l:I

    invoke-static {v8, v0}, Lcom/google/android/gms/games/h/b;->a(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 253
    invoke-virtual {p2, p1, v8, v6}, Lcom/google/android/gms/games/a/t;->h(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v7

    .line 261
    :try_start_2
    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 262
    invoke-virtual {p2, p1, v0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_2 .. :try_end_2} :catch_3

    move-result v0

    .line 274
    if-nez v0, :cond_f

    .line 275
    const-string v0, "ValidateServiceOp"

    const-string v1, "The installed version of Google Play services is out-of-date; it must be updated to communicate with the Play Games service."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 239
    :cond_d
    new-instance v1, Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    move-object v0, v1

    goto :goto_3

    .line 241
    :cond_e
    invoke-direct {p0, p1, p2, v6, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;Ljava/lang/String;I)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    move-object v8, v0

    goto :goto_4

    .line 263
    :catch_2
    move-exception v0

    .line 264
    invoke-direct {p0, p1, v8, v6, v0}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/auth/q;)V

    goto/16 :goto_2

    .line 266
    :catch_3
    move-exception v0

    .line 267
    const-string v1, "ValidateServiceOp"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error from revision check: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    const/16 v0, 0x8

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 282
    :cond_f
    invoke-static {v8}, Lcom/google/android/gms/games/service/k;->a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/l;

    move-result-object v4

    .line 283
    if-eqz v4, :cond_10

    invoke-virtual {v4}, Lcom/google/android/gms/games/service/l;->z()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/k;->h:Z

    invoke-virtual {v4}, Lcom/google/android/gms/games/service/l;->A()Z

    move-result v1

    if-eq v0, v1, :cond_11

    .line 285
    :cond_10
    iget-boolean v5, p0, Lcom/google/android/gms/games/service/a/k;->h:Z

    iget-object v9, v7, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, v7, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, v7, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/games/service/l;

    invoke-virtual {v9}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget v3, p0, Lcom/google/android/gms/games/service/a/k;->d:I

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/l;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Z)V

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_12

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    const/4 v3, 0x1

    invoke-virtual {p2, v9, v2, v1, v3}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z

    move-result v1

    if-nez v1, :cond_12

    const/4 v0, 0x0

    :goto_5
    move-object v4, v0

    .line 290
    :cond_11
    if-nez v4, :cond_13

    .line 291
    const/16 v0, 0xa

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto/16 :goto_2

    .line 285
    :cond_12
    invoke-static {v2, v0}, Lcom/google/android/gms/games/service/k;->a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;)V

    goto :goto_5

    .line 297
    :cond_13
    new-instance v9, Ljava/util/HashMap;

    invoke-direct {v9}, Ljava/util/HashMap;-><init>()V

    sget-object v0, Lcom/google/android/gms/drive/b;->c:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    sget-object v0, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    new-instance v1, Lcom/google/android/gms/drive/g;

    invoke-direct {v1}, Lcom/google/android/gms/drive/g;-><init>()V

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/g;->a(I)Lcom/google/android/gms/drive/g;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/g;->a()Lcom/google/android/gms/drive/f;

    move-result-object v1

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->n:Ljava/util/ArrayList;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->n:Ljava/util/ArrayList;

    const-string v1, "copresence"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_15

    sget-object v0, Lcom/google/android/gms/location/copresence/f;->b:Lcom/google/android/gms/common/api/c;

    const/4 v1, 0x0

    invoke-interface {v9, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_15
    invoke-virtual {v4}, Lcom/google/android/gms/games/service/l;->C()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v9, v0}, Ljava/util/Map;->putAll(Ljava/util/Map;)V

    .line 298
    invoke-interface {v9}, Ljava/util/Map;->size()I

    move-result v0

    if-lez v0, :cond_1b

    invoke-static {v4, v9}, Lcom/google/android/gms/games/service/a/k;->a(Lcom/google/android/gms/games/service/l;Ljava/util/Map;)Z

    move-result v0

    if-nez v0, :cond_1b

    .line 299
    invoke-virtual {v4, v9}, Lcom/google/android/gms/games/service/l;->a(Ljava/util/Map;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    invoke-interface {v9, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_16

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    move-object v7, v0

    .line 301
    :goto_6
    new-instance v0, Lcom/google/android/gms/games/service/a/l;

    move-object v1, p0

    move-object v2, p1

    move-object v3, v8

    move-object v5, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/service/a/l;-><init>(Lcom/google/android/gms/games/service/a/k;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/games/a/t;Ljava/lang/String;)V

    .line 303
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, p1, v0, v0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    invoke-virtual {v8}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/common/api/w;->a([Ljava/lang/String;)Lcom/google/android/gms/common/api/w;

    move-result-object v3

    new-instance v1, Landroid/os/Handler;

    iget-object v2, v0, Lcom/google/android/gms/games/service/a/l;->a:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    const-string v2, "Handler must not be null"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Landroid/os/Handler;->getLooper()Landroid/os/Looper;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/common/api/w;->c:Landroid/os/Looper;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k;->f:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    .line 309
    invoke-interface {v9}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_7
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_19

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/Map$Entry;

    .line 310
    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-nez v2, :cond_18

    .line 311
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/c;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    goto :goto_7

    .line 300
    :cond_16
    sget-object v1, Lcom/google/android/gms/drive/b;->c:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k;->g:[Ljava/lang/String;

    invoke-static {v2, v1}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_17

    move-object v7, v0

    goto :goto_6

    :cond_17
    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/h;->b([Ljava/lang/Object;Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    move-object v7, v0

    goto :goto_6

    .line 313
    :cond_18
    invoke-interface {v1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/c;

    invoke-interface {v1}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/api/e;

    invoke-virtual {v3, v2, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    goto :goto_7

    .line 317
    :cond_19
    invoke-virtual {v3}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v1, v0, Lcom/google/android/gms/games/service/a/l;->b:Lcom/google/android/gms/common/api/v;

    if-nez v1, :cond_1a

    const/4 v1, 0x1

    :goto_8
    const-string v3, "Cannot re-use a connector!"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    iput-object v2, v0, Lcom/google/android/gms/games/service/a/l;->b:Lcom/google/android/gms/common/api/v;

    iget-object v0, v0, Lcom/google/android/gms/games/service/a/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto/16 :goto_2

    :cond_1a
    const/4 v1, 0x0

    goto :goto_8

    .line 325
    :cond_1b
    invoke-virtual {p0, v7, v4, p2}, Lcom/google/android/gms/games/service/a/k;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/games/a/t;)V

    goto/16 :goto_2
.end method

.method final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/games/a/t;)V
    .locals 10

    .prologue
    .line 617
    const-string v0, "Do not validate on the main thread!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->c(Ljava/lang/String;)V

    .line 618
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 619
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 620
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 621
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    .line 623
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/k;->h:Z

    if-nez v0, :cond_7

    .line 625
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 627
    new-instance v2, Lcom/google/android/gms/games/t;

    new-instance v6, Lcom/google/android/gms/games/a/o;

    invoke-direct {v6, v1}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v6, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 630
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->c()I

    move-result v0

    if-nez v0, :cond_0

    .line 635
    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 636
    const/4 v0, 0x4

    const/4 v3, 0x0

    invoke-direct {p0, v1, v4, v3}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v3

    invoke-direct {p0, v1, v0, v3}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 643
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    .line 694
    :goto_0
    return-void

    .line 640
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 641
    invoke-static {v0}, Lcom/google/android/gms/games/PlayerRef;->a(Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v6

    .line 643
    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    .line 645
    invoke-virtual {p2, v6}, Lcom/google/android/gms/games/service/l;->a(Landroid/content/ContentValues;)V

    .line 647
    iget v2, p0, Lcom/google/android/gms/games/service/a/k;->d:I

    invoke-static {v2}, Lcom/google/android/gms/common/util/x;->c(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 648
    const-string v2, "com.google.android.gms.games.current_player"

    invoke-virtual {v5, v2, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 652
    :cond_1
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    const-string v0, "593950602418"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 654
    invoke-static {p1}, Lcom/google/android/gms/games/service/a/k;->a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/Game;

    move-result-object v2

    .line 655
    if-nez v2, :cond_2

    .line 657
    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 658
    const/4 v0, 0x4

    const/4 v2, 0x0

    invoke-direct {p0, v1, v4, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-direct {p0, v1, v0, v2}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/content/Intent;)V

    goto :goto_0

    .line 643
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/t;->w_()V

    throw v0

    .line 663
    :cond_2
    iget v0, p0, Lcom/google/android/gms/games/service/a/k;->d:I

    invoke-static {v0}, Lcom/google/android/gms/common/util/x;->c(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 664
    const-string v6, "com.google.android.gms.games.current_game"

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v5, v6, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 666
    :cond_3
    invoke-static {v2}, Lcom/google/android/gms/games/GameRef;->a(Lcom/google/android/gms/games/Game;)Landroid/content/ContentValues;

    move-result-object v0

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/l;->b(Landroid/content/ContentValues;)V

    .line 670
    :cond_4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 671
    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/i/a;->c(Landroid/content/Context;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/i/a;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/i/a;->f(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_8

    const/4 v0, 0x1

    .line 672
    :goto_1
    invoke-virtual {p3, p1}, Lcom/google/android/gms/games/a/t;->p(Lcom/google/android/gms/games/a/au;)J

    move-result-wide v6

    .line 673
    const-wide/16 v8, -0x1

    cmp-long v2, v6, v8

    if-eqz v2, :cond_5

    .line 675
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 676
    const-wide/32 v8, 0xdbba00

    cmp-long v2, v6, v8

    if-ltz v2, :cond_9

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v0, v2

    .line 680
    :cond_5
    if-eqz v0, :cond_6

    const-string v0, "593950602418"

    invoke-virtual {v0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 681
    const-string v0, "show_welcome_popup"

    const/4 v2, 0x1

    invoke-virtual {v5, v0, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 682
    invoke-static {v1, v3}, Lcom/google/android/gms/games/i/a;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 686
    :cond_6
    invoke-virtual {p2}, Lcom/google/android/gms/games/service/l;->G()V

    .line 691
    :cond_7
    invoke-virtual {p2}, Lcom/google/android/gms/games/service/l;->E()V

    .line 693
    const/4 v2, 0x0

    const/4 v4, 0x0

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 671
    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    .line 676
    :cond_9
    const/4 v2, 0x0

    goto :goto_2
.end method
