.class final Lcom/google/android/gms/games/a/k;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Lcom/google/android/gms/games/h/a/q;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/k;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 46
    const-string v0, "AclAgent"

    sget-object v1, Lcom/google/android/gms/games/a/k;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 47
    new-instance v0, Lcom/google/android/gms/games/h/a/q;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/q;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/k;->b:Lcom/google/android/gms/games/h/a/q;

    .line 48
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 272
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v0

    .line 273
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 274
    const-string v0, "AclAgent"

    const-string v1, "Error resolving external game Id."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-void

    .line 277
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/games/provider/ab;->a(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 278
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 279
    const-string v2, "gameplay_acl_status"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 280
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 176
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V

    .line 177
    return-void
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;)I
    .locals 9

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    .line 153
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/games/a/k;->b:Lcom/google/android/gms/games/h/a/q;

    new-instance v4, Lcom/google/android/gms/games/h/a/p;

    invoke-direct {v4, p3}, Lcom/google/android/gms/games/h/a/p;-><init>(Ljava/lang/String;)V

    const-string v5, "applications/%1$s/acls/gameplay"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {p4}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v3, v3, Lcom/google/android/gms/games/h/a/q;->a:Lcom/google/android/gms/common/server/n;

    const/4 v6, 0x2

    invoke-virtual {v3, p2, v6, v5, v4}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 155
    :try_start_1
    invoke-static {p3}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/common/people/a/f;->a([B)Lcom/google/android/gms/common/people/a/f;

    move-result-object v4

    move v3, v0

    :goto_0
    invoke-virtual {v4}, Lcom/google/android/gms/common/people/a/f;->c()I

    move-result v5

    if-ge v3, v5, :cond_2

    invoke-virtual {v4, v3}, Lcom/google/android/gms/common/people/a/f;->a(I)Lcom/google/android/gms/common/people/a/h;

    move-result-object v5

    iget-boolean v6, v5, Lcom/google/android/gms/common/people/a/h;->d:Z

    if-eqz v6, :cond_1

    iget v5, v5, Lcom/google/android/gms/common/people/a/h;->e:I

    if-ne v5, v2, :cond_1

    :goto_1
    if-eqz v2, :cond_0

    const/4 v1, 0x3

    :cond_0
    invoke-static {p1, p2, p4, v1}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    .line 162
    :goto_2
    return v0

    .line 155
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    :cond_2
    move v2, v0

    goto :goto_1

    :catch_0
    move-exception v1

    :try_start_2
    const-string v1, "AclAgent"

    const-string v2, "Error parsing Gameplay ACL SharingRoster protobuf."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 157
    :catch_1
    move-exception v0

    .line 158
    const-string v1, "AclAgent"

    const-string v2, "Failed to send Gameplay ACL data over the network"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 160
    const-string v1, "AclAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 162
    :cond_3
    const/4 v0, 0x6

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)I
    .locals 4

    .prologue
    .line 88
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/k;->b:Lcom/google/android/gms/games/h/a/q;

    new-instance v1, Lcom/google/android/gms/games/h/a/p;

    invoke-direct {v1, p2}, Lcom/google/android/gms/games/h/a/p;-><init>(Ljava/lang/String;)V

    const-string v2, "players/me/acls/notify"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/q;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x2

    invoke-virtual {v0, p1, v3, v2, v1}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 89
    const/4 v0, 0x0

    .line 95
    :goto_0
    return v0

    .line 90
    :catch_0
    move-exception v0

    .line 91
    const-string v1, "AclAgent"

    const-string v2, "Failed to send Notify ACL data over the network"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 93
    const-string v1, "AclAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 95
    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/k;->b:Lcom/google/android/gms/games/h/a/q;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v3, "players/me/acls/notify"

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/q;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/o;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/o;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 71
    sget-object v1, Lcom/google/android/gms/games/provider/q;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    invoke-virtual {v0, v6}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_0
    return-object v0

    .line 64
    :catch_0
    move-exception v0

    .line 65
    const-string v1, "AclAgent"

    const-string v2, "Failed to retrieve Notify ACL from the network"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 66
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 67
    const-string v1, "AclAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 69
    :cond_1
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 103
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/k;->b:Lcom/google/android/gms/games/h/a/q;

    invoke-static {p1}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "applications/%1$s/acls/gameplay"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v1, :cond_0

    const-string v2, "language"

    invoke-static {v1}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v2, v1}, Lcom/google/android/gms/games/h/a/q;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/q;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/o;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/o;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_1

    .line 105
    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v2, "pacl"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/util/m;->b(Ljava/lang/String;)[B

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/common/people/data/c;->a([B)Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/people/data/AudienceMember;

    invoke-virtual {v1}, Lcom/google/android/gms/common/people/data/AudienceMember;->j()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v6

    :goto_0
    if-eqz v1, :cond_3

    const/4 v1, 0x3

    :goto_1
    const-string v2, "AclAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Setting gameplay ACL status to "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, p2, p3, v1}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;I)V
    :try_end_1
    .catch Lcom/google/protobuf/a/e; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_1

    .line 106
    :goto_2
    :try_start_2
    sget-object v1, Lcom/google/android/gms/games/provider/q;->a:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 115
    :goto_3
    return-object v0

    :cond_2
    move v1, v7

    .line 105
    goto :goto_0

    :cond_3
    const/4 v1, 0x2

    goto :goto_1

    :catch_0
    move-exception v1

    const-string v1, "AclAgent"

    const-string v2, "Error parsing Gameplay ACL RenderedSharingRoster protobuf."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_2

    .line 110
    :catch_1
    move-exception v0

    .line 111
    const-string v1, "AclAgent"

    const-string v2, "Failed to retrieve Gameplay ACL from the network"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 113
    const-string v1, "AclAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 115
    :cond_4
    const/4 v0, 0x4

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_3
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Z)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 133
    invoke-static {p2, p3}, Lcom/google/android/gms/games/provider/ab;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v2, "gameplay_acl_status"

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v2

    .line 136
    const/4 v1, 0x0

    .line 137
    if-eqz p4, :cond_0

    const/4 v3, 0x3

    if-ne v2, v3, :cond_1

    :cond_0
    if-nez p4, :cond_3

    const/4 v3, 0x2

    if-eq v2, v3, :cond_3

    .line 139
    :cond_1
    if-eqz p4, :cond_2

    .line 140
    const-string v1, "AclAgent"

    const-string v2, "Gameplay ACL appears to now be public, updating status..."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    :goto_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/games/a/k;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    .line 147
    :goto_1
    return v0

    .line 142
    :cond_2
    const-string v1, "AclAgent"

    const-string v2, "Gameplay ACL no longer appears to be public, updating status..."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_1
.end method
