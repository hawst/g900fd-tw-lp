.class public abstract Lcom/google/android/gms/auth/trustagent/trustlet/ap;
.super Lcom/google/android/gms/auth/trustagent/trustlet/bh;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/BroadcastReceiver;

.field private b:Landroid/content/SharedPreferences;

.field d:Z

.field e:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;-><init>()V

    return-void
.end method

.method private a(I)V
    .locals 2

    .prologue
    .line 126
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 127
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a(I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->m()V

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V
    .locals 1

    .prologue
    .line 25
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->c(Z)V

    return-void
.end method

.method private c(Z)V
    .locals 2

    .prologue
    .line 208
    new-instance v0, Lcom/google/android/gms/auth/e/f;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->e()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->d:I

    .line 210
    if-eqz p1, :cond_0

    .line 211
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->e:I

    .line 215
    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/f;->a()V

    .line 216
    return-void

    .line 213
    :cond_0
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->e:I

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)Z
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->l()Z

    move-result v0

    return v0
.end method

.method private h()Ljava/lang/String;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_unlock_attempt_count_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private i()I
    .locals 3

    .prologue
    .line 131
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->h()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x3

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private l()Z
    .locals 2

    .prologue
    .line 148
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->i()I

    move-result v0

    const/4 v1, 0x3

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private m()V
    .locals 1

    .prologue
    .line 192
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d:Z

    if-eqz v0, :cond_0

    .line 193
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d:Z

    .line 194
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->f()V

    .line 196
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(Z)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 64
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->l()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 65
    const-string v0, "Coffee - OneTimeAuthTrustlet"

    const-string v1, "handleUserAuthentication called when max number of unlock attempts reached."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_0
    :goto_0
    return-void

    .line 69
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d:Z

    if-nez v0, :cond_2

    .line 70
    const-string v0, "Coffee - OneTimeAuthTrustlet"

    const-string v1, "handleUserAuthentication call unexpected (and ignored)."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 73
    :cond_2
    if-eqz p1, :cond_3

    .line 76
    const-string v0, "Successfully authenticated using one time trustlet."

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 77
    invoke-direct {p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->c(Z)V

    .line 78
    iput-boolean v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->e:Z

    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->m()V

    goto :goto_0

    .line 81
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->i()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a(I)V

    .line 82
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->l()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "Coffee - OneTimeAuthTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Maximum number of failed unlock attempts for trustlet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", disabled until device unlocked."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->m()V

    goto :goto_0
.end method

.method protected abstract a()Z
.end method

.method protected b()V
    .locals 3

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b:Landroid/content/SharedPreferences;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b:Landroid/content/SharedPreferences;

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->h()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 94
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a(I)V

    .line 96
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_1

    const-string v0, "Coffee - OneTimeAuthTrustlet"

    const-string v1, "Registering broadcast receiver more than once."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :goto_0
    return-void

    .line 96
    :cond_1
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/aq;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aq;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method protected c()V
    .locals 2

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_0

    const-string v0, "Coffee - OneTimeAuthTrustlet"

    const-string v1, "Attempting to unregister broadcast receiver when none is registered."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    :goto_0
    return-void

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a:Landroid/content/BroadcastReceiver;

    goto :goto_0
.end method

.method protected abstract f()V
.end method
