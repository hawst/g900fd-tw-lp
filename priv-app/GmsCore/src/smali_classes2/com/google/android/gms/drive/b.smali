.class public final Lcom/google/android/gms/drive/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/Scope;

.field public static final c:Lcom/google/android/gms/common/api/Scope;

.field public static final d:Lcom/google/android/gms/common/api/Scope;

.field public static final e:Lcom/google/android/gms/common/api/Scope;

.field public static final f:Lcom/google/android/gms/common/api/c;

.field public static final g:Lcom/google/android/gms/common/api/c;

.field public static final h:Lcom/google/android/gms/drive/h;

.field public static final i:Lcom/google/android/gms/drive/p;

.field public static final j:Lcom/google/android/gms/drive/ab;

.field public static final k:Lcom/google/android/gms/drive/w;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 42
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b;->a:Lcom/google/android/gms/common/api/j;

    .line 72
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/drive.file"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->b:Lcom/google/android/gms/common/api/Scope;

    .line 80
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/drive.appdata"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->c:Lcom/google/android/gms/common/api/Scope;

    .line 90
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/drive"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->d:Lcom/google/android/gms/common/api/Scope;

    .line 100
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/drive.apps"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->e:Lcom/google/android/gms/common/api/Scope;

    .line 166
    new-instance v0, Lcom/google/android/gms/common/api/c;

    new-instance v1, Lcom/google/android/gms/drive/c;

    invoke-direct {v1}, Lcom/google/android/gms/drive/c;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/b;->a:Lcom/google/android/gms/common/api/j;

    new-array v3, v4, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->f:Lcom/google/android/gms/common/api/c;

    .line 175
    new-instance v0, Lcom/google/android/gms/common/api/c;

    new-instance v1, Lcom/google/android/gms/drive/d;

    invoke-direct {v1}, Lcom/google/android/gms/drive/d;-><init>()V

    sget-object v2, Lcom/google/android/gms/drive/b;->a:Lcom/google/android/gms/common/api/j;

    new-array v3, v4, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    .line 187
    new-instance v0, Lcom/google/android/gms/drive/internal/o;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/o;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    .line 194
    new-instance v0, Lcom/google/android/gms/drive/internal/at;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/at;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b;->i:Lcom/google/android/gms/drive/p;

    .line 201
    new-instance v0, Lcom/google/android/gms/drive/internal/bo;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/bo;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b;->j:Lcom/google/android/gms/drive/ab;

    .line 207
    new-instance v0, Lcom/google/android/gms/drive/internal/bf;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/bf;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/b;->k:Lcom/google/android/gms/drive/w;

    return-void
.end method
