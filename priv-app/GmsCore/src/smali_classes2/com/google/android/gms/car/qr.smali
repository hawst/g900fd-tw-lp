.class final Lcom/google/android/gms/car/qr;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/qo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/qo;Landroid/os/Looper;)V
    .locals 1

    .prologue
    .line 368
    iput-object p1, p0, Lcom/google/android/gms/car/qr;->a:Lcom/google/android/gms/car/qo;

    .line 369
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 370
    return-void
.end method


# virtual methods
.method public final a(J)V
    .locals 1

    .prologue
    .line 373
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/qr;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    .line 374
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/car/qr;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 375
    return-void
.end method

.method public final a(JLcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 378
    const/4 v0, 0x2

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/car/qr;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 379
    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/car/qr;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 380
    return-void
.end method

.method public final a(ZZ)V
    .locals 1

    .prologue
    .line 383
    if-eqz p1, :cond_0

    .line 384
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/qr;->removeMessages(I)V

    .line 386
    :cond_0
    if-eqz p1, :cond_1

    if-eqz p2, :cond_1

    .line 387
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/qr;->removeMessages(I)V

    .line 389
    :cond_1
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 397
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 404
    :goto_0
    return-void

    .line 399
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/car/qr;->a:Lcom/google/android/gms/car/qo;

    invoke-static {v0}, Lcom/google/android/gms/car/qo;->a(Lcom/google/android/gms/car/qo;)V

    goto :goto_0

    .line 402
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 403
    iget-object v1, p0, Lcom/google/android/gms/car/qr;->a:Lcom/google/android/gms/car/qo;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/qo;->a(Lcom/google/android/gms/car/qo;Lcom/google/android/gms/car/oi;)V

    goto :goto_0

    .line 397
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
