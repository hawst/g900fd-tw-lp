.class public final Lcom/google/android/gms/car/pa;
.super Lcom/google/android/gms/car/oi;
.source "SourceFile"


# instance fields
.field private k:Landroid/graphics/Bitmap;

.field private l:Lcom/google/android/gms/car/on;

.field private m:I

.field private n:I

.field private o:I

.field private p:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 23
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/car/oi;-><init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V

    .line 16
    iput v0, p0, Lcom/google/android/gms/car/pa;->m:I

    .line 17
    iput v0, p0, Lcom/google/android/gms/car/pa;->n:I

    .line 18
    iput v0, p0, Lcom/google/android/gms/car/pa;->o:I

    .line 19
    iput v0, p0, Lcom/google/android/gms/car/pa;->p:I

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/car/pa;->l:Lcom/google/android/gms/car/on;

    .line 25
    return-void
.end method

.method private y()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 60
    iget v0, p0, Lcom/google/android/gms/car/pa;->m:I

    if-eqz v0, :cond_0

    .line 61
    new-array v0, v3, [I

    iget v1, p0, Lcom/google/android/gms/car/pa;->m:I

    aput v1, v0, v2

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    .line 62
    iput v2, p0, Lcom/google/android/gms/car/pa;->m:I

    .line 64
    :cond_0
    return-void
.end method


# virtual methods
.method protected final a(F[F)V
    .locals 9

    .prologue
    const/16 v4, 0x14

    const/4 v1, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v3, 0x0

    .line 74
    iget v0, p0, Lcom/google/android/gms/car/pa;->m:I

    if-nez v0, :cond_0

    .line 75
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/car/oi;->a(F[F)V

    .line 105
    :goto_0
    return-void

    .line 78
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/car/on;->c(I)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/car/on;->b(I)Lcom/google/android/gms/car/ot;

    move-result-object v6

    .line 81
    const/16 v0, 0xbe2

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    .line 82
    iget v0, p0, Lcom/google/android/gms/car/pa;->p:I

    invoke-static {v0, p1}, Landroid/opengl/GLES20;->glUniform1f(IF)V

    .line 83
    const/16 v0, 0x302

    const/16 v2, 0x303

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBlendFunc(II)V

    .line 84
    iget v0, v6, Lcom/google/android/gms/car/ot;->b:I

    invoke-static {v0, v7, v3, p2, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 86
    const v0, 0x84c0

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 87
    const v0, 0x8d65

    iget v2, p0, Lcom/google/android/gms/car/pa;->d:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 88
    iget v0, p0, Lcom/google/android/gms/car/pa;->n:I

    invoke-static {v0, v3}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 90
    const v0, 0x84c1

    invoke-static {v0}, Landroid/opengl/GLES20;->glActiveTexture(I)V

    .line 91
    const/16 v0, 0xde1

    iget v2, p0, Lcom/google/android/gms/car/pa;->m:I

    invoke-static {v0, v2}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 92
    iget v0, p0, Lcom/google/android/gms/car/pa;->o:I

    invoke-static {v0, v7}, Landroid/opengl/GLES20;->glUniform1i(II)V

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v3}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 95
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/pa;->e:Ljava/nio/FloatBuffer;

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 97
    iget v0, v6, Lcom/google/android/gms/car/ot;->d:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->e:Ljava/nio/FloatBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/FloatBuffer;->position(I)Ljava/nio/Buffer;

    .line 100
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    const/16 v2, 0x1406

    iget-object v5, p0, Lcom/google/android/gms/car/pa;->e:Ljava/nio/FloatBuffer;

    move v1, v8

    invoke-static/range {v0 .. v5}, Landroid/opengl/GLES20;->glVertexAttribPointer(IIIZILjava/nio/Buffer;)V

    .line 102
    iget v0, v6, Lcom/google/android/gms/car/ot;->e:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnableVertexAttribArray(I)V

    .line 103
    iget v0, v6, Lcom/google/android/gms/car/ot;->c:I

    iget-object v1, p0, Lcom/google/android/gms/car/pa;->f:[F

    invoke-static {v0, v7, v3, v1, v3}, Landroid/opengl/GLES20;->glUniformMatrix4fv(IIZ[FI)V

    .line 104
    const/4 v0, 0x5

    const/4 v1, 0x4

    invoke-static {v0, v3, v1}, Landroid/opengl/GLES20;->glDrawArrays(III)V

    goto :goto_0
.end method

.method public final declared-synchronized a(Landroid/graphics/Bitmap;)V
    .locals 1

    .prologue
    .line 29
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/pa;->k:Landroid/graphics/Bitmap;

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->l:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/pa;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 31
    monitor-exit p0

    return-void

    .line 29
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized l()V
    .locals 1

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/pa;->y()V

    .line 69
    invoke-super {p0}, Lcom/google/android/gms/car/oi;->l()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 70
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized x()V
    .locals 4

    .prologue
    .line 35
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/pa;->y()V

    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [I

    .line 37
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    .line 39
    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/car/pa;->m:I

    .line 40
    const/16 v0, 0xde1

    iget v1, p0, Lcom/google/android/gms/car/pa;->m:I

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 41
    const/16 v0, 0xde1

    const/16 v1, 0x2801

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 43
    const/16 v0, 0xde1

    const/16 v1, 0x2800

    const/high16 v2, 0x46180000    # 9728.0f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 45
    const/16 v0, 0xde1

    const/16 v1, 0x2802

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 47
    const/16 v0, 0xde1

    const/16 v1, 0x2803

    const v2, 0x812f

    invoke-static {v0, v1, v2}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 49
    const/16 v0, 0xde1

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/pa;->k:Landroid/graphics/Bitmap;

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->k:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 51
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/pa;->k:Landroid/graphics/Bitmap;

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/car/pa;->a:Lcom/google/android/gms/car/on;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->b(I)Lcom/google/android/gms/car/ot;

    move-result-object v0

    .line 54
    iget v1, v0, Lcom/google/android/gms/car/ot;->a:I

    const-string v2, "sTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/car/pa;->n:I

    .line 55
    iget v1, v0, Lcom/google/android/gms/car/ot;->a:I

    const-string v2, "sAlphaTexture"

    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/car/pa;->o:I

    .line 56
    iget v0, v0, Lcom/google/android/gms/car/ot;->a:I

    const-string v1, "uAlpha"

    invoke-static {v0, v1}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/pa;->p:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 57
    monitor-exit p0

    return-void

    .line 35
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
