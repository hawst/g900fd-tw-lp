.class public final Lcom/google/android/gms/backup/m;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field private a:Z

.field private b:Ljava/lang/String;

.field private c:Z

.field private d:Ljava/lang/String;

.field private e:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 115
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 120
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/m;->b:Ljava/lang/String;

    .line 137
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/m;->d:Ljava/lang/String;

    .line 175
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/m;->e:I

    .line 115
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 178
    iget v0, p0, Lcom/google/android/gms/backup/m;->e:I

    if-gez v0, :cond_0

    .line 180
    invoke-virtual {p0}, Lcom/google/android/gms/backup/m;->b()I

    .line 182
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/m;->e:I

    return v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/backup/m;
    .locals 1

    .prologue
    .line 124
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/m;->a:Z

    .line 125
    iput-object p1, p0, Lcom/google/android/gms/backup/m;->b:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 112
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/m;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/m;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/m;->b(Ljava/lang/String;)Lcom/google/android/gms/backup/m;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x42 -> :sswitch_1
        0x4a -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 2

    .prologue
    .line 167
    iget-boolean v0, p0, Lcom/google/android/gms/backup/m;->a:Z

    if-eqz v0, :cond_0

    .line 168
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/backup/m;->b:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 170
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/backup/m;->c:Z

    if-eqz v0, :cond_1

    .line 171
    const/16 v0, 0x9

    iget-object v1, p0, Lcom/google/android/gms/backup/m;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 173
    :cond_1
    return-void
.end method

.method public final b()I
    .locals 3

    .prologue
    .line 187
    const/4 v0, 0x0

    .line 188
    iget-boolean v1, p0, Lcom/google/android/gms/backup/m;->a:Z

    if-eqz v1, :cond_0

    .line 189
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/backup/m;->b:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 192
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/backup/m;->c:Z

    if-eqz v1, :cond_1

    .line 193
    const/16 v1, 0x9

    iget-object v2, p0, Lcom/google/android/gms/backup/m;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 196
    :cond_1
    iput v0, p0, Lcom/google/android/gms/backup/m;->e:I

    .line 197
    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/backup/m;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/m;->c:Z

    .line 142
    iput-object p1, p0, Lcom/google/android/gms/backup/m;->d:Ljava/lang/String;

    .line 143
    return-object p0
.end method
