.class public Lcom/google/android/gms/search/corpora/f;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 25
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method private c()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 6

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/f;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v2

    .line 40
    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3

    .line 41
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/f;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v0, Lcom/google/android/gms/icing/impl/a/aa;

    const/4 v1, 0x1

    new-array v4, v1, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/f;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;

    iget-object v1, v1, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Request;->b:Ljava/lang/String;

    aput-object v1, v4, v5

    const/4 v1, 0x0

    invoke-interface {v2, v0, v4, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    .line 43
    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 44
    const/4 v0, 0x0

    monitor-exit v3

    .line 48
    :goto_0
    return-object v0

    .line 47
    :cond_0
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v0}, Lcom/google/android/gms/icing/impl/a/x;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 48
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->h(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 49
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;-><init>()V

    invoke-direct {p0}, Lcom/google/android/gms/search/corpora/f;->c()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->b:Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusInfoCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method
