.class public Lcom/google/android/gms/car/y;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;
.implements Lcom/google/android/gms/car/om;


# static fields
.field private static final b:Landroid/util/SparseArray;


# instance fields
.field volatile a:Lcom/google/android/gms/car/oi;

.field private final c:Lcom/google/android/gms/car/ae;

.field private final d:Landroid/content/ComponentName;

.field private final e:Lcom/google/android/gms/car/aa;

.field private final f:Lcom/google/android/gms/car/ab;

.field private final g:Ljava/lang/Object;

.field private final h:Ljava/lang/Runnable;

.field private final i:Ljava/lang/String;

.field private j:I

.field private k:Z

.field private l:Landroid/content/Intent;

.field private m:Landroid/content/Intent;

.field private volatile n:Lcom/google/android/gms/car/mo;

.field private o:I

.field private p:Z

.field private q:Landroid/os/Bundle;

.field private volatile r:Z

.field private s:I

.field private t:Z

.field private final u:Ljava/util/Queue;

.field private v:Z

.field private w:Z

.field private x:Landroid/os/HandlerThread;

.field private y:Landroid/os/Handler;

.field private z:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/16 v3, 0xa

    .line 60
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0, v3}, Landroid/util/SparseArray;-><init>(I)V

    .line 62
    sput-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/4 v1, 0x0

    const-string v2, "SETUP"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 63
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/4 v1, 0x2

    const-string v2, "START"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 64
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/4 v1, 0x6

    const-string v2, "RESUME"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 65
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0x8

    const-string v2, "VIDEO_CONFIG_CHANGE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 67
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const-string v1, "PAUSE"

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 68
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0x13

    const-string v2, "INPUT_FOCUS_CHANGE"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 69
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0xc

    const-string v2, "ON_NEW_INTENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 70
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0x3e9

    const-string v2, "ON_TOUCH_EVENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 71
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0x3ea

    const-string v2, "ON_KEY_EVENT"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 72
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    const/16 v1, 0x3e8

    const-string v2, "ACQUIRE_COMMAND_LOCK"

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 73
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 177
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 91
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->g:Ljava/lang/Object;

    .line 95
    new-instance v0, Lcom/google/android/gms/car/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/z;-><init>(Lcom/google/android/gms/car/y;)V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->h:Ljava/lang/Runnable;

    .line 112
    iput v1, p0, Lcom/google/android/gms/car/y;->j:I

    .line 113
    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->k:Z

    .line 139
    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 147
    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->r:Z

    .line 151
    iput v1, p0, Lcom/google/android/gms/car/y;->s:I

    .line 158
    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->t:Z

    .line 160
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->u:Ljava/util/Queue;

    .line 178
    iput-object p1, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    .line 179
    iput-object p2, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    .line 180
    new-instance v0, Lcom/google/android/gms/car/aa;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/aa;-><init>(Lcom/google/android/gms/car/y;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->e:Lcom/google/android/gms/car/aa;

    .line 181
    new-instance v0, Lcom/google/android/gms/car/id;

    iget-object v1, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v1}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/id;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->e()Z

    move-result v0

    .line 183
    new-instance v1, Lcom/google/android/gms/car/ab;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, p0, v2, v0, p2}, Lcom/google/android/gms/car/ab;-><init>(Lcom/google/android/gms/car/y;Landroid/os/Looper;ZLandroid/content/ComponentName;)V

    iput-object v1, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    .line 186
    return-void
.end method

.method static E()V
    .locals 0

    .prologue
    .line 763
    return-void
.end method

.method static I()V
    .locals 0

    .prologue
    .line 817
    return-void
.end method

.method static J()V
    .locals 0

    .prologue
    .line 829
    return-void
.end method

.method private declared-synchronized U()Z
    .locals 1

    .prologue
    .line 354
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    packed-switch v0, :pswitch_data_0

    .line 362
    :pswitch_0
    const/4 v0, 0x0

    :goto_0
    monitor-exit p0

    return v0

    .line 357
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 354
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private V()V
    .locals 3

    .prologue
    .line 603
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 604
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 605
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 613
    :cond_0
    :goto_0
    return-void

    .line 611
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 612
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->o()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;I)I
    .locals 0

    .prologue
    .line 53
    iput p1, p0, Lcom/google/android/gms/car/y;->o:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;Landroid/content/Intent;)Landroid/content/Intent;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/google/android/gms/car/y;->q:Landroid/os/Bundle;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ae;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;Ljava/lang/Throwable;)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/y;->b(Ljava/lang/Throwable;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/y;Z)V
    .locals 0

    .prologue
    .line 53
    iput-boolean p1, p0, Lcom/google/android/gms/car/y;->z:Z

    return-void
.end method

.method static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1800
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1801
    const-string v0, "Unknown"

    .line 1803
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/car/x;->a:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/car/y;)V
    .locals 3

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v0}, Lcom/google/android/gms/car/mo;->d()V

    goto :goto_0
.end method

.method private b(Ljava/lang/Throwable;)V
    .locals 2

    .prologue
    .line 942
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->z:Z

    if-eqz v0, :cond_0

    .line 943
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    invoke-virtual {v1}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 948
    :cond_0
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/y;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->y:Landroid/os/Handler;

    return-object v0
.end method

.method static c(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 1810
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 1811
    const-string v0, "Unknown"

    .line 1813
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/google/android/gms/car/y;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/car/y;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/y;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->u:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/oi;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    return-object v0
.end method

.method static y()V
    .locals 0

    .prologue
    .line 714
    return-void
.end method


# virtual methods
.method final A()V
    .locals 3

    .prologue
    .line 731
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-nez v0, :cond_1

    .line 732
    :cond_0
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Setup complete but state is crashed or unitialized."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-static {p0}, Lcom/google/android/gms/car/ae;->c(Lcom/google/android/gms/car/y;)V

    .line 738
    :goto_0
    return-void

    .line 736
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->b(Lcom/google/android/gms/car/y;)V

    goto :goto_0
.end method

.method final B()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 742
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v1, :cond_0

    .line 746
    :goto_0
    return v0

    .line 745
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/mo;->c(I)V

    .line 746
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final C()V
    .locals 1

    .prologue
    .line 750
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->e(Lcom/google/android/gms/car/y;)V

    .line 751
    return-void
.end method

.method final D()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 755
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v1, :cond_0

    .line 759
    :goto_0
    return v0

    .line 758
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/mo;->b(I)V

    .line 759
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final F()Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 767
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v1, :cond_0

    .line 771
    :goto_0
    return v0

    .line 770
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/mo;->a(I)V

    .line 771
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final G()V
    .locals 1

    .prologue
    .line 775
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->d(Lcom/google/android/gms/car/y;)V

    .line 777
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/y;->s:I

    .line 778
    return-void
.end method

.method final H()V
    .locals 1

    .prologue
    .line 801
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 802
    return-void
.end method

.method final K()Z
    .locals 2

    .prologue
    .line 833
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final L()V
    .locals 1

    .prologue
    .line 839
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    .line 840
    if-eqz v0, :cond_0

    .line 841
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->s()V

    .line 843
    :cond_0
    return-void
.end method

.method final M()V
    .locals 1

    .prologue
    .line 846
    const/4 v0, 0x6

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    .line 847
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->f(Lcom/google/android/gms/car/y;)V

    .line 848
    return-void
.end method

.method final N()V
    .locals 2

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->i()V

    .line 852
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    iget-object v1, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ae;->b(Landroid/content/Intent;)V

    .line 853
    return-void
.end method

.method final O()V
    .locals 1

    .prologue
    .line 856
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->w:Z

    .line 857
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->h()V

    .line 858
    return-void
.end method

.method final P()Z
    .locals 2

    .prologue
    .line 861
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->w:Z

    .line 862
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->w:Z

    .line 863
    return v0
.end method

.method final Q()V
    .locals 1

    .prologue
    .line 871
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->g(Lcom/google/android/gms/car/y;)V

    .line 872
    return-void
.end method

.method final R()Z
    .locals 1

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_0

    .line 876
    const/4 v0, 0x0

    .line 879
    :goto_0
    return v0

    .line 878
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v0}, Lcom/google/android/gms/car/mo;->b()V

    .line 879
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final S()V
    .locals 3

    .prologue
    .line 890
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_0

    .line 898
    :goto_0
    return-void

    .line 894
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v0}, Lcom/google/android/gms/car/mo;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 896
    :catch_0
    move-exception v0

    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; remote exception sending kill"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final T()Z
    .locals 1

    .prologue
    .line 1793
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->v:Z

    return v0
.end method

.method public a()V
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->x()V

    .line 262
    return-void
.end method

.method public declared-synchronized a(I)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x2

    const/4 v4, 0x5

    const/4 v3, 0x3

    .line 434
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 435
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Move to state called with state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/gms/car/y;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current state "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/y;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/y;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_2

    .line 440
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 441
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; mService is null, ignoring move to state request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 484
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 446
    :cond_2
    packed-switch p1, :pswitch_data_0

    .line 486
    :try_start_1
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No state that matches "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 434
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 448
    :pswitch_0
    :try_start_2
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v3, :cond_1

    .line 451
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->s()V

    .line 452
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    goto :goto_0

    .line 455
    :pswitch_1
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v4, :cond_1

    .line 458
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v3, :cond_3

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v6, :cond_3

    .line 459
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->s()V

    .line 461
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->v()V

    .line 462
    const/4 v0, 0x5

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    goto :goto_0

    .line 465
    :pswitch_2
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v6, :cond_1

    .line 468
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-ne v0, v4, :cond_1

    .line 471
    invoke-direct {p0}, Lcom/google/android/gms/car/y;->V()V

    .line 472
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    goto :goto_0

    .line 476
    :pswitch_3
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eq v0, v5, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eqz v0, :cond_1

    .line 479
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-ne v0, v4, :cond_4

    .line 480
    invoke-direct {p0}, Lcom/google/android/gms/car/y;->V()V

    .line 482
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->u()V

    .line 483
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 446
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 568
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 577
    :cond_0
    :goto_0
    return-void

    .line 574
    :cond_1
    if-eqz p1, :cond_0

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ab;->a(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    .line 339
    invoke-direct {p0}, Lcom/google/android/gms/car/y;->U()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 340
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->v:Z

    .line 350
    :cond_0
    :goto_0
    iput-object p2, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    .line 351
    return-void

    .line 342
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->v:Z

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v0

    .line 345
    const/high16 v1, 0x100000

    and-int/2addr v1, v0

    if-eqz v1, :cond_0

    .line 346
    const v1, -0x100001

    and-int/2addr v0, v1

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method

.method a(Landroid/content/res/Configuration;I)V
    .locals 3

    .prologue
    .line 628
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 629
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 630
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 638
    :cond_0
    :goto_0
    return-void

    .line 636
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 637
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/ab;->a(Landroid/content/res/Configuration;I)V

    goto :goto_0
.end method

.method public a(Landroid/view/KeyEvent;)V
    .locals 4

    .prologue
    .line 678
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 679
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 680
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_0
    :goto_0
    return-void

    .line 685
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    if-eqz v0, :cond_0

    .line 688
    iget-object v0, p0, Lcom/google/android/gms/car/y;->u:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 689
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v0, :cond_0

    .line 690
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ab;->a(Z)V

    .line 692
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/car/mo;->a(ILandroid/view/KeyEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 693
    :catch_0
    move-exception v0

    .line 694
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; Remote exception calling onKeyEvent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 696
    iget-object v1, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v1}, Lcom/google/android/gms/car/ab;->e()V

    .line 697
    iget-object v1, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/ab;->a(Lcom/google/android/gms/car/ab;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public a(Landroid/view/MotionEvent;)V
    .locals 4

    .prologue
    .line 653
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 654
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 655
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 674
    :cond_0
    :goto_0
    return-void

    .line 660
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    if-eqz v0, :cond_0

    .line 663
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v0, :cond_0

    .line 665
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ab;->a(Z)V

    .line 666
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    const/4 v1, 0x0

    invoke-interface {v0, v1, p1}, Lcom/google/android/gms/car/mo;->a(ILandroid/view/MotionEvent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 667
    :catch_0
    move-exception v0

    .line 668
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; Remote exception calling onTouchEvent"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 670
    iget-object v1, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v1}, Lcom/google/android/gms/car/ab;->e()V

    .line 671
    iget-object v1, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/ab;->a(Lcom/google/android/gms/car/ab;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method final a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;)V
    .locals 1

    .prologue
    .line 867
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p1, p2, p0}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;Lcom/google/android/gms/car/y;)V

    .line 868
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/oi;)V
    .locals 0

    .prologue
    .line 377
    iput-object p1, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    .line 378
    return-void
.end method

.method public a(Lcom/google/android/gms/car/ol;II)V
    .locals 3

    .prologue
    .line 239
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 240
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onWindowSurfaceTextureAvailable called for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->t:Z

    if-eqz v0, :cond_1

    .line 244
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->t:Z

    .line 245
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->s()V

    .line 247
    :cond_1
    return-void
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 1829
    const-string v0, "CarActivityManager "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1830
    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1831
    const-string v0, " State="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    invoke-static {v0}, Lcom/google/android/gms/car/y;->b(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1832
    iget-object v0, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    .line 1833
    if-eqz v0, :cond_0

    .line 1834
    const-string v1, " mStartIntent="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1836
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    .line 1837
    const-string v1, " mCurrentIntent="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1838
    if-eqz v0, :cond_2

    .line 1839
    invoke-virtual {v0}, Landroid/content/Intent;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1843
    :goto_0
    const-string v0, " mPid="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/y;->o:I

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(I)V

    .line 1844
    const-string v0, " mHasFocus="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Z)V

    .line 1845
    const-string v0, " mCrashCount="

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    iget v0, p0, Lcom/google/android/gms/car/y;->s:I

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(I)V

    .line 1846
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    .line 1847
    if-eqz v0, :cond_1

    .line 1848
    const-string v1, " mWindow="

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1849
    invoke-static {v0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1851
    :cond_1
    return-void

    .line 1841
    :cond_2
    const-string v0, "null"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0
.end method

.method final a(Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    const/4 v1, 0x3

    .line 901
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-eqz v0, :cond_1

    .line 902
    const-string v0, "CAR.CAM"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 903
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; We\'ve already handled a crash, returning"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 923
    :cond_0
    :goto_0
    return-void

    .line 908
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/y;->b(Ljava/lang/Throwable;)V

    .line 909
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    .line 910
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 911
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->o()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 912
    const-string v0, "CAR.CAM"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 913
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; We\'ve already received a stop, notify listeners"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 916
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->f(Lcom/google/android/gms/car/y;)V

    goto :goto_0

    .line 919
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    .line 920
    iget v0, p0, Lcom/google/android/gms/car/y;->s:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/y;->s:I

    .line 921
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Projection client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " has crashed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, p0, p1, v1}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/y;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 617
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 618
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 619
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ab;->b(Z)V

    goto :goto_0
.end method

.method final a(ILandroid/content/res/Configuration;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 782
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-nez v1, :cond_1

    .line 797
    :cond_0
    :goto_0
    return v0

    .line 785
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->n()Lcom/google/android/gms/car/DrawingSpec;

    move-result-object v1

    .line 786
    if-nez v1, :cond_2

    .line 789
    iget v2, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    .line 790
    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    .line 795
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v2, v0, p1, v1, p2}, Lcom/google/android/gms/car/mo;->a(IILcom/google/android/gms/car/DrawingSpec;Landroid/content/res/Configuration;)V

    .line 797
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 266
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    .line 267
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/y;->a(I)V

    .line 269
    :cond_0
    return-void
.end method

.method final b(Landroid/view/KeyEvent;)V
    .locals 1

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-nez v0, :cond_0

    .line 887
    :goto_0
    return-void

    .line 886
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->a(Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method final b(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 821
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_0

    .line 822
    const/4 v0, 0x0

    .line 825
    :goto_0
    return v0

    .line 824
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/mo;->a(Landroid/content/Intent;)V

    .line 825
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final b(Z)Z
    .locals 2

    .prologue
    .line 806
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_1

    .line 810
    :cond_0
    const/4 v0, 0x0

    .line 813
    :goto_0
    return v0

    .line 812
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/mo;->a(Z)V

    .line 813
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 277
    iget-object v0, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->flattenToString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final d()Z
    .locals 4

    .prologue
    .line 285
    iget-object v1, p0, Lcom/google/android/gms/car/y;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 286
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    if-eqz v0, :cond_1

    .line 287
    const-string v0, "CAR.CAM"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 288
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; already bound "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    .line 299
    :goto_0
    return v0

    .line 292
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    const/4 v3, 0x1

    invoke-virtual {v0, v2, p0, v3}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    .line 294
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    if-nez v0, :cond_2

    .line 295
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; bindService failed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 299
    :goto_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 297
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    iget-object v2, p0, Lcom/google/android/gms/car/y;->h:Ljava/lang/Runnable;

    invoke-static {v0, v2}, Lcom/google/android/gms/car/qh;->a(Landroid/os/Handler;Ljava/lang/Runnable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 307
    iget-object v1, p0, Lcom/google/android/gms/car/y;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 308
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    if-nez v0, :cond_2

    .line 309
    const-string v0, "CAR.CAM"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 310
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; not bound "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 312
    :cond_0
    monitor-exit v1

    .line 322
    :cond_1
    :goto_0
    return-void

    .line 314
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    .line 315
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 316
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    iput-object v2, p0, Lcom/google/android/gms/car/y;->y:Landroid/os/Handler;

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    if-eqz v0, :cond_1

    .line 319
    iget-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 320
    iput-object v2, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    goto :goto_0

    .line 316
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 329
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Lcom/google/android/gms/car/oi;
    .locals 1

    .prologue
    .line 370
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    return-object v0
.end method

.method public final h()I
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/google/android/gms/car/y;->s:I

    return v0
.end method

.method public final i()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 388
    iget-object v0, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    return-object v0
.end method

.method public final j()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    return-object v0
.end method

.method public final k()Landroid/content/ComponentName;
    .locals 1

    .prologue
    .line 396
    iget-object v0, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    return-object v0
.end method

.method public final l()I
    .locals 1

    .prologue
    .line 400
    iget v0, p0, Lcom/google/android/gms/car/y;->o:I

    return v0
.end method

.method public final m()I
    .locals 1

    .prologue
    .line 404
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    return v0
.end method

.method public final n()Z
    .locals 1

    .prologue
    .line 408
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    return v0
.end method

.method public final o()Z
    .locals 2

    .prologue
    .line 412
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 190
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 191
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onServiceConnected called for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    iget-object v1, p0, Lcom/google/android/gms/car/y;->h:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/qh;->b(Landroid/os/Handler;Ljava/lang/Runnable;)V

    .line 195
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    .line 196
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->o()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 197
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 198
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Client is stopping"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->f(Lcom/google/android/gms/car/y;)V

    .line 214
    :goto_0
    return-void

    .line 203
    :cond_2
    invoke-static {p2}, Lcom/google/android/gms/car/mp;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/mo;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    if-eqz v0, :cond_3

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 207
    :cond_3
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "PostMessageThread"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    .line 209
    iget-object v0, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 210
    new-instance v0, Lcom/google/android/gms/car/pf;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/y;->x:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/y;->y:Landroid/os/Handler;

    .line 212
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->f()V

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/y;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 218
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ";onServiceDisconnected called for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 223
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_2

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x6

    if-ne v0, v1, :cond_2

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->f(Lcom/google/android/gms/car/y;)V

    .line 235
    :cond_1
    :goto_0
    return-void

    .line 226
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_1

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->a()V

    .line 231
    iput-object v3, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    .line 232
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->r:Z

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ab;->a(Lcom/google/android/gms/car/ab;Ljava/lang/Throwable;)V

    .line 234
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->q()V

    goto :goto_0
.end method

.method public final declared-synchronized p()Z
    .locals 2

    .prologue
    .line 423
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/y;->j:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized q()V
    .locals 3

    .prologue
    .line 494
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 495
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Reset called for "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 498
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    if-nez v0, :cond_1

    .line 500
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/y;->s:I

    .line 502
    :cond_1
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 503
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/y;->j:I

    .line 504
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    .line 505
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/y;->o:I

    .line 506
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->b()V

    .line 507
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->t:Z

    .line 508
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->v:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 509
    monitor-exit p0

    return-void

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final r()V
    .locals 3

    .prologue
    .line 512
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 513
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; teardown client"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 517
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->b()V

    .line 518
    invoke-virtual {p0}, Lcom/google/android/gms/car/y;->e()V

    .line 519
    return-void
.end method

.method s()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    .line 526
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 527
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 528
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    :cond_0
    :goto_0
    return-void

    .line 533
    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/y;->j:I

    const/4 v2, 0x6

    if-ne v0, v2, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 534
    const-string v0, "CAR.CAM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Received onProjectionStart request, already finishing"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 533
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 540
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_0

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->m()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 544
    const-string v0, "CAR.CAM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 545
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "component: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "; Received onProjectionStart request, waiting for window surface tex"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 548
    :cond_4
    iput-boolean v1, p0, Lcom/google/android/gms/car/y;->t:Z

    goto :goto_0

    .line 551
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->h()V

    goto :goto_0
.end method

.method public final t()V
    .locals 1

    .prologue
    .line 559
    iget-object v0, p0, Lcom/google/android/gms/car/y;->l:Landroid/content/Intent;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/y;->a(Landroid/content/Intent;)V

    .line 560
    return-void
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 1818
    new-instance v0, Ljava/lang/StringBuffer;

    invoke-direct {v0}, Ljava/lang/StringBuffer;-><init>()V

    .line 1819
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CarActivityManager: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {p0}, Ljava/lang/System;->identityHashCode(Ljava/lang/Object;)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1820
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " Component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->d:Landroid/content/ComponentName;

    invoke-virtual {v2}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1821
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, " State: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/y;->j:I

    invoke-static {v2}, Lcom/google/android/gms/car/y;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 1822
    invoke-virtual {v0}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method u()V
    .locals 3

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 581
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 582
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 588
    :cond_0
    :goto_0
    return-void

    .line 587
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->j()V

    goto :goto_0
.end method

.method v()V
    .locals 3

    .prologue
    .line 591
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 592
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 593
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_0
    :goto_0
    return-void

    .line 598
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->p:Z

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->l()V

    goto :goto_0
.end method

.method public final w()V
    .locals 3

    .prologue
    .line 641
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v0, :cond_1

    .line 642
    const-string v0, "CAR.CAM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 643
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/y;->i:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; Service is null, ignoring request"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 649
    :cond_0
    :goto_0
    return-void

    .line 648
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/y;->f:Lcom/google/android/gms/car/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->z()V

    goto :goto_0
.end method

.method final x()Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 704
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-nez v1, :cond_0

    .line 710
    :goto_0
    return v0

    .line 707
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/y;->k:Z

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    iget-object v1, p0, Lcom/google/android/gms/car/y;->e:Lcom/google/android/gms/car/aa;

    iget-object v2, p0, Lcom/google/android/gms/car/y;->c:Lcom/google/android/gms/car/ae;

    invoke-virtual {v2}, Lcom/google/android/gms/car/ae;->g()Lcom/google/android/gms/car/gx;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/car/mo;->a(Lcom/google/android/gms/car/mr;Lcom/google/android/gms/car/jx;)V

    .line 710
    const/4 v0, 0x1

    goto :goto_0
.end method

.method final z()Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 718
    iget-object v1, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    if-nez v1, :cond_1

    .line 727
    :cond_0
    :goto_0
    return v0

    .line 721
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->n()Lcom/google/android/gms/car/DrawingSpec;

    move-result-object v1

    .line 722
    if-eqz v1, :cond_0

    .line 725
    iget-object v2, p0, Lcom/google/android/gms/car/y;->n:Lcom/google/android/gms/car/mo;

    iget-object v3, p0, Lcom/google/android/gms/car/y;->m:Landroid/content/Intent;

    iget-object v4, p0, Lcom/google/android/gms/car/y;->q:Landroid/os/Bundle;

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/gms/car/mo;->a(ILcom/google/android/gms/car/DrawingSpec;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 727
    const/4 v0, 0x1

    goto :goto_0
.end method
