.class final Lcom/google/android/gms/car/a/e;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/a;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/a/a;)V
    .locals 0

    .prologue
    .line 1002
    iput-object p1, p0, Lcom/google/android/gms/car/a/e;->a:Lcom/google/android/gms/car/a/a;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/a/a;B)V
    .locals 0

    .prologue
    .line 1002
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/e;-><init>(Lcom/google/android/gms/car/a/a;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 1005
    iget-object v0, p0, Lcom/google/android/gms/car/a/e;->a:Lcom/google/android/gms/car/a/a;

    invoke-static {v0}, Lcom/google/android/gms/car/a/a;->a(Lcom/google/android/gms/car/a/a;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1006
    const-string v0, "CAR.BT"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1007
    const-string v0, "CAR.BT"

    const-string v1, "Timeout check after cleanup"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1015
    :cond_0
    :goto_0
    return-void

    .line 1011
    :cond_1
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1012
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Timeout reached. current state="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/a/e;->a:Lcom/google/android/gms/car/a/a;

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(Lcom/google/android/gms/car/a/a;)I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/car/a/a;->b(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1014
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a/e;->a:Lcom/google/android/gms/car/a/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto :goto_0
.end method
