.class public final Lcom/google/android/gms/feedback/b/a;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private a:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Lcom/google/android/gms/common/internal/ClientSettings;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct/range {p0 .. p5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Lcom/google/android/gms/common/internal/ClientSettings;)V

    .line 56
    iput-object p1, p0, Lcom/google/android/gms/feedback/b/a;->a:Landroid/content/Context;

    .line 57
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 43
    invoke-static {p1}, Lcom/google/android/gms/feedback/b/c;->a(Landroid/os/IBinder;)Lcom/google/android/gms/feedback/b/b;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 77
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 78
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->o(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 81
    return-void
.end method

.method public final a(Lcom/google/android/gms/feedback/FeedbackOptions;)V
    .locals 5

    .prologue
    .line 89
    invoke-virtual {p0}, Lcom/google/android/gms/feedback/b/a;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/feedback/b/b;

    iget-object v1, p0, Lcom/google/android/gms/feedback/b/a;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/feedback/ErrorReport;

    invoke-direct {v3}, Lcom/google/android/gms/feedback/ErrorReport;-><init>()V

    if-eqz p1, :cond_a

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->b()Landroid/os/Bundle;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->b()Landroid/os/Bundle;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Bundle;->size()I

    move-result v1

    if-lez v1, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->b()Landroid/os/Bundle;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->E:Landroid/os/Bundle;

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->C:Ljava/lang/String;

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->d()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->c:Ljava/lang/String;

    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    if-eqz v1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwMethodName:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->N:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwLineNumber:I

    iput v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->L:I

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwClassName:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->M:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->stackTrace:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->O:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionClassName:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->J:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->exceptionMessage:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->P:Ljava/lang/String;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->e()Landroid/app/ApplicationErrorReport$CrashInfo;

    move-result-object v1

    iget-object v1, v1, Landroid/app/ApplicationErrorReport$CrashInfo;->throwFileName:Ljava/lang/String;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->K:Ljava/lang/String;

    :cond_3
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->c()Lcom/google/android/gms/feedback/ThemeSettings;

    move-result-object v1

    if-eqz v1, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->c()Lcom/google/android/gms/feedback/ThemeSettings;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->Z:Lcom/google/android/gms/feedback/ThemeSettings;

    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->f()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->f()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->Q:Ljava/lang/String;

    :cond_5
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->h()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    iget-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->b:Landroid/app/ApplicationErrorReport;

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->h()Ljava/lang/String;

    move-result-object v4

    iput-object v4, v1, Landroid/app/ApplicationErrorReport;->packageName:Ljava/lang/String;

    :cond_6
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->g()Lcom/google/android/gms/common/data/BitmapTeleporter;

    move-result-object v1

    if-eqz v1, :cond_7

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->g()Lcom/google/android/gms/common/data/BitmapTeleporter;

    move-result-object v1

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iget-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->T:Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/data/BitmapTeleporter;->a(Ljava/io/File;)V

    :cond_7
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->i()Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->i()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_9

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->i()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_8

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/feedback/FileTeleporter;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/feedback/FileTeleporter;->a(Ljava/io/File;)V

    goto :goto_0

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->i()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->i()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/feedback/FileTeleporter;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [Lcom/google/android/gms/feedback/FileTeleporter;

    iput-object v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->V:[Lcom/google/android/gms/feedback/FileTeleporter;

    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/feedback/FeedbackOptions;->j()Z

    move-result v1

    iput-boolean v1, v3, Lcom/google/android/gms/feedback/ErrorReport;->X:Z

    :cond_a
    invoke-interface {v0, v3}, Lcom/google/android/gms/feedback/b/b;->b(Lcom/google/android/gms/feedback/ErrorReport;)Z

    .line 90
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 61
    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    const-string v0, "com.google.android.gms.feedback.internal.IFeedbackService"

    return-object v0
.end method
