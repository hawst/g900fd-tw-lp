.class public final Lcom/google/android/gms/common/data/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/ac;
.implements Lcom/google/android/gms/common/data/d;
.implements Lcom/google/android/gms/common/data/r;


# instance fields
.field private final d:Ljava/util/ArrayList;

.field private final e:Ljava/util/ArrayList;

.field private f:I

.field private g:Landroid/os/Bundle;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/data/d;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    .line 22
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    .line 33
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/data/c;->a(Lcom/google/android/gms/common/data/d;)V

    .line 34
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/data/c;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/c;
    .locals 3

    .prologue
    .line 154
    new-instance v1, Lcom/google/android/gms/common/data/c;

    invoke-direct {v1}, Lcom/google/android/gms/common/data/c;-><init>()V

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 156
    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/data/c;->a(Lcom/google/android/gms/common/data/d;)V

    goto :goto_0

    .line 158
    :cond_0
    invoke-virtual {v1, p1}, Lcom/google/android/gms/common/data/c;->a(Lcom/google/android/gms/common/data/d;)V

    .line 159
    return-object v1
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 4

    .prologue
    .line 45
    monitor-enter p0

    .line 46
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 48
    if-ge p1, v3, :cond_0

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 50
    if-eqz v0, :cond_0

    .line 51
    sub-int v1, p1, v3

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    add-int/2addr v1, v2

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 55
    :goto_1
    return-object v0

    .line 46
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 55
    :cond_1
    const/4 v0, 0x0

    monitor-exit p0

    goto :goto_1

    .line 56
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a()V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 139
    .line 140
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    :goto_0
    if-ge v2, v3, :cond_0

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 143
    if-eqz v0, :cond_1

    .line 144
    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    add-int/2addr v0, v1

    .line 146
    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 141
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    .line 148
    :cond_0
    iput v1, p0, Lcom/google/android/gms/common/data/c;->f:I

    .line 149
    return-void

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 4

    .prologue
    .line 75
    if-nez p1, :cond_0

    .line 105
    :goto_0
    return-void

    .line 78
    :cond_0
    monitor-enter p0

    .line 80
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    .line 84
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    .line 85
    if-eqz v0, :cond_1

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    const-string v2, "prev_page_token"

    const-string v3, "prev_page_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 92
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/c;->a()V

    .line 98
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    .line 99
    if-eqz v0, :cond_2

    .line 100
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    const-string v2, "next_page_token"

    const-string v3, "next_page_token"

    invoke-virtual {v0, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    :goto_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 103
    :cond_2
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    const-string v1, "next_page_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 168
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 169
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 170
    instance-of v3, v0, Lcom/google/android/gms/common/data/r;

    if-eqz v3, :cond_0

    .line 171
    check-cast v0, Lcom/google/android/gms/common/data/r;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/data/r;->a(Ljava/lang/String;)V

    .line 168
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 174
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/c;->a()V

    .line 175
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 38
    monitor-enter p0

    .line 39
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/data/c;->f:I

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 40
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 0
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/c;->w_()V

    .line 119
    return-void
.end method

.method public final e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 110
    monitor-enter p0

    .line 111
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 112
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 129
    new-instance v0, Lcom/google/android/gms/common/data/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/data/e;-><init>(Lcom/google/android/gms/common/data/d;)V

    return-object v0
.end method

.method public final w_()V
    .locals 3

    .prologue
    .line 61
    monitor-enter p0

    .line 62
    const/4 v0, 0x0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/d;

    .line 64
    if-eqz v0, :cond_0

    .line 65
    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 62
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 68
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/common/data/c;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/data/c;->g:Landroid/os/Bundle;

    .line 71
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
