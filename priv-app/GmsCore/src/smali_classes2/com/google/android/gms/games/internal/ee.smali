.class public abstract Lcom/google/android/gms/games/internal/ee;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/internal/ed;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/games/internal/ee;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method

.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/games/internal/ed;
    .locals 2

    .prologue
    .line 28
    if-nez p0, :cond_0

    .line 29
    const/4 v0, 0x0

    .line 35
    :goto_0
    return-object v0

    .line 31
    :cond_0
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 32
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/games/internal/ed;

    if-eqz v1, :cond_1

    .line 33
    check-cast v0, Lcom/google/android/gms/games/internal/ed;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/internal/ef;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/internal/ef;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 178
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v2

    :goto_0
    return v2

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :sswitch_1
    const-string v1, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v1}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v3

    if-nez v3, :cond_0

    .line 57
    :goto_1
    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/internal/ee;->a(Landroid/os/IBinder;Lcom/google/android/gms/games/internal/eg;)V

    goto :goto_0

    .line 56
    :cond_0
    const-string v0, "com.google.android.gms.games.internal.IRoomServiceCallbacks"

    invoke-interface {v3, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_1

    instance-of v4, v0, Lcom/google/android/gms/games/internal/eg;

    if-eqz v4, :cond_1

    check-cast v0, Lcom/google/android/gms/games/internal/eg;

    goto :goto_1

    :cond_1
    new-instance v0, Lcom/google/android/gms/games/internal/ei;

    invoke-direct {v0, v3}, Lcom/google/android/gms/games/internal/ei;-><init>(Landroid/os/IBinder;)V

    goto :goto_1

    .line 62
    :sswitch_2
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/ee;->a()V

    goto :goto_0

    .line 68
    :sswitch_3
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/ee;->b()V

    goto :goto_0

    .line 74
    :sswitch_4
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 78
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 80
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 81
    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/gms/games/internal/ee;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 86
    :sswitch_5
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/ee;->c()V

    goto :goto_0

    .line 92
    :sswitch_6
    const-string v3, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v3}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 94
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_2

    .line 95
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 101
    :cond_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    if-eqz v3, :cond_3

    move v1, v2

    .line 102
    :cond_3
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/internal/ee;->a(Lcom/google/android/gms/common/data/DataHolder;Z)V

    goto :goto_0

    .line 107
    :sswitch_7
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 108
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/ee;->d()V

    goto/16 :goto_0

    .line 113
    :sswitch_8
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 115
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    move v1, v2

    .line 116
    :cond_4
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/internal/ee;->a(Z)V

    goto/16 :goto_0

    .line 121
    :sswitch_9
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 123
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 125
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v1

    .line 127
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v3

    .line 128
    invoke-virtual {p0, v0, v1, v3}, Lcom/google/android/gms/games/internal/ee;->a([BLjava/lang/String;I)V

    goto/16 :goto_0

    .line 133
    :sswitch_a
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 135
    invoke-virtual {p2}, Landroid/os/Parcel;->createByteArray()[B

    move-result-object v0

    .line 137
    invoke-virtual {p2}, Landroid/os/Parcel;->createStringArray()[Ljava/lang/String;

    move-result-object v1

    .line 138
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/internal/ee;->a([B[Ljava/lang/String;)V

    goto/16 :goto_0

    .line 143
    :sswitch_b
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 145
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 147
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 148
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/internal/ee;->a(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 153
    :sswitch_c
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 155
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 157
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 158
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/internal/ee;->b(Ljava/lang/String;I)V

    goto/16 :goto_0

    .line 163
    :sswitch_d
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 165
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 166
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/ee;->a(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 171
    :sswitch_e
    const-string v0, "com.google.android.gms.games.internal.IRoomService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 173
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v0

    .line 174
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/internal/ee;->b(Ljava/lang/String;)V

    goto/16 :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x3e9 -> :sswitch_1
        0x3ea -> :sswitch_2
        0x3eb -> :sswitch_3
        0x3ec -> :sswitch_4
        0x3ed -> :sswitch_5
        0x3ee -> :sswitch_6
        0x3ef -> :sswitch_7
        0x3f0 -> :sswitch_8
        0x3f1 -> :sswitch_9
        0x3f2 -> :sswitch_a
        0x3f3 -> :sswitch_b
        0x3f4 -> :sswitch_c
        0x3f5 -> :sswitch_d
        0x3f6 -> :sswitch_e
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
