.class final Lcom/google/android/gms/common/widget/a/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/j;


# instance fields
.field public final a:Lcom/google/android/gms/common/widget/a/i;

.field public b:I

.field final synthetic c:Lcom/google/android/gms/common/widget/a/l;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/widget/a/l;Lcom/google/android/gms/common/widget/a/i;I)V
    .locals 0

    .prologue
    .line 291
    iput-object p1, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 292
    iput-object p2, p0, Lcom/google/android/gms/common/widget/a/m;->a:Lcom/google/android/gms/common/widget/a/i;

    .line 293
    iput p3, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    .line 295
    invoke-virtual {p2, p0}, Lcom/google/android/gms/common/widget/a/i;->a(Lcom/google/android/gms/common/widget/a/j;)V

    .line 296
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 299
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->a:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    .line 300
    if-nez v0, :cond_0

    .line 301
    const/4 v0, 0x0

    .line 303
    :goto_0
    return v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a_(II)V
    .locals 3

    .prologue
    .line 327
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v2, p2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/widget/a/l;->b(II)V

    .line 328
    if-ge p1, p2, :cond_0

    .line 329
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 330
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 335
    :goto_0
    return-void

    .line 332
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p1

    add-int/lit8 v1, v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    .line 333
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    goto :goto_0
.end method

.method public final b_(I)V
    .locals 3

    .prologue
    .line 309
    iget-object v1, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->a:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/widget/a/e;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/a/l;->a(Lcom/google/android/gms/common/widget/a/e;)V

    .line 310
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/l;->b()V

    .line 311
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->a:Lcom/google/android/gms/common/widget/a/i;

    invoke-virtual {v0}, Lcom/google/android/gms/common/widget/a/i;->g()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/widget/a/l;->c(II)V

    .line 318
    :goto_0
    return-void

    .line 315
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->d(I)V

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/widget/a/l;->c(I)V

    goto :goto_0
.end method

.method public final c_(I)V
    .locals 3

    .prologue
    .line 322
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/m;->c:Lcom/google/android/gms/common/widget/a/l;

    iget v1, p0, Lcom/google/android/gms/common/widget/a/m;->b:I

    add-int/2addr v1, p1

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/widget/a/l;->a(II)V

    .line 323
    return-void
.end method
