.class public interface abstract Lcom/google/android/gms/plus/internal/p;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IInterface;


# virtual methods
.method public abstract a(Lcom/google/android/gms/plus/internal/c;IIILjava/lang/String;)Lcom/google/android/gms/common/internal/bd;
.end method

.method public abstract a()Ljava/lang/String;
.end method

.method public abstract a(Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;ILjava/lang/String;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;Landroid/net/Uri;Landroid/os/Bundle;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;Lcom/google/android/gms/common/server/response/SafeParcelResponse;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;Ljava/lang/String;)V
.end method

.method public abstract a(Lcom/google/android/gms/plus/internal/c;Ljava/util/List;)V
.end method

.method public abstract a(Ljava/lang/String;)V
.end method

.method public abstract a(Ljava/lang/String;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
.end method

.method public abstract b()V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/c;)V
.end method

.method public abstract b(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
.end method

.method public abstract c()Ljava/lang/String;
.end method

.method public abstract c(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
.end method

.method public abstract d(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
.end method

.method public abstract d()Z
.end method

.method public abstract e()Ljava/lang/String;
.end method

.method public abstract e(Lcom/google/android/gms/plus/internal/c;Ljava/lang/String;)V
.end method
