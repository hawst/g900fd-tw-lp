.class public final Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/people/identity/internal/ac;


# instance fields
.field final a:Z

.field final b:Z

.field final c:Ljava/lang/String;

.field final d:Z

.field final e:Landroid/os/Bundle;

.field private final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/gms/people/identity/internal/ac;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/ac;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->CREATOR:Lcom/google/android/gms/people/identity/internal/ac;

    return-void
.end method

.method constructor <init>(IZZZLjava/lang/String;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->f:I

    .line 63
    iput-boolean p2, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->a:Z

    .line 64
    iput-boolean p3, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->b:Z

    .line 65
    iput-object p5, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->c:Ljava/lang/String;

    .line 66
    iput-boolean p4, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->d:Z

    .line 67
    if-nez p6, :cond_0

    new-instance p6, Landroid/os/Bundle;

    invoke-direct {p6}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iput-object p6, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->e:Landroid/os/Bundle;

    .line 68
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/people/identity/e;)V
    .locals 6

    .prologue
    .line 77
    iget-boolean v1, p1, Lcom/google/android/gms/people/identity/e;->b:Z

    iget-boolean v2, p1, Lcom/google/android/gms/people/identity/e;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/people/identity/e;->d:Z

    iget-object v0, p1, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v4, v0, Lcom/google/android/gms/people/identity/c;->c:Ljava/lang/String;

    iget-object v0, p1, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v5, v0, Lcom/google/android/gms/people/identity/c;->d:Landroid/os/Bundle;

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;-><init>(ZZZLjava/lang/String;Landroid/os/Bundle;)V

    .line 83
    return-void
.end method

.method private constructor <init>(ZZZLjava/lang/String;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 73
    const/4 v1, 0x1

    move-object v0, p0

    move v2, p1

    move v3, p2

    move v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;-><init>(IZZZLjava/lang/String;Landroid/os/Bundle;)V

    .line 74
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->f:I

    return v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->d:Z

    return v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 99
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->a:Z

    return v0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 104
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->b:Z

    return v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 119
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final f()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->e:Landroid/os/Bundle;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 129
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "useOfflineDatabase"

    iget-boolean v2, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->a:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "useWebData"

    iget-boolean v2, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "endpoint"

    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 124
    invoke-static {p0, p1}, Lcom/google/android/gms/people/identity/internal/ac;->a(Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;Landroid/os/Parcel;)V

    .line 125
    return-void
.end method
