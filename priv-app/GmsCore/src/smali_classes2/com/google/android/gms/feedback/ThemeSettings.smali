.class public Lcom/google/android/gms/feedback/ThemeSettings;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field b:I

.field c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/feedback/ay;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/ay;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/ThemeSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 53
    const/4 v0, 0x1

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/feedback/ThemeSettings;-><init>(III)V

    .line 54
    return-void
.end method

.method constructor <init>(III)V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    iput p1, p0, Lcom/google/android/gms/feedback/ThemeSettings;->a:I

    .line 47
    iput p2, p0, Lcom/google/android/gms/feedback/ThemeSettings;->b:I

    .line 48
    iput p3, p0, Lcom/google/android/gms/feedback/ThemeSettings;->c:I

    .line 49
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 84
    iget v0, p0, Lcom/google/android/gms/feedback/ThemeSettings;->b:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/feedback/ThemeSettings;
    .locals 0

    .prologue
    .line 63
    iput p1, p0, Lcom/google/android/gms/feedback/ThemeSettings;->b:I

    .line 64
    return-object p0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/google/android/gms/feedback/ThemeSettings;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 101
    invoke-static {p0, p1}, Lcom/google/android/gms/feedback/ay;->a(Lcom/google/android/gms/feedback/ThemeSettings;Landroid/os/Parcel;)V

    .line 102
    return-void
.end method
