.class public final Lcom/google/android/gms/games/service/a/e/e;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/e;->c:Lcom/google/android/gms/games/internal/dr;

    .line 22
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/e/e;->d:Ljava/lang/String;

    .line 23
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/e;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->j(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 34
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/e/e;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/e/e;->d:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
