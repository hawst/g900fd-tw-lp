.class public final Lcom/google/android/gms/car/senderprotocol/be;
.super Lcom/google/android/gms/car/senderprotocol/aq;
.source "SourceFile"


# instance fields
.field private final f:Lcom/google/android/gms/car/ip;

.field private g:[I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ip;Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/aq;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 33
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->a:I

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/be;->f:Lcom/google/android/gms/car/ip;

    .line 35
    return-void
.end method

.method private a(ILjava/lang/Integer;)V
    .locals 4

    .prologue
    .line 105
    new-instance v0, Lcom/google/android/c/b/db;

    invoke-direct {v0}, Lcom/google/android/c/b/db;-><init>()V

    .line 106
    invoke-virtual {v0, p1}, Lcom/google/android/c/b/db;->a(I)Lcom/google/android/c/b/db;

    .line 107
    if-eqz p2, :cond_0

    .line 108
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/db;->b(I)Lcom/google/android/c/b/db;

    .line 110
    :cond_0
    const-string v1, "CAR.VIDEO"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 111
    const-string v1, "CAR.VIDEO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sending video focus request mode="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " reason="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    :cond_1
    const v1, 0x8007

    invoke-static {v0}, Lcom/google/android/c/b/db;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/be;->a(I[B)V

    .line 115
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 1

    .prologue
    .line 70
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->d:Z

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->f:Lcom/google/android/gms/car/ip;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->b()V

    .line 73
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(I)V

    .line 74
    return-void
.end method

.method protected final a(Lcom/google/android/c/b/t;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 39
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/t;)V

    .line 40
    iget-object v1, p1, Lcom/google/android/c/b/t;->b:[I

    array-length v1, v1

    if-nez v1, :cond_0

    .line 41
    const-string v0, "CAR.VIDEO"

    const-string v1, "No configuration indices."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    :goto_0
    return-void

    .line 44
    :cond_0
    iget-object v1, p1, Lcom/google/android/c/b/t;->b:[I

    array-length v1, v1

    new-array v2, v1, [I

    .line 46
    iget-object v3, p1, Lcom/google/android/c/b/t;->b:[I

    array-length v4, v3

    move v1, v0

    :goto_1
    if-ge v0, v4, :cond_1

    aget v5, v3, v0

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    .line 47
    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    aput v5, v2, v1

    .line 48
    add-int/lit8 v1, v1, 0x1

    .line 46
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 50
    :cond_1
    iput-object v2, p0, Lcom/google/android/gms/car/senderprotocol/be;->g:[I

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 90
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 91
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoFocus lost focusState=0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " unsolicited="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->f:Lcom/google/android/gms/car/ip;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ip;->b()V

    .line 94
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/c/b/ct;

    invoke-direct {v0}, Lcom/google/android/c/b/ct;-><init>()V

    .line 56
    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/be;->c:I

    iput v1, v0, Lcom/google/android/c/b/ct;->a:I

    .line 57
    iput p1, v0, Lcom/google/android/c/b/ct;->b:I

    .line 58
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/senderprotocol/be;->a(Lcom/google/android/c/b/ct;)V

    .line 59
    return-void
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 98
    const-string v0, "CAR.VIDEO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 99
    const-string v0, "CAR.VIDEO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "VideoFocus gained focusState=0"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, " unsolicited="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->f:Lcom/google/android/gms/car/ip;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/be;->g:[I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/ip;->a([I)V

    .line 102
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 85
    const/4 v0, 0x2

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/be;->a(ILjava/lang/Integer;)V

    .line 86
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 77
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/be;->a(ILjava/lang/Integer;)V

    .line 78
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 81
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/be;->a(ILjava/lang/Integer;)V

    .line 82
    return-void
.end method

.method public final l_()V
    .locals 1

    .prologue
    .line 63
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/aq;->l_()V

    .line 65
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/be;->c:I

    .line 66
    return-void
.end method
