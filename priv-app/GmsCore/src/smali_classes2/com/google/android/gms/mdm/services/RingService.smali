.class public Lcom/google/android/gms/mdm/services/RingService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Landroid/media/MediaPlayer$OnErrorListener;
.implements Landroid/media/MediaPlayer$OnPreparedListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/android/volley/w;
.implements Lcom/android/volley/x;


# static fields
.field private static final a:Landroid/content/IntentFilter;

.field private static final b:[I


# instance fields
.field private c:Landroid/os/Handler;

.field private d:Landroid/media/AudioManager;

.field private e:Landroid/media/MediaPlayer;

.field private f:Lcom/google/android/gms/mdm/services/j;

.field private g:Lcom/google/android/gms/mdm/services/i;

.field private h:Landroid/view/WindowManager;

.field private i:Landroid/view/View;

.field private final j:Ljava/lang/Runnable;

.field private final k:Landroid/content/BroadcastReceiver;

.field private l:I

.field private m:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 58
    new-instance v0, Lcom/google/android/gms/mdm/services/RingService$1;

    invoke-direct {v0}, Lcom/google/android/gms/mdm/services/RingService$1;-><init>()V

    sput-object v0, Lcom/google/android/gms/mdm/services/RingService;->a:Landroid/content/IntentFilter;

    .line 64
    const/4 v0, 0x3

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/mdm/services/RingService;->b:[I

    return-void

    :array_0
    .array-data 4
        0x1
        0x4
        0x2
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    .line 49
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 80
    new-instance v0, Lcom/google/android/gms/mdm/services/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/mdm/services/g;-><init>(Lcom/google/android/gms/mdm/services/RingService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    .line 87
    new-instance v0, Lcom/google/android/gms/mdm/services/h;

    invoke-direct {v0, p0}, Lcom/google/android/gms/mdm/services/h;-><init>(Lcom/google/android/gms/mdm/services/RingService;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    .line 95
    iput v1, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    .line 96
    iput v1, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    .line 359
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/mdm/services/RingService;)Ljava/lang/Runnable;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    return-object v0
.end method

.method public static a(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 146
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/RingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 147
    invoke-virtual {p0, v0}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 148
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 139
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/services/RingService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 140
    const-string v1, "echoServerToken"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 141
    const-string v1, "includeBatteryStatus"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 142
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 143
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/mdm/services/RingService;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x2

    .line 49
    const-string v0, "Android Device Manager ringing [%s]"

    new-array v1, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getRingerMode()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    if-ne v0, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->getStreamVolume(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    invoke-virtual {v0, v3}, Landroid/media/AudioManager;->setRingerMode(I)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    invoke-virtual {v1, v3}, Landroid/media/AudioManager;->getStreamMaxVolume(I)I

    move-result v1

    invoke-virtual {v0, v3, v1, v4}, Landroid/media/AudioManager;->setStreamVolume(III)V

    if-eqz p1, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnErrorListener(Landroid/media/MediaPlayer$OnErrorListener;)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, p0}, Landroid/media/MediaPlayer;->setOnPreparedListener(Landroid/media/MediaPlayer$OnPreparedListener;)V

    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_1

    new-instance v0, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v0}, Landroid/media/AudioAttributes$Builder;-><init>()V

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Landroid/media/AudioAttributes$Builder;->setUsage(I)Landroid/media/AudioAttributes$Builder;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/media/MediaPlayer;->setAudioAttributes(Landroid/media/AudioAttributes;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v5}, Landroid/media/MediaPlayer;->setLooping(Z)V

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->prepareAsync()V

    :goto_1
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0, v3}, Landroid/media/MediaPlayer;->setAudioStreamType(I)V

    goto :goto_0

    :cond_2
    new-instance v0, Lcom/google/android/gms/mdm/services/j;

    invoke-direct {v0, p0, v4}, Lcom/google/android/gms/mdm/services/j;-><init>(Lcom/google/android/gms/mdm/services/RingService;B)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lcom/google/android/gms/mdm/services/j;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lcom/google/android/gms/mdm/services/j;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/j;->start()V

    goto :goto_1
.end method

.method private a(Landroid/net/Uri;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 328
    if-nez p1, :cond_0

    .line 337
    :goto_0
    return v0

    .line 332
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v2, p0, p1}, Landroid/media/MediaPlayer;->setDataSource(Landroid/content/Context;Landroid/net/Uri;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v1

    .line 333
    goto :goto_0

    .line 334
    :catch_0
    move-exception v2

    .line 335
    const-string v3, "Failed to play requested ringtone"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object v2, v1, v0

    const-string v2, "MDM"

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 336
    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v1}, Landroid/media/MediaPlayer;->reset()V

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/mdm/services/RingService;)Landroid/os/Handler;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/os/Handler;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 198
    const-string v0, "Failed to send the payload"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 199
    return-void
.end method

.method public final bridge synthetic a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 49
    return-void
.end method

.method final a()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 300
    new-instance v4, Landroid/media/RingtoneManager;

    invoke-direct {v4, p0}, Landroid/media/RingtoneManager;-><init>(Landroid/content/Context;)V

    .line 301
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 302
    invoke-virtual {v4}, Landroid/media/RingtoneManager;->getCursor()Landroid/database/Cursor;

    move-result-object v5

    move v1, v2

    .line 303
    :goto_0
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v6

    if-ge v1, v6, :cond_2

    .line 304
    invoke-interface {v5, v1}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 305
    invoke-interface {v5, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 306
    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-virtual {v4, v1}, Landroid/media/RingtoneManager;->getRingtoneUri(I)Landroid/net/Uri;

    move-result-object v6

    invoke-direct {p0, v6}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/net/Uri;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v2, v3

    .line 324
    :cond_0
    :goto_1
    return v2

    .line 303
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 317
    :cond_2
    sget-object v1, Lcom/google/android/gms/mdm/services/RingService;->b:[I

    array-length v4, v1

    move v0, v2

    :goto_2
    if-ge v0, v4, :cond_0

    aget v5, v1, v0

    .line 318
    invoke-static {p0, v5}, Landroid/media/RingtoneManager;->getActualDefaultRingtoneUri(Landroid/content/Context;I)Landroid/net/Uri;

    move-result-object v5

    invoke-direct {p0, v5}, Lcom/google/android/gms/mdm/services/RingService;->a(Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_3

    move v2, v3

    .line 319
    goto :goto_1

    .line 317
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    const/4 v1, -0x1

    .line 105
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/os/Handler;

    .line 106
    const-string v0, "audio"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    .line 108
    const-string v0, "window"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/WindowManager;

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/WindowManager;

    .line 109
    new-instance v0, Landroid/view/WindowManager$LayoutParams;

    const/16 v3, 0x7da

    const v4, 0x800c0

    const/4 v5, -0x2

    move v2, v1

    invoke-direct/range {v0 .. v5}, Landroid/view/WindowManager$LayoutParams;-><init>(IIIII)V

    .line 117
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p0}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/view/View;

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/view/View;

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/WindowManager;

    iget-object v2, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/view/View;

    invoke-interface {v1, v2, v0}, Landroid/view/WindowManager;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    sget-object v1, Lcom/google/android/gms/mdm/services/RingService;->a:Landroid/content/IntentFilter;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/mdm/services/RingService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 123
    new-instance v0, Landroid/support/v4/app/bk;

    invoke-direct {v0, p0}, Landroid/support/v4/app/bk;-><init>(Landroid/content/Context;)V

    sget v1, Lcom/google/android/gms/h;->cd:I

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(I)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->eY:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/services/RingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->a(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->rd:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/services/RingService;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/bk;->b(Ljava/lang/CharSequence;)Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/mdm/receivers/b;->a(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v4/app/bk;->d:Landroid/app/PendingIntent;

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->a()Landroid/support/v4/app/bk;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/RingService;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/f;->O:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, v0, Landroid/support/v4/app/bk;->y:I

    const/4 v1, 0x2

    iput v1, v0, Landroid/support/v4/app/bk;->j:I

    const-string v1, "alarm"

    iput-object v1, v0, Landroid/support/v4/app/bk;->w:Ljava/lang/String;

    const/4 v1, 0x1

    iput v1, v0, Landroid/support/v4/app/bk;->z:I

    invoke-virtual {v0}, Landroid/support/v4/app/bk;->b()Landroid/app/Notification;

    move-result-object v0

    .line 134
    sget v1, Lcom/google/android/gms/mdm/f/e;->a:I

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/mdm/services/RingService;->startForeground(ILandroid/app/Notification;)V

    .line 135
    return-void
.end method

.method public onDestroy()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v5, 0x1

    const/4 v4, -0x1

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Lcom/google/android/gms/mdm/services/i;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/mdm/services/i;->cancel(Z)Z

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Lcom/google/android/gms/mdm/services/i;

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    invoke-virtual {v0}, Landroid/media/MediaPlayer;->release()V

    iput-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lcom/google/android/gms/mdm/services/j;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->f:Lcom/google/android/gms/mdm/services/j;

    invoke-virtual {v0}, Lcom/google/android/gms/mdm/services/j;->a()V

    :cond_1
    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    if-eq v0, v4, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/mdm/services/RingService;->l:I

    const/4 v3, 0x0

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->setStreamVolume(III)V

    :cond_2
    iget v0, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    if-eq v0, v4, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->d:Landroid/media/AudioManager;

    iget v1, p0, Lcom/google/android/gms/mdm/services/RingService;->m:I

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->setRingerMode(I)V

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->c:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->j:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->k:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/services/RingService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 180
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->h:Landroid/view/WindowManager;

    iget-object v1, p0, Lcom/google/android/gms/mdm/services/RingService;->i:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeView(Landroid/view/View;)V

    .line 181
    invoke-virtual {p0, v5}, Lcom/google/android/gms/mdm/services/RingService;->stopForeground(Z)V

    .line 182
    return-void
.end method

.method public onError(Landroid/media/MediaPlayer;II)Z
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 208
    const-string v0, "Error playing ringtone, what: %s extra: %s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v4

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/RingService;->stopSelf()V

    .line 210
    return v4
.end method

.method public onPrepared(Landroid/media/MediaPlayer;)V
    .locals 0

    .prologue
    .line 203
    invoke-virtual {p1}, Landroid/media/MediaPlayer;->start()V

    .line 204
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Lcom/google/android/gms/mdm/services/i;

    if-nez v0, :cond_0

    .line 154
    new-instance v0, Landroid/media/MediaPlayer;

    invoke-direct {v0}, Landroid/media/MediaPlayer;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->e:Landroid/media/MediaPlayer;

    .line 155
    new-instance v0, Lcom/google/android/gms/mdm/services/i;

    invoke-direct {v0, p0, v3}, Lcom/google/android/gms/mdm/services/i;-><init>(Lcom/google/android/gms/mdm/services/RingService;B)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Lcom/google/android/gms/mdm/services/i;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/mdm/services/RingService;->g:Lcom/google/android/gms/mdm/services/i;

    new-array v2, v3, [Ljava/lang/Void;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/mdm/services/i;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 160
    :cond_0
    const-string v0, "includeBatteryStatus"

    invoke-virtual {p1, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    invoke-static {p0}, Lcom/google/android/gms/mdm/f/a;->a(Landroid/content/Context;)Lcom/google/af/a/c/a/a;

    move-result-object v2

    .line 164
    :goto_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    aput v3, v0, v3

    const-string v3, "echoServerToken"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    move-object v4, v1

    move-object v5, p0

    move-object v6, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/mdm/a/b;->a([ILandroid/location/Location;Lcom/google/af/a/c/a/a;Ljava/lang/String;Lcom/google/af/a/c/a/h;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;

    .line 168
    const/4 v0, 0x2

    return v0

    :cond_1
    move-object v2, v1

    goto :goto_0
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1

    .prologue
    .line 186
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/services/RingService;->stopSelf()V

    .line 188
    const/4 v0, 0x1

    return v0
.end method
