.class abstract Lcom/google/android/gms/common/internal/ag;
.super Lcom/google/android/gms/common/internal/ae;
.source "SourceFile"


# instance fields
.field final d:Ljava/lang/CharSequence;

.field final e:Z

.field f:I


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/common/internal/aa;Ljava/lang/CharSequence;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 506
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/internal/ae;-><init>(B)V

    .line 504
    iput v0, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    .line 507
    iget-boolean v0, p1, Lcom/google/android/gms/common/internal/aa;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ag;->e:Z

    .line 508
    iput-object p2, p0, Lcom/google/android/gms/common/internal/ag;->d:Ljava/lang/CharSequence;

    .line 509
    return-void
.end method


# virtual methods
.method abstract a(I)I
.end method

.method protected final synthetic a()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v3, -0x1

    .line 496
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    if-eq v0, v3, :cond_3

    iget v1, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    iget v0, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/ag;->a(I)I

    move-result v0

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/internal/ag;->d:Ljava/lang/CharSequence;

    invoke-interface {v0}, Ljava/lang/CharSequence;->length()I

    move-result v0

    iput v3, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/common/internal/ag;->e:Z

    if-eqz v2, :cond_1

    if-eq v1, v0, :cond_0

    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ag;->d:Ljava/lang/CharSequence;

    invoke-interface {v2, v1, v0}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    return-object v0

    :cond_2
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/ag;->b(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/common/internal/ag;->f:I

    goto :goto_0

    :cond_3
    sget-object v0, Lcom/google/android/gms/common/internal/af;->c:Lcom/google/android/gms/common/internal/af;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    const/4 v0, 0x0

    goto :goto_1
.end method

.method abstract b(I)I
.end method
