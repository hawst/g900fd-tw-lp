.class public final Lcom/google/android/gms/games/ui/signin/j;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/s;


# instance fields
.field private b:Lcom/google/android/gms/games/i/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 24
    const/16 v0, 0x8

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 41
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/j;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 61
    :goto_0
    return-void

    .line 45
    :cond_0
    if-eqz p1, :cond_1

    .line 46
    const-string v0, "RecordSignInFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error found when recording sign in attempt: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    invoke-static {p1}, Lcom/google/android/gms/games/ui/signin/j;->c(I)I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/ui/signin/j;->a(II)V

    goto :goto_0

    .line 54
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v0

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/j;->b:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c()Z

    move-result v1

    if-nez v1, :cond_2

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/j;->b:Lcom/google/android/gms/games/i/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/ea;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 60
    :cond_2
    :goto_1
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/j;->b(I)V

    goto :goto_0

    .line 56
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 6

    .prologue
    .line 34
    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/j;->b:Lcom/google/android/gms/games/i/b;

    .line 35
    iget-object v4, p0, Lcom/google/android/gms/games/ui/signin/j;->b:Lcom/google/android/gms/games/i/b;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->l()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v0, v4, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v1, Lcom/google/android/gms/games/i/v;

    invoke-direct {v1, v4, p0}, Lcom/google/android/gms/games/i/v;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/i/s;)V

    iget-object v3, v4, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    iget-object v4, v4, Lcom/google/android/gms/games/i/b;->e:[Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 36
    :goto_0
    return-void

    .line 35
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0xc

    return v0
.end method
