.class public final Lcom/google/android/gms/games/service/a/l/d;
.super Lcom/google/android/gms/games/service/a/b;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:I

.field private final e:Ljava/util/ArrayList;

.field private final f:I

.field private g:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;IILjava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 33
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p5}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 34
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/l/d;->b:Lcom/google/android/gms/games/internal/dr;

    .line 35
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/l/d;->c:Lcom/google/android/gms/games/a/au;

    .line 36
    iput p4, p0, Lcom/google/android/gms/games/service/a/l/d;->d:I

    .line 37
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/l/d;->e:Ljava/util/ArrayList;

    .line 38
    iput p3, p0, Lcom/google/android/gms/games/service/a/l/d;->f:I

    .line 39
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 44
    if-ltz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p2, v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 47
    if-nez p2, :cond_0

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/d;->c:Lcom/google/android/gms/games/a/au;

    new-instance v1, Lcom/google/android/gms/games/service/ad;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/ad;-><init>()V

    invoke-virtual {p1, v0, v1}, Lcom/google/android/gms/games/a/t;->i(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/service/a/l/d;->g:I

    .line 52
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 53
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l/d;->c:Lcom/google/android/gms/games/a/au;

    iget v2, p0, Lcom/google/android/gms/games/service/a/l/d;->f:I

    iget v4, p0, Lcom/google/android/gms/games/service/a/l/d;->d:I

    iget v5, p0, Lcom/google/android/gms/games/service/a/l/d;->g:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 44
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 59
    .line 60
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    move v1, v0

    move v2, v0

    .line 61
    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/d;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/internal/b/g;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 63
    aget-object v4, p1, v1

    invoke-virtual {v3, v0, v4}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 64
    aget-object v0, p1, v1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    add-int/2addr v2, v0

    .line 61
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/d;->b:Lcom/google/android/gms/games/internal/dr;

    iget v1, p0, Lcom/google/android/gms/games/service/a/l/d;->g:I

    invoke-static {v2, v1}, Lcom/google/android/gms/games/a/l;->a(II)I

    move-result v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/gms/games/internal/dr;->b(ILandroid/os/Bundle;)V

    .line 67
    return-void
.end method
