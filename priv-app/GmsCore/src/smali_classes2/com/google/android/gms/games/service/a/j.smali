.class public final Lcom/google/android/gms/games/service/a/j;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Z

.field private final d:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/j;->b:Lcom/google/android/gms/games/internal/dr;

    .line 24
    iput-boolean p3, p0, Lcom/google/android/gms/games/service/a/j;->c:Z

    .line 25
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/j;->d:Landroid/os/Bundle;

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j;->b:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->b(I)V

    .line 38
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/a/j;->c:Z

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/j;->d:Landroid/os/Bundle;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/common/server/ClientContext;ZLandroid/os/Bundle;)I

    move-result v0

    return v0
.end method
