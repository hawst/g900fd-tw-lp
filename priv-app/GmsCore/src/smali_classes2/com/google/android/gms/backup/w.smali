.class public final Lcom/google/android/gms/backup/w;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:J

.field b:Z

.field c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:J

.field private g:Z

.field private h:J

.field private i:Z

.field private j:J

.field private k:Z

.field private l:J

.field private m:Z

.field private n:I

.field private o:Z

.field private p:Ljava/lang/String;

.field private q:Z

.field private r:J

.field private s:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 338
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 343
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->a:J

    .line 360
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->f:J

    .line 377
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->h:J

    .line 394
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/w;->c:Ljava/lang/String;

    .line 411
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->j:J

    .line 428
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->l:J

    .line 445
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/backup/w;->n:I

    .line 462
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/w;->p:Ljava/lang/String;

    .line 479
    iput-wide v2, p0, Lcom/google/android/gms/backup/w;->r:J

    .line 546
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/w;->s:I

    .line 338
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 549
    iget v0, p0, Lcom/google/android/gms/backup/w;->s:I

    if-gez v0, :cond_0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/gms/backup/w;->b()I

    .line 553
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/w;->s:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 335
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->d:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->e:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->f:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->g:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->h:J

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->b:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/w;->c:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->k()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->i:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->j:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->k:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->l:J

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->m:Z

    iput v0, p0, Lcom/google/android/gms/backup/w;->n:I

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->o:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/w;->p:Ljava/lang/String;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/w;->q:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/w;->r:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x40 -> :sswitch_1
        0x48 -> :sswitch_2
        0x50 -> :sswitch_3
        0x6a -> :sswitch_4
        0x71 -> :sswitch_5
        0x78 -> :sswitch_6
        0x80 -> :sswitch_7
        0x8a -> :sswitch_8
        0x98 -> :sswitch_9
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 517
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->d:Z

    if-eqz v0, :cond_0

    .line 518
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 520
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->e:Z

    if-eqz v0, :cond_1

    .line 521
    const/16 v0, 0x9

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->f:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->a(IJ)V

    .line 523
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->g:Z

    if-eqz v0, :cond_2

    .line 524
    const/16 v0, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->h:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->a(IJ)V

    .line 526
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->b:Z

    if-eqz v0, :cond_3

    .line 527
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/backup/w;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 529
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->i:Z

    if-eqz v0, :cond_4

    .line 530
    const/16 v0, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->c(IJ)V

    .line 532
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->k:Z

    if-eqz v0, :cond_5

    .line 533
    const/16 v0, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->l:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->a(IJ)V

    .line 535
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->m:Z

    if-eqz v0, :cond_6

    .line 536
    const/16 v0, 0x10

    iget v1, p0, Lcom/google/android/gms/backup/w;->n:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(II)V

    .line 538
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->o:Z

    if-eqz v0, :cond_7

    .line 539
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/gms/backup/w;->p:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 541
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/backup/w;->q:Z

    if-eqz v0, :cond_8

    .line 542
    const/16 v0, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->r:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->a(IJ)V

    .line 544
    :cond_8
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 558
    const/4 v0, 0x0

    .line 559
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->d:Z

    if-eqz v1, :cond_0

    .line 560
    const/16 v0, 0x8

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->a:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 563
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->e:Z

    if-eqz v1, :cond_1

    .line 564
    const/16 v1, 0x9

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->f:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->g:Z

    if-eqz v1, :cond_2

    .line 568
    const/16 v1, 0xa

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->h:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 571
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->b:Z

    if-eqz v1, :cond_3

    .line 572
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/backup/w;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->i:Z

    if-eqz v1, :cond_4

    .line 576
    const/16 v1, 0xe

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->j:J

    invoke-static {v1}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x8

    add-int/2addr v0, v1

    .line 579
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->k:Z

    if-eqz v1, :cond_5

    .line 580
    const/16 v1, 0xf

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->l:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 583
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->m:Z

    if-eqz v1, :cond_6

    .line 584
    const/16 v1, 0x10

    iget v2, p0, Lcom/google/android/gms/backup/w;->n:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->e(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 587
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->o:Z

    if-eqz v1, :cond_7

    .line 588
    const/16 v1, 0x11

    iget-object v2, p0, Lcom/google/android/gms/backup/w;->p:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 591
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/backup/w;->q:Z

    if-eqz v1, :cond_8

    .line 592
    const/16 v1, 0x13

    iget-wide v2, p0, Lcom/google/android/gms/backup/w;->r:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 595
    :cond_8
    iput v0, p0, Lcom/google/android/gms/backup/w;->s:I

    .line 596
    return v0
.end method
