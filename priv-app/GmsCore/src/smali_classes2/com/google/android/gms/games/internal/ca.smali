.class final Lcom/google/android/gms/games/internal/ca;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 1435
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 1436
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ca;->a:Lcom/google/android/gms/common/api/m;

    .line 1437
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 1441
    const/4 v0, 0x0

    invoke-static {v0}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 1442
    iget-object v1, p0, Lcom/google/android/gms/games/internal/ca;->a:Lcom/google/android/gms/common/api/m;

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 1443
    return-void
.end method
