.class public final Lcom/google/android/gms/common/internal/bs;
.super Lcom/google/android/gms/common/internal/br;
.source "SourceFile"


# instance fields
.field public d:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/br;-><init>(Ljava/lang/String;)V

    .line 144
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/bs;->d:Z

    .line 148
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 10

    .prologue
    const/4 v0, 0x0

    const/4 v9, 0x7

    const/16 v8, 0x2d

    const/4 v1, 0x0

    .line 157
    iget-object v2, p0, Lcom/google/android/gms/common/internal/bs;->a:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 215
    :cond_0
    :goto_0
    return-object v0

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/internal/bs;->a:Ljava/lang/String;

    const-string v3, "/"

    invoke-virtual {v2, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 163
    array-length v4, v3

    .line 165
    if-lt v4, v9, :cond_0

    const/16 v2, 0x9

    if-gt v4, v2, :cond_0

    .line 170
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    .line 171
    const/4 v0, 0x1

    move v2, v1

    .line 173
    :goto_1
    if-ge v2, v4, :cond_3

    .line 174
    if-ge v2, v9, :cond_2

    .line 175
    aget-object v6, v3, v2

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "/"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 173
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 180
    :cond_2
    aget-object v4, v3, v2

    const-string v6, "."

    invoke-virtual {v4, v6}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 181
    aget-object v0, v3, v2

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 190
    :cond_3
    iget v2, p0, Lcom/google/android/gms/common/internal/bs;->b:I

    if-ltz v2, :cond_5

    .line 191
    if-nez v0, :cond_4

    .line 192
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 194
    :cond_4
    const/16 v0, 0x73

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/common/internal/bs;->b:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move v0, v1

    .line 198
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/common/internal/bs;->c:Z

    if-eqz v2, :cond_7

    .line 199
    if-nez v0, :cond_6

    .line 200
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 202
    :cond_6
    const-string v0, "fbw=1"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move v0, v1

    .line 206
    :cond_7
    iget-boolean v2, p0, Lcom/google/android/gms/common/internal/bs;->d:Z

    if-nez v2, :cond_a

    .line 207
    if-nez v0, :cond_8

    .line 208
    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 210
    :cond_8
    const-string v0, "ns"

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 214
    :goto_2
    if-eqz v1, :cond_9

    const-string v0, ""

    :goto_3
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 215
    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 214
    :cond_9
    const-string v0, "/"

    goto :goto_3

    :cond_a
    move v1, v0

    goto :goto_2
.end method
