.class public Lcom/google/android/gms/drive/StorageStats;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:J

.field final c:J

.field final d:J

.field final e:J

.field final f:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/drive/ap;

    invoke-direct {v0}, Lcom/google/android/gms/drive/ap;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/StorageStats;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IJJJJI)V
    .locals 0

    .prologue
    .line 96
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 97
    iput p1, p0, Lcom/google/android/gms/drive/StorageStats;->a:I

    .line 98
    iput-wide p2, p0, Lcom/google/android/gms/drive/StorageStats;->b:J

    .line 99
    iput-wide p4, p0, Lcom/google/android/gms/drive/StorageStats;->c:J

    .line 100
    iput-wide p6, p0, Lcom/google/android/gms/drive/StorageStats;->d:J

    .line 101
    iput-wide p8, p0, Lcom/google/android/gms/drive/StorageStats;->e:J

    .line 102
    iput p10, p0, Lcom/google/android/gms/drive/StorageStats;->f:I

    .line 103
    return-void
.end method

.method public constructor <init>(JJJJI)V
    .locals 11

    .prologue
    .line 114
    const/4 v1, 0x1

    move-object v0, p0

    move-wide v2, p1

    move-wide v4, p3

    move-wide/from16 v6, p5

    move-wide/from16 v8, p7

    move/from16 v10, p9

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/drive/StorageStats;-><init>(IJJJJI)V

    .line 116
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 81
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/ap;->a(Lcom/google/android/gms/drive/StorageStats;Landroid/os/Parcel;)V

    .line 82
    return-void
.end method
