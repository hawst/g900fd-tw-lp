.class final Lcom/google/android/gms/car/a;
.super Lcom/google/android/gms/car/np;
.source "SourceFile"


# instance fields
.field private final a:I

.field private final b:I

.field private final c:I

.field private d:[Ljava/nio/ByteBuffer;


# direct methods
.method public constructor <init>(III)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 32
    const/4 v0, 0x0

    invoke-direct {p0, v1, v1, v0}, Lcom/google/android/gms/car/np;-><init>(ZZLcom/google/android/gms/car/ns;)V

    .line 33
    iput p1, p0, Lcom/google/android/gms/car/a;->a:I

    .line 34
    iput p2, p0, Lcom/google/android/gms/car/a;->b:I

    .line 35
    iput p3, p0, Lcom/google/android/gms/car/a;->c:I

    .line 36
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/car/e;)I
    .locals 14
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 60
    const v0, 0xf4240

    iget v1, p0, Lcom/google/android/gms/car/a;->a:I

    iget v3, p0, Lcom/google/android/gms/car/a;->c:I

    mul-int/2addr v1, v3

    iget v3, p0, Lcom/google/android/gms/car/a;->b:I

    div-int/lit8 v3, v3, 0x8

    mul-int/2addr v1, v3

    div-int/2addr v0, v1

    int-to-long v10, v0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/car/a;->d:[Ljava/nio/ByteBuffer;

    if-nez v0, :cond_0

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/car/a;->j:Landroid/media/MediaCodec;

    invoke-virtual {v0}, Landroid/media/MediaCodec;->getInputBuffers()[Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/a;->d:[Ljava/nio/ByteBuffer;

    .line 67
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v9

    .line 70
    iget-wide v4, p1, Lcom/google/android/gms/car/e;->a:J

    .line 71
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    move v7, v2

    move v8, v2

    .line 73
    :goto_0
    if-ge v8, v9, :cond_1

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/car/a;->j:Landroid/media/MediaCodec;

    const-wide/32 v12, 0x7a120

    invoke-virtual {v0, v12, v13}, Landroid/media/MediaCodec;->dequeueInputBuffer(J)I

    move-result v1

    .line 75
    if-gez v1, :cond_2

    .line 78
    const-string v0, "CAR.AUDIO"

    const-string v1, "Encoder input buffer unavailable. Dropping audio data"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_1
    return v7

    .line 82
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/a;->d:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/car/a;->d:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    .line 84
    sub-int v3, v9, v8

    .line 85
    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v3

    .line 88
    iget-object v0, p0, Lcom/google/android/gms/car/a;->d:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    iget-object v6, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v0, v6, v8, v3}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 90
    iget-object v0, p0, Lcom/google/android/gms/car/a;->j:Landroid/media/MediaCodec;

    move v6, v2

    invoke-virtual/range {v0 .. v6}, Landroid/media/MediaCodec;->queueInputBuffer(IIIJI)V

    .line 91
    add-int v1, v8, v3

    .line 92
    const-wide/16 v12, 0x0

    cmp-long v0, v4, v12

    if-eqz v0, :cond_3

    .line 93
    int-to-long v12, v3

    mul-long/2addr v12, v10

    add-long/2addr v4, v12

    .line 96
    :cond_3
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    move v8, v1

    .line 97
    goto :goto_0
.end method

.method protected final a()Landroid/media/MediaCodec;
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 41
    const-string v0, "audio/mp4a-latm"

    iget v1, p0, Lcom/google/android/gms/car/a;->a:I

    iget v2, p0, Lcom/google/android/gms/car/a;->c:I

    invoke-static {v0, v1, v2}, Landroid/media/MediaFormat;->createAudioFormat(Ljava/lang/String;II)Landroid/media/MediaFormat;

    move-result-object v0

    .line 43
    const-string v1, "aac-profile"

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 44
    iget v1, p0, Lcom/google/android/gms/car/a;->a:I

    iget v2, p0, Lcom/google/android/gms/car/a;->c:I

    mul-int/2addr v1, v2

    mul-int/lit8 v1, v1, 0x10

    .line 45
    const-string v2, "bitrate"

    div-int/lit8 v1, v1, 0x2

    const v3, 0x7d000

    invoke-static {v1, v3}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-virtual {v0, v2, v1}, Landroid/media/MediaFormat;->setInteger(Ljava/lang/String;I)V

    .line 47
    const-string v1, "audio/mp4a-latm"

    invoke-static {v1}, Landroid/media/MediaCodec;->createEncoderByType(Ljava/lang/String;)Landroid/media/MediaCodec;

    move-result-object v1

    .line 48
    const/4 v2, 0x1

    invoke-virtual {v1, v0, v4, v4, v2}, Landroid/media/MediaCodec;->configure(Landroid/media/MediaFormat;Landroid/view/Surface;Landroid/media/MediaCrypto;I)V

    .line 50
    invoke-virtual {v1}, Landroid/media/MediaCodec;->start()V

    .line 51
    return-object v1
.end method
