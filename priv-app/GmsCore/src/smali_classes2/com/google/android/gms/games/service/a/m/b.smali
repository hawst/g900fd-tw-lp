.class public final Lcom/google/android/gms/games/service/a/m/b;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/service/b;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 23
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 24
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/b;->b:Lcom/google/android/gms/games/internal/dr;

    .line 25
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/m/b;->c:Lcom/google/android/gms/games/service/b;

    .line 26
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/m/b;->e:Ljava/lang/String;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/b;->d:Lcom/google/android/gms/games/a/au;

    .line 28
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/b;->c:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 44
    return-void
.end method

.method protected final a(I)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/b;->b:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/b;->e:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/dr;->e(ILjava/lang/String;)V

    .line 39
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/b;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/b;->c:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/m/b;->e:Ljava/lang/String;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I

    move-result v0

    return v0
.end method
