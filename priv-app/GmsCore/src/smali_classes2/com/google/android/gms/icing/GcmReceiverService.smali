.class public Lcom/google/android/gms/icing/GcmReceiverService;
.super Landroid/app/Service;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Intent;)[B
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 23
    if-nez p0, :cond_0

    .line 38
    :goto_0
    return-object v0

    .line 24
    :cond_0
    invoke-virtual {p0}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v1

    .line 25
    if-nez v1, :cond_1

    .line 26
    const-string v1, "Gcm message has no extras"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 29
    :cond_1
    const-string v2, "icing-gcm-msg-base64"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 30
    if-nez v1, :cond_2

    .line 31
    const-string v1, "Gcm message payload missing"

    invoke-static {v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 35
    :cond_2
    const/4 v2, 0x0

    :try_start_0
    invoke-static {v1, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 36
    :catch_0
    move-exception v1

    .line 37
    const-string v2, "Gcm message payload decode failed: %s"

    invoke-virtual {v1}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 44
    const-string v1, "Received gcm intent: %s extras: %s"

    if-eqz p1, :cond_0

    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v0

    :goto_0
    invoke-static {v1, p1, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 46
    const-class v0, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 47
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/GcmReceiverService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 48
    const/4 v0, 0x2

    return v0

    .line 44
    :cond_0
    const-string v0, ""

    goto :goto_0
.end method
