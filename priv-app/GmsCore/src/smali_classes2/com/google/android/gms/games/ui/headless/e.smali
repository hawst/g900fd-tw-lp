.class final Lcom/google/android/gms/games/ui/headless/e;
.super Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/headless/b;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/headless/b;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 277
    iput-object p1, p0, Lcom/google/android/gms/games/ui/headless/e;->a:Lcom/google/android/gms/games/ui/headless/b;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListTabStrip;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected final a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2

    .prologue
    .line 281
    sget v0, Lcom/google/android/gms/l;->br:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/view/View;Landroid/support/v4/view/at;I)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 287
    invoke-virtual {p0, p3}, Lcom/google/android/gms/games/ui/headless/e;->a(I)Landroid/view/View$OnClickListener;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 288
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/e;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {p2, p3}, Landroid/support/v4/view/at;->b(I)Ljava/lang/CharSequence;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    const/4 v0, 0x0

    instance-of v1, p2, Lcom/google/android/gms/games/ui/d/m;

    if-eqz v1, :cond_0

    check-cast p2, Lcom/google/android/gms/games/ui/d/m;

    invoke-interface {p2}, Lcom/google/android/gms/games/ui/d/m;->g()Lcom/google/android/gms/games/ui/d/l;

    move-result-object v0

    :cond_0
    if-eqz v0, :cond_5

    invoke-interface {v0, p3}, Lcom/google/android/gms/games/ui/d/l;->c(I)I

    move-result v0

    move v1, v0

    :goto_0
    sget v0, Lcom/google/android/gms/j;->ki:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-lez v1, :cond_4

    new-instance v4, Landroid/graphics/drawable/GradientDrawable;

    invoke-direct {v4}, Landroid/graphics/drawable/GradientDrawable;-><init>()V

    const/16 v5, 0xa

    if-lt v1, v5, :cond_1

    invoke-virtual {v4, v2}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    sget v5, Lcom/google/android/gms/g;->Q:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    int-to-float v3, v3

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/GradientDrawable;->setCornerRadius(F)V

    :goto_1
    const/4 v3, -0x1

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/GradientDrawable;->setColor(I)V

    const/16 v3, 0x10

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_2
    const/16 v3, 0x63

    if-le v1, v3, :cond_3

    sget v1, Lcom/google/android/gms/p;->iG:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    :goto_3
    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 289
    :goto_4
    return-void

    .line 288
    :cond_1
    const/4 v3, 0x1

    invoke-virtual {v4, v3}, Landroid/graphics/drawable/GradientDrawable;->setShape(I)V

    goto :goto_1

    :cond_2
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_2

    :cond_3
    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_4
    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_4

    :cond_5
    move v1, v2

    goto :goto_0
.end method
