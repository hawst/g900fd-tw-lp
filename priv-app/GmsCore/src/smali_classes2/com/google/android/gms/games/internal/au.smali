.class final Lcom/google/android/gms/games/internal/au;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 789
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 790
    iput-object p1, p0, Lcom/google/android/gms/games/internal/au;->a:Lcom/google/android/gms/common/api/aj;

    .line 791
    return-void
.end method


# virtual methods
.method public final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 814
    iget-object v0, p0, Lcom/google/android/gms/games/internal/au;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/at;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/at;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 815
    return-void
.end method

.method public final r(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 796
    new-instance v1, Lcom/google/android/gms/games/multiplayer/turnbased/c;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 797
    const/4 v0, 0x0

    .line 799
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 800
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 803
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->w_()V

    .line 807
    if-eqz v0, :cond_1

    .line 808
    iget-object v1, p0, Lcom/google/android/gms/games/internal/au;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v2, Lcom/google/android/gms/games/internal/av;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/av;-><init>(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 810
    :cond_1
    return-void

    .line 803
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/turnbased/c;->w_()V

    throw v0
.end method
