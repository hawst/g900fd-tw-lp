.class public final Lcom/google/android/gms/common/internal/bq;
.super Lcom/google/android/gms/common/internal/br;
.source "SourceFile"


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 104
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/br;-><init>(Ljava/lang/String;)V

    .line 105
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/common/internal/bq;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 111
    const/4 v0, 0x0

    .line 125
    :goto_0
    return-object v0

    .line 114
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    iget-object v0, p0, Lcom/google/android/gms/common/internal/bq;->a:Ljava/lang/String;

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/common/internal/bq;->a:Ljava/lang/String;

    const-string v3, "="

    invoke-virtual {v0, v3}, Ljava/lang/String;->lastIndexOf(Ljava/lang/String;)I

    move-result v0

    const/4 v3, -0x1

    if-ne v0, v3, :cond_2

    const/4 v0, 0x1

    .line 117
    :goto_1
    iget v3, p0, Lcom/google/android/gms/common/internal/bq;->b:I

    if-ltz v3, :cond_5

    .line 118
    if-eqz v0, :cond_3

    const-string v0, "="

    :goto_2
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "s"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v3, p0, Lcom/google/android/gms/common/internal/bq;->b:I

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 122
    :goto_3
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/bq;->c:Z

    if-eqz v0, :cond_1

    .line 123
    if-eqz v1, :cond_4

    const-string v0, "="

    :goto_4
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "fbw=1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    :cond_1
    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 115
    goto :goto_1

    .line 118
    :cond_3
    const-string v0, "-"

    goto :goto_2

    .line 123
    :cond_4
    const-string v0, "-"

    goto :goto_4

    :cond_5
    move v1, v0

    goto :goto_3
.end method
