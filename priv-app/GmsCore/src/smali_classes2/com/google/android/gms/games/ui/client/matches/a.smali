.class public final Lcom/google/android/gms/games/ui/client/matches/a;
.super Lcom/google/android/gms/games/ui/common/matches/u;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/games/ui/client/a;

.field private final b:Lcom/google/android/gms/games/ui/client/a/a;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/a;)V
    .locals 2

    .prologue
    .line 52
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/common/matches/u;-><init>()V

    .line 53
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 54
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    .line 56
    new-instance v0, Lcom/google/android/gms/games/ui/client/a/a;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/ui/client/a/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->b:Lcom/google/android/gms/games/ui/client/a/a;

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v1, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 60
    return-void
.end method

.method private d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 313
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 316
    const-string v2, "turn_based_match"

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 317
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/4 v2, -0x1

    invoke-virtual {v0, v2, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(ILandroid/content/Intent;)V

    .line 318
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 319
    return-void
.end method

.method private e(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 5

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 85
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 86
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "acceptInvitation: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 113
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->z_()Z

    move-result v1

    if-nez v1, :cond_1

    .line 91
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "acceptInvitation: invitation not valid anymore..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 95
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    sget v2, Lcom/google/android/gms/p;->ks:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 98
    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    .line 99
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 102
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/matches/b;-><init>(Lcom/google/android/gms/games/ui/client/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0

    .line 111
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/InvitationEntity;

    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    const-string v3, "invitation"

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v4}, Lcom/google/android/gms/games/ui/client/a;->P()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v2, v3, v0, v1, v4}, Lcom/google/android/gms/common/internal/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "Unable to return invitation to game. Something has gone very wrong."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0

    :cond_3
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v0, v2}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/ui/client/a;->setResult(ILandroid/content/Intent;)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 161
    const-string v0, "Trying to install game from Client UI!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 162
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 199
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 200
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 201
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/a;->c(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 200
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 203
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 187
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 188
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onInvitationClusterSeeMoreClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    :goto_0
    return-void

    .line 192
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->k:Lcom/google/android/gms/games/multiplayer/d;

    invoke-interface {v1, v0, p1, p2, p3}, Lcom/google/android/gms/games/multiplayer/d;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/client/a;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 2

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 65
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onInvitationAccepted: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 76
    :goto_0
    return-void

    .line 70
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/games/multiplayer/Invitation;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, p1, p0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/app/Activity;Lcom/google/android/gms/games/multiplayer/Invitation;Lcom/google/android/gms/games/ui/d/aq;)V

    goto :goto_0

    .line 74
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/a;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/Invitation;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 172
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 174
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onInvitationParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->k()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->e()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 0

    .prologue
    .line 234
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/a;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    .line 235
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v1

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 280
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onMatchParticipantListClicked: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 287
    :goto_0
    return-void

    .line 284
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->l()Ljava/util/ArrayList;

    move-result-object v2

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->v()I

    move-result v3

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->a()Lcom/google/android/gms/games/Game;

    move-result-object v6

    move-object v4, p2

    move-object v5, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/Game;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/turnbased/f;)V
    .locals 4

    .prologue
    .line 214
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 215
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/f;->c()Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;

    move-result-object v1

    .line 217
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/ui/d/a;->dismiss(Landroid/support/v4/app/q;Ljava/lang/String;)V

    .line 219
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 220
    sget v0, Lcom/google/android/gms/p;->iU:I

    invoke-static {v0}, Lcom/google/android/gms/games/ui/b/d;->a(I)Lcom/google/android/gms/games/ui/b/d;

    move-result-object v0

    .line 222
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v2, "com.google.android.gms.games.ui.dialog.alertDialogNetworkError"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 230
    :goto_0
    return-void

    .line 224
    :cond_0
    if-nez v1, :cond_1

    .line 225
    const-string v1, "ClientMultiplayerInboxHelper"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No turn-based match received after accepting invite: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 229
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/a;->d(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->b:Lcom/google/android/gms/games/ui/client/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 167
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;)V
    .locals 4

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->d()Ljava/util/ArrayList;

    move-result-object v2

    .line 208
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 209
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Invitation;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/a;->d(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 208
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 211
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/a;->e(Lcom/google/android/gms/games/multiplayer/Invitation;)V

    .line 81
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 3

    .prologue
    .line 239
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 241
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onMatchDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    :goto_0
    return-void

    .line 245
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->e(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/Game;)V
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->b:Lcom/google/android/gms/games/ui/client/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/a/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 273
    return-void
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 118
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 119
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onInvitationDeclined: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    :goto_0
    return-void

    .line 123
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    .line 124
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v2

    .line 125
    packed-switch v1, :pswitch_data_0

    .line 133
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 127
    :pswitch_0
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0

    .line 130
    :pswitch_1
    sget-object v1, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/realtime/b;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0

    .line 125
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;)V
    .locals 4

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onMatchRematch: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    :goto_0
    return-void

    .line 256
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    sget v2, Lcom/google/android/gms/p;->kx:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/a;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 257
    invoke-static {v1}, Lcom/google/android/gms/games/ui/b/f;->a(Ljava/lang/String;)Lcom/google/android/gms/games/ui/b/f;

    move-result-object v1

    .line 258
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    const-string v3, "com.google.android.gms.games.ui.dialog.progressDialogStartingTbmp"

    invoke-static {v2, v1, v3}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    .line 261
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/turnbased/TurnBasedMatch;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/matches/c;-><init>(Lcom/google/android/gms/games/ui/client/matches/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/Invitation;)V
    .locals 4

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 140
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/a;->a:Lcom/google/android/gms/games/ui/client/a;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 141
    const-string v0, "ClientMultiplayerInboxHelper"

    const-string v1, "onInvitationDismissed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 153
    :goto_0
    return-void

    .line 145
    :cond_0
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->i()I

    move-result v1

    .line 146
    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/Invitation;->f()Ljava/lang/String;

    move-result-object v2

    .line 147
    packed-switch v1, :pswitch_data_0

    .line 155
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unknown invitation type "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 149
    :pswitch_0
    sget-object v1, Lcom/google/android/gms/games/d;->l:Lcom/google/android/gms/games/multiplayer/turnbased/e;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/turnbased/e;->d(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0

    .line 152
    :pswitch_1
    sget-object v1, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/multiplayer/realtime/b;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
