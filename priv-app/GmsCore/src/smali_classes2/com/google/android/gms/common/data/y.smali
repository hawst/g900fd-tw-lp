.class public final Lcom/google/android/gms/common/data/y;
.super Lcom/google/android/gms/common/data/t;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/HashSet;

.field private final c:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/a;)V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/t;-><init>(Lcom/google/android/gms/common/data/d;)V

    .line 22
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/y;->b:Ljava/util/HashSet;

    .line 23
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/data/y;->c:Ljava/util/ArrayList;

    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/common/data/y;->g()V

    .line 28
    return-void
.end method

.method private g()V
    .locals 4

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 85
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/common/data/y;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v1

    :goto_0
    if-ge v0, v1, :cond_1

    .line 86
    iget-object v2, p0, Lcom/google/android/gms/common/data/y;->b:Ljava/util/HashSet;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashSet;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 87
    iget-object v2, p0, Lcom/google/android/gms/common/data/y;->c:Ljava/util/ArrayList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 90
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    if-ltz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->b:Ljava/util/HashSet;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/common/data/y;->g()V

    .line 57
    :cond_0
    return-void
.end method

.method public final b(I)I
    .locals 3

    .prologue
    .line 37
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/y;->c()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 38
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Position "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is out of bounds for this buffer"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final c()I
    .locals 2

    .prologue
    .line 32
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->a_:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/data/y;->b:Ljava/util/HashSet;

    invoke-virtual {v1}, Ljava/util/HashSet;->size()I

    move-result v1

    sub-int/2addr v0, v1

    return v0
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/common/data/y;->b:Ljava/util/HashSet;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 69
    invoke-direct {p0}, Lcom/google/android/gms/common/data/y;->g()V

    .line 70
    return-void
.end method
