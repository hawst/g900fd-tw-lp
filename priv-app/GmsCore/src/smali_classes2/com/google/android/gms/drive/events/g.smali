.class public abstract Lcom/google/android/gms/drive/events/g;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/drive/events/i;

.field b:Z

.field c:I

.field private final d:Ljava/lang/String;

.field private e:Ljava/util/concurrent/CountDownLatch;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 184
    const-string v0, "DriveEventService"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/events/g;-><init>(Ljava/lang/String;)V

    .line 185
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 173
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 80
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/g;->b:Z

    .line 84
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/drive/events/g;->c:I

    .line 174
    iput-object p1, p0, Lcom/google/android/gms/drive/events/g;->d:Ljava/lang/String;

    .line 175
    return-void
.end method

.method public static a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 163
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.drive.events.HANDLE_EVENT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 164
    invoke-virtual {v0, p0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 165
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/events/g;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 59
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v2

    iget v1, p0, Lcom/google/android/gms/drive/events/g;->c:I

    if-eq v2, v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/g;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v3, "com.google.android.gms"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v3, "com.google.android.gms"

    invoke-virtual {p0}, Lcom/google/android/gms/drive/events/g;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    if-eqz v4, :cond_0

    array-length v5, v4

    move v1, v0

    :goto_0
    if-ge v1, v5, :cond_0

    aget-object v6, v4, v1

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_3

    iput v2, p0, Lcom/google/android/gms/drive/events/g;->c:I

    :cond_1
    return-void

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    :cond_3
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Caller is not GooglePlayServices"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/drive/events/g;Lcom/google/android/gms/drive/internal/OnEventResponse;)V
    .locals 6

    .prologue
    .line 59
    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnEventResponse;->a()Lcom/google/android/gms/drive/events/DriveEvent;

    move-result-object v2

    const-string v1, "DriveEventService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "handleEventMessage: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    :try_start_0
    invoke-interface {v2}, Lcom/google/android/gms/drive/events/DriveEvent;->a()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    iget-object v1, p0, Lcom/google/android/gms/drive/events/g;->d:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unhandled event: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/drive/internal/be;->b(Ljava/lang/String;Ljava/lang/String;)V

    :goto_0
    return-void

    :pswitch_0
    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/drive/events/ChangeEvent;

    move-object v1, v0

    iget-object v3, p0, Lcom/google/android/gms/drive/events/g;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Unhandled change event: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/gms/drive/internal/be;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v1

    iget-object v3, p0, Lcom/google/android/gms/drive/events/g;->d:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Error handling event: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v1, v2}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/Throwable;Ljava/lang/String;)V

    goto :goto_0

    :pswitch_1
    :try_start_1
    move-object v0, v2

    check-cast v0, Lcom/google/android/gms/drive/events/CompletionEvent;

    move-object v1, v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/drive/events/g;->a(Lcom/google/android/gms/drive/events/CompletionEvent;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/drive/events/g;)Ljava/util/concurrent/CountDownLatch;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->e:Ljava/util/concurrent/CountDownLatch;

    return-object v0
.end method


# virtual methods
.method public a(Lcom/google/android/gms/drive/events/CompletionEvent;)V
    .locals 3

    .prologue
    .line 272
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->d:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled completion event: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    return-void
.end method

.method public final declared-synchronized onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 4

    .prologue
    .line 189
    monitor-enter p0

    :try_start_0
    const-string v0, "com.google.android.gms.drive.events.HANDLE_EVENT"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/drive/events/g;->b:Z

    if-nez v0, :cond_0

    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/drive/events/g;->b:Z

    .line 195
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    .line 196
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/drive/events/g;->e:Ljava/util/concurrent/CountDownLatch;

    .line 197
    new-instance v1, Lcom/google/android/gms/drive/events/h;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/drive/events/h;-><init>(Lcom/google/android/gms/drive/events/g;Ljava/util/concurrent/CountDownLatch;)V

    invoke-virtual {v1}, Lcom/google/android/gms/drive/events/h;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 224
    const-wide/16 v2, 0x1388

    :try_start_1
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 226
    if-nez v0, :cond_0

    .line 227
    const-string v0, "DriveEventService"

    const-string v1, "Failed to synchronously initialize event handler."

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->c(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 233
    :cond_0
    :try_start_2
    new-instance v0, Lcom/google/android/gms/drive/events/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/drive/events/j;-><init>(Lcom/google/android/gms/drive/events/g;)V

    invoke-virtual {v0}, Lcom/google/android/gms/drive/events/j;->asBinder()Landroid/os/IBinder;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 235
    :goto_0
    monitor-exit p0

    return-object v0

    .line 229
    :catch_0
    move-exception v0

    .line 230
    :try_start_3
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unable to start event handler"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 189
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 235
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized onDestroy()V
    .locals 4

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    const-string v0, "DriveEventService"

    const-string v1, "onDestroy"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    if-eqz v0, :cond_1

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    invoke-static {v0}, Lcom/google/android/gms/drive/events/i;->a(Lcom/google/android/gms/drive/events/i;)Landroid/os/Message;

    move-result-object v0

    .line 250
    iget-object v1, p0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/events/i;->sendMessage(Landroid/os/Message;)Z

    .line 251
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/events/g;->a:Lcom/google/android/gms/drive/events/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 254
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/drive/events/g;->e:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x1388

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 256
    if-nez v0, :cond_0

    .line 257
    const-string v0, "DriveEventService"

    const-string v1, "Failed to synchronously quit event handler. Will quit itself"

    invoke-static {v0, v1}, Lcom/google/android/gms/drive/internal/be;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 260
    :cond_0
    :goto_0
    const/4 v0, 0x0

    :try_start_2
    iput-object v0, p0, Lcom/google/android/gms/drive/events/g;->e:Ljava/util/concurrent/CountDownLatch;

    .line 262
    :cond_1
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 263
    monitor-exit p0

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 241
    const/4 v0, 0x1

    return v0
.end method
