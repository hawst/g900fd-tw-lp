.class public abstract Lcom/google/android/gms/car/ov;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    return-void
.end method

.method public static a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 102
    invoke-static {}, Landroid/opengl/GLES20;->glGetError()I

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    new-instance v1, Lcom/google/android/gms/car/ow;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": glError "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/ow;-><init>(Ljava/lang/String;)V

    throw v1

    .line 106
    :cond_0
    return-void
.end method

.method public static d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    packed-switch p0, :pswitch_data_0

    .line 96
    const-string v0, "Unknown"

    :goto_0
    return-object v0

    .line 84
    :pswitch_0
    const-string v0, "RAIL_WINDOW_ORDER"

    goto :goto_0

    .line 86
    :pswitch_1
    const-string v0, "CONTENT_WINDOW_ORDER"

    goto :goto_0

    .line 88
    :pswitch_2
    const-string v0, "NOTIFICATIONS_WINDOW_ORDER"

    goto :goto_0

    .line 90
    :pswitch_3
    const-string v0, "FULLSCREEN_WINDOW_ORDER"

    goto :goto_0

    .line 92
    :pswitch_4
    const-string v0, "IME_WINDOW_ORDER"

    goto :goto_0

    .line 94
    :pswitch_5
    const-string v0, "GSA_WINDOW_ORDER"

    goto :goto_0

    .line 82
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_5
    .end packed-switch
.end method


# virtual methods
.method abstract a(JI)V
.end method

.method abstract h()V
.end method
