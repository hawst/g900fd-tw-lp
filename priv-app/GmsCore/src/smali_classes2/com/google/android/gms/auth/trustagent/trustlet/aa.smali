.class public final Lcom/google/android/gms/auth/trustagent/trustlet/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/auth/trustagent/trustlet/ad;

.field final c:Lcom/google/android/gms/common/api/v;

.field final d:Landroid/content/IntentFilter;

.field final e:Lcom/google/android/gms/auth/trustagent/trustlet/ae;

.field f:I

.field g:Lcom/google/android/gms/common/api/x;

.field h:Lcom/google/android/gms/common/api/y;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/ad;Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a:Landroid/content/Context;

    .line 76
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ad;

    .line 77
    iput-object p3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    .line 78
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.auth.trustagent.trustlet.ACTION_PERSONAL_NEARBY_ALERT"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->d:Landroid/content/IntentFilter;

    .line 79
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ae;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ae;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/aa;B)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->e:Lcom/google/android/gms/auth/trustagent/trustlet/ae;

    .line 80
    return-void
.end method


# virtual methods
.method a()Landroid/app/PendingIntent;
    .locals 4

    .prologue
    .line 116
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.auth.trustagent.trustlet.ACTION_PERSONAL_NEARBY_ALERT"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 118
    new-instance v1, Ljava/util/Random;

    invoke-direct {v1}, Ljava/util/Random;-><init>()V

    invoke-virtual {v1}, Ljava/util/Random;->nextInt()I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->f:I

    .line 119
    const-string v1, "INTENT_CODE_ID"

    iget v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->f:I

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 120
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    const/high16 v3, 0x8000000

    invoke-static {v1, v2, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public final a([Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 99
    array-length v0, p1

    if-nez v0, :cond_0

    .line 105
    :goto_0
    return-void

    .line 103
    :cond_0
    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    invoke-static {}, Lcom/google/android/gms/location/places/PlaceFilter;->a()Lcom/google/android/gms/location/places/g;

    move-result-object v2

    iput-object p1, v2, Lcom/google/android/gms/location/places/g;->b:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/location/places/g;->a()Lcom/google/android/gms/location/places/PlaceFilter;

    move-result-object v2

    const/4 v3, 0x3

    invoke-static {v3, v2}, Lcom/google/android/gms/location/places/NearbyAlertRequest;->a(ILcom/google/android/gms/location/places/PlaceFilter;)Lcom/google/android/gms/location/places/NearbyAlertRequest;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/NearbyAlertRequest;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;

    .line 104
    const-string v0, "Coffee - NearbyAlertTracker"

    const-string v1, "requestNearbyAlerts()"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
