.class public final Lcom/google/android/gms/search/queries/t;
.super Lcom/google/android/gms/icing/b/h;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/icing/impl/u;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;)V
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/b/h;-><init>(I)V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    .line 34
    return-void
.end method

.method public static a(Z)Lcom/google/android/gms/gcm/OneoffTask;
    .locals 6

    .prologue
    .line 107
    new-instance v0, Lcom/google/android/gms/gcm/an;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v1, Lcom/google/android/gms/icing/service/IcingGcmTaskService;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const-string v1, "update-app-params"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/gms/gcm/an;->a(Z)Lcom/google/android/gms/gcm/an;

    move-result-object v1

    sget-object v0, Lcom/google/android/gms/icing/a/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    sget-object v0, Lcom/google/android/gms/icing/a/a;->C:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    return-object v0
.end method

.method private c()Ljava/lang/Void;
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->G()Lcom/google/android/gms/icing/impl/k;

    move-result-object v0

    .line 51
    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v3

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/g;->a()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/icing/c/a/f;

    invoke-direct {v1}, Lcom/google/android/gms/icing/c/a/f;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/c/a/f;)V

    .line 103
    :goto_0
    return-object v7

    .line 58
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    .line 60
    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 61
    const/4 v2, 0x0

    const/4 v4, 0x1

    :try_start_0
    invoke-interface {v0, v2, v4}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;

    move-result-object v0

    .line 63
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    new-instance v1, Ljava/util/HashMap;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v1, v2}, Ljava/util/HashMap;-><init>(I)V

    .line 66
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 67
    iget-object v4, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-interface {v1, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 69
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 70
    iget-object v4, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v5, v0}, Lcom/google/android/gms/icing/impl/q;->r(Ljava/lang/String;)Landroid/support/v4/g/o;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    .line 63
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 77
    :cond_2
    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v0

    new-array v4, v0, [Lcom/google/android/gms/icing/c/a/m;

    .line 78
    const/4 v0, 0x0

    .line 79
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v0

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 80
    new-instance v6, Lcom/google/android/gms/icing/c/a/m;

    invoke-direct {v6}, Lcom/google/android/gms/icing/c/a/m;-><init>()V

    .line 81
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    iput-object v1, v6, Lcom/google/android/gms/icing/c/a/m;->a:Ljava/lang/String;

    .line 82
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/g/o;

    iget-object v1, v1, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    iput v1, v6, Lcom/google/android/gms/icing/c/a/m;->b:I

    .line 83
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/o;

    iget-object v0, v0, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    iput-object v0, v6, Lcom/google/android/gms/icing/c/a/m;->c:Ljava/lang/String;

    .line 84
    add-int/lit8 v0, v2, 0x1

    aput-object v6, v4, v2

    move v2, v0

    .line 85
    goto :goto_2

    .line 87
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->I()Lcom/google/android/gms/icing/c/b;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->A()I

    move-result v1

    invoke-interface {v0, v3, v1, v4}, Lcom/google/android/gms/icing/c/b;->a(Ljava/lang/String;I[Lcom/google/android/gms/icing/c/a/m;)Lcom/google/android/gms/icing/c/a/f;

    move-result-object v0

    .line 93
    if-nez v0, :cond_4

    .line 94
    const-string v0, "Getting app params failed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v0

    .line 99
    invoke-static {v8}, Lcom/google/android/gms/search/queries/t;->a(Z)Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    goto/16 :goto_0

    .line 101
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/search/queries/t;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/c/a/f;)V

    goto/16 :goto_0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/search/queries/t;->c()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method
