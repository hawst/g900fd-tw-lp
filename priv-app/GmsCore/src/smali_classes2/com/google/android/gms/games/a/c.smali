.class final Lcom/google/android/gms/games/a/c;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ce;


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final h:Lcom/google/android/gms/games/provider/a;


# instance fields
.field private final b:Ljava/util/Random;

.field private final c:Lcom/google/android/gms/games/h/a/m;

.field private final d:Lcom/google/android/gms/games/h/a/b;

.field private final e:Lcom/google/android/gms/games/h/a/n;

.field private final f:Lcom/google/android/gms/games/h/a/c;

.field private final g:Ljava/util/HashMap;

.field private final i:Lcom/google/android/gms/games/b/a;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 89
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/c;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 189
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "external_achievement_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "unlocked_icon_image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "revealed_icon_image_uri"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "total_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "formatted_total_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "definition_xp_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "current_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "formatted_current_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "last_updated_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "definition_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    const-string v1, "instance_xp_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/c;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->e:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/c;->h:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    .line 213
    const-string v0, "AchievementAgent"

    sget-object v1, Lcom/google/android/gms/games/a/c;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 214
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->b:Ljava/util/Random;

    .line 215
    new-instance v0, Lcom/google/android/gms/games/h/a/m;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/m;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    .line 216
    new-instance v0, Lcom/google/android/gms/games/h/a/b;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/b;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->d:Lcom/google/android/gms/games/h/a/b;

    .line 217
    new-instance v0, Lcom/google/android/gms/games/h/a/n;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/n;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->e:Lcom/google/android/gms/games/h/a/n;

    .line 218
    new-instance v0, Lcom/google/android/gms/games/h/a/c;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/c;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->f:Lcom/google/android/gms/games/h/a/c;

    .line 219
    new-instance v0, Lcom/google/android/gms/games/b/a;

    sget-object v1, Lcom/google/android/gms/games/a/c;->h:Lcom/google/android/gms/games/provider/a;

    iget-object v1, v1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/b/a;-><init>([Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    .line 220
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/c;->g:Ljava/util/HashMap;

    .line 221
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 423
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "definition_xp_value"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->e(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J
    .locals 14

    .prologue
    .line 851
    const-wide/16 v12, -0x1

    .line 852
    const/4 v10, -0x1

    .line 853
    const/4 v9, -0x1

    .line 854
    const/4 v8, -0x1

    .line 855
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/p;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    .line 856
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v5, Lcom/google/android/gms/games/a/i;->a:[Ljava/lang/String;

    const-string v6, "external_achievement_id=?"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/String;

    const/4 v11, 0x0

    aput-object p1, v7, v11

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/database/AbstractWindowedCursor;

    move-result-object v11

    .line 862
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToLast()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 863
    const/4 v2, 0x0

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 864
    const/4 v2, 0x2

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    .line 865
    const/4 v2, 0x3

    invoke-interface {v11, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_11

    .line 866
    const/4 v2, 0x3

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 868
    :goto_0
    const/4 v2, 0x4

    invoke-interface {v11, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_10

    .line 869
    const/4 v2, 0x4

    invoke-interface {v11, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 873
    :goto_1
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 875
    const/4 v8, -0x1

    if-eq v3, v8, :cond_0

    const/4 v8, -0x1

    if-ne v2, v8, :cond_3

    :cond_0
    const/4 v8, 0x1

    :goto_2
    const-string v9, "Both increment and set steps may not be positive"

    invoke-static {v8, v9}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 880
    const/4 v8, 0x0

    .line 881
    const-wide/16 v10, -0x1

    cmp-long v9, v6, v10

    if-eqz v9, :cond_1

    .line 882
    const/4 v9, 0x1

    move/from16 v0, p2

    if-eq v0, v9, :cond_4

    .line 883
    const/4 v8, 0x1

    .line 892
    :cond_1
    :goto_3
    iget-boolean v9, p0, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v9, :cond_6

    const/4 v9, 0x1

    :goto_4
    const-string v10, "Pending ops are always 3P"

    invoke-static {v9, v10}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 893
    iget-object v9, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    if-eqz v9, :cond_7

    iget-object v9, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x1

    :goto_5
    const-string v10, "Owning and target game IDs should be the same"

    invoke-static {v9, v10}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 895
    iget-object v9, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    if-nez v9, :cond_8

    const/4 v9, 0x1

    :goto_6
    const-string v10, "Target player ID should be null"

    invoke-static {v9, v10}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 899
    new-instance v9, Landroid/content/ContentValues;

    invoke-direct {v9}, Landroid/content/ContentValues;-><init>()V

    .line 900
    const-string v10, "client_context_id"

    iget-object v11, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v12, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v11, v12}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v12

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 903
    const-string v10, "external_achievement_id"

    invoke-virtual {v9, v10, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 904
    const-string v10, "achievement_type"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 905
    const-string v10, "external_game_id"

    iget-object v11, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 906
    const-string v10, "external_player_id"

    iget-object v11, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v9, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 908
    if-eqz v8, :cond_d

    .line 910
    const/4 v8, 0x1

    move/from16 v0, p2

    if-eq v0, v8, :cond_9

    .line 912
    const/4 v2, 0x1

    if-ne v5, v2, :cond_2

    if-nez p3, :cond_2

    .line 913
    const-string v2, "new_state"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 924
    :cond_2
    :goto_7
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    .line 925
    invoke-static {v4, v6, v7}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v3, v9, v4, v5}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_c

    .line 939
    :goto_8
    return-wide v6

    .line 873
    :catchall_0
    move-exception v2

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v2

    .line 875
    :cond_3
    const/4 v8, 0x0

    goto/16 :goto_2

    .line 884
    :cond_4
    if-lez p5, :cond_5

    const/4 v9, -0x1

    if-ne v3, v9, :cond_5

    .line 885
    const/4 v8, 0x1

    goto/16 :goto_3

    .line 886
    :cond_5
    if-lez p4, :cond_1

    const/4 v9, -0x1

    if-ne v2, v9, :cond_1

    .line 887
    const/4 v8, 0x1

    goto/16 :goto_3

    .line 892
    :cond_6
    const/4 v9, 0x0

    goto/16 :goto_4

    .line 893
    :cond_7
    const/4 v9, 0x0

    goto/16 :goto_5

    .line 895
    :cond_8
    const/4 v9, 0x0

    goto :goto_6

    .line 915
    :cond_9
    if-lez v3, :cond_a

    .line 916
    const-string v2, "steps_to_increment"

    add-int v3, v3, p4

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_7

    .line 917
    :cond_a
    if-lez v2, :cond_b

    .line 918
    const-string v3, "min_steps_to_set"

    move/from16 v0, p5

    invoke-static {v0, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v9, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_7

    .line 920
    :cond_b
    const-string v2, "AchievementAgent"

    const-string v3, "Can\'t coalesce an incremental achievement op with no steps!"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 921
    const-wide/16 v6, -0x1

    goto :goto_8

    .line 928
    :cond_c
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to update existing pending op with ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 929
    const-wide/16 v6, -0x1

    goto :goto_8

    .line 932
    :cond_d
    const/4 v2, 0x1

    move/from16 v0, p2

    if-eq v0, v2, :cond_e

    .line 933
    const-string v2, "new_state"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 939
    :goto_9
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v4, v9}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v6

    goto :goto_8

    .line 934
    :cond_e
    if-lez p5, :cond_f

    .line 935
    const-string v2, "min_steps_to_set"

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_9

    .line 937
    :cond_f
    const-string v2, "steps_to_increment"

    invoke-static/range {p4 .. p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v9, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_9

    :cond_10
    move v2, v8

    goto/16 :goto_1

    :cond_11
    move v3, v9

    goto/16 :goto_0

    :cond_12
    move v2, v8

    move v3, v9

    move v5, v10

    move-wide v6, v12

    goto/16 :goto_1
.end method

.method private a(Lcom/google/android/gms/games/a/f;ZZ)J
    .locals 11

    .prologue
    const/16 v10, 0xbb9

    const/4 v5, 0x1

    const/4 v1, -0x1

    const/4 v4, 0x0

    const-wide/16 v6, -0x1

    .line 1509
    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    .line 1511
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v0

    .line 1513
    if-nez v0, :cond_0

    .line 1514
    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown achievement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1515
    iput v10, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1619
    :goto_0
    return-wide v0

    .line 1520
    :cond_0
    iget-object v0, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-eq v0, v5, :cond_1

    .line 1521
    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Achievement "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not incremental."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1522
    const/16 v0, 0xbba

    iput v0, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1523
    goto :goto_0

    .line 1527
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-direct {p0, v2, v0}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v8

    .line 1528
    cmp-long v0, v8, v6

    if-nez v0, :cond_2

    .line 1529
    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find instance for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1530
    iput v10, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1531
    goto :goto_0

    .line 1535
    :cond_2
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->b:Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/games/a/f;->h:Landroid/os/Bundle;

    .line 1538
    iget-object v0, v2, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p1, Lcom/google/android/gms/games/a/f;->g:Landroid/net/Uri;

    .line 1540
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->g:Landroid/net/Uri;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/games/a/h;->a:[Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v8

    .line 1549
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 1550
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 1551
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 1552
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 1555
    :goto_1
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 1559
    if-nez v0, :cond_4

    .line 1561
    sget-object v0, Lcom/google/android/gms/games/c/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1562
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    iget-object v1, p1, Lcom/google/android/gms/games/a/f;->d:Lcom/google/android/gms/games/internal/el;

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->h:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 1565
    :cond_3
    iput v4, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1566
    goto/16 :goto_0

    .line 1555
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1571
    :cond_4
    if-nez p2, :cond_5

    iget v0, p1, Lcom/google/android/gms/games/a/f;->c:I

    if-lt v2, v0, :cond_5

    .line 1572
    iput v4, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1573
    goto/16 :goto_0

    .line 1577
    :cond_5
    if-eq v3, v1, :cond_6

    if-ne v2, v1, :cond_7

    .line 1578
    :cond_6
    const-string v0, "AchievementAgent"

    const-string v1, "Unable to find state record for incremental achievement"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1579
    iput v10, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1580
    goto/16 :goto_0

    .line 1584
    :cond_7
    iget v1, p1, Lcom/google/android/gms/games/a/f;->c:I

    if-eqz p2, :cond_9

    move v0, v2

    :goto_2
    add-int/2addr v0, v1

    invoke-static {v3, v0}, Ljava/lang/Math;->min(II)I

    move-result v0

    iput v0, p1, Lcom/google/android/gms/games/a/f;->e:I

    .line 1586
    if-eqz p2, :cond_8

    iget v0, p1, Lcom/google/android/gms/games/a/f;->e:I

    if-gez v0, :cond_8

    .line 1588
    iput v3, p1, Lcom/google/android/gms/games/a/f;->e:I

    .line 1591
    :cond_8
    if-ge v2, v3, :cond_a

    iget v0, p1, Lcom/google/android/gms/games/a/f;->e:I

    if-ne v0, v3, :cond_a

    move v0, v5

    .line 1593
    :goto_3
    iget-object v1, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->g:Landroid/net/Uri;

    iget v3, p1, Lcom/google/android/gms/games/a/f;->e:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    const/4 v8, 0x0

    invoke-static {v1, v2, v3, v0, v8}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_b

    .line 1595
    const-string v0, "AchievementAgent"

    const-string v1, "Unable to update incremental achievement record."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1596
    iput v5, p1, Lcom/google/android/gms/games/a/f;->f:I

    move-wide v0, v6

    .line 1597
    goto/16 :goto_0

    :cond_9
    move v0, v4

    .line 1584
    goto :goto_2

    :cond_a
    move v0, v4

    .line 1591
    goto :goto_3

    .line 1600
    :cond_b
    const-wide/16 v2, 0x0

    .line 1604
    if-eqz v0, :cond_d

    .line 1606
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->h:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/achievement/Achievement;

    .line 1608
    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v0

    .line 1609
    iput-boolean v5, p1, Lcom/google/android/gms/games/a/f;->i:Z

    .line 1610
    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    iget-object v3, p1, Lcom/google/android/gms/games/a/f;->d:Lcom/google/android/gms/games/internal/el;

    iget-object v5, p1, Lcom/google/android/gms/games/a/f;->h:Landroid/os/Bundle;

    invoke-static {v2, v3, v5}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 1614
    :goto_4
    if-eqz p3, :cond_c

    .line 1615
    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v2

    iput v2, p1, Lcom/google/android/gms/games/a/f;->f:I

    goto/16 :goto_0

    .line 1617
    :cond_c
    iput v4, p1, Lcom/google/android/gms/games/a/f;->f:I

    goto/16 :goto_0

    :cond_d
    move-wide v0, v2

    goto :goto_4

    :cond_e
    move v0, v1

    move v2, v1

    move v3, v1

    goto/16 :goto_1
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;
    .locals 16

    .prologue
    .line 491
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/c;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 494
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/games/a/c;->g(Lcom/google/android/gms/games/a/au;)I

    .line 495
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/c;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 496
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Couldn\'t find local achievement to update for achievement ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 502
    :cond_0
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v2

    .line 503
    if-nez v2, :cond_1

    .line 504
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/16 v3, 0xbb9

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    .line 634
    :goto_0
    return-object v2

    .line 509
    :cond_1
    if-nez p3, :cond_2

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-eqz v2, :cond_2

    .line 510
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Achievement "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is incremental, and cannot be unlocked."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 512
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/16 v3, 0xbb8

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto :goto_0

    .line 516
    :cond_2
    invoke-direct/range {p0 .. p2}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v10

    .line 517
    const-wide/16 v2, -0x1

    cmp-long v2, v10, v2

    if-nez v2, :cond_3

    .line 518
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find instance for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 519
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/16 v3, 0xbb9

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto :goto_0

    .line 521
    :cond_3
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v10, v11}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v12

    .line 525
    const/4 v2, -0x1

    .line 526
    new-instance v3, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v3, v12}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v3

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    const-string v6, "state"

    aput-object v6, v4, v5

    iput-object v4, v3, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v4

    .line 531
    :try_start_0
    invoke-interface {v4}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-eqz v3, :cond_e

    .line 532
    const/4 v2, 0x0

    invoke-interface {v4, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    move v3, v2

    .line 535
    :goto_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 539
    const/4 v2, -0x1

    if-ne v3, v2, :cond_4

    .line 540
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "No prior achievement state set for instance ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 541
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/16 v3, 0xbb9

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    .line 535
    :catchall_0
    move-exception v2

    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    throw v2

    .line 546
    :cond_4
    sget-object v2, Lcom/google/android/gms/games/c/a;->q:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 547
    if-nez p3, :cond_5

    move/from16 v0, p3

    if-ne v0, v3, :cond_5

    .line 548
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 551
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    .line 555
    :cond_5
    const-wide/16 v4, 0x0

    .line 558
    move/from16 v0, p3

    if-eq v0, v3, :cond_d

    if-eqz v3, :cond_d

    .line 559
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v13

    .line 560
    new-instance v14, Landroid/content/ContentValues;

    invoke-direct {v14}, Landroid/content/ContentValues;-><init>()V

    .line 561
    const-string v2, "state"

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 562
    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 563
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13, v12, v14, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 564
    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    .line 565
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/c;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v15

    .line 567
    if-nez p3, :cond_c

    .line 568
    const-string v2, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-virtual {v15, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/achievement/Achievement;

    .line 570
    invoke-interface {v2}, Lcom/google/android/gms/games/achievement/Achievement;->r()J

    move-result-wide v8

    .line 576
    :goto_2
    if-nez p3, :cond_7

    .line 578
    :try_start_1
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1, v15}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 580
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v4, "achievements/%1$s/unlock"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/games/h/a/h;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1

    .line 627
    :cond_6
    :goto_3
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;)V

    move-wide v2, v8

    .line 634
    :goto_4
    new-instance v4, Lcom/google/android/gms/games/a/g;

    const/4 v5, 0x0

    invoke-direct {v4, v5, v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    move-object v2, v4

    goto/16 :goto_0

    .line 582
    :cond_7
    const/4 v2, 0x1

    move/from16 v0, p3

    if-ne v0, v2, :cond_6

    .line 583
    :try_start_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v4, "achievements/%1$s/reveal"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iget-object v2, v2, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/games/h/a/f;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/f;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/f;->b()Ljava/lang/String;

    move-result-object v2

    .line 586
    invoke-static {v2}, Lcom/google/android/gms/games/internal/b/a;->a(Ljava/lang/String;)I

    move-result v2

    .line 587
    if-nez v2, :cond_6

    .line 590
    const-string v3, "state"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v14, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 591
    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v14, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 593
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v13, v12, v14, v2, v3}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_8

    .line 594
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to update local instance for ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    :cond_8
    move-object/from16 v0, p1

    move-object/from16 v1, p4

    invoke-static {v0, v1, v15}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V
    :try_end_2
    .catch Lcom/android/volley/ac; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_3

    .line 601
    :catch_0
    move-exception v2

    .line 602
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 603
    const-string v3, "AchievementAgent"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 605
    :cond_9
    invoke-static {v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 606
    const-string v2, "AchievementAgent"

    const-string v3, "Encountered hard error while incrementing achievement."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    .line 612
    :cond_a
    const-string v2, "AchievementAgent"

    const-string v3, "Unable to update achievement. Update will be deferred."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 613
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v5, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 615
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v8, v9}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    goto/16 :goto_0

    .line 618
    :catch_1
    move-exception v2

    move-object v8, v2

    .line 620
    const-string v2, "AchievementAgent"

    const-string v3, "Auth error while updating achievement over network"

    invoke-static {v2, v3, v8}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 621
    const/4 v4, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move/from16 v5, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 623
    throw v8

    .line 629
    :cond_b
    const-string v2, "AchievementAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to update local achievement instance for ID "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    :cond_c
    move-wide v8, v4

    goto/16 :goto_2

    :cond_d
    move-wide v2, v4

    goto/16 :goto_4

    :cond_e
    move v3, v2

    goto/16 :goto_1
.end method

.method private static a(Landroid/content/ContentValues;Lcom/google/android/gms/games/provider/a;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1414
    invoke-virtual {p2, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v2

    .line 1415
    iget-object v3, p1, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    move v0, v1

    .line 1416
    :goto_0
    array-length v4, v3

    if-ge v0, v4, :cond_1

    .line 1417
    aget-object v4, v3, v0

    invoke-virtual {p2, v4}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1418
    sget-object v4, Lcom/google/android/gms/games/a/d;->a:[I

    aget-object v5, v3, v0

    invoke-virtual {p1, v5}, Lcom/google/android/gms/games/provider/a;->a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/provider/d;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 1416
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1422
    :pswitch_0
    aget-object v4, v3, v0

    aget-object v5, v3, v0

    invoke-virtual {p2, v5, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 1425
    :pswitch_1
    aget-object v4, v3, v0

    aget-object v5, v3, v0

    invoke-virtual {p2, v5, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;II)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 1428
    :pswitch_2
    aget-object v4, v3, v0

    aget-object v5, v3, v0

    invoke-virtual {p2, v5, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p0, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1432
    :cond_1
    return-void

    .line 1418
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 1486
    const-string v0, "AchievementAgent"

    invoke-static {p0, v0, p1, p2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1487
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/f;IZ)V
    .locals 4

    .prologue
    .line 1635
    iget v0, p1, Lcom/google/android/gms/games/a/f;->e:I

    if-le p2, v0, :cond_0

    .line 1636
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    iget-object v0, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/f;->g:Landroid/net/Uri;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, p3, v3}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z

    .line 1638
    if-eqz p3, :cond_0

    .line 1639
    const/16 v0, 0xbbb

    iput v0, p1, Lcom/google/android/gms/games/a/f;->f:I

    .line 1640
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/f;->i:Z

    if-nez v0, :cond_0

    .line 1641
    iget-object v0, p1, Lcom/google/android/gms/games/a/f;->a:Lcom/google/android/gms/games/a/au;

    iget-object v1, p1, Lcom/google/android/gms/games/a/f;->d:Lcom/google/android/gms/games/internal/el;

    iget-object v2, p1, Lcom/google/android/gms/games/a/f;->h:Landroid/os/Bundle;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/c/a;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 1646
    :cond_0
    return-void
.end method

.method private static a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v0, 0x0

    .line 430
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 431
    if-eqz p2, :cond_0

    .line 432
    const-string v2, "current_steps"

    invoke-virtual {v1, v2, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 433
    const-string v2, "formatted_current_steps"

    invoke-static {}, Ljava/text/NumberFormat;->getInstance()Ljava/text/NumberFormat;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/text/NumberFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    :cond_0
    const-string v2, "last_updated_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 437
    if-eqz p3, :cond_1

    .line 438
    const-string v2, "state"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 440
    :cond_1
    if-eqz p4, :cond_2

    .line 441
    const-string v2, "state"

    invoke-static {p4}, Lcom/google/android/gms/games/internal/b/a;->a(Ljava/lang/String;)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 445
    :cond_2
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, p1, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-eqz v1, :cond_3

    const/4 v0, 0x1

    :cond_3
    return v0
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/e;)Z
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 750
    .line 753
    :try_start_0
    const-string v0, "AchievementAgent"

    const-string v1, "Sending achievement batch..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    iget-object v4, p3, Lcom/google/android/gms/games/a/e;->d:Lcom/google/android/gms/games/h/a/i;

    const-string v3, "achievements/updateMultiple"

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const-class v5, Lcom/google/android/gms/games/h/a/j;

    move-object v1, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/j;

    .line 759
    iget-object v1, p0, Lcom/google/android/gms/games/a/c;->g:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 762
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/j;->getUpdatedAchievements()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/l;

    .line 763
    const-string v1, "AchievementAgent"

    const-string v3, "Achievement batch response [ID=%s, state=%s]"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->b()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    const/4 v5, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->c()Ljava/lang/String;

    move-result-object v8

    aput-object v8, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 765
    iget-object v1, p3, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p3, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_3

    move v1, v6

    :goto_1
    if-eqz v1, :cond_0

    .line 766
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->b()Ljava/lang/String;

    move-result-object v1

    iget-object v3, p3, Lcom/google/android/gms/games/a/e;->e:Lcom/google/android/gms/games/a/c;

    iget-object v4, p3, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    invoke-direct {v3, v4, v1}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v4

    const-wide/16 v8, -0x1

    cmp-long v3, v4, v8

    if-nez v3, :cond_4

    const-string v3, "AchievementAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not find instance for "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v1, 0x0

    .line 767
    :goto_2
    if-nez v1, :cond_1

    .line 768
    const-string v3, "AchievementAgent"

    const-string v4, "Failed to find instance data for achievement"

    invoke-static {v3, v4}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 771
    :cond_1
    iget-object v3, p3, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    iget-object v3, v3, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->d()Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->e()Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/l;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v1, v4, v5, v0}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/Integer;ZLjava/lang/String;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 776
    :catch_0
    move-exception v0

    .line 777
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 778
    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 780
    :cond_2
    invoke-static {v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 781
    const-string v0, "AchievementAgent"

    const-string v1, "Could not submit pending operations, will try again later"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v7

    .line 794
    :goto_3
    return v0

    :cond_3
    move v1, v7

    .line 765
    goto :goto_1

    .line 766
    :cond_4
    :try_start_1
    iget-object v1, p3, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    iget-object v1, v1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1, v4, v5}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_1

    move-result-object v1

    goto :goto_2

    :cond_5
    move v0, v6

    .line 792
    goto :goto_3

    .line 784
    :cond_6
    const-string v0, "AchievementAgent"

    const-string v1, "Encountered hard error while submitting pending operations."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 792
    goto :goto_3

    .line 787
    :catch_1
    move-exception v0

    .line 790
    const-string v1, "Failed to flush operation"

    invoke-static {p1, v1, v0}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v0, v6

    .line 791
    goto :goto_3
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Z
    .locals 20

    .prologue
    .line 1321
    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1322
    :cond_0
    const/4 v2, 0x1

    .line 1410
    :goto_0
    return v2

    .line 1326
    :cond_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    .line 1329
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v12

    .line 1330
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/gms/games/a/au;->i:Z

    if-nez v2, :cond_2

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->b()Z

    move-result v2

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v6, "Missing local player ID for "

    invoke-direct {v3, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    move-object/from16 v0, p1

    iget-wide v2, v0, Lcom/google/android/gms/games/a/au;->j:J

    move-wide v6, v2

    .line 1331
    :goto_1
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v2

    .line 1332
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 1333
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3, v2}, Lcom/google/android/gms/games/provider/aq;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 1335
    new-instance v9, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v9, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v9, v3}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v3

    .line 1339
    :try_start_0
    sget-object v9, Lcom/google/android/gms/games/a/c;->h:Lcom/google/android/gms/games/provider/a;

    invoke-static {v8, v9, v3}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/ContentValues;Lcom/google/android/gms/games/provider/a;Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1341
    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1348
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v9, v4, v5}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    const-string v5, "external_achievement_id"

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v4

    .line 1352
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v5

    const-string v9, "external_achievement_id"

    invoke-static {v3, v5, v9}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v9

    .line 1360
    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    .line 1361
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 1363
    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    .line 1364
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v11

    .line 1365
    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v14

    move v3, v2

    :goto_2
    if-ge v3, v14, :cond_6

    .line 1367
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ct;

    .line 1368
    iget-object v15, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1369
    const-string v2, "player_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v16

    move-object/from16 v0, v16

    invoke-virtual {v15, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1370
    const-string v2, "external_achievement_id"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 1372
    const-string v2, "external_achievement_id"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1373
    const-string v2, "external_game_id"

    invoke-virtual {v15, v2}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1374
    move-object/from16 v0, v16

    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1375
    if-nez v2, :cond_3

    .line 1376
    const-string v2, "AchievementAgent"

    new-instance v15, Ljava/lang/StringBuilder;

    const-string v17, "Unable to find local record for external achievement ID "

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v2, v15}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    :goto_3
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_2

    .line 1330
    :cond_2
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v2

    move-wide v6, v2

    goto/16 :goto_1

    .line 1341
    :catchall_0
    move-exception v2

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v2

    .line 1380
    :cond_3
    const-string v17, "definition_id"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1383
    move-object/from16 v0, v16

    invoke-virtual {v9, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    .line 1384
    if-eqz v10, :cond_5

    .line 1385
    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v16

    if-nez v2, :cond_4

    invoke-static {v11}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    :goto_4
    move/from16 v0, v16

    invoke-virtual {v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_4
    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v11, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    goto :goto_4

    .line 1389
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, v16

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1391
    new-instance v16, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v16

    .line 1395
    :try_start_1
    sget-object v2, Lcom/google/android/gms/games/a/c;->h:Lcom/google/android/gms/games/provider/a;

    move-object/from16 v0, v16

    invoke-static {v15, v2, v0}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/ContentValues;Lcom/google/android/gms/games/provider/a;Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 1397
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 1399
    invoke-virtual {v15, v8}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 1400
    invoke-virtual {v5, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 1397
    :catchall_1
    move-exception v2

    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v2

    .line 1404
    :cond_6
    invoke-virtual {v5}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_7

    .line 1406
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/a;->a(Ljava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v4

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v10

    invoke-virtual/range {v3 .. v11}, Lcom/google/android/gms/games/b/a;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V

    .line 1410
    :cond_7
    const-string v2, "AchievementAgent"

    invoke-static {v12, v13, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    goto/16 :goto_0
.end method

.method private static b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 4

    .prologue
    .line 450
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 451
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 452
    const-string v0, "external_achievement_id"

    const-string v2, "=?"

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 454
    new-instance v2, Lcom/google/android/gms/games/achievement/a;

    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/achievement/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 457
    const/4 v0, 0x0

    .line 459
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/achievement/a;->c()I

    move-result v1

    if-lez v1, :cond_0

    .line 460
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/achievement/a;->b(I)Lcom/google/android/gms/games/achievement/Achievement;

    move-result-object v0

    .line 461
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 462
    const-string v3, "com.google.android.gms.games.ACHIEVEMENT"

    invoke-interface {v0}, Lcom/google/android/gms/games/achievement/Achievement;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    invoke-virtual {v1, v3, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-object v0, v1

    .line 466
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/achievement/a;->w_()V

    .line 469
    return-object v0

    .line 466
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/achievement/a;->w_()V

    throw v0
.end method

.method private c(Lcom/google/android/gms/games/a/au;)I
    .locals 20

    .prologue
    .line 660
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/p;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v7

    .line 661
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v8

    .line 662
    const/4 v3, 0x0

    .line 663
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 664
    new-instance v2, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    const-string v4, "account_name=?"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/String;

    const/4 v6, 0x0

    aput-object v8, v5, v6

    invoke-virtual {v2, v7, v4, v5}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/games/a/i;->a:[Ljava/lang/String;

    iput-object v4, v2, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v4, "package_name"

    iput-object v4, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v10

    .line 668
    new-instance v11, Ljava/util/HashMap;

    invoke-direct {v11}, Ljava/util/HashMap;-><init>()V

    .line 670
    :goto_0
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 672
    const/4 v2, 0x7

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 673
    const/16 v2, 0x8

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 674
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 675
    const/4 v2, 0x6

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    .line 676
    const/4 v6, 0x5

    invoke-interface {v10, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 677
    const/4 v13, 0x2

    invoke-interface {v10, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 678
    const/4 v14, 0x3

    invoke-interface {v10, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    .line 679
    const/4 v15, 0x4

    invoke-interface {v10, v15}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 680
    invoke-static {v2, v6, v8}, Lcom/google/android/gms/games/a/l;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v16

    .line 682
    invoke-virtual/range {v16 .. v16}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    const-string v6, "Pending operations should always be 3P"

    invoke-static {v2, v6}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 686
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 687
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const/4 v6, 0x0

    move-object/from16 v0, v16

    invoke-static {v2, v0, v4, v5, v6}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/games/a/au;

    move-result-object v2

    .line 690
    new-instance v4, Lcom/google/android/gms/games/a/e;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v2}, Lcom/google/android/gms/games/a/e;-><init>(Lcom/google/android/gms/games/a/c;Lcom/google/android/gms/games/a/au;)V

    move-object/from16 v0, v16

    invoke-virtual {v11, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 695
    :cond_0
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/e;

    const/4 v6, 0x0

    const/4 v5, 0x0

    if-lez v15, :cond_2

    new-instance v5, Lcom/google/android/gms/games/h/a/bn;

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-direct {v5, v4}, Lcom/google/android/gms/games/h/a/bn;-><init>(Ljava/lang/Integer;)V

    const-string v4, "SET_STEPS_AT_LEAST"

    :goto_2
    new-instance v13, Lcom/google/android/gms/games/h/a/k;

    invoke-direct {v13, v12, v6, v5, v4}, Lcom/google/android/gms/games/h/a/k;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/h/a/bm;Lcom/google/android/gms/games/h/a/bn;Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/gms/games/a/e;->b:Ljava/util/ArrayList;

    invoke-virtual {v2, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 701
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 702
    invoke-static {v7, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v4

    .line 703
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v5

    .line 704
    move-object/from16 v0, v16

    invoke-virtual {v11, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/e;

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/gms/games/a/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 710
    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    .line 682
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 695
    :cond_2
    if-lez v14, :cond_3

    :try_start_1
    new-instance v6, Lcom/google/android/gms/games/h/a/bm;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/a/c;->b:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v14}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-direct {v6, v4, v13}, Lcom/google/android/gms/games/h/a/bm;-><init>(Ljava/lang/Long;Ljava/lang/Integer;)V

    const-string v4, "INCREMENT"

    goto :goto_2

    :cond_3
    const/4 v4, 0x1

    if-ne v13, v4, :cond_4

    const-string v4, "REVEAL"

    goto :goto_2

    :cond_4
    const-string v4, "UNLOCK"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 710
    :cond_5
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 712
    invoke-virtual {v11}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v4, v3

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/server/ClientContext;

    .line 713
    invoke-virtual {v11, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/a/e;

    .line 715
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p0

    invoke-direct {v0, v6, v2, v3}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/e;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 716
    iget-object v2, v3, Lcom/google/android/gms/games/a/e;->c:Ljava/util/ArrayList;

    invoke-virtual {v9, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    goto :goto_3

    .line 719
    :cond_6
    const/4 v2, 0x5

    move v4, v2

    .line 721
    goto :goto_3

    .line 724
    :cond_7
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "AchievementAgent"

    invoke-static {v2, v9, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 725
    return v4
.end method

.method private static c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/util/Pair;
    .locals 8

    .prologue
    const-wide/16 v4, -0x1

    const/4 v6, 0x1

    const/4 v7, 0x0

    const/4 v1, -0x1

    .line 800
    .line 802
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "external_achievement_id=?"

    new-array v6, v6, [Ljava/lang/String;

    aput-object p1, v6, v7

    invoke-virtual {v0, v2, v3, v6}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/games/a/j;->a:[Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v6

    .line 809
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 810
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 811
    const/4 v0, 0x1

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 814
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 817
    cmp-long v4, v2, v4

    if-eqz v4, :cond_0

    if-ne v0, v1, :cond_1

    .line 818
    :cond_0
    const/4 v0, 0x0

    .line 820
    :goto_1
    return-object v0

    .line 814
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0

    .line 820
    :cond_1
    new-instance v1, Landroid/util/Pair;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    move-object v0, v1

    goto :goto_1

    :cond_2
    move v0, v1

    move-wide v2, v4

    goto :goto_0
.end method

.method private d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1103
    .line 1104
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;)I

    .line 1106
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-eqz v0, :cond_0

    .line 1107
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 1110
    :cond_0
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    if-nez v0, :cond_4

    .line 1111
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->g(Lcom/google/android/gms/games/a/au;)I

    .line 1112
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->h(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 1116
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v1

    .line 1117
    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/b/a;->b(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-virtual {v2, v1, v4, v5}, Lcom/google/android/gms/games/b/a;->a(Ljava/lang/Object;J)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 1119
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    const/4 v2, 0x0

    const/4 v3, -0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 1124
    :goto_2
    return-object v0

    :cond_2
    move v0, v1

    .line 1110
    goto :goto_0

    .line 1123
    :cond_3
    invoke-static {p1}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v1

    .line 1124
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    const-string v2, "state,last_updated_timestamp DESC,sorting_rank"

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    :cond_4
    move v0, v1

    goto :goto_1
.end method

.method private static d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 825
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 827
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    const-wide/16 v2, 0x1

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private e(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 10

    .prologue
    const-wide/16 v8, -0x1

    const/4 v6, 0x1

    .line 955
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/c;->f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 956
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 957
    cmp-long v3, v0, v8

    if-nez v3, :cond_1

    .line 958
    const-string v0, "AchievementAgent"

    const-string v1, "forceResolveInstanceId did not find instance"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 961
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->g(Lcom/google/android/gms/games/a/au;)I

    .line 962
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->h(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 963
    if-eqz v0, :cond_0

    .line 967
    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Inserting a local stub for achievement instance for game "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", achievement "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", and player "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 970
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Landroid/util/Pair;

    move-result-object v1

    if-nez v1, :cond_2

    const-string v0, "AchievementAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Could not find definition for "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 974
    :cond_0
    :goto_0
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/c;->f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 975
    cmp-long v3, v0, v8

    if-nez v3, :cond_1

    .line 977
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Cannot find achievement instance to update; Game: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Achievement: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, ", Player: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    new-instance v4, Ljava/lang/Throwable;

    invoke-direct {v4}, Ljava/lang/Throwable;-><init>()V

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/games/a/c;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 983
    :cond_1
    return-wide v0

    .line 970
    :cond_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v4, "definition_id"

    iget-object v0, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v3, v4, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "player_id"

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "state"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ne v0, v6, :cond_3

    const-string v0, "current_steps"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v0, "formatted_current_steps"

    const-string v1, "0"

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    :cond_3
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    goto/16 :goto_0
.end method

.method private e(Lcom/google/android/gms/games/a/au;)V
    .locals 2

    .prologue
    .line 1139
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1140
    return-void
.end method

.method private static f(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 1464
    invoke-static {p0}, Lcom/google/android/gms/games/provider/o;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v2

    .line 1465
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v3, "_id"

    const-string v4, "external_achievement_id=?"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v5, v0

    const-wide/16 v6, -0x1

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method private f(Lcom/google/android/gms/games/a/au;)V
    .locals 4

    .prologue
    .line 1163
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->g:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v1

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1164
    return-void
.end method

.method private g(Lcom/google/android/gms/games/a/au;)I
    .locals 12

    .prologue
    const/16 v7, 0x1f4

    const/4 v6, 0x0

    .line 1179
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v1, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->f:Lcom/google/android/gms/games/h/a/c;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v4, "applications/%1$s/achievements"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v8, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v8

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v0, :cond_0

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/c;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/c;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/d;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/d;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/d;->getItems()Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 1189
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v1, v8}, Lcom/google/android/gms/games/provider/n;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const-string v8, "external_achievement_id"

    invoke-static {v0, v1, v8}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v8

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v9

    move v1, v6

    :goto_1
    if-ge v1, v9, :cond_4

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/a;

    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v10, "game_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v10, "sorting_rank"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v0, v10, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v10, "external_achievement_id"

    invoke-virtual {v0, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v8, v10}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v10, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v10}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v10

    invoke-static {v10}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v10

    invoke-virtual {v10, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v10

    invoke-virtual {v0, v10}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1179
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->d:Lcom/google/android/gms/games/h/a/b;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const-string v3, "achievements"

    if-eqz v0, :cond_2

    const-string v4, "language"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/b;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/b;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/d;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/d;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 1181
    :catch_0
    move-exception v0

    .line 1182
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 1183
    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_3
    move v0, v7

    .line 1190
    :goto_2
    return v0

    .line 1189
    :cond_4
    invoke-virtual {v8}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    :cond_5
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "AchievementAgent"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v0

    .line 1190
    if-eqz v0, :cond_6

    move v0, v6

    goto :goto_2

    :cond_6
    move v0, v7

    goto :goto_2
.end method

.method private h(Lcom/google/android/gms/games/a/au;)I
    .locals 10

    .prologue
    const/16 v8, 0x1f4

    const/4 v7, 0x0

    .line 1208
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->a(Ljava/util/ArrayList;)Lcom/google/android/gms/games/h/a/y;

    move-result-object v6

    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->e:Lcom/google/android/gms/games/h/a/n;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v2

    const-string v3, "ALL"

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/h/a/n;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/h/a/y;)Lcom/google/android/gms/games/h/a/cu;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->f(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cu;->getItems()Ljava/util/ArrayList;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1218
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Z

    move-result v0

    .line 1219
    if-eqz v0, :cond_3

    move v0, v7

    :goto_1
    return v0

    .line 1208
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    const-string v3, "ALL"

    const-string v5, "players/%1$s/achievements"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    if-eqz v4, :cond_1

    const-string v5, "language"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v5, v4}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_1
    const-string v4, "state"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v4, v3}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v2, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/cv;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cv;

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->f(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cv;->getItems()Ljava/util/ArrayList;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    move-result-object v0

    goto :goto_0

    .line 1210
    :catch_0
    move-exception v0

    .line 1211
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1212
    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_2
    move v0, v8

    .line 1214
    goto :goto_1

    :cond_3
    move v0, v8

    .line 1219
    goto :goto_1
.end method

.method private static i(Lcom/google/android/gms/games/a/au;)V
    .locals 2

    .prologue
    .line 1490
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/service/GamesUploadService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 1491
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 1025
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)Lcom/google/android/gms/games/a/g;
    .locals 13

    .prologue
    .line 285
    new-instance v10, Lcom/google/android/gms/games/a/f;

    move/from16 v0, p3

    move-object/from16 v1, p4

    invoke-direct {v10, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/f;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    .line 287
    const/4 v2, 0x1

    move/from16 v0, p5

    invoke-direct {p0, v10, v2, v0}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/f;ZZ)J

    move-result-wide v8

    .line 289
    const-wide/16 v2, -0x1

    cmp-long v2, v8, v2

    if-nez v2, :cond_0

    .line 290
    new-instance v2, Lcom/google/android/gms/games/a/g;

    iget v3, v10, Lcom/google/android/gms/games/a/f;->f:I

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    .line 339
    :goto_0
    return-object v2

    .line 293
    :cond_0
    if-eqz p5, :cond_1

    iget v2, v10, Lcom/google/android/gms/games/a/f;->f:I

    if-eqz v2, :cond_2

    .line 294
    :cond_1
    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move/from16 v6, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 296
    invoke-static {p1}, Lcom/google/android/gms/games/a/c;->i(Lcom/google/android/gms/games/a/au;)V

    .line 297
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v8, v9}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    goto :goto_0

    .line 305
    :cond_2
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/a/c;->b:Ljava/util/Random;

    invoke-virtual {v5}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    const-string v5, "achievements/%1$s/increment"

    const/4 v7, 0x1

    new-array v7, v7, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v12

    aput-object v12, v7, v11

    invoke-static {v5, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    const-string v7, "stepsToIncrement"

    invoke-static {v4}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v7, v4}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v6, :cond_3

    const-string v4, "requestId"

    invoke-static {v6}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v4, v6}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_3
    iget-object v2, v2, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const/4 v6, 0x0

    const-class v7, Lcom/google/android/gms/games/h/a/e;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/e;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    .line 332
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;)V

    .line 333
    if-eqz v2, :cond_6

    .line 334
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/e;->b()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/e;->c()Ljava/lang/Boolean;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    invoke-direct {p0, v10, v3, v4}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/f;IZ)V

    .line 335
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/e;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    .line 336
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v2

    .line 339
    :goto_1
    new-instance v4, Lcom/google/android/gms/games/a/g;

    iget v5, v10, Lcom/google/android/gms/games/a/f;->f:I

    invoke-direct {v4, v5, v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    move-object v2, v4

    goto/16 :goto_0

    .line 307
    :catch_0
    move-exception v2

    .line 308
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 309
    const-string v3, "AchievementAgent"

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 311
    :cond_4
    invoke-static {v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v2

    if-nez v2, :cond_5

    .line 312
    const-string v2, "AchievementAgent"

    const-string v3, "Encountered hard error while incrementing achievement."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x6

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    .line 318
    :cond_5
    const-string v2, "AchievementAgent"

    const-string v3, "Unable to increment achievement. Increment will be deferred."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move/from16 v6, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 321
    new-instance v2, Lcom/google/android/gms/games/a/g;

    const/4 v3, 0x5

    invoke-direct {v2, v3, v8, v9}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    goto/16 :goto_0

    .line 323
    :catch_1
    move-exception v2

    move-object v8, v2

    .line 325
    const-string v2, "AchievementAgent"

    const-string v3, "Auth error while incrementing achievement over network"

    invoke-static {v2, v3, v8}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 326
    const/4 v4, 0x1

    const/4 v5, -0x1

    const/4 v7, 0x0

    move-object v2, p1

    move-object v3, p2

    move/from16 v6, p3

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 328
    throw v8

    :cond_6
    move-wide v2, v8

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;
    .locals 1

    .prologue
    .line 243
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 225
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->i:Lcom/google/android/gms/games/b/a;

    iget-object v0, v0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 226
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 651
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 652
    if-eqz v0, :cond_0

    .line 653
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 655
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 1040
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->h:Z

    const-string v1, "The internal achievements should only be called by first party contexts."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1042
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;Z)Lcom/google/android/gms/games/a/g;
    .locals 10

    .prologue
    .line 360
    new-instance v8, Lcom/google/android/gms/games/a/f;

    invoke-direct {v8, p1, p2, p3, p4}, Lcom/google/android/gms/games/a/f;-><init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)V

    .line 362
    const/4 v0, 0x0

    invoke-direct {p0, v8, v0, p5}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/f;ZZ)J

    move-result-wide v6

    .line 364
    const-wide/16 v0, -0x1

    cmp-long v0, v6, v0

    if-nez v0, :cond_0

    .line 365
    new-instance v0, Lcom/google/android/gms/games/a/g;

    iget v1, v8, Lcom/google/android/gms/games/a/f;->f:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    .line 414
    :goto_0
    return-object v0

    .line 368
    :cond_0
    if-eqz p5, :cond_1

    iget v0, v8, Lcom/google/android/gms/games/a/f;->f:I

    if-eqz v0, :cond_2

    .line 369
    :cond_1
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v0, p1

    move-object v1, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 371
    invoke-static {p1}, Lcom/google/android/gms/games/a/c;->i(Lcom/google/android/gms/games/a/au;)V

    .line 372
    new-instance v0, Lcom/google/android/gms/games/a/g;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    goto :goto_0

    .line 379
    :cond_2
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/c;->c:Lcom/google/android/gms/games/h/a/m;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, "achievements/%1$s/setStepsAtLeast"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "steps"

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/h/a/m;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v0, v0, Lcom/google/android/gms/games/h/a/m;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x1

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/g;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/g;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    .line 406
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->e(Lcom/google/android/gms/games/a/au;)V

    .line 407
    if-eqz v0, :cond_5

    .line 408
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/g;->b()Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/g;->c()Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-direct {p0, v8, v1, v2}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/f;IZ)V

    .line 409
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/g;->c()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_5

    .line 410
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 414
    :goto_1
    new-instance v2, Lcom/google/android/gms/games/a/g;

    iget v3, v8, Lcom/google/android/gms/games/a/f;->f:I

    invoke-direct {v2, v3, v0, v1}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    move-object v0, v2

    goto :goto_0

    .line 381
    :catch_0
    move-exception v0

    .line 382
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 383
    const-string v1, "AchievementAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 385
    :cond_3
    invoke-static {v0}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 386
    const-string v0, "AchievementAgent"

    const-string v1, "Encountered hard error while setting achievement steps."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 387
    new-instance v0, Lcom/google/android/gms/games/a/g;

    const/4 v1, 0x6

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/a/g;-><init>(I)V

    goto/16 :goto_0

    .line 392
    :cond_4
    const-string v0, "AchievementAgent"

    const-string v1, "Unable to set achievement steps. Set will be deferred."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v0, p1

    move-object v1, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 395
    new-instance v0, Lcom/google/android/gms/games/a/g;

    const/4 v1, 0x5

    invoke-direct {v0, v1, v6, v7}, Lcom/google/android/gms/games/a/g;-><init>(IJ)V

    goto/16 :goto_0

    .line 397
    :catch_1
    move-exception v0

    move-object v6, v0

    .line 399
    const-string v0, "AchievementAgent"

    const-string v1, "Auth error while incrementing achievement over network"

    invoke-static {v0, v1, v6}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 400
    const/4 v2, 0x1

    const/4 v3, -0x1

    const/4 v4, 0x0

    move-object v0, p1

    move-object v1, p2

    move v5, p3

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;IIII)J

    .line 402
    throw v6

    :cond_5
    move-wide v0, v6

    goto :goto_1
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0, p3}, Lcom/google/android/gms/games/a/c;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;ILcom/google/android/gms/games/internal/el;)Lcom/google/android/gms/games/a/g;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z
    .locals 6

    .prologue
    .line 1057
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->i:Z

    const-string v1, "The internal achievements should be called by first party background contexts."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1059
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->g(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 1060
    :goto_0
    if-nez v0, :cond_0

    .line 1062
    const-string v1, "AchievementAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to sync definitions for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1064
    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    .line 1066
    :cond_0
    return v0

    .line 1059
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1082
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 1097
    :cond_0
    :goto_0
    return v0

    .line 1087
    :cond_1
    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->i:Z

    const-string v3, "The internal achievements should be called by first party background contexts."

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1089
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    .line 1090
    :goto_1
    if-eqz v2, :cond_3

    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/c;->h(Lcom/google/android/gms/games/a/au;)I

    move-result v2

    if-nez v2, :cond_3

    .line 1091
    :goto_2
    if-nez v0, :cond_0

    .line 1093
    const-string v1, "AchievementAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to sync instances for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1095
    iget-object v1, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v1, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0

    :cond_2
    move v2, v1

    .line 1089
    goto :goto_1

    :cond_3
    move v0, v1

    .line 1090
    goto :goto_2
.end method
