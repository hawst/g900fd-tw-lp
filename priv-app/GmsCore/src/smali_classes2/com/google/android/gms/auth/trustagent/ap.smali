.class final Lcom/google/android/gms/auth/trustagent/ap;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Ljava/lang/Object;

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/auth/trustagent/am;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/am;Ljava/lang/Object;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    iput-object p3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Ljava/lang/Void;
    .locals 6

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/am;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 189
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    if-nez v0, :cond_0

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    invoke-interface {v0, v2}, Lcom/google/android/gms/auth/trustagent/q;->b(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    :goto_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 209
    const/4 v0, 0x0

    return-object v0

    .line 191
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/String;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 205
    :catch_0
    move-exception v0

    .line 206
    :try_start_3
    const-string v2, "Coffee - PreferenceServiceClient"

    const-string v3, "Failed to access preference service."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 193
    :cond_1
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Boolean;

    if-eqz v0, :cond_2

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;Z)V

    goto :goto_0

    .line 195
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/Set;

    if-eqz v0, :cond_3

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/Set;

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    invoke-interface {v2, v3, v4}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 197
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/util/List;

    if-eqz v0, :cond_4

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v2, v3, v0}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;Ljava/util/List;)V

    goto :goto_0

    .line 199
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    instance-of v0, v0, Ljava/lang/Long;

    if-eqz v0, :cond_5

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-interface {v2, v3, v4, v5}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;J)V

    goto :goto_0

    .line 202
    :cond_5
    const-string v0, "Coffee - PreferenceServiceClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid type for preference value. Ignored. (key: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " value: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 184
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/ap;->a()Ljava/lang/Void;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->c:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ap;->a:Ljava/lang/Object;

    return-void
.end method
