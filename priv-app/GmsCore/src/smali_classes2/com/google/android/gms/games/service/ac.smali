.class public final Lcom/google/android/gms/games/service/ac;
.super Lcom/google/android/gms/common/f/a;
.source "SourceFile"


# static fields
.field private static final a:[Ljava/lang/String;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:Landroid/os/Bundle;

.field private static final e:Landroid/os/Bundle;

.field private static final f:Landroid/os/Bundle;

.field private static final g:Landroid/os/Bundle;

.field private static final h:Ljava/lang/Object;


# instance fields
.field private final i:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 55
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v3

    const-string v1, "feed"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/service/ac;->a:[Ljava/lang/String;

    .line 87
    new-array v0, v4, [Ljava/lang/String;

    const-string v1, "http://games.google.com/sync/match/%s"

    aput-object v1, v0, v3

    const-string v1, "http://games.google.com/sync/request/%s"

    aput-object v1, v0, v2

    sput-object v0, Lcom/google/android/gms/games/service/ac;->b:[Ljava/lang/String;

    .line 93
    new-array v0, v2, [Ljava/lang/String;

    const-string v1, "com.google.android.gms.games.notification"

    aput-object v1, v0, v3

    sput-object v0, Lcom/google/android/gms/games/service/ac;->c:[Ljava/lang/String;

    .line 100
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/ac;->d:Landroid/os/Bundle;

    .line 101
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/ac;->e:Landroid/os/Bundle;

    .line 102
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/ac;->f:Landroid/os/Bundle;

    .line 103
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/ac;->g:Landroid/os/Bundle;

    .line 105
    sget-object v0, Lcom/google/android/gms/games/service/ac;->e:Landroid/os/Bundle;

    const-string v1, "force"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 106
    sget-object v0, Lcom/google/android/gms/games/service/ac;->f:Landroid/os/Bundle;

    const-string v1, "peridoic_sync"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    sget-object v0, Lcom/google/android/gms/games/service/ac;->g:Landroid/os/Bundle;

    const-string v1, "fine_grained_sync"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 111
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/ac;->h:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 119
    const-string v0, "com.google.android.gms.games.background"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/f/a;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    .line 120
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/ac;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    .line 121
    return-void
.end method

.method public static a()V
    .locals 2

    .prologue
    .line 542
    const-string v0, "GamesSyncAdapter"

    const-string v1, "Forced tickle syncs are deprecated, ignoring request."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    return-void
.end method

.method public static a(Landroid/accounts/Account;)V
    .locals 5

    .prologue
    .line 528
    sget-object v0, Lcom/google/android/gms/games/c/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    .line 529
    const-string v2, "GamesSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Establishing sync with period "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 530
    const-string v2, "com.google.android.gms.games.background"

    sget-object v3, Lcom/google/android/gms/games/service/ac;->f:Landroid/os/Bundle;

    invoke-static {p0, v2, v3, v0, v1}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 532
    return-void
.end method

.method private static a(Landroid/accounts/Account;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 394
    invoke-static {p0, p1}, Landroid/content/ContentResolver;->getIsSyncable(Landroid/accounts/Account;Ljava/lang/String;)I

    move-result v0

    if-ltz v0, :cond_0

    if-eqz p2, :cond_1

    .line 395
    :cond_0
    invoke-static {p0, p1, v1}, Landroid/content/ContentResolver;->setIsSyncable(Landroid/accounts/Account;Ljava/lang/String;I)V

    .line 397
    :cond_1
    invoke-static {p0, p1, v1}, Landroid/content/ContentResolver;->setSyncAutomatically(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 400
    sget-object v0, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-static {p0, p1, v0}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 401
    return-void
.end method

.method public static a(Landroid/accounts/Account;Z)V
    .locals 1

    .prologue
    .line 377
    const-string v0, "com.google.android.gms.games.background"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/games/service/ac;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 378
    const-string v0, "com.google.android.gms.games"

    invoke-static {p0, v0, p1}, Lcom/google/android/gms/games/service/ac;->a(Landroid/accounts/Account;Ljava/lang/String;Z)V

    .line 379
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 4

    .prologue
    .line 486
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    .line 487
    sget-object v1, Lcom/google/android/gms/games/service/ac;->d:Landroid/os/Bundle;

    if-eqz v0, :cond_0

    new-instance v2, Landroid/accounts/Account;

    const-string v3, "com.google"

    invoke-direct {v2, v0, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "com.google.android.gms.games.background"

    invoke-static {v2, v0, v1}, Landroid/content/ContentResolver;->requestSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 488
    :cond_0
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/t;Lcom/google/android/gms/games/service/ad;Z)V
    .locals 2

    .prologue
    .line 316
    sget-object v1, Lcom/google/android/gms/games/service/ac;->h:Ljava/lang/Object;

    monitor-enter v1

    .line 319
    :try_start_0
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 320
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->e(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 321
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->f(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 322
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->h(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 323
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 324
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->g(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 325
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->m(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 328
    if-eqz p3, :cond_0

    .line 329
    invoke-virtual {p1, p0}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;

    .line 330
    const/4 v0, 0x3

    invoke-virtual {p2, v0}, Lcom/google/android/gms/games/service/ad;->a(I)V

    .line 331
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 332
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->d(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 333
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->n(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I

    .line 338
    :cond_0
    invoke-virtual {p1, p0, p2}, Lcom/google/android/gms/games/a/t;->k(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 339
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Landroid/content/Context;Landroid/accounts/Account;)Z
    .locals 14

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 555
    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v8

    .line 556
    const-wide/16 v0, 0x0

    cmp-long v0, v8, v0

    if-gtz v0, :cond_0

    .line 557
    const-string v0, "GamesSyncAdapter"

    const-string v1, "Unable to retrieve ID, failed to register for notifications."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 625
    :goto_0
    return v0

    .line 562
    :cond_0
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 563
    sget-object v0, Lcom/google/android/gsf/o;->a:Landroid/net/Uri;

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "authority=?"

    sget-object v2, Lcom/google/android/gms/games/service/ac;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 568
    iget-object v11, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 569
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 570
    new-instance v12, Ljava/util/HashMap;

    invoke-direct {v12}, Ljava/util/HashMap;-><init>()V

    .line 571
    sget-object v1, Lcom/google/android/gsf/o;->a:Landroid/net/Uri;

    sget-object v2, Lcom/google/android/gms/games/service/ac;->a:[Ljava/lang/String;

    const-string v3, "_sync_account=? AND authority=?"

    const/4 v4, 0x2

    new-array v4, v4, [Ljava/lang/String;

    aput-object v11, v4, v6

    const-string v5, "com.google.android.gms.games"

    aput-object v5, v4, v7

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 575
    :goto_1
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 576
    const/4 v2, 0x1

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v12, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 579
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_1
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 583
    sget-object v1, Lcom/google/android/gms/games/service/ac;->b:[Ljava/lang/String;

    array-length v2, v1

    move v1, v6

    .line 584
    :goto_2
    if-ge v1, v2, :cond_3

    .line 585
    sget-object v3, Lcom/google/android/gms/games/service/ac;->b:[Ljava/lang/String;

    aget-object v3, v3, v1

    new-array v4, v7, [Ljava/lang/Object;

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 586
    invoke-virtual {v12, v3}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 588
    invoke-virtual {v12, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 584
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 591
    :cond_2
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 592
    const-string v5, "_sync_account"

    invoke-virtual {v4, v5, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 593
    const-string v5, "_sync_account_type"

    const-string v13, "com.google"

    invoke-virtual {v4, v5, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 595
    const-string v5, "feed"

    invoke-virtual {v4, v5, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    const-string v3, "service"

    const-string v5, "games"

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 597
    const-string v3, "authority"

    const-string v5, "com.google.android.gms.games"

    invoke-virtual {v4, v3, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 599
    sget-object v3, Lcom/google/android/gsf/o;->a:Landroid/net/Uri;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v4}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v3

    invoke-virtual {v10, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 606
    :cond_3
    invoke-virtual {v12}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_4
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 607
    sget-object v3, Lcom/google/android/gsf/o;->a:Landroid/net/Uri;

    invoke-virtual {v12, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v3, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    .line 609
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v10, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 613
    :cond_4
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 615
    :try_start_1
    sget-object v1, Lcom/google/android/gsf/o;->a:Landroid/net/Uri;

    invoke-virtual {v1}, Landroid/net/Uri;->getAuthority()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v10}, Landroid/content/ContentResolver;->applyBatch(Ljava/lang/String;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/content/OperationApplicationException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    move v0, v7

    .line 625
    goto/16 :goto_0

    .line 616
    :catch_0
    move-exception v0

    .line 617
    new-instance v1, Ljava/lang/AssertionError;

    invoke-direct {v1, v0}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v1

    .line 618
    :catch_1
    move-exception v0

    .line 619
    const-string v1, "GamesSyncAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error applying batch operation: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v6

    .line 620
    goto/16 :goto_0
.end method

.method private a(Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 343
    if-eqz p1, :cond_0

    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 349
    :cond_0
    :goto_0
    return v0

    .line 347
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    .line 348
    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 349
    const-string v4, "http://games.google.com/sync/match/%s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/t;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/games/service/ad;)Z
    .locals 10

    .prologue
    .line 169
    if-eqz p3, :cond_0

    const-string v0, "initialize"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 170
    const/4 v0, 0x0

    invoke-static {p2, v0}, Lcom/google/android/gms/games/service/ac;->a(Landroid/accounts/Account;Z)V

    .line 173
    const/4 v6, 0x1

    .line 303
    :goto_0
    return v6

    .line 177
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->h(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 178
    const-string v0, "GamesSyncAdapter"

    const-string v1, "Sync for NON-EXISTENT ACCOUNT"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 179
    const/4 v6, 0x0

    goto :goto_0

    .line 183
    :cond_1
    const-string v0, "com.google.android.gms.games"

    invoke-virtual {v0, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/ac;->a(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/ac;->b(Landroid/os/Bundle;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 185
    const-string v0, "GamesSyncAdapter"

    const-string v1, "Syncing notifications without tickle; exiting"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 186
    const/4 v6, 0x0

    goto :goto_0

    .line 191
    :cond_2
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/service/ac;->b(Landroid/accounts/Account;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 192
    const-string v0, "GamesSyncAdapter"

    const-string v1, "User is not G+ enabled. Aborting sync"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 193
    const/4 v6, 0x0

    goto :goto_0

    .line 198
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    iget-object v1, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 200
    invoke-static {p3}, Lcom/google/android/gms/games/service/ac;->c(Landroid/os/Bundle;)Z

    move-result v4

    .line 201
    if-eqz p3, :cond_4

    const-string v0, "fine_grained_sync"

    const/4 v1, 0x0

    invoke-virtual {p3, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x1

    move v2, v0

    .line 205
    :goto_1
    if-eqz v4, :cond_5

    sget-object v0, Lcom/google/android/gms/games/c/a;->z:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_5

    .line 206
    const-string v0, "GamesSyncAdapter"

    const-string v1, "Periodic syncs are disabled. Bailing."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    const/4 v6, 0x0

    goto :goto_0

    .line 201
    :cond_4
    const/4 v0, 0x0

    move v2, v0

    goto :goto_1

    .line 212
    :cond_5
    if-eqz v4, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/gms/games/a/t;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 213
    const-string v0, "GamesSyncAdapter"

    const-string v1, "User has not gamed recently; ignoring periodic sync"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 217
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-virtual {p1, v0, v3}, Lcom/google/android/gms/games/a/t;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/au;

    move-result-object v5

    .line 221
    const/4 v0, 0x0

    .line 225
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-virtual {p1, v1, v3}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v1

    .line 226
    const/4 v6, 0x1

    invoke-virtual {p5, v6}, Lcom/google/android/gms/games/service/ad;->a(I)V

    .line 227
    if-nez v1, :cond_7

    .line 228
    const-string v1, "GamesSyncAdapter"

    const-string v6, "Failed revision check during sync. Your version of Google Play services is out of date."

    invoke-static {v1, v6}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 230
    iget-object v1, p5, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    .line 231
    const/4 v6, 0x0

    goto/16 :goto_0

    .line 237
    :cond_7
    invoke-virtual {p1, v5}, Lcom/google/android/gms/games/a/t;->e(Lcom/google/android/gms/games/a/au;)V

    .line 238
    const/4 v1, 0x2

    invoke-virtual {p5, v1}, Lcom/google/android/gms/games/service/ad;->a(I)V

    .line 241
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/ac;->a(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_b

    .line 243
    invoke-virtual {p1, v5, p5}, Lcom/google/android/gms/games/a/t;->j(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I

    .line 244
    invoke-virtual {p1, v5, p5}, Lcom/google/android/gms/games/a/t;->l(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1

    .line 245
    const/4 v0, 0x1

    move v6, v0

    .line 268
    :goto_2
    iget-object v0, v5, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 271
    invoke-static {v3}, Lcom/google/android/gms/games/ui/d/ar;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v1

    iget-object v3, p2, Landroid/accounts/Account;->name:Ljava/lang/String;

    new-instance v7, Lcom/google/android/gms/games/internal/e/b;

    iget-object v8, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-virtual {p1, v8, v1}, Lcom/google/android/gms/games/a/t;->e(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v8

    invoke-direct {v7, v8}, Lcom/google/android/gms/games/internal/e/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    :try_start_1
    iget-object v8, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {v8, v1, v3, v0, v7}, Lcom/google/android/gms/games/service/i;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/internal/e/b;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-virtual {v7}, Lcom/google/android/gms/games/internal/e/b;->w_()V

    .line 278
    invoke-virtual {p1, v5}, Lcom/google/android/gms/games/a/t;->q(Lcom/google/android/gms/games/a/au;)V

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {p2}, Lcom/google/android/gms/games/service/ac;->a(Landroid/accounts/Account;)V

    .line 286
    if-nez v4, :cond_8

    if-eqz v2, :cond_9

    .line 287
    :cond_8
    sget-object v0, Lcom/google/android/gms/games/c/a;->B:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    invoke-virtual {p1, v5, v0, v1}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;J)Z

    move-result v0

    if-eqz v0, :cond_e

    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    sget-object v0, Lcom/google/android/gms/games/c/a;->A:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    const-string v2, "GamesSyncAdapter"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v7, "Establishing fine grained sync with period "

    invoke-direct {v3, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "com.google.android.gms.games.background"

    sget-object v3, Lcom/google/android/gms/games/service/ac;->g:Landroid/os/Bundle;

    invoke-static {p2, v2, v3, v0, v1}, Landroid/content/ContentResolver;->addPeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;J)V

    .line 296
    :cond_9
    :goto_3
    if-eqz v4, :cond_a

    .line 297
    invoke-virtual {p1, v5}, Lcom/google/android/gms/games/a/t;->k(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/games/service/ae;

    move-result-object v4

    iget-object v0, v5, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v5, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget v2, v4, Lcom/google/android/gms/games/service/ae;->a:I

    iget v3, v4, Lcom/google/android/gms/games/service/ae;->b:I

    iget-wide v4, v4, Lcom/google/android/gms/games/service/ae;->c:J

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;IIJ)V

    .line 302
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/service/ac;->a(Landroid/content/Context;Landroid/accounts/Account;)Z

    goto/16 :goto_0

    .line 246
    :cond_b
    :try_start_2
    invoke-direct {p0, p3}, Lcom/google/android/gms/games/service/ac;->b(Landroid/os/Bundle;)Z

    move-result v1

    if-eqz v1, :cond_c

    .line 248
    invoke-virtual {p1, v5, p5}, Lcom/google/android/gms/games/a/t;->i(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)I

    .line 249
    invoke-virtual {p1, v5, p5}, Lcom/google/android/gms/games/a/t;->l(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 250
    const/4 v0, 0x1

    move v6, v0

    goto/16 :goto_2

    .line 251
    :cond_c
    if-eqz v2, :cond_d

    .line 252
    invoke-virtual {p1, v5, p5}, Lcom/google/android/gms/games/a/t;->m(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/ad;)V

    .line 253
    const/4 v0, 0x1

    move v6, v0

    goto/16 :goto_2

    .line 255
    :cond_d
    invoke-static {v5, p1, p5, v4}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/t;Lcom/google/android/gms/games/service/ad;Z)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_2 .. :try_end_2} :catch_1

    .line 256
    const/4 v0, 0x1

    move v6, v0

    .line 265
    goto/16 :goto_2

    .line 258
    :catch_0
    move-exception v1

    .line 259
    const-string v6, "GamesSyncAdapter"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Auth error executing an operation: "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v6, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 260
    iget-object v1, p5, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v1, v1, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v8, 0x1

    add-long/2addr v6, v8

    iput-wide v6, v1, Landroid/content/SyncStats;->numAuthExceptions:J

    move v6, v0

    .line 265
    goto/16 :goto_2

    .line 261
    :catch_1
    move-exception v1

    .line 262
    const-string v6, "GamesSyncAdapter"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move v6, v0

    goto/16 :goto_2

    .line 271
    :catchall_0
    move-exception v0

    invoke-virtual {v7}, Lcom/google/android/gms/games/internal/e/b;->w_()V

    throw v0

    .line 290
    :cond_e
    const-string v0, "com.google.android.gms.games.background"

    sget-object v1, Lcom/google/android/gms/games/service/ac;->g:Landroid/os/Bundle;

    invoke-static {p2, v0, v1}, Landroid/content/ContentResolver;->removePeriodicSync(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_3
.end method

.method private b(Landroid/accounts/Account;)Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    const/4 v1, 0x1

    .line 453
    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    .line 454
    iget-object v0, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 455
    sget-object v3, Lcom/google/android/gms/common/internal/aj;->f:[Ljava/lang/String;

    invoke-virtual {v0, p1, v3, v4, v4}, Landroid/accounts/AccountManager;->hasFeatures(Landroid/accounts/Account;[Ljava/lang/String;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    move-result-object v0

    .line 458
    const-wide/16 v4, 0x3c

    :try_start_0
    sget-object v3, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v4, v5, v3}, Landroid/accounts/AccountManagerFuture;->getResult(JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;
    :try_end_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 468
    :goto_0
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    :goto_1
    return v0

    .line 459
    :catch_0
    move-exception v0

    .line 460
    const-string v3, "GamesSyncAdapter"

    const-string v4, "Authenticator error checking account"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 465
    goto :goto_0

    .line 461
    :catch_1
    move-exception v0

    .line 462
    const-string v3, "GamesSyncAdapter"

    const-string v4, "Operation canceled error checking account"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    .line 465
    goto :goto_0

    .line 463
    :catch_2
    move-exception v0

    .line 464
    const-string v3, "GamesSyncAdapter"

    const-string v4, "IO error checking account"

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/internal/dq;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v2

    goto :goto_0

    :cond_0
    move v0, v1

    .line 468
    goto :goto_1
.end method

.method private b(Landroid/os/Bundle;)Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v0, 0x0

    .line 353
    if-eqz p1, :cond_0

    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 359
    :cond_0
    :goto_0
    return v0

    .line 357
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v2

    .line 358
    const-string v1, "feed"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    new-array v4, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v4, v0

    invoke-static {v1, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 359
    const-string v4, "http://games.google.com/sync/request/%s"

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v5, v0

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method private static c(Landroid/os/Bundle;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 363
    if-eqz p0, :cond_0

    const-string v1, "peridoic_sync"

    invoke-virtual {p0, v1, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method


# virtual methods
.method public final onPerformSync(Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Landroid/content/ContentProviderClient;Landroid/content/SyncResult;)V
    .locals 16

    .prologue
    .line 128
    sget-object v2, Lcom/google/android/gms/games/c/a;->U:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 129
    const-string v2, "GamesSyncAdapter"

    const-string v3, "Aborting sync attempt during network capture"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 163
    :goto_0
    return-void

    .line 136
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/service/ac;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/util/a;->b(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    const-string v2, "GamesSyncAdapter"

    const-string v3, "In restricted profile; skipping sync."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 142
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/gms/games/service/ac;->getContext()Landroid/content/Context;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/util/a;->g(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 143
    const-string v2, "GamesSyncAdapter"

    const-string v3, "Restricted account; skipping sync."

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 147
    :cond_2
    const/4 v8, 0x0

    .line 148
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v10

    .line 149
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting sync for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->b()V

    .line 150
    const/4 v2, 0x1

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/ac;->a(Z)V

    .line 151
    new-instance v7, Lcom/google/android/gms/games/service/ad;

    move-object/from16 v0, p5

    invoke-direct {v7, v0}, Lcom/google/android/gms/games/service/ad;-><init>(Landroid/content/SyncResult;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v3

    move-object/from16 v2, p0

    move-object/from16 v4, p1

    move-object/from16 v5, p2

    move-object/from16 v6, p3

    .line 154
    :try_start_0
    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/games/a/t;Landroid/accounts/Account;Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/games/service/ad;)Z
    :try_end_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v8

    .line 158
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/t;->a()V

    .line 159
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/ac;->a(Z)V

    .line 162
    :goto_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    sub-long v14, v2, v10

    iget-object v2, v7, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v2, v2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v2, Landroid/content/SyncStats;->numIoExceptions:J

    iget-object v4, v7, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v4, v4, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v4, v4, Landroid/content/SyncStats;->numAuthExceptions:J

    add-long/2addr v2, v4

    long-to-int v9, v2

    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->isSyncPending(Landroid/accounts/Account;Ljava/lang/String;)Z

    move-result v11

    invoke-static/range {p2 .. p2}, Lcom/google/android/gms/games/service/ac;->c(Landroid/os/Bundle;)Z

    move-result v10

    const-string v5, ""

    if-eqz p2, :cond_3

    const-string v2, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "feed"

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Sync duration for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v14, v15}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->b()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/service/ac;->i:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v7}, Lcom/google/android/gms/games/service/ad;->a()[I

    move-result-object v12

    move-object/from16 v4, p3

    move-wide v6, v14

    invoke-static/range {v2 .. v12}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;JZIZZ[I)V

    goto/16 :goto_0

    .line 155
    :catch_0
    move-exception v2

    .line 156
    :try_start_1
    const-string v4, "GamesSyncAdapter"

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 158
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/t;->a()V

    .line 159
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/ac;->a(Z)V

    goto :goto_1

    .line 158
    :catchall_0
    move-exception v2

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/t;->a()V

    .line 159
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/service/ac;->a(Z)V

    throw v2
.end method
