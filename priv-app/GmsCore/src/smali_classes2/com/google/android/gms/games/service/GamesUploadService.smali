.class public Lcom/google/android/gms/games/service/GamesUploadService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# static fields
.field private static a:Ljava/util/concurrent/atomic/AtomicBoolean;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    sput-object v0, Lcom/google/android/gms/games/service/GamesUploadService;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 7

    .prologue
    const-wide/16 v4, 0x3e8

    const/4 v6, 0x1

    .line 78
    sget-object v0, Lcom/google/android/gms/games/service/GamesUploadService;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v6}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 95
    :goto_0
    return-void

    .line 81
    :cond_0
    const-string v0, "GamesUploadService"

    const-string v1, "Requesting network sync"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/google/android/gms/games/c/a;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    div-long/2addr v0, v4

    long-to-int v1, v0

    .line 85
    sget-object v0, Lcom/google/android/gms/games/c/a;->u:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    div-long/2addr v2, v4

    long-to-int v0, v2

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 88
    new-instance v2, Lcom/google/android/gms/gcm/an;

    invoke-direct {v2}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v3, Lcom/google/android/gms/games/service/GamesUploadService;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    int-to-long v4, v1

    int-to-long v0, v0

    invoke-virtual {v2, v4, v5, v0, v1}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const-string v1, "GamesUploadService:%s"

    new-array v2, v6, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    .line 94
    invoke-static {p0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 12

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x2

    const/4 v2, 0x0

    .line 39
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/GamesUploadService;->getApplicationContext()Landroid/content/Context;

    move-result-object v4

    .line 40
    new-instance v5, Lcom/google/android/gms/games/service/ad;

    invoke-direct {v5}, Lcom/google/android/gms/games/service/ad;-><init>()V

    .line 41
    iget-object v3, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    const-string v6, ":"

    invoke-virtual {v3, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v6, v3

    if-eq v6, v0, :cond_1

    const-string v3, "GamesUploadService"

    const-string v6, "Found unexpected GCM tag when scheduling; aborting"

    invoke-static {v3, v6}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 42
    :goto_0
    if-nez v3, :cond_2

    .line 43
    const-string v1, "GamesUploadService"

    const-string v3, "Failed to execute network upload - aborting"

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    sget-object v1, Lcom/google/android/gms/games/service/GamesUploadService;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 72
    :cond_0
    :goto_1
    return v0

    .line 41
    :cond_1
    aget-object v3, v3, v1

    invoke-static {v4, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    goto :goto_0

    .line 48
    :cond_2
    invoke-static {v4}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v6

    .line 49
    invoke-virtual {v6, v4, v3}, Lcom/google/android/gms/games/a/t;->e(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/au;

    move-result-object v3

    .line 52
    const/4 v4, 0x0

    :try_start_0
    invoke-static {v3, v6, v5, v4}, Lcom/google/android/gms/games/service/ac;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/t;Lcom/google/android/gms/games/service/ad;Z)V
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 61
    invoke-virtual {v6}, Lcom/google/android/gms/games/a/t;->a()V

    .line 65
    :goto_2
    iget-object v3, v5, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v6, v3, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v8, 0x0

    cmp-long v3, v6, v8

    if-lez v3, :cond_3

    move v0, v1

    .line 66
    goto :goto_1

    .line 55
    :catch_0
    move-exception v3

    :try_start_1
    iget-object v3, v5, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    iget-object v3, v3, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v8, v3, Landroid/content/SyncStats;->numAuthExceptions:J

    const-wide/16 v10, 0x1

    add-long/2addr v8, v10

    iput-wide v8, v3, Landroid/content/SyncStats;->numAuthExceptions:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 61
    invoke-virtual {v6}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_2

    .line 56
    :catch_1
    move-exception v3

    .line 57
    :try_start_2
    const-string v4, "GamesUploadService"

    invoke-virtual {v3}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v7

    invoke-static {v4, v7, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 61
    invoke-virtual {v6}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_2

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/google/android/gms/games/a/t;->a()V

    throw v0

    .line 70
    :cond_3
    sget-object v1, Lcom/google/android/gms/games/service/GamesUploadService;->a:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 71
    const-string v1, "GamesUploadService"

    const-string v3, "Network sync complete"

    invoke-static {v1, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 72
    iget-object v1, v5, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    invoke-virtual {v1}, Landroid/content/SyncResult;->hasHardError()Z

    move-result v1

    if-nez v1, :cond_0

    move v0, v2

    goto :goto_1
.end method
