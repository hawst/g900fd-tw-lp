.class final Lcom/google/android/gms/auth/trustagent/trustlet/ai;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/concurrent/Callable;


# instance fields
.field final synthetic a:Landroid/nfc/Tag;

.field final synthetic b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ah;Landroid/nfc/Tag;)V
    .locals 0

    .prologue
    .line 78
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->a:Landroid/nfc/Tag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final synthetic call()Ljava/lang/Object;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 78
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->j()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "Coffee - NFCTrustlet"

    const-string v2, "Ignoring tag read, device already trusted."

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a:Lcom/google/android/gms/auth/trustagent/trustlet/an;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->a:Landroid/nfc/Tag;

    invoke-virtual {v2}, Landroid/nfc/Tag;->getId()[B

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a([B)Ljava/lang/String;

    move-result-object v3

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/an;->a:Lcom/google/android/gms/auth/trustagent/trustlet/af;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/af;->b(Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    move-result-object v1

    if-eqz v1, :cond_1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a(Landroid/nfc/Tag;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v0, 0x1

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d:Z

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    if-eqz v0, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v2, v2, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    :goto_1
    iput-wide v2, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->c:J

    const-string v1, "Coffee - NFCTrustlet"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NFC tag read (success: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") but  trustlet not ready to authenticate."

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :goto_2
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    goto :goto_0

    :cond_2
    const-wide/16 v2, 0x0

    goto :goto_1

    :cond_3
    const-string v1, "Coffee - NFCTrustlet"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "NFC tag read to unlock, success: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ai;->b:Lcom/google/android/gms/auth/trustagent/trustlet/ah;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->a(Z)V

    goto :goto_2
.end method
