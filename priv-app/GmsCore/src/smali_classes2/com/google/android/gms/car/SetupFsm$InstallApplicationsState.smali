.class public Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_STARTED_MOVING"
            b = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_APPLICATIONS_UP_TO_DATE"
            b = Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
            c = Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_APPLICATION_INSTALLATION_FAILED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupRetryState;
            c = Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method

.method private b()V
    .locals 2

    .prologue
    .line 480
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    .line 481
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->i()Landroid/content/Intent;

    move-result-object v1

    .line 482
    if-eqz v1, :cond_0

    .line 483
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Landroid/content/Intent;)V

    .line 487
    :goto_0
    return-void

    .line 485
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->k()V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 448
    invoke-direct {p0}, Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;->b()V

    .line 449
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 453
    const-string v1, "EVENT_ACTIVITY_RESULT"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 454
    if-eqz p2, :cond_2

    .line 455
    check-cast p2, Lcom/google/android/car/fsm/ActivityResult;

    .line 456
    iget v1, p2, Lcom/google/android/car/fsm/ActivityResult;->a:I

    if-nez v1, :cond_1

    .line 457
    iget-object v1, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v2, "EVENT_APPLICATION_INSTALLATION_FAILED"

    invoke-virtual {v1, v2}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 476
    :cond_0
    :goto_0
    return v0

    .line 459
    :cond_1
    iget v1, p2, Lcom/google/android/car/fsm/ActivityResult;->a:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    .line 460
    invoke-direct {p0}, Lcom/google/android/gms/car/SetupFsm$InstallApplicationsState;->b()V

    goto :goto_0

    .line 466
    :cond_2
    const-string v1, "EVENT_CAR_STARTED_MOVING"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EVENT_ERROR"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EVENT_APPLICATIONS_UP_TO_DATE"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    const-string v1, "EVENT_APPLICATION_INSTALLATION_FAILED"

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 472
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method
