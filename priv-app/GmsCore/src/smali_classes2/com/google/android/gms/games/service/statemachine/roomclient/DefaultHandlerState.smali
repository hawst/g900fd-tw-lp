.class final Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 56
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 60
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 162
    :pswitch_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->g:Z

    :goto_0
    return v0

    .line 63
    :pswitch_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 65
    :pswitch_2
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "REGISTER_WAITING_ROOM_LISTENER_RESTRICTED: when not in room"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;

    .line 69
    const/4 v1, 0x1

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->a(Ljava/lang/Object;)V

    .line 70
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 73
    :pswitch_3
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/y;

    .line 74
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/y;->a(Ljava/lang/Object;)V

    .line 75
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 78
    :pswitch_4
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;

    .line 79
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;->a(Ljava/lang/Object;)V

    .line 80
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 83
    :pswitch_5
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;

    .line 85
    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->a:Lcom/google/android/gms/games/internal/dr;

    const/16 v2, 0x1966

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/dr;->a(IILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 95
    :pswitch_6
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 97
    :pswitch_7
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 100
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 101
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a()V

    .line 102
    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;

    move-result-object v0

    const/16 v1, 0x1b58

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 104
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 107
    :pswitch_8
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    .line 109
    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v1, :cond_0

    .line 110
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    const/16 v2, 0x1b5c

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/dr;->d(ILjava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 117
    :cond_0
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto :goto_0

    .line 114
    :catch_1
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Landroid/os/RemoteException;)V

    goto :goto_2

    .line 120
    :pswitch_9
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    .line 122
    :try_start_2
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v1, :cond_1

    .line 123
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    const/16 v2, 0x1b5c

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/dr;->d(ILjava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    .line 129
    :cond_1
    :goto_3
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 126
    :catch_2
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Landroid/os/RemoteException;)V

    goto :goto_3

    .line 132
    :pswitch_a
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unexpected: DONE_CLEANING"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 135
    :pswitch_b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;

    .line 136
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/g;->a()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b()V

    .line 139
    :cond_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 142
    :pswitch_c
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 144
    :pswitch_d
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 147
    :pswitch_e
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unexpected: ON_ROOM_LEFT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    invoke-static {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ca;Landroid/os/Message;)V

    .line 149
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 152
    :pswitch_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;

    .line 153
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bi;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 154
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 157
    :pswitch_10
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unexpected: ON_DCM_ROOM_ENTER_RESULT"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DefaultHandlerState;->f:Z

    goto/16 :goto_0

    .line 60
    :pswitch_data_0
    .packed-switch 0xd
        :pswitch_b
        :pswitch_a
        :pswitch_f
        :pswitch_9
        :pswitch_5
        :pswitch_6
        :pswitch_4
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_c
        :pswitch_7
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_e
        :pswitch_0
        :pswitch_0
        :pswitch_10
    .end packed-switch
.end method
