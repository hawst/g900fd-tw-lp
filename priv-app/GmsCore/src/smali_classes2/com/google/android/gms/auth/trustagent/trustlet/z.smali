.class final Lcom/google/android/gms/auth/trustagent/trustlet/z;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/x;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/x;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/x;B)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/z;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/x;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 84
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 85
    const-string v1, "Coffee - LocationProviderTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Receive location provider state changed action: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/x;->a()Z

    move-result v0

    .line 87
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iget-boolean v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    if-eq v1, v0, :cond_0

    .line 88
    const-string v1, "Coffee - LocationProviderTracker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Provider Enable state changed: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iget-boolean v3, v3, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "->"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/x;->b:Lcom/google/android/gms/auth/trustagent/trustlet/y;

    invoke-interface {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/y;->a(Z)V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/z;->a:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iput-boolean v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    .line 93
    :cond_0
    return-void
.end method
