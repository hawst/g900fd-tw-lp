.class public final Lcom/google/android/gms/herrevad/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/r;

.field public static final b:Lcom/google/android/gms/common/a/r;

.field public static final c:Lcom/google/android/gms/common/a/r;

.field public static final d:Lcom/google/android/gms/common/a/r;

.field public static final e:Lcom/google/android/gms/common/a/r;

.field public static final f:Lcom/google/android/gms/common/a/r;

.field public static final g:Lcom/google/android/gms/common/a/r;

.field private static final h:Lcom/google/android/gms/common/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 15
    new-instance v0, Lcom/google/android/gms/common/a/m;

    const-string v1, "herrevad"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/a/m;-><init>(Ljava/lang/String;)V

    .line 18
    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "networkQualityUploadsToday"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->a:Lcom/google/android/gms/common/a/r;

    .line 26
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "dayOfLastNetworkQualityUpload"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->b:Lcom/google/android/gms/common/a/r;

    .line 33
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "hasValidCaptivePortalData"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->c:Lcom/google/android/gms/common/a/r;

    .line 41
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "lastActiveApSsidBssidHash"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->d:Lcom/google/android/gms/common/a/r;

    .line 49
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "lastApIsCaptivePortal"

    invoke-static {v3}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->e:Lcom/google/android/gms/common/a/r;

    .line 62
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "herrevadId"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->f:Lcom/google/android/gms/common/a/r;

    .line 67
    sget-object v0, Lcom/google/android/gms/herrevad/b/a;->h:Lcom/google/android/gms/common/a/m;

    const-string v1, "lastHerrevadIdGenerationTimeSecs"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/herrevad/b/a;->g:Lcom/google/android/gms/common/a/r;

    return-void
.end method
