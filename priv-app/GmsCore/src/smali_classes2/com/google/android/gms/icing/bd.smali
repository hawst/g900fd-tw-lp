.class public final Lcom/google/android/gms/icing/bd;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:Z

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 486
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 487
    iput v0, p0, Lcom/google/android/gms/icing/bd;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/bd;->b:I

    iput-boolean v0, p0, Lcom/google/android/gms/icing/bd;->c:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bd;->cachedSize:I

    .line 488
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 558
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 559
    iget v1, p0, Lcom/google/android/gms/icing/bd;->a:I

    if-eqz v1, :cond_0

    .line 560
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/bd;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 563
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/bd;->b:I

    if-eqz v1, :cond_1

    .line 564
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/bd;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 567
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/bd;->c:Z

    if-eqz v1, :cond_2

    .line 568
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/bd;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 571
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 572
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 575
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 501
    if-ne p1, p0, :cond_1

    .line 524
    :cond_0
    :goto_0
    return v0

    .line 504
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bd;

    if-nez v2, :cond_2

    move v0, v1

    .line 505
    goto :goto_0

    .line 507
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bd;

    .line 508
    iget v2, p0, Lcom/google/android/gms/icing/bd;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/bd;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 509
    goto :goto_0

    .line 511
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/bd;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/bd;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 512
    goto :goto_0

    .line 514
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/bd;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/bd;->c:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 515
    goto :goto_0

    .line 517
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 518
    iget-object v2, p1, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 519
    goto :goto_0

    .line 521
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 522
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 529
    iget v0, p0, Lcom/google/android/gms/icing/bd;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 531
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bd;->b:I

    add-int/2addr v0, v1

    .line 532
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/bd;->c:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    .line 533
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_1
    add-int/2addr v0, v1

    .line 535
    return v0

    .line 532
    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0

    .line 533
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 457
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bd;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bd;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/bd;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 541
    iget v0, p0, Lcom/google/android/gms/icing/bd;->a:I

    if-eqz v0, :cond_0

    .line 542
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/bd;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 544
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/bd;->b:I

    if-eqz v0, :cond_1

    .line 545
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/bd;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 547
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/bd;->c:Z

    if-eqz v0, :cond_2

    .line 548
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/bd;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 550
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 551
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/bd;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 553
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 554
    return-void
.end method
