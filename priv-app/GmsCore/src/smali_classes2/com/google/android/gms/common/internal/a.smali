.class public abstract Lcom/google/android/gms/common/internal/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field public a:I

.field private b:Ljava/lang/Object;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/a;->a:I

    .line 31
    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    iget v0, p0, Lcom/google/android/gms/common/internal/a;->a:I

    if-eq v0, v3, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 45
    iget v0, p0, Lcom/google/android/gms/common/internal/a;->a:I

    packed-switch v0, :pswitch_data_0

    .line 52
    :pswitch_0
    iput v3, p0, Lcom/google/android/gms/common/internal/a;->a:I

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/a;->a()Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/a;->b:Ljava/lang/Object;

    iget v0, p0, Lcom/google/android/gms/common/internal/a;->a:I

    const/4 v3, 0x2

    if-eq v0, v3, :cond_0

    iput v2, p0, Lcom/google/android/gms/common/internal/a;->a:I

    move v2, v1

    :cond_0
    :goto_1
    :pswitch_1
    return v2

    :cond_1
    move v0, v2

    .line 44
    goto :goto_0

    :pswitch_2
    move v2, v1

    .line 49
    goto :goto_1

    .line 45
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/a;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 70
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/internal/a;->a:I

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/common/internal/a;->b:Ljava/lang/Object;

    return-object v0
.end method

.method public final remove()V
    .locals 1

    .prologue
    .line 76
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
