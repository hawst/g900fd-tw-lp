.class public final Lcom/google/android/gms/car/jq;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field final b:Lcom/google/android/gms/common/api/v;

.field c:I

.field final d:Ljava/lang/Object;

.field e:Landroid/location/Location;

.field f:Landroid/location/Location;

.field private final g:Z

.field private final h:Lcom/google/android/gms/common/api/x;

.field private final i:Lcom/google/android/gms/common/api/y;

.field private final j:Lcom/google/android/gms/location/n;

.field private final k:Landroid/location/LocationListener;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/id;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    iput-boolean v0, p0, Lcom/google/android/gms/car/jq;->a:Z

    .line 39
    iput v0, p0, Lcom/google/android/gms/car/jq;->c:I

    .line 43
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->d:Ljava/lang/Object;

    .line 57
    new-instance v0, Lcom/google/android/gms/car/jr;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jr;-><init>(Lcom/google/android/gms/car/jq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->h:Lcom/google/android/gms/common/api/x;

    .line 81
    new-instance v0, Lcom/google/android/gms/car/js;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/js;-><init>(Lcom/google/android/gms/car/jq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->i:Lcom/google/android/gms/common/api/y;

    .line 91
    new-instance v0, Lcom/google/android/gms/car/jt;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jt;-><init>(Lcom/google/android/gms/car/jq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->j:Lcom/google/android/gms/location/n;

    .line 101
    new-instance v0, Lcom/google/android/gms/car/ju;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ju;-><init>(Lcom/google/android/gms/car/jq;)V

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->k:Landroid/location/LocationListener;

    .line 127
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/jq;->h:Lcom/google/android/gms/common/api/x;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/jq;->i:Lcom/google/android/gms/common/api/y;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/jq;->b:Lcom/google/android/gms/common/api/v;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/car/jq;->b:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 133
    invoke-virtual {p2}, Lcom/google/android/gms/car/id;->f()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/car/jq;->g:Z

    .line 143
    return-void
.end method
