.class final Lcom/google/android/gms/icing/impl/e/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field final b:Ljava/util/Map;

.field final c:Lcom/google/android/gms/icing/impl/a/x;

.field private final d:Landroid/content/pm/PackageManager;

.field private final e:Lcom/google/android/gms/icing/impl/a/f;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/x;Lcom/google/android/gms/icing/impl/a/f;)V
    .locals 1

    .prologue
    .line 144
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 145
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/e/k;->d:Landroid/content/pm/PackageManager;

    .line 146
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/e/k;->c:Lcom/google/android/gms/icing/impl/a/x;

    .line 147
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/e/k;->e:Lcom/google/android/gms/icing/impl/a/f;

    .line 148
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/k;->a:Ljava/util/Map;

    .line 149
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e/k;->b:Ljava/util/Map;

    .line 150
    return-void
.end method

.method private b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;
    .locals 2

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 156
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/k;->d:Landroid/content/pm/PackageManager;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 160
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/k;->a:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    :goto_1
    return-object v0

    .line 158
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0

    .line 163
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e/k;->a:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/PackageInfo;

    goto :goto_1
.end method


# virtual methods
.method final a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;
    .locals 2

    .prologue
    .line 182
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/e/k;->b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 183
    if-nez v0, :cond_0

    const/4 v0, 0x0

    .line 185
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e/k;->e:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(ILjava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;J)Z
    .locals 2

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/e/k;->b(Ljava/lang/String;)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_0

    iget-wide v0, v0, Landroid/content/pm/PackageInfo;->firstInstallTime:J

    cmp-long v0, v0, p2

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
