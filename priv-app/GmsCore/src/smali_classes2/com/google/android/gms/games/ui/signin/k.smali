.class public final Lcom/google/android/gms/games/ui/signin/k;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/o;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    .line 65
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x3

    return v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 51
    if-nez p1, :cond_0

    .line 53
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/k;->b(I)V

    .line 62
    :goto_0
    return-void

    .line 56
    :cond_0
    const-string v0, "ValidateAccountFragment"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Account is not allowed to use games. Status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/k;->e(I)V

    .line 58
    new-instance v0, Lcom/google/android/gms/games/ui/signin/l;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/signin/l;-><init>()V

    .line 59
    const/4 v1, 0x0

    invoke-virtual {v0, p0, v1}, Lcom/google/android/gms/games/ui/signin/l;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/k;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const-string v2, "ValidateAccountFragment"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final a(Lcom/google/android/gms/games/i/b;)V
    .locals 3

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v1, p1, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v2, Lcom/google/android/gms/games/i/c;

    invoke-direct {v2, p1, p0}, Lcom/google/android/gms/games/i/c;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/i/o;)V

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 47
    :goto_0
    return-void

    .line 46
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0xf

    return v0
.end method
