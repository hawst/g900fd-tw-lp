.class final Lcom/google/android/gms/car/il;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarSensorEvent;

.field final synthetic b:Lcom/google/android/gms/car/ij;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ij;Lcom/google/android/gms/car/CarSensorEvent;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    iput-object p2, p0, Lcom/google/android/gms/car/il;->a:Lcom/google/android/gms/car/CarSensorEvent;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/car/il;->a:Lcom/google/android/gms/car/CarSensorEvent;

    iget-object v1, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v1}, Lcom/google/android/gms/car/ij;->c(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/CarSensorEvent;->a(Landroid/location/Location;)Landroid/location/Location;

    .line 290
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 291
    const-string v0, "CAR.SENSOR"

    const-string v1, "handleGpsEvent"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 292
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "received location "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v2}, Lcom/google/android/gms/car/ij;->c(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    iget-object v1, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v1}, Lcom/google/android/gms/car/ij;->c(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ij;->a(Lcom/google/android/gms/car/ij;Landroid/location/Location;)V

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    iget-object v1, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v1}, Lcom/google/android/gms/car/ij;->d(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ij;->b(Lcom/google/android/gms/car/ij;Landroid/location/Location;)V

    .line 300
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->d(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->hasSpeed()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->d(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v0

    invoke-virtual {v0}, Landroid/location/Location;->getSpeed()F

    move-result v0

    const/high16 v1, 0x3f000000    # 0.5f

    cmpl-float v0, v0, v1

    if-lez v0, :cond_1

    .line 302
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->e(Lcom/google/android/gms/car/ij;)Ljava/util/concurrent/atomic/AtomicBoolean;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->set(Z)V

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/ij;->a(Lcom/google/android/gms/car/ij;J)J

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/car/il;->b:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->f(Lcom/google/android/gms/car/ij;)V

    .line 307
    return-void
.end method
