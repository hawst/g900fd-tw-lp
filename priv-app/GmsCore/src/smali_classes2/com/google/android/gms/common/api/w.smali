.class public final Lcom/google/android/gms/common/api/w;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Landroid/os/Looper;

.field private final d:Ljava/util/Set;

.field private e:I

.field private f:Landroid/view/View;

.field private g:Ljava/lang/String;

.field private final h:Landroid/content/Context;

.field private final i:Ljava/util/Map;

.field private j:Landroid/support/v4/app/q;

.field private k:I

.field private l:Lcom/google/android/gms/common/api/y;

.field private final m:Ljava/util/Set;

.field private final n:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 454
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 430
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    .line 438
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    .line 440
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/w;->k:I

    .line 444
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->m:Ljava/util/Set;

    .line 446
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->n:Ljava/util/Set;

    .line 455
    iput-object p1, p0, Lcom/google/android/gms/common/api/w;->h:Landroid/content/Context;

    .line 456
    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->c:Landroid/os/Looper;

    .line 457
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    .line 458
    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->g:Ljava/lang/String;

    .line 459
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 1

    .prologue
    .line 472
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    .line 473
    const-string v0, "Must provide a connected listener"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 474
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->m:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 475
    const-string v0, "Must provide a connection failed listener"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 477
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->n:Ljava/util/Set;

    invoke-interface {v0, p3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 478
    return-void
.end method

.method private b()Lcom/google/android/gms/common/internal/ClientSettings;
    .locals 7

    .prologue
    .line 698
    new-instance v0, Lcom/google/android/gms/common/internal/ClientSettings;

    iget-object v1, p0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    iget v3, p0, Lcom/google/android/gms/common/api/w;->e:I

    iget-object v4, p0, Lcom/google/android/gms/common/api/w;->f:Landroid/view/View;

    iget-object v5, p0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/common/api/w;->g:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/internal/ClientSettings;-><init>(Ljava/lang/String;Ljava/util/Collection;ILandroid/view/View;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/v;
    .locals 9

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "must call addApi() to add at least one API"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 727
    iget v0, p0, Lcom/google/android/gms/common/api/w;->k:I

    if-ltz v0, :cond_2

    .line 728
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->j:Landroid/support/v4/app/q;

    invoke-static {v0}, Lcom/google/android/gms/common/api/at;->a(Landroid/support/v4/app/q;)Lcom/google/android/gms/common/api/at;

    move-result-object v8

    iget v0, p0, Lcom/google/android/gms/common/api/w;->k:I

    invoke-virtual {v8, v0}, Lcom/google/android/gms/common/api/at;->a(I)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/z;

    iget-object v1, p0, Lcom/google/android/gms/common/api/w;->h:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/w;->c:Landroid/os/Looper;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/w;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/gms/common/api/w;->m:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gms/common/api/w;->n:Ljava/util/Set;

    iget v7, p0, Lcom/google/android/gms/common/api/w;->k:I

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/api/z;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    :cond_0
    iget v1, p0, Lcom/google/android/gms/common/api/w;->k:I

    iget-object v2, p0, Lcom/google/android/gms/common/api/w;->l:Lcom/google/android/gms/common/api/y;

    invoke-virtual {v8, v1, v0, v2}, Lcom/google/android/gms/common/api/at;->a(ILcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/y;)V

    .line 731
    :goto_1
    return-object v0

    .line 724
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 731
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/z;

    iget-object v1, p0, Lcom/google/android/gms/common/api/w;->h:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/common/api/w;->c:Landroid/os/Looper;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/w;->b()Lcom/google/android/gms/common/internal/ClientSettings;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    iget-object v5, p0, Lcom/google/android/gms/common/api/w;->m:Ljava/util/Set;

    iget-object v6, p0, Lcom/google/android/gms/common/api/w;->n:Ljava/util/Set;

    const/4 v7, -0x1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/common/api/z;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V

    goto :goto_1
.end method

.method public final a(Landroid/support/v4/app/q;ILcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;
    .locals 2

    .prologue
    .line 684
    if-ltz p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "clientId must be non-negative"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 685
    iput p2, p0, Lcom/google/android/gms/common/api/w;->k:I

    .line 686
    const-string v0, "Null activity is not permitted."

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/q;

    iput-object v0, p0, Lcom/google/android/gms/common/api/w;->j:Landroid/support/v4/app/q;

    .line 688
    iput-object p3, p0, Lcom/google/android/gms/common/api/w;->l:Lcom/google/android/gms/common/api/y;

    .line 689
    return-object p0

    .line 684
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;
    .locals 2

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 549
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;
    .locals 5

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-interface {v0, p1, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 577
    iget-object v2, p1, Lcom/google/android/gms/common/api/c;->c:Ljava/util/ArrayList;

    .line 578
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 579
    iget-object v4, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 578
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 581
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;
    .locals 5

    .prologue
    .line 592
    const-string v0, "Null options are not permitted for this Api"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 593
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->i:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 594
    iget-object v2, p1, Lcom/google/android/gms/common/api/c;->c:Ljava/util/ArrayList;

    .line 595
    const/4 v0, 0x0

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 596
    iget-object v4, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Scope;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 595
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 598
    :cond_0
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/x;)Lcom/google/android/gms/common/api/w;
    .locals 1

    .prologue
    .line 506
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 507
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/w;
    .locals 1

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/gms/common/api/w;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 527
    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lcom/google/android/gms/common/api/w;
    .locals 3

    .prologue
    .line 563
    const/4 v0, 0x0

    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_0

    .line 564
    iget-object v1, p0, Lcom/google/android/gms/common/api/w;->d:Ljava/util/Set;

    aget-object v2, p1, v0

    invoke-interface {v1, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 563
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 566
    :cond_0
    return-object p0
.end method
