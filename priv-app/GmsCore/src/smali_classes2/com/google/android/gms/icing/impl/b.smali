.class public final Lcom/google/android/gms/icing/impl/b;
.super Lcom/google/android/gms/icing/b/h;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/icing/impl/u;

.field private b:Lcom/google/android/gms/icing/impl/q;

.field private c:Lcom/google/android/gms/icing/impl/a/x;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;)V
    .locals 1

    .prologue
    .line 37
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/b/h;-><init>(I)V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    .line 39
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/b;->b:Lcom/google/android/gms/icing/impl/q;

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/b;->c:Lcom/google/android/gms/icing/impl/a/x;

    .line 41
    return-void
.end method

.method static a(Landroid/content/Context;)V
    .locals 8

    .prologue
    .line 62
    invoke-static {p0}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v1

    .line 63
    new-instance v0, Lcom/google/android/gms/gcm/an;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v2, Lcom/google/android/gms/icing/service/IcingGcmTaskService;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const-string v2, "app-history-upload"

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Z)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    sget-object v0, Lcom/google/android/gms/icing/a/a;->F:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sget-object v0, Lcom/google/android/gms/icing/a/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v2, v4, v5, v6, v7}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 64
    return-void
.end method

.method public static a(Lcom/google/android/gms/icing/impl/u;)V
    .locals 2

    .prologue
    .line 54
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->n()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/b;->a(Landroid/content/Context;)V

    .line 57
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/q;->b(Z)V

    goto :goto_0
.end method

.method private c()Landroid/support/v4/g/o;
    .locals 10

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/b;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/b;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/a/x;->h()Ljava/util/Set;

    move-result-object v1

    .line 158
    new-instance v3, Landroid/util/SparseArray;

    invoke-direct {v3}, Landroid/util/SparseArray;-><init>()V

    .line 159
    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v0

    new-array v4, v0, [J

    .line 160
    const/4 v0, 0x0

    .line 161
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 162
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/b;->c:Lcom/google/android/gms/icing/impl/a/x;

    invoke-interface {v6, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v6

    .line 163
    add-int/lit8 v0, v1, 0x1

    iget-wide v8, v6, Lcom/google/android/gms/icing/g;->i:J

    aput-wide v8, v4, v1

    .line 164
    iget v1, v6, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v3, v1, v6}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move v1, v0

    .line 165
    goto :goto_0

    .line 166
    :cond_0
    new-instance v0, Landroid/support/v4/g/o;

    invoke-direct {v0, v4, v3}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 167
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 22

    .prologue
    .line 28
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->b:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/q;->m()Lcom/google/android/gms/icing/e;

    move-result-object v18

    const/16 v17, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x0

    move v11, v2

    move v12, v3

    :goto_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->G()Lcom/google/android/gms/icing/impl/k;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/google/android/gms/icing/impl/k;->a(Ljava/lang/String;)Z

    move-result v3

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/k;->b()J

    move-result-wide v8

    invoke-virtual {v4, v8, v9}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v4

    if-nez v6, :cond_a

    sget-object v2, Lcom/google/android/gms/icing/a/a;->w:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_a

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->I()Lcom/google/android/gms/icing/c/b;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/icing/c/b;->a()Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x1

    const-wide/16 v2, 0x1

    move-wide v14, v2

    move v13, v4

    move-object/from16 v16, v5

    :goto_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/gms/icing/impl/b;->c()Landroid/support/v4/g/o;

    move-result-object v19

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/gms/icing/e;->a:J

    move-object/from16 v0, v18

    iget-wide v6, v0, Lcom/google/android/gms/icing/e;->c:J

    move-object/from16 v0, v18

    iget-wide v8, v0, Lcom/google/android/gms/icing/e;->b:J

    move-object/from16 v0, v19

    iget-object v10, v0, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    check-cast v10, [J

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JJJ[J)Lcom/google/android/gms/icing/bg;

    move-result-object v8

    iget-object v2, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v2, v2

    if-eqz v2, :cond_9

    sget-object v2, Lcom/google/android/gms/icing/a/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_6

    if-eqz v16, :cond_6

    if-eqz v13, :cond_6

    const-wide/16 v2, 0x0

    cmp-long v2, v14, v2

    if-lez v2, :cond_6

    iget-object v2, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v2, v2

    new-array v9, v2, [Lcom/google/android/gms/icing/c/a/b;

    const/4 v4, 0x0

    iget-object v10, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v13, v10

    const/4 v2, 0x0

    move v7, v2

    move v6, v11

    :goto_2
    if-ge v7, v13, :cond_1

    aget-object v11, v10, v7

    iget-object v2, v11, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    iget v2, v2, Lcom/google/android/gms/icing/ag;->d:I

    int-to-long v2, v2

    cmp-long v2, v2, v14

    if-gez v2, :cond_0

    add-int/lit8 v2, v6, 0x1

    move v3, v2

    move v2, v4

    :goto_3
    add-int/lit8 v4, v7, 0x1

    move v7, v4

    move v6, v3

    move v4, v2

    goto :goto_2

    :cond_0
    move-object/from16 v0, v19

    iget-object v2, v0, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v2, Landroid/util/SparseArray;

    iget-object v3, v11, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    iget v3, v3, Lcom/google/android/gms/icing/o;->a:I

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/g;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v3

    iget-object v5, v2, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v3, v5}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/e;->a()Landroid/support/v4/g/o;

    move-result-object v20

    add-int/lit8 v5, v4, 0x1

    move-object/from16 v0, v20

    iget-object v3, v0, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v21

    move-object/from16 v0, v20

    iget-object v3, v0, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v3, Ljava/lang/String;

    iget-object v0, v11, Lcom/google/android/gms/icing/bh;->b:Lcom/google/android/gms/icing/o;

    move-object/from16 v20, v0

    iget-object v11, v11, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    move/from16 v0, v21

    move-object/from16 v1, v20

    invoke-static {v0, v3, v2, v1, v11}, Lcom/google/android/gms/icing/impl/a/j;->a(ILjava/lang/String;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/o;Lcom/google/android/gms/icing/ag;)Lcom/google/android/gms/icing/c/a/b;

    move-result-object v2

    aput-object v2, v9, v4

    move v2, v5

    move v3, v6

    goto :goto_3

    :cond_1
    invoke-static {v9, v4}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/gms/icing/c/a/b;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->I()Lcom/google/android/gms/icing/c/b;

    move-result-object v3

    move-object/from16 v0, v16

    invoke-interface {v3, v0, v2}, Lcom/google/android/gms/icing/c/b;->a(Ljava/lang/String;[Lcom/google/android/gms/icing/c/a/b;)Z

    move-result v2

    if-nez v2, :cond_4

    const/4 v2, 0x1

    move v3, v2

    move v2, v6

    :goto_4
    if-gtz v12, :cond_2

    if-lez v2, :cond_3

    :cond_2
    const-string v4, "App history upload skipped %d %d"

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v4, v5, v2}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_3
    if-eqz v3, :cond_7

    const-string v2, "App history upload failed"

    invoke-static {v2}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/b;->a(Lcom/google/android/gms/icing/impl/u;)V

    :goto_5
    const/4 v2, 0x0

    return-object v2

    :cond_4
    move v2, v6

    move v3, v12

    :goto_6
    iget-wide v4, v8, Lcom/google/android/gms/icing/bg;->b:J

    move-object/from16 v0, v18

    iput-wide v4, v0, Lcom/google/android/gms/icing/e;->a:J

    iget-object v4, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    iget-object v5, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v5, v5

    add-int/lit8 v5, v5, -0x1

    aget-object v4, v4, v5

    iget-object v4, v4, Lcom/google/android/gms/icing/bh;->a:Lcom/google/android/gms/icing/ag;

    iget-wide v4, v4, Lcom/google/android/gms/icing/ag;->f:J

    move-object/from16 v0, v18

    iput-wide v4, v0, Lcom/google/android/gms/icing/e;->c:J

    iget-wide v4, v8, Lcom/google/android/gms/icing/bg;->c:J

    move-object/from16 v0, v18

    iput-wide v4, v0, Lcom/google/android/gms/icing/e;->b:J

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/b;->b:Lcom/google/android/gms/icing/impl/q;

    move-object/from16 v0, v18

    invoke-virtual {v4, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/e;)V

    if-eqz v8, :cond_5

    iget-object v4, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v4, v4

    const/16 v5, 0xa

    if-ge v4, v5, :cond_8

    :cond_5
    move v12, v3

    move/from16 v3, v17

    goto :goto_4

    :cond_6
    iget-object v2, v8, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v2, v2

    add-int v3, v12, v2

    move v2, v11

    goto :goto_6

    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/b;->b:Lcom/google/android/gms/icing/impl/q;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/impl/q;->b(Z)V

    goto :goto_5

    :cond_8
    move v11, v2

    move v12, v3

    goto/16 :goto_0

    :cond_9
    move v2, v11

    move/from16 v3, v17

    goto :goto_4

    :cond_a
    move-wide v14, v4

    move v13, v3

    move-object/from16 v16, v6

    goto/16 :goto_1
.end method
