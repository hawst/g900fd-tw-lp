.class final Lcom/google/android/gms/common/people/views/a;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/people/views/AudienceView;

.field private final b:Ljava/lang/String;

.field private final c:Landroid/widget/ImageView;

.field private final d:Landroid/os/ParcelFileDescriptor;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/people/views/AudienceView;Landroid/os/ParcelFileDescriptor;Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 485
    iput-object p1, p0, Lcom/google/android/gms/common/people/views/a;->a:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 486
    iput-object p2, p0, Lcom/google/android/gms/common/people/views/a;->d:Landroid/os/ParcelFileDescriptor;

    .line 487
    iput-object p3, p0, Lcom/google/android/gms/common/people/views/a;->b:Ljava/lang/String;

    .line 488
    iput-object p4, p0, Lcom/google/android/gms/common/people/views/a;->c:Landroid/widget/ImageView;

    .line 489
    return-void
.end method

.method private varargs a()Landroid/graphics/Bitmap;
    .locals 2

    .prologue
    .line 494
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->d:Landroid/os/ParcelFileDescriptor;

    invoke-static {v0}, Lcom/google/android/gms/people/af;->a(Landroid/os/ParcelFileDescriptor;)Landroid/graphics/Bitmap;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 495
    if-nez v0, :cond_0

    .line 496
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 506
    :goto_0
    const/4 v0, 0x0

    :goto_1
    return-object v0

    .line 500
    :cond_0
    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/common/util/ac;->b(Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-result-object v0

    .line 503
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/a;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0

    goto :goto_1

    :catch_0
    move-exception v1

    goto :goto_1

    .line 502
    :catchall_0
    move-exception v0

    .line 503
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/a;->d:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1}, Landroid/os/ParcelFileDescriptor;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_1

    .line 506
    :goto_2
    throw v0

    :catch_1
    move-exception v1

    goto :goto_2

    :catch_2
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 480
    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/a;->a()Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 480
    check-cast p1, Landroid/graphics/Bitmap;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->a:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-static {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Lcom/google/android/gms/common/people/views/AudienceView;)Lcom/google/android/gms/common/internal/a/a;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/a;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/common/internal/a/a;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->a:Lcom/google/android/gms/common/people/views/AudienceView;

    invoke-static {v0}, Lcom/google/android/gms/common/people/views/AudienceView;->b(Lcom/google/android/gms/common/people/views/AudienceView;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->a:Lcom/google/android/gms/common/people/views/AudienceView;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/a;->c:Landroid/widget/ImageView;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    :cond_0
    return-void
.end method
