.class final Lcom/google/android/gms/auth/trustagent/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/af;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/af;)V
    .locals 0

    .prologue
    .line 177
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private static a(Ljava/lang/String;[Landroid/accounts/Account;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 181
    array-length v2, p1

    move v1, v0

    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, p1, v1

    .line 182
    iget-object v3, v3, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v3, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 183
    const/4 v0, 0x1

    .line 186
    :cond_0
    return v0

    .line 181
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 6

    .prologue
    const/4 v5, 0x1

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/af;->d(Lcom/google/android/gms/auth/trustagent/af;)V

    .line 192
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/af;->e(Lcom/google/android/gms/auth/trustagent/af;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 193
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    .line 197
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v2}, Lcom/google/android/gms/auth/trustagent/af;->a()Lcom/google/android/gms/auth/trustagent/am;

    move-result-object v2

    .line 199
    array-length v3, v1

    if-lez v3, :cond_2

    .line 202
    const-string v3, "auth_trust_agent_pref_trusted_place_home_work_account"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 205
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-static {v3, v1}, Lcom/google/android/gms/auth/trustagent/ah;->a(Ljava/lang/String;[Landroid/accounts/Account;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 208
    const-string v1, "auth_trust_agent_bundle_trusted_place_home_work_account"

    const-string v3, "auth_trust_agent_pref_trusted_place_home_work_account"

    const-string v4, ""

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/af;->startActivity(Landroid/content/Intent;)V

    .line 247
    :goto_1
    return v5

    .line 219
    :cond_1
    const/4 v3, 0x0

    aget-object v1, v1, v3

    iget-object v1, v1, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 220
    const-string v3, "auth_trust_agent_pref_trusted_place_home_work_account"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 224
    const-string v2, "auth_trust_agent_bundle_trusted_place_home_work_account"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0

    .line 233
    :cond_2
    const-string v1, "auth_trust_agent_pref_trusted_place_home_work_account"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 236
    const-string v1, "auth_trust_agent_pref_trusted_place_home_work_account"

    invoke-virtual {v2, v1}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    goto :goto_0

    .line 243
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/af;->getActivity()Landroid/app/Activity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ah;->a:Lcom/google/android/gms/auth/trustagent/af;

    sget v2, Lcom/google/android/gms/p;->dm:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/af;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v5}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
