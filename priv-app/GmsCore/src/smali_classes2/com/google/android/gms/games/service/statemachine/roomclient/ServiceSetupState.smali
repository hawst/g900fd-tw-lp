.class final Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 34
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 41
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->g:Z

    :goto_0
    return v0

    .line 36
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 37
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->m:Lcom/google/android/gms/games/service/statemachine/roomclient/am;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    iget v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/am;->a:I

    const/4 v2, 0x2

    if-lt v0, v2, :cond_0

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b()V

    .line 39
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->f:Z

    goto :goto_0

    .line 37
    :cond_0
    const/4 v0, 0x0

    goto :goto_1

    :cond_1
    :try_start_0
    iget v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/am;->a:I

    add-int/lit8 v0, v0, 0x1

    iput v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/am;->a:I

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/ed;->a()V

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/CleanupBindingState;->b()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "Unable to load libjingle."

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 34
    :pswitch_data_0
    .packed-switch 0x1d
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 29
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->c()V

    .line 30
    return-void
.end method
