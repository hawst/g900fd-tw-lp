.class public final Lcom/google/android/gms/games/service/ai;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

.field private final b:Lcom/google/android/gms/games/service/al;


# direct methods
.method private constructor <init>()V
    .locals 4

    .prologue
    .line 94
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    new-instance v0, Lcom/google/android/gms/games/service/al;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/service/al;-><init>(Lcom/google/android/gms/games/service/ai;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/ai;->b:Lcom/google/android/gms/games/service/al;

    .line 95
    const-string v0, "RoomServiceClient"

    const-string v1, "Creating RoomServiceClient and mStateMachine"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v0, Lcom/google/android/gms/games/service/aj;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/aj;-><init>(Lcom/google/android/gms/games/service/ai;)V

    .line 139
    new-instance v1, Lcom/google/android/gms/games/service/d;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/d;-><init>()V

    .line 140
    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    iget-object v3, p0, Lcom/google/android/gms/games/service/ai;->b:Lcom/google/android/gms/games/service/al;

    invoke-direct {v2, v0, v1, v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/bz;Lcom/google/android/gms/games/service/c;Lcom/google/android/gms/games/internal/eg;)V

    iput-object v2, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->d()V

    .line 142
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Lcom/google/android/gms/games/service/ai;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/bs;)I
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->h()I

    move-result v0

    iput v0, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 167
    iget v0, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/bt;)I
    .locals 1

    .prologue
    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 172
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/bw;)I
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 229
    const/4 v0, 0x0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/y;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 194
    const/16 v0, 0xd

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 197
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/ParcelFileDescriptor;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 206
    :goto_0
    return-object v0

    .line 198
    :catch_0
    move-exception v0

    .line 199
    const-string v1, "RoomServiceClient"

    const-string v2, "During createNativeSocket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 206
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 200
    :catch_1
    move-exception v0

    .line 201
    const-string v1, "RoomServiceClient"

    const-string v2, "During createNativeSocket"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1

    .line 204
    :cond_0
    const-string v0, "RoomServiceClient"

    const-string v1, "Native sockets are not supported at this API level"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/bp;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 217
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 223
    :goto_0
    return-object v0

    .line 218
    :catch_0
    move-exception v0

    .line 219
    const-string v1, "RoomServiceClient"

    const-string v2, "During registerWaitingRoomListenerRestricted"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 223
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 220
    :catch_1
    move-exception v0

    .line 221
    const-string v1, "RoomServiceClient"

    const-string v2, "During registerWaitingRoomListenerRestricted"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/ac;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 181
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 187
    :goto_0
    return-object v0

    .line 182
    :catch_0
    move-exception v0

    .line 183
    const-string v1, "RoomServiceClient"

    const-string v2, "During createSocketConnection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 187
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 184
    :catch_1
    move-exception v0

    .line 185
    const-string v1, "RoomServiceClient"

    const-string v2, "During createSocketConnection"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 235
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;J)V
    .locals 3

    .prologue
    .line 238
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/google/android/gms/games/service/statemachine/roomclient/an;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;J)V

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 241
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/ag;)V
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 157
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/u;)V
    .locals 1

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 161
    return-void
.end method

.method protected final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 149
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "[RTMP DUMP] "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    if-eqz v0, :cond_0

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/service/ai;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Ljava/io/PrintWriter;)V

    .line 153
    :cond_0
    return-void
.end method
