.class final Lcom/google/android/gms/games/internal/bm;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 824
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 825
    iput-object p1, p0, Lcom/google/android/gms/games/internal/bm;->a:Lcom/google/android/gms/common/api/aj;

    .line 826
    return-void
.end method

.method private static P(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/quest/Quest;
    .locals 3

    .prologue
    .line 840
    new-instance v1, Lcom/google/android/gms/games/quest/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/quest/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 841
    const/4 v0, 0x0

    .line 843
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 844
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/quest/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;

    invoke-interface {v0}, Lcom/google/android/gms/games/quest/Quest;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/quest/Quest;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 847
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    .line 850
    return-object v0

    .line 847
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/quest/b;->w_()V

    throw v0
.end method


# virtual methods
.method public final K(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 3

    .prologue
    .line 830
    invoke-static {p1}, Lcom/google/android/gms/games/internal/bm;->P(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/quest/Quest;

    move-result-object v0

    .line 833
    if-eqz v0, :cond_0

    .line 834
    iget-object v1, p0, Lcom/google/android/gms/games/internal/bm;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v2, Lcom/google/android/gms/games/internal/bl;

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/internal/bl;-><init>(Lcom/google/android/gms/games/quest/Quest;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 836
    :cond_0
    return-void
.end method
