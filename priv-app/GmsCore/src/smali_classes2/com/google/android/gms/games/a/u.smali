.class final Lcom/google/android/gms/games/a/u;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/aa;


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;


# instance fields
.field private final b:Lcom/google/android/gms/games/h/a/ba;

.field private final c:Lcom/google/android/gms/games/h/a/bb;

.field private final d:Lcom/google/android/gms/games/a/y;

.field private final e:Lcom/google/android/gms/games/a/y;

.field private final f:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 90
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/u;->a:Ljava/util/concurrent/locks/ReentrantLock;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 146
    const-string v0, "EventAgent"

    sget-object v1, Lcom/google/android/gms/games/a/u;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 139
    new-instance v0, Lcom/google/android/gms/games/a/y;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/a/y;-><init>(Lcom/google/android/gms/games/a/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/u;->d:Lcom/google/android/gms/games/a/y;

    .line 140
    new-instance v0, Lcom/google/android/gms/games/a/y;

    invoke-direct {v0, p0, v2}, Lcom/google/android/gms/games/a/y;-><init>(Lcom/google/android/gms/games/a/u;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/u;->e:Lcom/google/android/gms/games/a/y;

    .line 147
    new-instance v0, Lcom/google/android/gms/games/h/a/ba;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/ba;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/u;->b:Lcom/google/android/gms/games/h/a/ba;

    .line 148
    new-instance v0, Lcom/google/android/gms/games/h/a/bb;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/bb;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/u;->c:Lcom/google/android/gms/games/h/a/bb;

    .line 149
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/u;->f:Ljava/util/Random;

    .line 150
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 861
    invoke-static {p0}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v2

    .line 862
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v3, "_id"

    const-string v4, "external_event_id=?"

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    aput-object p1, v5, v0

    const-wide/16 v6, -0x1

    invoke-static/range {v1 .. v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method protected static a()Lcom/google/android/gms/common/util/p;
    .locals 1

    .prologue
    .line 583
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/database/Cursor;)Lcom/google/android/gms/games/a/z;
    .locals 8

    .prologue
    .line 643
    new-instance v0, Lcom/google/android/gms/games/a/z;

    const/4 v1, 0x7

    invoke-interface {p0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x2

    invoke-interface {p0, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    const/4 v4, 0x3

    invoke-interface {p0, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    const/4 v6, 0x5

    invoke-interface {p0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/a/z;-><init>(Ljava/lang/String;JJJ)V

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;JJ)Lcom/google/android/gms/games/a/z;
    .locals 9

    .prologue
    .line 384
    invoke-static {p0}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v0

    .line 385
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 386
    const-string v0, "client_context_id"

    invoke-static {p3, p4}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 388
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v1, Lcom/google/android/gms/games/a/w;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v1, "external_game_id,window_start_time DESC limit 1"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v8

    .line 394
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    .line 395
    if-eqz v0, :cond_0

    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 399
    const/4 v0, 0x2

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 400
    const/4 v0, 0x3

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 410
    :goto_0
    new-instance v0, Lcom/google/android/gms/games/a/z;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-wide/16 v6, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/a/z;-><init>(Ljava/lang/String;JJJ)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 413
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    return-object v0

    .line 408
    :cond_0
    :try_start_1
    sget-object v0, Lcom/google/android/gms/games/c/a;->D:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v0

    add-long v4, p1, v0

    move-wide v2, p1

    goto :goto_0

    .line 413
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/HashMap;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 248
    invoke-static {p1}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v4

    .line 249
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "external_event_id"

    invoke-static {v0, v4, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v2

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/a/u;->d:Lcom/google/android/gms/games/a/y;

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/games/a/y;->a(Lcom/google/android/gms/games/a/y;Lcom/google/android/gms/games/a/au;Z)Z

    move-result v5

    .line 254
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v3, v1

    :goto_0
    if-ge v3, v6, :cond_0

    .line 255
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 256
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    if-eqz v5, :cond_1

    .line 258
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->f(Lcom/google/android/gms/games/a/au;)V

    .line 259
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v3, "external_event_id"

    invoke-static {v0, v4, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v2

    .line 275
    :cond_0
    :goto_1
    if-eqz p3, :cond_3

    .line 277
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v1

    :goto_2
    if-ge v3, v5, :cond_2

    .line 278
    invoke-virtual {p2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 279
    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_4

    .line 280
    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/u;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    .line 281
    const/4 v0, 0x1

    .line 277
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_2

    .line 263
    :catch_0
    move-exception v0

    .line 264
    const-string v3, "EventAgent"

    const-string v5, "Unable to refresh events"

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 265
    :catch_1
    move-exception v0

    .line 266
    const-string v3, "EventAgent"

    const-string v5, "Unable to refresh events"

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 267
    :catch_2
    move-exception v0

    .line 268
    const-string v3, "EventAgent"

    const-string v5, "Unable to refresh events"

    invoke-static {v3, v5, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 254
    :cond_1
    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_0

    .line 284
    :cond_2
    if-eqz v1, :cond_3

    .line 285
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v1, "external_event_id"

    invoke-static {v0, v4, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v0

    .line 289
    :goto_4
    return-object v0

    :cond_3
    move-object v0, v2

    goto :goto_4

    :cond_4
    move v0, v1

    goto :goto_3
.end method

.method private a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;)V
    .locals 22

    .prologue
    .line 675
    new-instance v2, Lcom/google/android/gms/common/e/b;

    move-object/from16 v0, p2

    invoke-direct {v2, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 677
    new-instance v3, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v3, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v2, v3, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v2, Lcom/google/android/gms/games/a/w;->a:[Ljava/lang/String;

    iput-object v2, v3, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v2, "external_game_id,request_id,window_start_time DESC"

    iput-object v2, v3, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v12

    .line 683
    const/4 v5, 0x0

    :try_start_0
    invoke-interface {v12}, Landroid/database/Cursor;->getCount()I

    move-result v13

    :goto_0
    if-ge v5, v13, :cond_8

    .line 685
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 686
    new-instance v14, Ljava/util/ArrayList;

    invoke-direct {v14}, Ljava/util/ArrayList;-><init>()V

    .line 687
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 688
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 689
    invoke-interface {v12, v5}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 692
    const/16 v2, 0xa

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 693
    const/16 v2, 0x9

    invoke-interface {v12, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v16

    .line 696
    invoke-static {v12}, Lcom/google/android/gms/games/a/u;->a(Landroid/database/Cursor;)Lcom/google/android/gms/games/a/z;

    move-result-object v8

    .line 698
    iget-wide v2, v8, Lcom/google/android/gms/games/a/z;->c:J

    const-wide/16 v10, 0x0

    cmp-long v2, v2, v10

    if-nez v2, :cond_1

    .line 699
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/u;->f:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v2

    move-wide v10, v2

    move v9, v5

    move-object v3, v4

    .line 705
    :goto_1
    if-ge v9, v13, :cond_5

    .line 706
    invoke-interface {v12, v9}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 707
    invoke-static {v12}, Lcom/google/android/gms/games/a/u;->a(Landroid/database/Cursor;)Lcom/google/android/gms/games/a/z;

    move-result-object v2

    .line 708
    iget-object v4, v8, Lcom/google/android/gms/games/a/z;->d:Ljava/lang/String;

    iget-object v5, v2, Lcom/google/android/gms/games/a/z;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    iget-wide v4, v8, Lcom/google/android/gms/games/a/z;->c:J

    iget-wide v0, v2, Lcom/google/android/gms/games/a/z;->c:J

    move-wide/from16 v18, v0

    cmp-long v4, v4, v18

    if-nez v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_3

    const/4 v4, 0x1

    .line 711
    :goto_3
    if-eqz v4, :cond_5

    .line 712
    invoke-virtual {v8, v2}, Lcom/google/android/gms/games/a/z;->a(Lcom/google/android/gms/games/a/z;)Z

    move-result v4

    if-nez v4, :cond_4

    const/4 v4, 0x1

    .line 717
    :goto_4
    if-eqz v4, :cond_9

    .line 718
    invoke-static {v7, v3, v8}, Lcom/google/android/gms/games/a/u;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/z;)V

    .line 719
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 722
    :goto_5
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v5, 0x0

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v5

    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v14}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-static {v8}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v8

    invoke-virtual {v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v14, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v5, Lcom/google/android/gms/games/h/a/ay;

    const/4 v8, 0x6

    invoke-interface {v12, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    const/16 v17, 0x4

    move/from16 v0, v17

    invoke-interface {v12, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-direct {v5, v8, v0}, Lcom/google/android/gms/games/h/a/ay;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-wide v0, v2, Lcom/google/android/gms/games/a/z;->c:J

    move-wide/from16 v18, v0

    const-wide/16 v20, 0x0

    cmp-long v5, v18, v20

    if-nez v5, :cond_0

    const/4 v5, 0x0

    invoke-interface {v12, v5}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v18

    move-wide/from16 v0, v18

    invoke-static {v4, v0, v1}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    const-string v5, "request_id"

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v4, v5, v8}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-static {v5}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v5

    invoke-virtual {v4, v5}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 725
    :cond_0
    add-int/lit8 v4, v9, 0x1

    move-object v8, v2

    move v9, v4

    .line 727
    goto/16 :goto_1

    .line 701
    :cond_1
    iget-wide v2, v8, Lcom/google/android/gms/games/a/z;->c:J

    move-wide v10, v2

    move v9, v5

    move-object v3, v4

    goto/16 :goto_1

    .line 708
    :cond_2
    const/4 v4, 0x0

    goto/16 :goto_2

    :cond_3
    const/4 v4, 0x0

    goto/16 :goto_3

    .line 712
    :cond_4
    const/4 v4, 0x0

    goto/16 :goto_4

    .line 729
    :cond_5
    invoke-static {v7, v3, v8}, Lcom/google/android/gms/games/a/u;->a(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/z;)V

    .line 733
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "EventAgent"

    invoke-static {v2, v6, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 736
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v15, v0, v2}, Lcom/google/android/gms/games/a/l;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v3

    .line 739
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/u;->b:Lcom/google/android/gms/games/h/a/ba;

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    new-instance v6, Lcom/google/android/gms/games/h/a/ax;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v16

    invoke-static/range {v16 .. v17}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-direct {v6, v5, v10, v7}, Lcom/google/android/gms/games/h/a/ax;-><init>(Ljava/lang/Long;Ljava/lang/Long;Ljava/util/ArrayList;)V

    const-string v5, "events"

    if-eqz v4, :cond_6

    const-string v7, "language"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v7, v4}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    :cond_6
    iget-object v2, v2, Lcom/google/android/gms/games/h/a/ba;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x1

    const-class v7, Lcom/google/android/gms/games/h/a/az;

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/az;

    .line 748
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v3

    const/16 v4, 0x8

    invoke-interface {v12, v4}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iget-object v4, v8, Lcom/google/android/gms/games/a/z;->d:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    .line 753
    move-object/from16 v0, p1

    iget-boolean v4, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v4, :cond_7

    .line 754
    iget-object v4, v8, Lcom/google/android/gms/games/a/z;->d:Ljava/lang/String;

    iput-object v4, v3, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    .line 756
    :cond_7
    invoke-virtual {v3}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v3

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/az;->getPlayerEvents()Ljava/util/ArrayList;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v14, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 759
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "EventAgent"

    invoke-static {v2, v14, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v5, v9

    .line 761
    goto/16 :goto_0

    .line 763
    :cond_8
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 764
    return-void

    .line 763
    :catchall_0
    move-exception v2

    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    throw v2

    :cond_9
    move-object v2, v8

    goto/16 :goto_5
.end method

.method private static a(Ljava/util/ArrayList;Ljava/util/ArrayList;Lcom/google/android/gms/games/a/z;)V
    .locals 4

    .prologue
    .line 773
    new-instance v0, Lcom/google/android/gms/games/h/a/au;

    iget-wide v2, p2, Lcom/google/android/gms/games/a/z;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    iget-wide v2, p2, Lcom/google/android/gms/games/a/z;->a:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/h/a/au;-><init>(Ljava/lang/Long;Ljava/lang/Long;)V

    .line 776
    new-instance v1, Lcom/google/android/gms/games/h/a/av;

    invoke-direct {v1, v0, p1}, Lcom/google/android/gms/games/h/a/av;-><init>(Lcom/google/android/gms/games/h/a/au;Ljava/util/ArrayList;)V

    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 777
    return-void
.end method

.method private b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 6

    .prologue
    .line 907
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 908
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 911
    const-string v0, "EventAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inserting a local stub for event instance for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 914
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v0

    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v2

    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    const-string v5, "definition_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v4, v5, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v2, "player_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v4, v2, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "value"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v4, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 916
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 918
    :cond_0
    return-wide v0
.end method

.method private static b(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/games/a/v;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 970
    new-instance v2, Lcom/google/android/gms/games/a/v;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/a/v;-><init>(B)V

    .line 972
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    .line 973
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/u;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    .line 978
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v6, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/google/android/gms/games/provider/u;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    const-string v7, "external_event_id"

    invoke-static {v0, v6, v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 982
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 983
    iget-object v8, v2, Lcom/google/android/gms/games/a/v;->b:Ljava/util/HashSet;

    invoke-virtual {v8, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 987
    :cond_0
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v7

    :goto_1
    if-ge v1, v7, :cond_2

    .line 988
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/as;

    .line 989
    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 990
    const-string v8, "event_definitions_game_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 991
    const-string v8, "sorting_rank"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-virtual {v0, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 992
    const-string v8, "external_event_id"

    invoke-virtual {v0, v8}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 993
    iget-object v9, v2, Lcom/google/android/gms/games/a/v;->b:Ljava/util/HashSet;

    invoke-virtual {v9, v8}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 994
    invoke-virtual {v6, v8}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_1

    .line 995
    iget-object v9, v2, Lcom/google/android/gms/games/a/v;->c:Ljava/util/ArrayList;

    invoke-virtual {v9, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 998
    :cond_1
    iget-object v8, v2, Lcom/google/android/gms/games/a/v;->a:Ljava/util/ArrayList;

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v9

    invoke-virtual {v9, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v9

    invoke-virtual {v0, v9}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 987
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 1003
    :cond_2
    return-object v2
.end method

.method private static c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 2

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/u;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 929
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v1, v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v0

    return-wide v0
.end method

.method private static c(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 12

    .prologue
    .line 1026
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 1029
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    move-object v0, v1

    .line 1073
    :goto_0
    return-object v0

    .line 1032
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v2

    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4, v2, v3}, Lcom/google/android/gms/games/provider/u;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v2

    const-string v3, "external_event_id"

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v3

    .line 1034
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v4

    .line 1036
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {p0}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v2

    const-string v6, "external_event_id"

    invoke-static {v0, v2, v6}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v6

    .line 1045
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v7

    .line 1046
    const/4 v0, 0x0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v8

    move v2, v0

    :goto_1
    if-ge v2, v8, :cond_4

    .line 1048
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cw;

    .line 1049
    iget-object v9, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    .line 1050
    const-string v0, "player_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v9, v0, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1051
    const-string v0, "external_event_id"

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 1052
    const-string v0, "external_event_id"

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1053
    const-string v0, "external_game_id"

    invoke-virtual {v9, v0}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1054
    invoke-virtual {v3, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1055
    if-nez v0, :cond_2

    .line 1056
    const-string v0, "EventAgent"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v11, "Unable to find local record for external event ID "

    invoke-direct {v9, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v0, v9}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1046
    :goto_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 1060
    :cond_2
    const-string v11, "definition_id"

    invoke-virtual {v9, v11, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1062
    invoke-virtual {v6, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1064
    if-nez v0, :cond_3

    .line 1065
    invoke-static {v7}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    .line 1070
    :goto_3
    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v9

    invoke-virtual {v0, v9}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1067
    :cond_3
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-static {v7, v10, v11}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 1068
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    goto :goto_3

    :cond_4
    move-object v0, v1

    .line 1073
    goto/16 :goto_0
.end method

.method private d(Lcom/google/android/gms/games/a/au;)I
    .locals 4

    .prologue
    const/16 v0, 0x1f4

    .line 426
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/u;->e:Lcom/google/android/gms/games/a/y;

    iget-boolean v2, p1, Lcom/google/android/gms/games/a/au;->g:Z

    invoke-static {v1, p1, v2}, Lcom/google/android/gms/games/a/y;->a(Lcom/google/android/gms/games/a/y;Lcom/google/android/gms/games/a/au;Z)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 427
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->e(Lcom/google/android/gms/games/a/au;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_1

    .line 429
    :cond_0
    const/4 v0, 0x0

    .line 441
    :cond_1
    :goto_0
    return v0

    .line 430
    :catch_0
    move-exception v1

    .line 431
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 432
    const-string v2, "EventAgent"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    goto :goto_0

    .line 435
    :catch_1
    move-exception v1

    .line 436
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 437
    const-string v2, "EventAgent"

    const-string v3, "Unable to resolve"

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    .locals 4

    .prologue
    .line 937
    invoke-static {p0, p1}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 938
    const-wide/16 v2, -0x1

    cmp-long v2, v0, v2

    if-nez v2, :cond_0

    .line 941
    const-string v0, "EventAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Inserting a local stub for event definition for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", event "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", and player "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 944
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v0

    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    const-string v3, "external_event_id"

    invoke-virtual {v2, v3, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, "event_definitions_game_id"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, v3, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v0, "name"

    const-string v1, "Unresolved event"

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "description"

    const-string v1, "Waiting to get event information from the server"

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v0, "visibility"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/u;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 946
    :cond_0
    return-wide v0
.end method

.method private e(Lcom/google/android/gms/games/a/au;)V
    .locals 12

    .prologue
    const/4 v11, 0x1

    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 563
    invoke-static {p1}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v0

    .line 564
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;)V

    .line 566
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    move-object v0, v4

    :cond_0
    iget-boolean v3, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v3, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/games/a/u;->c:Lcom/google/android/gms/games/h/a/bb;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v3

    iget-object v8, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v9, "players/%1$s/applications/%2$s/events"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v2

    invoke-static {v8}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v10, v11

    invoke-static {v9, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_1

    const-string v8, "language"

    invoke-static {v7}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v0, :cond_2

    const-string v8, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v8, v0}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v5, Lcom/google/android/gms/games/h/a/bb;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/cx;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cx;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cx;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cx;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 568
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 569
    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v3

    :goto_1
    if-ge v2, v3, :cond_6

    .line 570
    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cw;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/cw;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 569
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 566
    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/games/a/u;->b:Lcom/google/android/gms/games/h/a/ba;

    const-string v3, "events"

    if-eqz v7, :cond_4

    const-string v8, "language"

    invoke-static {v7}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz v0, :cond_5

    const-string v8, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v8, v0}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    iget-object v0, v5, Lcom/google/android/gms/games/h/a/ba;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/cx;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/cx;

    goto :goto_0

    .line 572
    :cond_6
    invoke-direct {p0, p1, v1, v11}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/HashMap;

    .line 574
    invoke-static {p1, v6}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v0

    .line 576
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "EventAgent"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 579
    iget-object v0, p0, Lcom/google/android/gms/games/a/u;->e:Lcom/google/android/gms/games/a/y;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/y;->a(Lcom/google/android/gms/games/a/au;)V

    .line 580
    return-void
.end method

.method private f(Lcom/google/android/gms/games/a/au;)V
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x0

    .line 593
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v7

    move-object v0, v4

    :cond_0
    iget-boolean v3, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v3, :cond_3

    iget-object v5, p0, Lcom/google/android/gms/games/a/u;->c:Lcom/google/android/gms/games/h/a/bb;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v8, "applications/%1$s/eventDefinitions"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v9, v2

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v7, :cond_1

    const-string v8, "language"

    invoke-static {v7}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz v0, :cond_2

    const-string v8, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v8, v0}, Lcom/google/android/gms/games/h/a/bb;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v5, Lcom/google/android/gms/games/h/a/bb;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/at;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/at;

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/at;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/at;->b()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_0

    .line 594
    invoke-static {p1, v6}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/games/a/v;

    move-result-object v1

    .line 596
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v3, v1, Lcom/google/android/gms/games/a/v;->a:Ljava/util/ArrayList;

    const-string v4, "EventAgent"

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 598
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 602
    iget-object v0, v1, Lcom/google/android/gms/games/a/v;->c:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    :goto_1
    if-ge v2, v4, :cond_6

    .line 603
    iget-object v0, v1, Lcom/google/android/gms/games/a/v;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 604
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    .line 602
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 593
    :cond_3
    iget-object v5, p0, Lcom/google/android/gms/games/a/u;->b:Lcom/google/android/gms/games/h/a/ba;

    const-string v3, "eventDefinitions"

    if-eqz v7, :cond_4

    const-string v8, "language"

    invoke-static {v7}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-static {v3, v8, v9}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_4
    if-eqz v0, :cond_5

    const-string v8, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v8, v0}, Lcom/google/android/gms/games/h/a/ba;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_5
    iget-object v0, v5, Lcom/google/android/gms/games/h/a/ba;->a:Lcom/google/android/gms/common/server/n;

    const-class v5, Lcom/google/android/gms/games/h/a/at;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/at;

    goto :goto_0

    .line 608
    :cond_6
    iget-object v0, v1, Lcom/google/android/gms/games/a/v;->b:Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 609
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 613
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 619
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v2, "EventAgent"

    invoke-static {v0, v3, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    goto :goto_2

    .line 622
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/games/a/u;->d:Lcom/google/android/gms/games/a/y;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/a/y;->a(Lcom/google/android/gms/games/a/au;)V

    .line 623
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)I
    .locals 16

    .prologue
    .line 299
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    .line 303
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 304
    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v3, v2

    :goto_0
    if-ge v3, v7, :cond_0

    .line 305
    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/internal/d/c;

    iget-object v2, v2, Lcom/google/android/gms/games/internal/d/c;->a:Ljava/lang/String;

    invoke-virtual {v6, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 304
    add-int/lit8 v2, v3, 0x1

    move v3, v2

    goto :goto_0

    .line 312
    :cond_0
    const/4 v2, 0x0

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v6, v2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;Z)Ljava/util/HashMap;
    :try_end_0
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v6

    .line 323
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v8

    .line 325
    move-object/from16 v0, p1

    invoke-static {v0, v4, v5, v8, v9}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;JJ)Lcom/google/android/gms/games/a/z;

    move-result-object v5

    .line 327
    const/4 v2, 0x0

    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v7

    move v4, v2

    :goto_1
    if-ge v4, v7, :cond_4

    .line 328
    move-object/from16 v0, p2

    invoke-virtual {v0, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/internal/d/c;

    .line 329
    iget-object v3, v2, Lcom/google/android/gms/games/internal/d/c;->a:Ljava/lang/String;

    invoke-virtual {v6, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    .line 330
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    const-wide/16 v12, -0x1

    cmp-long v10, v10, v12

    if-nez v10, :cond_2

    .line 333
    :cond_1
    const-string v3, "EventAgent"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Increment for unknown event "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/gms/games/internal/d/c;->a:Ljava/lang/String;

    invoke-virtual {v10, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 327
    :goto_2
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 314
    :catch_0
    move-exception v2

    .line 315
    const-string v3, "EventAgent"

    const-string v4, "Unable to increment events"

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 316
    const/4 v2, 0x6

    .line 374
    :goto_3
    return v2

    .line 317
    :catch_1
    move-exception v2

    .line 318
    const-string v3, "EventAgent"

    const-string v4, "Unable to increment events"

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 319
    const/4 v2, 0x6

    goto :goto_3

    .line 336
    :cond_2
    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v11, v2, Lcom/google/android/gms/games/internal/d/c;->a:Ljava/lang/String;

    invoke-static {v10, v11}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 338
    new-instance v11, Lcom/google/android/gms/common/e/b;

    invoke-direct {v11, v10}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 339
    const-string v10, "client_context_id"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v11, v10, v12}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 341
    new-instance v10, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v10, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v11, v10, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v11, Lcom/google/android/gms/games/a/w;->a:[Ljava/lang/String;

    iput-object v11, v10, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v11, "external_game_id,window_start_time DESC limit 1"

    iput-object v11, v10, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v10}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v10

    .line 348
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v11

    if-eqz v11, :cond_3

    invoke-static {v10}, Lcom/google/android/gms/games/a/u;->a(Landroid/database/Cursor;)Lcom/google/android/gms/games/a/z;

    move-result-object v11

    invoke-virtual {v5, v11}, Lcom/google/android/gms/games/a/z;->a(Lcom/google/android/gms/games/a/z;)Z

    move-result v11

    if-eqz v11, :cond_3

    .line 351
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 352
    const-string v11, "increment"

    iget v2, v2, Lcom/google/android/gms/games/internal/d/c;->b:I

    int-to-long v12, v2

    const/4 v2, 0x4

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    add-long/2addr v12, v14

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v11, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 355
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v12, 0x0

    invoke-interface {v10, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    invoke-static {v11, v12, v13}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;

    move-result-object v11

    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-virtual {v2, v11, v3, v12, v13}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 371
    :goto_4
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_2

    .line 361
    :cond_3
    :try_start_2
    new-instance v11, Landroid/content/ContentValues;

    invoke-direct {v11}, Landroid/content/ContentValues;-><init>()V

    .line 362
    const-string v12, "client_context_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 363
    const-string v12, "window_start_time"

    iget-wide v14, v5, Lcom/google/android/gms/games/a/z;->a:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 364
    const-string v12, "window_end_time"

    iget-wide v14, v5, Lcom/google/android/gms/games/a/z;->b:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v11, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 365
    const-string v12, "increment"

    iget v2, v2, Lcom/google/android/gms/games/internal/d/c;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v11, v12, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 366
    const-string v2, "instance_id"

    invoke-virtual {v11, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 367
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v2, v3, v11}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_4

    .line 371
    :catchall_0
    move-exception v2

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v2

    .line 374
    :cond_4
    const/4 v2, 0x5

    goto/16 :goto_3
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JLjava/util/ArrayList;)J
    .locals 7

    .prologue
    const-wide/16 v2, -0x1

    .line 493
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    cmp-long v0, v0, v2

    if-nez v0, :cond_1

    const/4 v0, 0x1

    .line 496
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/u;->e:Lcom/google/android/gms/games/a/y;

    invoke-static {v1, p1, v0}, Lcom/google/android/gms/games/a/y;->a(Lcom/google/android/gms/games/a/y;Lcom/google/android/gms/games/a/au;Z)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 497
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->e(Lcom/google/android/gms/games/a/au;)V

    .line 499
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_4
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-wide v0

    .line 527
    :cond_0
    :goto_1
    return-wide v0

    .line 493
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_0
    move-exception v1

    .line 510
    :cond_2
    :goto_2
    if-eqz v0, :cond_3

    .line 511
    :try_start_1
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/u;->d(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    .line 514
    :cond_3
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/a/u;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)J

    move-result-wide v0

    .line 516
    const-wide/16 v4, 0x0

    cmp-long v4, p3, v4

    if-ltz v4, :cond_0

    .line 519
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 520
    new-instance v5, Lcom/google/android/gms/games/h/a/cw;

    invoke-static {p3, p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-direct {v5, p2, v6}, Lcom/google/android/gms/games/h/a/cw;-><init>(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 521
    invoke-static {p1, v4}, Lcom/google/android/gms/games/a/u;->c(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {p5, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/google/android/gms/games/a/x; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_1

    .line 525
    :catch_1
    move-exception v0

    move-wide v0, v2

    goto :goto_1

    .line 527
    :catch_2
    move-exception v0

    move-wide v0, v2

    goto :goto_1

    .line 507
    :catch_3
    move-exception v1

    goto :goto_2

    :catch_4
    move-exception v1

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/games/a/au;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 451
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->d(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 454
    invoke-static {p1}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v1

    .line 455
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v1

    const-string v2, "sorting_rank"

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 471
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->d(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    .line 473
    invoke-static {p1}, Lcom/google/android/gms/games/provider/v;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v1

    .line 475
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 476
    const-string v1, "external_event_id"

    invoke-virtual {v2, v1, p2}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;[Ljava/lang/String;)V

    .line 477
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    const-string v2, "sorting_rank"

    iput-object v2, v1, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v0, v1, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 159
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 177
    :goto_0
    return v0

    .line 163
    :cond_0
    const/4 v1, 0x0

    .line 165
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->f(Lcom/google/android/gms/games/a/au;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 169
    const-string v2, "EventAgent"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 171
    :cond_1
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    move v0, v1

    .line 176
    goto :goto_0

    .line 172
    :catch_1
    move-exception v0

    .line 173
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 174
    const-string v2, "EventAgent"

    const-string v3, "Unable to resolve"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)I
    .locals 2

    .prologue
    .line 542
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/u;->d()V

    .line 543
    invoke-static {p1}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/games/a/au;)Landroid/net/Uri;

    move-result-object v0

    .line 544
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 545
    const/4 v0, 0x0

    .line 553
    :goto_0
    return v0

    .line 547
    :catch_0
    move-exception v0

    .line 548
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 549
    const-string v1, "EventAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 551
    :cond_0
    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    .line 188
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v1, :cond_0

    .line 206
    :goto_0
    return v0

    .line 192
    :cond_0
    const/4 v1, 0x0

    .line 194
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/u;->e(Lcom/google/android/gms/games/a/au;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/a/x; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 196
    :catch_0
    move-exception v0

    .line 197
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 198
    const-string v2, "EventAgent"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 200
    :cond_1
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    move v0, v1

    .line 205
    goto :goto_0

    .line 201
    :catch_1
    move-exception v0

    .line 202
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 203
    const-string v2, "EventAgent"

    const-string v3, "Unable to resolve"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;)I
    .locals 1

    .prologue
    .line 660
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 661
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 662
    const/4 v0, 0x0

    .line 666
    :goto_0
    return v0

    .line 664
    :catch_0
    move-exception v0

    const/4 v0, 0x6

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 631
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/w;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 633
    :try_start_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/u;->a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;)V
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 640
    :goto_0
    return-void

    .line 634
    :catch_0
    move-exception v0

    .line 635
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 636
    const-string v1, "EventAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 638
    :cond_0
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    goto :goto_0
.end method
