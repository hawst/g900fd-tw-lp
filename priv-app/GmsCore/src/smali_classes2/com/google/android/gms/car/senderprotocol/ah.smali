.class public final Lcom/google/android/gms/car/senderprotocol/ah;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final a:[Landroid/view/MotionEvent$PointerProperties;

.field private static f:Landroid/util/SparseIntArray;


# instance fields
.field private final b:Lcom/google/android/gms/car/gx;

.field private final c:Ljava/lang/Object;

.field private final d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

.field private e:J

.field private final g:Landroid/util/SparseArray;

.field private final k:Ljava/util/Queue;

.field private final l:Ljava/lang/Runnable;


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x6

    const/4 v5, 0x5

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 50
    new-array v0, v3, [Landroid/view/MotionEvent$PointerProperties;

    .line 57
    sput-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->a:[Landroid/view/MotionEvent$PointerProperties;

    new-instance v1, Landroid/view/MotionEvent$PointerProperties;

    invoke-direct {v1}, Landroid/view/MotionEvent$PointerProperties;-><init>()V

    aput-object v1, v0, v2

    .line 58
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->a:[Landroid/view/MotionEvent$PointerProperties;

    aget-object v0, v0, v2

    const/16 v1, 0x1f

    iput v1, v0, Landroid/view/MotionEvent$PointerProperties;->id:I

    .line 61
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 63
    sput-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 64
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v3, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 65
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 66
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 68
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v6, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 70
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    .line 73
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 52
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->c:Ljava/lang/Object;

    .line 53
    new-instance v0, Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    .line 191
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->g:Landroid/util/SparseArray;

    .line 192
    new-instance v0, Ljava/util/ArrayDeque;

    invoke-direct {v0}, Ljava/util/ArrayDeque;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->k:Ljava/util/Queue;

    .line 194
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/ai;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/ai;-><init>(Lcom/google/android/gms/car/senderprotocol/ah;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->l:Ljava/lang/Runnable;

    .line 74
    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    .line 75
    return-void
.end method

.method public static a(Lcom/google/android/c/b/ar;I)Landroid/view/KeyEvent;
    .locals 10

    .prologue
    const-wide/16 v2, 0x0

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 240
    and-int/lit16 v0, p1, 0x80

    if-eqz v0, :cond_0

    move v0, v4

    .line 241
    :goto_0
    new-instance v1, Landroid/view/KeyEvent;

    iget-boolean v6, p0, Lcom/google/android/c/b/ar;->b:Z

    if-eqz v6, :cond_1

    move v6, v5

    :goto_1
    iget v7, p0, Lcom/google/android/c/b/ar;->a:I

    if-eqz v0, :cond_2

    move v8, v4

    :goto_2
    iget v9, p0, Lcom/google/android/c/b/ar;->c:I

    move-wide v4, v2

    invoke-direct/range {v1 .. v9}, Landroid/view/KeyEvent;-><init>(JJIIII)V

    .line 243
    invoke-virtual {v1}, Landroid/view/KeyEvent;->getFlags()I

    move-result v0

    or-int/2addr v0, p1

    invoke-static {v1, v0}, Landroid/view/KeyEvent;->changeFlags(Landroid/view/KeyEvent;I)Landroid/view/KeyEvent;

    move-result-object v0

    .line 244
    return-object v0

    :cond_0
    move v0, v5

    .line 240
    goto :goto_0

    :cond_1
    move v6, v4

    .line 241
    goto :goto_1

    :cond_2
    move v8, v5

    goto :goto_2
.end method

.method private static a(Lcom/google/android/c/b/cw;)Lcom/google/android/gms/car/od;
    .locals 7

    .prologue
    .line 248
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ah;->f:Landroid/util/SparseIntArray;

    invoke-virtual {p0}, Lcom/google/android/c/b/cw;->b()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/c/b/cw;->a()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v2, v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/od;->a(III)Lcom/google/android/gms/car/od;

    move-result-object v1

    .line 251
    iget-object v2, p0, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v3, v2

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v3, :cond_0

    aget-object v4, v2, v0

    .line 252
    iget v5, v4, Lcom/google/android/c/b/cx;->a:I

    iget v6, v4, Lcom/google/android/c/b/cx;->b:I

    iget v4, v4, Lcom/google/android/c/b/cx;->c:I

    invoke-static {v5, v6, v4}, Lcom/google/android/gms/car/od;->b(III)Lcom/google/android/gms/car/oe;

    move-result-object v4

    .line 254
    iget-object v5, v1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 256
    :cond_0
    return-object v1
.end method

.method static synthetic a(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->c:Ljava/lang/Object;

    return-object v0
.end method

.method private a(Lcom/google/android/c/b/aj;)V
    .locals 14

    .prologue
    const-wide/16 v12, 0x7d0

    const/4 v2, 0x0

    .line 157
    iget-object v0, p1, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    if-eqz v0, :cond_4

    .line 158
    iget-object v0, p1, Lcom/google/android/c/b/aj;->c:Lcom/google/android/c/b/aq;

    iget-object v3, v0, Lcom/google/android/c/b/aq;->a:[Lcom/google/android/c/b/ar;

    array-length v4, v3

    move v1, v2

    :goto_0
    if-ge v1, v4, :cond_4

    aget-object v5, v3, v1

    .line 159
    iget-boolean v0, v5, Lcom/google/android/c/b/ar;->b:Z

    if-eqz v0, :cond_1

    invoke-virtual {v5}, Lcom/google/android/c/b/ar;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 161
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0, v5, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/c/b/ar;I)V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    const/16 v6, 0x80

    invoke-virtual {v0, v5, v6}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/c/b/ar;I)V

    .line 165
    :try_start_0
    invoke-static {v5}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/c/b/ar;->a([B)Lcom/google/android/c/b/ar;

    move-result-object v0

    .line 166
    const/4 v6, 0x0

    iput-boolean v6, v0, Lcom/google/android/c/b/ar;->b:Z

    .line 167
    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    const/4 v7, 0x0

    invoke-virtual {v6, v0, v7}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/c/b/ar;I)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 175
    :goto_1
    const-string v0, "CAR.INPUT"

    const/4 v6, 0x3

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 176
    const-string v0, "CAR.INPUT"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Injecting Key code="

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v5, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " down="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-boolean v5, v5, Lcom/google/android/c/b/ar;->b:Z

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v0, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 172
    :cond_1
    iget-boolean v0, v5, Lcom/google/android/c/b/ar;->b:Z

    if-eqz v0, :cond_3

    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/ah;->c:Ljava/lang/Object;

    monitor-enter v6

    :try_start_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v8

    add-long/2addr v8, v12

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/aj;

    invoke-direct {v0, v5, v8, v9}, Lcom/google/android/gms/car/senderprotocol/aj;-><init>(Lcom/google/android/c/b/ar;J)V

    iget-object v7, p0, Lcom/google/android/gms/car/senderprotocol/ah;->k:Ljava/util/Queue;

    invoke-interface {v7}, Ljava/util/Queue;->isEmpty()Z

    move-result v7

    iget-object v8, p0, Lcom/google/android/gms/car/senderprotocol/ah;->k:Ljava/util/Queue;

    invoke-interface {v8, v0}, Ljava/util/Queue;->offer(Ljava/lang/Object;)Z

    iget-object v8, p0, Lcom/google/android/gms/car/senderprotocol/ah;->g:Landroid/util/SparseArray;

    iget v9, v5, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v8, v9, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    if-eqz v7, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    iget-object v7, p0, Lcom/google/android/gms/car/senderprotocol/ah;->l:Ljava/lang/Runnable;

    const-wide/16 v8, 0x7d0

    sget-object v10, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v7, v8, v9, v10}, Ljava/util/concurrent/ScheduledThreadPoolExecutor;->schedule(Ljava/lang/Runnable;JLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;

    :cond_2
    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0, v5, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/c/b/ar;I)V

    goto :goto_1

    .line 172
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    :cond_3
    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/ah;->c:Ljava/lang/Object;

    monitor-enter v6

    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->g:Landroid/util/SparseArray;

    iget v7, v5, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/aj;

    iget-object v7, p0, Lcom/google/android/gms/car/senderprotocol/ah;->k:Ljava/util/Queue;

    invoke-interface {v7, v0}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->g:Landroid/util/SparseArray;

    iget v7, v5, Lcom/google/android/c/b/ar;->a:I

    invoke-virtual {v0, v7}, Landroid/util/SparseArray;->remove(I)V

    monitor-exit v6
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    goto :goto_2

    :catchall_1
    move-exception v0

    monitor-exit v6

    throw v0

    .line 180
    :cond_4
    return-void

    .line 170
    :catch_0
    move-exception v0

    goto/16 :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/Queue;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->k:Ljava/util/Queue;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/senderprotocol/ah;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/senderprotocol/ah;)Ljava/util/concurrent/ScheduledThreadPoolExecutor;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ah;->d:Ljava/util/concurrent/ScheduledThreadPoolExecutor;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 110
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->a()V

    .line 111
    return-void
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 20

    .prologue
    .line 79
    sget-object v2, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 80
    packed-switch p1, :pswitch_data_0

    .line 101
    :pswitch_0
    const-string v2, "CAR.INPUT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Received invalid message type: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p1

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    :goto_0
    sget-object v2, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 104
    return-void

    .line 84
    :pswitch_1
    new-instance v2, Lcom/google/android/c/b/ap;

    invoke-direct {v2}, Lcom/google/android/c/b/ap;-><init>()V

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/android/c/b/ap;

    .line 86
    const-string v3, "CAR.INPUT"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 87
    const-string v3, "CAR.INPUT"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Key binding response, status = "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Lcom/google/android/c/b/ap;->a:I

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 91
    :pswitch_2
    new-instance v2, Lcom/google/android/c/b/aj;

    invoke-direct {v2}, Lcom/google/android/c/b/aj;-><init>()V

    move-object/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    move-object/from16 v18, v2

    check-cast v18, Lcom/google/android/c/b/aj;

    .line 93
    if-eqz v18, :cond_0

    .line 94
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    if-eqz v2, :cond_1

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/c/b/aj;->b:Lcom/google/android/c/b/cw;

    iget-object v3, v2, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v3, v3

    if-eqz v3, :cond_1

    invoke-virtual {v2}, Lcom/google/android/c/b/cw;->a()I

    move-result v4

    if-lt v4, v3, :cond_4

    const-string v2, "CAR.INPUT"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bad InputReport. Expecting "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " got "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 95
    :cond_1
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/c/b/aj;)V

    .line 96
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/c/b/aj;->e:Lcom/google/android/c/b/cf;

    if-eqz v2, :cond_3

    const-string v3, "CAR.INPUT"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    const-string v3, "CAR.INPUT"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got new relative event keycode="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, v2, Lcom/google/android/c/b/cf;->a:[Lcom/google/android/c/b/cg;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget v5, v5, Lcom/google/android/c/b/cg;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " value="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, v2, Lcom/google/android/c/b/cf;->a:[Lcom/google/android/c/b/cg;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget v5, v5, Lcom/google/android/c/b/cg;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget-object v3, v2, Lcom/google/android/c/b/cf;->a:[Lcom/google/android/c/b/cg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/c/b/cg;->b:I

    if-nez v3, :cond_5

    const-string v2, "CAR.INPUT"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v2, "CAR.INPUT"

    const-string v3, "Ignoring zero delta relative event."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    :cond_3
    :goto_2
    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    if-eqz v2, :cond_0

    move-object/from16 v0, v18

    iget-object v2, v0, Lcom/google/android/c/b/aj;->f:Lcom/google/android/c/b/cw;

    iget-object v3, v2, Lcom/google/android/c/b/cw;->a:[Lcom/google/android/c/b/cx;

    array-length v3, v3

    if-eqz v3, :cond_0

    invoke-virtual {v2}, Lcom/google/android/c/b/cw;->a()I

    move-result v4

    if-lt v4, v3, :cond_6

    const-string v2, "CAR.INPUT"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Bad TouchPadEvent. Expecting "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " got "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 94
    :cond_4
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/c/b/cw;)Lcom/google/android/gms/car/od;

    move-result-object v2

    invoke-virtual {v3, v2}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/od;)V

    goto/16 :goto_1

    .line 96
    :cond_5
    iget-object v3, v2, Lcom/google/android/c/b/cf;->a:[Lcom/google/android/c/b/cg;

    const/4 v4, 0x0

    aget-object v3, v3, v4

    iget v3, v3, Lcom/google/android/c/b/cg;->a:I

    const/high16 v4, 0x10000

    if-ne v3, v4, :cond_3

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    move-object/from16 v19, v0

    const/4 v3, 0x1

    new-array v9, v3, [Landroid/view/MotionEvent$PointerCoords;

    const/4 v3, 0x0

    new-instance v4, Landroid/view/MotionEvent$PointerCoords;

    invoke-direct {v4}, Landroid/view/MotionEvent$PointerCoords;-><init>()V

    aput-object v4, v9, v3

    const/4 v3, 0x0

    aget-object v3, v9, v3

    const/16 v4, 0x9

    iget-object v2, v2, Lcom/google/android/c/b/cf;->a:[Lcom/google/android/c/b/cg;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    iget v2, v2, Lcom/google/android/c/b/cg;->b:I

    int-to-float v2, v2

    invoke-virtual {v3, v4, v2}, Landroid/view/MotionEvent$PointerCoords;->setAxisValue(IF)V

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v2

    const/16 v6, 0x8

    const/4 v7, 0x1

    sget-object v8, Lcom/google/android/gms/car/senderprotocol/ah;->a:[Landroid/view/MotionEvent$PointerProperties;

    const/4 v10, 0x0

    const/4 v11, 0x0

    const/high16 v12, 0x3f800000    # 1.0f

    const/high16 v13, 0x3f800000    # 1.0f

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x2002

    const/16 v17, 0x0

    move-wide v4, v2

    invoke-static/range {v2 .. v17}, Landroid/view/MotionEvent;->obtain(JJII[Landroid/view/MotionEvent$PointerProperties;[Landroid/view/MotionEvent$PointerCoords;IIFFIIII)Landroid/view/MotionEvent;

    move-result-object v2

    move-object/from16 v0, v19

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/gx;->a(Landroid/view/MotionEvent;)V

    goto/16 :goto_2

    .line 97
    :cond_6
    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/ah;->a(Lcom/google/android/c/b/cw;)Lcom/google/android/gms/car/od;

    move-result-object v7

    iget v2, v7, Lcom/google/android/gms/car/od;->b:I

    shl-int/lit8 v3, v4, 0x8

    or-int/2addr v2, v3

    iput v2, v7, Lcom/google/android/gms/car/od;->b:I

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget v2, v7, Lcom/google/android/gms/car/od;->b:I

    if-nez v2, :cond_7

    move-object/from16 v0, p0

    iput-wide v4, v0, Lcom/google/android/gms/car/senderprotocol/ah;->e:J

    :cond_7
    move-object/from16 v0, p0

    iget-wide v2, v0, Lcom/google/android/gms/car/senderprotocol/ah;->e:J

    iget v6, v7, Lcom/google/android/gms/car/od;->b:I

    iget-object v7, v7, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    const/4 v8, 0x0

    const/4 v9, 0x0

    const v10, 0x100008

    const/4 v11, 0x3

    invoke-static/range {v2 .. v11}, Lcom/google/android/gms/car/od;->a(JJILjava/util/ArrayList;IIII)Landroid/view/MotionEvent;

    move-result-object v2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/car/senderprotocol/ah;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v3, v2}, Lcom/google/android/gms/car/gx;->b(Landroid/view/MotionEvent;)V

    goto/16 :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x8001
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a([I)V
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/c/b/ao;

    invoke-direct {v0}, Lcom/google/android/c/b/ao;-><init>()V

    .line 115
    iput-object p1, v0, Lcom/google/android/c/b/ao;->a:[I

    .line 116
    const v1, 0x8002

    invoke-static {v0}, Lcom/google/android/c/b/ao;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/ah;->a(I[B)V

    .line 118
    return-void
.end method
