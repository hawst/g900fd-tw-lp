.class final Lcom/google/android/gms/car/hg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Ljava/lang/String;

.field final synthetic c:Lcom/google/android/gms/car/gx;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/gx;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 262
    iput-object p1, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    iput p2, p0, Lcom/google/android/gms/car/hg;->a:I

    iput-object p3, p0, Lcom/google/android/gms/car/hg;->b:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 265
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->c(Lcom/google/android/gms/car/gx;)I

    move-result v2

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/gms/car/hg;->a:I

    const/16 v3, 0xa

    if-ne v2, v3, :cond_1

    .line 268
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ignore critical error in emulator:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/hg;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_0
    :goto_0
    return-void

    .line 273
    :cond_1
    const-string v2, "CAR.SERVICE"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Critical error "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/car/hg;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " msg: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/car/hg;->b:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->d(Lcom/google/android/gms/car/gx;)Z

    move-result v2

    .line 277
    if-eqz v2, :cond_0

    .line 280
    iget v2, p0, Lcom/google/android/gms/car/hg;->a:I

    if-lez v2, :cond_2

    const/16 v3, 0x2710

    if-ge v2, v3, :cond_2

    move v2, v1

    :goto_1
    if-eqz v2, :cond_0

    .line 281
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->e(Lcom/google/android/gms/car/gx;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 282
    iget-object v0, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->f(Lcom/google/android/gms/car/gx;)I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/car/hg;->a:I

    if-eq v0, v1, :cond_0

    .line 283
    const-string v0, "CAR.SERVICE"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 284
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "got new critical error:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/hg;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while handling old one:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->f(Lcom/google/android/gms/car/gx;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    move v2, v0

    .line 280
    goto :goto_1

    .line 290
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->g(Lcom/google/android/gms/car/gx;)Z

    .line 291
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    iget v3, p0, Lcom/google/android/gms/car/hg;->a:I

    invoke-static {v2, v3}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;I)I

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->c(Lcom/google/android/gms/car/gx;)I

    move-result v2

    if-nez v2, :cond_4

    .line 294
    iget-object v0, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->B()V

    goto/16 :goto_0

    .line 299
    :cond_4
    iget v2, p0, Lcom/google/android/gms/car/hg;->a:I

    packed-switch v2, :pswitch_data_0

    :pswitch_0
    move v0, v1

    :pswitch_1
    if-eqz v0, :cond_5

    .line 300
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v1}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/car/CarErrorDisplayActivity;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 301
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 302
    iget-object v1, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-static {v1}, Lcom/google/android/gms/car/gx;->h(Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/CarService;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/CarService;->startActivity(Landroid/content/Intent;)V

    .line 304
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/hg;->c:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->B()V

    goto/16 :goto_0

    .line 299
    nop

    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
