.class public abstract Lcom/google/android/gms/common/internal/service/c;
.super Landroid/content/ContextWrapper;
.source "SourceFile"


# instance fields
.field private mProxy:Lcom/google/android/gms/common/internal/service/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 18
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/content/ContextWrapper;-><init>(Landroid/content/Context;)V

    .line 19
    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 87
    return-void
.end method

.method public final getApplication()Landroid/app/Application;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/service/d;->getApplication()Landroid/app/Application;

    move-result-object v0

    return-object v0
.end method

.method public abstract onBind(Landroid/content/Intent;)Landroid/os/IBinder;
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 0

    .prologue
    .line 45
    return-void
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 31
    return-void
.end method

.method public onDestroy()V
    .locals 0

    .prologue
    .line 42
    return-void
.end method

.method public onLowMemory()V
    .locals 0

    .prologue
    .line 48
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 60
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 37
    invoke-virtual {p0, p1, p3}, Lcom/google/android/gms/common/internal/service/c;->onStart(Landroid/content/Intent;I)V

    .line 38
    const/4 v0, 0x0

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 63
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 0

    .prologue
    .line 51
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method final setProxy(Lcom/google/android/gms/common/internal/service/d;)V
    .locals 0

    .prologue
    .line 22
    iput-object p1, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/internal/service/c;->attachBaseContext(Landroid/content/Context;)V

    .line 24
    return-void
.end method

.method public final startForeground(ILandroid/app/Notification;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/common/internal/service/d;->startForeground(ILandroid/app/Notification;)V

    .line 79
    return-void
.end method

.method public final stopForeground(Z)V
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/d;->stopForeground(Z)V

    .line 83
    return-void
.end method

.method public final stopSelf()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/service/d;->stopSelf()V

    .line 67
    return-void
.end method

.method public final stopSelf(I)V
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/d;->stopSelf(I)V

    .line 71
    return-void
.end method

.method public final stopSelfResult(I)Z
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/c;->mProxy:Lcom/google/android/gms/common/internal/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/d;->stopSelfResult(I)Z

    move-result v0

    return v0
.end method
