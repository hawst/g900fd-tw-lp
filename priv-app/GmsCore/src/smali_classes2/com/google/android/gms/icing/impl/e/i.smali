.class public final Lcom/google/android/gms/icing/impl/e/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/icing/impl/e/k;


# direct methods
.method public constructor <init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/x;Lcom/google/android/gms/icing/impl/a/f;)V
    .locals 1

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/icing/impl/e/k;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/e/k;-><init>(Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/x;Lcom/google/android/gms/icing/impl/a/f;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/e/i;-><init>(Lcom/google/android/gms/icing/impl/e/k;)V

    .line 56
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/icing/impl/e/k;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/e/i;->a:Lcom/google/android/gms/icing/impl/e/k;

    .line 60
    return-void
.end method

.method public static a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/bf;)Lcom/google/android/gms/icing/ag;
    .locals 6

    .prologue
    .line 268
    new-instance v0, Lcom/google/android/gms/icing/ag;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ag;-><init>()V

    .line 269
    iget-wide v2, p0, Lcom/google/android/gms/icing/g;->i:J

    iput-wide v2, v0, Lcom/google/android/gms/icing/ag;->a:J

    .line 270
    iget-object v1, p1, Lcom/google/android/gms/icing/bf;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/icing/ag;->b:Ljava/lang/String;

    .line 271
    iget-wide v2, p1, Lcom/google/android/gms/icing/bf;->f:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    long-to-int v1, v2

    iput v1, v0, Lcom/google/android/gms/icing/ag;->d:I

    .line 272
    iget v1, p1, Lcom/google/android/gms/icing/bf;->g:I

    iput v1, v0, Lcom/google/android/gms/icing/ag;->e:I

    .line 273
    iget v1, p1, Lcom/google/android/gms/icing/bf;->i:I

    iput v1, v0, Lcom/google/android/gms/icing/ag;->g:I

    .line 274
    return-object v0
.end method
