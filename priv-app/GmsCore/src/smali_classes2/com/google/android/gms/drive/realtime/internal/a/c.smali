.class public final Lcom/google/android/gms/drive/realtime/internal/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/realtime/internal/a/d;


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/realtime/internal/a/b;

.field private final b:Ljava/util/List;

.field private c:I


# direct methods
.method private constructor <init>(Lcom/google/android/gms/drive/realtime/internal/a/b;)V
    .locals 1

    .prologue
    .line 158
    iput-object p1, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->a:Lcom/google/android/gms/drive/realtime/internal/a/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    .line 160
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->c:I

    return-void
.end method

.method public synthetic constructor <init>(Lcom/google/android/gms/drive/realtime/internal/a/b;B)V
    .locals 0

    .prologue
    .line 158
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/realtime/internal/a/c;-><init>(Lcom/google/android/gms/drive/realtime/internal/a/b;)V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->c:I

    return v0
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 165
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->a:Lcom/google/android/gms/drive/realtime/internal/a/b;

    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    iget v2, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->c:I

    add-int/lit8 v3, v2, 0x1

    iput v3, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->c:I

    invoke-static {v0, v1, p1, v2}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a(Lcom/google/android/gms/drive/realtime/internal/a/b;Ljava/util/List;Ljava/lang/Object;I)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 172
    return-void

    .line 166
    :catch_0
    move-exception v0

    move-object v1, v0

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/database/CursorWindow;

    .line 168
    invoke-virtual {v0}, Landroid/database/CursorWindow;->close()V

    goto :goto_0

    .line 170
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 171
    throw v1
.end method

.method public final b()Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 182
    iget-object v1, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->a:Lcom/google/android/gms/drive/realtime/internal/a/b;

    iget-object v0, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/gms/drive/realtime/internal/a/c;->b:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Landroid/database/CursorWindow;

    invoke-interface {v0, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Landroid/database/CursorWindow;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/realtime/internal/a/b;->a([Landroid/database/CursorWindow;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
