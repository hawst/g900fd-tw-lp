.class public final Lcom/google/android/gms/car/on;
.super Lcom/google/android/gms/car/ov;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/iv;
.implements Lcom/google/android/gms/car/ny;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final C:Ljava/lang/Byte;

.field private static final D:Ljava/lang/Byte;

.field private static final E:Ljava/lang/Byte;

.field private static final F:Ljava/lang/Byte;

.field private static L:I

.field private static R:Landroid/util/SparseIntArray;

.field private static final c:Lcom/google/android/gms/car/ii;


# instance fields
.field private volatile A:J

.field private B:J

.field private final G:Lcom/google/android/gms/car/pe;

.field private H:I

.field private I:I

.field private J:I

.field private K:I

.field private final M:[I

.field private final N:Lcom/google/android/gms/car/ip;

.field private O:I

.field private final P:Landroid/util/SparseLongArray;

.field private final Q:Landroid/util/SparseArray;

.field private final S:Ljava/lang/Object;

.field private final T:Landroid/util/SparseArray;

.field private final U:Lcom/google/android/gms/car/qo;

.field private V:I

.field private W:Landroid/graphics/SurfaceTexture;

.field private X:Lcom/google/android/gms/car/as;

.field a:[Lcom/google/android/gms/car/ot;

.field b:Landroid/util/SparseArray;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/car/gx;

.field private f:Landroid/content/BroadcastReceiver;

.field private g:Ljava/lang/Thread;

.field private volatile h:Landroid/view/Surface;

.field private i:Ljava/util/concurrent/Semaphore;

.field private j:Landroid/os/Looper;

.field private volatile k:Lcom/google/android/gms/car/oq;

.field private l:Lcom/google/android/gms/car/ix;

.field private volatile m:Lcom/google/android/gms/car/iy;

.field private volatile n:Lcom/google/android/gms/car/ou;

.field private o:Z

.field private p:I

.field private volatile q:Z

.field private volatile r:Z

.field private s:I

.field private t:I

.field private u:I

.field private final v:[F

.field private final w:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final x:Ljava/util/concurrent/atomic/AtomicInteger;

.field private volatile y:Z

.field private volatile z:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x1

    const/4 v4, 0x6

    const/4 v3, 0x4

    const/4 v2, 0x2

    .line 66
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.enable_frames_log"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    .line 178
    invoke-static {v5}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/on;->C:Ljava/lang/Byte;

    .line 179
    invoke-static {v2}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/on;->D:Ljava/lang/Byte;

    .line 180
    invoke-static {v6}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/on;->E:Ljava/lang/Byte;

    .line 181
    invoke-static {v3}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/on;->F:Ljava/lang/Byte;

    .line 197
    sput v4, Lcom/google/android/gms/car/on;->L:I

    .line 211
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    .line 215
    sput-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 216
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 217
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v6, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 218
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 219
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    const/16 v1, 0xc

    invoke-virtual {v0, v4, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 220
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 221
    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    const/4 v1, 0x7

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 222
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/ip;Lcom/google/android/gms/car/gx;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 243
    invoke-direct {p0}, Lcom/google/android/gms/car/ov;-><init>()V

    .line 141
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->o:Z

    .line 144
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->q:Z

    .line 146
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->r:Z

    .line 149
    iput v2, p0, Lcom/google/android/gms/car/on;->s:I

    .line 151
    iput v2, p0, Lcom/google/android/gms/car/on;->t:I

    .line 153
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->u:I

    .line 155
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/google/android/gms/car/ot;

    iput-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    .line 156
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/car/on;->v:[F

    .line 160
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->w:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 162
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 164
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->y:Z

    .line 165
    iput v2, p0, Lcom/google/android/gms/car/on;->z:I

    .line 173
    const-wide/16 v0, 0xe

    iput-wide v0, p0, Lcom/google/android/gms/car/on;->A:J

    .line 193
    iput v2, p0, Lcom/google/android/gms/car/on;->H:I

    .line 194
    iput v2, p0, Lcom/google/android/gms/car/on;->I:I

    .line 195
    iput v2, p0, Lcom/google/android/gms/car/on;->J:I

    .line 196
    iput v2, p0, Lcom/google/android/gms/car/on;->K:I

    .line 198
    sget v0, Lcom/google/android/gms/car/on;->L:I

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/car/on;->M:[I

    .line 203
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->O:I

    .line 206
    new-instance v0, Landroid/util/SparseLongArray;

    invoke-direct {v0}, Landroid/util/SparseLongArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->P:Landroid/util/SparseLongArray;

    .line 208
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->Q:Landroid/util/SparseArray;

    .line 225
    new-instance v0, Landroid/util/SparseArray;

    const/16 v1, 0xf

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    .line 228
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    .line 230
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    .line 238
    iput v2, p0, Lcom/google/android/gms/car/on;->V:I

    .line 244
    iput-object p1, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    .line 245
    iput-object p2, p0, Lcom/google/android/gms/car/on;->N:Lcom/google/android/gms/car/ip;

    .line 246
    iput-object p3, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    .line 247
    new-instance v0, Lcom/google/android/gms/car/qo;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/qo;-><init>(Lcom/google/android/gms/car/on;)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    .line 248
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    .line 249
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->G:Lcom/google/android/gms/car/pe;

    .line 253
    return-void
.end method

.method private static a(ILjava/lang/String;)I
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1197
    invoke-static {p0}, Landroid/opengl/GLES20;->glCreateShader(I)I

    move-result v1

    .line 1198
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "glCreateShader type="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1199
    invoke-static {v1, p1}, Landroid/opengl/GLES20;->glShaderSource(ILjava/lang/String;)V

    .line 1200
    invoke-static {v1}, Landroid/opengl/GLES20;->glCompileShader(I)V

    .line 1201
    const/4 v2, 0x1

    new-array v2, v2, [I

    .line 1202
    const v3, 0x8b81

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetShaderiv(II[II)V

    .line 1203
    aget v2, v2, v0

    if-nez v2, :cond_0

    .line 1204
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not compile shader "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetShaderInfoLog(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1206
    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteShader(I)V

    .line 1209
    :goto_0
    return v0

    :cond_0
    move v0, v1

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/car/oi;JI)J
    .locals 2

    .prologue
    .line 784
    .line 785
    if-nez p4, :cond_0

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/car/on;->P:Landroid/util/SparseLongArray;

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v1

    invoke-virtual {v0, v1, p2, p3}, Landroid/util/SparseLongArray;->put(IJ)V

    .line 790
    :goto_0
    return-wide p2

    .line 788
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->P:Landroid/util/SparseLongArray;

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseLongArray;->get(I)J

    move-result-wide p2

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;Landroid/os/Looper;)Landroid/os/Looper;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    return-object p1
.end method

.method private a(II)Lcom/google/android/gms/car/oi;
    .locals 4

    .prologue
    .line 803
    iget-object v2, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v2

    .line 804
    const/4 v0, 0x6

    move v1, v0

    .line 805
    :goto_0
    if-lez v1, :cond_1

    .line 806
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 807
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->d()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/oi;->a(II)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 808
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 812
    :goto_1
    return-object v0

    .line 805
    :cond_0
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 811
    :cond_1
    monitor-exit v2

    .line 812
    const/4 v0, 0x0

    goto :goto_1

    .line 811
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private a(III)Lcom/google/android/gms/car/oi;
    .locals 2

    .prologue
    .line 794
    iget-object v0, p0, Lcom/google/android/gms/car/on;->Q:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 795
    if-nez v0, :cond_0

    .line 796
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/car/on;->a(II)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 797
    iget-object v1, p0, Lcom/google/android/gms/car/on;->Q:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 799
    :cond_0
    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oq;)Lcom/google/android/gms/car/oq;
    .locals 0

    .prologue
    .line 55
    iput-object p1, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    return-object p1
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/car/ot;
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, -0x1

    const/4 v0, 0x0

    .line 1322
    const v1, 0x8b31

    invoke-static {v1, p1}, Lcom/google/android/gms/car/on;->a(ILjava/lang/String;)I

    move-result v2

    if-nez v2, :cond_0

    move v1, v0

    .line 1323
    :goto_0
    const-string v0, "aPosition"

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v4

    .line 1324
    const-string v0, "glGetAttribLocation aPosition"

    invoke-static {v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1325
    if-ne v4, v6, :cond_3

    .line 1326
    new-instance v0, Lcom/google/android/gms/car/ow;

    const-string v1, "Could not get attrib location for aPosition"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ow;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1322
    :cond_0
    const v1, 0x8b30

    invoke-static {v1, p2}, Lcom/google/android/gms/car/on;->a(ILjava/lang/String;)I

    move-result v3

    if-nez v3, :cond_1

    move v1, v0

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/opengl/GLES20;->glCreateProgram()I

    move-result v1

    const-string v4, "glCreateProgram"

    invoke-static {v4}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    if-nez v1, :cond_2

    const-string v4, "CAR.WM"

    const-string v5, "Could not create program"

    invoke-static {v4, v5}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-static {v1, v2}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "glAttachShader"

    invoke-static {v2}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    invoke-static {v1, v3}, Landroid/opengl/GLES20;->glAttachShader(II)V

    const-string v2, "glAttachShader"

    invoke-static {v2}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glLinkProgram(I)V

    new-array v2, v7, [I

    const v3, 0x8b82

    invoke-static {v1, v3, v2, v0}, Landroid/opengl/GLES20;->glGetProgramiv(II[II)V

    aget v2, v2, v0

    if-eq v2, v7, :cond_7

    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not link program: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Landroid/opengl/GLES20;->glGetProgramInfoLog(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-static {v1}, Landroid/opengl/GLES20;->glDeleteProgram(I)V

    :goto_1
    move v1, v0

    goto :goto_0

    .line 1328
    :cond_3
    const-string v0, "aTextureCoord"

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetAttribLocation(ILjava/lang/String;)I

    move-result v5

    .line 1329
    const-string v0, "glGetAttribLocation aTextureCoord"

    invoke-static {v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1330
    if-ne v5, v6, :cond_4

    .line 1331
    new-instance v0, Lcom/google/android/gms/car/ow;

    const-string v1, "Could not get attrib location for aTextureCoord"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ow;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1333
    :cond_4
    const-string v0, "uMVPMatrix"

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v2

    .line 1334
    const-string v0, "glGetUniformLocation uMVPMatrix"

    invoke-static {v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1335
    if-ne v2, v6, :cond_5

    .line 1336
    new-instance v0, Lcom/google/android/gms/car/ow;

    const-string v1, "Could not get attrib location for uMVPMatrix"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ow;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1338
    :cond_5
    const-string v0, "uSTMatrix"

    invoke-static {v1, v0}, Landroid/opengl/GLES20;->glGetUniformLocation(ILjava/lang/String;)I

    move-result v3

    .line 1339
    const-string v0, "glGetUniformLocation uSTMatrix"

    invoke-static {v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1340
    if-ne v3, v6, :cond_6

    .line 1341
    new-instance v0, Lcom/google/android/gms/car/ow;

    const-string v1, "Could not get attrib location for uSTMatrix"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ow;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1344
    :cond_6
    new-instance v0, Lcom/google/android/gms/car/ot;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/ot;-><init>(IIIII)V

    return-object v0

    :cond_7
    move v0, v1

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;)Lcom/google/android/gms/car/ou;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/car/oi;ILcom/google/android/gms/car/oq;)V
    .locals 5

    .prologue
    .line 556
    const/4 v1, 0x0

    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v0, Lcom/google/android/gms/car/ou;->c:I

    sget-object v0, Lcom/google/android/gms/car/on;->R:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p2}, Landroid/util/SparseIntArray;->get(I)I

    move-result v3

    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "no layout specified for window layer "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->h:I

    :goto_0
    invoke-virtual {p1, v1, v2, v0, v3}, Lcom/google/android/gms/car/oi;->a(IIII)V

    .line 557
    invoke-virtual {p3, p1}, Lcom/google/android/gms/car/oq;->a(Lcom/google/android/gms/car/oi;)V

    .line 558
    return-void

    .line 556
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v0, Lcom/google/android/gms/car/ou;->h:I

    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->d:I

    iget-object v4, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v4, v4, Lcom/google/android/gms/car/ou;->h:I

    sub-int/2addr v0, v4

    goto :goto_0

    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->d:I

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->i:I

    sub-int v1, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->i:I

    goto :goto_0

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->d:I

    goto :goto_0

    :pswitch_4
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->j:I

    goto :goto_0

    :pswitch_5
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->d:I

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_2
        :pswitch_1
        :pswitch_5
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;I)V
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/iy;->a(I)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/car/on;->m()V

    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V
    .locals 5

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->g()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-nez v0, :cond_4

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    move-object v1, v0

    :goto_0
    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.WM"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "window init window already in list "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " for z "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    rem-int/lit8 v0, v2, 0x2

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    add-int/lit8 v3, v2, -0x1

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->k()V

    const-string v0, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "doWindowInit active windows "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " in z "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " added "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    return-void

    :cond_3
    invoke-virtual {v1, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    :cond_4
    move-object v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/ox;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->n()V

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->b:I

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->a:I

    mul-int/2addr v1, v2

    invoke-static {v1}, Ljava/nio/IntBuffer;->allocate(I)Ljava/nio/IntBuffer;

    move-result-object v6

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v1, Lcom/google/android/gms/car/ou;->a:I

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v3, v1, Lcom/google/android/gms/car/ou;->b:I

    const/16 v4, 0x1908

    const/16 v5, 0x1401

    move v1, v0

    invoke-static/range {v0 .. v6}, Landroid/opengl/GLES20;->glReadPixels(IIIIIILjava/nio/Buffer;)V

    new-instance v0, Lcom/google/android/gms/car/oo;

    invoke-direct {v0, p0, p1, v6}, Lcom/google/android/gms/car/oo;-><init>(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/ox;Ljava/nio/IntBuffer;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    return-void
.end method

.method private static a(Ljava/lang/String;Ljava/lang/String;[F)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 1379
    invoke-static {v3, p0, p1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 1380
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x4

    if-ge v0, v1, :cond_0

    .line 1381
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    aget v2, p2, v0

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v0, 0x4

    aget v2, p2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v0, 0x8

    aget v2, p2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v2, v0, 0xc

    aget v2, p2, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, p0, v1}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 1380
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1387
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/on;Z)Z
    .locals 0

    .prologue
    .line 55
    iput-boolean p1, p0, Lcom/google/android/gms/car/on;->r:Z

    return p1
.end method

.method private b(II)V
    .locals 5

    .prologue
    const v4, 0x812f

    const/high16 v1, 0x46180000    # 9728.0f

    const/4 v3, 0x0

    const/16 v2, 0xde1

    .line 1348
    invoke-static {v2, p1}, Landroid/opengl/GLES20;->glBindTexture(II)V

    .line 1349
    const/16 v0, 0x2801

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1351
    const/16 v0, 0x2800

    invoke-static {v2, v0, v1}, Landroid/opengl/GLES20;->glTexParameterf(IIF)V

    .line 1353
    const/16 v0, 0x2802

    invoke-static {v2, v0, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1355
    const/16 v0, 0x2803

    invoke-static {v2, v0, v4}, Landroid/opengl/GLES20;->glTexParameteri(III)V

    .line 1358
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 1359
    iput-boolean v3, v0, Landroid/graphics/BitmapFactory$Options;->inPremultiplied:Z

    .line 1360
    iget-object v1, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1, p2, v0}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1362
    invoke-static {v2, v3, v0, v3}, Landroid/opengl/GLUtils;->texImage2D(IILandroid/graphics/Bitmap;I)V

    .line 1363
    const-string v1, "texImage2D shadow texture"

    invoke-static {v1}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1364
    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 1365
    return-void
.end method

.method private b(Lcom/google/android/gms/car/od;)V
    .locals 9

    .prologue
    .line 820
    iget v6, p1, Lcom/google/android/gms/car/od;->b:I

    .line 823
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 825
    iget-object v0, p1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oe;

    .line 826
    iget v1, v0, Lcom/google/android/gms/car/oe;->c:I

    iget v4, v0, Lcom/google/android/gms/car/oe;->a:I

    iget v5, v0, Lcom/google/android/gms/car/oe;->b:I

    invoke-direct {p0, v1, v4, v5}, Lcom/google/android/gms/car/on;->a(III)Lcom/google/android/gms/car/oi;

    move-result-object v1

    .line 827
    if-nez v1, :cond_1

    .line 845
    :cond_0
    return-void

    .line 830
    :cond_1
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-nez v4, :cond_2

    .line 831
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {v2, v1, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 833
    :cond_2
    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 837
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 839
    invoke-virtual {v2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_1
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 840
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/oi;

    .line 841
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/util/ArrayList;

    .line 842
    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;JI)J

    move-result-wide v2

    .line 843
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/car/oi;->a(JJILjava/util/ArrayList;)Z

    goto :goto_1
.end method

.method static synthetic b(Lcom/google/android/gms/car/on;)V
    .locals 14

    .prologue
    .line 55
    new-instance v0, Lcom/google/android/gms/car/ix;

    iget-object v1, p0, Lcom/google/android/gms/car/on;->h:Landroid/view/Surface;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ix;-><init>(Landroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->l:Lcom/google/android/gms/car/ix;

    iget-object v0, p0, Lcom/google/android/gms/car/on;->l:Lcom/google/android/gms/car/ix;

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, Lcom/google/android/gms/car/ix;->c:Landroid/opengl/EGLSurface;

    iget-object v3, v0, Lcom/google/android/gms/car/ix;->c:Landroid/opengl/EGLSurface;

    iget-object v0, v0, Lcom/google/android/gms/car/ix;->b:Landroid/opengl/EGLContext;

    invoke-static {v1, v2, v3, v0}, Landroid/opengl/EGL14;->eglMakeCurrent(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;Landroid/opengl/EGLSurface;Landroid/opengl/EGLContext;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "eglMakeCurrent failed"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->u:I

    iget-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    const/4 v1, 0x0

    const-string v2, "precision highp float;\nuniform mat4 uMVPMatrix;\nuniform mat4 uSTMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n}\n"

    const-string v3, "#extension GL_OES_EGL_image_external : require\nprecision highp float;\nvarying vec2 vTextureCoord;\nuniform samplerExternalOES sTexture;\nvoid main() {\n  gl_FragColor = texture2D(sTexture, vTextureCoord);\n}\n"

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/car/ot;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    const/4 v1, 0x1

    const-string v2, "precision highp float;\nuniform mat4 uMVPMatrix;\nuniform mat4 uSTMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n}\n"

    const-string v3, "precision highp float;\nvarying vec2 vTextureCoord;\nuniform sampler2D sTexture;\nvoid main() {\n  vec4 color = texture2D(sTexture, vTextureCoord);\n  gl_FragColor = color;}\n"

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/car/ot;

    move-result-object v2

    aput-object v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    const/4 v1, 0x2

    const-string v2, "precision highp float;\nuniform mat4 uMVPMatrix;\nuniform mat4 uSTMatrix;\nattribute vec4 aPosition;\nattribute vec4 aTextureCoord;\nvarying vec2 vTextureCoord;\nvoid main() {\n  gl_Position = uMVPMatrix * aPosition;\n  vTextureCoord = (uSTMatrix * aTextureCoord).xy;\n}\n"

    const-string v3, "#extension GL_OES_EGL_image_external : require\nprecision highp float;\nvarying vec2 vTextureCoord;\nuniform float uAlpha;\nuniform samplerExternalOES sTexture;\nuniform sampler2D sAlphaTexture;\nvoid main() {\n  vec4 color = texture2D(sTexture, vTextureCoord);\n  vec4 alphaMap = texture2D(sAlphaTexture, vTextureCoord);\n  gl_FragColor.rgb = color.rgb;\n  gl_FragColor.a = alphaMap.a * uAlpha;\n}\n"

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/car/ot;

    move-result-object v2

    aput-object v2, v0, v1

    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "initGl codec w:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " disp w:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " h:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    const/4 v0, 0x0

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->a:I

    iget-object v3, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v3, v3, Lcom/google/android/gms/car/ou;->b:I

    invoke-static {v0, v1, v2, v3}, Landroid/opengl/GLES20;->glViewport(IIII)V

    const/16 v0, 0x10

    new-array v0, v0, [F

    const/16 v1, 0x10

    new-array v11, v1, [F

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->a:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v12, v1, v2

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->b:I

    int-to-float v1, v1

    const/high16 v2, 0x40000000    # 2.0f

    div-float v13, v1, v2

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->e:I

    neg-int v1, v1

    int-to-float v1, v1

    add-float v2, v1, v12

    iget-object v1, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->f:I

    neg-int v1, v1

    int-to-float v1, v1

    add-float v3, v1, v13

    const/4 v1, 0x0

    const/high16 v4, 0x41a00000    # 20.0f

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/high16 v9, 0x3f800000    # 1.0f

    const/4 v10, 0x0

    move v5, v2

    move v6, v3

    invoke-static/range {v0 .. v10}, Landroid/opengl/Matrix;->setLookAtM([FIFFFFFFFFF)V

    const/4 v2, 0x0

    neg-float v3, v12

    neg-float v5, v13

    const/high16 v7, -0x3db80000    # -50.0f

    const/high16 v8, 0x42480000    # 50.0f

    move-object v1, v11

    move v4, v12

    move v6, v13

    invoke-static/range {v1 .. v8}, Landroid/opengl/Matrix;->orthoM([FIFFFFFF)V

    iget-object v1, p0, Lcom/google/android/gms/car/on;->v:[F

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v6, 0x0

    move-object v3, v11

    move-object v5, v0

    invoke-static/range {v1 .. v6}, Landroid/opengl/Matrix;->multiplyMM([FI[FI[FI)V

    const-string v1, "CAR.WM"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "CAR.WM"

    const-string v2, "vMatrix"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;[F)V

    const-string v0, "CAR.WM"

    const-string v1, "projMatrix"

    invoke-static {v0, v1, v11}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;[F)V

    const-string v0, "CAR.WM"

    const-string v1, "mMVPMatrix"

    iget-object v2, p0, Lcom/google/android/gms/car/on;->v:[F

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;Ljava/lang/String;[F)V

    :cond_2
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const/4 v1, 0x0

    aget v1, v0, v1

    iput v1, p0, Lcom/google/android/gms/car/on;->s:I

    const/4 v1, 0x1

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/car/on;->t:I

    iget v0, p0, Lcom/google/android/gms/car/on;->s:I

    sget v1, Lcom/google/android/gms/h;->dd:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/on;->b(II)V

    iget v0, p0, Lcom/google/android/gms/car/on;->t:I

    sget v1, Lcom/google/android/gms/h;->dc:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/on;->b(II)V

    const/16 v0, 0xb71

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v0, 0xb44

    invoke-static {v0}, Landroid/opengl/GLES20;->glEnable(I)V

    const/16 v0, 0x405

    invoke-static {v0}, Landroid/opengl/GLES20;->glCullFace(I)V

    const/16 v0, 0x901

    invoke-static {v0}, Landroid/opengl/GLES20;->glFrontFace(I)V

    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {v1, v0, v2}, Landroid/opengl/GLES20;->glGenTextures(I[II)V

    const/4 v1, 0x0

    aget v0, v0, v1

    iput v0, p0, Lcom/google/android/gms/car/on;->V:I

    new-instance v0, Landroid/graphics/SurfaceTexture;

    iget v1, p0, Lcom/google/android/gms/car/on;->V:I

    invoke-direct {v0, v1}, Landroid/graphics/SurfaceTexture;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->W:Landroid/graphics/SurfaceTexture;

    iget-object v0, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    new-instance v0, Lcom/google/android/gms/car/as;

    const-string v2, "Dummy"

    const/4 v3, 0x1

    const/4 v4, 0x1

    const/16 v5, 0x82

    new-instance v6, Landroid/view/Surface;

    iget-object v7, p0, Lcom/google/android/gms/car/on;->W:Landroid/graphics/SurfaceTexture;

    invoke-direct {v6, v7}, Landroid/view/Surface;-><init>(Landroid/graphics/SurfaceTexture;)V

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/as;-><init>(Landroid/hardware/display/DisplayManager;Ljava/lang/String;IIILandroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/gms/car/on;->X:Lcom/google/android/gms/car/as;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V
    .locals 4

    .prologue
    .line 55
    if-eqz p1, :cond_3

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->g()I

    move-result v1

    rem-int/lit8 v0, v1, 0x2

    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    add-int/lit8 v2, v1, 0x1

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-eqz v0, :cond_0

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->l()V

    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-eqz v0, :cond_1

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    :cond_1
    const-string v0, "CAR.WM"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "window cleanup window not in list for z "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " win "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->k()V

    :cond_3
    :goto_0
    return-void

    :cond_4
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->remove(I)V

    :cond_5
    const-string v1, "CAR.WM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "doWindowCleanup removed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " remaining wins in list "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->k()V

    goto :goto_0
.end method

.method private b(Z)V
    .locals 3

    .prologue
    .line 1976
    const-string v0, "CAR.WM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1977
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dump screen shot; share? "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1980
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1981
    if-nez v0, :cond_1

    .line 2010
    :goto_0
    return-void

    .line 1985
    :cond_1
    new-instance v1, Lcom/google/android/gms/car/op;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/op;-><init>(Lcom/google/android/gms/car/on;Z)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oq;->a(Lcom/google/android/gms/car/ox;)V

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/car/od;)V
    .locals 11

    .prologue
    .line 877
    iget v4, p1, Lcom/google/android/gms/car/od;->b:I

    .line 878
    iget v3, p1, Lcom/google/android/gms/car/od;->c:I

    .line 879
    iget-object v0, p1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt v3, v0, :cond_1

    .line 880
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Touch event\'s action index out of array "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 931
    :cond_0
    :goto_0
    return-void

    .line 884
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oe;

    .line 885
    iget v1, v0, Lcom/google/android/gms/car/oe;->c:I

    iget v2, v0, Lcom/google/android/gms/car/oe;->a:I

    iget v5, v0, Lcom/google/android/gms/car/oe;->b:I

    invoke-direct {p0, v1, v2, v5}, Lcom/google/android/gms/car/on;->a(III)Lcom/google/android/gms/car/oi;

    move-result-object v1

    .line 887
    if-nez v1, :cond_2

    .line 888
    const-string v0, "CAR.WM"

    const-string v1, "Touch event does not correspond to a window layer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 891
    :cond_2
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 893
    const/4 v2, 0x0

    .line 894
    iget-object v5, p1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v6, v3

    move v3, v2

    :goto_1
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/car/oe;

    .line 897
    iget v8, v2, Lcom/google/android/gms/car/oe;->c:I

    iget v9, v2, Lcom/google/android/gms/car/oe;->a:I

    iget v10, v2, Lcom/google/android/gms/car/oe;->b:I

    invoke-direct {p0, v8, v9, v10}, Lcom/google/android/gms/car/on;->a(III)Lcom/google/android/gms/car/oi;

    move-result-object v8

    if-ne v1, v8, :cond_c

    .line 898
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 899
    if-ne v2, v0, :cond_3

    move v6, v3

    .line 902
    :cond_3
    add-int/lit8 v2, v3, 0x1

    move v3, v6

    :goto_2
    move v6, v3

    move v3, v2

    .line 904
    goto :goto_1

    .line 906
    :cond_4
    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_b

    .line 907
    const/4 v2, 0x5

    if-ne v4, v2, :cond_8

    .line 908
    const-string v2, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 909
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pointer id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/gms/car/oe;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " changed from ACTION_POINTER_DOWN to ACTION_DOWN"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 912
    :cond_5
    const/4 v2, 0x0

    move v8, v2

    .line 922
    :goto_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    .line 923
    invoke-direct {p0, v1, v4, v5, v8}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;JI)J

    move-result-wide v2

    .line 924
    const/4 v9, 0x5

    if-eq v8, v9, :cond_6

    const/4 v9, 0x6

    if-ne v8, v9, :cond_a

    .line 925
    :cond_6
    shl-int/lit8 v6, v6, 0x8

    or-int/2addr v6, v8

    .line 927
    :goto_4
    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/car/oi;->a(JJILjava/util/ArrayList;)Z

    .line 928
    const/4 v1, 0x6

    if-eq v6, v1, :cond_7

    const/4 v1, 0x1

    if-ne v6, v1, :cond_0

    .line 929
    :cond_7
    iget v0, v0, Lcom/google/android/gms/car/oe;->c:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->e(I)V

    goto/16 :goto_0

    .line 913
    :cond_8
    const/4 v2, 0x6

    if-ne v4, v2, :cond_b

    .line 914
    const-string v2, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 915
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Pointer id "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/gms/car/oe;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " changed from ACTION_POINTER_UP to ACTION_UP"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 918
    :cond_9
    const/4 v2, 0x1

    move v8, v2

    goto :goto_3

    :cond_a
    move v6, v8

    goto :goto_4

    :cond_b
    move v8, v4

    goto :goto_3

    :cond_c
    move v2, v3

    move v3, v6

    goto/16 :goto_2
.end method

.method static synthetic c(Lcom/google/android/gms/car/on;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    if-eqz v0, :cond_0

    iput-boolean v2, v0, Lcom/google/android/gms/car/iy;->a:Z

    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/on;->s:I

    if-eqz v0, :cond_1

    new-array v0, v3, [I

    iget v1, p0, Lcom/google/android/gms/car/on;->s:I

    aput v1, v0, v2

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    iput v2, p0, Lcom/google/android/gms/car/on;->s:I

    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/on;->t:I

    if-eqz v0, :cond_2

    new-array v0, v3, [I

    iget v1, p0, Lcom/google/android/gms/car/on;->t:I

    aput v1, v0, v2

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    iput v2, p0, Lcom/google/android/gms/car/on;->t:I

    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->o()V

    iget-object v0, p0, Lcom/google/android/gms/car/on;->X:Lcom/google/android/gms/car/as;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/on;->X:Lcom/google/android/gms/car/as;

    invoke-virtual {v0}, Lcom/google/android/gms/car/as;->a()V

    iput-object v4, p0, Lcom/google/android/gms/car/on;->X:Lcom/google/android/gms/car/as;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->W:Landroid/graphics/SurfaceTexture;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/on;->W:Landroid/graphics/SurfaceTexture;

    invoke-virtual {v0}, Landroid/graphics/SurfaceTexture;->release()V

    iput-object v4, p0, Lcom/google/android/gms/car/on;->W:Landroid/graphics/SurfaceTexture;

    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/on;->V:I

    if-eqz v0, :cond_5

    new-array v0, v3, [I

    iget v1, p0, Lcom/google/android/gms/car/on;->V:I

    aput v1, v0, v2

    invoke-static {v3, v0, v2}, Landroid/opengl/GLES20;->glDeleteTextures(I[II)V

    iput v2, p0, Lcom/google/android/gms/car/on;->V:I

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/on;->l:Lcom/google/android/gms/car/ix;

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    sget-object v2, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    if-eq v1, v2, :cond_6

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, Lcom/google/android/gms/car/ix;->c:Landroid/opengl/EGLSurface;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroySurface(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    iget-object v2, v0, Lcom/google/android/gms/car/ix;->b:Landroid/opengl/EGLContext;

    invoke-static {v1, v2}, Landroid/opengl/EGL14;->eglDestroyContext(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLContext;)Z

    invoke-static {}, Landroid/opengl/EGL14;->eglReleaseThread()Z

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    invoke-static {v1}, Landroid/opengl/EGL14;->eglTerminate(Landroid/opengl/EGLDisplay;)Z

    :cond_6
    iget-object v1, v0, Lcom/google/android/gms/car/ix;->d:Landroid/view/Surface;

    invoke-virtual {v1}, Landroid/view/Surface;->release()V

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_DISPLAY:Landroid/opengl/EGLDisplay;

    iput-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_CONTEXT:Landroid/opengl/EGLContext;

    iput-object v1, v0, Lcom/google/android/gms/car/ix;->b:Landroid/opengl/EGLContext;

    sget-object v1, Landroid/opengl/EGL14;->EGL_NO_SURFACE:Landroid/opengl/EGLSurface;

    iput-object v1, v0, Lcom/google/android/gms/car/ix;->c:Landroid/opengl/EGLSurface;

    iput-object v4, v0, Lcom/google/android/gms/car/ix;->d:Landroid/view/Surface;

    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/car/on;Lcom/google/android/gms/car/oi;)V
    .locals 3

    .prologue
    .line 55
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->g()I

    move-result v1

    add-int/lit8 v0, v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-nez v0, :cond_1

    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "z order changed for alredy removed window "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_0
    return-void

    :cond_1
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    if-nez v0, :cond_2

    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    :cond_2
    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method static synthetic d(Lcom/google/android/gms/car/on;)Ljava/util/concurrent/Semaphore;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->i:Ljava/util/concurrent/Semaphore;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/on;)Landroid/os/Looper;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    return-object v0
.end method

.method private e(I)V
    .locals 1

    .prologue
    .line 816
    iget-object v0, p0, Lcom/google/android/gms/car/on;->Q:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 817
    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/car/on;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method private f(I)Lcom/google/android/gms/car/oi;
    .locals 2

    .prologue
    .line 1030
    iget-object v1, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v1

    .line 1031
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1032
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/on;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->m()V

    return-void
.end method

.method private g(I)Z
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1044
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_0

    move v0, v1

    .line 1049
    :goto_0
    return v0

    .line 1047
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v2

    .line 1048
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 1049
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->d()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1050
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    move v0, v1

    .line 1049
    goto :goto_1
.end method

.method static synthetic h(Lcom/google/android/gms/car/on;)V
    .locals 0

    .prologue
    .line 55
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->o()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/car/on;)I
    .locals 1

    .prologue
    .line 55
    iget v0, p0, Lcom/google/android/gms/car/on;->z:I

    return v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/on;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/car/oi;)V
    .locals 0

    .prologue
    .line 55
    invoke-virtual {p0}, Lcom/google/android/gms/car/oi;->w()Z

    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 757
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 758
    if-nez v0, :cond_0

    .line 762
    :goto_0
    return-void

    .line 761
    :cond_0
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/iy;->a(J)V

    goto :goto_0
.end method

.method private k(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 765
    if-nez p1, :cond_1

    .line 773
    :cond_0
    :goto_0
    return-void

    .line 768
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 769
    if-eqz v0, :cond_0

    .line 772
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oq;->b(Lcom/google/android/gms/car/oi;)V

    goto :goto_0
.end method

.method static synthetic k(Lcom/google/android/gms/car/on;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->b(Z)V

    return-void
.end method

.method private l()V
    .locals 1

    .prologue
    .line 1190
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1191
    if-eqz v0, :cond_0

    .line 1192
    invoke-virtual {v0}, Lcom/google/android/gms/car/oq;->b()V

    .line 1194
    :cond_0
    return-void
.end method

.method private m()V
    .locals 8

    .prologue
    const/4 v0, 0x0

    .line 1416
    .line 1417
    iget-object v1, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v0

    move v1, v0

    .line 1419
    :goto_0
    if-ge v2, v3, :cond_1

    .line 1420
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 1421
    if-eqz v0, :cond_0

    .line 1422
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 1425
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->w()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1426
    const/4 v0, 0x1

    :goto_2
    move v1, v0

    .line 1428
    goto :goto_1

    .line 1419
    :cond_0
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1431
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 1432
    iget-wide v4, p0, Lcom/google/android/gms/car/on;->B:J

    sub-long v4, v2, v4

    .line 1435
    iget-wide v6, p0, Lcom/google/android/gms/car/on;->A:J

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 1438
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 1439
    if-nez v0, :cond_2

    .line 1494
    :goto_3
    return-void

    .line 1442
    :cond_2
    const-wide/16 v2, 0x32

    sub-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/iy;->a(J)V

    .line 1443
    iget v0, p0, Lcom/google/android/gms/car/on;->J:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->J:I

    .line 1444
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    goto :goto_3

    .line 1450
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    .line 1451
    if-eqz v1, :cond_5

    .line 1464
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 1465
    iget v0, p0, Lcom/google/android/gms/car/on;->H:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->H:I

    .line 1468
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    .line 1477
    :goto_4
    iput-wide v2, p0, Lcom/google/android/gms/car/on;->B:J

    .line 1481
    iget-object v0, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1482
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->n()V

    .line 1483
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_6

    .line 1484
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1485
    const-string v0, "CAR.WM"

    const-string v1, "rendering skipped as composition is stopping"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    :cond_4
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto :goto_3

    .line 1469
    :cond_5
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 1473
    iget v0, p0, Lcom/google/android/gms/car/on;->I:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/on;->I:I

    .line 1476
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    goto :goto_4

    .line 1492
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/on;->l:Lcom/google/android/gms/car/ix;

    iget-object v1, v0, Lcom/google/android/gms/car/ix;->a:Landroid/opengl/EGLDisplay;

    iget-object v0, v0, Lcom/google/android/gms/car/ix;->c:Landroid/opengl/EGLSurface;

    invoke-static {v1, v0}, Landroid/opengl/EGL14;->eglSwapBuffers(Landroid/opengl/EGLDisplay;Landroid/opengl/EGLSurface;)Z

    .line 1493
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto :goto_3

    :cond_7
    move v0, v1

    goto :goto_2
.end method

.method private n()V
    .locals 11

    .prologue
    const/4 v10, 0x2

    const/4 v0, 0x0

    const/4 v2, 0x0

    .line 1502
    const/high16 v1, 0x3f800000    # 1.0f

    invoke-static {v2, v2, v2, v1}, Landroid/opengl/GLES20;->glClearColor(FFFF)V

    .line 1504
    const/16 v1, 0x4100

    invoke-static {v1}, Landroid/opengl/GLES20;->glClear(I)V

    .line 1507
    const/high16 v3, -0x40800000    # -1.0f

    .line 1508
    const/4 v1, 0x0

    .line 1509
    iget-object v2, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->size()I

    move-result v6

    move v5, v0

    move v2, v0

    .line 1510
    :goto_0
    if-ge v5, v6, :cond_2

    .line 1511
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v5}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 1512
    if-eqz v0, :cond_1

    .line 1513
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 1516
    iget-object v4, p0, Lcom/google/android/gms/car/on;->v:[F

    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/oi;->a([F)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 1517
    add-int/lit8 v4, v2, 0x1

    .line 1518
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->g()I

    move-result v2

    int-to-float v2, v2

    .line 1519
    cmpl-float v3, v3, v2

    if-ltz v3, :cond_0

    if-eqz v1, :cond_0

    .line 1520
    const-string v3, "CAR.WM"

    const/4 v8, 0x4

    invoke-static {v3, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 1521
    const-string v3, "CAR.WM"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "window z order wrong, first "

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v8, " second "

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    move v1, v2

    move v2, v4

    :goto_2
    move v3, v1

    move-object v1, v0

    .line 1528
    goto :goto_1

    .line 1510
    :cond_1
    add-int/lit8 v0, v5, 0x1

    move v5, v0

    goto :goto_0

    .line 1530
    :cond_2
    sget v0, Lcom/google/android/gms/car/on;->L:I

    if-ge v2, v0, :cond_3

    .line 1531
    iget-object v0, p0, Lcom/google/android/gms/car/on;->M:[I

    aget v1, v0, v2

    add-int/lit8 v1, v1, 0x1

    aput v1, v0, v2

    .line 1533
    :cond_3
    const-string v0, "CAR.WM"

    invoke-static {v0, v10}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    if-ge v2, v10, :cond_4

    .line 1534
    const-string v0, "CAR.WM"

    const-string v1, "Too little number of windows rendered"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1537
    :cond_4
    const-string v0, "window draw"

    invoke-static {v0}, Lcom/google/android/gms/car/on;->a(Ljava/lang/String;)V

    .line 1538
    return-void

    :cond_5
    move-object v0, v1

    move v1, v3

    goto :goto_2
.end method

.method private o()V
    .locals 5

    .prologue
    .line 1552
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1553
    const-string v0, "CAR.WM"

    const-string v1, "doTearDown windows"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1555
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    .line 1556
    const/4 v0, 0x0

    move v2, v0

    :goto_0
    if-ge v2, v3, :cond_3

    .line 1557
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 1558
    if-eqz v0, :cond_2

    .line 1559
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/oi;

    .line 1562
    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->r()V

    .line 1563
    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->l()V

    goto :goto_1

    .line 1565
    :cond_1
    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 1556
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1567
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->clear()V

    .line 1568
    return-void
.end method


# virtual methods
.method public final a(I)I
    .locals 1

    .prologue
    .line 780
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->g:I

    div-int/lit16 v0, v0, 0xa0

    mul-int/2addr v0, p1

    int-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Math;->round(F)I

    move-result v0

    return v0
.end method

.method public final a(ILcom/google/android/gms/car/om;)Lcom/google/android/gms/car/oi;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 504
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_0

    move-object v0, v1

    .line 526
    :goto_0
    return-object v0

    .line 508
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 518
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/car/oi;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/oi;-><init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V

    .line 521
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 522
    if-nez v2, :cond_1

    move-object v0, v1

    .line 523
    goto :goto_0

    .line 512
    :pswitch_1
    new-instance v0, Lcom/google/android/gms/car/pb;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/pb;-><init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V

    goto :goto_1

    .line 515
    :pswitch_2
    new-instance v0, Lcom/google/android/gms/car/pa;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/pa;-><init>(Lcom/google/android/gms/car/on;ILcom/google/android/gms/car/om;)V

    goto :goto_1

    .line 525
    :cond_1
    invoke-direct {p0, v0, p1, v2}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;ILcom/google/android/gms/car/oq;)V

    goto :goto_0

    .line 508
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final a()V
    .locals 4

    .prologue
    .line 486
    iget-object v0, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    .line 487
    if-gez v0, :cond_1

    .line 488
    const-string v1, "CAR.WM"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 489
    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "negative remaining frames:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 493
    :cond_1
    return-void
.end method

.method final a(JI)V
    .locals 3

    .prologue
    .line 1165
    const-string v0, "CAR.WM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1166
    const-string v0, "CAR.WM"

    const-string v1, "requestIdleUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1168
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1169
    if-eqz v0, :cond_1

    .line 1170
    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/car/oq;->a(JI)V

    .line 1172
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)V
    .locals 6

    .prologue
    const/4 v5, 0x6

    const/4 v4, 0x4

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 979
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_1

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 983
    :cond_1
    invoke-direct {p0, v5}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 984
    if-eqz v0, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->d()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 985
    iget v1, p0, Lcom/google/android/gms/car/on;->O:I

    if-eq v1, v5, :cond_2

    .line 986
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    .line 988
    :cond_2
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->b(Landroid/view/KeyEvent;)V

    goto :goto_0

    .line 995
    :cond_3
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v0

    .line 996
    const/16 v1, 0x15

    if-eq v0, v1, :cond_4

    const/16 v1, 0x16

    if-eq v0, v1, :cond_4

    if-eq v0, v2, :cond_4

    if-ne v0, v3, :cond_5

    .line 1000
    :cond_4
    iget v1, p0, Lcom/google/android/gms/car/on;->O:I

    if-eq v1, v2, :cond_5

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v1

    if-nez v1, :cond_5

    .line 1001
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v1

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    .line 1006
    :cond_5
    if-ne v0, v4, :cond_8

    .line 1007
    invoke-virtual {p1}, Landroid/view/KeyEvent;->isLongPress()Z

    move-result v0

    if-eqz v0, :cond_7

    .line 1008
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 1009
    const-string v0, "CAR.WM"

    const-string v1, "Long press on back, switching to rail."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1012
    :cond_6
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    if-eq v0, v3, :cond_0

    .line 1013
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    goto :goto_0

    .line 1018
    :cond_7
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    if-eq v0, v2, :cond_8

    .line 1019
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    .line 1023
    :cond_8
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1024
    if-eqz v0, :cond_0

    .line 1025
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->b(Landroid/view/KeyEvent;)V

    goto :goto_0
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 953
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_1

    .line 960
    :cond_0
    :goto_0
    return-void

    .line 956
    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 957
    if-eqz v0, :cond_0

    .line 958
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->a(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/od;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    .line 935
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_0

    .line 949
    :goto_0
    return-void

    .line 939
    :cond_0
    iget v0, p1, Lcom/google/android/gms/car/od;->b:I

    .line 940
    if-eqz v0, :cond_1

    if-ne v0, v9, :cond_4

    .line 941
    :cond_1
    iget-object v0, p1, Lcom/google/android/gms/car/od;->e:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oe;

    iget v1, v0, Lcom/google/android/gms/car/oe;->a:I

    iget v2, v0, Lcom/google/android/gms/car/oe;->b:I

    iget v8, v0, Lcom/google/android/gms/car/oe;->c:I

    iget v6, p1, Lcom/google/android/gms/car/od;->b:I

    invoke-direct {p0, v8, v1, v2}, Lcom/google/android/gms/car/on;->a(III)Lcom/google/android/gms/car/oi;

    move-result-object v1

    if-nez v1, :cond_3

    const-string v0, "CAR.WM"

    const-string v1, "Touch event does not correspond to a window layer"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 948
    :cond_2
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/od;->a()V

    goto :goto_0

    .line 941
    :cond_3
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7, v9}, Ljava/util/ArrayList;-><init>(I)V

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    invoke-direct {p0, v1, v4, v5, v6}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;JI)J

    move-result-wide v2

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/gms/car/oi;->a(JJILjava/util/ArrayList;)Z

    if-ne v6, v9, :cond_2

    invoke-direct {p0, v8}, Lcom/google/android/gms/car/on;->e(I)V

    goto :goto_1

    .line 942
    :cond_4
    const/4 v1, 0x2

    if-ne v0, v1, :cond_5

    .line 943
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/on;->b(Lcom/google/android/gms/car/od;)V

    goto :goto_1

    .line 945
    :cond_5
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/on;->c(Lcom/google/android/gms/car/od;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/oi;)V
    .locals 3

    .prologue
    .line 443
    const-string v0, "CAR.WM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameAvailableForWindow "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_2

    .line 447
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 448
    const-string v0, "CAR.WM"

    const-string v1, "onFrameAvailable called but composition not running"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 457
    :cond_1
    :goto_0
    return-void

    .line 452
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 453
    if-eqz v0, :cond_1

    .line 456
    invoke-virtual {v0}, Lcom/google/android/gms/car/iy;->a()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/oi;Landroid/view/KeyEvent;)V
    .locals 8

    .prologue
    const/16 v7, 0x14

    const/16 v6, 0x13

    const/4 v3, 0x5

    const/4 v2, 0x4

    const/4 v1, 0x1

    .line 1075
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v5

    .line 1076
    if-ne v5, v7, :cond_1

    move v4, v1

    .line 1077
    :goto_0
    if-ne v5, v6, :cond_2

    move v0, v1

    :goto_1
    or-int/2addr v4, v0

    .line 1078
    invoke-virtual {p2}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-ne v0, v1, :cond_3

    move v0, v1

    :goto_2
    and-int/2addr v0, v4

    .line 1079
    if-nez v0, :cond_4

    .line 1147
    :cond_0
    :goto_3
    return-void

    .line 1076
    :cond_1
    const/4 v0, 0x0

    move v4, v0

    goto :goto_0

    .line 1077
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 1078
    :cond_3
    const/4 v0, 0x0

    goto :goto_2

    .line 1095
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    .line 1097
    packed-switch v0, :pswitch_data_0

    .line 1141
    const-string v1, "CAR.WM"

    const-string v2, "You added a window layer but didn\'t update onUnhandledKeyEvent"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_5
    move v1, v0

    .line 1144
    :cond_6
    :goto_4
    if-eq v0, v1, :cond_0

    .line 1145
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    goto :goto_3

    .line 1099
    :pswitch_0
    if-ne v5, v7, :cond_8

    .line 1100
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v1

    if-eqz v1, :cond_7

    move v1, v2

    .line 1101
    goto :goto_4

    .line 1103
    :cond_7
    const/4 v1, 0x2

    goto :goto_4

    .line 1105
    :cond_8
    if-ne v5, v6, :cond_5

    invoke-direct {p0, v3}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v1

    if-eqz v1, :cond_5

    move v1, v3

    .line 1106
    goto :goto_4

    .line 1110
    :pswitch_1
    if-ne v5, v6, :cond_5

    goto :goto_4

    .line 1115
    :pswitch_2
    if-ne v5, v7, :cond_5

    .line 1116
    invoke-direct {p0, v2}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v3

    if-eqz v3, :cond_6

    move v1, v2

    .line 1117
    goto :goto_4

    .line 1124
    :pswitch_3
    if-ne v5, v7, :cond_5

    .line 1125
    const/4 v1, 0x2

    goto :goto_4

    :pswitch_4
    move v1, v0

    .line 1130
    goto :goto_4

    .line 1132
    :pswitch_5
    if-ne v5, v6, :cond_5

    .line 1133
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v2

    if-eqz v2, :cond_6

    move v1, v3

    .line 1134
    goto :goto_4

    .line 1097
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_4
        :pswitch_5
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/car/ox;)V
    .locals 1

    .prologue
    .line 1572
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1573
    if-eqz v0, :cond_0

    .line 1574
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oq;->a(Lcom/google/android/gms/car/ox;)V

    .line 1576
    :cond_0
    return-void
.end method

.method final a(Lcom/google/android/gms/car/pa;)V
    .locals 1

    .prologue
    .line 1183
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1184
    if-eqz v0, :cond_0

    .line 1185
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oq;->a(Lcom/google/android/gms/car/pa;)V

    .line 1187
    :cond_0
    return-void
.end method

.method final a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 2013
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "frame rate "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->o:Z

    if-eqz v0, :cond_0

    const-string v0, "30"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " decoder add depth:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/on;->p:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2015
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "power saving mode "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/car/on;->q:Z

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2016
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Frames to send "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2017
    iget v0, p0, Lcom/google/android/gms/car/on;->H:I

    iget v2, p0, Lcom/google/android/gms/car/on;->I:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/car/on;->J:I

    add-int/2addr v0, v2

    iget v2, p0, Lcom/google/android/gms/car/on;->K:I

    add-int/2addr v0, v2

    .line 2019
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "total frames:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " normal frames:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/on;->H:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " idle frames:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/on;->I:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " drop by timer:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/on;->J:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " drop by ack:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/car/on;->K:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2022
    iput v1, p0, Lcom/google/android/gms/car/on;->H:I

    .line 2023
    iput v1, p0, Lcom/google/android/gms/car/on;->I:I

    .line 2024
    iput v1, p0, Lcom/google/android/gms/car/on;->J:I

    .line 2025
    iput v1, p0, Lcom/google/android/gms/car/on;->K:I

    .line 2026
    const-string v0, "num renderings per num windows:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    move v0, v1

    .line 2027
    :goto_1
    sget v2, Lcom/google/android/gms/car/on;->L:I

    if-ge v0, v2, :cond_1

    .line 2028
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, " "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/on;->M:[I

    aget v3, v3, v0

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2027
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2013
    :cond_0
    const-string v0, "60"

    goto/16 :goto_0

    .line 2030
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->M:[I

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([II)V

    .line 2031
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    .line 2032
    const-string v0, "Windows: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 2047
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v3

    move v2, v1

    .line 2048
    :goto_2
    if-ge v2, v3, :cond_3

    .line 2049
    iget-object v0, p0, Lcom/google/android/gms/car/on;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/LinkedList;

    .line 2050
    if-eqz v0, :cond_2

    .line 2051
    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_3
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 2054
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->a(Ljava/io/PrintWriter;)V

    goto :goto_3

    .line 2048
    :cond_2
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 2057
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/id;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 2058
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/on;->b(Z)V

    .line 2060
    :cond_4
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 422
    if-nez v0, :cond_0

    .line 440
    :goto_0
    return-void

    .line 425
    :cond_0
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/iy;->a(Z)V

    .line 426
    if-eqz p1, :cond_2

    .line 427
    const-string v0, "CAR.WM"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 428
    const-string v0, "CAR.WM"

    const-string v1, "rendering in power saving mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 430
    :cond_1
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/on;->q:Z

    .line 431
    const-wide/16 v0, 0x64

    .line 437
    :goto_1
    iput-wide v0, p0, Lcom/google/android/gms/car/on;->A:J

    goto :goto_0

    .line 433
    :cond_2
    const-string v0, "CAR.WM"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 434
    const-string v0, "CAR.WM"

    const-string v1, "rendering in normal mode"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 436
    :cond_3
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/on;->q:Z

    .line 437
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->o:Z

    if-eqz v0, :cond_4

    const-wide/16 v0, 0x1e

    goto :goto_1

    :cond_4
    const-wide/16 v0, 0xe

    goto :goto_1
.end method

.method public final declared-synchronized a(IIIIIILandroid/view/Surface;I)Z
    .locals 11

    .prologue
    .line 266
    monitor-enter p0

    :try_start_0
    const-string v2, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 267
    const-string v2, "CAR.WM"

    const-string v3, "startComposition"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 269
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/on;->N:Lcom/google/android/gms/car/ip;

    invoke-virtual {v2, p0}, Lcom/google/android/gms/car/ip;->a(Lcom/google/android/gms/car/iv;)V

    .line 270
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/gms/car/on;->h:Landroid/view/Surface;

    .line 271
    const/16 v2, 0x1e

    if-ne p3, v2, :cond_6

    const/4 v2, 0x1

    :goto_0
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->o:Z

    .line 272
    move/from16 v0, p6

    iput v0, p0, Lcom/google/android/gms/car/on;->p:I

    .line 273
    iget-boolean v2, p0, Lcom/google/android/gms/car/on;->o:Z

    if-eqz v2, :cond_7

    .line 274
    const-string v2, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 275
    const-string v2, "CAR.WM"

    const-string v3, "Restrict frame rate to 30 fps"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 277
    :cond_1
    const-wide/16 v2, 0x1e

    iput-wide v2, p0, Lcom/google/android/gms/car/on;->A:J

    .line 281
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 282
    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v3

    iget v3, v3, Landroid/util/DisplayMetrics;->density:F

    .line 284
    sget v4, Lcom/google/android/gms/g;->e:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    div-float/2addr v4, v3

    move/from16 v0, p8

    int-to-float v5, v0

    mul-float/2addr v4, v5

    const/high16 v5, 0x43200000    # 160.0f

    div-float/2addr v4, v5

    float-to-int v8, v4

    .line 288
    move/from16 v0, p5

    if-lt v8, v0, :cond_2

    .line 289
    const-string v4, "CAR.WM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "rail height too big:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 290
    add-int/lit8 v8, p5, -0x1

    .line 292
    :cond_2
    sget v4, Lcom/google/android/gms/g;->c:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v4

    div-float/2addr v4, v3

    move/from16 v0, p8

    int-to-float v5, v0

    mul-float/2addr v4, v5

    const/high16 v5, 0x43200000    # 160.0f

    div-float/2addr v4, v5

    float-to-int v10, v4

    .line 294
    move/from16 v0, p5

    if-lt v10, v0, :cond_3

    .line 295
    const-string v4, "CAR.WM"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "ime height too big:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 296
    add-int/lit8 v10, p5, -0x1

    .line 298
    :cond_3
    sget v4, Lcom/google/android/gms/g;->d:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    div-float/2addr v2, v3

    move/from16 v0, p8

    int-to-float v3, v0

    mul-float/2addr v2, v3

    const/high16 v3, 0x43200000    # 160.0f

    div-float/2addr v2, v3

    float-to-int v9, v2

    .line 300
    move/from16 v0, p5

    if-lt v9, v0, :cond_4

    .line 301
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "notification height too big:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    add-int/lit8 v9, p5, -0x1

    .line 304
    :cond_4
    const-string v2, "CAR.WM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 305
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "layout rail h:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ime h:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " notification h:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    :cond_5
    new-instance v2, Lcom/google/android/gms/car/ou;

    move v3, p1

    move v4, p2

    move v5, p4

    move/from16 v6, p5

    move/from16 v7, p8

    invoke-direct/range {v2 .. v10}, Lcom/google/android/gms/car/ou;-><init>(IIIIIIII)V

    iput-object v2, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    .line 310
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/car/on;->z:I

    .line 311
    iget-object v2, p0, Lcom/google/android/gms/car/on;->x:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 312
    iget-object v2, p0, Lcom/google/android/gms/car/on;->w:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 313
    new-instance v2, Lcom/google/android/gms/car/iy;

    iget-boolean v3, p0, Lcom/google/android/gms/car/on;->o:Z

    move/from16 v0, p6

    invoke-direct {v2, p0, v3, v0}, Lcom/google/android/gms/car/iy;-><init>(Lcom/google/android/gms/car/ov;ZI)V

    iput-object v2, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 314
    new-instance v2, Ljava/lang/Thread;

    new-instance v3, Lcom/google/android/gms/car/or;

    const/4 v4, 0x0

    invoke-direct {v3, p0, v4}, Lcom/google/android/gms/car/or;-><init>(Lcom/google/android/gms/car/on;B)V

    const-string v4, "CompositionThread"

    invoke-direct {v2, v3, v4}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/gms/car/on;->g:Ljava/lang/Thread;

    .line 315
    new-instance v2, Ljava/util/concurrent/Semaphore;

    const/4 v3, 0x0

    invoke-direct {v2, v3}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/gms/car/on;->i:Ljava/util/concurrent/Semaphore;

    .line 316
    iget-object v2, p0, Lcom/google/android/gms/car/on;->g:Ljava/lang/Thread;

    invoke-virtual {v2}, Ljava/lang/Thread;->start()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 317
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/on;->i:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 318
    :goto_2
    :try_start_2
    iget-boolean v2, p0, Lcom/google/android/gms/car/on;->r:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_8

    .line 319
    const/4 v2, 0x0

    .line 341
    :goto_3
    monitor-exit p0

    return v2

    .line 271
    :cond_6
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 279
    :cond_7
    const-wide/16 v2, 0xe

    :try_start_3
    iput-wide v2, p0, Lcom/google/android/gms/car/on;->A:J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_1

    .line 266
    :catchall_0
    move-exception v2

    monitor-exit p0

    throw v2

    .line 321
    :cond_8
    :try_start_4
    iget-object v2, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    const/4 v3, 0x1

    iput-boolean v3, v2, Lcom/google/android/gms/car/iy;->a:Z

    .line 322
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->y:Z

    .line 323
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/car/on;->H:I

    .line 324
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/car/on;->I:I

    .line 325
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/car/on;->J:I

    .line 326
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/gms/car/on;->K:I

    .line 327
    iget-object v2, p0, Lcom/google/android/gms/car/on;->M:[I

    const/4 v3, 0x0

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([II)V

    .line 328
    iget-object v3, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v3
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 329
    :try_start_5
    iget-object v2, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v2}, Landroid/util/SparseArray;->clear()V

    .line 330
    monitor-exit v3
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 332
    :try_start_6
    iget-object v2, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/car/oy;->a(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 333
    new-instance v2, Lcom/google/android/gms/car/oy;

    iget-object v3, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    invoke-direct {v2, v3, p0}, Lcom/google/android/gms/car/oy;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/on;)V

    .line 335
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    iget-object v3, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    iget-wide v4, v3, Lcom/google/android/gms/car/iy;->b:J

    iget-object v3, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    invoke-virtual {v2, v4, v5, v3}, Lcom/google/android/gms/car/qo;->a(JLcom/google/android/gms/car/ou;)V

    .line 336
    iget-object v2, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    if-nez v2, :cond_a

    .line 337
    new-instance v2, Lcom/google/android/gms/car/os;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/gms/car/os;-><init>(Lcom/google/android/gms/car/on;B)V

    iput-object v2, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    .line 338
    iget-object v2, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    new-instance v4, Landroid/content/IntentFilter;

    const-string v5, "com.google.android.gms.car.DumpScreenshot"

    invoke-direct {v4, v5}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 341
    :cond_a
    const/4 v2, 0x1

    goto :goto_3

    .line 330
    :catchall_1
    move-exception v2

    monitor-exit v3

    throw v2
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    :catch_0
    move-exception v2

    goto :goto_2
.end method

.method public final b(I)Lcom/google/android/gms/car/ot;
    .locals 1

    .prologue
    .line 1715
    iget-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    aget-object v0, v0, p1

    return-object v0
.end method

.method public final declared-synchronized b()V
    .locals 2

    .prologue
    .line 346
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    const-string v0, "CAR.WM"

    const-string v1, "stopComposition"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 380
    :goto_0
    monitor-exit p0

    return-void

    .line 352
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    .line 356
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    if-eqz v0, :cond_3

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/car/iy;->a:Z

    .line 360
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qo;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 362
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    if-eqz v0, :cond_4

    .line 366
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->l()V

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    .line 368
    iget-object v0, p0, Lcom/google/android/gms/car/on;->g:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->join()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 373
    :cond_4
    :goto_1
    const/4 v0, 0x0

    :try_start_3
    iput-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 374
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->g:Ljava/lang/Thread;

    .line 375
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->h:Landroid/view/Surface;

    .line 376
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->i:Ljava/util/concurrent/Semaphore;

    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 378
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    .line 379
    sget-object v0, Lcom/google/android/gms/car/on;->c:Lcom/google/android/gms/car/ii;

    goto :goto_0

    .line 371
    :catch_0
    move-exception v0

    const-string v0, "CAR.WM"

    const-string v1, "Interupted exception before composition thread joined"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final b(Landroid/view/MotionEvent;)V
    .locals 1

    .prologue
    .line 964
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_1

    .line 971
    :cond_0
    :goto_0
    return-void

    .line 967
    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 968
    if-eqz v0, :cond_0

    .line 969
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->b(Landroid/view/MotionEvent;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/car/oi;)V
    .locals 4

    .prologue
    const/4 v3, 0x2

    .line 465
    const-string v0, "CAR.WM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 466
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onFrameAvaiableForHiddenWindow "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 468
    :cond_0
    if-nez p1, :cond_2

    .line 482
    :cond_1
    :goto_0
    return-void

    .line 471
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_3

    .line 472
    const-string v0, "CAR.WM"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 473
    const-string v0, "CAR.WM"

    const-string v1, "onFrameAvaiableForHiddenWindow called but composition not running"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 477
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 478
    if-eqz v0, :cond_1

    .line 481
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oq;->d(Lcom/google/android/gms/car/oi;)V

    goto :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 386
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 387
    const-string v0, "CAR.WM"

    const-string v1, "stopCompositionNow"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v0, :cond_2

    .line 411
    :cond_1
    :goto_0
    return-void

    .line 392
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_3

    .line 393
    iget-object v0, p0, Lcom/google/android/gms/car/on;->d:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 394
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/on;->f:Landroid/content/BroadcastReceiver;

    .line 396
    :cond_3
    iput-boolean v2, p0, Lcom/google/android/gms/car/on;->y:Z

    .line 397
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 398
    if-eqz v0, :cond_4

    .line 399
    iput-boolean v2, v0, Lcom/google/android/gms/car/iy;->a:Z

    .line 400
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/on;->j:Landroid/os/Looper;

    .line 403
    if-eqz v0, :cond_1

    .line 407
    invoke-direct {p0}, Lcom/google/android/gms/car/on;->l()V

    .line 408
    invoke-virtual {v0}, Landroid/os/Looper;->quitSafely()V

    goto :goto_0
.end method

.method public final c(I)V
    .locals 1

    .prologue
    .line 1727
    iget v0, p0, Lcom/google/android/gms/car/on;->u:I

    if-eq p1, v0, :cond_0

    .line 1728
    iget-object v0, p0, Lcom/google/android/gms/car/on;->a:[Lcom/google/android/gms/car/ot;

    aget-object v0, v0, p1

    iget v0, v0, Lcom/google/android/gms/car/ot;->a:I

    invoke-static {v0}, Landroid/opengl/GLES20;->glUseProgram(I)V

    .line 1729
    iput p1, p0, Lcom/google/android/gms/car/on;->u:I

    .line 1731
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/car/oi;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 536
    iget-boolean v1, p0, Lcom/google/android/gms/car/on;->y:Z

    if-nez v1, :cond_1

    .line 551
    :cond_0
    :goto_0
    return v0

    .line 539
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    invoke-virtual {v1, p1}, Lcom/google/android/gms/car/qo;->b(Lcom/google/android/gms/car/oi;)Z

    move-result v1

    .line 540
    if-nez v1, :cond_2

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->p()Z

    move-result v1

    if-nez v1, :cond_2

    .line 541
    const-string v1, "CAR.WM"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 542
    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "recycleProjectionWindow for non-removed window "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 546
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 547
    if-eqz v1, :cond_0

    .line 550
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/on;->a(Lcom/google/android/gms/car/oi;ILcom/google/android/gms/car/oq;)V

    .line 551
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final declared-synchronized d(Lcom/google/android/gms/car/oi;)V
    .locals 6

    .prologue
    const/4 v5, 0x4

    const/4 v4, 0x3

    const/4 v2, 0x1

    .line 604
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v1

    .line 605
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    if-ne v1, v0, :cond_4

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->f()Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarUiInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 611
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/oi;->a(Z)V

    .line 627
    :cond_0
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 628
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 629
    iget-object v3, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v3, v1, p1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 630
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 631
    if-eqz v0, :cond_2

    if-eq v0, p1, :cond_2

    .line 632
    :try_start_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->f()I

    move-result v1

    .line 633
    if-eq v1, v4, :cond_2

    if-eq v1, v5, :cond_2

    .line 635
    const-string v1, "CAR.WM"

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 636
    const-string v1, "CAR.WM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "attachProjectionWindow called while old window is not removed "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->r()V

    .line 640
    iget-object v1, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/car/qo;->a(Lcom/google/android/gms/car/oi;ZZ)V

    .line 641
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->c()V

    .line 644
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->q()V

    .line 645
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 646
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window attached "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 648
    :cond_3
    monitor-exit p0

    return-void

    .line 613
    :cond_4
    if-eq v1, v4, :cond_5

    if-eq v1, v2, :cond_5

    if-eq v1, v5, :cond_5

    const/4 v0, 0x6

    if-ne v1, v0, :cond_0

    .line 623
    :cond_5
    :try_start_3
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 604
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 630
    :catchall_1
    move-exception v0

    :try_start_4
    monitor-exit v2

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 415
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    return v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->d:I

    return v0
.end method

.method public final declared-synchronized e(Lcom/google/android/gms/car/oi;)V
    .locals 3

    .prologue
    .line 655
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 656
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window attach complete "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    .line 658
    :cond_0
    if-nez p1, :cond_1

    .line 676
    :goto_0
    monitor-exit p0

    return-void

    .line 661
    :cond_1
    :try_start_1
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 662
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    .line 666
    iget-object v1, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 667
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 668
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 669
    if-ne v0, p1, :cond_2

    .line 670
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/qo;->a(Lcom/google/android/gms/car/oi;)V

    .line 675
    :goto_1
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto :goto_0

    .line 668
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 655
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0

    .line 673
    :cond_2
    :try_start_4
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/on;->f(Lcom/google/android/gms/car/oi;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto :goto_1
.end method

.method final f()V
    .locals 2

    .prologue
    .line 746
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->q:Z

    if-nez v0, :cond_0

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->s()Lcom/google/android/gms/car/oc;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/oc;->a(I)V

    .line 749
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->m:Lcom/google/android/gms/car/iy;

    .line 750
    if-nez v0, :cond_1

    .line 754
    :goto_0
    return-void

    .line 753
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/iy;->a()V

    goto :goto_0
.end method

.method public final declared-synchronized f(Lcom/google/android/gms/car/oi;)V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    .line 681
    monitor-enter p0

    if-nez p1, :cond_1

    .line 737
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 684
    :cond_1
    :try_start_0
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 685
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removeProjectionWindow "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v1

    .line 689
    iget-object v2, p0, Lcom/google/android/gms/car/on;->S:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 690
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/oi;

    .line 691
    if-ne v0, p1, :cond_3

    .line 692
    iget-object v3, p0, Lcom/google/android/gms/car/on;->T:Landroid/util/SparseArray;

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 694
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 695
    :try_start_2
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->f()I

    move-result v2

    .line 696
    if-ne v2, v6, :cond_5

    .line 697
    const-string v0, "CAR.WM"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 698
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removing window that is already removed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 700
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 694
    :catchall_1
    move-exception v0

    :try_start_3
    monitor-exit v2

    throw v0

    .line 702
    :cond_5
    if-ne v2, v5, :cond_6

    .line 703
    iget-object v2, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/car/qo;->c(Lcom/google/android/gms/car/oi;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 704
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->c()V

    goto :goto_0

    .line 708
    :cond_6
    if-eq p1, v0, :cond_7

    .line 709
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->r()V

    .line 710
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/on;->k(Lcom/google/android/gms/car/oi;)V

    .line 711
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->c()V

    .line 712
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 713
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window removed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 719
    :cond_7
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    if-ne v0, v1, :cond_c

    if-eq v1, v5, :cond_8

    const/4 v0, 0x6

    if-eq v1, v0, :cond_8

    const/4 v0, 0x5

    if-ne v1, v0, :cond_c

    .line 722
    :cond_8
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->g(I)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 723
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    .line 731
    :cond_9
    :goto_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->r()V

    .line 732
    const-string v0, "CAR.WM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 733
    const-string v0, "CAR.WM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "window detached "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 735
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/on;->U:Lcom/google/android/gms/car/qo;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/car/qo;->a(Lcom/google/android/gms/car/oi;ZZ)V

    .line 736
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->c()V

    goto/16 :goto_0

    .line 725
    :cond_b
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V

    goto :goto_1

    .line 727
    :cond_c
    if-ne v1, v6, :cond_9

    .line 729
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/on;->h(Lcom/google/android/gms/car/oi;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final g()I
    .locals 1

    .prologue
    .line 776
    iget-object v0, p0, Lcom/google/android/gms/car/on;->n:Lcom/google/android/gms/car/ou;

    iget v0, v0, Lcom/google/android/gms/car/ou;->g:I

    return v0
.end method

.method final g(Lcom/google/android/gms/car/oi;)V
    .locals 0

    .prologue
    .line 741
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/on;->k(Lcom/google/android/gms/car/oi;)V

    .line 742
    return-void
.end method

.method final h()V
    .locals 2

    .prologue
    .line 1152
    const-string v0, "CAR.WM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1153
    const-string v0, "CAR.WM"

    const-string v1, "requestNormalUpdate"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1155
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/on;->w:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    .line 1156
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1157
    if-eqz v0, :cond_1

    .line 1158
    invoke-virtual {v0}, Lcom/google/android/gms/car/oq;->a()V

    .line 1160
    :cond_1
    return-void
.end method

.method final h(Lcom/google/android/gms/car/oi;)V
    .locals 4

    .prologue
    .line 1054
    iget-boolean v0, p0, Lcom/google/android/gms/car/on;->y:Z

    if-eqz v0, :cond_0

    if-nez p1, :cond_1

    .line 1072
    :cond_0
    :goto_0
    return-void

    .line 1059
    :cond_1
    iget v0, p0, Lcom/google/android/gms/car/on;->O:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/on;->f(I)Lcom/google/android/gms/car/oi;

    move-result-object v1

    .line 1060
    const-string v0, "CAR.WM"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 1061
    const-string v2, "CAR.WM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Moving input focus from "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v1, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/car/on;->d(I)Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " to "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/car/on;->d(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 1065
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/on;->e:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->f()Lcom/google/android/gms/car/CarUiInfo;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarUiInfo;->b()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 1066
    if-eqz v1, :cond_3

    .line 1067
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/oi;->a(Z)V

    .line 1069
    :cond_3
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Lcom/google/android/gms/car/oi;->a(Z)V

    .line 1071
    :cond_4
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/on;->O:I

    goto :goto_0

    .line 1061
    :cond_5
    const-string v0, "[null]"

    goto :goto_1
.end method

.method public final i()I
    .locals 1

    .prologue
    .line 1719
    iget v0, p0, Lcom/google/android/gms/car/on;->s:I

    return v0
.end method

.method final i(Lcom/google/android/gms/car/oi;)V
    .locals 1

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/google/android/gms/car/on;->k:Lcom/google/android/gms/car/oq;

    .line 1177
    if-eqz v0, :cond_0

    .line 1178
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oq;->c(Lcom/google/android/gms/car/oi;)V

    .line 1180
    :cond_0
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 1723
    iget v0, p0, Lcom/google/android/gms/car/on;->t:I

    return v0
.end method
