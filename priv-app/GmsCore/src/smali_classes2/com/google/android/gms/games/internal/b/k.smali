.class public final Lcom/google/android/gms/games/internal/b/k;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 26
    packed-switch p0, :pswitch_data_0

    .line 37
    const-string v0, "MatchTurnStatus"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown match turn status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 38
    const-string v0, "TURN_STATUS_UNKNOWN"

    :goto_0
    return-object v0

    .line 28
    :pswitch_0
    const-string v0, "TURN_STATUS_INVITED"

    goto :goto_0

    .line 30
    :pswitch_1
    const-string v0, "TURN_STATUS_MY_TURN"

    goto :goto_0

    .line 32
    :pswitch_2
    const-string v0, "TURN_STATUS_THEIR_TURN"

    goto :goto_0

    .line 34
    :pswitch_3
    const-string v0, "TURN_STATUS_COMPLETE"

    goto :goto_0

    .line 26
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
