.class final Lcom/google/android/gms/games/internal/bv;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;

.field private final b:Lcom/google/android/gms/common/api/aj;

.field private final c:Lcom/google/android/gms/common/api/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/aj;Lcom/google/android/gms/common/api/aj;)V
    .locals 1

    .prologue
    .line 1156
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 1158
    const-string v0, "Callbacks must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/aj;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bv;->a:Lcom/google/android/gms/common/api/aj;

    .line 1160
    iput-object p2, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    .line 1161
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bv;->c:Lcom/google/android/gms/common/api/aj;

    .line 1162
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1230
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1231
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/be;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/be;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1234
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V
    .locals 2

    .prologue
    .line 1276
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->c:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1277
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->c:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/aw;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/aw;-><init>(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1280
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1238
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1239
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bf;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/bf;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1242
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1246
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1247
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bg;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/bg;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1250
    :cond_0
    return-void
.end method

.method public final d(ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 1176
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/ag;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/ag;-><init>(ILjava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1177
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1254
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1255
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bc;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/bc;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1257
    :cond_0
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1261
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1262
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/az;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/az;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1264
    :cond_0
    return-void
.end method

.method public final e(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1214
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1215
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bb;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/bb;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1218
    :cond_0
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1268
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/ba;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ba;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1271
    :cond_0
    return-void
.end method

.method public final f(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1222
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1223
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bd;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/internal/bd;-><init>(Lcom/google/android/gms/common/data/DataHolder;[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1226
    :cond_0
    return-void
.end method

.method public final s(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1166
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/by;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/by;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1167
    return-void
.end method

.method public final t(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1171
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/ac;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ac;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1172
    return-void
.end method

.method public final u(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1186
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bx;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bx;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1189
    :cond_0
    return-void
.end method

.method public final v(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1194
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bu;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bu;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1196
    :cond_0
    return-void
.end method

.method public final w(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1181
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/bw;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bw;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1182
    return-void
.end method

.method public final x(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1200
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1201
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/j;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/j;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1203
    :cond_0
    return-void
.end method

.method public final y(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 1207
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    if-eqz v0, :cond_0

    .line 1208
    iget-object v0, p0, Lcom/google/android/gms/games/internal/bv;->b:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/games/internal/o;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/o;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    .line 1210
    :cond_0
    return-void
.end method
