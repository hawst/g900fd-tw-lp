.class public final Lcom/google/android/gms/udc/ui/z;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/p;


# instance fields
.field private a:Lcom/google/android/gms/udc/e/g;

.field private b:Lcom/google/android/gms/udc/ui/aa;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 142
    return-void
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/udc/e/g;)Lcom/google/android/gms/udc/ui/z;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Landroid/os/Bundle;

    const/4 v1, 0x2

    invoke-direct {v0, v1}, Landroid/os/Bundle;-><init>(I)V

    .line 43
    const-string v1, "UdcOverviewAccount"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 44
    const-string v1, "UdcOverviewConfig"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/udc/util/f;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)V

    .line 46
    new-instance v1, Lcom/google/android/gms/udc/ui/z;

    invoke-direct {v1}, Lcom/google/android/gms/udc/ui/z;-><init>()V

    .line 47
    invoke-virtual {v1, v0}, Lcom/google/android/gms/udc/ui/z;->setArguments(Landroid/os/Bundle;)V

    .line 49
    return-object v1
.end method


# virtual methods
.method public final onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 55
    instance-of v0, p1, Lcom/google/android/gms/udc/ui/aa;

    if-eqz v0, :cond_0

    .line 56
    check-cast p1, Lcom/google/android/gms/udc/ui/aa;

    iput-object p1, p0, Lcom/google/android/gms/udc/ui/z;->b:Lcom/google/android/gms/udc/ui/aa;

    return-void

    .line 58
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attaching activity must implement OnSettingSelectedListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final onClick(Landroid/view/View;Lcom/google/android/gms/common/widget/a/o;)V
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p2}, Lcom/google/android/gms/common/widget/a/o;->g()I

    move-result v0

    .line 135
    if-ltz v0, :cond_1

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/z;->b:Lcom/google/android/gms/udc/ui/aa;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/z;->a:Lcom/google/android/gms/udc/e/g;

    iget-object v2, v2, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    aget-object v0, v2, v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/udc/ui/aa;->a(Lcom/google/android/gms/udc/e/m;)V

    .line 140
    :cond_0
    :goto_0
    return-void

    .line 137
    :cond_1
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/z;->b:Lcom/google/android/gms/udc/ui/aa;

    invoke-interface {v0}, Lcom/google/android/gms/udc/ui/aa;->e()V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 65
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/z;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 67
    const-string v1, "UdcOverviewConfig"

    new-instance v2, Lcom/google/android/gms/udc/e/g;

    invoke-direct {v2}, Lcom/google/android/gms/udc/e/g;-><init>()V

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/udc/util/f;->b(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/g;

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/z;->a:Lcom/google/android/gms/udc/e/g;

    .line 68
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/z;->a:Lcom/google/android/gms/udc/e/g;

    const-string v1, "Fragment requires consent configuration"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    return-void
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 76
    sget v0, Lcom/google/android/gms/l;->ai:I

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 78
    if-nez v0, :cond_0

    .line 79
    const-string v0, "UdcSettingsListFragment"

    const-string v1, "Unable to inflate fragment view"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    const/4 v0, 0x0

    .line 128
    :goto_0
    return-object v0

    .line 83
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    .line 84
    new-instance v4, Lcom/google/android/gms/common/widget/a/l;

    invoke-direct {v4, v3}, Lcom/google/android/gms/common/widget/a/l;-><init>(Landroid/content/Context;)V

    .line 85
    iget-object v5, v4, Lcom/google/android/gms/common/widget/a/l;->c:Lcom/google/android/gms/common/widget/a/i;

    .line 87
    new-instance v2, Lcom/google/android/gms/udc/f/a;

    invoke-direct {v2}, Lcom/google/android/gms/udc/f/a;-><init>()V

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 90
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/z;->a:Lcom/google/android/gms/udc/e/g;

    iget-object v2, v2, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    array-length v6, v2

    move v2, v1

    .line 91
    :goto_1
    if-ge v2, v6, :cond_4

    .line 92
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/z;->a:Lcom/google/android/gms/udc/e/g;

    iget-object v1, v1, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    aget-object v7, v1, v2

    .line 95
    iget-object v1, v7, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v1, v1, Lcom/google/android/gms/udc/e/p;->a:I

    const/4 v8, 0x2

    if-ne v1, v8, :cond_1

    sget-object v1, Lcom/google/android/gms/udc/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-nez v1, :cond_2

    .line 97
    :cond_1
    iget-object v1, v7, Lcom/google/android/gms/udc/e/m;->e:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v1

    .line 103
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_3

    .line 104
    new-instance v8, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v8, v3}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    .line 105
    invoke-virtual {v8, v2}, Lcom/google/android/gms/common/widget/a/o;->b(I)V

    .line 106
    invoke-virtual {v8, v1}, Lcom/google/android/gms/common/widget/a/o;->a(Ljava/lang/CharSequence;)V

    .line 107
    iget-object v1, v7, Lcom/google/android/gms/udc/e/m;->d:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v1

    invoke-virtual {v8, v1}, Lcom/google/android/gms/common/widget/a/o;->b(Ljava/lang/CharSequence;)V

    .line 108
    invoke-virtual {v8, p0}, Lcom/google/android/gms/common/widget/a/o;->a(Lcom/google/android/gms/common/widget/a/p;)V

    .line 109
    invoke-virtual {v8, v2}, Lcom/google/android/gms/common/widget/a/o;->a(I)V

    .line 110
    invoke-virtual {v5, v8}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 91
    :cond_2
    :goto_2
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 112
    :cond_3
    const-string v1, "UdcSettingsListFragment"

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "Drop nameless setting:"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v7, v7, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v7, v7, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v1, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 117
    :cond_4
    sget-object v1, Lcom/google/android/gms/udc/b/a;->i:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 118
    new-instance v1, Lcom/google/android/gms/common/widget/a/o;

    invoke-direct {v1, v3}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    .line 119
    const/4 v2, -0x1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/widget/a/o;->b(I)V

    .line 120
    sget v2, Lcom/google/android/gms/p;->yX:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/widget/a/o;->c(I)V

    .line 121
    invoke-virtual {v1, p0}, Lcom/google/android/gms/common/widget/a/o;->a(Lcom/google/android/gms/common/widget/a/p;)V

    .line 122
    invoke-virtual {v5, v1}, Lcom/google/android/gms/common/widget/a/i;->c(Lcom/google/android/gms/common/widget/a/e;)V

    .line 125
    :cond_5
    const v1, 0x102000a

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/widget/a/l;->a(Landroid/support/v7/widget/RecyclerView;)V

    goto/16 :goto_0
.end method
