.class public final Lcom/google/android/gms/games/internal/dq;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/internal/az;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 22
    new-instance v0, Lcom/google/android/gms/common/internal/az;

    const-string v1, "Games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/az;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 72
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    new-instance v1, Ljava/lang/Throwable;

    invoke-direct {v1}, Ljava/lang/Throwable;-><init>()V

    invoke-virtual {v0, p0, p1, p2, v1}, Lcom/google/android/gms/common/internal/az;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 73
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 76
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/az;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 77
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 33
    return-void
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/gms/common/internal/az;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 53
    return-void
.end method

.method public static a()Z
    .locals 2

    .prologue
    .line 28
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    move-result v0

    return v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 80
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/az;->a(I)Z

    .line 81
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method public static b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 60
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/gms/common/internal/az;->b(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 61
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 49
    return-void
.end method

.method public static c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 68
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1, p2}, Lcom/google/android/gms/common/internal/az;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 69
    return-void
.end method

.method public static d(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 56
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method public static e(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    sget-object v0, Lcom/google/android/gms/games/internal/dq;->a:Lcom/google/android/gms/common/internal/az;

    invoke-virtual {v0, p0, p1}, Lcom/google/android/gms/common/internal/az;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 65
    return-void
.end method
