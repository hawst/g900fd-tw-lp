.class public abstract Lcom/google/android/gms/common/data/i;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final b_:Lcom/google/android/gms/common/data/DataHolder;

.field private c:I

.field protected c_:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    .line 34
    invoke-virtual {p0, p2}, Lcom/google/android/gms/common/data/i;->a(I)V

    .line 35
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 42
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-ge p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 43
    iput p1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/data/i;->c:I

    .line 45
    return-void

    .line 42
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V
    .locals 3

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2, p2}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;IILandroid/database/CharArrayBuffer;)V

    .line 175
    return-void
.end method

.method public b(Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;II)J

    move-result-wide v0

    return-wide v0
.end method

.method public c(Ljava/lang/String;)I
    .locals 3

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->b(Ljava/lang/String;II)I

    move-result v0

    return v0
.end method

.method public final c_(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/data/DataHolder;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public d(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method public e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 196
    instance-of v1, p1, Lcom/google/android/gms/common/data/i;

    if-eqz v1, :cond_0

    .line 197
    check-cast p1, Lcom/google/android/gms/common/data/i;

    .line 198
    iget v1, p1, Lcom/google/android/gms/common/data/i;->c_:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c_:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget v1, p1, Lcom/google/android/gms/common/data/i;->c:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    .line 202
    :cond_0
    return v0
.end method

.method public f(Ljava/lang/String;)F
    .locals 3

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->e(Ljava/lang/String;II)F

    move-result v0

    return v0
.end method

.method public g(Ljava/lang/String;)D
    .locals 3

    .prologue
    .line 136
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->f(Ljava/lang/String;II)D

    move-result-wide v0

    return-wide v0
.end method

.method protected final h(Ljava/lang/String;)[B
    .locals 3

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->g(Ljava/lang/String;II)[B

    move-result-object v0

    return-object v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 191
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c_:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method protected final i(Ljava/lang/String;)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->h(Ljava/lang/String;II)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final j(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    iget v1, p0, Lcom/google/android/gms/common/data/i;->c_:I

    iget v2, p0, Lcom/google/android/gms/common/data/i;->c:I

    invoke-virtual {v0, p1, v1, v2}, Lcom/google/android/gms/common/data/DataHolder;->i(Ljava/lang/String;II)Z

    move-result v0

    return v0
.end method

.method protected final u_()I
    .locals 1

    .prologue
    .line 38
    iget v0, p0, Lcom/google/android/gms/common/data/i;->c_:I

    return v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/data/i;->b_:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->i()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
