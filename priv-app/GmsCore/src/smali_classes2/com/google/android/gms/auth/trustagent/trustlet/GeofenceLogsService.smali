.class public Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/common/api/v;

.field private b:Lcom/google/android/gms/common/api/x;

.field private c:Lcom/google/android/gms/common/api/y;

.field private d:Ljava/util/Set;

.field private e:Ljava/util/Set;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)V
    .locals 2

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->b:Lcom/google/android/gms/common/api/x;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->c:Lcom/google/android/gms/common/api/y;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/y;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->stopSelf()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;Landroid/location/Location;Ljava/util/Set;)V
    .locals 2

    .prologue
    .line 37
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/o;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/auth/trustagent/trustlet/o;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;Landroid/location/Location;Ljava/util/Set;)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/o;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->d:Ljava/util/Set;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->e:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 62
    if-nez p1, :cond_0

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->stopSelf()V

    .line 84
    :goto_0
    const/4 v0, 0x2

    return v0

    .line 65
    :cond_0
    const-string v1, "auth_trust_agent_trusted_places_id_list_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    .line 67
    const-string v1, "auth_trust_agent_entered_trusted_places_id_list_key"

    invoke-virtual {p1, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 69
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->d:Ljava/util/Set;

    .line 70
    array-length v4, v2

    move v1, v0

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v2, v1

    .line 71
    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->d:Ljava/util/Set;

    invoke-interface {v6, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 70
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 73
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->e:Ljava/util/Set;

    .line 74
    array-length v1, v3

    :goto_2
    if-ge v0, v1, :cond_2

    aget-object v2, v3, v0

    .line 75
    iget-object v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->e:Ljava/util/Set;

    invoke-interface {v4, v2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 74
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 78
    :cond_2
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/places/q;->b:Lcom/google/android/gms/common/api/c;

    sget-object v2, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a:Lcom/google/android/gms/location/places/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    .line 81
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/l;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->b:Lcom/google/android/gms/common/api/x;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->b:Lcom/google/android/gms/common/api/x;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/n;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->c:Lcom/google/android/gms/common/api/y;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->c:Lcom/google/android/gms/common/api/y;

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    goto :goto_0
.end method
