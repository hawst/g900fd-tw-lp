.class public final Lcom/google/android/gms/clearcut/a/h;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 30
    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 32
    iput-object p5, p0, Lcom/google/android/gms/clearcut/a/h;->a:Ljava/lang/String;

    .line 33
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 18
    invoke-static {p1}, Lcom/google/android/gms/clearcut/a/m;->a(Landroid/os/IBinder;)Lcom/google/android/gms/clearcut/a/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/clearcut/a/i;Lcom/google/android/gms/clearcut/LogEventParcelable;)V
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/gms/clearcut/a/h;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/clearcut/a/l;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/clearcut/a/l;->a(Lcom/google/android/gms/clearcut/a/i;Lcom/google/android/gms/clearcut/LogEventParcelable;)V

    .line 69
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 58
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 60
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->d(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 63
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    const-string v0, "com.google.android.gms.clearcut.service.START"

    return-object v0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 42
    const-string v0, "com.google.android.gms.clearcut.internal.IClearcutLoggerService"

    return-object v0
.end method

.method protected final g_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a/h;->a:Ljava/lang/String;

    return-object v0
.end method
