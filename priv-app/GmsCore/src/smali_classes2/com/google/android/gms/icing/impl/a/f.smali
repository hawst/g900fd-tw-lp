.class public final Lcom/google/android/gms/icing/impl/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/icing/impl/a/g;

.field public final b:Ljava/util/Map;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/a/g;)V
    .locals 1

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 126
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    .line 127
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/f;->e()V

    .line 129
    return-void
.end method

.method private e()V
    .locals 2

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/q;->q()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 133
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    goto :goto_0

    .line 135
    :cond_0
    return-void
.end method

.method private f()Ljava/util/Set;
    .locals 1

    .prologue
    .line 222
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-static {}, Lcom/google/android/gms/icing/impl/a/g;->c()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(ILjava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/icing/impl/a/g;->a(Ljava/lang/String;)Z

    move-result v5

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v0

    iget v0, v0, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v0, p1, :cond_2

    move v7, v1

    .line 149
    :goto_0
    if-eqz v5, :cond_3

    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v3, v1

    .line 150
    :goto_1
    if-eqz v5, :cond_4

    const-string v0, "com.google.android.apps.icing.ui"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    move v6, v1

    .line 151
    :goto_2
    if-eqz v5, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/f;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, p2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    move v4, v1

    .line 154
    :goto_3
    new-instance v0, Lcom/google/android/gms/icing/impl/a/aa;

    if-nez v7, :cond_0

    if-eqz v6, :cond_1

    :cond_0
    move v2, v1

    :cond_1
    move v1, p1

    move-object v6, p2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/a/aa;-><init>(IZZZZLjava/lang/String;)V

    return-object v0

    :cond_2
    move v7, v2

    .line 148
    goto :goto_0

    :cond_3
    move v3, v2

    .line 149
    goto :goto_1

    :cond_4
    move v6, v2

    .line 150
    goto :goto_2

    :cond_5
    move v4, v2

    .line 151
    goto :goto_3
.end method

.method public final a(Landroid/content/pm/ApplicationInfo;)Lcom/google/android/gms/icing/impl/a/aa;
    .locals 2

    .prologue
    .line 179
    iget v0, p1, Landroid/content/pm/ApplicationInfo;->uid:I

    iget-object v1, p1, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/icing/impl/a/f;->a(ILjava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;
    .locals 3

    .prologue
    .line 165
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    const-string v0, "Package name"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v2, v0}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 168
    invoke-virtual {p0, v1, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(ILjava/lang/String;)Lcom/google/android/gms/icing/impl/a/aa;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/aa;)Lcom/google/android/gms/icing/impl/a/e;
    .locals 2

    .prologue
    .line 238
    iget-object v1, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    .line 239
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 240
    invoke-virtual {p0, v1}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    return-object v0

    .line 239
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a()Lcom/google/android/gms/icing/impl/a/h;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 211
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    invoke-virtual {v2, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_8

    array-length v2, v5

    if-lez v2, :cond_8

    new-instance v3, Ljava/util/HashSet;

    array-length v0, v5

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    move v0, v4

    move v2, v4

    :goto_0
    array-length v7, v5

    if-ge v0, v7, :cond_2

    if-nez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    aget-object v7, v5, v0

    invoke-virtual {v2, v7}, Lcom/google/android/gms/icing/impl/a/g;->a(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    move v2, v6

    :goto_1
    aget-object v7, v5, v0

    invoke-interface {v3, v7}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v2, v4

    goto :goto_1

    :cond_2
    move-object v0, v3

    move v5, v2

    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget v2, v2, Landroid/content/pm/ApplicationInfo;->uid:I

    if-ne v2, v1, :cond_4

    move v7, v6

    :goto_3
    if-eqz v5, :cond_5

    const-string v2, "com.google.android.googlequicksearchbox"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    move v3, v6

    :goto_4
    if-eqz v5, :cond_6

    const-string v2, "com.google.android.apps.icing.ui"

    invoke-interface {v0, v2}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_6

    move v2, v6

    :goto_5
    new-instance v0, Lcom/google/android/gms/icing/impl/a/h;

    if-nez v7, :cond_3

    if-eqz v2, :cond_7

    :cond_3
    move v2, v6

    :goto_6
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/a/h;-><init>(IZZZZ)V

    return-object v0

    :cond_4
    move v7, v4

    goto :goto_3

    :cond_5
    move v3, v4

    goto :goto_4

    :cond_6
    move v2, v4

    goto :goto_5

    :cond_7
    move v2, v4

    goto :goto_6

    :cond_8
    move v5, v4

    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/h;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget v1, p1, Lcom/google/android/gms/icing/impl/a/h;->a:I

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v2

    .line 249
    if-eqz v2, :cond_0

    array-length v0, v2

    if-nez v0, :cond_1

    .line 250
    :cond_0
    const-string v0, "No packages found for UID %d"

    iget v1, p1, Lcom/google/android/gms/icing/impl/a/h;->a:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 251
    invoke-static {}, Ljava/util/Collections;->emptySet()Ljava/util/Set;

    move-result-object v0

    .line 257
    :goto_0
    return-object v0

    .line 253
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    array-length v0, v2

    invoke-direct {v1, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 254
    const/4 v0, 0x0

    :goto_1
    array-length v3, v2

    if-ge v0, v3, :cond_2

    .line 255
    aget-object v3, v2, v0

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 254
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    :cond_2
    move-object v0, v1

    .line 257
    goto :goto_0
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/f;->f()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 230
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    invoke-virtual {p1, v0}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    goto :goto_0

    .line 232
    :cond_0
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 10

    .prologue
    .line 418
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 419
    :try_start_0
    const-string v0, "\nRegistered client info:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->write(Ljava/lang/String;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 421
    iget-object v3, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->x()Ljava/lang/Object;

    move-result-object v3

    monitor-enter v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    :try_start_1
    const-string v4, "\nInfo for package %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->a()Landroid/support/v4/g/o;

    move-result-object v4

    const-string v5, "\n  version code: %d cert: %s"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, v4, Landroid/support/v4/g/o;->a:Ljava/lang/Object;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v4, v4, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    aput-object v4, v6, v7

    invoke-virtual {p1, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    iget-object v4, v0, Lcom/google/android/gms/icing/impl/a/e;->b:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v4, v4, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v4

    const-string v5, "\n  version name: %s"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v4, v4, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;

    aput-object v4, v6, v7

    invoke-virtual {p1, v5, v6}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_2
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_1
    :try_start_3
    const-string v4, "\n  fingerprint: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v4, "\n  resource fingerprint: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/icing/impl/q;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v4, "\n  blocked: %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/icing/impl/q;->g(Ljava/lang/String;)Z

    move-result v7

    invoke-static {v7}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/impl/q;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/c/a/g;

    move-result-object v4

    if-eqz v4, :cond_0

    iget-boolean v4, v4, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-eqz v4, :cond_0

    const-string v4, "\n  ***blacklisted***"

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    :cond_0
    const-string v4, "\n  global search info:"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    const-string v4, "\n    sourced at %d %s"

    const/4 v5, 0x2

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/icing/impl/q;->l(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v8, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v8}, Lcom/google/android/gms/icing/impl/q;->k(Ljava/lang/String;)I

    move-result v7

    invoke-static {v7}, Lcom/google/android/gms/icing/impl/a/ac;->b(I)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    const-string v4, "\n    %s"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v7, v0}, Lcom/google/android/gms/icing/impl/q;->i(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    aput-object v0, v5, v6

    invoke-virtual {p1, v4, v5}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;

    monitor-exit v3
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    :try_start_4
    monitor-exit v3

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 422
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 421
    :catch_0
    move-exception v4

    :try_start_5
    const-string v4, "\n  failed to get package info for this client."

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_1

    .line 422
    :cond_1
    :try_start_6
    monitor-exit v1
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    return-void
.end method

.method public final b()Ljava/util/Set;
    .locals 5

    .prologue
    .line 265
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 266
    :try_start_0
    new-instance v2, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 267
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 268
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->c()I

    move-result v4

    if-lez v4, :cond_0

    .line 269
    invoke-virtual {v2, v0}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 272
    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final b(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 215
    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/g;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;
    .locals 2

    .prologue
    .line 280
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 281
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 282
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final c()Ljava/util/Set;
    .locals 3

    .prologue
    .line 301
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 302
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 303
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;
    .locals 3

    .prologue
    .line 289
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 290
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 291
    if-nez v0, :cond_0

    .line 292
    new-instance v0, Lcom/google/android/gms/icing/impl/a/e;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-direct {v0, p1, v2}, Lcom/google/android/gms/icing/impl/a/e;-><init>(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/g;)V

    .line 293
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v2, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 295
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 296
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Ljava/util/Set;
    .locals 3

    .prologue
    .line 310
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 311
    :try_start_0
    new-instance v0, Ljava/util/HashSet;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 312
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e(Ljava/lang/String;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 327
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v2

    .line 328
    if-nez v2, :cond_1

    .line 345
    :cond_0
    :goto_0
    return v0

    .line 330
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/e;->f()Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v2

    .line 331
    if-eqz v2, :cond_0

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 336
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v3, v3, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    const/4 v4, 0x0

    invoke-virtual {v3, p1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    .line 337
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v4

    iget-wide v2, v3, Landroid/content/pm/PackageInfo;->lastUpdateTime:J

    cmp-long v2, v4, v2

    if-gez v2, :cond_2

    .line 338
    const-string v2, "GSAI from package %s is stale."

    invoke-static {v2, p1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 341
    :catch_0
    move-exception v2

    const-string v3, "Cannot find package %s"

    new-array v1, v1, [Ljava/lang/Object;

    aput-object p1, v1, v0

    invoke-static {v2, v3, v1}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0

    :cond_2
    move v0, v1

    .line 345
    goto :goto_0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 353
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->b:Ljava/util/Map;

    monitor-enter v1

    .line 355
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 356
    if-eqz v0, :cond_0

    .line 359
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/icing/impl/a/ac;->a()Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v2

    .line 360
    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/a/e;->a(Lcom/google/android/gms/icing/impl/a/ac;)V
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/b/a; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 365
    :try_start_2
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v2, p1}, Lcom/google/android/gms/icing/impl/q;->f(Ljava/lang/String;)V

    .line 366
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->k()V

    .line 368
    :cond_0
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 369
    if-eqz v0, :cond_1

    .line 370
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/g;->d:Lcom/google/android/gms/icing/impl/a/z;

    invoke-interface {v1, v0}, Lcom/google/android/gms/icing/impl/a/z;->a(Lcom/google/android/gms/icing/impl/a/e;)V

    .line 372
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/g;->d:Lcom/google/android/gms/icing/impl/a/z;

    invoke-interface {v1, v0}, Lcom/google/android/gms/icing/impl/a/z;->c(Lcom/google/android/gms/icing/impl/a/e;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 373
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/e;->a(J)Z

    .line 376
    :cond_1
    return-void

    .line 361
    :catch_0
    move-exception v0

    .line 363
    :try_start_3
    new-instance v2, Lcom/google/android/gms/icing/impl/b/c;

    invoke-direct {v2, v0}, Lcom/google/android/gms/icing/impl/b/c;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
