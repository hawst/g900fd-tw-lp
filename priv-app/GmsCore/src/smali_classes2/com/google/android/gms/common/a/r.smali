.class public abstract Lcom/google/android/gms/common/a/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field d:Lcom/google/android/gms/common/a/m;

.field final e:Ljava/lang/String;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/common/a/m;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 183
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 184
    iput-object p1, p0, Lcom/google/android/gms/common/a/r;->d:Lcom/google/android/gms/common/a/m;

    .line 185
    iput-object p2, p0, Lcom/google/android/gms/common/a/r;->e:Ljava/lang/String;

    .line 186
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/common/a/r;->d:Lcom/google/android/gms/common/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/m;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/a/r;->a(Landroid/content/SharedPreferences;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(Landroid/content/SharedPreferences;)Ljava/lang/Object;
.end method

.method protected abstract a(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)V
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/gms/common/a/r;->d:Lcom/google/android/gms/common/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/m;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    .line 207
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 208
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/common/a/r;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/Object;)V

    .line 209
    invoke-static {v0}, Lcom/google/android/gms/common/a/m;->a(Landroid/content/SharedPreferences$Editor;)Z

    .line 210
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 214
    iget-object v0, p0, Lcom/google/android/gms/common/a/r;->d:Lcom/google/android/gms/common/a/m;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/m;->a()Landroid/content/SharedPreferences;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/a/r;->e:Ljava/lang/String;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/m;->a(Landroid/content/SharedPreferences$Editor;)Z

    .line 215
    return-void
.end method
