.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;
.super Lcom/google/android/gms/games/realtime/network/s;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Ljava/lang/String;

.field private j:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V
    .locals 0

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/s;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    .line 215
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->e:Ljava/lang/String;

    .line 222
    iput p2, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->j:I

    .line 223
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->c()V

    .line 224
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 228
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 264
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 265
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->f:Z

    :goto_0
    return v0

    .line 230
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/c;

    .line 231
    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/c;->a:Z

    if-eqz v0, :cond_0

    .line 232
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->e:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->j:I

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/realtime/network/a;->a(Ljava/lang/String;I)V

    .line 233
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->d:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectedState;->a(Ljava/lang/String;)V

    .line 246
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->f:Z

    goto :goto_0

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->e(Ljava/lang/String;)V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->f:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/v;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/realtime/network/v;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->a(Ljava/lang/Runnable;)V

    goto :goto_1

    .line 249
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/k;

    .line 250
    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/k;->a:Z

    if-nez v0, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/a;->b()V

    .line 252
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b()V

    .line 254
    :cond_1
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->f:Z

    goto :goto_0

    .line 257
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a()V

    .line 258
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/service/statemachine/d;->b()V

    .line 259
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->f:Z

    goto :goto_0

    .line 228
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0xc9 -> :sswitch_0
        0x1771 -> :sswitch_2
    .end sparse-switch
.end method
