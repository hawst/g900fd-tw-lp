.class final Lcom/google/android/gms/common/api/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/at;

.field private final b:I

.field private final c:Lcom/google/android/gms/common/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/at;ILcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 346
    iput-object p1, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 347
    iput p2, p0, Lcom/google/android/gms/common/api/aw;->b:I

    .line 348
    iput-object p3, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    .line 349
    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    .line 353
    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/v;->f()Ljava/util/List;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-interface {v0, v1}, Ljava/util/List;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 359
    add-int/lit8 v0, v0, 0x1

    shl-int/lit8 v0, v0, 0x10

    add-int/lit8 v0, v0, 0x1

    .line 360
    iget-object v1, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    iget-object v2, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/c;->a(Landroid/app/Activity;I)V
    :try_end_0
    .catch Landroid/content/IntentSender$SendIntentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 376
    :goto_0
    return-void

    .line 363
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-static {v0}, Lcom/google/android/gms/common/api/at;->a(Lcom/google/android/gms/common/api/at;)V

    goto :goto_0

    .line 365
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/ew;->c(I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 367
    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/gms/common/ew;->a(ILandroid/app/Activity;Landroid/support/v4/app/Fragment;ILandroid/content/DialogInterface$OnCancelListener;)Z

    goto :goto_0

    .line 374
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/aw;->a:Lcom/google/android/gms/common/api/at;

    iget v1, p0, Lcom/google/android/gms/common/api/aw;->b:I

    iget-object v2, p0, Lcom/google/android/gms/common/api/aw;->c:Lcom/google/android/gms/common/c;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/api/at;->a(Lcom/google/android/gms/common/api/at;ILcom/google/android/gms/common/c;)V

    goto :goto_0
.end method
