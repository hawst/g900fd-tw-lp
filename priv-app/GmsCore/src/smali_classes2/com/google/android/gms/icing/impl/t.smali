.class final Lcom/google/android/gms/icing/impl/t;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/Map;

.field private final b:Lcom/google/android/gms/icing/aa;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/aa;)V
    .locals 1

    .prologue
    .line 1150
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1145
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/t;->a:Ljava/util/Map;

    .line 1151
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/t;->b:Lcom/google/android/gms/icing/aa;

    .line 1152
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1159
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    move v1, v2

    .line 1162
    :goto_0
    iget-object v0, p1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 1165
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/t;->a:Ljava/util/Map;

    iget-object v4, p1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1166
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/t;->a:Ljava/util/Map;

    iget-object v4, p1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ab;

    .line 1171
    :goto_1
    if-eqz v0, :cond_0

    .line 1172
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ab;

    .line 1174
    iput v1, v0, Lcom/google/android/gms/icing/ab;->a:I

    .line 1175
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1162
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1168
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/t;->a:Ljava/util/Map;

    const-string v4, ""

    invoke-interface {v0, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/ab;

    goto :goto_1

    .line 1179
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/t;->b:Lcom/google/android/gms/icing/aa;

    new-array v0, v2, [Lcom/google/android/gms/icing/ab;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/ab;

    iput-object v0, v1, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    .line 1181
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/t;->b:Lcom/google/android/gms/icing/aa;

    return-object v0
.end method
