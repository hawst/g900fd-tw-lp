.class public final Lcom/google/android/gms/backup/b/a;
.super Ljava/io/InputStream;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/ae/a/a;

.field private final b:[B


# direct methods
.method public constructor <init>(Lcom/google/ae/a/a;)V
    .locals 1

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/io/InputStream;-><init>()V

    .line 15
    const/4 v0, 0x1

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/backup/b/a;->b:[B

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/backup/b/a;->a:Lcom/google/ae/a/a;

    .line 19
    return-void
.end method


# virtual methods
.method public final available()I
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/backup/b/a;->a:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->h()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/backup/b/a;->a:Lcom/google/ae/a/a;

    invoke-interface {v0}, Lcom/google/ae/a/a;->i()V

    .line 79
    return-void
.end method

.method public final mark(I)V
    .locals 0

    .prologue
    .line 52
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 23
    move v1, v2

    .line 24
    :cond_0
    if-nez v1, :cond_1

    .line 25
    iget-object v1, p0, Lcom/google/android/gms/backup/b/a;->b:[B

    invoke-virtual {p0, v1}, Lcom/google/android/gms/backup/b/a;->read([B)I

    move-result v1

    .line 26
    if-ne v1, v0, :cond_0

    .line 30
    :goto_0
    return v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/b/a;->b:[B

    aget-byte v0, v0, v2

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 35
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/backup/b/a;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/backup/b/a;->a:Lcom/google/ae/a/a;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/ae/a/a;->a([BII)I

    move-result v0

    .line 43
    if-nez v0, :cond_0

    .line 44
    const/4 v0, -0x1

    .line 46
    :cond_0
    return v0
.end method

.method public final declared-synchronized reset()V
    .locals 1

    .prologue
    .line 62
    monitor-enter p0

    :try_start_0
    new-instance v0, Ljava/io/IOException;

    invoke-direct {v0}, Ljava/io/IOException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/gms/backup/b/a;->a:Lcom/google/ae/a/a;

    invoke-interface {v0, p1, p2}, Lcom/google/ae/a/a;->a(J)J

    move-result-wide v0

    return-wide v0
.end method
