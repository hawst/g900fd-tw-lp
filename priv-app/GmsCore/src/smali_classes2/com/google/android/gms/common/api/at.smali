.class public final Lcom/google/android/gms/common/api/at;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnCancelListener;
.implements Landroid/support/v4/app/av;


# instance fields
.field private a:Z

.field private b:I

.field private c:Lcom/google/android/gms/common/c;

.field private final d:Landroid/os/Handler;

.field private final e:Landroid/util/SparseArray;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 72
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    .line 76
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/at;->d:Landroid/os/Handler;

    .line 79
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    .line 392
    return-void
.end method

.method public static a(Landroid/support/v4/app/q;)Lcom/google/android/gms/common/api/at;
    .locals 4

    .prologue
    .line 92
    const-string v0, "Must be called from main thread of process"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Ljava/lang/String;)V

    .line 95
    invoke-virtual {p0}, Landroid/support/v4/app/q;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    .line 98
    :try_start_0
    const-string v0, "GmsSupportLifecycleFragment"

    invoke-virtual {v1, v0}, Landroid/support/v4/app/v;->a(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/at;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/at;->isRemoving()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 105
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/api/at;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/at;-><init>()V

    .line 106
    invoke-virtual {v1}, Landroid/support/v4/app/v;->a()Landroid/support/v4/app/aj;

    move-result-object v2

    const-string v3, "GmsSupportLifecycleFragment"

    invoke-virtual {v2, v0, v3}, Landroid/support/v4/app/aj;->a(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/aj;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/aj;->a()I

    .line 107
    invoke-virtual {v1}, Landroid/support/v4/app/v;->b()Z

    .line 110
    :cond_1
    return-object v0

    .line 99
    :catch_0
    move-exception v0

    .line 100
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Fragment with tag GmsSupportLifecycleFragment is not a SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private a()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 324
    iput-boolean v1, p0, Lcom/google/android/gms/common/api/at;->a:Z

    .line 325
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    .line 326
    iput-object v6, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    .line 328
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v2

    move v0, v1

    .line 330
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v3}, Landroid/util/SparseArray;->size()I

    move-result v3

    if-ge v0, v3, :cond_1

    .line 331
    iget-object v3, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v3

    .line 332
    invoke-direct {p0, v3}, Lcom/google/android/gms/common/api/at;->c(I)Lcom/google/android/gms/common/api/au;

    move-result-object v4

    .line 333
    if-eqz v4, :cond_0

    .line 334
    iget-boolean v5, v4, Lcom/google/android/gms/common/api/au;->b:Z

    if-eqz v5, :cond_0

    iput-boolean v1, v4, Lcom/google/android/gms/common/api/au;->b:Z

    iget-boolean v5, v4, Landroid/support/v4/a/j;->p:Z

    if-eqz v5, :cond_0

    iget-boolean v5, v4, Landroid/support/v4/a/j;->q:Z

    if-nez v5, :cond_0

    iget-object v4, v4, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v4}, Lcom/google/android/gms/common/api/v;->b()V

    .line 336
    :cond_0
    invoke-virtual {v2, v3, v6, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 330
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 338
    :cond_1
    return-void
.end method

.method private a(ILcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 306
    const-string v0, "GmsSupportLifecycleFragment"

    const-string v1, "Unresolved error while connecting client. Stopping auto-manage."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 307
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/av;

    .line 308
    if-eqz v0, :cond_0

    .line 309
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/at;->b(I)V

    .line 310
    iget-object v0, v0, Lcom/google/android/gms/common/api/av;->b:Lcom/google/android/gms/common/api/y;

    .line 311
    if-eqz v0, :cond_0

    .line 312
    invoke-interface {v0, p2}, Lcom/google/android/gms/common/api/y;->a(Lcom/google/android/gms/common/c;)V

    .line 315
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/at;->a()V

    .line 316
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/at;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/gms/common/api/at;->a()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/at;ILcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/at;->a(ILcom/google/android/gms/common/c;)V

    return-void
.end method

.method private c(I)Lcom/google/android/gms/common/api/au;
    .locals 3

    .prologue
    .line 283
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/au;->b(I)Landroid/support/v4/a/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/au;
    :try_end_0
    .catch Ljava/lang/ClassCastException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v0

    .line 285
    :catch_0
    move-exception v0

    .line 286
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Unknown loader in SupportLifecycleFragment"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 3

    .prologue
    .line 262
    new-instance v1, Lcom/google/android/gms/common/api/au;

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/av;

    iget-object v0, v0, Lcom/google/android/gms/common/api/av;->a:Lcom/google/android/gms/common/api/v;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/api/au;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;)V

    return-object v1
.end method

.method public final a(I)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 123
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/at;->c(I)Lcom/google/android/gms/common/api/au;

    move-result-object v0

    .line 125
    if-eqz v0, :cond_0

    .line 126
    iget-object v0, v0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    .line 129
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(ILcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/y;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 149
    const-string v0, "GoogleApiClient instance cannot be null"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->indexOfKey(I)I

    move-result v0

    if-gez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Already managing a GoogleApiClient with id "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 153
    new-instance v0, Lcom/google/android/gms/common/api/av;

    invoke-direct {v0, p2, p3, v1}, Lcom/google/android/gms/common/api/av;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/common/api/y;B)V

    .line 154
    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v1, p1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 157
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 159
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 150
    goto :goto_0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 2

    .prologue
    .line 276
    iget v0, p1, Landroid/support/v4/a/j;->m:I

    iget v1, p0, Lcom/google/android/gms/common/api/at;->b:I

    if-ne v0, v1, :cond_0

    .line 277
    invoke-direct {p0}, Lcom/google/android/gms/common/api/at;->a()V

    .line 279
    :cond_0
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 39
    check-cast p2, Lcom/google/android/gms/common/c;

    invoke-virtual {p2}, Lcom/google/android/gms/common/c;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p1, Landroid/support/v4/a/j;->m:I

    iget v1, p0, Lcom/google/android/gms/common/api/at;->b:I

    if-ne v0, v1, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/at;->a()V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget v0, p1, Landroid/support/v4/a/j;->m:I

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/at;->a:Z

    if-nez v1, :cond_0

    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/common/api/at;->a:Z

    iput v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    iput-object p2, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->d:Landroid/os/Handler;

    new-instance v2, Lcom/google/android/gms/common/api/aw;

    invoke-direct {v2, p0, v0, p2}, Lcom/google/android/gms/common/api/aw;-><init>(Lcom/google/android/gms/common/api/at;ILcom/google/android/gms/common/c;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_0
.end method

.method public final b(I)V
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 170
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v4/app/au;->a(I)V

    .line 171
    return-void
.end method

.method public final onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 238
    const/4 v1, 0x0

    .line 239
    packed-switch p1, :pswitch_data_0

    :cond_0
    move v0, v1

    .line 253
    :goto_0
    if-eqz v0, :cond_1

    .line 254
    invoke-direct {p0}, Lcom/google/android/gms/common/api/at;->a()V

    .line 258
    :goto_1
    return-void

    .line 243
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/common/ew;->a(Landroid/content/Context;)I

    move-result v2

    if-nez v2, :cond_0

    goto :goto_0

    .line 248
    :pswitch_1
    const/4 v2, -0x1

    if-ne p2, v2, :cond_0

    goto :goto_0

    .line 256
    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/at;->a(ILcom/google/android/gms/common/c;)V

    goto :goto_1

    .line 239
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public final onAttach(Landroid/app/Activity;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 180
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 182
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    .line 184
    invoke-direct {p0, v2}, Lcom/google/android/gms/common/api/at;->c(I)Lcom/google/android/gms/common/api/au;

    move-result-object v3

    .line 186
    if-eqz v3, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/av;

    .line 188
    iget-object v0, v0, Lcom/google/android/gms/common/api/av;->a:Lcom/google/android/gms/common/api/v;

    iget-object v3, v3, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    if-eq v0, v3, :cond_0

    .line 189
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Landroid/support/v4/app/au;->b(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 182
    :goto_1
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 194
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v0

    invoke-virtual {v0, v2, v4, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    goto :goto_1

    .line 196
    :cond_1
    return-void
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 2

    .prologue
    .line 175
    iget v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/api/at;->a(ILcom/google/android/gms/common/c;)V

    .line 176
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 200
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 201
    if-eqz p1, :cond_0

    .line 202
    const-string v0, "resolving_error"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/at;->a:Z

    .line 203
    const-string v0, "failed_client_id"

    const/4 v1, -0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    .line 204
    iget v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    if-ltz v0, :cond_0

    .line 205
    new-instance v1, Lcom/google/android/gms/common/c;

    const-string v0, "failed_status"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    const-string v0, "failed_resolution"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V

    iput-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    .line 212
    :cond_0
    return-void
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 216
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 217
    const-string v0, "resolving_error"

    iget-boolean v1, p0, Lcom/google/android/gms/common/api/at;->a:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 218
    iget v0, p0, Lcom/google/android/gms/common/api/at;->b:I

    if-ltz v0, :cond_0

    .line 219
    const-string v0, "failed_client_id"

    iget v1, p0, Lcom/google/android/gms/common/api/at;->b:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 220
    const-string v0, "failed_status"

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {v1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 221
    const-string v0, "failed_resolution"

    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {v1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 223
    :cond_0
    return-void
.end method

.method public final onStart()V
    .locals 4

    .prologue
    .line 227
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 229
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/at;->a:Z

    if-nez v0, :cond_0

    .line 230
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 231
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/at;->getLoaderManager()Landroid/support/v4/app/au;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/api/at;->e:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3, p0}, Landroid/support/v4/app/au;->a(ILandroid/os/Bundle;Landroid/support/v4/app/av;)Landroid/support/v4/a/j;

    .line 230
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_0
    return-void
.end method
