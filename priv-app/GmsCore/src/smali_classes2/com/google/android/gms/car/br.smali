.class public final Lcom/google/android/gms/car/br;
.super Lcom/google/android/gms/car/kw;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/a/b;
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/d;


# static fields
.field private static final a:[I


# instance fields
.field private final b:I

.field private final c:I

.field private final d:Lcom/google/android/gms/car/a/k;

.field private final e:Lcom/google/android/gms/car/a/a;

.field private final f:Ljava/util/List;

.field private g:Lcom/google/android/gms/car/senderprotocol/c;

.field private h:Lcom/google/android/gms/car/a/g;

.field private i:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/car/br;->a:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x2
        0x4
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;[I)V
    .locals 8

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/gms/car/kw;-><init>()V

    .line 51
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/br;->i:Z

    .line 65
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 66
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CarBluetoothService. car address="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    :cond_0
    const-string v0, "SKIP_THIS_BLUETOOTH"

    invoke-virtual {v0, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 70
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 71
    const-string v0, "CAR.BT"

    const-string v1, "Special car Bluetooth address that should be skipped"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 73
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    .line 74
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/br;->c:I

    .line 75
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    .line 76
    new-instance v0, Lcom/google/android/gms/car/a/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/car/a/a;-><init>(Landroid/os/Looper;Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/b;)V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 133
    :goto_0
    return-void

    .line 81
    :cond_2
    const/4 v1, -0x1

    .line 82
    sget-object v4, Lcom/google/android/gms/car/br;->a:[I

    array-length v5, v4

    const/4 v0, 0x0

    move v3, v0

    :goto_1
    if-ge v3, v5, :cond_5

    aget v6, v4, v3

    .line 83
    array-length v7, p3

    const/4 v0, 0x0

    move v2, v0

    :goto_2
    if-ge v2, v7, :cond_9

    aget v0, p3, v2

    .line 84
    if-ne v0, v6, :cond_4

    .line 86
    const-string v1, "CAR.BT"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 87
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v6, "Bluetooth pairing method chosen: "

    invoke-direct {v2, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 82
    :cond_3
    :goto_3
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 83
    :cond_4
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 93
    :cond_5
    iput v1, p0, Lcom/google/android/gms/car/br;->c:I

    .line 94
    iget v0, p0, Lcom/google/android/gms/car/br;->c:I

    const/4 v1, -0x1

    if-ne v0, v1, :cond_7

    .line 95
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 96
    const-string v0, "CAR.BT"

    const-string v1, "No supported pairing method"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_6
    const/4 v0, -0x4

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    .line 100
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    .line 101
    new-instance v0, Lcom/google/android/gms/car/a/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/car/a/a;-><init>(Landroid/os/Looper;Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/b;)V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    .line 102
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto :goto_0

    .line 106
    :cond_7
    new-instance v0, Lcom/google/android/gms/car/a/k;

    new-instance v1, Lcom/google/android/gms/car/ch;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/gms/car/ch;-><init>(Lcom/google/android/gms/car/br;B)V

    invoke-direct {v0, p1, p2, v1}, Lcom/google/android/gms/car/a/k;-><init>(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/car/a/p;)V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    iget v0, v0, Lcom/google/android/gms/car/a/k;->k:I

    packed-switch v0, :pswitch_data_0

    .line 121
    :pswitch_0
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    .line 125
    :goto_4
    iget v0, p0, Lcom/google/android/gms/car/br;->b:I

    if-eqz v0, :cond_8

    .line 126
    new-instance v0, Lcom/google/android/gms/car/a/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/car/a/a;-><init>(Landroid/os/Looper;Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/b;)V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    const/16 v1, 0x64

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto/16 :goto_0

    .line 110
    :pswitch_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    goto :goto_4

    .line 113
    :pswitch_2
    const/4 v0, -0x3

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    goto :goto_4

    .line 117
    :pswitch_3
    const/4 v0, -0x5

    iput v0, p0, Lcom/google/android/gms/car/br;->b:I

    goto :goto_4

    .line 131
    :cond_8
    new-instance v0, Lcom/google/android/gms/car/a/a;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/car/a/a;-><init>(Landroid/os/Looper;Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/b;)V

    iput-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    const/16 v1, 0x65

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto/16 :goto_0

    :cond_9
    move v0, v1

    goto/16 :goto_3

    .line 108
    nop

    :pswitch_data_0
    .packed-switch -0x3
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/a/g;)Lcom/google/android/gms/car/a/g;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/gms/car/br;->h:Lcom/google/android/gms/car/a/g;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/senderprotocol/c;)Lcom/google/android/gms/car/senderprotocol/c;
    .locals 0

    .prologue
    .line 28
    iput-object p1, p0, Lcom/google/android/gms/car/br;->g:Lcom/google/android/gms/car/senderprotocol/c;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/co;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/car/co;)V
    .locals 6

    .prologue
    .line 519
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 520
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "deliverEventToClients. callbackinvoker="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 522
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/br;->b:I

    if-eqz v0, :cond_2

    .line 524
    const-string v0, "CAR.BT"

    const-string v1, "deliverEventToClients: Service not initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 550
    :cond_1
    :goto_0
    return-void

    .line 527
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/br;->g:Lcom/google/android/gms/car/senderprotocol/c;

    if-nez v0, :cond_3

    .line 529
    const-string v0, "CAR.BT"

    const-string v1, "deliverEventToClients: Channel not established yet"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 532
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/car/br;->i:Z

    if-nez v0, :cond_1

    .line 536
    iget-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 537
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 538
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cp;

    .line 540
    :try_start_0
    iget-object v1, v0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {p1, v1}, Lcom/google/android/gms/car/co;->a(Lcom/google/android/gms/car/ky;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 541
    :catch_0
    move-exception v1

    .line 542
    const-string v3, "CAR.BT"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Exception in deliverEventToClients. clientCallbackInvoker="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 546
    invoke-virtual {v0}, Lcom/google/android/gms/car/cp;->a()V

    .line 547
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V

    goto :goto_1
.end method

.method private a(Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 260
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 261
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handleIncomingMessage. handler="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 264
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/cb;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/cb;-><init>(Lcom/google/android/gms/car/br;Ljava/lang/Runnable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 285
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/br;)Z
    .locals 1

    .prologue
    .line 28
    iget-boolean v0, p0, Lcom/google/android/gms/car/br;->i:Z

    return v0
.end method

.method private a(Ljava/util/concurrent/Callable;)Z
    .locals 3

    .prologue
    .line 496
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "doBinderTask. task="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 500
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bx;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/bx;-><init>(Lcom/google/android/gms/car/br;Ljava/util/concurrent/Callable;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/br;)I
    .locals 1

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/car/br;->b:I

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/k;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/senderprotocol/c;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->g:Lcom/google/android/gms/car/senderprotocol/c;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/g;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->h:Lcom/google/android/gms/car/a/g;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/car/br;)V
    .locals 2

    .prologue
    .line 28
    iget v0, p0, Lcom/google/android/gms/car/br;->b:I

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Service not initialized"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/car/br;)Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/car/br;)V
    .locals 2

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/car/br;->g:Lcom/google/android/gms/car/senderprotocol/c;

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/br;->i:Z

    if-eqz v0, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 172
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 173
    const-string v0, "CAR.BT"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/br;->i:Z

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cp;

    .line 179
    invoke-virtual {v0}, Lcom/google/android/gms/car/cp;->a()V

    goto :goto_0

    .line 181
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/a;->a()V

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    if-eqz v0, :cond_3

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const-string v1, "CAR.BT"

    const-string v2, "cleanup"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_2
    iget v1, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v1, :cond_4

    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.BT"

    const-string v1, "cleanup: This object wasn\'t initialized successfully."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_3
    :goto_1
    return-void

    .line 186
    :cond_4
    const/4 v1, -0x1

    iput v1, v0, Lcom/google/android/gms/car/a/k;->k:I

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/a/k;->f:Lcom/google/android/gms/car/a/n;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/a/k;->g:Lcom/google/android/gms/car/a/s;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/a/k;->h:Lcom/google/android/gms/car/a/q;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/a/k;->i:Lcom/google/android/gms/car/a/r;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/car/a/k;->j:Lcom/google/android/gms/car/a/m;

    invoke-virtual {v1, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v1, v0, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    if-eqz v1, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/h;->e()V

    goto :goto_1
.end method

.method public final a(IZ)V
    .locals 3

    .prologue
    .line 210
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 211
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPairingResponse. status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " alreadyPaired="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/by;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/car/by;-><init>(Lcom/google/android/gms/car/br;IZ)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/lang/Runnable;)V

    .line 241
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 2

    .prologue
    .line 145
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 146
    const-string v0, "CAR.BT"

    const-string v1, "onEndPointReady"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bs;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/bs;-><init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/senderprotocol/az;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 167
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 5

    .prologue
    .line 192
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Preferred pairing method: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/br;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 193
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v0, "BluetoothUtil info: "

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    if-nez v0, :cond_0

    const-string v0, "null"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 195
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "BluetoothFsm info: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    invoke-static {}, Lcom/google/android/gms/car/a/a;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/car/br;->f:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/cp;

    .line 197
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v0}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 193
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v0, "Phone MAC addr="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v2, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    if-nez v0, :cond_1

    const-string v0, "null"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v0, ", peer MAC addr="

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v0, v2, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    if-nez v0, :cond_2

    const-string v0, "null"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", init status="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/google/android/gms/car/a/k;->k:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", ProfileUtil="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, v2, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", reqd pairing method="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/google/android/gms/car/a/k;->m:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", reqd pairing key="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v2, Lcom/google/android/gms/car/a/k;->n:I

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v4, ", auth data from client="

    invoke-direct {v0, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/gms/car/a/k;->o:Ljava/lang/String;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    :cond_1
    iget-object v0, v2, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_2

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 199
    :cond_3
    iget v0, p0, Lcom/google/android/gms/car/br;->b:I

    .line 200
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Initialization status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 201
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 246
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onAuthenticationData: auth data="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 250
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/ca;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/ca;-><init>(Lcom/google/android/gms/car/br;Ljava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/lang/Runnable;)V

    .line 256
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ky;)Z
    .locals 3

    .prologue
    .line 366
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 367
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "registerClient. client="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {p1}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/cf;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/cf;-><init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/ky;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public final b()V
    .locals 7

    .prologue
    const/4 v5, 0x3

    .line 294
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 295
    const-string v0, "CAR.BT"

    const-string v1, "prepareBluetoothProfileUtil"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 297
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/br;->h:Lcom/google/android/gms/car/a/g;

    if-eqz v0, :cond_2

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    iget-object v1, p0, Lcom/google/android/gms/car/br;->h:Lcom/google/android/gms/car/a/g;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(Lcom/google/android/gms/car/a/g;)V

    .line 299
    iget-object v0, p0, Lcom/google/android/gms/car/br;->e:Lcom/google/android/gms/car/a/a;

    const/16 v1, 0x69

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 314
    :cond_1
    :goto_0
    return-void

    .line 302
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    new-instance v2, Lcom/google/android/gms/car/cc;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/cc;-><init>(Lcom/google/android/gms/car/br;)V

    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.BT"

    const-string v3, "getProfileUtil"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget v0, v1, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_5

    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "CAR.BT"

    const-string v1, "getProfileUtil: This object wasn\'t initialized successfully."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    const/4 v0, 0x0

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/a/o;->a(Lcom/google/android/gms/car/a/g;)V

    goto :goto_0

    :cond_5
    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    if-eqz v0, :cond_a

    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    const-string v3, "CAR.BT"

    invoke-static {v3, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_6

    const-string v3, "CAR.BT"

    const-string v4, "isInitialized"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    iget-object v3, v0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v3, :cond_8

    iget-object v0, v0, Lcom/google/android/gms/car/a/h;->c:Landroid/bluetooth/BluetoothA2dp;

    if-eqz v0, :cond_8

    const/4 v0, 0x1

    :goto_1
    if-eqz v0, :cond_9

    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "CAR.BT"

    const-string v3, "mProfileUtil already initialized"

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    iget-object v0, v1, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/a/o;->a(Lcom/google/android/gms/car/a/g;)V

    goto :goto_0

    :cond_8
    const/4 v0, 0x0

    goto :goto_1

    :cond_9
    const-string v0, "CAR.BT"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.BT"

    const-string v1, "mProfileUtil not initialized yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_a
    new-instance v0, Lcom/google/android/gms/car/a/h;

    iget-object v3, v1, Lcom/google/android/gms/car/a/k;->a:Landroid/content/Context;

    iget-object v4, v1, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    iget-object v5, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    new-instance v6, Lcom/google/android/gms/car/a/l;

    invoke-direct {v6, v1, v2}, Lcom/google/android/gms/car/a/l;-><init>(Lcom/google/android/gms/car/a/k;Lcom/google/android/gms/car/a/o;)V

    invoke-direct {v0, v3, v4, v5, v6}, Lcom/google/android/gms/car/a/h;-><init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Ljava/lang/Runnable;)V

    iput-object v0, v1, Lcom/google/android/gms/car/a/k;->l:Lcom/google/android/gms/car/a/h;

    goto/16 :goto_0
.end method

.method public final c()V
    .locals 3

    .prologue
    const/4 v2, 0x3

    .line 319
    const-string v0, "CAR.BT"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 320
    const-string v0, "CAR.BT"

    const-string v1, "requestCarPairingPreparation"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a/k;->e()V

    .line 323
    const-string v0, "CAR.BT"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 324
    const-string v0, "CAR.BT"

    const-string v1, "Sending a pairing request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/br;->g:Lcom/google/android/gms/car/senderprotocol/c;

    iget-object v0, p0, Lcom/google/android/gms/car/br;->d:Lcom/google/android/gms/car/a/k;

    iget v2, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v2, :cond_2

    const/4 v0, 0x0

    :goto_0
    iget v2, p0, Lcom/google/android/gms/car/br;->c:I

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/senderprotocol/c;->a(Ljava/lang/String;I)V

    .line 328
    return-void

    .line 326
    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->b:Landroid/bluetooth/BluetoothAdapter;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothAdapter;->getAddress()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 332
    new-instance v0, Lcom/google/android/gms/car/cd;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/cd;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/co;)V

    .line 338
    return-void
.end method

.method public final e()I
    .locals 2

    .prologue
    .line 346
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 347
    const-string v0, "CAR.BT"

    const-string v1, "getInitializationStatus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/ce;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ce;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 403
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 404
    const-string v0, "CAR.BT"

    const-string v1, "isEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 406
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/cg;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/cg;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public final g()Z
    .locals 2

    .prologue
    .line 417
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "CAR.BT"

    const-string v1, "isPairing"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 420
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bt;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/bt;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 431
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 432
    const-string v0, "CAR.BT"

    const-string v1, "isPaired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 434
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bu;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/bu;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public final i()Z
    .locals 2

    .prologue
    .line 445
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 446
    const-string v0, "CAR.BT"

    const-string v1, "isHfpConnecting"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 448
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bv;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/bv;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method

.method public final j()Z
    .locals 2

    .prologue
    .line 462
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    const-string v0, "CAR.BT"

    const-string v1, "isHfpConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bw;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/bw;-><init>(Lcom/google/android/gms/car/br;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/br;->a(Ljava/util/concurrent/Callable;)Z

    move-result v0

    return v0
.end method
