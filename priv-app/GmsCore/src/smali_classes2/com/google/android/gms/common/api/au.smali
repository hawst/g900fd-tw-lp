.class final Lcom/google/android/gms/common/api/au;
.super Landroid/support/v4/a/j;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field public final a:Lcom/google/android/gms/common/api/v;

.field b:Z

.field private c:Lcom/google/android/gms/common/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 403
    invoke-direct {p0, p1}, Landroid/support/v4/a/j;-><init>(Landroid/content/Context;)V

    .line 404
    iput-object p2, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    .line 405
    return-void
.end method

.method private b(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 467
    iput-object p1, p0, Lcom/google/android/gms/common/api/au;->c:Lcom/google/android/gms/common/c;

    .line 468
    iget-boolean v0, p0, Landroid/support/v4/a/j;->p:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v4/a/j;->q:Z

    if-nez v0, :cond_0

    .line 469
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/au;->b(Ljava/lang/Object;)V

    .line 471
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 456
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/au;->b:Z

    .line 457
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/au;->b(Lcom/google/android/gms/common/c;)V

    .line 458
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 462
    invoke-super {p0, p1, p2, p3, p4}, Landroid/support/v4/a/j;->a(Ljava/lang/String;Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p1, p3}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    .line 464
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 446
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/au;->b:Z

    .line 447
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/au;->b(Lcom/google/android/gms/common/c;)V

    .line 448
    return-void
.end method

.method protected final e()V
    .locals 1

    .prologue
    .line 418
    invoke-super {p0}, Landroid/support/v4/a/j;->e()V

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->c:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_0

    .line 422
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->c:Lcom/google/android/gms/common/c;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/au;->b(Ljava/lang/Object;)V

    .line 425
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->g()Z

    move-result v0

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/au;->b:Z

    if-nez v0, :cond_1

    .line 426
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 428
    :cond_1
    return-void
.end method

.method protected final f()V
    .locals 1

    .prologue
    .line 432
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 433
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 452
    return-void
.end method

.method protected final g()V
    .locals 1

    .prologue
    .line 437
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/au;->c:Lcom/google/android/gms/common/c;

    .line 438
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/au;->b:Z

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    .line 440
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/y;)V

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/common/api/au;->a:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 442
    return-void
.end method
