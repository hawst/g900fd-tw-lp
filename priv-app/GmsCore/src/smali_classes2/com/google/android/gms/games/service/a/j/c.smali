.class public final Lcom/google/android/gms/games/service/a/j/c;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:[Ljava/lang/String;

.field private final f:[I

.field private final g:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[II)V
    .locals 1

    .prologue
    .line 25
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/j/c;->d:Lcom/google/android/gms/games/a/au;

    .line 27
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/j/c;->c:Lcom/google/android/gms/games/internal/dr;

    .line 28
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/j/c;->f:[I

    .line 29
    iput p4, p0, Lcom/google/android/gms/games/service/a/j/c;->g:I

    .line 30
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->e:[Ljava/lang/String;

    .line 31
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/j/c;->d:Lcom/google/android/gms/games/a/au;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/j/c;->c:Lcom/google/android/gms/games/internal/dr;

    .line 38
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/j/c;->e:[Ljava/lang/String;

    .line 39
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/service/a/j/c;->g:I

    .line 40
    sget-object v0, Lcom/google/android/gms/games/quest/e;->a:[I

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->f:[I

    .line 41
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 56
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->M(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 61
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->d:Lcom/google/android/gms/games/a/au;

    iget-boolean v0, v0, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/j/c;->f:[I

    iget v2, p0, Lcom/google/android/gms/games/service/a/j/c;->g:I

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/j/c;->e:[Ljava/lang/String;

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 50
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/c;->d:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/j/c;->f:[I

    iget v2, p0, Lcom/google/android/gms/games/service/a/j/c;->g:I

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/j/c;->e:[Ljava/lang/String;

    invoke-virtual {p2, v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;[II[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method
