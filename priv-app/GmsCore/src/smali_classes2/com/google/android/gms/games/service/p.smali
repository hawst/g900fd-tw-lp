.class public final Lcom/google/android/gms/games/service/p;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/common/server/ClientContext;

.field final b:Landroid/content/Context;

.field final c:Lcom/google/android/gms/common/util/p;

.field final d:Lcom/google/android/gms/games/service/r;

.field e:I

.field final f:Lcom/google/android/gms/games/service/l;

.field g:Ljava/lang/String;

.field h:J

.field i:Lcom/google/android/gms/ads/identifier/c;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;)V
    .locals 6

    .prologue
    .line 66
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/games/service/q;

    invoke-direct {v5}, Lcom/google/android/gms/games/service/q;-><init>()V

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/p;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/games/service/r;)V

    .line 96
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/common/util/p;Lcom/google/android/gms/games/service/r;)V
    .locals 0

    .prologue
    .line 101
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 102
    iput-object p1, p0, Lcom/google/android/gms/games/service/p;->b:Landroid/content/Context;

    .line 103
    iput-object p2, p0, Lcom/google/android/gms/games/service/p;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 104
    iput-object p3, p0, Lcom/google/android/gms/games/service/p;->f:Lcom/google/android/gms/games/service/l;

    .line 105
    iput-object p4, p0, Lcom/google/android/gms/games/service/p;->c:Lcom/google/android/gms/common/util/p;

    .line 106
    iput-object p5, p0, Lcom/google/android/gms/games/service/p;->d:Lcom/google/android/gms/games/service/r;

    .line 107
    return-void
.end method


# virtual methods
.method final a()V
    .locals 11

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/service/p;->c:Lcom/google/android/gms/common/util/p;

    invoke-interface {v0}, Lcom/google/android/gms/common/util/p;->b()J

    move-result-wide v8

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/games/service/p;->g:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/gms/games/service/p;->h:J

    cmp-long v0, v8, v0

    if-lez v0, :cond_0

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/games/service/p;->d:Lcom/google/android/gms/games/service/r;

    iget-object v2, p0, Lcom/google/android/gms/games/service/p;->b:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/service/p;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, p0, Lcom/google/android/gms/games/service/p;->f:Lcom/google/android/gms/games/service/l;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/l;->z()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/games/service/p;->g:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/gms/games/service/p;->h:J

    iget-object v10, p0, Lcom/google/android/gms/games/service/p;->i:Lcom/google/android/gms/ads/identifier/c;

    invoke-interface/range {v1 .. v10}, Lcom/google/android/gms/games/service/r;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;JJLcom/google/android/gms/ads/identifier/c;)V

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/p;->f:Lcom/google/android/gms/games/service/l;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/l;->H()V

    .line 166
    iget-object v0, p0, Lcom/google/android/gms/games/service/p;->f:Lcom/google/android/gms/games/service/l;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/l;->B()V

    .line 169
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/p;->g:Ljava/lang/String;

    .line 170
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/games/service/p;->h:J

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/service/p;->e:I

    .line 172
    return-void
.end method
