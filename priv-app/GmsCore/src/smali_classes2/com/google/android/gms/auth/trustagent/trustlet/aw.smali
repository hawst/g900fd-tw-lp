.class public Lcom/google/android/gms/auth/trustagent/trustlet/aw;
.super Lcom/google/android/gms/auth/trustagent/trustlet/bh;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/ad;
.implements Lcom/google/android/gms/auth/trustagent/trustlet/y;


# static fields
.field public static final a:Lcom/google/android/gms/location/places/s;


# instance fields
.field private b:Landroid/content/SharedPreferences;

.field private c:Landroid/content/SharedPreferences$Editor;

.field private d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private e:Lcom/google/android/gms/auth/trustagent/trustlet/az;

.field private i:Lcom/google/android/gms/common/api/v;

.field private j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

.field private k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

.field private l:Z

.field private m:Ljava/util/Set;

.field private n:Ljava/util/Set;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 76
    new-instance v0, Lcom/google/android/gms/location/places/t;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/t;-><init>()V

    const-string v1, "auth"

    iput-object v1, v0, Lcom/google/android/gms/location/places/t;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/t;->a()Lcom/google/android/gms/location/places/s;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a:Lcom/google/android/gms/location/places/s;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;-><init>()V

    .line 357
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/aw;Z)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 49
    new-instance v0, Lcom/google/android/gms/auth/e/f;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    iput v2, v0, Lcom/google/android/gms/auth/e/f;->d:I

    if-eqz p1, :cond_0

    const/4 v1, 0x1

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->h:I

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->size()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/auth/e/f;->i:I

    invoke-virtual {v0}, Lcom/google/android/gms/auth/e/f;->a()V

    return-void

    :cond_0
    iput v2, v0, Lcom/google/android/gms/auth/e/f;->h:I

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    return-object v0
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 318
    const-string v0, ""

    .line 319
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move-object v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 320
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 321
    goto :goto_0

    .line 322
    :cond_0
    const-string v0, "Coffee - PlaceTrustlet"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 323
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Entered Trusted Places Id List: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 324
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Nearby alert: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 326
    :cond_1
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Lcom/google/android/gms/auth/trustagent/trustlet/aa;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Lcom/google/android/gms/common/api/v;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i:Lcom/google/android/gms/common/api/v;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Z
    .locals 1

    .prologue
    .line 49
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Ljava/util/Set;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    return-object v0
.end method

.method private f()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    .line 219
    iput-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "android.intent.action.USER_PRESENT"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->e:Lcom/google/android/gms/auth/trustagent/trustlet/ae;

    iget-object v3, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->d:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/ab;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ab;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/aa;)V

    iput-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->g:Lcom/google/android/gms/common/api/x;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->g:Lcom/google/android/gms/common/api/x;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/ac;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ac;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/aa;)V

    iput-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->h:Lcom/google/android/gms/common/api/y;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->h:Lcom/google/android/gms/common/api/y;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 224
    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)Landroid/content/SharedPreferences$Editor;
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->c:Landroid/content/SharedPreferences$Editor;

    return-object v0
.end method

.method private h()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    sget-object v1, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a()Landroid/app/PendingIntent;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Landroid/app/PendingIntent;)Lcom/google/android/gms/common/api/am;

    const-string v1, "Coffee - NearbyAlertTracker"

    const-string v2, "removeNearbyAlerts()"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/v;->d()V

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->g:Lcom/google/android/gms/common/api/x;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/x;)V

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->c:Lcom/google/android/gms/common/api/v;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->h:Lcom/google/android/gms/common/api/y;

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/y;)V

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->e:Lcom/google/android/gms/auth/trustagent/trustlet/ae;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 230
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 231
    iput-boolean v4, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    .line 232
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v0, :cond_0

    .line 233
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e(Ljava/lang/String;)V

    .line 235
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iput-boolean v4, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    .line 236
    const-string v0, "Coffee - PlaceTrustlet"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 237
    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "stopPlaceTrustlet(), revokeTrust(null)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :cond_1
    return-void
.end method

.method private i()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 283
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 285
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, ""

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {p0, v4, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "isTrusted() && !mEnteredTrustedPlacesIdList.isEmpty(), grantTrust!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 300
    :cond_0
    :goto_1
    return-void

    .line 285
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    sget v1, Lcom/google/android/gms/p;->di:I

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 292
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 294
    invoke-virtual {p0, v4}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e(Ljava/lang/String;)V

    .line 295
    const-string v0, "Coffee - PlaceTrustlet"

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 296
    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "isTrusted() && mEnteredTrustedPlacesIdList.isEmpty(), revokeTrust(null)"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method


# virtual methods
.method public final a()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    invoke-interface {v3, v0, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "Coffee - PlaceTrustlet"

    const/4 v3, 0x4

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Initial addPlaceToTrustedPlaces: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    invoke-virtual {p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->c(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    new-array v2, v5, [Ljava/lang/String;

    invoke-interface {v0, v2}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aa;->a([Ljava/lang/String;)V

    .line 190
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 203
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 205
    const-string v0, "Enter"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 206
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i()V

    .line 208
    :cond_0
    return-void
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 194
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    if-nez v0, :cond_1

    if-eqz p1, :cond_1

    .line 195
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->f()V

    .line 199
    :cond_0
    :goto_0
    return-void

    .line 196
    :cond_1
    if-nez p1, :cond_0

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->h()V

    goto :goto_0
.end method

.method protected final b()V
    .locals 3

    .prologue
    .line 104
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    .line 105
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    .line 106
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->c:Landroid/content/SharedPreferences$Editor;

    .line 109
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/az;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/az;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->e:Lcom/google/android/gms/auth/trustagent/trustlet/az;

    .line 110
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/places/q;->b:Lcom/google/android/gms/common/api/c;

    sget-object v2, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a:Lcom/google/android/gms/location/places/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/location/p;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i:Lcom/google/android/gms/common/api/v;

    .line 112
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i:Lcom/google/android/gms/common/api/v;

    invoke-direct {v0, v1, p0, v2}, Lcom/google/android/gms/auth/trustagent/trustlet/aa;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/ad;Lcom/google/android/gms/common/api/v;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->k:Lcom/google/android/gms/auth/trustagent/trustlet/aa;

    .line 113
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    invoke-direct {v0, v1, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/x;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/y;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->a:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->d:Lcom/google/android/gms/auth/trustagent/trustlet/z;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->c:Landroid/content/IntentFilter;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 120
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/ax;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/ax;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->d:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/x;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 159
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->f()V

    .line 161
    :cond_0
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 212
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 213
    const-string v0, "Exit"

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 214
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i()V

    .line 215
    return-void
.end method

.method protected final c()V
    .locals 2

    .prologue
    .line 177
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->c()V

    .line 178
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->l:Z

    if-eqz v0, :cond_0

    .line 179
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->h()V

    .line 181
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->j:Lcom/google/android/gms/auth/trustagent/trustlet/x;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->e:Z

    iget-object v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->a:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/x;->d:Lcom/google/android/gms/auth/trustagent/trustlet/z;

    invoke-virtual {v1, v0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 182
    const-string v0, "Coffee - PlaceTrustlet"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 183
    const-string v0, "Coffee - PlaceTrustlet"

    const-string v1, "onDestroy(), stop()"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_1
    return-void
.end method

.method protected final c(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v1, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 263
    if-eqz v0, :cond_0

    .line 264
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Z)V

    .line 266
    :cond_0
    return-void
.end method

.method protected final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 342
    const-string v0, "Place"

    return-object v0
.end method

.method protected final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 271
    if-nez v0, :cond_1

    .line 272
    const-string v0, "Coffee - PlaceTrustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Attempting to remove unknown place, id="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 280
    :cond_0
    :goto_0
    return-void

    .line 275
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->n:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 276
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->i()V

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->b(Z)V

    goto :goto_0
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 347
    const/4 v0, 0x2

    return v0
.end method
