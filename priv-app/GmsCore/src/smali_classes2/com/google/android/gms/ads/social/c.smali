.class public final Lcom/google/android/gms/ads/social/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/ads/internal/n/a;


# instance fields
.field private final a:Landroid/content/Context;

.field private final b:Landroid/content/SharedPreferences;

.field private final c:Landroid/content/SharedPreferences;

.field private final d:Landroid/content/SharedPreferences;

.field private final e:Landroid/content/pm/PackageManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    iput-object p1, p0, Lcom/google/android/gms/ads/social/c;->a:Landroid/content/Context;

    .line 40
    const-string v0, "social.account_doritos"

    invoke-static {p1, v0}, Lcom/google/android/gms/ads/social/c;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/c;->b:Landroid/content/SharedPreferences;

    .line 42
    const-string v0, "social.package_doritos"

    invoke-static {p1, v0}, Lcom/google/android/gms/ads/social/c;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/c;->c:Landroid/content/SharedPreferences;

    .line 44
    const-string v0, "social.package_doritos"

    invoke-static {p1, v0}, Lcom/google/android/gms/ads/social/c;->a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/c;->d:Landroid/content/SharedPreferences;

    .line 46
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/c;->e:Landroid/content/pm/PackageManager;

    .line 47
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 199
    const/16 v0, 0xb

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 200
    const/4 v0, 0x4

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 203
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 156
    if-nez p1, :cond_1

    .line 168
    :cond_0
    :goto_0
    return v0

    .line 160
    :cond_1
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/ads/social/c;->e:Landroid/content/pm/PackageManager;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 168
    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    .line 165
    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 12

    .prologue
    .line 106
    sget-object v0, Lcom/google/android/gms/ads/a/f;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->d:Landroid/content/SharedPreferences;

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    sget-object v0, Lcom/google/android/gms/ads/a/f;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    sget-object v0, Lcom/google/android/gms/ads/a/f;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->d()Ljava/lang/String;

    move-result-object v4

    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->d:Landroid/content/SharedPreferences;

    const/4 v5, -0x1

    invoke-interface {v0, v4, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    sget-object v0, Lcom/google/android/gms/ads/a/f;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iget-object v6, p0, Lcom/google/android/gms/ads/social/c;->a:Landroid/content/Context;

    invoke-static {v6}, Lcom/google/android/gms/gcm/y;->a(Landroid/content/Context;)Lcom/google/android/gms/gcm/y;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/ads/social/c;->a:Landroid/content/Context;

    invoke-static {v7}, Lcom/google/android/gms/gcm/f;->a(Landroid/content/Context;)V

    invoke-static {}, Lcom/google/android/gms/gcm/f;->d()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    if-ne v2, v3, :cond_0

    if-eq v5, v0, :cond_1

    :cond_0
    const-string v8, "Scheduling long-period DRT refresh task."

    invoke-static {v8}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    new-instance v8, Ljava/lang/StringBuilder;

    const-string v9, "ads.social.doritos-"

    invoke-direct {v8, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v8, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/google/android/gms/gcm/aq;

    invoke-direct {v9}, Lcom/google/android/gms/gcm/aq;-><init>()V

    const-class v10, Lcom/google/android/gms/ads/social/GcmSchedulerWakeupService;

    invoke-virtual {v9, v10}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/aq;

    move-result-object v9

    int-to-long v10, v3

    iput-wide v10, v9, Lcom/google/android/gms/gcm/aq;->a:J

    int-to-long v10, v0

    iput-wide v10, v9, Lcom/google/android/gms/gcm/aq;->b:J

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Lcom/google/android/gms/gcm/aq;->a(Z)Lcom/google/android/gms/gcm/aq;

    move-result-object v9

    invoke-virtual {v9, v8}, Lcom/google/android/gms/gcm/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/aq;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/gcm/aq;->b()Lcom/google/android/gms/gcm/PeriodicTask;

    move-result-object v8

    invoke-virtual {v6, v8}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    iget-object v8, p0, Lcom/google/android/gms/ads/social/c;->d:Landroid/content/SharedPreferences;

    invoke-interface {v8}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v8

    invoke-interface {v8, v1, v3}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v4, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    const/4 v0, -0x1

    if-eq v2, v0, :cond_2

    const/4 v0, -0x1

    if-ne v5, v0, :cond_3

    :cond_2
    const-string v0, "Scheduling near-term DRT refresh task."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "ads.social.doritos-oneoff-"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    new-instance v0, Lcom/google/android/gms/gcm/an;

    invoke-direct {v0}, Lcom/google/android/gms/gcm/an;-><init>()V

    const-class v2, Lcom/google/android/gms/ads/social/GcmSchedulerWakeupService;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/Class;)Lcom/google/android/gms/gcm/an;

    move-result-object v2

    const-wide/16 v4, 0x0

    sget-object v0, Lcom/google/android/gms/ads/a/f;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v8, v0

    invoke-virtual {v2, v4, v5, v8, v9}, Lcom/google/android/gms/gcm/an;->a(JJ)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/gcm/an;->a(Ljava/lang/String;)Lcom/google/android/gms/gcm/an;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/gcm/an;->b()Lcom/google/android/gms/gcm/OneoffTask;

    move-result-object v0

    invoke-virtual {v6, v0}, Lcom/google/android/gms/gcm/y;->a(Lcom/google/android/gms/gcm/Task;)V

    .line 107
    :cond_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/gms/ads/social/c;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->c:Landroid/content/SharedPreferences;

    const-wide/16 v4, 0x0

    invoke-interface {v0, p1, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    sub-long v4, v2, v0

    sget-object v0, Lcom/google/android/gms/ads/a/f;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    int-to-long v0, v0

    cmp-long v0, v4, v0

    if-lez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->c:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, p1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const/4 v0, 0x1

    :goto_0
    if-eqz v0, :cond_7

    .line 109
    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_8

    iget-object v1, p0, Lcom/google/android/gms/ads/social/c;->a:Landroid/content/Context;

    invoke-static {v1}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    const-string v2, "com.google"

    invoke-virtual {v1, v2}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v1

    array-length v2, v1

    if-lez v2, :cond_8

    const/4 v0, 0x0

    aget-object v0, v1, v0

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    move-object v1, v0

    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->b:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-static {v0}, Ljava/net/HttpCookie;->parse(Ljava/lang/String;)Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    const/4 v3, 0x1

    if-ne v0, v3, :cond_4

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpCookie;

    invoke-virtual {v0}, Ljava/net/HttpCookie;->getName()Ljava/lang/String;

    move-result-object v0

    const-string v3, "_drt_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    const/4 v0, 0x0

    invoke-interface {v2, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/net/HttpCookie;

    invoke-virtual {v0}, Ljava/net/HttpCookie;->hasExpired()Z

    move-result v2

    if-eqz v2, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/ads/social/c;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_4
    const/4 v0, 0x0

    .line 111
    :goto_2
    return-object v0

    .line 108
    :cond_5
    const/4 v0, 0x0

    goto :goto_0

    .line 109
    :cond_6
    invoke-virtual {v0}, Ljava/net/HttpCookie;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_2

    .line 111
    :cond_7
    const/4 v0, 0x0

    goto :goto_2

    :cond_8
    move-object v1, v0

    goto :goto_1
.end method
