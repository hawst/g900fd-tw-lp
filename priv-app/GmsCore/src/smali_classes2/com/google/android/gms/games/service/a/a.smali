.class public abstract Lcom/google/android/gms/games/service/a/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field protected final a:Lcom/google/android/gms/common/server/ClientContext;

.field protected final b:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 56
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Z)V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 51
    iput-boolean p2, p0, Lcom/google/android/gms/games/service/a/a;->b:Z

    .line 52
    return-void
.end method


# virtual methods
.method public a()V
    .locals 0

    .prologue
    .line 99
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 4

    .prologue
    .line 62
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/service/a/a;->b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/games/h/c/a; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_2

    move-result-object v0

    move-object v1, v0

    .line 87
    :goto_0
    :try_start_1
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/service/a/a;->a(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_3
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/a;->b:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 96
    :cond_0
    :goto_1
    return-void

    .line 63
    :catch_0
    move-exception v0

    .line 64
    const-string v1, "DataHolderOperation"

    const-string v2, "Auth error while performing operation, requesting reconnect"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 65
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    .line 83
    goto :goto_0

    .line 66
    :catch_1
    move-exception v1

    .line 67
    const-string v0, "DataHolderOperation"

    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 72
    invoke-virtual {v1}, Lcom/google/android/gms/games/h/c/a;->a()I

    move-result v1

    .line 73
    const/16 v2, 0x5dc

    if-ne v1, v2, :cond_1

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {p1, v1, p2}, Lcom/google/android/gms/games/k/a;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/t;)V

    .line 75
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    move-object v1, v0

    goto :goto_0

    .line 76
    :cond_1
    const/16 v2, 0x3eb

    if-ne v1, v2, :cond_2

    .line 77
    invoke-virtual {p2, p1}, Lcom/google/android/gms/games/a/t;->c(Landroid/content/Context;)V

    :cond_2
    move-object v1, v0

    .line 83
    goto :goto_0

    .line 79
    :catch_2
    move-exception v0

    .line 80
    const-string v1, "DataHolderOperation"

    const-string v2, "Killing (on development devices) due to RuntimeException"

    invoke-static {p1, v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 82
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 88
    :catch_3
    move-exception v0

    .line 89
    :try_start_2
    const-string v2, "DataHolderOperation"

    const-string v3, "When providing result "

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 92
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/a/a;->b:Z

    if-eqz v0, :cond_0

    .line 93
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_1

    .line 92
    :catchall_0
    move-exception v0

    iget-boolean v2, p0, Lcom/google/android/gms/games/service/a/a;->b:Z

    if-eqz v2, :cond_3

    .line 93
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_3
    throw v0
.end method

.method protected abstract a(Lcom/google/android/gms/common/data/DataHolder;)V
.end method

.method protected abstract b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
.end method
