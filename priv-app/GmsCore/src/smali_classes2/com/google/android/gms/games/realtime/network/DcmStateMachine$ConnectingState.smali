.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;
.super Lcom/google/android/gms/games/realtime/network/s;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V
    .locals 0

    .prologue
    .line 169
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/s;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    .line 170
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 178
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 201
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->f:Z

    :goto_0
    return v0

    .line 180
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/k;

    .line 181
    iget-boolean v1, v0, Lcom/google/android/gms/games/jingle/k;->a:Z

    if-eqz v1, :cond_0

    .line 182
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v2, v1, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    sget-object v1, Lcom/google/android/gms/games/c/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Z)V

    .line 183
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v1}, Lcom/google/android/gms/games/jingle/Libjingle;->c()V

    .line 184
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v1, v1, Lcom/google/android/gms/games/realtime/network/u;->e:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;

    iget-object v2, v0, Lcom/google/android/gms/games/jingle/k;->b:Ljava/lang/String;

    iget v0, v0, Lcom/google/android/gms/games/jingle/k;->c:I

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$SubscribingBuzzState;->a(Ljava/lang/String;I)V

    .line 190
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->f:Z

    goto :goto_0

    .line 187
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->c:Lcom/google/android/gms/games/realtime/network/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/a;->a()V

    .line 188
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b()V

    goto :goto_1

    .line 193
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a()V

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/service/statemachine/d;->b()V

    .line 195
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->f:Z

    goto :goto_0

    .line 178
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x1771 -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 173
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$ConnectingState;->c()V

    .line 174
    return-void
.end method
