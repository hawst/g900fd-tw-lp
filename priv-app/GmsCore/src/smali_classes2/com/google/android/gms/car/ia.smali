.class public final Lcom/google/android/gms/car/ia;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field private static volatile a:Lcom/google/android/gms/car/ia;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 54
    const-string v0, "carservicedata.db"

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 55
    return-void
.end method

.method private a(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;
    .locals 2

    .prologue
    .line 186
    const-string v0, "allowedcars"

    const/4 v1, 0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/ia;->a(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Lcom/google/android/gms/car/CarInfoInternal;
    .locals 12

    .prologue
    const/4 v6, 0x3

    const/4 v8, 0x1

    const/4 v0, 0x0

    const/4 v11, -0x1

    const/4 v9, 0x0

    .line 235
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "   SELECT * FROM "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " WHERE manufacturer = ?     AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "model = ?     AND modelyear"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?     AND vehicleid = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "     AND headUnitProtocolMajorVersionNumber = ?     AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "headUnitProtocolMinorVersionNumber = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 244
    invoke-virtual {p0}, Lcom/google/android/gms/car/ia;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 247
    const/4 v3, 0x6

    :try_start_0
    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->b:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->c:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x2

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->d:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x3

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->i:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x4

    iget v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->f:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x5

    iget v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->g:I

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    .line 255
    const/4 v4, 0x0

    invoke-virtual {v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;Landroid/os/CancellationSignal;)Landroid/database/Cursor;
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v10

    .line 265
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    if-nez v1, :cond_1

    .line 266
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 270
    :cond_0
    :goto_0
    return-object v0

    .line 256
    :catch_0
    move-exception v1

    .line 257
    const-string v2, "CAR.SETTING"

    invoke-static {v2, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 258
    const-string v2, "CAR.SETTING"

    const-string v3, "Got exception from db: "

    invoke-static {v2, v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 268
    :cond_1
    :try_start_2
    const-string v0, "manufacturer"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    const-string v0, "model"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    const-string v0, "modelyear"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v0, "vehicleid"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    const-string v0, "headUnitProtocolMajorVersionNumber"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v11, :cond_5

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    :goto_1
    const-string v0, "headUnitProtocolMinorVersionNumber"

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    if-eq v0, v11, :cond_4

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    :goto_2
    new-instance v0, Lcom/google/android/gms/car/CarInfoInternal;

    const/4 v7, 0x0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/car/CarInfoInternal;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IIZ)V

    const-string v1, "id"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/car/CarInfoInternal;->k:J

    if-eqz p3, :cond_2

    const-string v1, "vehicleidclient"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/car/CarInfoInternal;->e:Ljava/lang/String;

    const-string v1, "bluetoothConnectionAllowed"

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v1

    if-eq v1, v11, :cond_2

    invoke-interface {v10, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-eqz v1, :cond_3

    move v1, v8

    :goto_3
    iput-boolean v1, v0, Lcom/google/android/gms/car/CarInfoInternal;->j:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 270
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    :cond_3
    move v1, v9

    .line 268
    goto :goto_3

    .line 270
    :catchall_0
    move-exception v0

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_4
    move v6, v9

    goto :goto_2

    :cond_5
    move v5, v9

    goto :goto_1
.end method

.method static declared-synchronized a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;
    .locals 2

    .prologue
    .line 59
    const-class v1, Lcom/google/android/gms/car/ia;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/ia;->a:Lcom/google/android/gms/car/ia;

    if-nez v0, :cond_0

    .line 60
    new-instance v0, Lcom/google/android/gms/car/ia;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ia;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/car/ia;->a:Lcom/google/android/gms/car/ia;

    .line 62
    :cond_0
    sget-object v0, Lcom/google/android/gms/car/ia;->a:Lcom/google/android/gms/car/ia;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 59
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static declared-synchronized a()V
    .locals 2

    .prologue
    .line 67
    const-class v0, Lcom/google/android/gms/car/ia;

    monitor-enter v0

    const/4 v1, 0x0

    :try_start_0
    sput-object v1, Lcom/google/android/gms/car/ia;->a:Lcom/google/android/gms/car/ia;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 68
    monitor-exit v0

    return-void

    .line 67
    :catchall_0
    move-exception v1

    monitor-exit v0

    throw v1
.end method

.method private static a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 150
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "CREATE TABLE "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "(id INTEGER PRIMARY KEY,manufacturer"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT,model TEXT,modelyear"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " TEXT,vehicleid TEXT,"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_0

    const-string v0, "vehicleidclient TEXT,"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "headUnitProtocolMajorVersionNumber INTEGER,headUnitProtocolMinorVersionNumber INTEGER"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-eqz p2, :cond_1

    const-string v0, ",bluetoothConnectionAllowed INTEGER"

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 161
    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 162
    return-void

    .line 150
    :cond_0
    const-string v0, ""

    goto :goto_0

    :cond_1
    const-string v0, ""

    goto :goto_1
.end method

.method private a(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 363
    invoke-virtual {p0}, Lcom/google/android/gms/car/ia;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 364
    invoke-virtual {v0, p1, v1, v1}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 365
    return-void
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z
    .locals 4

    .prologue
    .line 78
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v0

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/ia;->a(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v0

    .line 79
    if-nez v0, :cond_0

    .line 80
    const/4 v0, 0x0

    .line 85
    :goto_0
    return v0

    .line 82
    :cond_0
    iget-wide v2, v0, Lcom/google/android/gms/car/CarInfoInternal;->k:J

    iput-wide v2, p1, Lcom/google/android/gms/car/CarInfoInternal;->k:J

    .line 83
    iget-object v1, v0, Lcom/google/android/gms/car/CarInfoInternal;->e:Ljava/lang/String;

    iput-object v1, p1, Lcom/google/android/gms/car/CarInfoInternal;->e:Ljava/lang/String;

    .line 84
    iget-boolean v0, v0, Lcom/google/android/gms/car/CarInfoInternal;->j:Z

    iput-boolean v0, p1, Lcom/google/android/gms/car/CarInfoInternal;->j:Z

    .line 85
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static a(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;Z)Z
    .locals 4

    .prologue
    .line 140
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v1

    const-string v2, "allowedcars"

    const-string v3, "bluetoothConnectionAllowed"

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-direct {v1, p1, v2, v3, v0}, Lcom/google/android/gms/car/ia;->a(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    iput-boolean p2, p1, Lcom/google/android/gms/car/CarInfoInternal;->j:Z

    :cond_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Ljava/lang/String;I)Z
    .locals 10

    .prologue
    const/4 v5, 0x0

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 369
    invoke-virtual {p0}, Lcom/google/android/gms/car/ia;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 370
    new-array v2, v9, [Ljava/lang/String;

    aput-object p3, v2, v8

    const-string v3, "id = ?"

    new-array v4, v9, [Ljava/lang/String;

    iget-wide v6, p1, Lcom/google/android/gms/car/CarInfoInternal;->k:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    aput-object v1, v4, v8

    move-object v1, p2

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 374
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-nez v2, :cond_0

    .line 375
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    .line 390
    :goto_0
    return v0

    .line 377
    :cond_0
    :try_start_1
    invoke-interface {v1, p3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    invoke-interface {v1, v2}, Landroid/database/Cursor;->getInt(I)I
    :try_end_1
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-ne v2, p4, :cond_1

    .line 378
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v9

    goto :goto_0

    .line 380
    :cond_1
    :try_start_2
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 381
    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, p3, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 382
    const-string v3, "id = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    const/4 v5, 0x0

    iget-wide v6, p1, Lcom/google/android/gms/car/CarInfoInternal;->k:J

    invoke-static {v6, v7}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v0, p2, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_2
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 388
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v9

    .line 390
    goto :goto_0

    .line 384
    :catch_0
    move-exception v0

    .line 385
    :try_start_3
    const-string v2, "CAR.SETTING"

    const-string v3, "Exception while updating database"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 386
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    move v0, v8

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private b(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;
    .locals 2

    .prologue
    .line 199
    const-string v0, "rejectedcars"

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/ia;->a(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v0

    return-object v0
.end method

.method static b(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 131
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v0

    const-string v1, "allowedcars"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ia;->a(Ljava/lang/String;)V

    const-string v1, "rejectedcars"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ia;->a(Ljava/lang/String;)V

    .line 132
    return-void
.end method

.method static b(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 95
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v1

    invoke-direct {v1, p1}, Lcom/google/android/gms/car/ia;->a(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v1

    .line 96
    if-eqz v1, :cond_1

    .line 97
    const-string v1, "CAR.SETTING"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 98
    const-string v1, "CAR.SETTING"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "addAllowedCar, already existing: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :cond_0
    :goto_0
    return v0

    .line 103
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/car/CarInfoInternal;->b()V

    .line 104
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v1

    const-string v2, "allowedcars"

    invoke-direct {v1, p1, v2, v0}, Lcom/google/android/gms/car/ia;->b(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/car/ia;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 330
    new-instance v4, Landroid/content/ContentValues;

    invoke-direct {v4}, Landroid/content/ContentValues;-><init>()V

    .line 331
    const-string v0, "manufacturer"

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->b:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 332
    const-string v0, "model"

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->c:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 333
    const-string v0, "modelyear"

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->d:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 334
    const-string v0, "vehicleid"

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->i:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 335
    if-eqz p3, :cond_0

    .line 336
    const-string v0, "vehicleidclient"

    iget-object v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->e:Ljava/lang/String;

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :cond_0
    const-string v0, "headUnitProtocolMajorVersionNumber"

    iget v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->f:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 340
    const-string v0, "headUnitProtocolMinorVersionNumber"

    iget v5, p1, Lcom/google/android/gms/car/CarInfoInternal;->g:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 342
    if-eqz p3, :cond_1

    .line 343
    const-string v5, "bluetoothConnectionAllowed"

    iget-boolean v0, p1, Lcom/google/android/gms/car/CarInfoInternal;->j:Z

    if-eqz v0, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v4, v5, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 348
    :cond_1
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v3, p2, v0, v4}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v4

    .line 349
    const-wide/16 v6, -0x1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    .line 350
    const-string v0, "CAR.SETTING"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "adding car info to db table "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " failed"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    :goto_1
    return v2

    :cond_2
    move v0, v2

    .line 343
    goto :goto_0

    .line 354
    :cond_3
    iput-wide v4, p1, Lcom/google/android/gms/car/CarInfoInternal;->k:J
    :try_end_0
    .catch Landroid/database/sqlite/SQLiteException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v1

    .line 359
    goto :goto_1

    .line 355
    :catch_0
    move-exception v0

    .line 356
    const-string v1, "CAR.SETTING"

    const-string v3, "Exception while inserting into database"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_1
.end method

.method static c(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z
    .locals 1

    .prologue
    .line 108
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v0

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/ia;->b(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 112
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method static d(Landroid/content/Context;Lcom/google/android/gms/car/CarInfoInternal;)Z
    .locals 3

    .prologue
    .line 117
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v0

    invoke-direct {v0, p1}, Lcom/google/android/gms/car/ia;->b(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/CarInfoInternal;

    move-result-object v0

    .line 118
    if-eqz v0, :cond_1

    .line 119
    const-string v0, "CAR.SETTING"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    const-string v0, "CAR.SETTING"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "addRejectedCar, already existing: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 122
    :cond_0
    const/4 v0, 0x1

    .line 124
    :goto_0
    return v0

    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/car/ia;->a(Landroid/content/Context;)Lcom/google/android/gms/car/ia;

    move-result-object v0

    const-string v1, "rejectedcars"

    const/4 v2, 0x0

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/gms/car/ia;->b(Lcom/google/android/gms/car/CarInfoInternal;Ljava/lang/String;Z)Z

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2

    .prologue
    .line 145
    const-string v0, "allowedcars"

    const/4 v1, 0x1

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/car/ia;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)V

    .line 146
    const-string v0, "rejectedcars"

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/car/ia;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Z)V

    .line 147
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 1

    .prologue
    .line 167
    const-string v0, "DROP TABLE IF EXISTS allowedcars"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 168
    const-string v0, "DROP TABLE IF EXISTS rejectedcars"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 169
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/ia;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 170
    return-void
.end method
