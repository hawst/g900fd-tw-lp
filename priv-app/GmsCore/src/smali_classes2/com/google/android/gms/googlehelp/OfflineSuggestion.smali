.class public final Lcom/google/android/gms/googlehelp/OfflineSuggestion;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Ljava/lang/String;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/googlehelp/c;

    invoke-direct {v0}, Lcom/google/android/gms/googlehelp/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->a:I

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->b:Ljava/lang/String;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->c:Ljava/lang/String;

    .line 52
    iput-object p4, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->d:Ljava/lang/String;

    .line 53
    iput-object p5, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->e:Ljava/lang/String;

    .line 54
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/gms/googlehelp/OfflineSuggestion;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 102
    invoke-static {p0, p1}, Lcom/google/android/gms/googlehelp/c;->a(Lcom/google/android/gms/googlehelp/OfflineSuggestion;Landroid/os/Parcel;)V

    .line 103
    return-void
.end method
