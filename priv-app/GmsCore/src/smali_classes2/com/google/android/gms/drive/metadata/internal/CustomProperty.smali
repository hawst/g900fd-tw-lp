.class public Lcom/google/android/gms/drive/metadata/internal/CustomProperty;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

.field final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/d;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/internal/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    iput p1, p0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->a:I

    .line 49
    const-string v0, "key"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b:Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->c:Ljava/lang/String;

    .line 52
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;-><init>(ILcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)V

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/metadata/CustomPropertyKey;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->b:Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/drive/metadata/internal/CustomProperty;->c:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 65
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/metadata/internal/d;->a(Lcom/google/android/gms/drive/metadata/internal/CustomProperty;Landroid/os/Parcel;I)V

    .line 66
    return-void
.end method
