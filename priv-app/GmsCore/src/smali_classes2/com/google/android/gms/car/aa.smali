.class final Lcom/google/android/gms/car/aa;
.super Lcom/google/android/gms/car/ms;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/y;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/y;)V
    .locals 0

    .prologue
    .line 1614
    iput-object p1, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-direct {p0}, Lcom/google/android/gms/car/ms;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/y;B)V
    .locals 0

    .prologue
    .line 1614
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/aa;-><init>(Lcom/google/android/gms/car/y;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 1618
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1619
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onSetupComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1622
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;I)I

    .line 1623
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->g()V

    .line 1624
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 1788
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;Landroid/content/Intent;)Landroid/content/Intent;

    .line 1789
    return-void
.end method

.method public final a(Landroid/graphics/Bitmap;)V
    .locals 2

    .prologue
    .line 1776
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1778
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only Google signed packages can call this method"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1780
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->h(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/oi;

    move-result-object v0

    .line 1781
    if-eqz v0, :cond_1

    .line 1782
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/oi;->a(Landroid/graphics/Bitmap;)V

    .line 1784
    :cond_1
    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 1724
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 1725
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ExceptionParcel;)V
    .locals 3

    .prologue
    .line 1715
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1716
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onCrash (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1719
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ab;->a(Lcom/google/android/gms/car/ExceptionParcel;)V

    .line 1720
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;)V
    .locals 1

    .prologue
    .line 1761
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/ab;->a(Lcom/google/android/gms/car/b/g;Landroid/view/inputmethod/EditorInfo;)V

    .line 1762
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 1660
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->g(Lcom/google/android/gms/car/y;)Ljava/util/Queue;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/KeyEvent;

    .line 1661
    iget-object v1, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v1}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v1

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/car/ab;->a(Landroid/view/KeyEvent;Z)V

    .line 1662
    return-void
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 1628
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1629
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onVideoFocusGainedComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1632
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->m()V

    .line 1633
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 1771
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/ab;->c(Z)V

    .line 1772
    return-void
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 1637
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1638
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onVideoFocusConfigurationChangeComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1641
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->n()V

    .line 1642
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 1646
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1647
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onVideoFocusLostComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1650
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->p()V

    .line 1651
    return-void
.end method

.method public final e()V
    .locals 1

    .prologue
    .line 1655
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->w()V

    .line 1656
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 1666
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1667
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onStartComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1670
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->i()V

    .line 1671
    return-void
.end method

.method public final g()V
    .locals 3

    .prologue
    .line 1675
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1676
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onNewIntentComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1679
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->q()V

    .line 1680
    return-void
.end method

.method public final h()V
    .locals 3

    .prologue
    .line 1684
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1685
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; onStopComplete (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1688
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->k()V

    .line 1689
    return-void
.end method

.method public final i()V
    .locals 3

    .prologue
    .line 1693
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1694
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; preFinish called (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1699
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->L()V

    .line 1701
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->r()V

    .line 1702
    return-void
.end method

.method public final j()V
    .locals 3

    .prologue
    .line 1706
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1707
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; finish called (binder) "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1710
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->s()V

    .line 1711
    return-void
.end method

.method public final k()V
    .locals 3

    .prologue
    .line 1729
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1730
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; reqlinquishVideoFocus called by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1733
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1735
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only Google signed packages can call this method"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1737
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->t()V

    .line 1738
    return-void
.end method

.method public final l()V
    .locals 3

    .prologue
    .line 1742
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1743
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "component: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->d(Lcom/google/android/gms/car/y;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "; requestVideoFocus called by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v2}, Lcom/google/android/gms/car/y;->e(Lcom/google/android/gms/car/y;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1746
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ae;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 1748
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "Only Google signed packages can call this method"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1750
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->u()V

    .line 1751
    return-void
.end method

.method public final m()V
    .locals 1

    .prologue
    .line 1755
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->v()V

    .line 1756
    return-void
.end method

.method public final n()V
    .locals 1

    .prologue
    .line 1766
    iget-object v0, p0, Lcom/google/android/gms/car/aa;->a:Lcom/google/android/gms/car/y;

    invoke-static {v0}, Lcom/google/android/gms/car/y;->f(Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/ab;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ab;->y()V

    .line 1767
    return-void
.end method
