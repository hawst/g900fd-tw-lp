.class final Lcom/google/android/gms/udc/ui/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

.field private final b:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;Z)V
    .locals 0

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    iput-boolean p2, p0, Lcom/google/android/gms/udc/ui/b;->b:Z

    .line 204
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 6

    .prologue
    .line 208
    new-instance v0, Lcom/google/android/gms/udc/ui/e;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->b(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v4}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)I

    move-result v4

    iget-boolean v5, p0, Lcom/google/android/gms/udc/ui/b;->b:Z

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/ui/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 222
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 198
    check-cast p2, Lcom/google/android/gms/common/api/Status;

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->d(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/b;->b:Z

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->c(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;)I

    move-result v2

    new-instance v3, Lcom/google/android/gms/analytics/bf;

    invoke-direct {v3}, Lcom/google/android/gms/analytics/bf;-><init>()V

    const-string v4, "DeviceConsent"

    invoke-virtual {v3, v4}, Lcom/google/android/gms/analytics/bf;->a(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Toggle"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "On"

    :goto_0
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Lcom/google/android/gms/analytics/bf;->b(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/analytics/bf;->c(Ljava/lang/String;)Lcom/google/android/gms/analytics/bf;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/analytics/bf;->a()Ljava/util/Map;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/b;->a:Lcom/google/android/gms/udc/ui/DeviceUsageActivity;

    sget v1, Lcom/google/android/gms/p;->yT:I

    invoke-static {v0, p2, v1}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a(Lcom/google/android/gms/udc/ui/DeviceUsageActivity;Lcom/google/android/gms/common/api/Status;I)V

    :cond_0
    return-void

    :cond_1
    const-string v0, "Off"

    goto :goto_0
.end method
