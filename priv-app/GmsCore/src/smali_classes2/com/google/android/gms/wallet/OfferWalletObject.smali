.class public final Lcom/google/android/gms/wallet/OfferWalletObject;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Ljava/lang/String;

.field public c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

.field private final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Lcom/google/android/gms/wallet/u;

    invoke-direct {v0}, Lcom/google/android/gms/wallet/u;-><init>()V

    sput-object v0, Lcom/google/android/gms/wallet/OfferWalletObject;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>()V
    .locals 1

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    const/4 v0, 0x3

    iput v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    .line 89
    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/wallet/wobs/CommonWalletObject;)V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    iput p1, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    .line 75
    iput-object p3, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->b:Ljava/lang/String;

    .line 78
    const/4 v0, 0x3

    if-ge p1, v0, :cond_0

    .line 79
    invoke-static {}, Lcom/google/android/gms/wallet/wobs/CommonWalletObject;->a()Lcom/google/android/gms/wallet/wobs/a;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/wallet/wobs/a;->a(Ljava/lang/String;)Lcom/google/android/gms/wallet/wobs/a;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/wallet/wobs/a;->a:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    iput-object v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    .line 85
    :goto_0
    return-void

    .line 83
    :cond_0
    iput-object p4, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    goto :goto_0
.end method

.method public static a()Lcom/google/android/gms/wallet/t;
    .locals 3

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/wallet/t;

    new-instance v1, Lcom/google/android/gms/wallet/OfferWalletObject;

    invoke-direct {v1}, Lcom/google/android/gms/wallet/OfferWalletObject;-><init>()V

    invoke-virtual {v1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/wallet/t;-><init>(Lcom/google/android/gms/wallet/OfferWalletObject;B)V

    return-object v0
.end method


# virtual methods
.method public final b()I
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->d:I

    return v0
.end method

.method public final c()Lcom/google/android/gms/wallet/wobs/CommonWalletObject;
    .locals 1

    .prologue
    .line 274
    iget-object v0, p0, Lcom/google/android/gms/wallet/OfferWalletObject;->c:Lcom/google/android/gms/wallet/wobs/CommonWalletObject;

    return-object v0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 56
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/wallet/u;->a(Lcom/google/android/gms/wallet/OfferWalletObject;Landroid/os/Parcel;I)V

    .line 57
    return-void
.end method
