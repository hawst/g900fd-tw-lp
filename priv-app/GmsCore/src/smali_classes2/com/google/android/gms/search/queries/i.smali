.class public Lcom/google/android/gms/search/queries/i;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# instance fields
.field private final d:Lcom/google/android/gms/icing/impl/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 39
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 40
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/u;->B()Lcom/google/android/gms/icing/impl/q;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    .line 41
    return-void
.end method

.method private a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 16

    .prologue
    .line 59
    if-nez p4, :cond_0

    const/4 v2, 0x1

    move v3, v2

    .line 61
    :goto_0
    new-instance v2, Lcom/google/android/gms/icing/impl/e/g;

    const/4 v4, 0x1

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/bp;->b(I)I

    move-result v3

    invoke-direct {v2, v4, v3}, Lcom/google/android/gms/icing/impl/e/g;-><init>(II)V

    .line 64
    invoke-static/range {p1 .. p4}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/lang/String;

    move-result-object v3

    .line 65
    if-eqz v3, :cond_1

    .line 66
    invoke-static {v3}, Lcom/google/android/gms/appdatasearch/aq;->b(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v2

    .line 133
    :goto_1
    return-object v2

    .line 59
    :cond_0
    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    move v3, v2

    goto :goto_0

    .line 69
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/e/g;->a()V

    .line 71
    const-string v3, "Query global start %d num %d"

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 72
    const-string v3, "Query global: [%s]"

    move-object/from16 v0, p1

    invoke-static {v3, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 73
    const-string v3, "Index docs: %d pls: %d"

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->m()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->n()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 76
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v9

    monitor-enter v9

    .line 77
    :try_start_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/i;->c:Lcom/google/android/gms/icing/impl/a/h;

    const/4 v5, 0x0

    move-object/from16 v0, p4

    invoke-virtual {v3, v4, v0, v5}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/impl/a/h;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;

    move-result-object v5

    .line 80
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->l()Z

    move-result v3

    if-eqz v3, :cond_2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->h()V

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v4, Lcom/google/android/gms/search/queries/t;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-direct {v4, v6}, Lcom/google/android/gms/search/queries/t;-><init>(Lcom/google/android/gms/icing/impl/u;)V

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->g()Lcom/google/android/gms/icing/impl/r;

    move-result-object v3

    .line 81
    new-instance v6, Lcom/google/android/gms/icing/impl/c/b;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    move-object/from16 v0, p4

    invoke-direct {v6, v4, v5, v0, v3}, Lcom/google/android/gms/icing/impl/c/b;-><init>(Lcom/google/android/gms/icing/impl/n;Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Lcom/google/android/gms/icing/impl/r;)V

    .line 84
    new-instance v7, Lcom/google/android/gms/icing/aj;

    invoke-direct {v7}, Lcom/google/android/gms/icing/aj;-><init>()V

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/icing/aj;->g:Z

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/icing/aj;->h:Z

    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    if-nez v3, :cond_4

    const/4 v3, 0x0

    :goto_2
    iput v3, v7, Lcom/google/android/gms/icing/aj;->f:I

    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v3, v3

    new-array v3, v3, [Lcom/google/android/gms/icing/ak;

    iput-object v3, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    const/4 v3, 0x1

    iput-boolean v3, v7, Lcom/google/android/gms/icing/aj;->d:Z

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const/4 v3, 0x0

    :goto_3
    iget-object v8, v6, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v8, v8

    if-ge v3, v8, :cond_7

    iget-object v8, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    iget-object v10, v6, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    aget-object v10, v10, v3

    iget-object v11, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget-boolean v12, v6, Lcom/google/android/gms/icing/impl/c/b;->c:Z

    new-instance v13, Lcom/google/android/gms/icing/ak;

    invoke-direct {v13}, Lcom/google/android/gms/icing/ak;-><init>()V

    iget-object v14, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget v14, v14, Lcom/google/android/gms/icing/g;->a:I

    iput v14, v13, Lcom/google/android/gms/icing/ak;->a:I

    iget-object v14, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v14, v14, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    iput-object v14, v13, Lcom/google/android/gms/icing/ak;->d:[Lcom/google/android/gms/icing/az;

    if-eqz v11, :cond_3

    if-eqz v12, :cond_5

    iget-object v12, v10, Lcom/google/android/gms/icing/impl/c/a;->a:Lcom/google/android/gms/icing/impl/n;

    new-instance v14, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v15, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v15, v15, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v10, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v10, v10, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-direct {v14, v15, v10}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v10, v11, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->h:Ljava/lang/String;

    invoke-interface {v12, v14, v10}, Lcom/google/android/gms/icing/impl/n;->a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;)Lcom/google/android/gms/icing/impl/o;

    move-result-object v10

    if-eqz v10, :cond_3

    iget-wide v10, v10, Lcom/google/android/gms/icing/impl/o;->a:D

    const-wide/high16 v14, 0x40f0000000000000L    # 65536.0

    mul-double/2addr v10, v14

    invoke-static {v10, v11}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v10

    double-to-int v10, v10

    iput v10, v13, Lcom/google/android/gms/icing/ak;->e:I

    :cond_3
    :goto_4
    aput-object v13, v8, v3

    iget-object v8, v6, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    aget-object v8, v8, v3

    invoke-virtual {v8, v4}, Lcom/google/android/gms/icing/impl/c/a;->a(Ljava/util/List;)V

    add-int/lit8 v3, v3, 0x1

    goto :goto_3

    :cond_4
    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget v3, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->c:I

    goto :goto_2

    :cond_5
    iget-object v12, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v12, v12, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v10, v10, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v10, v10, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v11, v12, v10}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->b(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    move-result-object v10

    if-eqz v10, :cond_6

    iget v11, v10, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    const/4 v12, 0x1

    if-le v11, v12, :cond_6

    iget v10, v10, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    iput v10, v13, Lcom/google/android/gms/icing/ak;->e:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_4

    .line 134
    :catchall_0
    move-exception v2

    monitor-exit v9

    throw v2

    .line 84
    :cond_6
    const/4 v10, 0x1

    :try_start_1
    iput v10, v13, Lcom/google/android/gms/icing/ak;->e:I

    goto :goto_4

    :cond_7
    iget-object v3, v7, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-interface {v4, v3}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v3

    check-cast v3, [Lcom/google/android/gms/icing/ao;

    iput-object v3, v7, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    if-nez v3, :cond_8

    const/4 v3, 0x1

    move v4, v3

    :goto_5
    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bp;->a(I)I

    move-result v3

    iput v3, v7, Lcom/google/android/gms/icing/aj;->k:I

    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    if-nez v3, :cond_9

    const/4 v3, 0x0

    :goto_6
    invoke-static {v3, v4}, Lcom/google/android/gms/icing/impl/bp;->a(II)I

    move-result v3

    iput v3, v7, Lcom/google/android/gms/icing/aj;->o:I

    iget-boolean v3, v6, Lcom/google/android/gms/icing/impl/c/b;->c:Z

    if-nez v3, :cond_b

    const/4 v4, 0x1

    iget-object v8, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v10, v8

    const/4 v3, 0x0

    :goto_7
    if-ge v3, v10, :cond_a

    aget-object v11, v8, v3

    iget v11, v11, Lcom/google/android/gms/icing/ak;->e:I

    invoke-static {v4, v11}, Ljava/lang/Math;->max(II)I

    move-result v4

    add-int/lit8 v3, v3, 0x1

    goto :goto_7

    :cond_8
    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget v3, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    move v4, v3

    goto :goto_5

    :cond_9
    iget-object v3, v6, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    iget v3, v3, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->g:I

    goto :goto_6

    :cond_a
    const/high16 v3, 0x10000

    div-int v4, v3, v4

    iget-object v8, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v10, v8

    const/4 v3, 0x0

    :goto_8
    if-ge v3, v10, :cond_b

    aget-object v11, v8, v3

    iget v12, v11, Lcom/google/android/gms/icing/ak;->e:I

    mul-int/2addr v12, v4

    iput v12, v11, Lcom/google/android/gms/icing/ak;->e:I

    add-int/lit8 v3, v3, 0x1

    goto :goto_8

    :cond_b
    const/4 v3, 0x0

    :goto_9
    iget-object v4, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    array-length v4, v4

    if-ge v3, v4, :cond_d

    iget-object v4, v6, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    aget-object v4, v4, v3

    iget-object v4, v4, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v4, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v8, v6, Lcom/google/android/gms/icing/impl/c/b;->d:Lcom/google/android/gms/icing/impl/r;

    invoke-virtual {v8, v4}, Lcom/google/android/gms/icing/impl/r;->b(Ljava/lang/String;)D

    move-result-wide v10

    const-wide/16 v12, 0x0

    cmpl-double v8, v10, v12

    if-lez v8, :cond_c

    const-wide/high16 v12, 0x3ff0000000000000L    # 1.0

    cmpg-double v8, v10, v12

    if-gez v8, :cond_c

    iget-object v8, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    aget-object v8, v8, v3

    iget-object v12, v7, Lcom/google/android/gms/icing/aj;->a:[Lcom/google/android/gms/icing/ak;

    aget-object v12, v12, v3

    iget v12, v12, Lcom/google/android/gms/icing/ak;->e:I

    int-to-double v12, v12

    mul-double/2addr v12, v10

    invoke-static {v12, v13}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v12

    double-to-int v12, v12

    iput v12, v8, Lcom/google/android/gms/icing/ak;->e:I

    const-string v8, "Demoted %s by %s"

    invoke-static {v10, v11}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v10

    invoke-static {v8, v4, v10}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_c
    add-int/lit8 v3, v3, 0x1

    goto :goto_9

    .line 85
    :cond_d
    iget v3, v7, Lcom/google/android/gms/icing/aj;->f:I

    if-lez v3, :cond_e

    .line 86
    invoke-interface {v5}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_a
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_e

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/g;

    .line 87
    const-string v5, "Corpus: %s:%s id %d"

    const/4 v8, 0x3

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    iget-object v11, v3, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    aput-object v11, v8, v10

    const/4 v10, 0x1

    iget-object v11, v3, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    aput-object v11, v8, v10

    const/4 v10, 0x2

    iget v3, v3, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v8, v10

    invoke-static {v5, v8}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_a

    .line 92
    :cond_e
    if-eqz p4, :cond_f

    .line 93
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->c:Lcom/google/android/gms/icing/impl/a/h;

    iget-boolean v3, v3, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-eqz v3, :cond_11

    move-object/from16 v0, p4

    iget v3, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->e:I

    :goto_b
    iput v3, v7, Lcom/google/android/gms/icing/aj;->j:I

    .line 96
    :cond_f
    if-eqz p4, :cond_10

    .line 97
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->c:Lcom/google/android/gms/icing/impl/a/h;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/h;->a()Z

    move-result v3

    iput-boolean v3, v7, Lcom/google/android/gms/icing/aj;->m:Z

    .line 99
    :cond_10
    new-instance v3, Lcom/google/android/gms/icing/al;

    invoke-direct {v3}, Lcom/google/android/gms/icing/al;-><init>()V

    iput-object v3, v7, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    .line 100
    iget-object v3, v7, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/q;->B()I

    move-result v4

    iput v4, v3, Lcom/google/android/gms/icing/al;->a:I

    .line 101
    iget-object v3, v7, Lcom/google/android/gms/icing/aj;->n:Lcom/google/android/gms/icing/al;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/q;->C()I

    move-result v4

    iput v4, v3, Lcom/google/android/gms/icing/al;->b:I

    .line 103
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->d:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/q;->y()Z

    move-result v3

    if-eqz v3, :cond_13

    .line 104
    const/4 v4, 0x0

    .line 105
    const-string v3, "numResults changed to 0 due to icing experiment."

    invoke-static {v3}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 106
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v3

    const-string v5, "expt_disable_icing_results"

    invoke-interface {v3, v5}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 110
    :goto_c
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/e/g;->b()V

    .line 111
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v3

    move-object/from16 v0, p1

    move/from16 v1, p2

    invoke-virtual {v3, v0, v7, v1, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aj;II)Lcom/google/android/gms/icing/ap;

    move-result-object v3

    .line 114
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/e/g;->c()V

    .line 116
    invoke-virtual {v6, v3}, Lcom/google/android/gms/icing/impl/c/b;->a(Lcom/google/android/gms/icing/ap;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v8

    .line 118
    iget v5, v3, Lcom/google/android/gms/icing/ap;->b:I

    iget v6, v3, Lcom/google/android/gms/icing/ap;->e:I

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->H()Lcom/google/android/gms/icing/impl/bf;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/bf;->f()I

    move-result v7

    move-object/from16 v3, p1

    invoke-virtual/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/e/g;->a(Ljava/lang/String;IIII)Lcom/google/k/f/ap;

    move-result-object v3

    .line 121
    sget-object v2, Lcom/google/android/gms/icing/a/a;->o:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Boolean;

    invoke-virtual {v2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    if-eqz v2, :cond_12

    .line 122
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    new-instance v4, Lcom/google/android/gms/search/queries/j;

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v3}, Lcom/google/android/gms/search/queries/j;-><init>(Lcom/google/android/gms/search/queries/i;Lcom/google/k/f/ap;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/icing/impl/u;->a(Lcom/google/android/gms/icing/b/h;)Lcom/google/android/gms/icing/b/h;

    .line 133
    :goto_d
    monitor-exit v9

    move-object v2, v8

    goto/16 :goto_1

    .line 93
    :cond_11
    const/4 v3, 0x0

    goto/16 :goto_b

    .line 130
    :cond_12
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->F()Lcom/google/android/gms/icing/impl/m;

    move-result-object v2

    invoke-interface {v2, v3}, Lcom/google/android/gms/icing/impl/m;->a(Lcom/google/k/f/ap;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_d

    :cond_13
    move/from16 v4, p3

    goto :goto_c
.end method

.method static synthetic a(Lcom/google/android/gms/search/queries/i;)Lcom/google/android/gms/icing/impl/u;
    .locals 1

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/search/queries/i;->a:Lcom/google/android/gms/icing/impl/u;

    return-object v0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 6

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/search/queries/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;

    iget-object v1, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;

    iget v2, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->b:I

    iget-object v0, p0, Lcom/google/android/gms/search/queries/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;

    iget v3, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->c:I

    iget-object v0, p0, Lcom/google/android/gms/search/queries/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GlobalQueryCall$Request;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    invoke-direct {p0, v1, v2, v3, v0}, Lcom/google/android/gms/search/queries/i;->a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Lcom/google/android/gms/appdatasearch/SearchResults;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;

    invoke-direct {v2}, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x8

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/SearchResults;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-direct {v0, v3, v4, v5}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v1, v2, Lcom/google/android/gms/search/queries/GlobalQueryCall$Response;->b:Lcom/google/android/gms/appdatasearch/SearchResults;

    return-object v2

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method
