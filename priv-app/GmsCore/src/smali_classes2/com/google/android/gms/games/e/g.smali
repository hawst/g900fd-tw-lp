.class public final Lcom/google/android/gms/games/e/g;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/os/Bundle;


# direct methods
.method public constructor <init>(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    if-nez p1, :cond_0

    new-instance p1, Landroid/os/Bundle;

    invoke-direct {p1}, Landroid/os/Bundle;-><init>()V

    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    .line 61
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/e/h;)V
    .locals 3

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "external_game_id"

    iget-object v2, p1, Lcom/google/android/gms/games/e/h;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "external_leaderboard_id"

    iget-object v2, p1, Lcom/google/android/gms/games/e/h;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "time_span"

    iget v2, p1, Lcom/google/android/gms/games/e/h;->c:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "leaderboard_collection"

    iget v2, p1, Lcom/google/android/gms/games/e/h;->d:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "page_type"

    iget v2, p1, Lcom/google/android/gms/games/e/h;->e:I

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "next_page_token"

    iget-object v2, p1, Lcom/google/android/gms/games/e/h;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "prev_page_token"

    iget-object v2, p1, Lcom/google/android/gms/games/e/h;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 57
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/e/h;B)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/e/g;-><init>(Lcom/google/android/gms/games/e/h;)V

    return-void
.end method

.method public static a()Lcom/google/android/gms/games/e/h;
    .locals 2

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/games/e/h;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/e/h;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 2

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    const-string v1, "external_leaderboard_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
