.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;
.super Lcom/google/android/gms/auth/trustagent/as;
.source "SourceFile"

# interfaces
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/gms/auth/trustagent/bn;
.implements Lcom/google/android/gms/auth/trustagent/br;


# instance fields
.field private b:Lcom/google/android/gms/common/account/g;

.field private c:Ljava/lang/String;

.field private d:Landroid/os/AsyncTask;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/as;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    return-object v0
.end method

.method private a(Z)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_1

    .line 154
    if-nez p1, :cond_0

    .line 225
    :goto_0
    return-void

    .line 157
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    invoke-virtual {v0, v4}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 160
    :cond_1
    new-instance v0, Lcom/google/android/gms/auth/trustagent/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/p;-><init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 220
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 221
    invoke-static {p0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 223
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    new-array v1, v4, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    goto :goto_0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Landroid/os/AsyncTask;
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Lcom/google/android/gms/auth/trustagent/bh;
    .locals 1

    .prologue
    .line 45
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    return-object v0
.end method

.method private h()Lcom/google/android/gms/auth/trustagent/bh;
    .locals 2

    .prologue
    .line 260
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/bh;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 229
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;)V

    .line 231
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 235
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2, p3}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 237
    return-void
.end method

.method protected final e()Landroid/preference/PreferenceFragment;
    .locals 1

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/gms/auth/trustagent/bh;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/bh;-><init>()V

    return-object v0
.end method

.method public final g()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 243
    new-instance v1, Lcom/google/android/gms/location/places/a/b;

    invoke-direct {v1}, Lcom/google/android/gms/location/places/a/b;-><init>()V

    .line 244
    const-string v0, "unknown mode"

    invoke-static {v4, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    iget-object v0, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v2, "mode"

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 245
    iget-object v0, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v2, "hide_nearby_places"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 246
    iget-object v0, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v2, "hide_add_a_place"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 247
    iget-object v0, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v2, "hide_indoor_level_picker"

    invoke-virtual {v0, v2, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 248
    sget-object v0, Lcom/google/android/location/x;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->intValue()I

    move-result v0

    mul-int/lit8 v0, v0, 0x2

    .line 249
    sget v2, Lcom/google/android/gms/h;->ci:I

    iget-object v3, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v4, "reference_marker_overlay_resource_id"

    invoke-virtual {v3, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v3, "reference_marker_overlay_width_meters"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    iget-object v2, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const-string v3, "reference_marker_overlay_height_meters"

    invoke-virtual {v2, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 251
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/ew;->b(Landroid/content/Context;)V

    iget-object v0, v1, Lcom/google/android/gms/location/places/a/b;->a:Landroid/content/Intent;

    const/16 v1, 0x3e9

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Lcom/google/android/gms/common/ev; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/common/eu; {:try_start_0 .. :try_end_0} :catch_1

    .line 257
    :goto_0
    return-void

    .line 252
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/ev;->a()I

    move-result v0

    const/4 v1, -0x1

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/common/ew;->b(ILandroid/app/Activity;I)Z

    goto :goto_0

    .line 255
    :catch_1
    move-exception v0

    const-string v0, "Coffee - GoogleTrustAgentTrustedPlacesSettings"

    const-string v1, "GooglePlayServicesNotAvailableException in TrustedPlacesSettings"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    .line 79
    const/16 v0, 0x3e9

    if-ne p1, v0, :cond_0

    .line 80
    const/4 v0, -0x1

    if-ne p2, v0, :cond_1

    .line 81
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v1

    const-string v0, "context must not be null"

    invoke-static {p0, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "selected_place"

    sget-object v2, Lcom/google/android/gms/location/places/internal/PlaceImpl;->CREATOR:Lcom/google/android/gms/location/places/internal/h;

    invoke-static {p3, v0, v2}, Lcom/google/android/gms/common/internal/safeparcel/d;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/os/Parcelable$Creator;)Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/internal/PlaceImpl;

    invoke-static {p0}, Lcom/google/android/gms/location/places/internal/j;->a(Landroid/content/Context;)Lcom/google/android/gms/location/places/internal/j;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/location/places/internal/PlaceImpl;->a(Lcom/google/android/gms/location/places/internal/j;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/bh;->a(Lcom/google/android/gms/location/places/f;)V

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 83
    :cond_1
    const-string v0, "Coffee - GoogleTrustAgentTrustedPlacesSettings"

    const-string v1, "Place picker couldn\'t be launched."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 64
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/as;->onCreate(Landroid/os/Bundle;)V

    .line 65
    new-instance v0, Lcom/google/android/gms/location/places/t;

    invoke-direct {v0}, Lcom/google/android/gms/location/places/t;-><init>()V

    const-string v1, "auth"

    iput-object v1, v0, Lcom/google/android/gms/location/places/t;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/t;->a()Lcom/google/android/gms/location/places/s;

    .line 67
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "auth_trust_agent_bundle_trusted_place_home_work_account"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 70
    new-instance v0, Lcom/google/android/gms/common/account/h;

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/account/h;-><init>(Landroid/support/v7/app/a;)V

    sget v1, Lcom/google/android/gms/p;->cO:I

    iput v1, v0, Lcom/google/android/gms/common/account/h;->a:I

    iput-object p0, v0, Lcom/google/android/gms/common/account/h;->b:Landroid/widget/AdapterView$OnItemSelectedListener;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/account/h;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/account/h;->a()Lcom/google/android/gms/common/account/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->b:Lcom/google/android/gms/common/account/g;

    .line 75
    :cond_0
    return-void
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 104
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/as;->onDestroy()V

    .line 105
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    if-eqz v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/AsyncTask;->cancel(Z)Z

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->d:Landroid/os/AsyncTask;

    .line 109
    :cond_0
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3

    .prologue
    .line 114
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->d()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->b:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v1, p3}, Lcom/google/android/gms/common/account/g;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->e()V

    .line 118
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->f()V

    .line 119
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->g()V

    .line 120
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/bh;->h()V

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Home"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 124
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Work"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->b:Lcom/google/android/gms/common/account/g;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/account/g;->a(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    .line 128
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->h()Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;)V

    .line 129
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->a(Z)V

    .line 131
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0

    .prologue
    .line 134
    return-void
.end method

.method protected onResume()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 95
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/as;->onResume()V

    .line 96
    const-string v0, "location"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/location/LocationManager;

    const-string v3, "gps"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v3

    if-nez v3, :cond_0

    const-string v3, "network"

    invoke-virtual {v0, v3}, Landroid/location/LocationManager;->isProviderEnabled(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {p0}, Lcom/google/android/gms/auth/trustagent/ak;->a(Landroid/content/Context;)Z

    move-result v3

    if-nez v0, :cond_4

    if-eqz v3, :cond_4

    sget v0, Lcom/google/android/gms/p;->da:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 97
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c:Ljava/lang/String;

    if-eqz v0, :cond_2

    .line 98
    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->a(Z)V

    .line 100
    :cond_2
    return-void

    :cond_3
    move v0, v1

    .line 96
    goto :goto_0

    :cond_4
    if-eqz v0, :cond_5

    if-nez v3, :cond_5

    sget v0, Lcom/google/android/gms/p;->dc:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1

    :cond_5
    if-nez v0, :cond_1

    if-nez v3, :cond_1

    sget v0, Lcom/google/android/gms/p;->db:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method
