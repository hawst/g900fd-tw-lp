.class public Lcom/google/android/gms/search/queries/c;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# instance fields
.field private final d:Landroid/util/TimingLogger;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 41
    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v1, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v2, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/icing/impl/bb;->a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/search/queries/c;->e:Ljava/lang/String;

    .line 46
    new-instance v1, Landroid/util/TimingLogger;

    const-string v2, "Icing"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "getDocuments "

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->b:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->c:[Ljava/lang/String;

    array-length v0, v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v2, v0}, Landroid/util/TimingLogger;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    .line 48
    return-void
.end method

.method private c()Lcom/google/android/gms/appdatasearch/DocumentResults;
    .locals 12

    .prologue
    const/4 v2, 0x0

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->e:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v0

    .line 129
    :goto_0
    return-object v0

    .line 65
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    const-string v1, "wait index init"

    invoke-virtual {v0, v1}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v0, Lcom/google/android/gms/icing/impl/a/aa;

    .line 68
    iget-object v1, p0, Lcom/google/android/gms/search/queries/c;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v3

    .line 69
    invoke-interface {v3}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 70
    const/4 v1, 0x1

    :try_start_0
    new-array v5, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v1, v1, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->b:Ljava/lang/String;

    aput-object v1, v5, v6

    const/4 v1, 0x1

    invoke-interface {v3, v0, v5, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v1

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    const-string v5, "authentication"

    invoke-virtual {v0, v5}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->a:Lcom/google/android/gms/icing/impl/u;

    const/4 v0, 0x1

    new-array v5, v0, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->b:Ljava/lang/String;

    aput-object v0, v5, v6

    invoke-static {v1, v5}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/util/Set;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 77
    if-eqz v0, :cond_1

    .line 78
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v0

    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 130
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    .line 80
    :cond_1
    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/search/queries/c;->a:Lcom/google/android/gms/icing/impl/u;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-virtual {v5, v0, v1}, Lcom/google/android/gms/icing/impl/u;->a(Ljava/util/List;Ljava/util/Set;)Ljava/lang/String;

    move-result-object v0

    .line 81
    if-eqz v0, :cond_2

    .line 82
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v0

    monitor-exit v4

    goto :goto_0

    .line 85
    :cond_2
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 87
    invoke-interface {v3, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v3

    .line 90
    new-instance v5, Lcom/google/android/gms/icing/ak;

    invoke-direct {v5}, Lcom/google/android/gms/icing/ak;-><init>()V

    .line 91
    iget v0, v3, Lcom/google/android/gms/icing/g;->a:I

    iput v0, v5, Lcom/google/android/gms/icing/ak;->a:I

    .line 92
    invoke-static {v3}, Lcom/google/android/gms/icing/impl/a/j;->c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v6

    .line 94
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/icing/an;

    iput-object v0, v5, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    move v1, v2

    .line 97
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_3

    .line 99
    iget-object v0, v5, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    new-instance v2, Lcom/google/android/gms/icing/an;

    invoke-direct {v2}, Lcom/google/android/gms/icing/an;-><init>()V

    aput-object v2, v0, v1

    .line 100
    iget-object v0, v5, Lcom/google/android/gms/icing/ak;->b:[Lcom/google/android/gms/icing/an;

    aget-object v2, v0, v1

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/Section;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/Section;->b:Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/t;

    iget v0, v0, Lcom/google/android/gms/icing/impl/a/t;->a:I

    iput v0, v2, Lcom/google/android/gms/icing/an;->a:I

    .line 97
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 106
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v0, :cond_4

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    iget-object v1, v5, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/gms/icing/ak;->c:[Ljava/lang/String;

    .line 110
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    const-string v1, "build corpus spec"

    invoke-virtual {v0, v1}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 113
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v6

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->c:[Ljava/lang/String;

    invoke-virtual {v1, v0, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a([Ljava/lang/String;Lcom/google/android/gms/icing/ak;)Lcom/google/android/gms/icing/ap;

    move-result-object v1

    .line 116
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    sub-long v6, v8, v6

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    const-string v2, "execute query"

    invoke-virtual {v0, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 120
    const-string v0, "Retrieved: %d Docs: %d Elapsed: %d ms"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v8, 0x0

    iget v9, v1, Lcom/google/android/gms/icing/ap;->e:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v8

    const/4 v8, 0x1

    iget v9, v1, Lcom/google/android/gms/icing/ap;->b:I

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v2, v8

    const/4 v8, 0x2

    const-wide/32 v10, 0xf4240

    div-long/2addr v6, v10

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v2, v8

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 123
    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v2, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->c:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/c;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;

    iget-object v6, v0, Lcom/google/android/gms/search/queries/GetDocumentsCall$Request;->d:Lcom/google/android/gms/appdatasearch/QuerySpecification;

    invoke-static {v2, v1}, Lcom/google/android/gms/appdatasearch/aq;->a([Ljava/lang/String;Lcom/google/android/gms/icing/ap;)[Ljava/lang/String;

    move-result-object v2

    new-instance v0, Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-static {v2}, Lcom/google/android/gms/appdatasearch/aq;->a([Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v7

    invoke-static {v5, v1, v6, v2}, Lcom/google/android/gms/appdatasearch/aq;->a(Lcom/google/android/gms/icing/ak;Lcom/google/android/gms/icing/ap;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v8

    invoke-static {v5, v1, v3, v6, v2}, Lcom/google/android/gms/appdatasearch/aq;->a(Lcom/google/android/gms/icing/ak;Lcom/google/android/gms/icing/ap;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/appdatasearch/QuerySpecification;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {v0, v7, v8, v1}, Lcom/google/android/gms/appdatasearch/DocumentResults;-><init>(Landroid/os/Bundle;Landroid/os/Bundle;Landroid/os/Bundle;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    const-string v2, "build DocumentResults"

    invoke-virtual {v1, v2}, Landroid/util/TimingLogger;->addSplit(Ljava/lang/String;)V

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/search/queries/c;->d:Landroid/util/TimingLogger;

    invoke-virtual {v1}, Landroid/util/TimingLogger;->dumpToLog()V

    .line 129
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/search/queries/c;->c()Lcom/google/android/gms/appdatasearch/DocumentResults;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;

    invoke-direct {v2}, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;-><init>()V

    iput-object v1, v2, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->b:Lcom/google/android/gms/appdatasearch/DocumentResults;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/DocumentResults;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x8

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/DocumentResults;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v0, v3, v1, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/search/queries/GetDocumentsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v2

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method
