.class final Lcom/google/android/gms/games/service/v;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/games/internal/dx;

.field private final c:Ljava/lang/String;

.field private d:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 207
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 208
    iput-object p1, p0, Lcom/google/android/gms/games/service/v;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 209
    iput-object p2, p0, Lcom/google/android/gms/games/service/v;->b:Lcom/google/android/gms/games/internal/dx;

    .line 210
    iput-object p3, p0, Lcom/google/android/gms/games/service/v;->c:Ljava/lang/String;

    .line 211
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->b:Lcom/google/android/gms/games/internal/dx;

    iget-object v1, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/dx;->b(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 241
    :cond_0
    :goto_0
    return-void

    .line 237
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 237
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v1, :cond_1

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_1
    throw v0
.end method

.method protected final a(Lcom/google/android/gms/games/h/c/a;)V
    .locals 1

    .prologue
    .line 227
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/c/a;->b()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    .line 228
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 2

    .prologue
    .line 215
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/service/v;->c:Ljava/lang/String;

    invoke-virtual {p2, p1, v0, v1}, Lcom/google/android/gms/games/a/t;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    .line 222
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/v;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    return v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method
