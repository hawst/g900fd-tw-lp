.class final Lcom/google/android/gms/games/internal/ar;
.super Lcom/google/android/gms/games/internal/t;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/e/p;


# instance fields
.field private final c:Lcom/google/android/gms/games/e/c;

.field private final d:Lcom/google/android/gms/games/e/f;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 2392
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/internal/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2393
    new-instance v1, Lcom/google/android/gms/games/e/b;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/e/b;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 2395
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 2396
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/e/b;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/e/a;

    invoke-interface {v0}, Lcom/google/android/gms/games/e/a;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/e/c;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ar;->c:Lcom/google/android/gms/games/e/c;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 2401
    :goto_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->w_()V

    .line 2403
    new-instance v0, Lcom/google/android/gms/games/e/f;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/e/f;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v0, p0, Lcom/google/android/gms/games/internal/ar;->d:Lcom/google/android/gms/games/e/f;

    .line 2404
    return-void

    .line 2398
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-object v0, p0, Lcom/google/android/gms/games/internal/ar;->c:Lcom/google/android/gms/games/e/c;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 2401
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/e/b;->w_()V

    throw v0
.end method


# virtual methods
.method public final c()Lcom/google/android/gms/games/e/a;
    .locals 1

    .prologue
    .line 2408
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ar;->c:Lcom/google/android/gms/games/e/c;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/games/e/f;
    .locals 1

    .prologue
    .line 2413
    iget-object v0, p0, Lcom/google/android/gms/games/internal/ar;->d:Lcom/google/android/gms/games/e/f;

    return-object v0
.end method
