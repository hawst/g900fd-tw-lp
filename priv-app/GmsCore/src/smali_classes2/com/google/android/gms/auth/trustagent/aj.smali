.class final Lcom/google/android/gms/auth/trustagent/aj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceChangeListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/af;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/af;)V
    .locals 0

    .prologue
    .line 311
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/aj;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z
    .locals 3

    .prologue
    .line 315
    instance-of v0, p2, Ljava/lang/Boolean;

    if-eqz v0, :cond_0

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/aj;->a:Lcom/google/android/gms/auth/trustagent/af;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/af;->a()Lcom/google/android/gms/auth/trustagent/am;

    move-result-object v0

    const-string v1, "auth_trust_agent_pref_trustlet_enabled_com.google.android.gms.auth.trustagent.trustlet.PhonePositionTrustlet"

    check-cast p2, Ljava/lang/Boolean;

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 319
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
