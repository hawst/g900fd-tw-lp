.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/games/jingle/Libjingle;

.field final b:Lcom/google/android/gms/games/realtime/network/a;

.field final c:Lcom/google/android/gms/games/service/statemachine/d;

.field d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

.field private e:Ljava/util/ArrayList;

.field private f:Landroid/os/Message;

.field private final g:Ljava/util/HashMap;

.field private final h:Ljava/util/HashMap;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/jingle/Libjingle;Lcom/google/android/gms/games/realtime/network/a;Lcom/google/android/gms/games/service/statemachine/d;)V
    .locals 1

    .prologue
    .line 155
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->e:Ljava/util/ArrayList;

    .line 65
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->g:Ljava/util/HashMap;

    .line 66
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->h:Ljava/util/HashMap;

    .line 156
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    .line 157
    iput-object p2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    .line 158
    iput-object p3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    .line 159
    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 183
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->e:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 197
    :cond_0
    return-void

    .line 192
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->e:Ljava/util/ArrayList;

    .line 193
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->e:Ljava/util/ArrayList;

    .line 194
    const/4 v0, 0x0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 195
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/y;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/y;->a()V

    .line 194
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Landroid/os/Message;)V
    .locals 3

    .prologue
    .line 51
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->f:Landroid/os/Message;

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->f:Landroid/os/Message;

    invoke-virtual {v0, p1}, Landroid/os/Message;->copyFrom(Landroid/os/Message;)V

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "deferMessage: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    return-void
.end method

.method private a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PeerStateMachine: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method private a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;Landroid/os/Message;Lcom/google/android/gms/games/realtime/network/y;)Z
    .locals 7

    .prologue
    const/4 v0, 0x0

    const/4 v6, -0x1

    const/4 v5, 0x0

    const/4 v1, 0x1

    .line 211
    iput-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    .line 212
    iput-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->f:Landroid/os/Message;

    .line 213
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " handle "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    iget v4, p2, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/google/android/gms/games/service/statemachine/d;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    .line 214
    invoke-virtual {p1, p2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a(Landroid/os/Message;)Z

    move-result v2

    .line 215
    if-nez v2, :cond_3

    .line 216
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Not handled -- invoking default handler for:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    iget v4, p2, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/google/android/gms/games/service/statemachine/d;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    .line 217
    iget v2, p2, Landroid/os/Message;->what:I

    sparse-switch v2, :sswitch_data_0

    .line 219
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->f:Landroid/os/Message;

    if-eqz v1, :cond_0

    .line 220
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->e:Ljava/util/ArrayList;

    invoke-virtual {v1, p3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    if-eqz v1, :cond_1

    .line 225
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->h:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 226
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->g:Ljava/util/HashMap;

    iget-object v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 227
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " transitionTo "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    invoke-virtual {v2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    .line 228
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    invoke-virtual {v1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->h:Ljava/util/HashMap;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    invoke-virtual {v2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    invoke-virtual {v1, v2, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 230
    invoke-direct {p0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a()V

    .line 233
    :cond_1
    return v0

    .line 217
    :sswitch_0
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/m;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/realtime/network/m;->a(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :sswitch_1
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/g;

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/realtime/network/g;->a(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :sswitch_2
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/h;

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/realtime/network/h;->a(Ljava/lang/Object;)V

    move v0, v1

    goto :goto_0

    :sswitch_3
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/realtime/network/j;

    new-instance v2, Lcom/google/android/gms/games/realtime/network/c;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v4, v0, Lcom/google/android/gms/games/realtime/network/j;->a:Ljava/lang/String;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/games/jingle/Libjingle;->g(Ljava/lang/String;)Lcom/google/android/gms/games/jingle/PeerDiagnostics;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/games/realtime/network/c;-><init>(Lcom/google/android/gms/games/jingle/PeerDiagnostics;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/realtime/network/j;->a(Ljava/lang/Object;)V

    move v0, v1

    goto/16 :goto_0

    :sswitch_4
    iget-object v0, p2, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/e;

    iget-object v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    iget v3, v2, Lcom/google/android/gms/games/g/aj;->b:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/gms/games/g/aj;->b:I

    iget v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    iget v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    const/4 v3, 0x2

    if-gt v2, v3, :cond_2

    iget-object v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a:Lcom/google/android/gms/games/g/aj;

    iget v3, v2, Lcom/google/android/gms/games/g/aj;->c:I

    add-int/lit8 v3, v3, 0x1

    iput v3, v2, Lcom/google/android/gms/games/g/aj;->c:I

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sending response to directed presence request "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v0, v0, Lcom/google/android/gms/games/jingle/e;->a:Ljava/lang/String;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/jingle/Libjingle;->d(Ljava/lang/String;)I

    :goto_1
    move v0, v1

    goto/16 :goto_0

    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "ignoring directed presence request "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->c:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    goto :goto_1

    :sswitch_5
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    new-instance v3, Lcom/google/android/gms/games/realtime/a;

    iget-object v4, p1, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    const-string v5, "P2P_FAILED"

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/gms/games/realtime/a;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)V

    invoke-interface {v2, v3}, Lcom/google/android/gms/games/realtime/network/a;->a(Lcom/google/android/gms/games/realtime/a;)V

    move v0, v1

    goto/16 :goto_0

    :cond_3
    move v0, v2

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x191 -> :sswitch_4
        0x1774 -> :sswitch_3
        0x1775 -> :sswitch_1
        0x1776 -> :sswitch_2
        0x1777 -> :sswitch_0
        0x1779 -> :sswitch_5
    .end sparse-switch
.end method


# virtual methods
.method public final a(Landroid/os/Message;)V
    .locals 2

    .prologue
    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->g:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 276
    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;Landroid/os/Message;)Z

    goto :goto_0

    .line 278
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Landroid/os/Message;)Z
    .locals 4

    .prologue
    .line 242
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "JID must not be empty or null: message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    iget v3, p2, Landroid/os/Message;->what:I

    invoke-interface {v2, v3}, Lcom/google/android/gms/games/service/statemachine/d;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 244
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->g:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    .line 245
    if-nez v0, :cond_0

    .line 246
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Creating state for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Ljava/lang/String;)V

    .line 247
    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;-><init>(Ljava/lang/String;)V

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$EntryState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 248
    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->g:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 250
    :cond_0
    new-instance v1, Lcom/google/android/gms/games/realtime/network/x;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/games/realtime/network/x;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Landroid/os/Message;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;Landroid/os/Message;Lcom/google/android/gms/games/realtime/network/y;)Z

    move-result v0

    return v0

    .line 242
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 259
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Session ID must not be empty or null: message "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    iget v4, p2, Landroid/os/Message;->what:I

    invoke-interface {v3, v4}, Lcom/google/android/gms/games/service/statemachine/d;->a(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    .line 262
    if-nez v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No session mapped for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 266
    :goto_1
    return v1

    :cond_0
    move v0, v1

    .line 259
    goto :goto_0

    .line 266
    :cond_1
    new-instance v1, Lcom/google/android/gms/games/realtime/network/z;

    invoke-direct {v1, p0, p2, p1}, Lcom/google/android/gms/games/realtime/network/z;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Landroid/os/Message;Ljava/lang/String;)V

    invoke-direct {p0, v0, p2, v1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;Landroid/os/Message;Lcom/google/android/gms/games/realtime/network/y;)Z

    move-result v1

    goto :goto_1
.end method
