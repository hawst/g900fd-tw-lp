.class final Lcom/google/android/gms/games/service/y;
.super Lcom/google/android/gms/games/service/s;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 284
    invoke-direct {p0}, Lcom/google/android/gms/games/service/s;-><init>()V

    .line 285
    iput-object p1, p0, Lcom/google/android/gms/games/service/y;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 286
    iput-object p2, p0, Lcom/google/android/gms/games/service/y;->b:Ljava/lang/String;

    .line 287
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 0

    .prologue
    .line 311
    return-void
.end method

.method public final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 291
    const/4 v0, 0x0

    .line 295
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/y;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/y;->b:Ljava/lang/String;

    invoke-virtual {p2, p1, v1, v2}, Lcom/google/android/gms/games/a/t;->d(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 299
    if-eqz v0, :cond_0

    .line 300
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 304
    :cond_0
    :goto_0
    if-eqz v0, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    :goto_1
    return v0

    .line 297
    :catch_0
    move-exception v1

    :try_start_1
    const-string v1, "SignInIntentService"

    const-string v2, "Failed to update gameplay ACL status"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    throw v0

    .line 304
    :cond_1
    const/4 v0, 0x1

    goto :goto_1
.end method
