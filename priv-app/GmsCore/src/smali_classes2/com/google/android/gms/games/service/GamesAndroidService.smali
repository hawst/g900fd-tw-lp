.class public final Lcom/google/android/gms/games/service/GamesAndroidService;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method

.method private static a(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 72
    const-string v0, "GamesService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Binding to games service: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 73
    sget-object v2, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    monitor-enter v2

    .line 75
    const/4 v0, 0x0

    :try_start_0
    sget-object v1, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 76
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 77
    invoke-virtual {v0, p0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    monitor-exit v2

    .line 85
    :goto_1
    return-void

    .line 75
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 83
    :cond_1
    const-string v0, "GamesService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Adding intent: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 85
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method private static b(Landroid/content/Intent;)Z
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 97
    const-string v0, "GamesService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unbinding from games service: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 98
    sget-object v3, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    monitor-enter v3

    .line 100
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    .line 101
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 102
    invoke-virtual {v0, p0}, Landroid/content/Intent;->filterEquals(Landroid/content/Intent;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 103
    const-string v4, "GamesService"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Removing intent: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 104
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    .line 110
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_2

    const/4 v0, 0x1

    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 100
    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    :cond_2
    move v0, v1

    .line 110
    goto :goto_1

    .line 111
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0
.end method


# virtual methods
.method protected final dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 116
    const-string v0, "Dumping Games service cache"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 117
    sget-object v1, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    monitor-enter v1

    .line 118
    const/4 v0, 0x0

    :try_start_0
    sget-object v2, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    :goto_0
    if-ge v0, v2, :cond_0

    .line 119
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Bound intent: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/games/service/GamesAndroidService;->a:Ljava/util/ArrayList;

    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p2, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 118
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 121
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    invoke-static {p2}, Lcom/google/android/gms/games/service/k;->a(Ljava/io/PrintWriter;)V

    .line 123
    invoke-static {}, Lcom/google/android/gms/games/service/ak;->a()Lcom/google/android/gms/games/service/ai;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/service/ai;->a(Ljava/io/PrintWriter;)V

    .line 125
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/a/am;->a(Ljava/io/PrintWriter;)V

    .line 126
    return-void

    .line 121
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 2

    .prologue
    .line 29
    const-string v0, "com.google.android.gms.games.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 30
    invoke-static {p1}, Lcom/google/android/gms/games/service/GamesAndroidService;->a(Landroid/content/Intent;)V

    .line 31
    new-instance v0, Lcom/google/android/gms/games/service/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/k;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/k;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    .line 33
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onRebind(Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 39
    const-string v0, "com.google.android.gms.games.service.START"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 40
    invoke-static {p1}, Lcom/google/android/gms/games/service/GamesAndroidService;->a(Landroid/content/Intent;)V

    .line 42
    :cond_0
    return-void
.end method

.method public final onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 46
    invoke-static {p1}, Lcom/google/android/gms/games/service/GamesAndroidService;->b(Landroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 47
    const-string v0, "GamesService"

    const-string v1, "Unbound from all clients. Cleaning up."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    invoke-static {}, Lcom/google/android/gms/games/service/k;->c()V

    .line 52
    :cond_0
    const/4 v0, 0x1

    return v0
.end method
