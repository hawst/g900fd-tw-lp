.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 22
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 1

    .prologue
    .line 33
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 50
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->g:Z

    :goto_0
    return v0

    .line 35
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/at;

    .line 36
    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/at;->a:I

    if-nez v0, :cond_0

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b()V

    .line 44
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->f:Z

    goto :goto_0

    .line 41
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->g()V

    .line 42
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->b()V

    goto :goto_1

    .line 47
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 48
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->f:Z

    goto :goto_0

    .line 33
    nop

    :sswitch_data_0
    .sparse-switch
        0x19 -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitDcmLoadLibrariesState;->c()V

    .line 29
    return-void
.end method
