.class final Lcom/google/android/gms/car/j;
.super Lcom/google/android/gms/car/pf;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

.field private volatile c:Z

.field private final d:Ljava/util/concurrent/Semaphore;

.field private e:Z

.field private f:Ljava/nio/ByteBuffer;

.field private g:Lcom/google/android/gms/car/nz;

.field private h:Lcom/google/android/gms/car/n;

.field private volatile i:I

.field private volatile j:I

.field private final k:Z

.field private l:Lcom/google/android/gms/car/f;

.field private final m:Landroid/content/Context;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Landroid/os/Looper;Landroid/content/Context;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 489
    iput-object p1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 490
    const/4 v0, 0x0

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/car/pf;-><init>(Ljava/lang/Object;Landroid/os/Looper;)V

    .line 474
    iput-boolean v1, p0, Lcom/google/android/gms/car/j;->c:Z

    .line 476
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/j;->d:Ljava/util/concurrent/Semaphore;

    .line 477
    iput-boolean v1, p0, Lcom/google/android/gms/car/j;->e:Z

    .line 484
    iput v1, p0, Lcom/google/android/gms/car/j;->j:I

    .line 491
    iput-object p3, p0, Lcom/google/android/gms/car/j;->m:Landroid/content/Context;

    .line 492
    iput-boolean p4, p0, Lcom/google/android/gms/car/j;->k:Z

    .line 493
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Landroid/os/Looper;Landroid/content/Context;ZB)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/gms/car/j;-><init>(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Landroid/os/Looper;Landroid/content/Context;Z)V

    return-void
.end method

.method private declared-synchronized a()V
    .locals 1

    .prologue
    .line 515
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 519
    :goto_0
    monitor-exit p0

    return-void

    .line 518
    :cond_0
    const/4 v0, 0x2

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/j;)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/j;II)V
    .locals 4

    .prologue
    .line 461
    monitor-enter p0

    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z

    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/gms/car/j;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->sendMessage(Landroid/os/Message;)Z

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/j;->d:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x1388

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Config change took too long"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    :catch_0
    move-exception v0

    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_0
    return-void
.end method

.method private declared-synchronized b()V
    .locals 1

    .prologue
    .line 523
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 527
    :goto_0
    monitor-exit p0

    return-void

    .line 526
    :cond_0
    const/4 v0, 0x3

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 523
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/j;)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->a()V

    return-void
.end method

.method private declared-synchronized c()V
    .locals 1

    .prologue
    .line 530
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 535
    :goto_0
    monitor-exit p0

    return-void

    .line 533
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z

    .line 534
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 530
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/j;)Z
    .locals 1

    .prologue
    .line 461
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->c:Z

    return v0
.end method

.method private declared-synchronized d()V
    .locals 1

    .prologue
    .line 538
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 542
    :goto_0
    monitor-exit p0

    return-void

    .line 541
    :cond_0
    const/4 v0, 0x4

    :try_start_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/j;->sendMessage(Landroid/os/Message;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 538
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/j;)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->d()V

    return-void
.end method

.method private e()V
    .locals 6

    .prologue
    .line 653
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 654
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/nz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->b()Lcom/google/android/gms/car/e;

    move-result-object v0

    .line 657
    :goto_0
    if-eqz v0, :cond_4

    .line 658
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    const/4 v2, 0x4

    if-ne v1, v2, :cond_3

    .line 660
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    iget-boolean v2, v1, Lcom/google/android/gms/car/n;->k:Z

    if-nez v2, :cond_1

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "start() has not been called"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    sget-object v2, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v2, v1, Lcom/google/android/gms/car/n;->j:Lcom/google/android/gms/car/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/car/e;)I

    move-result v0

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/car/n;->g:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1, v0}, Ljava/util/concurrent/Semaphore;->acquire(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 661
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->f()V

    .line 675
    :cond_2
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/nz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->c()V

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/nz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->b()Lcom/google/android/gms/car/e;

    move-result-object v0

    goto :goto_0

    .line 663
    :cond_3
    iget-object v1, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/am;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 664
    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/car/e;->a:J

    invoke-virtual {v2, v4, v5, v1}, Lcom/google/android/gms/car/senderprotocol/a;->a(JLjava/nio/ByteBuffer;)V

    .line 665
    iget v1, p0, Lcom/google/android/gms/car/j;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/j;->i:I

    .line 666
    iget v1, p0, Lcom/google/android/gms/car/j;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/j;->j:I

    .line 667
    iget-boolean v1, p0, Lcom/google/android/gms/car/j;->k:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    if-eqz v1, :cond_2

    .line 668
    iget-object v1, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    iget-object v2, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    iget-object v0, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/car/f;->a([BII)V

    goto :goto_2

    .line 678
    :cond_4
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 679
    return-void

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method static synthetic e(Lcom/google/android/gms/car/j;)V
    .locals 0

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->b()V

    return-void
.end method

.method private f()V
    .locals 6

    .prologue
    .line 684
    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->b()Lcom/google/android/gms/car/e;

    move-result-object v0

    .line 685
    :goto_0
    if-eqz v0, :cond_1

    .line 686
    iget-object v1, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/am;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 687
    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/car/e;->a:J

    invoke-virtual {v2, v4, v5, v1}, Lcom/google/android/gms/car/senderprotocol/a;->a(JLjava/nio/ByteBuffer;)V

    .line 688
    iget v1, p0, Lcom/google/android/gms/car/j;->i:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/j;->i:I

    .line 689
    iget v1, p0, Lcom/google/android/gms/car/j;->j:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/gms/car/j;->j:I

    .line 690
    iget-boolean v1, p0, Lcom/google/android/gms/car/j;->k:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    if-eqz v1, :cond_0

    .line 691
    iget-object v1, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    iget-object v2, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    iget-object v3, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    iget-object v0, v0, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v0

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/car/f;->a([BII)V

    .line 695
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->c()V

    .line 696
    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    invoke-virtual {v0}, Lcom/google/android/gms/car/nz;->b()Lcom/google/android/gms/car/e;

    move-result-object v0

    goto :goto_0

    .line 698
    :cond_1
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/PrintWriter;)V
    .locals 2

    .prologue
    .line 759
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, " packets sent since start:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/j;->i:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " total packets sent:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/j;->j:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 761
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v0, 0x2

    const/4 v6, 0x0

    const/4 v10, 0x4

    const/4 v5, 0x1

    .line 546
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Z

    move-result v1

    if-nez v1, :cond_1

    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v5, :cond_1

    iget v1, p1, Landroid/os/Message;->what:I

    if-eq v1, v11, :cond_1

    iget v1, p1, Landroid/os/Message;->what:I

    const/4 v2, 0x5

    if-eq v1, v2, :cond_1

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 551
    :cond_1
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 647
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported message "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 553
    :pswitch_0
    iget v2, p1, Landroid/os/Message;->arg1:I

    iget v7, p1, Landroid/os/Message;->arg2:I

    const/16 v1, 0x3e80

    if-ne v2, v5, :cond_17

    const v1, 0xbb80

    move v2, v0

    :goto_1
    iget-object v3, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    new-instance v4, Lcom/google/android/gms/car/nz;

    iget-object v8, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v8}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v8

    const/16 v9, 0x8

    invoke-direct {v4, v8, v9}, Lcom/google/android/gms/car/nz;-><init>(II)V

    invoke-static {v3, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;Lcom/google/android/gms/car/nz;)Lcom/google/android/gms/car/nz;

    if-eq v7, v0, :cond_2

    if-ne v7, v10, :cond_5

    :cond_2
    const/16 v0, 0x400

    invoke-static {v0}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :cond_3
    new-instance v0, Lcom/google/android/gms/car/nz;

    const/16 v3, 0x1000

    const/16 v4, 0x8

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/car/nz;-><init>(II)V

    iput-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    iget-object v0, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/n;->a()V

    :cond_4
    new-instance v0, Lcom/google/android/gms/car/n;

    iget-object v3, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    iget-object v4, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    if-ne v7, v10, :cond_7

    :goto_2
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/n;-><init>(IILjava/nio/ByteBuffer;Lcom/google/android/gms/car/nz;Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    :cond_5
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v11}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "init, samplingRate: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ", numberOfBits: 16, numberOfChannels: "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", bufferSize: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", encoding: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 554
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/j;->d:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    goto/16 :goto_0

    :cond_7
    move v5, v6

    .line 553
    goto :goto_2

    .line 558
    :pswitch_1
    iget-boolean v1, p0, Lcom/google/android/gms/car/j;->c:Z

    if-eqz v1, :cond_8

    .line 559
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v10}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 560
    const-string v0, "CAR.AUDIO"

    const-string v1, "stream already started"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 564
    :cond_8
    sget-object v1, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 565
    iput-boolean v5, p0, Lcom/google/android/gms/car/j;->c:Z

    .line 568
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/senderprotocol/a;->a_(I)V

    .line 569
    iput v6, p0, Lcom/google/android/gms/car/j;->i:I

    .line 570
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    if-eq v1, v0, :cond_9

    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    if-ne v1, v10, :cond_b

    .line 572
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    iget-boolean v1, v1, Lcom/google/android/gms/car/n;->k:Z

    if-nez v1, :cond_a

    .line 573
    iget-object v1, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    iget-boolean v2, v1, Lcom/google/android/gms/car/n;->k:Z

    if-nez v2, :cond_a

    iput-boolean v5, v1, Lcom/google/android/gms/car/n;->k:Z

    iget-object v2, v1, Lcom/google/android/gms/car/n;->g:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    iget-object v2, v1, Lcom/google/android/gms/car/n;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    new-instance v2, Lcom/google/android/gms/car/a;

    iget v3, v1, Lcom/google/android/gms/car/n;->a:I

    iget v4, v1, Lcom/google/android/gms/car/n;->b:I

    iget v7, v1, Lcom/google/android/gms/car/n;->c:I

    invoke-direct {v2, v3, v4, v7}, Lcom/google/android/gms/car/a;-><init>(III)V

    iput-object v2, v1, Lcom/google/android/gms/car/n;->j:Lcom/google/android/gms/car/a;

    iget-object v2, v1, Lcom/google/android/gms/car/n;->j:Lcom/google/android/gms/car/a;

    iget-object v3, v1, Lcom/google/android/gms/car/n;->i:Lcom/google/android/gms/car/o;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/a;->a(Lcom/google/android/gms/car/nr;)Z

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/car/n;->e:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->acquire()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_2

    .line 575
    :cond_a
    :goto_3
    iget-object v1, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    new-array v1, v1, [B

    iget-object v2, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    invoke-static {v2, v3, v1, v6, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/car/senderprotocol/a;->a([B)V

    .line 577
    :cond_b
    sget-object v1, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 578
    iget-boolean v1, p0, Lcom/google/android/gms/car/j;->k:Z

    if-eqz v1, :cond_0

    .line 582
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    if-ne v1, v5, :cond_16

    move v1, v5

    .line 586
    :goto_4
    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    if-eq v2, v0, :cond_c

    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v0

    if-ne v0, v10, :cond_d

    .line 589
    :cond_c
    or-int/lit8 v1, v1, 0x2

    move v6, v5

    .line 592
    :cond_d
    new-instance v0, Lcom/google/android/gms/car/f;

    iget-object v2, p0, Lcom/google/android/gms/car/j;->m:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v3

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/gms/car/f;-><init>(Landroid/content/Context;II)V

    iput-object v0, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    .line 593
    if-eqz v6, :cond_0

    .line 594
    iget-object v0, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    iget-object v1, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/gms/car/j;->f:Ljava/nio/ByteBuffer;

    invoke-virtual {v3}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v3

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/f;->a([BII)V

    goto/16 :goto_0

    .line 602
    :pswitch_2
    iget-boolean v1, p0, Lcom/google/android/gms/car/j;->c:Z

    if-eqz v1, :cond_0

    .line 604
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->e()V

    .line 605
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    if-eq v1, v0, :cond_e

    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v0

    if-ne v0, v10, :cond_f

    .line 608
    :cond_e
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->f()V

    .line 610
    :cond_f
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 611
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->c(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)Lcom/google/android/gms/car/senderprotocol/a;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/a;->l_()V

    .line 614
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 615
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v11}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 618
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopping, sent "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/j;->i:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 621
    :cond_10
    iput-boolean v6, p0, Lcom/google/android/gms/car/j;->c:Z

    .line 623
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    .line 625
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->k:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    if-eqz v0, :cond_0

    .line 626
    iget-object v0, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    iget-object v1, v0, Lcom/google/android/gms/car/f;->a:Ljava/io/BufferedOutputStream;

    if-eqz v1, :cond_11

    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/car/f;->a:Ljava/io/BufferedOutputStream;

    invoke-virtual {v1}, Ljava/io/BufferedOutputStream;->flush()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :goto_5
    :try_start_2
    iget-object v0, v0, Lcom/google/android/gms/car/f;->a:Ljava/io/BufferedOutputStream;

    invoke-virtual {v0}, Ljava/io/BufferedOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 627
    :cond_11
    :goto_6
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/j;->l:Lcom/google/android/gms/car/f;

    goto/16 :goto_0

    .line 633
    :pswitch_3
    iget-boolean v0, p0, Lcom/google/android/gms/car/j;->c:Z

    if-eqz v0, :cond_12

    .line 634
    invoke-direct {p0}, Lcom/google/android/gms/car/j;->e()V

    goto/16 :goto_0

    .line 636
    :cond_12
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v11}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stream send requested while stopped "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->f(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 643
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v1

    if-eq v1, v0, :cond_13

    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->d(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)I

    move-result v0

    if-ne v0, v10, :cond_15

    :cond_13
    iget-object v0, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    if-eqz v0, :cond_14

    iget-object v0, p0, Lcom/google/android/gms/car/j;->h:Lcom/google/android/gms/car/n;

    invoke-virtual {v0}, Lcom/google/android/gms/car/n;->a()V

    :cond_14
    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    if-eqz v0, :cond_15

    iget-object v0, p0, Lcom/google/android/gms/car/j;->g:Lcom/google/android/gms/car/nz;

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->clear()V

    :cond_15
    iget-object v0, p0, Lcom/google/android/gms/car/j;->a:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-static {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto/16 :goto_0

    :catch_0
    move-exception v0

    goto :goto_6

    :catch_1
    move-exception v1

    goto :goto_5

    :catch_2
    move-exception v1

    goto/16 :goto_3

    :cond_16
    move v1, v6

    goto/16 :goto_4

    :cond_17
    move v2, v5

    goto/16 :goto_1

    .line 551
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method
