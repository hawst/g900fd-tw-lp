.class public final Lcom/google/android/gms/drive/ak;
.super Lcom/google/android/gms/common/data/a;
.source "SourceFile"


# instance fields
.field private final b:Ljava/lang/String;

.field private c:Lcom/google/android/gms/drive/al;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 2

    .prologue
    .line 39
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/data/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/drive/ak;->b:Ljava/lang/String;

    .line 43
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v0

    const-class v1, Lcom/google/android/gms/drive/ak;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 44
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 4

    .prologue
    .line 68
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 69
    invoke-static {}, Lcom/google/android/gms/drive/metadata/internal/f;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/metadata/f;

    .line 70
    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->d()I

    move-result v3

    if-gt v3, p1, :cond_0

    .line 71
    invoke-interface {v0}, Lcom/google/android/gms/drive/metadata/f;->c()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 74
    :cond_1
    new-instance v0, Ljava/util/HashSet;

    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v0}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_2
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 75
    invoke-interface {v1, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 76
    invoke-virtual {p0}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v3

    invoke-virtual {v3, v0}, Landroid/os/Bundle;->remove(Ljava/lang/String;)V

    goto :goto_1

    .line 79
    :cond_3
    return-void
.end method


# virtual methods
.method public final synthetic a(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/gms/drive/ak;->b(I)Lcom/google/android/gms/drive/aj;

    move-result-object v0

    return-object v0
.end method

.method public final b(I)Lcom/google/android/gms/drive/aj;
    .locals 2

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/gms/drive/ak;->c:Lcom/google/android/gms/drive/al;

    .line 49
    if-eqz v0, :cond_0

    iget v1, v0, Lcom/google/android/gms/drive/al;->a:I

    if-eq v1, p1, :cond_1

    .line 50
    :cond_0
    new-instance v0, Lcom/google/android/gms/drive/al;

    iget-object v1, p0, Lcom/google/android/gms/drive/ak;->a:Lcom/google/android/gms/common/data/DataHolder;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/drive/al;-><init>(Lcom/google/android/gms/common/data/DataHolder;I)V

    .line 51
    iput-object v0, p0, Lcom/google/android/gms/drive/ak;->c:Lcom/google/android/gms/drive/al;

    .line 53
    :cond_1
    return-object v0
.end method
