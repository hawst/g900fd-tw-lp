.class final Lcom/google/android/gms/common/internal/au;
.super Lcom/google/android/gms/common/internal/at;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private final a:Ljava/util/HashMap;

.field private final b:Landroid/content/Context;

.field private final c:Landroid/os/Handler;

.field private final d:Lcom/google/android/gms/common/stats/b;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/at;-><init>()V

    .line 40
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    .line 51
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/au;->b:Landroid/content/Context;

    .line 52
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p1}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/au;->c:Landroid/os/Handler;

    .line 53
    invoke-static {}, Lcom/google/android/gms/common/stats/b;->a()Lcom/google/android/gms/common/stats/b;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/internal/au;->d:Lcom/google/android/gms/common/stats/b;

    .line 54
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/internal/au;)Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;)V
    .locals 6

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 159
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/aw;

    .line 160
    if-nez v0, :cond_0

    .line 161
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Nonexistent connection status for service config: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 187
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 169
    :cond_0
    :try_start_1
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/internal/aw;->a(Landroid/content/ServiceConnection;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 170
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to unbind a GmsServiceConnection  that was not bound before.  config="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 173
    :cond_1
    iget-object v2, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    iget-object v2, v2, Lcom/google/android/gms/common/internal/au;->d:Lcom/google/android/gms/common/stats/b;

    iget-object v3, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    iget-object v3, v3, Lcom/google/android/gms/common/internal/au;->b:Landroid/content/Context;

    invoke-virtual {v2, v3, p2}, Lcom/google/android/gms/common/stats/b;->b(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    iget-object v2, v0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v2, p2}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    .line 174
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/aw;->a()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 179
    iget-object v2, p0, Lcom/google/android/gms/common/internal/au;->c:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    .line 180
    iget-object v2, p0, Lcom/google/android/gms/common/internal/au;->c:Landroid/os/Handler;

    const-wide/16 v4, 0x1388

    invoke-virtual {v2, v0, v4, v5}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 187
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    .locals 4

    .prologue
    .line 91
    iget-object v1, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 92
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/aw;

    .line 93
    if-nez v0, :cond_0

    .line 98
    new-instance v0, Lcom/google/android/gms/common/internal/aw;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/common/internal/aw;-><init>(Lcom/google/android/gms/common/internal/au;Lcom/google/android/gms/common/internal/av;)V

    .line 99
    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/common/internal/aw;->a(Landroid/content/ServiceConnection;Ljava/lang/String;)V

    .line 100
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/internal/aw;->a(Ljava/lang/String;)V

    .line 101
    iget-object v2, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    invoke-virtual {v2, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    :goto_0
    iget-boolean v0, v0, Lcom/google/android/gms/common/internal/aw;->d:Z

    monitor-exit v1

    return v0

    .line 103
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/common/internal/au;->c:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Landroid/os/Handler;->removeMessages(ILjava/lang/Object;)V

    .line 104
    invoke-virtual {v0, p2}, Lcom/google/android/gms/common/internal/aw;->a(Landroid/content/ServiceConnection;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Trying to bind a GmsServiceConnection that was already connected before.  config="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 115
    :cond_1
    :try_start_1
    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/common/internal/aw;->a(Landroid/content/ServiceConnection;Ljava/lang/String;)V

    .line 116
    iget v2, v0, Lcom/google/android/gms/common/internal/aw;->c:I

    packed-switch v2, :pswitch_data_0

    goto :goto_0

    .line 118
    :pswitch_0
    iget-object v2, v0, Lcom/google/android/gms/common/internal/aw;->g:Landroid/content/ComponentName;

    iget-object v3, v0, Lcom/google/android/gms/common/internal/aw;->e:Landroid/os/IBinder;

    invoke-interface {p2, v2, v3}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    goto :goto_0

    .line 123
    :pswitch_1
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/internal/aw;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 116
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/common/internal/au;)Landroid/content/Context;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/common/internal/au;->b:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/common/internal/au;)Lcom/google/android/gms/common/stats/b;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/common/internal/au;->d:Lcom/google/android/gms/common/stats/b;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 83
    new-instance v0, Lcom/google/android/gms/common/internal/av;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/av;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final a(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 73
    new-instance v0, Lcom/google/android/gms/common/internal/av;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/av;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final b(Landroid/content/ComponentName;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 150
    new-instance v0, Lcom/google/android/gms/common/internal/av;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/av;-><init>(Landroid/content/ComponentName;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;)V

    .line 151
    return-void
.end method

.method public final b(Ljava/lang/String;Landroid/content/ServiceConnection;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 141
    new-instance v0, Lcom/google/android/gms/common/internal/av;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/internal/av;-><init>(Ljava/lang/String;)V

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/av;Landroid/content/ServiceConnection;)V

    .line 142
    return-void
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 192
    iget v1, p1, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 207
    :goto_0
    return v0

    .line 194
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/internal/aw;

    .line 195
    iget-object v1, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    monitor-enter v1

    .line 200
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/aw;->a()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 201
    iget-object v2, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    iget-object v2, v2, Lcom/google/android/gms/common/internal/au;->d:Lcom/google/android/gms/common/stats/b;

    iget-object v3, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    iget-object v3, v3, Lcom/google/android/gms/common/internal/au;->b:Landroid/content/Context;

    iget-object v4, v0, Lcom/google/android/gms/common/internal/aw;->a:Lcom/google/android/gms/common/internal/ax;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/common/stats/b;->a(Landroid/content/Context;Landroid/content/ServiceConnection;)V

    const/4 v2, 0x0

    iput-boolean v2, v0, Lcom/google/android/gms/common/internal/aw;->d:Z

    const/4 v2, 0x2

    iput v2, v0, Lcom/google/android/gms/common/internal/aw;->c:I

    .line 202
    iget-object v2, p0, Lcom/google/android/gms/common/internal/au;->a:Ljava/util/HashMap;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/aw;->f:Lcom/google/android/gms/common/internal/av;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 204
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 205
    const/4 v0, 0x1

    goto :goto_0

    .line 204
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 192
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
