.class public final Lcom/google/android/gms/games/service/a/i/q;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Z)V
    .locals 1

    .prologue
    .line 22
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i/q;->b:Lcom/google/android/gms/games/internal/dr;

    .line 24
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i/q;->c:Lcom/google/android/gms/games/a/au;

    .line 25
    iput-boolean p3, p0, Lcom/google/android/gms/games/service/a/i/q;->d:Z

    .line 26
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 1

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/q;->b:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->c(I)V

    .line 37
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i/q;->c:Lcom/google/android/gms/games/a/au;

    iget-boolean v1, p0, Lcom/google/android/gms/games/service/a/i/q;->d:Z

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Z)I

    move-result v0

    return v0
.end method
