.class public final Lcom/google/android/gms/checkin/b/l;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:J

.field private b:Z

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Ljava/lang/String;

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 795
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 800
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/l;->c:Ljava/lang/String;

    .line 817
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/l;->e:Ljava/lang/String;

    .line 834
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/checkin/b/l;->a:J

    .line 875
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/checkin/b/l;->g:I

    .line 795
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 878
    iget v0, p0, Lcom/google/android/gms/checkin/b/l;->g:I

    if-gez v0, :cond_0

    .line 880
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/l;->b()I

    .line 882
    :cond_0
    iget v0, p0, Lcom/google/android/gms/checkin/b/l;->g:I

    return v0
.end method

.method public final a(J)Lcom/google/android/gms/checkin/b/l;
    .locals 1

    .prologue
    .line 838
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->f:Z

    .line 839
    iput-wide p1, p0, Lcom/google/android/gms/checkin/b/l;->a:J

    .line 840
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;
    .locals 1

    .prologue
    .line 804
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->b:Z

    .line 805
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/l;->c:Ljava/lang/String;

    .line 806
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 2

    .prologue
    .line 792
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/l;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/l;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/checkin/b/l;->a(J)Lcom/google/android/gms/checkin/b/l;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 864
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->b:Z

    if-eqz v0, :cond_0

    .line 865
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/l;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 867
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->d:Z

    if-eqz v0, :cond_1

    .line 868
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/l;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 870
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->f:Z

    if-eqz v0, :cond_2

    .line 871
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/checkin/b/l;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 873
    :cond_2
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 887
    const/4 v0, 0x0

    .line 888
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/l;->b:Z

    if-eqz v1, :cond_0

    .line 889
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/l;->c:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 892
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/l;->d:Z

    if-eqz v1, :cond_1

    .line 893
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/l;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 896
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/l;->f:Z

    if-eqz v1, :cond_2

    .line 897
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/checkin/b/l;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 900
    :cond_2
    iput v0, p0, Lcom/google/android/gms/checkin/b/l;->g:I

    .line 901
    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/l;
    .locals 1

    .prologue
    .line 821
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/l;->d:Z

    .line 822
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/l;->e:Ljava/lang/String;

    .line 823
    return-object p0
.end method
