.class final Lcom/google/android/gms/icing/service/m;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Z

.field final synthetic b:Lcom/google/android/gms/appdatasearch/a/h;

.field final synthetic c:Lcom/google/android/gms/icing/service/g;


# direct methods
.method constructor <init>(Lcom/google/android/gms/icing/service/g;ZLcom/google/android/gms/appdatasearch/a/h;)V
    .locals 0

    .prologue
    .line 493
    iput-object p1, p0, Lcom/google/android/gms/icing/service/m;->c:Lcom/google/android/gms/icing/service/g;

    iput-boolean p2, p0, Lcom/google/android/gms/icing/service/m;->a:Z

    iput-object p3, p0, Lcom/google/android/gms/icing/service/m;->b:Lcom/google/android/gms/appdatasearch/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 496
    iget-object v0, p0, Lcom/google/android/gms/icing/service/m;->c:Lcom/google/android/gms/icing/service/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/service/g;->a(Lcom/google/android/gms/icing/service/g;)Landroid/content/Context;

    move-result-object v0

    const-string v1, "lightweight-appdatasearch"

    invoke-virtual {v0, v1, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 498
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "usage_reporting_enabled"

    iget-boolean v2, p0, Lcom/google/android/gms/icing/service/m;->a:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 500
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/service/m;->b:Lcom/google/android/gms/appdatasearch/a/h;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v0, v1}, Lcom/google/android/gms/appdatasearch/a/h;->a(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 504
    :goto_0
    return-void

    .line 501
    :catch_0
    move-exception v0

    const-string v1, "Client died during setUsageReportingEnabled"

    new-array v2, v3, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method
