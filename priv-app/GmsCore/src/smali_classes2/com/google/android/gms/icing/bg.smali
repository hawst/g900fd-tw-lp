.class public final Lcom/google/android/gms/icing/bg;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:[Lcom/google/android/gms/icing/bh;

.field public b:J

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 9322
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9323
    invoke-static {}, Lcom/google/android/gms/icing/bh;->a()[Lcom/google/android/gms/icing/bh;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    iput-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    iput-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bg;->cachedSize:I

    .line 9324
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/bg;
    .locals 1

    .prologue
    .line 9460
    new-instance v0, Lcom/google/android/gms/icing/bg;

    invoke-direct {v0}, Lcom/google/android/gms/icing/bg;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/bg;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 9390
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v1

    .line 9391
    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 9392
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 9393
    iget-object v2, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    aget-object v2, v2, v0

    .line 9394
    if-eqz v2, :cond_0

    .line 9395
    const/4 v3, 0x1

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 9392
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9400
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_2

    .line 9401
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 9404
    :cond_2
    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    cmp-long v0, v2, v4

    if-eqz v0, :cond_3

    .line 9405
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 9408
    :cond_3
    return v1
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9336
    if-ne p1, p0, :cond_1

    .line 9353
    :cond_0
    :goto_0
    return v0

    .line 9339
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bg;

    if-nez v2, :cond_2

    move v0, v1

    .line 9340
    goto :goto_0

    .line 9342
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bg;

    .line 9343
    iget-object v2, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    iget-object v3, p1, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 9345
    goto :goto_0

    .line 9347
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bg;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 9348
    goto :goto_0

    .line 9350
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/bg;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 9351
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 9358
    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 9361
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bg;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 9363
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/bg;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 9365
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 9151
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/bh;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/bh;

    invoke-direct {v3}, Lcom/google/android/gms/icing/bh;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/bh;

    invoke-direct {v3}, Lcom/google/android/gms/icing/bh;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 9371
    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v0, v0

    if-lez v0, :cond_1

    .line 9372
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 9373
    iget-object v1, p0, Lcom/google/android/gms/icing/bg;->a:[Lcom/google/android/gms/icing/bh;

    aget-object v1, v1, v0

    .line 9374
    if-eqz v1, :cond_0

    .line 9375
    const/4 v2, 0x1

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 9372
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 9379
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/bg;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 9380
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 9382
    :cond_2
    iget-wide v0, p0, Lcom/google/android/gms/icing/bg;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_3

    .line 9383
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/bg;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 9385
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9386
    return-void
.end method
