.class public Lcom/google/android/gms/car/VoiceActionService;
.super Landroid/app/IntentService;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/car/qm;

.field private b:Lcom/google/android/gms/car/qn;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    const-string v0, "CAR.VOICE"

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/VoiceActionService;-><init>(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 30
    invoke-direct {p0, p1}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 32
    new-instance v0, Lcom/google/android/gms/car/qm;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/qm;-><init>(Lcom/google/android/gms/car/VoiceActionService;)V

    iput-object v0, p0, Lcom/google/android/gms/car/VoiceActionService;->a:Lcom/google/android/gms/car/qm;

    .line 33
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/car/VoiceActionService;->a:Lcom/google/android/gms/car/qm;

    return-object v0
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 44
    iget-object v0, p0, Lcom/google/android/gms/car/VoiceActionService;->b:Lcom/google/android/gms/car/qn;

    if-eqz v0, :cond_0

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/car/VoiceActionService;->b:Lcom/google/android/gms/car/qn;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/qn;->a(Landroid/content/Intent;)V

    .line 49
    :goto_0
    return-void

    .line 47
    :cond_0
    const-string v0, "CAR.VOICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Voice action received but callback was not set. "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
