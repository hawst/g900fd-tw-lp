.class public abstract Lcom/google/android/gms/common/api/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/am;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Ljava/util/concurrent/CountDownLatch;

.field protected final c:Lcom/google/android/gms/common/api/b;

.field private final d:Ljava/util/ArrayList;

.field private e:Lcom/google/android/gms/common/api/aq;

.field private volatile f:Lcom/google/android/gms/common/api/ap;

.field private volatile g:Z

.field private h:Z

.field private i:Z

.field private j:Lcom/google/android/gms/common/internal/bd;


# direct methods
.method protected constructor <init>(Landroid/os/Looper;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    .line 32
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->d:Ljava/util/ArrayList;

    .line 44
    new-instance v0, Lcom/google/android/gms/common/api/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/b;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/b;

    .line 45
    return-void
.end method

.method protected constructor <init>(Lcom/google/android/gms/common/api/b;)V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    .line 32
    new-instance v0, Ljava/util/concurrent/CountDownLatch;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->d:Ljava/util/ArrayList;

    .line 49
    iput-object p1, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/b;

    .line 50
    return-void
.end method

.method static b(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    .line 254
    instance-of v1, p0, Lcom/google/android/gms/common/api/ao;

    if-eqz v1, :cond_0

    .line 256
    :try_start_0
    move-object v0, p0

    check-cast v0, Lcom/google/android/gms/common/api/ao;

    move-object v1, v0

    invoke-interface {v1}, Lcom/google/android/gms/common/api/ao;->w_()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 257
    :catch_0
    move-exception v1

    .line 258
    const-string v2, "AbstractPendingResult"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to release "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/common/api/ap;)V
    .locals 4

    .prologue
    .line 233
    iput-object p1, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    .line 234
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->j:Lcom/google/android/gms/common/internal/bd;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    .line 238
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/ap;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->e:Lcom/google/android/gms/common/api/aq;

    if-eqz v0, :cond_0

    .line 240
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/b;->a()V

    .line 241
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->h:Z

    if-nez v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/b;

    iget-object v2, p0, Lcom/google/android/gms/common/api/a;->e:Lcom/google/android/gms/common/api/aq;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->i()Lcom/google/android/gms/common/api/ap;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/api/b;->a(Lcom/google/android/gms/common/api/aq;Lcom/google/android/gms/common/api/ap;)V

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/an;

    .line 247
    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/an;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    .line 249
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 250
    return-void
.end method

.method private h()Z
    .locals 4

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->getCount()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()Lcom/google/android/gms/common/api/ap;
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 199
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 200
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v2, :cond_0

    :goto_0
    const-string v2, "Result has already been consumed."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 201
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    const-string v2, "Result is not ready."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    .line 204
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    .line 205
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/gms/common/api/a;->e:Lcom/google/android/gms/common/api/aq;

    .line 206
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/a;->g:Z

    .line 207
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/a;->d()V

    .line 210
    return-object v0

    .line 200
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 207
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 214
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 215
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 216
    sget-object v0, Lcom/google/android/gms/common/api/Status;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 217
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/a;->i:Z

    .line 219
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/api/ap;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 59
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "await must not be called on the UI thread"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 61
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v0, :cond_1

    :goto_1
    const-string v0, "Result has already been consumed"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 63
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 68
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->i()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 59
    goto :goto_0

    :cond_1
    move v1, v2

    .line 61
    goto :goto_1

    .line 65
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->j()V

    goto :goto_2
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 73
    const-wide/16 v4, 0x0

    cmp-long v0, p1, v4

    if-lez v0, :cond_0

    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    if-eq v0, v3, :cond_2

    :cond_0
    move v0, v2

    :goto_0
    const-string v3, "await must not be called on the UI thread when time is greater than zero."

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 75
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v0, :cond_3

    :goto_1
    const-string v0, "Result has already been consumed."

    invoke-static {v2, v0}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 77
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->b:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0, p1, p2, p3}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    .line 78
    if-nez v0, :cond_1

    .line 79
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/a;->e()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 84
    :cond_1
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    const-string v1, "Result is not ready."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 85
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->i()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    return-object v0

    :cond_2
    move v0, v1

    .line 73
    goto :goto_0

    :cond_3
    move v2, v1

    .line 75
    goto :goto_1

    .line 82
    :catch_0
    move-exception v0

    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->j()V

    goto :goto_2
.end method

.method protected abstract a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
.end method

.method public final a(Lcom/google/android/gms/common/api/an;)V
    .locals 2

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 126
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 127
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/ap;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/an;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 133
    :goto_1
    monitor-exit v1

    return-void

    .line 125
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 131
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->d:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    .line 133
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 166
    iget-object v3, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 167
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/a;->i:Z

    if-nez v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/a;->h:Z

    if-eqz v2, :cond_1

    .line 168
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/api/a;->b(Lcom/google/android/gms/common/api/ap;)V

    .line 169
    monitor-exit v3

    .line 175
    :goto_0
    return-void

    .line 171
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v2

    if-nez v2, :cond_2

    move v2, v0

    :goto_1
    const-string v4, "Results have already been set"

    invoke-static {v2, v4}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 172
    iget-boolean v2, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v2, :cond_3

    :goto_2
    const-string v1, "Result has already been consumed"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 174
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/a;->c(Lcom/google/android/gms/common/api/ap;)V

    .line 175
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v2, v1

    .line 171
    goto :goto_1

    :cond_3
    move v0, v1

    .line 172
    goto :goto_2
.end method

.method public final a(Lcom/google/android/gms/common/api/aq;)V
    .locals 3

    .prologue
    .line 90
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Result has already been consumed."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 91
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/a;->c()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 93
    monitor-exit v1

    .line 101
    :goto_1
    return-void

    .line 90
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 95
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->c:Lcom/google/android/gms/common/api/b;

    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->i()Lcom/google/android/gms/common/api/ap;

    move-result-object v2

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/common/api/b;->a(Lcom/google/android/gms/common/api/aq;Lcom/google/android/gms/common/api/ap;)V

    .line 101
    :goto_2
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 99
    :cond_2
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/common/api/a;->e:Lcom/google/android/gms/common/api/aq;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bd;)V
    .locals 2

    .prologue
    .line 192
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 193
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/common/api/a;->j:Lcom/google/android/gms/common/internal/bd;

    .line 194
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public b()V
    .locals 2

    .prologue
    .line 138
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 139
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->h:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->g:Z

    if-eqz v0, :cond_1

    .line 141
    :cond_0
    monitor-exit v1

    .line 155
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->j:Lcom/google/android/gms/common/internal/bd;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_2

    .line 145
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->j:Lcom/google/android/gms/common/internal/bd;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/bd;->a()V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 151
    :cond_2
    :goto_1
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/a;->f:Lcom/google/android/gms/common/api/ap;

    invoke-static {v0}, Lcom/google/android/gms/common/api/a;->b(Lcom/google/android/gms/common/api/ap;)V

    .line 152
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/a;->e:Lcom/google/android/gms/common/api/aq;

    .line 153
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/a;->h:Z

    .line 154
    sget-object v0, Lcom/google/android/gms/common/api/Status;->e:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/a;->c(Lcom/google/android/gms/common/api/ap;)V

    .line 155
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final c()Z
    .locals 2

    .prologue
    .line 160
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 161
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/a;->h:Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 162
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected d()V
    .locals 0

    .prologue
    .line 189
    return-void
.end method

.method final e()V
    .locals 2

    .prologue
    .line 224
    iget-object v1, p0, Lcom/google/android/gms/common/api/a;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 225
    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/common/api/a;->h()Z

    move-result v0

    if-nez v0, :cond_0

    .line 226
    sget-object v0, Lcom/google/android/gms/common/api/Status;->d:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/a;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/a;->i:Z

    .line 229
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
