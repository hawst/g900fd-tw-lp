.class public final Lcom/google/android/gms/people/t;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/people/t;


# instance fields
.field private b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/people/t;

    invoke-direct {v0}, Lcom/google/android/gms/people/t;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/t;->a:Lcom/google/android/gms/people/t;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    const/16 v0, 0x14

    iput v0, p0, Lcom/google/android/gms/people/t;->d:I

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/people/t;
    .locals 0

    .prologue
    .line 59
    iput p1, p0, Lcom/google/android/gms/people/t;->d:I

    .line 60
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/people/t;
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gms/people/t;->b:Ljava/lang/String;

    .line 48
    return-object p0
.end method

.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/people/t;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/gms/people/t;->d:I

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/people/t;
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/google/android/gms/people/t;->c:Ljava/lang/String;

    .line 72
    return-object p0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/people/t;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x6

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    const-string v2, "mQuery"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/people/t;->b:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "mPageToken"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/people/t;->c:Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "mPageSize"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    iget v2, p0, Lcom/google/android/gms/people/t;->d:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Lcom/google/android/gms/people/internal/n;->a([Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
