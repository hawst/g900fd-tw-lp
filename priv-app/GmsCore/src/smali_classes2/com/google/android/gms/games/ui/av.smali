.class final Lcom/google/android/gms/games/ui/av;
.super Landroid/widget/BaseAdapter;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/aj;

.field private b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/aj;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 1007
    iput-object p1, p0, Lcom/google/android/gms/games/ui/av;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 1008
    invoke-static {p2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1009
    iput-object p2, p0, Lcom/google/android/gms/games/ui/av;->b:Ljava/util/ArrayList;

    .line 1010
    return-void
.end method


# virtual methods
.method public final areAllItemsEnabled()Z
    .locals 1

    .prologue
    .line 1014
    const/4 v0, 0x0

    return v0
.end method

.method public final getCount()I
    .locals 1

    .prologue
    .line 1034
    iget-object v0, p0, Lcom/google/android/gms/games/ui/av;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public final getItem(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1039
    iget-object v0, p0, Lcom/google/android/gms/games/ui/av;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final getItemId(I)J
    .locals 2

    .prologue
    .line 1044
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/av;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ay;

    iget v0, v0, Lcom/google/android/gms/games/ui/ay;->b:I

    int-to-long v0, v0

    return-wide v0
.end method

.method public final getItemViewType(I)I
    .locals 1

    .prologue
    .line 1029
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/av;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ay;

    iget v0, v0, Lcom/google/android/gms/games/ui/ay;->c:I

    return v0
.end method

.method public final getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4

    .prologue
    .line 1049
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/av;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ay;

    .line 1052
    if-nez p2, :cond_0

    .line 1053
    iget-object v1, v0, Lcom/google/android/gms/games/ui/ay;->f:Lcom/google/android/gms/games/ui/aj;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/aj;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, v0, Lcom/google/android/gms/games/ui/ay;->d:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 1058
    :cond_0
    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/ui/ay;->a(Landroid/view/View;)V

    .line 1060
    return-object p2
.end method

.method public final getViewTypeCount()I
    .locals 1

    .prologue
    .line 1024
    const/4 v0, 0x3

    return v0
.end method

.method public final isEnabled(I)Z
    .locals 1

    .prologue
    .line 1019
    iget-object v0, p0, Lcom/google/android/gms/games/ui/av;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/ay;

    iget-boolean v0, v0, Lcom/google/android/gms/games/ui/ay;->e:Z

    return v0
.end method
