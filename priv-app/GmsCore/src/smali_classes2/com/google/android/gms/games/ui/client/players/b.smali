.class final Lcom/google/android/gms/games/ui/client/players/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/client/players/h;

.field final synthetic b:Lcom/google/android/gms/games/ui/client/players/a;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/ui/client/players/a;Lcom/google/android/gms/games/ui/client/players/h;)V
    .locals 0

    .prologue
    .line 580
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    iput-object p2, p0, Lcom/google/android/gms/games/ui/client/players/b;->a:Lcom/google/android/gms/games/ui/client/players/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 2

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    .line 583
    if-eqz v0, :cond_0

    .line 584
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->removeView(Landroid/view/View;)V

    .line 586
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/b;->a:Lcom/google/android/gms/games/ui/client/players/h;

    iget-object v0, v0, Lcom/google/android/gms/games/ui/client/players/h;->b:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/client/players/a;->a(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 588
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->b(Lcom/google/android/gms/games/ui/client/players/a;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_1

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->d(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/widget/TextView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v1}, Lcom/google/android/gms/games/ui/client/players/a;->c(Lcom/google/android/gms/games/ui/client/players/a;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setHint(Ljava/lang/CharSequence;)V

    .line 597
    :cond_1
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_2

    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x11

    if-eq v0, v1, :cond_2

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/b;->b:Lcom/google/android/gms/games/ui/client/players/a;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/a;->d(Lcom/google/android/gms/games/ui/client/players/a;)Landroid/widget/TextView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/TextView;->requestFocus()Z

    .line 601
    :cond_2
    return-void
.end method
