.class public final Lcom/google/android/gms/people/identity/internal/k;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static final a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 6
    .annotation build Landroid/annotation/TargetApi;
        value = 0xe
    .end annotation

    .prologue
    const/4 v0, 0x0

    .line 613
    const/16 v1, 0xd

    invoke-virtual {p0, v1}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v1

    .line 614
    if-eqz v1, :cond_0

    const/16 v2, 0xe

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 616
    :try_start_0
    sget-object v2, Landroid/provider/ContactsContract$DisplayPhoto;->CONTENT_URI:Landroid/net/Uri;

    invoke-static {v1}, Ljava/lang/Long;->valueOf(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 623
    :cond_0
    :goto_0
    return-object v0

    .line 620
    :catch_0
    move-exception v1

    goto :goto_0
.end method
