.class public final Lcom/google/android/gms/checkin/b/j;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/checkin/b/i;

.field public b:Z

.field public c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Lcom/google/android/gms/checkin/b/p;

.field private g:Z

.field private h:Lcom/google/android/gms/checkin/b/q;

.field private i:Z

.field private j:J

.field private k:Ljava/util/List;

.field private l:Ljava/util/List;

.field private m:Ljava/util/List;

.field private n:Z

.field private o:Ljava/lang/String;

.field private p:Z

.field private q:Ljava/lang/String;

.field private r:Z

.field private s:I

.field private t:Z

.field private u:I

.field private v:Z

.field private w:I

.field private x:Z

.field private y:Lcom/google/android/gms/checkin/b/k;

.field private z:I


# direct methods
.method public constructor <init>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1107
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 1112
    iput-object v2, p0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    .line 1132
    iput-object v2, p0, Lcom/google/android/gms/checkin/b/j;->f:Lcom/google/android/gms/checkin/b/p;

    .line 1152
    iput-object v2, p0, Lcom/google/android/gms/checkin/b/j;->h:Lcom/google/android/gms/checkin/b/q;

    .line 1172
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/checkin/b/j;->j:J

    .line 1188
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    .line 1221
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    .line 1254
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    .line 1288
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->o:Ljava/lang/String;

    .line 1305
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->c:Ljava/lang/String;

    .line 1322
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->q:Ljava/lang/String;

    .line 1339
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/checkin/b/j;->s:I

    .line 1356
    iput v3, p0, Lcom/google/android/gms/checkin/b/j;->u:I

    .line 1373
    iput v3, p0, Lcom/google/android/gms/checkin/b/j;->w:I

    .line 1390
    iput-object v2, p0, Lcom/google/android/gms/checkin/b/j;->y:Lcom/google/android/gms/checkin/b/k;

    .line 1483
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/checkin/b/j;->z:I

    .line 1107
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1486
    iget v0, p0, Lcom/google/android/gms/checkin/b/j;->z:I

    if-gez v0, :cond_0

    .line 1488
    invoke-virtual {p0}, Lcom/google/android/gms/checkin/b/j;->b()I

    .line 1490
    :cond_0
    iget v0, p0, Lcom/google/android/gms/checkin/b/j;->z:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1343
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->r:Z

    .line 1344
    iput p1, p0, Lcom/google/android/gms/checkin/b/j;->s:I

    .line 1345
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1176
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->i:Z

    .line 1177
    iput-wide p1, p0, Lcom/google/android/gms/checkin/b/j;->j:J

    .line 1178
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/checkin/b/i;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1116
    if-nez p1, :cond_0

    .line 1117
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1119
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->d:Z

    .line 1120
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    .line 1121
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1394
    if-nez p1, :cond_0

    .line 1395
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1397
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->x:Z

    .line 1398
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/j;->y:Lcom/google/android/gms/checkin/b/k;

    .line 1399
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/checkin/b/l;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1238
    if-nez p1, :cond_0

    .line 1239
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1241
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1242
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    .line 1244
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1245
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1205
    if-nez p1, :cond_0

    .line 1206
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1208
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1209
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    .line 1211
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1212
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 1104
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    new-instance v0, Lcom/google/android/gms/checkin/b/i;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/i;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/i;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/checkin/b/j;->a(J)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/android/gms/checkin/b/l;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/l;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/l;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_4
    new-instance v0, Lcom/google/android/gms/checkin/b/o;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/o;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->a(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->a(I)Lcom/google/android/gms/checkin/b/j;

    goto :goto_0

    :sswitch_a
    new-instance v0, Lcom/google/android/gms/checkin/b/q;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/q;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/j;->g:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->h:Lcom/google/android/gms/checkin/b/q;

    goto :goto_0

    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/j;->t:Z

    iput v0, p0, Lcom/google/android/gms/checkin/b/j;->u:I

    goto/16 :goto_0

    :sswitch_c
    new-instance v0, Lcom/google/android/gms/checkin/b/p;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/p;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iput-boolean v2, p0, Lcom/google/android/gms/checkin/b/j;->e:Z

    iput-object v0, p0, Lcom/google/android/gms/checkin/b/j;->f:Lcom/google/android/gms/checkin/b/p;

    goto/16 :goto_0

    :sswitch_d
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->b(I)Lcom/google/android/gms/checkin/b/j;

    goto/16 :goto_0

    :sswitch_e
    new-instance v0, Lcom/google/android/gms/checkin/b/k;

    invoke-direct {v0}, Lcom/google/android/gms/checkin/b/k;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/checkin/b/j;->a(Lcom/google/android/gms/checkin/b/k;)Lcom/google/android/gms/checkin/b/j;

    goto/16 :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
        0x3a -> :sswitch_7
        0x42 -> :sswitch_8
        0x48 -> :sswitch_9
        0x5a -> :sswitch_a
        0x60 -> :sswitch_b
        0x6a -> :sswitch_c
        0x70 -> :sswitch_d
        0x7a -> :sswitch_e
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 1439
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->d:Z

    if-eqz v0, :cond_0

    .line 1440
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1442
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->i:Z

    if-eqz v0, :cond_1

    .line 1443
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/checkin/b/j;->j:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1445
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/l;

    .line 1446
    const/4 v2, 0x3

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 1448
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/o;

    .line 1449
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_1

    .line 1451
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1452
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    goto :goto_2

    .line 1454
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->n:Z

    if-eqz v0, :cond_5

    .line 1455
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->o:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1457
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->b:Z

    if-eqz v0, :cond_6

    .line 1458
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->c:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1460
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->p:Z

    if-eqz v0, :cond_7

    .line 1461
    const/16 v0, 0x8

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->q:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1463
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->r:Z

    if-eqz v0, :cond_8

    .line 1464
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/checkin/b/j;->s:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1466
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->g:Z

    if-eqz v0, :cond_9

    .line 1467
    const/16 v0, 0xb

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->h:Lcom/google/android/gms/checkin/b/q;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1469
    :cond_9
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->t:Z

    if-eqz v0, :cond_a

    .line 1470
    const/16 v0, 0xc

    iget v1, p0, Lcom/google/android/gms/checkin/b/j;->u:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1472
    :cond_a
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->e:Z

    if-eqz v0, :cond_b

    .line 1473
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->f:Lcom/google/android/gms/checkin/b/p;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1475
    :cond_b
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->v:Z

    if-eqz v0, :cond_c

    .line 1476
    const/16 v0, 0xe

    iget v1, p0, Lcom/google/android/gms/checkin/b/j;->w:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1478
    :cond_c
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->x:Z

    if-eqz v0, :cond_d

    .line 1479
    const/16 v0, 0xf

    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->y:Lcom/google/android/gms/checkin/b/k;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    .line 1481
    :cond_d
    return-void
.end method

.method public final b()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1495
    .line 1496
    iget-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->d:Z

    if-eqz v0, :cond_d

    .line 1497
    const/4 v0, 0x1

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->a:Lcom/google/android/gms/checkin/b/i;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1500
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/checkin/b/j;->i:Z

    if-eqz v2, :cond_0

    .line 1501
    const/4 v2, 0x2

    iget-wide v4, p0, Lcom/google/android/gms/checkin/b/j;->j:J

    invoke-static {v2, v4, v5}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v2

    add-int/2addr v0, v2

    .line 1504
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->l:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/l;

    .line 1505
    const/4 v4, 0x3

    invoke-static {v4, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    .line 1507
    goto :goto_1

    .line 1508
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/checkin/b/o;

    .line 1509
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v2, v0

    .line 1511
    goto :goto_2

    .line 1514
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1515
    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1517
    goto :goto_3

    .line 1518
    :cond_3
    add-int v0, v2, v1

    .line 1519
    iget-object v1, p0, Lcom/google/android/gms/checkin/b/j;->k:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 1521
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->n:Z

    if-eqz v1, :cond_4

    .line 1522
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->o:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1525
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->b:Z

    if-eqz v1, :cond_5

    .line 1526
    const/4 v1, 0x7

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->c:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1529
    :cond_5
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->p:Z

    if-eqz v1, :cond_6

    .line 1530
    const/16 v1, 0x8

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->q:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1533
    :cond_6
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->r:Z

    if-eqz v1, :cond_7

    .line 1534
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/checkin/b/j;->s:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1537
    :cond_7
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->g:Z

    if-eqz v1, :cond_8

    .line 1538
    const/16 v1, 0xb

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->h:Lcom/google/android/gms/checkin/b/q;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1541
    :cond_8
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->t:Z

    if-eqz v1, :cond_9

    .line 1542
    const/16 v1, 0xc

    iget v2, p0, Lcom/google/android/gms/checkin/b/j;->u:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1545
    :cond_9
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->e:Z

    if-eqz v1, :cond_a

    .line 1546
    const/16 v1, 0xd

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->f:Lcom/google/android/gms/checkin/b/p;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1549
    :cond_a
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->v:Z

    if-eqz v1, :cond_b

    .line 1550
    const/16 v1, 0xe

    iget v2, p0, Lcom/google/android/gms/checkin/b/j;->w:I

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1553
    :cond_b
    iget-boolean v1, p0, Lcom/google/android/gms/checkin/b/j;->x:Z

    if-eqz v1, :cond_c

    .line 1554
    const/16 v1, 0xf

    iget-object v2, p0, Lcom/google/android/gms/checkin/b/j;->y:Lcom/google/android/gms/checkin/b/k;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1557
    :cond_c
    iput v0, p0, Lcom/google/android/gms/checkin/b/j;->z:I

    .line 1558
    return v0

    :cond_d
    move v0, v1

    goto/16 :goto_0
.end method

.method public final b(I)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1377
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->v:Z

    .line 1378
    iput p1, p0, Lcom/google/android/gms/checkin/b/j;->w:I

    .line 1379
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1292
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->n:Z

    .line 1293
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/j;->o:Ljava/lang/String;

    .line 1294
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1309
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->b:Z

    .line 1310
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/j;->c:Ljava/lang/String;

    .line 1311
    return-object p0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/checkin/b/j;
    .locals 1

    .prologue
    .line 1326
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/checkin/b/j;->p:Z

    .line 1327
    iput-object p1, p0, Lcom/google/android/gms/checkin/b/j;->q:Ljava/lang/String;

    .line 1328
    return-object p0
.end method
