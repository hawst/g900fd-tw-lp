.class final Lcom/google/android/gms/car/nq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/util/concurrent/Semaphore;

.field final synthetic b:Lcom/google/android/gms/car/np;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/np;Ljava/util/concurrent/Semaphore;)V
    .locals 0

    .prologue
    .line 176
    iput-object p1, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    iput-object p2, p0, Lcom/google/android/gms/car/nq;->a:Ljava/util/concurrent/Semaphore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    iget-boolean v0, v0, Lcom/google/android/gms/car/np;->g:Z

    if-eqz v0, :cond_2

    .line 180
    const/4 v0, -0x8

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 186
    :goto_0
    :try_start_0
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    const-string v0, "CAR.MEDIA"

    const-string v1, "encodingThread starts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    iget-object v1, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    invoke-virtual {v1}, Lcom/google/android/gms/car/np;->a()Landroid/media/MediaCodec;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/car/np;->j:Landroid/media/MediaCodec;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/car/nq;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 191
    iget-object v0, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    invoke-static {v0}, Lcom/google/android/gms/car/np;->a(Lcom/google/android/gms/car/np;)V

    .line 192
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 193
    const-string v0, "CAR.MEDIA"

    const-string v1, "encodingThread ends"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    :cond_1
    :goto_1
    return-void

    .line 182
    :cond_2
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    goto :goto_0

    .line 195
    :catch_0
    move-exception v0

    .line 197
    iget-object v1, p0, Lcom/google/android/gms/car/nq;->a:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V

    .line 198
    iget-object v1, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    const/4 v2, 0x0

    iput-boolean v2, v1, Lcom/google/android/gms/car/np;->i:Z

    .line 199
    const-string v1, "CAR.MEDIA"

    const-string v2, "media encoder error"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 200
    iget-object v1, p0, Lcom/google/android/gms/car/nq;->b:Lcom/google/android/gms/car/np;

    iget-object v1, v1, Lcom/google/android/gms/car/np;->h:Lcom/google/android/gms/car/nr;

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/nr;->a(Ljava/lang/String;)V

    goto :goto_1
.end method
