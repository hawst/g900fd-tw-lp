.class public Lcom/google/android/gms/common/download/DownloadCompleteIntentService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    const-string v0, "DownloadCompleteService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 36
    return-void
.end method

.method private a(Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/common/download/f;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 114
    .line 119
    :try_start_0
    iget-object v0, p2, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    if-eqz v0, :cond_4

    new-instance v0, Ljava/io/File;

    invoke-virtual {p0}, Lcom/google/android/gms/common/download/DownloadCompleteIntentService;->getFilesDir()Ljava/io/File;

    move-result-object v3

    iget-object v4, p2, Lcom/google/android/gms/common/download/f;->e:Ljava/lang/String;

    invoke-direct {v0, v3, v4}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    :goto_0
    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_0

    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    :cond_0
    new-instance v5, Ljava/io/File;

    iget-object v3, p2, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-direct {v5, v0, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 120
    new-instance v0, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".tmp"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 121
    new-instance v4, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;

    invoke-direct {v4, p1}, Landroid/os/ParcelFileDescriptor$AutoCloseInputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 122
    :try_start_1
    new-instance v3, Ljava/io/FileOutputStream;

    invoke-direct {v3, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_5
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 123
    const/16 v2, 0x1000

    :try_start_2
    new-array v2, v2, [B

    .line 125
    :goto_1
    invoke-virtual {v4, v2}, Ljava/io/FileInputStream;->read([B)I

    move-result v6

    const/4 v7, -0x1

    if-eq v6, v7, :cond_5

    .line 126
    const/4 v7, 0x0

    invoke-virtual {v3, v2, v7, v6}, Ljava/io/FileOutputStream;->write([BII)V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    goto :goto_1

    .line 130
    :catch_0
    move-exception v0

    move-object v2, v0

    .line 131
    :goto_2
    :try_start_3
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "DownloadCompleteService"

    const-string v5, "Failed to copy to private storage."

    invoke-static {v0, v5, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    :cond_1
    iget-object v0, p2, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    .line 133
    if-eqz v4, :cond_2

    .line 137
    :try_start_4
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 139
    :cond_2
    if-eqz v3, :cond_3

    .line 140
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_3

    :cond_3
    move v0, v1

    .line 163
    :goto_3
    return v0

    .line 119
    :cond_4
    :try_start_5
    new-instance v0, Ljava/io/File;

    invoke-static {p0}, Lcom/google/android/gms/common/download/DownloadService;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_1
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_0

    .line 130
    :catch_1
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v2, v0

    goto :goto_2

    .line 129
    :cond_5
    :try_start_6
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V
    :try_end_6
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    .line 136
    :try_start_7
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 139
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_2

    .line 148
    invoke-static {v0}, Lcom/google/android/gms/common/download/DownloadService;->a(Ljava/io/File;)Ljava/lang/String;

    move-result-object v2

    .line 149
    iget-object v3, p2, Lcom/google/android/gms/common/download/f;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_c

    .line 150
    invoke-virtual {v0, v5}, Ljava/io/File;->renameTo(Ljava/io/File;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 151
    invoke-virtual {v5, v1}, Ljava/io/File;->setExecutable(Z)Z

    .line 152
    invoke-virtual {v5, v1}, Ljava/io/File;->setWritable(Z)Z

    .line 154
    invoke-static {p0, p2}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;)V

    .line 155
    const/4 v0, 0x1

    goto :goto_3

    .line 143
    :catch_2
    move-exception v0

    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    const-string v0, "DownloadCompleteService"

    const-string v2, "Failed to close file."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_6
    move v0, v1

    .line 144
    goto :goto_3

    .line 143
    :catch_3
    move-exception v0

    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "DownloadCompleteService"

    const-string v2, "Failed to close file."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    move v0, v1

    .line 144
    goto :goto_3

    .line 135
    :catchall_0
    move-exception v0

    move-object v4, v2

    .line 136
    :goto_4
    if-eqz v4, :cond_8

    .line 137
    :try_start_8
    invoke-virtual {v4}, Ljava/io/FileInputStream;->close()V

    .line 139
    :cond_8
    if-eqz v2, :cond_9

    .line 140
    invoke-virtual {v2}, Ljava/io/FileOutputStream;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_4

    .line 144
    :cond_9
    throw v0

    .line 143
    :catch_4
    move-exception v0

    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_a

    const-string v0, "DownloadCompleteService"

    const-string v2, "Failed to close file."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_a
    move v0, v1

    .line 144
    goto/16 :goto_3

    .line 157
    :cond_b
    const-string v0, "DownloadCompleteService"

    const-string v2, "Failed to move file."

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v1

    .line 158
    goto/16 :goto_3

    .line 161
    :cond_c
    const-string v0, "DownloadCompleteService"

    const-string v2, "SHA-1 of downloaded file does not match request"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    iget-object v0, p2, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    move v0, v1

    .line 163
    goto/16 :goto_3

    .line 135
    :catchall_1
    move-exception v0

    goto :goto_4

    :catchall_2
    move-exception v0

    move-object v2, v3

    goto :goto_4

    .line 130
    :catch_5
    move-exception v0

    move-object v3, v2

    move-object v2, v0

    goto/16 :goto_2
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 40
    const-string v0, "extra_download_id"

    invoke-virtual {p1, v0, v4, v5}, Landroid/content/Intent;->getLongExtra(Ljava/lang/String;J)J

    move-result-wide v2

    .line 41
    cmp-long v0, v2, v4

    if-nez v0, :cond_1

    .line 42
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "DownloadCompleteService"

    const-string v1, "Received an ID that is not valid."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 89
    :cond_0
    :goto_0
    return-void

    .line 46
    :cond_1
    const-string v0, "download"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/download/DownloadCompleteIntentService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/DownloadManager;

    .line 48
    new-instance v1, Landroid/app/DownloadManager$Query;

    invoke-direct {v1}, Landroid/app/DownloadManager$Query;-><init>()V

    const/4 v4, 0x1

    new-array v4, v4, [J

    const/4 v5, 0x0

    aput-wide v2, v4, v5

    invoke-virtual {v1, v4}, Landroid/app/DownloadManager$Query;->setFilterById([J)Landroid/app/DownloadManager$Query;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/app/DownloadManager;->query(Landroid/app/DownloadManager$Query;)Landroid/database/Cursor;

    move-result-object v1

    .line 50
    if-eqz v1, :cond_2

    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v4

    if-nez v4, :cond_3

    .line 51
    :cond_2
    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 62
    if-eqz v1, :cond_0

    .line 63
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 55
    :cond_3
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v4

    if-eqz v4, :cond_5

    const-string v4, "status"

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getColumnIndexOrThrow(Ljava/lang/String;)I

    move-result v4

    invoke-interface {v1, v4}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    const/16 v5, 0x8

    if-eq v4, v5, :cond_5

    .line 57
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "DownloadCompleteService"

    const-string v4, "Download not successful."

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_4
    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 62
    if-eqz v1, :cond_0

    .line 63
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 62
    :cond_5
    if-eqz v1, :cond_6

    .line 63
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 67
    :cond_6
    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->a(Landroid/content/Context;J)Lcom/google/android/gms/common/download/f;

    move-result-object v1

    .line 68
    if-nez v1, :cond_9

    .line 69
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_7

    const-string v0, "DownloadCompleteService"

    const-string v1, "Download enqueued improperly."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    :cond_7
    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 62
    :catchall_0
    move-exception v0

    if-eqz v1, :cond_8

    .line 63
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    :cond_8
    throw v0

    .line 76
    :cond_9
    :try_start_2
    invoke-virtual {v0, v2, v3}, Landroid/app/DownloadManager;->openDownloadedFile(J)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 77
    if-nez v0, :cond_a

    .line 78
    new-instance v0, Ljava/io/FileNotFoundException;

    invoke-direct {v0}, Ljava/io/FileNotFoundException;-><init>()V

    throw v0
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_0

    .line 81
    :catch_0
    move-exception v0

    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V

    goto/16 :goto_0

    .line 85
    :cond_a
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/download/DownloadCompleteIntentService;->a(Landroid/os/ParcelFileDescriptor;Lcom/google/android/gms/common/download/f;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 86
    sget-object v0, Lcom/google/android/gms/common/download/a/a;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_b

    const-string v0, "DownloadCompleteService"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Download Service Completed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    :cond_b
    invoke-static {p0, v2, v3}, Lcom/google/android/gms/common/download/r;->b(Landroid/content/Context;J)V

    goto/16 :goto_0
.end method
