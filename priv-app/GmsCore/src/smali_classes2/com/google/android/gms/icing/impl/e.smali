.class public final Lcom/google/android/gms/icing/impl/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Landroid/content/Context;

.field final b:Lcom/google/android/gms/icing/b/a;

.field final c:Lcom/google/android/gms/icing/impl/m;

.field final d:Lcom/google/android/gms/icing/impl/a/f;

.field final e:Lcom/google/android/gms/icing/impl/a/p;

.field final f:Ljava/util/Set;

.field final g:Ljava/lang/Runnable;

.field private final h:Lcom/google/android/gms/icing/impl/a;

.field private final i:Lcom/google/android/gms/icing/impl/NativeIndex;

.field private final j:Lcom/google/android/gms/icing/impl/bn;

.field private final k:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/a;Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/m;Lcom/google/android/gms/icing/impl/NativeIndex;Lcom/google/android/gms/icing/impl/bn;Lcom/google/android/gms/icing/impl/a/p;Lcom/google/android/gms/icing/impl/a/f;)V
    .locals 1

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e;->k:Ljava/util/Set;

    .line 92
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e;->f:Ljava/util/Set;

    .line 98
    new-instance v0, Lcom/google/android/gms/icing/impl/f;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/f;-><init>(Lcom/google/android/gms/icing/impl/e;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/e;->g:Ljava/lang/Runnable;

    .line 114
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/e;->a:Landroid/content/Context;

    .line 115
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/e;->h:Lcom/google/android/gms/icing/impl/a;

    .line 116
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    .line 117
    iput-object p4, p0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    .line 118
    iput-object p5, p0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    .line 119
    iput-object p6, p0, Lcom/google/android/gms/icing/impl/e;->j:Lcom/google/android/gms/icing/impl/bn;

    .line 120
    iput-object p7, p0, Lcom/google/android/gms/icing/impl/e;->e:Lcom/google/android/gms/icing/impl/a/p;

    .line 121
    iput-object p8, p0, Lcom/google/android/gms/icing/impl/e;->d:Lcom/google/android/gms/icing/impl/a/f;

    .line 122
    return-void
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/g;JJ)V
    .locals 11

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 191
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 192
    const-string v0, "Cannot schedule indexing on non-synced corpus %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;Ljava/lang/Object;)I

    .line 237
    :cond_0
    :goto_0
    return-void

    .line 195
    :cond_1
    iget-object v4, p2, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 196
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->d:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 197
    if-eqz v0, :cond_2

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/a/e;->c:Lcom/google/android/gms/icing/impl/q;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/q;->m(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 198
    :cond_2
    const-string v0, "Not indexing corpus from package %s as it has never connected"

    invoke-static {v0, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 201
    :cond_3
    iget v5, p2, Lcom/google/android/gms/icing/g;->a:I

    .line 202
    iget-boolean v7, p2, Lcom/google/android/gms/icing/g;->g:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->e:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/e;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 209
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/e;->a()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 211
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/h;

    move-object v2, p0

    move-object v3, p1

    move-wide/from16 v8, p5

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/icing/impl/h;-><init>(Lcom/google/android/gms/icing/impl/e;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;ZJ)V

    invoke-virtual {v0, v1, p3, p4}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/icing/g;JLcom/google/android/gms/icing/impl/d;Lcom/google/android/gms/icing/impl/az;)Z
    .locals 22

    .prologue
    .line 402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 404
    :try_start_0
    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v15

    .line 405
    move-object/from16 v0, p4

    move-wide/from16 v1, p2

    invoke-virtual {v0, v15, v1, v2}, Lcom/google/android/gms/icing/impl/d;->a(Landroid/net/Uri;J)V

    .line 408
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/icing/impl/a/j;->c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v16

    .line 410
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-wide v18

    .line 413
    const/4 v14, 0x0

    move-wide/from16 v12, p2

    .line 414
    :goto_0
    :try_start_1
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->c()Z

    move-result v4

    if-eqz v4, :cond_17

    .line 415
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->a()Lcom/google/android/gms/icing/impl/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/c;->b()J

    move-result-wide v4

    .line 416
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->b()Lcom/google/android/gms/icing/impl/c;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/icing/impl/c;->b()J

    move-result-wide v6

    .line 418
    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    .line 419
    cmp-long v4, v4, v6

    if-nez v4, :cond_1

    const/4 v4, 0x1

    move v8, v4

    .line 422
    :goto_1
    cmp-long v4, v6, v12

    if-nez v4, :cond_3

    .line 423
    add-int/lit8 v4, v14, 0x1

    .line 424
    const-string v9, "Dup seqno for %s pkg %s %d last %d"

    const/4 v5, 0x4

    new-array v10, v5, [Ljava/lang/Object;

    const/4 v11, 0x0

    if-eqz v8, :cond_2

    const-string v5, "docs"

    :goto_2
    aput-object v5, v10, v11

    const/4 v5, 0x1

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    aput-object v11, v10, v5

    const/4 v5, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v5

    const/4 v5, 0x3

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v11

    aput-object v11, v10, v5

    invoke-static {v9, v10}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    move v14, v4

    .line 433
    :goto_3
    cmp-long v4, v6, v12

    if-ltz v4, :cond_0

    const/16 v4, 0xa

    if-le v14, v4, :cond_5

    .line 434
    :cond_0
    const-string v5, "Out of order seqno for %s pkg %s %d last %d"

    const/4 v4, 0x4

    new-array v9, v4, [Ljava/lang/Object;

    const/4 v10, 0x0

    if-eqz v8, :cond_4

    const-string v4, "docs"

    :goto_4
    aput-object v4, v9, v10

    const/4 v4, 0x1

    move-object/from16 v0, p1

    iget-object v8, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    aput-object v8, v9, v4

    const/4 v4, 0x2

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v4

    const/4 v4, 0x3

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v9, v4

    invoke-static {v5, v9}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 437
    const-string v4, "out of order seqno"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 439
    move-object/from16 v0, p5

    iput-wide v12, v0, Lcom/google/android/gms/icing/impl/az;->b:J

    .line 470
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    iget v5, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v4, v5, v12, v13}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IJ)V

    const/4 v4, 0x0

    .line 472
    :goto_5
    return v4

    .line 419
    :cond_1
    const/4 v4, 0x0

    move v8, v4

    goto :goto_1

    .line 424
    :cond_2
    :try_start_2
    const-string v5, "tags"

    goto :goto_2

    .line 429
    :cond_3
    const/4 v4, 0x0

    move v14, v4

    goto :goto_3

    .line 434
    :cond_4
    const-string v4, "tags"

    goto :goto_4

    .line 443
    :cond_5
    if-eqz v8, :cond_f

    .line 444
    cmp-long v4, v6, v12

    if-lez v4, :cond_6

    .line 445
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->a()Lcom/google/android/gms/icing/impl/c;

    move-result-object v10

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/gms/icing/g;->a:I

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v5, 0x2

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/b/a;->b(I)V

    const-string v4, "action"

    invoke-virtual {v10, v4}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "uri"

    invoke-virtual {v10, v5}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "Uri"

    const/16 v11, 0x100

    invoke-static {v9, v5, v11}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v9

    if-eqz v9, :cond_7

    const-string v4, "bad uri"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    :goto_6
    move-wide v12, v6

    .line 450
    :cond_6
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->a()Lcom/google/android/gms/icing/impl/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/c;->c()Z

    move-result v4

    if-nez v4, :cond_11

    .line 451
    move-object/from16 v0, p4

    invoke-virtual {v0, v15, v6, v7}, Lcom/google/android/gms/icing/impl/d;->a(Landroid/net/Uri;J)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    move-wide v4, v12

    .line 463
    :goto_7
    :try_start_3
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_2

    move-result-wide v6

    sub-long v6, v6, v18

    const-wide/16 v8, 0x1388

    cmp-long v6, v6, v8

    if-ltz v6, :cond_14

    .line 464
    :goto_8
    move-object/from16 v0, p5

    iput-wide v4, v0, Lcom/google/android/gms/icing/impl/az;->b:J

    .line 470
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v6, v7, v4, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IJ)V

    .line 472
    const/4 v4, 0x1

    goto :goto_5

    .line 445
    :cond_7
    :try_start_4
    const-string v9, "add"

    invoke-static {v4, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_d

    new-instance v11, Lcom/google/android/gms/icing/o;

    invoke-direct {v11}, Lcom/google/android/gms/icing/o;-><init>()V

    iput-object v5, v11, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    const-string v4, "doc_score"

    const/4 v5, 0x0

    iget-boolean v9, v10, Lcom/google/android/gms/icing/impl/c;->b:Z

    if-eqz v9, :cond_16

    iget-object v9, v10, Lcom/google/android/gms/icing/impl/c;->c:Ljava/util/Map;

    invoke-interface {v9, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_16

    iget-object v5, v10, Lcom/google/android/gms/icing/impl/c;->a:Lcom/google/android/gms/icing/impl/bh;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/icing/impl/bh;->b(I)I

    move-result v4

    :goto_9
    iput v4, v11, Lcom/google/android/gms/icing/o;->c:I

    iput v8, v11, Lcom/google/android/gms/icing/o;->a:I

    const-string v4, "created_timestamp"

    const-wide/16 v8, 0x0

    iget-boolean v5, v10, Lcom/google/android/gms/icing/impl/c;->b:Z

    if-eqz v5, :cond_15

    iget-object v5, v10, Lcom/google/android/gms/icing/impl/c;->c:Ljava/util/Map;

    invoke-interface {v5, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    if-eqz v4, :cond_15

    iget-object v5, v10, Lcom/google/android/gms/icing/impl/c;->a:Lcom/google/android/gms/icing/impl/bh;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v5, v4}, Lcom/google/android/gms/icing/impl/bh;->c(I)J

    move-result-wide v4

    :goto_a
    iput-wide v4, v11, Lcom/google/android/gms/icing/o;->f:J

    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->size()I

    move-result v4

    new-array v4, v4, [Lcom/google/android/gms/icing/q;

    iput-object v4, v11, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    const/4 v4, 0x0

    invoke-interface/range {v16 .. v16}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v5

    invoke-interface {v5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v8, v4

    :cond_8
    :goto_b
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_c

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/Map$Entry;

    new-instance v17, Ljava/lang/StringBuilder;

    const-string v5, "section_"

    move-object/from16 v0, v17

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    move-object/from16 v0, v17

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/icing/impl/a/t;

    invoke-virtual {v10, v5}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v17

    if-nez v17, :cond_8

    new-instance v17, Lcom/google/android/gms/icing/q;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/gms/icing/q;-><init>()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    :try_start_5
    iget v0, v4, Lcom/google/android/gms/icing/impl/a/t;->a:I

    move/from16 v20, v0

    move/from16 v0, v20

    move-object/from16 v1, v17

    iput v0, v1, Lcom/google/android/gms/icing/q;->a:I

    iget-object v0, v4, Lcom/google/android/gms/icing/impl/a/t;->b:Lcom/google/android/gms/icing/av;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    const-string v20, "UTF-8"

    move-object/from16 v0, v20

    invoke-virtual {v5, v0}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, v17

    iput-object v0, v1, Lcom/google/android/gms/icing/q;->c:[B

    iget-object v0, v4, Lcom/google/android/gms/icing/impl/a/t;->b:Lcom/google/android/gms/icing/av;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    iget v0, v0, Lcom/google/android/gms/icing/av;->c:I

    move/from16 v20, v0

    const/16 v21, 0x2

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_9

    iget-object v4, v4, Lcom/google/android/gms/icing/impl/a/t;->b:Lcom/google/android/gms/icing/av;

    iget-object v0, v4, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_a

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/bg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/google/android/gms/icing/q;->f:[B
    :try_end_5
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :cond_9
    :goto_c
    :try_start_6
    iget-object v5, v11, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    add-int/lit8 v4, v8, 0x1

    aput-object v17, v5, v8
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move v8, v4

    goto/16 :goto_b

    :cond_a
    :try_start_7
    invoke-static/range {v20 .. v20}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v5

    const/4 v4, 0x0

    :goto_d
    array-length v0, v5

    move/from16 v21, v0

    move/from16 v0, v21

    if-ge v4, v0, :cond_b

    aget-object v21, v5, v4

    invoke-static/range {v21 .. v21}, Lcom/google/android/gms/icing/impl/bg;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    aput-object v21, v5, v4

    add-int/lit8 v4, v4, 0x1

    goto :goto_d

    :cond_b
    move-object/from16 v0, v20

    invoke-static {v0, v5}, Landroid/text/TextUtils;->join(Ljava/lang/CharSequence;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-virtual {v4, v5}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v4

    move-object/from16 v0, v17

    iput-object v4, v0, Lcom/google/android/gms/icing/q;->f:[B
    :try_end_7
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    goto :goto_c

    :catch_0
    move-exception v4

    :try_start_8
    new-instance v5, Ljava/lang/IllegalStateException;

    invoke-direct {v5, v4}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v5
    :try_end_8
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 469
    :catchall_0
    move-exception v4

    :goto_e
    move-object/from16 v0, p5

    iput-wide v12, v0, Lcom/google/android/gms/icing/impl/az;->b:J

    .line 470
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    move-object/from16 v0, p1

    iget v6, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v5, v6, v12, v13}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IJ)V

    throw v4

    .line 445
    :cond_c
    :try_start_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4, v6, v7, v11}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JLcom/google/android/gms/icing/o;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "index "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_d
    const-string v9, "del"

    invoke-static {v4, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4, v6, v7, v8, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILjava/lang/String;)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "delete "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    :cond_e
    const-string v4, "bad action"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    goto/16 :goto_6

    .line 454
    :cond_f
    cmp-long v4, v6, v12

    if-lez v4, :cond_10

    .line 455
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->b()Lcom/google/android/gms/icing/impl/c;

    move-result-object v4

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/gms/icing/g;->a:I

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v9, 0x2

    invoke-virtual {v5, v9}, Lcom/google/android/gms/icing/b/a;->b(I)V

    const-string v5, "action"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v9, "uri"

    invoke-virtual {v4, v9}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    const-string v10, "tag"

    invoke-virtual {v4, v10}, Lcom/google/android/gms/icing/impl/c;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    move-result-object v10

    :try_start_a
    const-string v4, "Uri"

    const/16 v11, 0x100

    invoke-static {v4, v9, v11}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    const-string v4, "Tag"

    const/16 v11, 0x3e8

    invoke-static {v4, v10, v11}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    :try_end_a
    .catch Ljava/lang/IllegalArgumentException; {:try_start_a .. :try_end_a} :catch_1
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    :try_start_b
    const-string v4, "add"

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_12

    const/4 v11, 0x1

    :goto_f
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual/range {v5 .. v11}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(JILjava/lang/String;Ljava/lang/String;Z)I

    move-result v4

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v8, "tag "

    invoke-direct {v5, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->e(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v5, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    :goto_10
    move-wide v12, v6

    .line 459
    :cond_10
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/icing/impl/d;->b()Lcom/google/android/gms/icing/impl/c;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/c;->c()Z

    move-result v4

    if-nez v4, :cond_11

    .line 460
    move-object/from16 v0, p4

    invoke-virtual {v0, v15, v6, v7}, Lcom/google/android/gms/icing/impl/d;->a(Landroid/net/Uri;J)V

    :cond_11
    move-wide v4, v12

    goto/16 :goto_7

    .line 455
    :catch_1
    move-exception v4

    const-string v4, "bad tag args"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    goto :goto_10

    :cond_12
    const-string v4, "del"

    invoke-static {v5, v4}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_13

    const/4 v11, 0x0

    goto :goto_f

    :cond_13
    const-string v4, "bad action"

    move-object/from16 v0, p5

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    goto :goto_10

    .line 469
    :catchall_1
    move-exception v4

    move-wide/from16 v12, p2

    goto/16 :goto_e

    :catchall_2
    move-exception v6

    move-wide v12, v4

    move-object v4, v6

    goto/16 :goto_e

    :cond_14
    move-wide v12, v4

    goto/16 :goto_0

    :cond_15
    move-wide v4, v8

    goto/16 :goto_a

    :cond_16
    move v4, v5

    goto/16 :goto_9

    :cond_17
    move-wide v4, v12

    goto/16 :goto_8
.end method


# virtual methods
.method final a()Ljava/util/Set;
    .locals 2

    .prologue
    .line 141
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->k:Ljava/util/Set;

    return-object v0
.end method

.method final a(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)V
    .locals 10

    .prologue
    .line 251
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    new-instance v1, Lcom/google/android/gms/icing/impl/i;

    move-object v2, p0

    move-object v3, p1

    move v4, p2

    move-object v5, p3

    move v6, p4

    move-object v7, p5

    move-wide/from16 v8, p6

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/icing/impl/i;-><init>(Lcom/google/android/gms/icing/impl/e;Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)V

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/icing/b/a;->a(Lcom/google/android/gms/icing/b/h;J)Lcom/google/android/gms/icing/b/h;

    .line 291
    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/g;)V
    .locals 8

    .prologue
    .line 240
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 241
    const-wide/16 v4, 0x3e8

    const-wide/32 v6, 0x493e0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;Lcom/google/android/gms/icing/g;JJ)V

    .line 243
    return-void
.end method

.method final a(Ljava/util/Set;JJ)V
    .locals 8

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 152
    invoke-interface {p1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 153
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/e;->e:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v1

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v1

    .line 154
    const-string v3, "Indexing for %s"

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 155
    iget-object v3, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    move-object v1, p0

    move-wide v4, p2

    move-wide v6, p4

    invoke-direct/range {v1 .. v7}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;Lcom/google/android/gms/icing/g;JJ)V

    goto :goto_0

    .line 157
    :cond_0
    return-void
.end method

.method final b(Ljava/lang/String;ILjava/lang/String;ZLcom/google/android/gms/icing/impl/d;J)I
    .locals 16

    .prologue
    .line 295
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->b:Lcom/google/android/gms/icing/b/a;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/b/a;->b(I)V

    .line 297
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->e:Lcom/google/android/gms/icing/impl/a/p;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/a/p;->a()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v9

    .line 298
    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v3

    .line 299
    if-eqz v3, :cond_0

    iget-object v2, v3, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v2, v2, Lcom/google/android/gms/icing/j;->d:I

    if-nez v2, :cond_0

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 303
    :cond_0
    const/4 v2, 0x2

    .line 396
    :cond_1
    :goto_0
    return v2

    .line 306
    :cond_2
    iget-object v6, v3, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 307
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->j:Lcom/google/android/gms/icing/impl/bn;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->o()D

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Lcom/google/android/gms/icing/impl/bn;->a(D)I

    move-result v4

    if-eqz p4, :cond_6

    if-nez v4, :cond_5

    const/4 v2, 0x1

    :goto_1
    if-nez v2, :cond_3

    const-string v5, "Cannot sync trimmable corpus: %s"

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bn;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_3
    :goto_2
    if-nez v2, :cond_8

    .line 309
    iget-boolean v2, v6, Lcom/google/android/gms/icing/j;->e:Z

    if-nez v2, :cond_4

    .line 310
    const/4 v2, 0x1

    iput-boolean v2, v6, Lcom/google/android/gms/icing/j;->e:Z

    .line 311
    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v6}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/j;)V

    .line 313
    :cond_4
    const/4 v2, 0x2

    goto :goto_0

    .line 307
    :cond_5
    const/4 v2, 0x0

    goto :goto_1

    :cond_6
    const/4 v2, 0x1

    if-gt v4, v2, :cond_7

    const/4 v2, 0x1

    :goto_3
    if-nez v2, :cond_3

    const-string v5, "Cannot sync untrimmable corpus: %s"

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bn;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_2

    :cond_7
    const/4 v2, 0x0

    goto :goto_3

    .line 316
    :cond_8
    iget-boolean v2, v6, Lcom/google/android/gms/icing/j;->e:Z

    if-eqz v2, :cond_9

    .line 317
    const/4 v2, 0x0

    iput-boolean v2, v6, Lcom/google/android/gms/icing/j;->e:Z

    .line 318
    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v6}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/j;)V

    .line 321
    :cond_9
    iget v8, v6, Lcom/google/android/gms/icing/j;->f:I

    .line 323
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->i:Lcom/google/android/gms/icing/impl/NativeIndex;

    const/4 v4, 0x0

    move/from16 v0, p2

    invoke-virtual {v2, v0, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IZ)J

    move-result-wide v4

    .line 325
    const-wide/16 v10, 0x0

    cmp-long v2, v4, v10

    if-gez v2, :cond_a

    .line 326
    const-wide/16 v4, 0x0

    .line 329
    :cond_a
    new-instance v7, Lcom/google/android/gms/icing/impl/az;

    iget-object v2, v6, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    invoke-direct {v7, v2, v4, v5}, Lcom/google/android/gms/icing/impl/az;-><init>([Lcom/google/android/gms/icing/k;J)V

    .line 333
    :try_start_0
    iget-object v3, v3, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    move-object/from16 v2, p0

    move-object/from16 v6, p5

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/e;->a(Lcom/google/android/gms/icing/g;JLcom/google/android/gms/icing/impl/d;Lcom/google/android/gms/icing/impl/az;)Z
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/bi; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    .line 335
    const/4 v3, 0x0

    .line 336
    if-nez v2, :cond_c

    .line 337
    const/4 v2, 0x2

    .line 374
    :goto_4
    invoke-virtual {v7}, Lcom/google/android/gms/icing/impl/az;->a()V

    .line 375
    iget-wide v10, v7, Lcom/google/android/gms/icing/impl/az;->b:J

    .line 376
    cmp-long v6, v10, v4

    if-lez v6, :cond_12

    const/4 v6, 0x1

    .line 378
    :goto_5
    if-eqz v6, :cond_13

    .line 379
    const-string v8, "Indexed %s from %d to %d"

    const/4 v10, 0x3

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    aput-object p1, v10, v11

    const/4 v11, 0x1

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    aput-object v4, v10, v11

    const/4 v4, 0x2

    iget-wide v12, v7, Lcom/google/android/gms/icing/impl/az;->b:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    aput-object v5, v10, v4

    invoke-static {v8, v10}, Lcom/google/android/gms/icing/c;->a(Ljava/lang/String;[Ljava/lang/Object;)I

    .line 384
    :goto_6
    iget-object v4, v7, Lcom/google/android/gms/icing/impl/az;->a:[Lcom/google/android/gms/icing/k;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    const/4 v7, 0x0

    move-object/from16 v0, p1

    invoke-virtual {v9, v0, v4, v5, v7}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;[Lcom/google/android/gms/icing/k;Ljava/lang/Integer;Lcom/google/android/gms/icing/l;)V

    .line 386
    if-lez v3, :cond_b

    .line 387
    const-string v3, "Retrying indexing in %dms"

    invoke-static/range {p6 .. p7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;Ljava/lang/Object;)I

    .line 388
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/e;->h:Lcom/google/android/gms/icing/impl/a;

    move-wide/from16 v0, p6

    invoke-interface {v3, v0, v1}, Lcom/google/android/gms/icing/impl/a;->a(J)V

    .line 392
    :cond_b
    if-eqz v6, :cond_1

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/google/android/gms/icing/impl/a/j;->g(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 393
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/e;->f:Ljava/util/Set;

    move-object/from16 v0, p1

    invoke-virtual {v9, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v3, v4}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0

    .line 338
    :cond_c
    :try_start_1
    iget-wide v10, v7, Lcom/google/android/gms/icing/impl/az;->b:J
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/bi; {:try_start_1 .. :try_end_1} :catch_1

    cmp-long v2, v10, v4

    if-lez v2, :cond_d

    .line 339
    const/4 v2, 0x1

    goto :goto_4

    .line 341
    :cond_d
    const/4 v2, 0x0

    goto :goto_4

    .line 343
    :catch_0
    move-exception v2

    move v3, v8

    :goto_7
    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/bi;->getCause()Ljava/lang/Throwable;

    move-result-object v2

    .line 345
    const-string v6, "Cursor call threw an exception"

    const/4 v8, 0x0

    new-array v8, v8, [Ljava/lang/Object;

    invoke-static {v2, v6, v8}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 346
    instance-of v6, v2, Landroid/os/DeadObjectException;

    if-eqz v6, :cond_10

    .line 347
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v6, "cursor_died_exception"

    invoke-interface {v2, v6}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 350
    const-string v2, "cursor-died"

    invoke-virtual {v7, v2}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    .line 351
    if-nez v3, :cond_e

    .line 353
    const/4 v2, 0x3

    .line 354
    const-string v3, "Indexing content provider failed; will retry %d times"

    const/4 v6, 0x3

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 372
    :goto_8
    const/4 v3, 0x2

    move v14, v3

    move v3, v2

    move v2, v14

    goto/16 :goto_4

    .line 356
    :cond_e
    add-int/lit8 v2, v3, -0x1

    .line 357
    if-nez v2, :cond_f

    .line 358
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v6, "cursor_retries_failed"

    invoke-interface {v3, v6}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 360
    :cond_f
    const-string v3, "Indexing content provider failed again; %d retries remaining"

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v3, v6}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_8

    .line 364
    :cond_10
    instance-of v2, v2, Ljava/lang/SecurityException;

    if-eqz v2, :cond_11

    .line 365
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v3, "cursor_security_exception"

    invoke-interface {v2, v3}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    .line 369
    :goto_9
    const-string v2, "cursor-exception"

    invoke-virtual {v7, v2}, Lcom/google/android/gms/icing/impl/az;->a(Ljava/lang/String;)V

    .line 370
    const/4 v2, 0x0

    goto :goto_8

    .line 367
    :cond_11
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/e;->c:Lcom/google/android/gms/icing/impl/m;

    const-string v3, "cursor_other_exception"

    invoke-interface {v2, v3}, Lcom/google/android/gms/icing/impl/m;->a(Ljava/lang/String;)V

    goto :goto_9

    .line 376
    :cond_12
    const/4 v6, 0x0

    goto/16 :goto_5

    .line 381
    :cond_13
    const-string v4, "Query from %s found nothing"

    move-object/from16 v0, p1

    invoke-static {v4, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    goto/16 :goto_6

    .line 343
    :catch_1
    move-exception v2

    goto :goto_7
.end method
