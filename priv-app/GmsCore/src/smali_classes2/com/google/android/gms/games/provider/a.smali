.class public final Lcom/google/android/gms/games/provider/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/HashMap;

.field public final b:[Ljava/lang/String;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/provider/c;)V
    .locals 4

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iget-object v0, p1, Lcom/google/android/gms/games/provider/c;->a:Ljava/util/HashMap;

    iput-object v0, p0, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    .line 59
    const/4 v0, 0x0

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 61
    iget-object v3, p0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    aput-object v0, v3, v1

    .line 62
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 63
    goto :goto_0

    .line 64
    :cond_0
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/provider/c;B)V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/a;-><init>(Lcom/google/android/gms/games/provider/c;)V

    return-void
.end method

.method public static a()Lcom/google/android/gms/games/provider/c;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/gms/games/provider/c;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/c;-><init>(B)V

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/database/Cursor;)Landroid/content/ContentValues;
    .locals 10

    .prologue
    const/4 v1, 0x0

    .line 93
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    move v0, v1

    .line 94
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 95
    iget-object v2, p0, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    aget-object v4, v2, v0

    .line 96
    invoke-interface {p1, v4}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v2

    .line 97
    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/provider/a;->a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;

    move-result-object v5

    .line 98
    sget-object v6, Lcom/google/android/gms/games/provider/b;->a:[I

    invoke-virtual {v5}, Lcom/google/android/gms/games/provider/d;->ordinal()I

    move-result v7

    aget v6, v6, v7

    packed-switch v6, :pswitch_data_0

    .line 112
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Type should not be in cursor: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 100
    :pswitch_1
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 103
    :pswitch_2
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    goto :goto_1

    .line 106
    :pswitch_3
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    goto :goto_1

    .line 109
    :pswitch_4
    invoke-interface {p1, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    const-wide/16 v8, 0x1

    cmp-long v2, v6, v8

    if-nez v2, :cond_0

    const/4 v2, 0x1

    :goto_2
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1

    :cond_0
    move v2, v1

    goto :goto_2

    .line 115
    :cond_1
    return-object v3

    .line 98
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/games/provider/d;
    .locals 4

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "No entry for column %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/provider/a;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/d;

    return-object v0
.end method
