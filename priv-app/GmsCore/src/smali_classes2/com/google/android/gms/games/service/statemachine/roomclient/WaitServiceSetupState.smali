.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 21
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 32
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 58
    :pswitch_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->g:Z

    :goto_0
    return v0

    .line 37
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bv;

    .line 39
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->g:Lcom/google/android/gms/games/service/statemachine/f;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/f;->a()Lcom/google/android/gms/games/service/statemachine/g;

    move-result-object v1

    .line 40
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bv;->a:Landroid/os/IBinder;

    new-instance v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cn;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;Lcom/google/android/gms/games/service/statemachine/g;)V

    const/4 v1, 0x0

    invoke-interface {v0, v2, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 51
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->i:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->b()V

    .line 52
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->f:Z

    goto :goto_0

    .line 48
    :catch_0
    move-exception v0

    .line 49
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 55
    :pswitch_2
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 56
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->f:Z

    goto :goto_0

    .line 32
    :pswitch_data_0
    .packed-switch 0x1b
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitServiceSetupState;->c()V

    .line 28
    return-void
.end method
