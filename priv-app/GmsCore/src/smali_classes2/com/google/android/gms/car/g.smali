.class public final Lcom/google/android/gms/car/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field final a:Z

.field final b:I

.field volatile c:Z

.field d:Landroid/media/AudioRecord;

.field volatile e:Z

.field volatile f:Z

.field volatile g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

.field final h:Lcom/google/android/gms/car/gx;

.field final i:Lcom/google/android/gms/car/aw;

.field j:Lcom/google/android/gms/car/nz;

.field private k:Lcom/google/android/gms/car/i;

.field private l:Ljava/lang/Thread;

.field private volatile m:Z

.field private n:[Lcom/google/android/c/b/f;

.field private o:[Lcom/google/android/gms/car/CarAudioConfiguration;

.field private p:I

.field private volatile q:Z

.field private volatile r:Z

.field private volatile s:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;IZ)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    .line 119
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->m:Z

    .line 127
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/g;->p:I

    .line 129
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->e:Z

    .line 130
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->q:Z

    .line 134
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z

    .line 136
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->r:Z

    .line 152
    iput-object p1, p0, Lcom/google/android/gms/car/g;->h:Lcom/google/android/gms/car/gx;

    .line 153
    iput-object p2, p0, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    .line 154
    iput p3, p0, Lcom/google/android/gms/car/g;->b:I

    .line 155
    const-string v0, "GalReceiver-Local"

    iget-object v1, p0, Lcom/google/android/gms/car/g;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->d()Lcom/google/android/gms/car/CarInfo;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/car/CarInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->a:Z

    .line 160
    :goto_0
    return-void

    .line 158
    :cond_0
    iput-boolean p4, p0, Lcom/google/android/gms/car/g;->a:Z

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/g;)V
    .locals 10

    .prologue
    const/4 v9, 0x2

    const/4 v8, 0x5

    const/4 v1, 0x0

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v0, v9}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(I)V

    move v0, v1

    :cond_0
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->c:Z

    if-nez v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z

    if-eqz v2, :cond_1

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    if-ne v2, v8, :cond_2

    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z

    if-nez v2, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    const/4 v2, 0x1

    invoke-interface {v0, v2}, Lcom/google/android/gms/car/i;->a(I)V

    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i()V

    iput-boolean v1, p0, Lcom/google/android/gms/car/g;->f:Z

    return-void

    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->l()I

    move-result v2

    const/16 v3, 0x8

    if-le v2, v3, :cond_3

    const-wide/16 v2, 0xa

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v2

    goto :goto_0

    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->k()Lcom/google/android/gms/car/e;

    move-result-object v3

    iget-object v4, v3, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v4

    :try_start_1
    iget-object v5, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    array-length v6, v4

    invoke-interface {v5, v4, v6, v2}, Lcom/google/android/gms/car/i;->a([BII)Z

    move-result v2

    if-nez v2, :cond_5

    iget-object v2, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/e;)V

    add-int/lit8 v0, v0, 0x1

    if-le v0, v8, :cond_0

    new-instance v0, Ljava/io/IOException;

    const-string v2, "cannot read"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/nio/BufferUnderflowException; {:try_start_1 .. :try_end_1} :catch_2

    :catch_1
    move-exception v0

    const-string v2, "CAR.AUDIO"

    invoke-static {v2, v8}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Exception while reading from client, stream:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    invoke-interface {v0, v9}, Lcom/google/android/gms/car/i;->a(I)V

    goto :goto_1

    :cond_5
    iget-object v0, v3, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, v3, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    array-length v2, v4

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, v3, Lcom/google/android/gms/car/e;->a:J

    iget-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(Lcom/google/android/gms/car/e;)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/g;->g()V

    move v0, v1

    goto/16 :goto_0

    :catch_2
    move-exception v0

    goto/16 :goto_1
.end method

.method static a([B)Z
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 757
    array-length v2, p0

    const/16 v3, 0x3c

    if-gt v2, v3, :cond_1

    .line 759
    array-length v0, p0

    invoke-static {p0, v1, v0}, Lcom/google/android/gms/car/g;->a([BII)Z

    move-result v0

    .line 772
    :cond_0
    :goto_0
    return v0

    .line 762
    :cond_1
    const/4 v2, 0x3

    new-array v3, v2, [I

    aput v1, v3, v1

    array-length v2, p0

    add-int/lit8 v2, v2, -0x14

    div-int/lit8 v2, v2, 0x2

    aput v2, v3, v0

    const/4 v2, 0x2

    array-length v4, p0

    add-int/lit8 v4, v4, -0x14

    aput v4, v3, v2

    .line 767
    array-length v4, v3

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    aget v5, v3, v2

    .line 768
    const/16 v6, 0x14

    invoke-static {p0, v5, v6}, Lcom/google/android/gms/car/g;->a([BII)Z

    move-result v5

    if-nez v5, :cond_0

    .line 767
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_2
    move v0, v1

    .line 772
    goto :goto_0
.end method

.method private static a([BII)Z
    .locals 2

    .prologue
    .line 779
    move v0, p1

    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_1

    .line 780
    aget-byte v1, p0, v0

    if-eqz v1, :cond_0

    .line 781
    const/4 v0, 0x1

    .line 784
    :goto_1
    return v0

    .line 779
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 784
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private declared-synchronized h()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 372
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 373
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startSystemSoundStreaming "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->a:Z

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/g;->b:I

    if-ne v0, v3, :cond_1

    .line 376
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 378
    :cond_1
    monitor-exit p0

    return-void

    .line 372
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized i()V
    .locals 3

    .prologue
    .line 384
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 385
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopSoundStreaming "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->k()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 388
    monitor-exit p0

    return-void

    .line 384
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private j()V
    .locals 3

    .prologue
    .line 391
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->c:Z

    .line 392
    new-instance v0, Lcom/google/android/gms/car/h;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "AudioCapture-"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/h;-><init>(Lcom/google/android/gms/car/g;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    .line 403
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->start()V

    .line 404
    return-void
.end method

.method private k()V
    .locals 4

    .prologue
    .line 409
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->c:Z

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    if-eq v0, v1, :cond_0

    .line 415
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 416
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V

    .line 417
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 418
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio capturing thread not finishing for stream:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v2, v3}, Ljava/lang/Thread;->join(J)V

    .line 421
    iget-object v0, p0, Lcom/google/android/gms/car/g;->l:Ljava/lang/Thread;

    invoke-virtual {v0}, Ljava/lang/Thread;->isAlive()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->q:Z

    if-eqz v0, :cond_0

    .line 422
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "audio capturing thread not finishing, 2nd trial, for stream:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/car/g;->h:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->A()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 432
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 163
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onReady "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->m:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 184
    :goto_0
    monitor-exit p0

    return-void

    .line 170
    :cond_1
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/car/g;->b:I

    iget-object v1, p0, Lcom/google/android/gms/car/g;->o:[Lcom/google/android/gms/car/CarAudioConfiguration;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/bg;->a(I[Lcom/google/android/gms/car/CarAudioConfiguration;)I

    move-result v0

    .line 172
    iget-boolean v1, p0, Lcom/google/android/gms/car/g;->a:Z

    if-eqz v1, :cond_3

    iget v1, p0, Lcom/google/android/gms/car/g;->b:I

    if-ne v1, v3, :cond_3

    .line 173
    const/4 v1, -0x1

    if-ne v0, v1, :cond_2

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/car/g;->h:Lcom/google/android/gms/car/gx;

    const/4 v2, 0x2

    const-string v3, "48000 stereo not supported"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 177
    :cond_2
    iput v0, p0, Lcom/google/android/gms/car/g;->p:I

    .line 178
    new-instance v1, Lcom/google/android/gms/car/nz;

    iget-object v2, p0, Lcom/google/android/gms/car/g;->o:[Lcom/google/android/gms/car/CarAudioConfiguration;

    aget-object v0, v2, v0

    invoke-static {v0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/CarAudioConfiguration;)I

    move-result v0

    const/16 v2, 0x18

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/car/nz;-><init>(II)V

    iput-object v1, p0, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    .line 181
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->h()V

    .line 183
    :cond_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->m:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 3

    .prologue
    .line 205
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onChannelAvailable, stream:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 208
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 209
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->a:Z

    if-eqz v0, :cond_2

    .line 210
    iget-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(I)V

    .line 211
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 217
    :cond_1
    :goto_0
    monitor-exit p0

    return-void

    .line 212
    :cond_2
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/car/g;->b:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_1

    .line 213
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->f:Z

    .line 215
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 205
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/i;)V
    .locals 1

    .prologue
    .line 246
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    .line 247
    iget-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->m:Z

    if-eqz v0, :cond_0

    .line 249
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->k()V

    .line 250
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->j()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 252
    :cond_0
    monitor-exit p0

    return-void

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 271
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 6

    .prologue
    .line 789
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stream type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " has focus:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/g;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " config chosen:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/g;->p:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 792
    const-string v0, "***Supported configs"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 793
    iget-object v1, p0, Lcom/google/android/gms/car/g;->n:[Lcom/google/android/c/b/f;

    .line 794
    if-eqz v1, :cond_1

    .line 795
    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 796
    if-eqz v3, :cond_0

    .line 797
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "num bits:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " num chs:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v3, Lcom/google/android/c/b/f;->c:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " sampling rate:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v3, v3, Lcom/google/android/c/b/f;->a:I

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 795
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 801
    :cond_1
    const-string v0, "null configs"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 803
    :cond_2
    return-void
.end method

.method public final declared-synchronized a([Lcom/google/android/c/b/f;[Lcom/google/android/gms/car/CarAudioConfiguration;)V
    .locals 1

    .prologue
    .line 257
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/g;->n:[Lcom/google/android/c/b/f;

    .line 258
    iput-object p2, p0, Lcom/google/android/gms/car/g;->o:[Lcom/google/android/gms/car/CarAudioConfiguration;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 259
    monitor-exit p0

    return-void

    .line 257
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 286
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 337
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 289
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    if-ne v2, v1, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/aw;->d(I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 291
    iget-object v2, p0, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    iget-object v3, v2, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v4, 0x2

    aget-object v3, v3, v4

    if-eqz v3, :cond_2

    iget-object v2, v2, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    const/4 v4, 0x2

    aget-object v2, v2, v4

    invoke-virtual {v3, v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    :cond_2
    iput-object v3, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 292
    iget-object v2, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    if-eqz v2, :cond_0

    .line 295
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z

    .line 318
    :cond_3
    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->m:Z

    if-nez v2, :cond_7

    .line 319
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startPlayback while car not ready "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 296
    :cond_4
    :try_start_2
    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_3

    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    if-nez v2, :cond_3

    .line 298
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    iget v3, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/aw;->b(I)Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    move-result-object v2

    .line 300
    iput-object v2, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 301
    if-nez v2, :cond_6

    .line 303
    const-wide/16 v2, 0x3e8

    invoke-virtual {p0, v2, v3}, Ljava/lang/Object;->wait(J)V

    .line 307
    :goto_1
    iget-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z

    if-nez v2, :cond_3

    .line 308
    const-string v1, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 309
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startPlayback focus timeout, stream:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/g;->i:Lcom/google/android/gms/car/aw;

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/aw;->c(I)V

    goto/16 :goto_0

    .line 315
    :catch_0
    move-exception v1

    goto/16 :goto_0

    .line 305
    :cond_6
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/car/g;->f:Z
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 322
    :cond_7
    :try_start_4
    const-string v0, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 323
    const-string v0, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "startPlayback with configIndex "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stream "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_8
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->k()V

    .line 326
    iget v0, p0, Lcom/google/android/gms/car/g;->p:I

    if-eq p1, v0, :cond_a

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/car/g;->n:[Lcom/google/android/c/b/f;

    array-length v0, v0

    if-lt p1, v0, :cond_9

    .line 328
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Wrong configuration index "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 330
    :cond_9
    const-string v0, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 331
    const-string v0, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "config change from "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/g;->p:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " to "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " stream"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 335
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->e:Z

    .line 336
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->j()V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move v0, v1

    .line 337
    goto/16 :goto_0
.end method

.method public final declared-synchronized b()V
    .locals 3

    .prologue
    .line 195
    monitor-enter p0

    :try_start_0
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onChannelLost, stream:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->f:Z

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    if-eqz v0, :cond_1

    .line 200
    iget-object v0, p0, Lcom/google/android/gms/car/g;->k:Lcom/google/android/gms/car/i;

    invoke-interface {v0}, Lcom/google/android/gms/car/i;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 202
    :cond_1
    monitor-exit p0

    return-void

    .line 195
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 2

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/g;->s:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 224
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->r:Z

    .line 225
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->r:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_0

    .line 227
    const-wide/16 v0, 0x3e8

    :try_start_1
    invoke-virtual {p0, v0, v1}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 232
    :cond_0
    :goto_0
    :try_start_2
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->r:Z

    if-eqz v0, :cond_1

    .line 233
    const-string v0, "CAR.AUDIO"

    const-string v1, "onChannelSwitch timed-out"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 236
    :cond_1
    monitor-exit p0

    return-void

    :catch_0
    move-exception v0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final b(I)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 445
    new-instance v0, Landroid/media/AudioAttributes$Builder;

    invoke-direct {v0}, Landroid/media/AudioAttributes$Builder;-><init>()V

    .line 448
    :try_start_0
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "setInternalCapturePreset"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 450
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const/16 v6, 0x8

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioAttributes$Builder;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 463
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    const-string v4, "addTag"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Ljava/lang/String;

    aput-object v7, v5, v6

    invoke-virtual {v3, v4, v5}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v3

    .line 464
    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    const-string v6, "fixedVolume"

    aput-object v6, v4, v5

    invoke-virtual {v3, v0, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioAttributes$Builder;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_3
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_5

    .line 476
    invoke-virtual {v0}, Landroid/media/AudioAttributes$Builder;->build()Landroid/media/AudioAttributes;

    move-result-object v0

    .line 477
    new-instance v3, Landroid/media/AudioFormat$Builder;

    invoke-direct {v3}, Landroid/media/AudioFormat$Builder;-><init>()V

    invoke-virtual {v3, v8}, Landroid/media/AudioFormat$Builder;->setEncoding(I)Landroid/media/AudioFormat$Builder;

    move-result-object v3

    const/16 v4, 0xc

    invoke-virtual {v3, v4}, Landroid/media/AudioFormat$Builder;->setChannelMask(I)Landroid/media/AudioFormat$Builder;

    move-result-object v3

    const v4, 0xbb80

    invoke-virtual {v3, v4}, Landroid/media/AudioFormat$Builder;->setSampleRate(I)Landroid/media/AudioFormat$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/media/AudioFormat$Builder;->build()Landroid/media/AudioFormat;

    move-result-object v3

    .line 484
    :try_start_2
    const-class v4, Landroid/media/AudioRecord;

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Class;

    const/4 v6, 0x0

    const-class v7, Landroid/media/AudioAttributes;

    aput-object v7, v5, v6

    const/4 v6, 0x1

    const-class v7, Landroid/media/AudioFormat;

    aput-object v7, v5, v6

    const/4 v6, 0x2

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    const/4 v6, 0x3

    sget-object v7, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v7, v5, v6

    invoke-virtual {v4, v5}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v4

    const/4 v5, 0x4

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    aput-object v0, v5, v6

    const/4 v0, 0x1

    aput-object v3, v5, v0

    const/4 v0, 0x2

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    const/4 v0, 0x3

    const/4 v3, 0x0

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v0

    invoke-virtual {v4, v5}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioRecord;

    iput-object v0, p0, Lcom/google/android/gms/car/g;->d:Landroid/media/AudioRecord;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_6
    .catch Ljava/lang/IllegalAccessException; {:try_start_2 .. :try_end_2} :catch_7
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_2 .. :try_end_2} :catch_8
    .catch Ljava/lang/InstantiationException; {:try_start_2 .. :try_end_2} :catch_9

    move v0, v1

    .line 500
    :goto_0
    return v0

    .line 451
    :catch_0
    move-exception v0

    .line 452
    const-string v1, "CAR.AUDIO"

    const-string v3, "setInternalCapturePreset method not found"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 453
    goto :goto_0

    .line 454
    :catch_1
    move-exception v0

    .line 455
    const-string v1, "CAR.AUDIO"

    const-string v3, "setInternalCapturePreset invocation error"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 456
    goto :goto_0

    .line 457
    :catch_2
    move-exception v0

    .line 458
    const-string v1, "CAR.AUDIO"

    const-string v3, "setInternalCapturePreset not accessible"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 459
    goto :goto_0

    .line 465
    :catch_3
    move-exception v0

    .line 466
    const-string v1, "CAR.AUDIO"

    const-string v3, "addTag method not found"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 467
    goto :goto_0

    .line 468
    :catch_4
    move-exception v0

    .line 469
    const-string v1, "CAR.AUDIO"

    const-string v3, "addTag invocation error"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 470
    goto :goto_0

    .line 471
    :catch_5
    move-exception v0

    .line 472
    const-string v1, "CAR.AUDIO"

    const-string v3, "setInternalCapturePreset not accessible"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 473
    goto :goto_0

    .line 487
    :catch_6
    move-exception v0

    .line 488
    const-string v1, "CAR.AUDIO"

    const-string v3, "AudioRecord constructor not found"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 489
    goto :goto_0

    .line 490
    :catch_7
    move-exception v0

    .line 491
    const-string v1, "CAR.AUDIO"

    const-string v3, "AudioRecord constructor not accessible"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 492
    goto :goto_0

    .line 493
    :catch_8
    move-exception v0

    .line 494
    const-string v1, "CAR.AUDIO"

    const-string v3, "AudioRecord constructor invocation error"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 495
    goto :goto_0

    .line 496
    :catch_9
    move-exception v0

    .line 497
    const-string v1, "CAR.AUDIO"

    const-string v3, "AudioRecord object not instantiated"

    invoke-static {v1, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move v0, v2

    .line 498
    goto :goto_0
.end method

.method public final declared-synchronized c()V
    .locals 1

    .prologue
    .line 263
    monitor-enter p0

    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->q:Z

    .line 264
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->m:Z

    .line 265
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->i()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    monitor-exit p0

    return-void

    .line 263
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 344
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->q:Z

    if-eqz v0, :cond_0

    .line 360
    :goto_0
    return-void

    .line 347
    :cond_0
    monitor-enter p0

    .line 348
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->e:Z

    if-nez v0, :cond_2

    .line 349
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 350
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopPlayback ignored "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    :cond_1
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 354
    :cond_2
    :try_start_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 355
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "stopPlayback for the current config "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->p:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " stream "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 358
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/car/g;->k()V

    .line 359
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->e:Z

    .line 360
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final declared-synchronized e()Z
    .locals 1

    .prologue
    .line 365
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->e:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final f()V
    .locals 4

    .prologue
    .line 655
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->f:Z

    if-nez v0, :cond_1

    .line 669
    :cond_0
    return-void

    .line 658
    :cond_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 659
    iget-object v0, p0, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    .line 660
    if-lez v0, :cond_2

    .line 661
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "system capture Q has entries:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 664
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/g;->j:Lcom/google/android/gms/car/nz;

    iget-object v0, v0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    .line 665
    if-eqz v0, :cond_0

    .line 666
    iget-object v1, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->b(Lcom/google/android/gms/car/e;)V

    goto :goto_0
.end method

.method final g()V
    .locals 3

    .prologue
    .line 672
    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->r:Z

    if-eqz v0, :cond_1

    .line 673
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 674
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "switching BH for stream:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/g;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " from BH "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to BH "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/g;->s:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 678
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->i()V

    .line 679
    iget-object v1, p0, Lcom/google/android/gms/car/g;->s:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iget-boolean v0, p0, Lcom/google/android/gms/car/g;->a:Z

    if-eqz v0, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(I)V

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/car/g;->s:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iput-object v0, p0, Lcom/google/android/gms/car/g;->g:Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 683
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/g;->r:Z

    .line 684
    monitor-enter p0

    .line 685
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V

    .line 686
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 688
    :cond_1
    return-void

    .line 679
    :cond_2
    const/4 v0, 0x2

    goto :goto_0

    .line 686
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
