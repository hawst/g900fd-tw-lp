.class public final Lcom/google/android/gms/people/identity/r;
.super Lcom/google/android/gms/people/identity/k;
.source "SourceFile"


# instance fields
.field private final b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private final d:Ljava/util/List;

.field private final e:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/identity/p;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;I)V
    .locals 9

    .prologue
    .line 493
    invoke-direct {p0, p1}, Lcom/google/android/gms/people/identity/k;-><init>(Lcom/google/android/gms/people/identity/p;)V

    .line 495
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 496
    invoke-static {p5, p6}, Lcom/google/android/gms/people/identity/r;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/p;

    .line 497
    new-instance v3, Lcom/google/android/gms/people/identity/m;

    invoke-direct {v3, v0}, Lcom/google/android/gms/people/identity/m;-><init>(Lcom/google/android/gms/people/identity/p;)V

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 500
    :cond_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 501
    invoke-static {p2, p6}, Lcom/google/android/gms/people/identity/r;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/p;

    .line 502
    new-instance v4, Lcom/google/android/gms/people/identity/l;

    const-string v5, "postal_address"

    invoke-virtual {v0, v5}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, "type"

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v4, v5, v0}, Lcom/google/android/gms/people/identity/l;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 507
    :cond_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 508
    invoke-static {p3, p6}, Lcom/google/android/gms/people/identity/r;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/p;

    .line 509
    new-instance v5, Lcom/google/android/gms/people/identity/n;

    const-string v6, "email"

    invoke-virtual {v0, v6}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "type"

    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v5, v6, v0}, Lcom/google/android/gms/people/identity/n;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 514
    :cond_2
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 515
    invoke-static {p4, p6}, Lcom/google/android/gms/people/identity/r;->a(Lcom/google/android/gms/common/data/DataHolder;I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/identity/p;

    .line 516
    new-instance v6, Lcom/google/android/gms/people/identity/o;

    const-string v7, "phone"

    invoke-virtual {v0, v7}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    const-string v8, "type"

    invoke-virtual {v0, v8}, Lcom/google/android/gms/people/identity/p;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-direct {v6, v7, v0}, Lcom/google/android/gms/people/identity/o;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v4, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_3

    .line 521
    :cond_3
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/r;->b:Ljava/util/List;

    .line 522
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/r;->c:Ljava/util/List;

    .line 523
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/r;->d:Ljava/util/List;

    .line 524
    invoke-static {v4}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/r;->e:Ljava/util/List;

    .line 525
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    const-string v0, "name"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 534
    const-string v0, "gaia_id"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 539
    const-string v0, "name_verified"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->c(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 544
    const-string v0, "tagline"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 550
    const-string v0, "profile_type"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->b(Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 555
    const-string v0, "avatar"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/r;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/List;
    .locals 1

    .prologue
    .line 560
    iget-object v0, p0, Lcom/google/android/gms/people/identity/r;->b:Ljava/util/List;

    return-object v0
.end method

.method public final h()Ljava/util/List;
    .locals 1

    .prologue
    .line 565
    iget-object v0, p0, Lcom/google/android/gms/people/identity/r;->c:Ljava/util/List;

    return-object v0
.end method

.method public final i()Ljava/util/List;
    .locals 1

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/gms/people/identity/r;->e:Ljava/util/List;

    return-object v0
.end method

.method public final j()Ljava/util/List;
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/gms/people/identity/r;->d:Ljava/util/List;

    return-object v0
.end method
