.class public final Lcom/google/android/gms/games/service/a/e/h;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/a/au;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 35
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 36
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/e/h;->b:Lcom/google/android/gms/games/a/au;

    .line 37
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/e/h;->c:Ljava/lang/String;

    .line 38
    iput-wide p3, p0, Lcom/google/android/gms/games/service/a/e/h;->d:J

    .line 39
    iput-wide p5, p0, Lcom/google/android/gms/games/service/a/e/h;->e:J

    .line 40
    iput-object p7, p0, Lcom/google/android/gms/games/service/a/e/h;->f:Ljava/lang/String;

    .line 41
    iput-boolean p8, p0, Lcom/google/android/gms/games/service/a/e/h;->g:Z

    .line 42
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 0

    .prologue
    .line 53
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 10

    .prologue
    .line 46
    iget-object v2, p0, Lcom/google/android/gms/games/service/a/e/h;->b:Lcom/google/android/gms/games/a/au;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/e/h;->c:Ljava/lang/String;

    iget-wide v4, p0, Lcom/google/android/gms/games/service/a/e/h;->d:J

    iget-wide v6, p0, Lcom/google/android/gms/games/service/a/e/h;->e:J

    iget-object v8, p0, Lcom/google/android/gms/games/service/a/e/h;->f:Ljava/lang/String;

    iget-boolean v9, p0, Lcom/google/android/gms/games/service/a/e/h;->g:Z

    move-object v1, p2

    invoke-virtual/range {v1 .. v9}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;JJLjava/lang/String;Z)I

    move-result v0

    return v0
.end method
