.class public abstract Lcom/google/android/gms/mdm/a;
.super Landroid/app/Activity;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Class;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 18
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/app/Activity;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_0

    .line 31
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/gms/mdm/a;->a()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/a;->startActivity(Landroid/content/Intent;)V

    .line 32
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/a;->finish()V

    .line 33
    :goto_0
    return-void

    .line 24
    :catch_0
    move-exception v0

    const-string v0, "LockscreenActivity calling app incorrectly signed, closing."

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 25
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/a;->setResult(I)V

    .line 26
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/a;->finish()V

    goto :goto_0
.end method
