.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;
.super Lcom/google/android/gms/auth/trustagent/as;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/bg;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 11
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/as;-><init>()V

    return-void
.end method

.method private g()Lcom/google/android/gms/auth/trustagent/bb;
    .locals 2

    .prologue
    .line 69
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const v1, 0x1020002

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/bb;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->g()Lcom/google/android/gms/auth/trustagent/bb;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/trustagent/bb;->a(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method protected final e()Landroid/preference/PreferenceFragment;
    .locals 1

    .prologue
    .line 74
    new-instance v0, Lcom/google/android/gms/auth/trustagent/bb;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/bb;-><init>()V

    return-object v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 1

    .prologue
    .line 47
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 48
    packed-switch p1, :pswitch_data_0

    .line 66
    :cond_0
    :goto_0
    return-void

    .line 50
    :pswitch_0
    const-string v0, "bluetooth_device_address"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 52
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->g()Lcom/google/android/gms/auth/trustagent/bb;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/auth/trustagent/bb;->a(Landroid/content/Intent;)Z

    goto :goto_0

    .line 53
    :cond_1
    const-string v0, "SELECTED_NFC_DEVICE"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 62
    :pswitch_1
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->g()Lcom/google/android/gms/auth/trustagent/bb;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/auth/trustagent/bb;->b(Landroid/content/Intent;)V

    goto :goto_0

    .line 59
    :pswitch_2
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->g()Lcom/google/android/gms/auth/trustagent/bb;

    move-result-object v0

    invoke-virtual {v0, p3}, Lcom/google/android/gms/auth/trustagent/bb;->a(Landroid/content/Intent;)Z

    goto :goto_0

    .line 48
    nop

    :pswitch_data_0
    .packed-switch 0x3e9
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 16
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/as;->onNewIntent(Landroid/content/Intent;)V

    .line 18
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->setIntent(Landroid/content/Intent;)V

    .line 19
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Lcom/google/android/gms/auth/trustagent/as;->onResume()V

    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 26
    if-nez v1, :cond_0

    .line 43
    :goto_0
    return-void

    .line 30
    :cond_0
    const-string v0, "com.google.android.gms.auth.TrustAgentIdOfNotificationToClose"

    invoke-virtual {v1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 31
    const-string v0, "com.google.android.gms.auth.TrustAgentIdOfNotificationToClose"

    const/4 v2, 0x0

    invoke-virtual {v1, v0, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 33
    const-string v0, "notification"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    .line 35
    invoke-virtual {v0, v2}, Landroid/app/NotificationManager;->cancel(I)V

    .line 39
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/as;->a:Landroid/preference/PreferenceFragment;

    check-cast v0, Lcom/google/android/gms/auth/trustagent/bb;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/bb;->a(Landroid/content/Intent;)Z

    .line 42
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedDevicesSettings;->setIntent(Landroid/content/Intent;)V

    goto :goto_0
.end method
