.class public final Lcom/google/android/gms/backup/b/d;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private volatile a:Ljava/io/IOException;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;)V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 14
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/gms/backup/b/d;->a:Ljava/io/IOException;

    if-eqz v0, :cond_0

    .line 80
    new-instance v0, Ljava/io/IOException;

    iget-object v1, p0, Lcom/google/android/gms/backup/b/d;->a:Ljava/io/IOException;

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v0

    .line 82
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/IOException;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/google/android/gms/backup/b/d;->a:Ljava/io/IOException;

    .line 74
    return-void
.end method

.method public final available()I
    .locals 1

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 54
    invoke-super {p0}, Ljava/io/FilterInputStream;->available()I

    move-result v0

    return v0
.end method

.method public final close()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 60
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 61
    return-void
.end method

.method public final mark(I)V
    .locals 0

    .prologue
    .line 43
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->mark(I)V

    .line 44
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Ljava/io/FilterInputStream;->markSupported()Z

    move-result v0

    return v0
.end method

.method public final read()I
    .locals 1

    .prologue
    .line 18
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 19
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    return v0
.end method

.method public final read([B)I
    .locals 1

    .prologue
    .line 24
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 25
    invoke-super {p0, p1}, Ljava/io/FilterInputStream;->read([B)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 32
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    return v0
.end method

.method public final reset()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 38
    invoke-super {p0}, Ljava/io/FilterInputStream;->reset()V

    .line 39
    return-void
.end method

.method public final skip(J)J
    .locals 3

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/backup/b/d;->a()V

    .line 66
    invoke-super {p0, p1, p2}, Ljava/io/FilterInputStream;->skip(J)J

    move-result-wide v0

    return-wide v0
.end method
