.class public abstract Lcom/google/android/gms/common/e/a;
.super Landroid/content/ContentProvider;
.source "SourceFile"

# interfaces
.implements Landroid/database/sqlite/SQLiteTransactionListener;


# static fields
.field private static final c:Ljava/lang/Object;


# instance fields
.field private volatile a:Z

.field private final b:Ljava/lang/ThreadLocal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 40
    new-instance v0, Ljava/lang/ThreadLocal;

    invoke-direct {v0}, Ljava/lang/ThreadLocal;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    return-void
.end method

.method protected static a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 5

    .prologue
    .line 234
    invoke-static {p1}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v0

    .line 235
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "=?"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    invoke-static {v0, v1}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v4

    invoke-virtual {p0, p2, p4, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 4

    .prologue
    .line 154
    array-length v2, p3

    .line 155
    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 157
    const/4 v0, 0x0

    move-object v1, p1

    :goto_0
    if-ge v0, v2, :cond_1

    .line 158
    :try_start_0
    aget-object v3, p3, v0

    invoke-virtual {p0, v1, p2, v3}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v3

    .line 159
    if-eqz v3, :cond_0

    .line 160
    const/4 v3, 0x1

    iput-boolean v3, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 162
    :cond_0
    iget-boolean v3, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 164
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely()Z

    .line 166
    iput-boolean v3, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 157
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 168
    :cond_1
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 170
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 173
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    .line 174
    return v2

    .line 170
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0
.end method

.method private a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 466
    const/4 v0, 0x1

    new-array v2, v0, [Ljava/lang/String;

    const-string v0, "_id"

    aput-object v0, v2, v1

    const/4 v5, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/e/a;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 469
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 470
    const/4 v0, 0x0

    invoke-interface {v1, v0}, Landroid/database/Cursor;->getLong(I)J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-wide v2

    .line 475
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    return-wide v2

    .line 472
    :cond_0
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not find internal ID for URI "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 475
    :catchall_0
    move-exception v0

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method private a(Ljava/util/ArrayList;)Landroid/net/Uri;
    .locals 6

    .prologue
    .line 349
    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-nez v0, :cond_0

    .line 350
    const/4 v0, 0x0

    .line 367
    :goto_0
    return-object v0

    .line 354
    :cond_0
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    move-result-object v1

    .line 355
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/e/a;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v3

    .line 360
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v2, "user"

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 361
    const/4 v0, 0x1

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_1
    if-ge v2, v4, :cond_1

    .line 362
    invoke-virtual {p1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->getUri()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/e/a;->b(Landroid/net/Uri;)Ljava/lang/String;

    move-result-object v0

    .line 363
    invoke-static {v3, v0}, Lcom/google/k/a/ad;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    const-string v5, "URIs in batch operation refer to different databases"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 361
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_1
    move-object v0, v1

    .line 367
    goto :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 10

    .prologue
    const/4 v2, 0x0

    .line 312
    .line 314
    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 316
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 317
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 318
    new-array v6, v5, [Landroid/content/ContentProviderResult;

    move v4, v2

    move v1, v2

    move v3, v2

    .line 319
    :goto_0
    if-ge v4, v5, :cond_2

    .line 320
    add-int/lit8 v1, v1, 0x1

    const/16 v0, 0x1f4

    if-le v1, v0, :cond_0

    .line 321
    new-instance v0, Landroid/content/OperationApplicationException;

    const-string v1, "Too many content provider operations between yield points. The maximum number of operations per yield point is 500"

    invoke-direct {v0, v1, v3}, Landroid/content/OperationApplicationException;-><init>(Ljava/lang/String;I)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 342
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 343
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    throw v0

    .line 326
    :cond_0
    :try_start_1
    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderOperation;

    .line 327
    if-lez v4, :cond_1

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation;->isYieldAllowed()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 329
    iget-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 330
    const-wide/16 v8, 0xfa0

    invoke-virtual {p1, v8, v9}, Landroid/database/sqlite/SQLiteDatabase;->yieldIfContendedSafely(J)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 331
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/e/a;->a(Ljava/util/ArrayList;)Landroid/net/Uri;

    move-result-object v7

    invoke-virtual {p0, v7}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v7

    invoke-virtual {v7}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object p1

    .line 332
    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 333
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v2

    .line 337
    :cond_1
    :goto_1
    invoke-virtual {v0, p0, v6, v4}, Landroid/content/ContentProviderOperation;->apply(Landroid/content/ContentProvider;[Landroid/content/ContentProviderResult;I)Landroid/content/ContentProviderResult;

    move-result-object v0

    aput-object v0, v6, v4

    .line 319
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 339
    :cond_2
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 342
    iget-object v0, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/ThreadLocal;->set(Ljava/lang/Object;)V

    .line 343
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 344
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    return-object v6

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 197
    invoke-direct {p0}, Lcom/google/android/gms/common/e/a;->b()Z

    move-result v0

    .line 199
    if-nez v0, :cond_2

    .line 200
    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 202
    :try_start_0
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 203
    if-lez v0, :cond_0

    .line 204
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 206
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 208
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    .line 219
    :cond_1
    :goto_0
    return v0

    .line 208
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 213
    :cond_2
    invoke-virtual/range {p0 .. p5}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 214
    if-lez v0, :cond_1

    .line 215
    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    goto :goto_0
.end method

.method protected static b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)I
    .locals 4

    .prologue
    .line 250
    invoke-virtual {p1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v0

    .line 251
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 252
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "=?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v0, v2, v3

    invoke-virtual {p0, p2, p4, v1, v2}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    return v0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 264
    invoke-direct {p0}, Lcom/google/android/gms/common/e/a;->b()Z

    move-result v0

    .line 266
    if-nez v0, :cond_2

    .line 267
    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 269
    :try_start_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 270
    if-lez v0, :cond_0

    .line 271
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 273
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 278
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    .line 285
    :cond_1
    :goto_0
    return v0

    .line 275
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 280
    :cond_2
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 281
    if-lez v0, :cond_1

    .line 282
    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    goto :goto_0
.end method

.method private b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 121
    invoke-direct {p0}, Lcom/google/android/gms/common/e/a;->b()Z

    move-result v0

    .line 123
    if-nez v0, :cond_2

    .line 124
    invoke-virtual {p1, p0}, Landroid/database/sqlite/SQLiteDatabase;->beginTransactionWithListener(Landroid/database/sqlite/SQLiteTransactionListener;)V

    .line 126
    :try_start_0
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 127
    if-eqz v0, :cond_0

    .line 128
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 130
    :cond_0
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 132
    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->c()V

    .line 142
    :cond_1
    :goto_0
    return-object v0

    .line 132
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    throw v0

    .line 137
    :cond_2
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    .line 138
    if-eqz v0, :cond_1

    .line 139
    iput-boolean v1, p0, Lcom/google/android/gms/common/e/a;->a:Z

    goto :goto_0
.end method

.method private b()Z
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/e/a;->b:Ljava/lang/ThreadLocal;

    invoke-virtual {v0}, Ljava/lang/ThreadLocal;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected abstract a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
.end method

.method protected abstract a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
.end method

.method protected abstract a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
.end method

.method protected abstract a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;
.end method

.method protected abstract a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 417
    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    move-object v7, v5

    move-object v8, v5

    invoke-virtual/range {v0 .. v8}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)Landroid/net/Uri;
    .locals 6

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x1

    .line 441
    invoke-virtual {p0, p3, p6, p7, p8}, Lcom/google/android/gms/common/e/a;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 442
    if-ne v1, v2, :cond_1

    .line 443
    invoke-direct {p0, p3, p7, p8}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)J

    move-result-wide v0

    invoke-static {p2, v0, v1}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 455
    :cond_0
    :goto_0
    return-object v0

    .line 445
    :cond_1
    if-nez v1, :cond_2

    .line 447
    invoke-virtual {p1, p4, v0, p6}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v2

    .line 448
    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-ltz v1, :cond_0

    .line 449
    invoke-static {p2, v2, v3}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0

    .line 451
    :cond_2
    if-le v1, v2, :cond_0

    .line 453
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Upsert affected multiple rows"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected abstract a()V
.end method

.method public final applyBatch(Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;
    .locals 2

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Ljava/util/ArrayList;)Landroid/net/Uri;

    move-result-object v0

    .line 300
    if-nez v0, :cond_0

    .line 301
    const/4 v0, 0x0

    new-array v0, v0, [Landroid/content/ContentProviderResult;

    .line 306
    :goto_0
    return-object v0

    .line 304
    :cond_0
    sget-object v1, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 305
    :try_start_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 306
    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Ljava/util/ArrayList;)[Landroid/content/ContentProviderResult;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected abstract b(Landroid/net/Uri;)Ljava/lang/String;
.end method

.method public final bulkInsert(Landroid/net/Uri;[Landroid/content/ContentValues;)I
    .locals 2

    .prologue
    .line 147
    sget-object v1, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 148
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 149
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Landroid/content/ContentValues;)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 150
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/google/android/gms/common/e/a;->a:Z

    if-eqz v0, :cond_0

    .line 393
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/e/a;->a:Z

    .line 394
    invoke-virtual {p0}, Lcom/google/android/gms/common/e/a;->a()V

    .line 396
    :cond_0
    return-void
.end method

.method public final delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 257
    sget-object v1, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 258
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 259
    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/common/e/a;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 260
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 114
    sget-object v1, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 115
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 116
    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/common/e/a;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public onBegin()V
    .locals 0

    .prologue
    .line 372
    return-void
.end method

.method public onCommit()V
    .locals 0

    .prologue
    .line 377
    return-void
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public onRollback()V
    .locals 0

    .prologue
    .line 383
    return-void
.end method

.method public final openAssetFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;
    .locals 2

    .prologue
    .line 291
    sget-object v1, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v1

    .line 292
    :try_start_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/res/AssetFileDescriptor;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 8

    .prologue
    .line 180
    sget-object v7, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v7

    .line 181
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 182
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/common/e/a;->a(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 183
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0
.end method

.method public final update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 7

    .prologue
    .line 189
    sget-object v6, Lcom/google/android/gms/common/e/a;->c:Ljava/lang/Object;

    monitor-enter v6

    .line 190
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/e/a;->a(Landroid/net/Uri;)Landroid/database/sqlite/SQLiteOpenHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteOpenHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    .line 191
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/e/a;->b(Landroid/database/sqlite/SQLiteDatabase;Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 192
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method
