.class public final Lcom/google/android/gms/icing/impl/q;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/bk;
.implements Lcom/google/android/gms/icing/impl/n;


# static fields
.field private static final b:I


# instance fields
.field final a:Ljava/lang/Object;

.field private final c:Landroid/content/Context;

.field private final d:Ljava/lang/String;

.field private final e:Landroid/content/SharedPreferences;

.field private f:Lcom/google/android/gms/icing/impl/s;

.field private g:Lcom/google/android/gms/icing/impl/s;

.field private h:Ljava/util/Map;

.field private i:Ljava/util/Map;

.field private j:Ljava/util/Map;

.field private k:Ljava/util/Map;

.field private l:Ljava/util/Map;

.field private m:Ljava/util/Map;

.field private n:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 66
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x4

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/gms/icing/impl/q;->b:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x7

    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 132
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    .line 135
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/q;->c:Landroid/content/Context;

    .line 136
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/q;->d:Ljava/lang/String;

    .line 137
    const-string v0, "-icing-settings"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    const-string v0, "settings-version"

    const/4 v1, 0x0

    invoke-interface {v3, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v6, :cond_0

    :goto_0
    iput-object v3, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    .line 138
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->D()V

    .line 139
    return-void

    .line 137
    :cond_0
    new-instance v0, Lcom/google/android/gms/icing/impl/bj;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->c:Landroid/content/Context;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/q;->d:Ljava/lang/String;

    move-object v2, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/icing/impl/bj;-><init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/bk;Landroid/content/SharedPreferences;ILjava/lang/String;)V

    iget v1, v0, Lcom/google/android/gms/icing/impl/bj;->c:I

    packed-switch v1, :pswitch_data_0

    :goto_1
    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "settings-version"

    invoke-interface {v0, v1, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    :pswitch_0
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bj;->a()V

    :pswitch_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bj;->b()V

    :pswitch_2
    iget-object v1, v0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "last-maintenance"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v1, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :pswitch_3
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bj;->c()V

    :pswitch_4
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bj;->d()V

    :pswitch_5
    new-instance v1, Lcom/google/r/a/a/b;

    invoke-direct {v1}, Lcom/google/r/a/a/b;-><init>()V

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    const-string v4, "experiment-config-key"

    const/4 v5, 0x0

    invoke-interface {v2, v4, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2, v1}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/r/a/a/b;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/p;->a(Lcom/google/r/a/a/b;)Lcom/google/r/a/a/b;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v4, "experiment-config-key"

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v2, v4, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :pswitch_6
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/bj;->e()V

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method private D()V
    .locals 3

    .prologue
    .line 150
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    .line 151
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 152
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->i:Ljava/util/Map;

    .line 153
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->j:Ljava/util/Map;

    .line 154
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    .line 155
    sget v0, Lcom/google/android/gms/icing/impl/q;->b:I

    iput v0, p0, Lcom/google/android/gms/icing/impl/q;->n:I

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "experiment-config-key"

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lcom/google/android/gms/icing/impl/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "pending-experiment-config-key"

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences;Ljava/lang/String;)Lcom/google/android/gms/icing/impl/s;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->F()V

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 157
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->E()V

    .line 158
    return-void

    .line 151
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 156
    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private E()V
    .locals 6

    .prologue
    .line 192
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 193
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v1, "app-params"

    const/4 v3, 0x0

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 195
    if-nez v0, :cond_0

    monitor-exit v2

    .line 211
    :goto_0
    return-void

    .line 196
    :cond_0
    new-instance v1, Lcom/google/android/gms/icing/c/a/f;

    invoke-direct {v1}, Lcom/google/android/gms/icing/c/a/f;-><init>()V

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/f;

    .line 200
    iget v1, v0, Lcom/google/android/gms/icing/c/a/f;->b:I

    if-nez v1, :cond_2

    sget v1, Lcom/google/android/gms/icing/impl/q;->b:I

    :goto_1
    iput v1, p0, Lcom/google/android/gms/icing/impl/q;->n:I

    .line 203
    iget-object v1, v0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v1, v1

    if-nez v1, :cond_3

    .line 204
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    .line 211
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 200
    :cond_2
    :try_start_1
    iget v1, v0, Lcom/google/android/gms/icing/c/a/f;->b:I

    goto :goto_1

    .line 206
    :cond_3
    new-instance v1, Ljava/util/HashMap;

    iget-object v3, v0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v3, v3

    invoke-direct {v1, v3}, Ljava/util/HashMap;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    .line 207
    const/4 v1, 0x0

    :goto_2
    iget-object v3, v0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    array-length v3, v3

    if-ge v1, v3, :cond_1

    .line 208
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    iget-object v4, v0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    aget-object v4, v4, v1

    iget-object v4, v4, Lcom/google/android/gms/icing/c/a/g;->a:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/icing/c/a/f;->a:[Lcom/google/android/gms/icing/c/a/g;

    aget-object v5, v5, v1

    invoke-interface {v3, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 207
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method private F()V
    .locals 5

    .prologue
    const/16 v4, 0x13f

    const/16 v3, 0x134

    const/16 v2, 0xed

    const/16 v1, 0xeb

    .line 226
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->u(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->i:Ljava/util/Map;

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->w(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->j:Ljava/util/Map;

    .line 228
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/icing/impl/s;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->v(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->k:Ljava/util/Map;

    .line 229
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/s;->f(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->x(Ljava/lang/String;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->l:Ljava/util/Map;

    .line 230
    :goto_1
    return-void

    .line 228
    :cond_2
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->k:Ljava/util/Map;

    goto :goto_0

    .line 229
    :cond_3
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->l:Ljava/util/Map;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/icing/impl/q;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    return-object v0
.end method

.method static a(Landroid/content/SharedPreferences;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 179
    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    .line 180
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 181
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 182
    const-string v3, "pkg-info-"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 183
    const/16 v3, 0x9

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 184
    new-instance v3, Lcom/google/android/gms/icing/aw;

    invoke-direct {v3}, Lcom/google/android/gms/icing/aw;-><init>()V

    .line 185
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    .line 186
    invoke-interface {p1, v1, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 189
    :cond_1
    return-void
.end method

.method private a(Lcom/google/r/a/a/c;Lcom/google/android/gms/icing/impl/s;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 763
    iget v1, p1, Lcom/google/r/a/a/c;->b:I

    .line 764
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/p;->d(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 766
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/p;->a(Ljava/lang/Integer;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 768
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 769
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/icing/impl/s;->a(I)Lcom/google/r/a/a/c;

    move-result-object v2

    iget v3, v2, Lcom/google/r/a/a/c;->b:I

    iget v4, p1, Lcom/google/r/a/a/c;->b:I

    if-ne v3, v4, :cond_0

    iget-object v3, v2, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/r/a/a/c;->d:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-boolean v3, v2, Lcom/google/r/a/a/c;->c:Z

    iget-boolean v4, p1, Lcom/google/r/a/a/c;->c:Z

    if-ne v3, v4, :cond_0

    iget v2, v2, Lcom/google/r/a/a/c;->e:I

    iget v3, p1, Lcom/google/r/a/a/c;->e:I

    if-ne v2, v3, :cond_0

    const/4 v0, 0x1

    :cond_0
    if-eqz v0, :cond_2

    .line 771
    invoke-virtual {p2, v1, p1}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    .line 772
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->delete(I)V

    .line 789
    :cond_1
    :goto_0
    return-void

    .line 776
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    .line 778
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->a(I)Lcom/google/r/a/a/c;

    move-result-object v0

    invoke-virtual {p2, v1, v0}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    goto :goto_0

    .line 782
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    goto :goto_0

    .line 786
    :cond_4
    invoke-virtual {p2, v1, p1}, Lcom/google/android/gms/icing/impl/s;->a(ILcom/google/r/a/a/c;)V

    goto :goto_0
.end method

.method private a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 645
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 646
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 647
    iget v2, v0, Lcom/google/android/gms/icing/aw;->h:I

    if-eq p2, v2, :cond_0

    .line 648
    iput p2, v0, Lcom/google/android/gms/icing/aw;->h:I

    .line 649
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 651
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V
    .locals 2

    .prologue
    .line 428
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    move-result v1

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    if-nez v1, :cond_0

    .line 429
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 431
    :cond_0
    return-void
.end method

.method static a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 463
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "pkg-info-"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 464
    iget-boolean v2, p2, Lcom/google/android/gms/icing/aw;->b:Z

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-nez v2, :cond_0

    iget v2, p2, Lcom/google/android/gms/icing/aw;->d:I

    if-nez v2, :cond_0

    iget-object v2, p2, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p2, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p2, Lcom/google/android/gms/icing/aw;->f:Z

    if-nez v2, :cond_0

    iget v2, p2, Lcom/google/android/gms/icing/aw;->h:I

    if-nez v2, :cond_0

    iget v2, p2, Lcom/google/android/gms/icing/aw;->i:I

    if-nez v2, :cond_0

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    .line 466
    invoke-interface {p0, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 470
    :goto_1
    return v0

    :cond_0
    move v2, v0

    .line 464
    goto :goto_0

    .line 469
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p0, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move v0, v1

    .line 470
    goto :goto_1
.end method

.method private c(Ljava/lang/String;Z)V
    .locals 3

    .prologue
    .line 634
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 635
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 636
    iget-boolean v2, v0, Lcom/google/android/gms/icing/aw;->f:Z

    if-eq p2, v2, :cond_0

    .line 638
    iput-boolean p2, v0, Lcom/google/android/gms/icing/aw;->f:Z

    .line 639
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 641
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;
    .locals 1

    .prologue
    .line 415
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    return-object v0
.end method

.method private t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;
    .locals 2

    .prologue
    .line 419
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    .line 420
    if-nez v0, :cond_0

    .line 421
    new-instance v0, Lcom/google/android/gms/icing/aw;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aw;-><init>()V

    .line 422
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 424
    :cond_0
    return-object v0
.end method

.method private static u(Ljava/lang/String;)Ljava/util/Map;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 1360
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1361
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1362
    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_0

    .line 1363
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1364
    const-string v4, "package"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1365
    const-string v5, "corpus"

    invoke-virtual {v3, v5}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1366
    const-string v6, "section"

    invoke-virtual {v3, v6}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1367
    const-string v7, "weight"

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v3

    .line 1368
    new-instance v7, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v7, v4, v5}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    new-instance v4, Landroid/support/v4/g/o;

    invoke-direct {v4, v7, v6}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v4, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1362
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1373
    :catch_0
    move-exception v0

    const-string v0, "Received bad json for section weights override -- ignoring."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1374
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :cond_0
    return-object v0
.end method

.method private static v(Ljava/lang/String;)Ljava/util/Map;
    .locals 14

    .prologue
    const/4 v2, 0x0

    .line 1399
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1400
    new-instance v4, Lorg/json/JSONArray;

    invoke-direct {v4, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    move v3, v2

    .line 1401
    :goto_0
    invoke-virtual {v4}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-ge v3, v1, :cond_1

    .line 1402
    invoke-virtual {v4, v3}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v1

    .line 1403
    const-string v5, "p"

    const-string v6, ""

    invoke-virtual {v1, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1405
    const-string v6, "c"

    const-string v7, ""

    invoke-virtual {v1, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1408
    new-instance v7, Lcom/google/android/gms/icing/aa;

    invoke-direct {v7}, Lcom/google/android/gms/icing/aa;-><init>()V

    .line 1409
    const-string v8, "ht"

    sget-object v9, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    iget v9, v9, Lcom/google/android/gms/icing/aa;->a:I

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/google/android/gms/icing/aa;->a:I

    .line 1411
    const-string v8, "gum"

    sget-object v9, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    iget v9, v9, Lcom/google/android/gms/icing/aa;->b:I

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/google/android/gms/icing/aa;->b:I

    .line 1414
    const-string v8, "sum"

    sget-object v9, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    iget v9, v9, Lcom/google/android/gms/icing/aa;->c:I

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/google/android/gms/icing/aa;->c:I

    .line 1417
    const-string v8, "gsum"

    sget-object v9, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    iget v9, v9, Lcom/google/android/gms/icing/aa;->d:I

    invoke-virtual {v1, v8, v9}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v8

    iput v8, v7, Lcom/google/android/gms/icing/aa;->d:I

    .line 1421
    new-instance v8, Lcom/google/android/gms/icing/impl/t;

    invoke-direct {v8, v7}, Lcom/google/android/gms/icing/impl/t;-><init>(Lcom/google/android/gms/icing/aa;)V

    .line 1424
    const-string v7, "s"

    invoke-virtual {v1, v7}, Lorg/json/JSONObject;->optJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 1425
    if-eqz v7, :cond_0

    move v1, v2

    .line 1426
    :goto_1
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v9

    if-ge v1, v9, :cond_0

    .line 1427
    invoke-virtual {v7, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v9

    .line 1428
    new-instance v10, Lcom/google/android/gms/icing/ab;

    invoke-direct {v10}, Lcom/google/android/gms/icing/ab;-><init>()V

    .line 1430
    const-string v11, "n"

    const-string v12, ""

    invoke-virtual {v9, v11, v12}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 1432
    const-string v12, "u"

    sget-object v13, Lcom/google/android/gms/icing/impl/p;->b:Lcom/google/android/gms/icing/ab;

    iget v13, v13, Lcom/google/android/gms/icing/ab;->b:I

    invoke-virtual {v9, v12, v13}, Lorg/json/JSONObject;->optInt(Ljava/lang/String;I)I

    move-result v9

    iput v9, v10, Lcom/google/android/gms/icing/ab;->b:I

    .line 1435
    iget-object v9, v8, Lcom/google/android/gms/icing/impl/t;->a:Ljava/util/Map;

    invoke-interface {v9, v11, v10}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1426
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1439
    :cond_0
    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v1, v5, v6}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1440
    invoke-interface {v0, v1, v8}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1401
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto/16 :goto_0

    .line 1444
    :catch_0
    move-exception v0

    const-string v0, "Received bad json for corpus scoring config -- ignoring."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1445
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :cond_1
    return-object v0
.end method

.method private static w(Ljava/lang/String;)Ljava/util/Map;
    .locals 6

    .prologue
    const/4 v5, -0x1

    .line 1453
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1454
    const/4 v0, 0x0

    .line 1455
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 1456
    const/16 v2, 0x3a

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->indexOf(II)I

    move-result v2

    .line 1457
    if-ne v2, v5, :cond_0

    .line 1458
    const-string v0, "Cannot find pkgname end"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    move-object v0, v1

    .line 1475
    :goto_1
    return-object v0

    .line 1461
    :cond_0
    invoke-virtual {p0, v0, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 1462
    const/16 v0, 0x2c

    add-int/lit8 v4, v2, 0x1

    invoke-virtual {p0, v0, v4}, Ljava/lang/String;->indexOf(II)I

    move-result v0

    .line 1463
    if-ne v0, v5, :cond_1

    .line 1465
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 1468
    :cond_1
    add-int/lit8 v4, v2, 0x1

    :try_start_0
    invoke-virtual {p0, v4, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1469
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v1, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1473
    :goto_2
    add-int/lit8 v0, v0, 0x1

    .line 1474
    goto :goto_0

    .line 1471
    :catch_0
    move-exception v3

    const-string v3, "Bad flags %s"

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2, v0}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_2

    :cond_2
    move-object v0, v1

    .line 1475
    goto :goto_1
.end method

.method private static x(Ljava/lang/String;)Ljava/util/Map;
    .locals 12

    .prologue
    .line 1490
    :try_start_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 1491
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, p0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 1493
    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v1, v3, :cond_1

    .line 1494
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v3

    .line 1495
    const-string v4, "p"

    const-string v5, ""

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 1496
    const-string v5, "c"

    const-string v6, ""

    invoke-virtual {v3, v5, v6}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 1497
    const-string v6, "ctx"

    const-string v7, ""

    invoke-virtual {v3, v6, v7}, Lorg/json/JSONObject;->optString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    .line 1498
    const-string v7, "w"

    invoke-virtual {v3, v7}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v8

    .line 1499
    const-wide/16 v10, 0x0

    cmpg-double v3, v8, v10

    if-lez v3, :cond_0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    cmpl-double v3, v8, v10

    if-lez v3, :cond_2

    .line 1500
    :cond_0
    new-instance v0, Lorg/json/JSONException;

    const-string v1, "Corpus weight demotion must be in range (0, 1]."

    invoke-direct {v0, v1}, Lorg/json/JSONException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1510
    :catch_0
    move-exception v0

    const-string v0, "Received bad json for corpus context scoring override -- ignoring."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->e(Ljava/lang/String;)I

    .line 1511
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    :cond_1
    return-object v0

    .line 1502
    :cond_2
    :try_start_1
    new-instance v3, Lcom/google/android/gms/appdatasearch/CorpusId;

    invoke-direct {v3, v4, v5}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1503
    new-instance v4, Lcom/google/android/gms/icing/impl/o;

    invoke-direct {v4, v8, v9}, Lcom/google/android/gms/icing/impl/o;-><init>(D)V

    .line 1505
    new-instance v5, Landroid/support/v4/g/o;

    invoke-direct {v5, v3, v6}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v5, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    .line 1493
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method


# virtual methods
.method public final A()I
    .locals 3

    .prologue
    const/16 v2, 0x158

    .line 1276
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/p;->a(I)I

    move-result v0

    .line 1278
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1279
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v0

    .line 1281
    :cond_0
    return v0
.end method

.method public final B()I
    .locals 2

    .prologue
    const/16 v1, 0xec

    .line 1326
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1329
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v0

    .line 1332
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final C()I
    .locals 2

    .prologue
    const/16 v1, 0x115

    .line 1336
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1338
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v0

    .line 1341
    :goto_0
    return v0

    :cond_0
    sget v0, Lcom/google/android/gms/icing/impl/p;->d:I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;I)I
    .locals 2

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->i:Ljava/util/Map;

    new-instance v1, Landroid/support/v4/g/o;

    invoke-direct {v1, p1, p2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 1208
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result p3

    :cond_0
    return p3
.end method

.method public final a(Ljava/lang/String;)Landroid/content/SharedPreferences;
    .locals 3

    .prologue
    .line 235
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->c:Landroid/content/Context;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->d:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;
    .locals 5

    .prologue
    .line 904
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 907
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->k:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/t;

    .line 909
    if-eqz v0, :cond_0

    .line 910
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/t;->a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;

    move-result-object v0

    monitor-exit v1

    .line 929
    :goto_0
    return-object v0

    .line 913
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->k:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/t;

    .line 915
    if-eqz v0, :cond_1

    .line 916
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/t;->a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 930
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 919
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->k:Ljava/util/Map;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/t;

    .line 922
    if-eqz v0, :cond_2

    .line 923
    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/t;->a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;

    move-result-object v0

    monitor-exit v1

    goto :goto_0

    .line 926
    :cond_2
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/a/j;->e(Lcom/google/android/gms/icing/g;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 927
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->c:Lcom/google/android/gms/icing/aa;

    monitor-exit v1

    goto :goto_0

    .line 929
    :cond_3
    sget-object v0, Lcom/google/android/gms/icing/impl/p;->a:Lcom/google/android/gms/icing/aa;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;)Lcom/google/android/gms/icing/impl/o;
    .locals 5

    .prologue
    .line 1233
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1235
    new-instance v1, Landroid/support/v4/g/o;

    invoke-direct {v1, p1, p2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1237
    new-instance v1, Landroid/support/v4/g/o;

    const-string v2, ""

    invoke-direct {v1, p1, v2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1239
    new-instance v1, Landroid/support/v4/g/o;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, p2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1242
    new-instance v1, Landroid/support/v4/g/o;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1245
    new-instance v1, Landroid/support/v4/g/o;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {v1, v2, p2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1248
    new-instance v1, Landroid/support/v4/g/o;

    new-instance v2, Lcom/google/android/gms/appdatasearch/CorpusId;

    const-string v3, ""

    const-string v4, ""

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    const-string v3, ""

    invoke-direct {v1, v2, v3}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1251
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1252
    :try_start_0
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/g/o;

    .line 1253
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->l:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/o;

    .line 1255
    if-eqz v0, :cond_0

    .line 1256
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1260
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 1261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/util/List;
    .locals 6

    .prologue
    .line 1213
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1214
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1215
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/g/o;

    .line 1216
    iget-object v2, v1, Landroid/support/v4/g/o;->b:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-virtual {v2, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-nez v0, :cond_0

    .line 1217
    invoke-interface {v3, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 1220
    :cond_1
    return-object v3
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 883
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "extension-delete"

    invoke-interface {v0, v1, p1, p2}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 884
    return-void
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;IJ)V
    .locals 5

    .prologue
    .line 558
    invoke-virtual {p1}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v0

    .line 559
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 560
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v2

    .line 561
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Lcom/google/android/gms/icing/t;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    .line 562
    iput p2, v2, Lcom/google/android/gms/icing/aw;->d:I

    .line 563
    iput-wide p3, v2, Lcom/google/android/gms/icing/aw;->g:J

    .line 564
    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 565
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/icing/b;)V
    .locals 3

    .prologue
    .line 875
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "extension-info"

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 876
    return-void
.end method

.method public final a(Lcom/google/android/gms/icing/c/a/f;)V
    .locals 6

    .prologue
    .line 319
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 320
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-params"

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-params-last-update"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-params-last-update-sched"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 325
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->E()V

    .line 326
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/icing/e;)V
    .locals 4

    .prologue
    .line 373
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 374
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-history-upload-status"

    invoke-static {p1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 377
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 800
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 801
    :try_start_0
    const-string v0, "Current:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 802
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const-string v2, "    "

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/icing/impl/s;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 803
    new-instance v0, Lcom/google/android/gms/icing/impl/s;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-direct {v0, v2}, Lcom/google/android/gms/icing/impl/s;-><init>(Lcom/google/android/gms/icing/impl/s;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Lcom/google/android/gms/icing/impl/s;)Z

    move-result v0

    .line 805
    if-eqz v0, :cond_0

    .line 806
    const-string v0, "Pending:\n"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 807
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    const-string v2, "    "

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/icing/impl/s;->a(Ljava/io/PrintWriter;Ljava/lang/String;)V

    .line 809
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;ILjava/lang/String;)V
    .locals 2

    .prologue
    .line 679
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 680
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 681
    iput p2, v0, Lcom/google/android/gms/icing/aw;->i:I

    .line 682
    iput-object p3, v0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    .line 683
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 684
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 486
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 487
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 488
    if-nez p2, :cond_0

    .line 489
    const-string v2, ""

    iput-object v2, v0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    .line 493
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 494
    monitor-exit v1

    return-void

    .line 491
    :cond_0
    iput-object p2, v0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 494
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 549
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 550
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 551
    iput-boolean p2, v0, Lcom/google/android/gms/icing/aw;->b:Z

    .line 552
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 553
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 266
    if-eqz p1, :cond_0

    .line 267
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 268
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 270
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const-string v3, "experiment-config-key"

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 271
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    const-string v3, "pending-experiment-config-key"

    iget-object v4, v2, Lcom/google/android/gms/icing/impl/s;->b:Landroid/util/SparseArray;

    invoke-virtual {v4}, Landroid/util/SparseArray;->clear()V

    iget-object v4, v2, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->clear()V

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 272
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 273
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    :cond_0
    return-void

    .line 273
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a()Z
    .locals 2

    .prologue
    .line 1225
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1226
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->l:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 1227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/r/a/a/b;)Z
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 712
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v3

    .line 713
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/icing/impl/p;->a()Landroid/util/SparseArray;

    move-result-object v4

    .line 716
    new-instance v5, Lcom/google/android/gms/icing/impl/s;

    invoke-direct {v5}, Lcom/google/android/gms/icing/impl/s;-><init>()V

    .line 718
    new-instance v0, Lcom/google/android/gms/icing/impl/s;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/s;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    .line 719
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/p;->a(Lcom/google/r/a/a/b;)Lcom/google/r/a/a/b;

    move-result-object v6

    move v2, v1

    .line 722
    :goto_0
    invoke-virtual {v4}, Landroid/util/SparseArray;->size()I

    move-result v0

    if-ge v2, v0, :cond_0

    .line 723
    invoke-virtual {v4, v2}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v0

    .line 724
    invoke-virtual {v4, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/r/a/a/c;

    .line 725
    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/r/a/a/c;Lcom/google/android/gms/icing/impl/s;)V

    .line 722
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 729
    :cond_0
    iget-object v2, v6, Lcom/google/r/a/a/b;->a:[Lcom/google/r/a/a/c;

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v7, v2, v0

    .line 730
    invoke-direct {p0, v7, v5}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/r/a/a/c;Lcom/google/android/gms/icing/impl/s;)V

    .line 729
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 733
    :cond_1
    iget-object v2, v6, Lcom/google/r/a/a/b;->c:[I

    array-length v4, v2

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_4

    aget v1, v2, v0

    .line 734
    const v6, 0x9a1d20

    if-lt v1, v6, :cond_2

    const v6, 0x9ba3bf

    if-ge v1, v6, :cond_2

    .line 736
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    iget-object v6, v6, Lcom/google/android/gms/icing/impl/s;->a:Ljava/util/Set;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 738
    invoke-virtual {v5, v1}, Lcom/google/android/gms/icing/impl/s;->c(I)V

    .line 733
    :cond_2
    :goto_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 740
    :cond_3
    iget-object v6, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v6, v1}, Lcom/google/android/gms/icing/impl/s;->c(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_3

    .line 754
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 745
    :cond_4
    :try_start_1
    new-instance v0, Lcom/google/android/gms/icing/impl/s;

    invoke-direct {v0, v5}, Lcom/google/android/gms/icing/impl/s;-><init>(Lcom/google/android/gms/icing/impl/s;)V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 748
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const-string v2, "experiment-config-key"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 749
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    const-string v2, "pending-experiment-config-key"

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    .line 751
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->F()V

    .line 753
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/icing/c/a/g;
    .locals 2

    .prologue
    .line 311
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 312
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->m:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/g;

    .line 313
    if-nez v0, :cond_0

    const/4 v0, 0x0

    monitor-exit v1

    .line 314
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/g;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 315
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "settings-version"

    const/4 v2, 0x7

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "last-maintenance"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 146
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->D()V

    .line 147
    return-void
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 520
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 521
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 522
    if-nez p2, :cond_0

    .line 523
    const-string v2, ""

    iput-object v2, v0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    .line 527
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 528
    monitor-exit v1

    return-void

    .line 525
    :cond_0
    iput-object p2, v0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;Z)V
    .locals 1

    .prologue
    .line 666
    if-eqz p2, :cond_0

    const/4 v0, 0x2

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;I)V

    .line 668
    return-void

    .line 666
    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 3

    .prologue
    .line 387
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 388
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-history-upload-pending"

    invoke-interface {v0, v2, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 391
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c()I
    .locals 3

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v1, "index-version"

    const/4 v2, -0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 475
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 476
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 477
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 478
    iget-object v0, v0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 480
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 482
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 247
    const-string v0, "index-version"

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const/16 v2, 0x35

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 248
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 498
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 499
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 500
    if-eqz v0, :cond_0

    .line 501
    const-string v2, ""

    iput-object v2, v0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    .line 502
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 504
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 508
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 509
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 510
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 511
    iget-object v0, v0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 513
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 515
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final e()Z
    .locals 6

    .prologue
    .line 254
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 257
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "last-maintenance"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->a(Lcom/google/android/gms/icing/impl/s;)Z

    move-result v0

    .line 260
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/q;->F()V

    .line 261
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 262
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()J
    .locals 4

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v1, "last-maintenance"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    return-wide v0
.end method

.method public final f(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 532
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 533
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->t(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 535
    const-string v2, ""

    iput-object v2, v0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    .line 536
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 538
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g()Lcom/google/android/gms/icing/impl/r;
    .locals 2

    .prologue
    .line 305
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 306
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/r;-><init>(Lcom/google/android/gms/icing/impl/q;)V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 307
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 542
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 543
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 544
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/gms/icing/aw;->b:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 545
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()V
    .locals 6

    .prologue
    .line 330
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 331
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "app-params-last-update-sched"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 334
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 575
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 576
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 577
    if-eqz v0, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 578
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 3

    .prologue
    .line 582
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 583
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 584
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-eqz v2, :cond_0

    .line 585
    iget-object v0, v0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/android/gms/icing/t;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 587
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 589
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()Z
    .locals 3

    .prologue
    .line 338
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 339
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "app-params-last-update-sched"

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 340
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j()J
    .locals 6

    .prologue
    .line 344
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 345
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "app-params-last-update"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 346
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final j(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 593
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 594
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 595
    if-eqz v0, :cond_0

    .line 596
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    .line 597
    const/4 v2, 0x0

    iput v2, v0, Lcom/google/android/gms/icing/aw;->d:I

    .line 598
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/google/android/gms/icing/aw;->g:J

    .line 599
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;Lcom/google/android/gms/icing/aw;)V

    .line 601
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k()I
    .locals 2

    .prologue
    .line 350
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 351
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/icing/impl/q;->n:I

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 352
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final k(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 605
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 606
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 607
    if-eqz v0, :cond_0

    iget v0, v0, Lcom/google/android/gms/icing/aw;->d:I

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 608
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final l(Ljava/lang/String;)J
    .locals 3

    .prologue
    .line 612
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 613
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 614
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/google/android/gms/icing/aw;->g:J

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0

    .line 615
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final l()Z
    .locals 6

    .prologue
    .line 356
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 357
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "app-params-last-update"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 358
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v4, "app-params-last-update-sched"

    invoke-interface {v0, v4}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    .line 359
    if-nez v0, :cond_0

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    iget v4, p0, Lcom/google/android/gms/icing/impl/q;->n:I

    int-to-long v4, v4

    invoke-virtual {v0, v4, v5}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v4

    add-long/2addr v2, v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    cmp-long v0, v2, v4

    if-gez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 362
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final m()Lcom/google/android/gms/icing/e;
    .locals 4

    .prologue
    .line 366
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 367
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "app-history-upload-status"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/icing/e;

    invoke-direct {v2}, Lcom/google/android/gms/icing/e;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/e;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 369
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final m(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 619
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 620
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 621
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/gms/icing/aw;->f:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 622
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final n(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 626
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;Z)V

    .line 627
    return-void
.end method

.method public final n()Z
    .locals 4

    .prologue
    .line 381
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 382
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "app-history-upload-pending"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 395
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 396
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "current-os-build-id"

    sget-object v3, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 397
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final o(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 630
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->c(Ljava/lang/String;Z)V

    .line 631
    return-void
.end method

.method public final p(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 655
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 656
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    .line 657
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return v0

    :cond_0
    iget v0, v0, Lcom/google/android/gms/icing/aw;->h:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 658
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final p()Z
    .locals 4

    .prologue
    .line 401
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 402
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "current-os-build-id"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 403
    if-nez v0, :cond_0

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/q;->o()V

    .line 406
    const/4 v0, 0x1

    monitor-exit v1

    .line 408
    :goto_0
    return v0

    :cond_0
    sget-object v2, Landroid/os/Build;->ID:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 410
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final q()Ljava/util/Set;
    .locals 2

    .prologue
    .line 569
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 570
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final q(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 662
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Ljava/lang/String;I)V

    .line 663
    return-void
.end method

.method public final r(Ljava/lang/String;)Landroid/support/v4/g/o;
    .locals 4

    .prologue
    .line 671
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 672
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/icing/impl/q;->s(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v2

    .line 673
    if-eqz v2, :cond_0

    new-instance v0, Landroid/support/v4/g/o;

    iget v3, v2, Lcom/google/android/gms/icing/aw;->i:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    invoke-direct {v0, v3, v2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    new-instance v0, Landroid/support/v4/g/o;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const-string v3, ""

    invoke-direct {v0, v2, v3}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 674
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final r()V
    .locals 6

    .prologue
    .line 688
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 689
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 690
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .line 692
    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 693
    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 694
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    .line 695
    const-string v5, ""

    iput-object v5, v1, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    .line 696
    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/android/gms/icing/aw;->b:Z

    .line 697
    const/4 v5, 0x0

    iput-object v5, v1, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    .line 698
    const/4 v5, 0x0

    iput v5, v1, Lcom/google/android/gms/icing/aw;->d:I

    .line 699
    const/4 v5, 0x0

    iput-boolean v5, v1, Lcom/google/android/gms/icing/aw;->f:Z

    .line 700
    const/4 v5, 0x0

    iput v5, v1, Lcom/google/android/gms/icing/aw;->h:I

    .line 701
    const/4 v5, 0x0

    iput v5, v1, Lcom/google/android/gms/icing/aw;->i:I

    .line 702
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 703
    invoke-interface {v4}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 707
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 706
    :cond_1
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 707
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final s()[I
    .locals 1

    .prologue
    .line 792
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/s;->a()[I

    move-result-object v0

    return-object v0
.end method

.method public final t()[I
    .locals 1

    .prologue
    .line 796
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->g:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/s;->a()[I

    move-result-object v0

    return-object v0
.end method

.method public final u()Lcom/google/android/gms/icing/af;
    .locals 5

    .prologue
    .line 824
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 825
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/af;

    invoke-direct {v0}, Lcom/google/android/gms/icing/af;-><init>()V

    .line 826
    new-instance v2, Lcom/google/android/gms/icing/ac;

    invoke-direct {v2}, Lcom/google/android/gms/icing/ac;-><init>()V

    .line 827
    iput-object v2, v0, Lcom/google/android/gms/icing/af;->a:Lcom/google/android/gms/icing/ac;

    .line 831
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10a

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 833
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10a

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->a:I

    .line 836
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10b

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 838
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10b

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->b:I

    .line 841
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10c

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 843
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10c

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->c:I

    .line 846
    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10d

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 848
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10d

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->d:I

    .line 851
    :cond_3
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10e

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 853
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x10e

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->e:I

    .line 856
    :cond_4
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x116

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 858
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    const/16 v4, 0x116

    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/s;->e(I)I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/ac;->f:I

    .line 862
    :cond_5
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 863
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final v()Lcom/google/android/gms/icing/b;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 867
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    const-string v2, "extension-info"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 868
    if-nez v1, :cond_0

    .line 871
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/icing/b;

    invoke-direct {v0}, Lcom/google/android/gms/icing/b;-><init>()V

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/b;

    goto :goto_0
.end method

.method public final w()V
    .locals 2

    .prologue
    .line 879
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->e:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "extension-info"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 880
    return-void
.end method

.method public final x()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 1189
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    return-object v0
.end method

.method public final y()Z
    .locals 3

    .prologue
    const/16 v2, 0xea

    .line 1196
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/p;->b(I)Z

    move-result v0

    .line 1198
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1199
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->d(I)Z

    move-result v0

    .line 1201
    :cond_0
    return v0
.end method

.method public final z()Z
    .locals 3

    .prologue
    const/16 v2, 0x151

    .line 1266
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/p;->b(I)Z

    move-result v0

    .line 1268
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/icing/impl/s;->b(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1269
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/q;->f:Lcom/google/android/gms/icing/impl/s;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/icing/impl/s;->d(I)Z

    move-result v0

    .line 1271
    :cond_0
    return v0
.end method
