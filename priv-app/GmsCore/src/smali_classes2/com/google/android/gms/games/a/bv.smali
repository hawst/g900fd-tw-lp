.class public final Lcom/google/android/gms/games/a/bv;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/a/ax;


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:[Ljava/lang/String;

.field private static final c:[Ljava/lang/String;

.field private static final d:[Ljava/lang/String;


# instance fields
.field private final e:Lcom/google/android/gms/games/h/a/eh;

.field private final f:Ljava/util/Random;

.field private g:J

.field private h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 99
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/bv;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 136
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const-string v2, "request_sync_token"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/gms/games/a/bv;->b:[Ljava/lang/String;

    .line 146
    sget-object v0, Lcom/google/android/gms/games/internal/di;->a:[Ljava/lang/String;

    array-length v0, v0

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/games/a/bv;->c:[Ljava/lang/String;

    move v0, v1

    .line 147
    :goto_0
    sget-object v2, Lcom/google/android/gms/games/internal/di;->a:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 148
    sget-object v2, Lcom/google/android/gms/games/a/bv;->c:[Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "sender_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v4, Lcom/google/android/gms/games/internal/di;->a:[Ljava/lang/String;

    aget-object v4, v4, v0

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    .line 147
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 151
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/internal/cw;->a:[Ljava/lang/String;

    array-length v0, v0

    sget-object v2, Lcom/google/android/gms/games/a/bv;->c:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v0, v2

    sget-object v2, Lcom/google/android/gms/games/internal/dm;->a:[Ljava/lang/String;

    array-length v2, v2

    add-int/2addr v0, v2

    new-array v0, v0, [Ljava/lang/String;

    sput-object v0, Lcom/google/android/gms/games/a/bv;->d:[Ljava/lang/String;

    .line 153
    sget-object v0, Lcom/google/android/gms/games/internal/cw;->a:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/games/a/bv;->d:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/internal/cw;->a:[Ljava/lang/String;

    array-length v3, v3

    invoke-static {v0, v1, v2, v1, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 155
    sget-object v0, Lcom/google/android/gms/games/a/bv;->c:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/games/a/bv;->d:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/internal/cw;->a:[Ljava/lang/String;

    array-length v3, v3

    sget-object v4, Lcom/google/android/gms/games/internal/di;->a:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 157
    sget-object v0, Lcom/google/android/gms/games/internal/dm;->a:[Ljava/lang/String;

    sget-object v2, Lcom/google/android/gms/games/a/bv;->d:[Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/games/internal/cw;->a:[Ljava/lang/String;

    array-length v3, v3

    sget-object v4, Lcom/google/android/gms/games/internal/di;->a:[Ljava/lang/String;

    array-length v4, v4

    add-int/2addr v3, v4

    sget-object v4, Lcom/google/android/gms/games/internal/dm;->a:[Ljava/lang/String;

    array-length v4, v4

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 160
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;)V
    .locals 4

    .prologue
    .line 183
    const-string v0, "RequestAgent"

    sget-object v1, Lcom/google/android/gms/games/a/bv;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 175
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bv;->g:J

    .line 180
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bv;->h:Z

    .line 184
    new-instance v0, Lcom/google/android/gms/games/h/a/eh;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/eh;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bv;->e:Lcom/google/android/gms/games/h/a/eh;

    .line 185
    new-instance v0, Ljava/util/Random;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-direct {v0, v2, v3}, Ljava/util/Random;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bv;->f:Ljava/util/Random;

    .line 186
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;Ljava/util/ArrayList;)I
    .locals 12

    .prologue
    const/4 v1, -0x1

    .line 1106
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v4

    .line 1107
    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v2

    .line 1108
    iget-object v6, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1109
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v7

    .line 1110
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->c()Ljava/lang/String;

    move-result-object v0

    .line 1111
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->d()Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    .line 1114
    const/16 v9, 0x3e8

    if-ne v8, v9, :cond_0

    .line 1115
    invoke-static {v6, v0}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 1116
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v2

    invoke-virtual {v0, v2}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1173
    :goto_0
    return v1

    .line 1125
    :cond_0
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->getInboundRequestInfo()Lcom/google/android/gms/games/h/a/bq;

    move-result-object v8

    .line 1127
    if-eqz v8, :cond_3

    .line 1128
    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/bq;->getSenderPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v9

    .line 1129
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1130
    iget-object v9, v9, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-interface {v7}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v10

    invoke-static {v6, v9, v10, v11}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v7

    invoke-virtual {p3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1136
    :goto_1
    new-instance v7, Landroid/content/ContentValues;

    iget-object v9, p2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v7, v9}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 1137
    const-string v9, "external_game_id"

    invoke-virtual {v7, v9}, Landroid/content/ContentValues;->remove(Ljava/lang/String;)V

    .line 1138
    const-string v9, "game_id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v7, v9, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1142
    invoke-virtual {p3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 1143
    invoke-static {v6}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    .line 1148
    if-ne v0, v1, :cond_1

    .line 1149
    const-string v0, "sender_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v3, v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1153
    :goto_2
    const/4 v0, 0x1

    invoke-virtual {v3, v0}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    .line 1154
    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1158
    invoke-static {v6}, Lcom/google/android/gms/games/provider/av;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1159
    if-eqz v8, :cond_2

    .line 1160
    new-instance v1, Landroid/content/ContentValues;

    iget-object v3, v8, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 1161
    const-string v3, "player_id"

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1162
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v1, "request_id"

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :goto_3
    move v1, v2

    .line 1173
    goto/16 :goto_0

    .line 1151
    :cond_1
    const-string v1, "sender_id"

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    goto :goto_2

    .line 1167
    :cond_2
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->getOutboundRequestInfo()Lcom/google/android/gms/games/h/a/cn;

    move-result-object v1

    .line 1168
    const-string v3, "Request has no outbound or inbound info"

    invoke-static {v1, v3}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 1169
    invoke-static {p1, v0, v1, v2, p3}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;Lcom/google/android/gms/games/h/a/cn;ILjava/util/ArrayList;)V

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/games/a/au;II)Lcom/google/android/gms/common/data/DataHolder;
    .locals 16

    .prologue
    .line 409
    sget-object v2, Lcom/google/android/gms/games/a/bv;->d:[Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->a([Ljava/lang/String;)Lcom/google/android/gms/common/data/m;

    move-result-object v5

    .line 410
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 414
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/at;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    .line 415
    new-instance v3, Lcom/google/android/gms/common/e/b;

    invoke-direct {v3, v2}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 416
    const-string v2, "metadata_version"

    const-string v4, "0"

    const-string v7, ">=?"

    invoke-virtual {v3, v2, v4, v7}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 417
    const-string v2, "type"

    move/from16 v0, p1

    invoke-virtual {v3, v2, v0}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;I)V

    .line 418
    new-instance v2, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-direct {v2, v4}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    iput-object v3, v2, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    move/from16 v0, p2

    iput v0, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 422
    new-instance v7, Lcom/google/android/gms/games/request/a;

    invoke-direct {v7, v2}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 424
    :try_start_0
    invoke-virtual {v7}, Lcom/google/android/gms/games/request/a;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_0
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_8

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/request/GameRequest;

    .line 425
    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v9

    .line 426
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/google/android/gms/games/a/bv;->a(Ljava/lang/String;Lcom/google/android/gms/games/request/GameRequest;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 427
    invoke-virtual {v6, v9}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 432
    invoke-virtual {v6, v9}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/games/a/bx;

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v10

    iget-object v4, v3, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    invoke-interface {v4, v10}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v4, v3, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    const-string v11, "player_count"

    iget-object v12, v3, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    invoke-interface {v12}, Ljava/util/Set;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->i()I

    move-result v4

    const/4 v11, 0x2

    if-ne v4, v11, :cond_2

    const-string v4, "wish_count"

    :goto_1
    iget-object v11, v3, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    invoke-virtual {v11, v4}, Landroid/content/ContentValues;->getAsInteger(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Integer;->intValue()I

    move-result v11

    add-int/lit8 v11, v11, 0x1

    iget-object v12, v3, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v12, v4, v11}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->k()J

    move-result-wide v12

    invoke-virtual {v3}, Lcom/google/android/gms/games/a/bx;->a()J

    move-result-wide v14

    cmp-long v4, v12, v14

    if-gez v4, :cond_3

    iget-object v4, v3, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    const-string v11, "expiration_timestamp"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v4, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const/4 v4, 0x1

    :goto_2
    if-eqz v4, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/games/a/bx;->a:Ljava/lang/String;

    invoke-virtual {v4, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, v3, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/games/PlayerRef;->a(Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;

    move-result-object v2

    invoke-virtual {v4, v2}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    :cond_1
    invoke-virtual {v6, v9, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 441
    :catchall_0
    move-exception v2

    invoke-virtual {v7}, Lcom/google/android/gms/games/request/a;->w_()V

    throw v2

    .line 432
    :cond_2
    :try_start_1
    const-string v4, "gift_count"

    goto :goto_1

    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 436
    :cond_4
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v11

    new-instance v12, Landroid/content/ContentValues;

    invoke-direct {v12}, Landroid/content/ContentValues;-><init>()V

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->f()Lcom/google/android/gms/games/Game;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/games/GameRef;->a(Lcom/google/android/gms/games/Game;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    const-string v3, "sender_"

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/gms/games/PlayerRef;->a(Ljava/lang/String;Lcom/google/android/gms/games/Player;)Landroid/content/ContentValues;

    move-result-object v3

    invoke-virtual {v12, v3}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->i()I

    move-result v3

    const/4 v4, 0x2

    if-ne v3, v4, :cond_5

    const/4 v3, 0x1

    move v4, v3

    :goto_3
    const-string v13, "wish_count"

    if-eqz v4, :cond_6

    const/4 v3, 0x1

    :goto_4
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v13, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v13, "gift_count"

    if-eqz v4, :cond_7

    const/4 v3, 0x0

    :goto_5
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v12, v13, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v3, "expiration_timestamp"

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->k()J

    move-result-wide v14

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v12, v3, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    new-instance v2, Lcom/google/android/gms/games/a/bx;

    invoke-direct {v2, v12, v10}, Lcom/google/android/gms/games/a/bx;-><init>(Landroid/content/ContentValues;Ljava/lang/String;)V

    iget-object v3, v2, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    invoke-interface {v3, v11}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    const-string v3, "player_count"

    iget-object v4, v2, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    invoke-interface {v4}, Ljava/util/Set;->size()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v12, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v6, v9, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_5
    const/4 v3, 0x0

    move v4, v3

    goto :goto_3

    :cond_6
    const/4 v3, 0x0

    goto :goto_4

    :cond_7
    const/4 v3, 0x1

    goto :goto_5

    .line 441
    :cond_8
    invoke-virtual {v7}, Lcom/google/android/gms/games/request/a;->w_()V

    .line 444
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v3, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 446
    invoke-static {v3}, Ljava/util/Collections;->sort(Ljava/util/List;)V

    .line 448
    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_6
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_9

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/a/bx;

    .line 449
    iget-object v2, v2, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/data/m;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/common/data/m;

    goto :goto_6

    .line 452
    :cond_9
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p2

    invoke-static {v2, v0}, Lcom/google/android/gms/games/a/l;->a(II)I

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/data/m;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    return-object v2
.end method

.method public static a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v2

    move v4, p1

    move v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JII)Lcom/google/android/gms/common/e/b;

    move-result-object v1

    .line 342
    packed-switch p3, :pswitch_data_0

    .line 352
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown sort order "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 344
    :pswitch_0
    const-string v0, "expiration_timestamp ASC"

    .line 354
    :goto_0
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v1, v2, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    iput-object v0, v2, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput p4, v2, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0

    .line 348
    :pswitch_1
    const-string v0, "sender_is_in_circles DESC, CASE WHEN sender_is_in_circles=0 THEN next_expiring_request ELSE NULL END ASC, CASE WHEN sender_is_in_circles=0 THEN sender_id ELSE NULL END,expiration_timestamp ASC"

    goto :goto_0

    .line 342
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;IZ)Lcom/google/android/gms/common/data/DataHolder;
    .locals 12

    .prologue
    .line 516
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    invoke-virtual {p2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v1, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v5, Lcom/google/android/gms/games/h/a/ed;

    iget-object v2, p0, Lcom/google/android/gms/games/a/bv;->f:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    packed-switch p3, :pswitch_data_0

    const-string v2, "RequestUpdateType"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v9, "Unknown request update state: "

    invoke-direct {v7, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const-string v2, "UNKNOWN_UPDATE"

    :goto_1
    invoke-direct {v5, v0, v6, v2}, Lcom/google/android/gms/games/h/a/ed;-><init>(Ljava/lang/String;Ljava/lang/Long;Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    const/16 v2, 0x19

    if-lt v0, v2, :cond_f

    new-instance v0, Lcom/google/android/gms/games/h/a/ee;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/games/h/a/ee;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    move-object v1, v0

    goto :goto_0

    :pswitch_0
    const-string v2, "ACCEPT"

    goto :goto_1

    :pswitch_1
    const-string v2, "DISMISS"

    goto :goto_1

    :cond_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/games/h/a/ee;

    invoke-direct {v0, v1, v3}, Lcom/google/android/gms/games/h/a/ee;-><init>(Ljava/util/ArrayList;Ljava/lang/String;)V

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :cond_1
    new-instance v9, Lcom/google/android/gms/games/internal/request/c;

    invoke-direct {v9}, Lcom/google/android/gms/games/internal/request/c;-><init>()V

    .line 519
    const/4 v0, 0x0

    iput v0, v9, Lcom/google/android/gms/games/internal/request/c;->b:I

    .line 521
    const/4 v6, 0x0

    .line 522
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 524
    const/4 v0, 0x0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v7, v0

    :goto_3
    if-ge v7, v11, :cond_3

    .line 526
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bv;->e:Lcom/google/android/gms/games/h/a/eh;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/games/h/a/ee;

    const-string v3, "requests"

    if-eqz v2, :cond_2

    const-string v5, "language"

    invoke-static {v2}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v5, v2}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/games/h/a/eh;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x2

    const-class v5, Lcom/google/android/gms/games/h/a/eg;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/eg;

    .line 529
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eg;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v10, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move v0, v6

    .line 524
    :goto_4
    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v6, v0

    goto :goto_3

    .line 530
    :catch_0
    move-exception v0

    .line 531
    const-string v1, "RequestAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to send an update: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    const/4 v1, 0x1

    .line 533
    invoke-virtual {v8, v7}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ee;

    .line 534
    const/4 v2, 0x1

    if-ne p3, v2, :cond_e

    .line 536
    invoke-static {p1, v0}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/ee;)V

    .line 539
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ee;->getItems()Ljava/util/ArrayList;

    move-result-object v3

    .line 540
    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v0

    :goto_5
    if-ge v2, v4, :cond_e

    .line 541
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ed;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ed;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v5, 0x0

    invoke-virtual {v9, v0, v5}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    .line 540
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_5

    .line 548
    :cond_3
    if-nez v6, :cond_4

    if-eqz p4, :cond_4

    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bv;->c(Lcom/google/android/gms/games/a/au;)I

    .line 554
    :cond_4
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 555
    iget-object v0, v9, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    .line 556
    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v1

    add-int v4, v1, v0

    .line 557
    invoke-virtual {v10}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :cond_5
    :goto_6
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ef;

    .line 558
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->getUpdatedRequest()Lcom/google/android/gms/games/h/a/dz;

    move-result-object v7

    .line 559
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->c()Ljava/lang/String;

    move-result-object v8

    const/4 v2, -0x1

    invoke-virtual {v8}, Ljava/lang/String;->hashCode()I

    move-result v10

    sparse-switch v10, :sswitch_data_0

    :cond_6
    :goto_7
    packed-switch v2, :pswitch_data_1

    const-string v2, "RequestUpdateResultOutcome"

    new-instance v10, Ljava/lang/StringBuilder;

    const-string v11, "Unknown outcome type string: "

    invoke-direct {v10, v11}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, -0x1

    .line 560
    :goto_8
    packed-switch v2, :pswitch_data_2

    .line 605
    :pswitch_2
    const-string v2, "RequestAgent"

    const-string v7, "Failed to update request %s: %s"

    const/4 v8, 0x2

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v10

    const/4 v10, 0x1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->c()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 607
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v9, v0, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    goto :goto_6

    .line 559
    :sswitch_0
    const-string v10, "SUCCESS"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x0

    goto :goto_7

    :sswitch_1
    const-string v10, "ALREADY_UPDATED"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x1

    goto :goto_7

    :sswitch_2
    const-string v10, "NOT_FOUND"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x2

    goto :goto_7

    :sswitch_3
    const-string v10, "INVALID_ID"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x3

    goto :goto_7

    :sswitch_4
    const-string v10, "INVALID_OPERATION"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x4

    goto :goto_7

    :sswitch_5
    const-string v10, "DUPLICATE_UPDATE"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x5

    goto :goto_7

    :sswitch_6
    const-string v10, "INCONSISTENT_UPDATE"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x6

    goto/16 :goto_7

    :sswitch_7
    const-string v10, "MALFORMED"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/4 v2, 0x7

    goto/16 :goto_7

    :sswitch_8
    const-string v10, "PERMISSION_DENIED"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/16 v2, 0x8

    goto/16 :goto_7

    :sswitch_9
    const-string v10, "REQUEST_EXPIRED"

    invoke-virtual {v8, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_6

    const/16 v2, 0x9

    goto/16 :goto_7

    :pswitch_3
    const/4 v2, 0x0

    goto/16 :goto_8

    :pswitch_4
    const/4 v2, 0x4

    goto/16 :goto_8

    :pswitch_5
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_6
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_7
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_8
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_9
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_a
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_b
    const/4 v2, 0x1

    goto/16 :goto_8

    :pswitch_c
    const/4 v2, 0x3

    goto/16 :goto_8

    .line 562
    :pswitch_d
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v9, v8, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    .line 564
    packed-switch p3, :pswitch_data_3

    .line 576
    invoke-direct {p0, p1, v7}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 577
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_6

    .line 567
    :pswitch_e
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 569
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v7, 0x0

    const/4 v8, 0x0

    invoke-virtual {v2, v0, v7, v8}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 571
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    .line 572
    goto/16 :goto_6

    .line 584
    :pswitch_f
    const-string v2, "RequestAgent"

    const-string v7, "Request %s was already expired; deleting."

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v11

    aput-object v11, v8, v10

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 586
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v7

    invoke-static {v2, v7}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 588
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    const/4 v8, 0x0

    const/4 v10, 0x0

    invoke-virtual {v7, v2, v8, v10}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 590
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v9, v0, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    goto/16 :goto_6

    .line 595
    :pswitch_10
    invoke-direct {p0, p1, v7}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 596
    add-int/lit8 v1, v1, 0x1

    .line 597
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v9, v0, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    goto/16 :goto_6

    .line 599
    :cond_7
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ef;->b()Ljava/lang/String;

    move-result-object v0

    const/4 v2, 0x1

    invoke-virtual {v9, v0, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    goto/16 :goto_6

    .line 614
    :cond_8
    if-nez v1, :cond_9

    if-eqz v6, :cond_9

    .line 615
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 638
    :goto_9
    return-object v0

    .line 619
    :cond_9
    if-ge v4, v3, :cond_b

    .line 621
    const/16 v0, 0x7d0

    iput v0, v9, Lcom/google/android/gms/games/internal/request/c;->b:I

    .line 622
    const/4 v0, 0x0

    move v1, v0

    :goto_a
    if-ge v1, v3, :cond_c

    .line 623
    invoke-virtual {p2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 624
    iget-object v2, v9, Lcom/google/android/gms/games/internal/request/c;->a:Ljava/util/HashMap;

    invoke-virtual {v2, v0}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    .line 625
    const/4 v2, 0x2

    invoke-virtual {v9, v0, v2}, Lcom/google/android/gms/games/internal/request/c;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/internal/request/c;

    .line 622
    :cond_a
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_a

    .line 628
    :cond_b
    if-ge v1, v4, :cond_c

    .line 631
    if-nez v1, :cond_d

    .line 632
    const/16 v0, 0x7d1

    iput v0, v9, Lcom/google/android/gms/games/internal/request/c;->b:I

    .line 638
    :cond_c
    :goto_b
    invoke-virtual {v9}, Lcom/google/android/gms/games/internal/request/c;->a()Lcom/google/android/gms/games/internal/request/b;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/internal/request/b;->a(Lcom/google/android/gms/games/internal/request/b;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_9

    .line 634
    :cond_d
    const/16 v0, 0x7d0

    iput v0, v9, Lcom/google/android/gms/games/internal/request/c;->b:I

    goto :goto_b

    :cond_e
    move v0, v1

    goto/16 :goto_4

    :cond_f
    move-object v0, v1

    goto/16 :goto_2

    .line 516
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 559
    :sswitch_data_0
    .sparse-switch
        -0x7e56c321 -> :sswitch_4
        -0x7330976f -> :sswitch_6
        -0x546b1bf5 -> :sswitch_8
        -0x5045737d -> :sswitch_3
        -0x447f341d -> :sswitch_0
        0x13ba7935 -> :sswitch_9
        0x290e4b9b -> :sswitch_7
        0x321f7e9d -> :sswitch_5
        0x3cfe1ed6 -> :sswitch_2
        0x3e279794 -> :sswitch_1
    .end sparse-switch

    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
    .end packed-switch

    .line 560
    :pswitch_data_2
    .packed-switch 0x0
        :pswitch_d
        :pswitch_2
        :pswitch_2
        :pswitch_f
        :pswitch_10
    .end packed-switch

    .line 564
    :pswitch_data_3
    .packed-switch 0x1
        :pswitch_e
    .end packed-switch
.end method

.method private static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JII)Lcom/google/android/gms/common/e/b;
    .locals 4

    .prologue
    .line 367
    add-int/lit8 v0, p5, -0x1

    and-int/2addr v0, p5

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Request type must be a single bit value!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 371
    invoke-static {p0}, Lcom/google/android/gms/games/provider/at;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 372
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 373
    const-string v0, "metadata_version"

    const-string v2, "0"

    const-string v3, ">=?"

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 374
    const-string v0, "type"

    invoke-static {p5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 375
    if-eqz p1, :cond_0

    .line 376
    const-string v0, "external_game_id"

    invoke-virtual {v1, v0, p1}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 379
    :cond_0
    packed-switch p4, :pswitch_data_0

    .line 391
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown request direction "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 367
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 382
    :pswitch_0
    const-string v0, "player_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    :goto_1
    return-object v1

    .line 387
    :pswitch_1
    const-string v0, "sender_id"

    invoke-static {p2, p3}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 379
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/by;
    .locals 9

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 845
    :goto_0
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v1

    .line 847
    iget-object v2, p0, Lcom/google/android/gms/games/a/bv;->e:Lcom/google/android/gms/games/h/a/eh;

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "ANDROID"

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v0, "requests/sync"

    if-eqz v3, :cond_0

    const-string v8, "language"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v8, v3}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const-string v3, "platformType"

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-eqz v5, :cond_1

    const-string v0, "requestingApplicationId"

    invoke-static {v5}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p2, :cond_2

    const-string v0, "syncToken"

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v0, v4}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/eh;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/ec;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ec;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 867
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ec;->getItems()Ljava/util/ArrayList;

    move-result-object v1

    .line 868
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ec;->c()Ljava/lang/String;

    move-result-object v2

    .line 869
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ec;->b()Ljava/lang/Boolean;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 870
    invoke-static {p2, v2}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_6

    const/4 v0, 0x1

    :goto_1
    const-string v3, "Server claims to have more data, yet sync tokens match!"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 872
    invoke-direct {p0, p1, v2}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/by;

    move-result-object v0

    .line 873
    iget v6, v0, Lcom/google/android/gms/games/a/by;->c:I

    .line 874
    if-nez v6, :cond_3

    .line 875
    iget-object v2, v0, Lcom/google/android/gms/games/a/by;->a:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 876
    iget-object v0, v0, Lcom/google/android/gms/games/a/by;->b:Ljava/lang/String;

    move-object v2, v0

    .line 880
    :cond_3
    if-nez v1, :cond_7

    .line 882
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 884
    :goto_2
    new-instance v1, Lcom/google/android/gms/games/a/by;

    invoke-direct {v1, v0, v2, v6}, Lcom/google/android/gms/games/a/by;-><init>(Ljava/util/ArrayList;Ljava/lang/String;I)V

    move-object v0, v1

    :goto_3
    return-object v0

    .line 850
    :catch_0
    move-exception v0

    .line 851
    const/16 v1, 0x19a

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 854
    new-instance v0, Ljava/util/ArrayList;

    const/4 v1, 0x3

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    const-string v2, "request_sync_token"

    invoke-virtual {v1, v2, v7}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "RequestAgent"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 855
    const-string v0, "RequestAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Token "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is invalid. Retrying with no token."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object p2, v7

    .line 856
    goto/16 :goto_0

    .line 859
    :cond_4
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_5

    .line 860
    const-string v1, "RequestAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 862
    :cond_5
    new-instance v0, Lcom/google/android/gms/games/a/by;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/by;-><init>()V

    goto :goto_3

    :cond_6
    move v0, v6

    .line 870
    goto/16 :goto_1

    :cond_7
    move-object v0, v1

    goto :goto_2
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;
    .locals 5

    .prologue
    .line 1276
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1277
    new-instance v2, Lcom/google/android/gms/games/request/a;

    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-static {p1}, Lcom/google/android/gms/games/provider/at;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 1281
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/request/GameRequest;

    .line 1282
    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/games/Player;->e()Landroid/net/Uri;

    move-result-object v4

    .line 1283
    invoke-interface {v0}, Lcom/google/android/gms/games/request/GameRequest;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1286
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->w_()V

    throw v0

    :cond_0
    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->w_()V

    .line 1288
    return-object v1
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/by;)Ljava/util/HashMap;
    .locals 5

    .prologue
    .line 916
    iget-object v2, p2, Lcom/google/android/gms/games/a/by;->a:Ljava/util/ArrayList;

    .line 917
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v3

    .line 918
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 919
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    .line 920
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ea;

    .line 921
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ea;->getRequest()Lcom/google/android/gms/games/h/a/dz;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dz;->b()Ljava/lang/String;

    move-result-object v0

    .line 922
    if-eqz v0, :cond_0

    .line 923
    invoke-virtual {v4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 919
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 926
    :cond_1
    invoke-static {p0, p1, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/util/Collection;)Ljava/util/HashMap;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Landroid/net/Uri;Lcom/google/android/gms/games/h/a/cn;ILjava/util/ArrayList;)V
    .locals 10

    .prologue
    .line 1179
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    .line 1180
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/cn;->getRecipients()Ljava/util/ArrayList;

    move-result-object v3

    .line 1181
    const/4 v0, 0x0

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v1, v0

    :goto_0
    if-ge v1, v4, :cond_0

    .line 1182
    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/eb;

    .line 1183
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/eb;->getRecipientPlayer()Lcom/google/android/gms/games/h/a/cs;

    move-result-object v5

    .line 1184
    invoke-virtual {p4}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 1185
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, v5, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v8

    invoke-static {v7, v5, v8, v9}, Lcom/google/android/gms/games/a/l;->a(Lcom/google/android/gms/common/server/ClientContext;Landroid/content/ContentValues;J)Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {p4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1189
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    iget-object v0, v0, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual {v5, v0}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "player_id"

    invoke-virtual {v0, v5, v6}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    const-string v5, "request_id"

    invoke-virtual {v0, v5, p3}, Landroid/content/ContentProviderOperation$Builder;->withValueBackReference(Ljava/lang/String;I)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p4, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1181
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 1195
    :cond_0
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/by;)V
    .locals 13

    .prologue
    .line 1206
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/bv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Ljava/util/HashMap;

    move-result-object v7

    .line 1208
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 1209
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/an;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v9

    .line 1210
    iget-object v10, p1, Lcom/google/android/gms/games/a/by;->a:Ljava/util/ArrayList;

    .line 1211
    const/4 v0, 0x0

    invoke-virtual {v10}, Ljava/util/ArrayList;->size()I

    move-result v11

    move v6, v0

    :goto_0
    if-ge v6, v11, :cond_4

    .line 1212
    invoke-virtual {v10, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ea;

    .line 1213
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ea;->getRequest()Lcom/google/android/gms/games/h/a/dz;

    move-result-object v3

    .line 1214
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/dz;->c()Ljava/lang/String;

    move-result-object v4

    .line 1215
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/dz;->b()Ljava/lang/String;

    move-result-object v1

    .line 1219
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->g()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1220
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v2

    iput-object v1, v2, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v5

    .line 1223
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/dz;->d()Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v12, 0x3e8

    if-ne v2, v12, :cond_1

    const/4 v2, 0x1

    .line 1224
    :goto_1
    invoke-static {v5, v4, v2}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Z

    move-result v2

    .line 1226
    if-eqz v2, :cond_2

    .line 1228
    const-string v2, "RequestAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v12, "Notification "

    invoke-direct {v3, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, " consumed by listener for game "

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v12, ". Deleting."

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1230
    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object v4, v2, v3

    .line 1231
    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "external_sub_id=?"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1237
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ea;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v12

    .line 1238
    if-eqz v12, :cond_0

    .line 1239
    iget-object v0, v5, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, v5, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x4

    const/4 v4, 0x6

    invoke-virtual {v12}, Lcom/google/android/gms/games/h/a/bj;->b()Ljava/lang/String;

    move-result-object v5

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;)V

    .line 1211
    :cond_0
    :goto_2
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 1223
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 1251
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ea;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1252
    new-instance v1, Lcom/google/android/gms/common/e/b;

    invoke-direct {v1, v9}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1253
    const-string v2, "external_sub_id"

    invoke-virtual {v1, v2, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1254
    const-string v2, "notification_id"

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ea;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/bj;->b()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1255
    invoke-static {v9}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v2

    iget-object v1, v1, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v0, v2, v1}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    .line 1259
    invoke-virtual {v3}, Lcom/google/android/gms/games/h/a/dz;->getInboundRequestInfo()Lcom/google/android/gms/games/h/a/bq;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 1260
    invoke-virtual {v7, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    .line 1261
    if-nez v0, :cond_3

    const/4 v0, 0x0

    .line 1262
    :goto_3
    const-string v2, "image_id"

    invoke-virtual {v1, v2, v0}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    .line 1263
    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 1261
    :cond_3
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_3

    .line 1269
    :cond_4
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1270
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v8, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1273
    :cond_5
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/ee;)V
    .locals 12

    .prologue
    const-wide/16 v10, 0x0

    .line 643
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 644
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 645
    new-instance v3, Ljava/util/ArrayList;

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ee;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 646
    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ee;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/ed;

    .line 647
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/ed;->b()Ljava/lang/String;

    move-result-object v0

    .line 648
    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 650
    invoke-static {v1, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v6

    cmp-long v6, v6, v10

    if-eqz v6, :cond_1

    .line 651
    invoke-static {v5}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 655
    :cond_1
    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/au;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    .line 656
    invoke-static {v1, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;)J

    move-result-wide v6

    cmp-long v5, v6, v10

    if-nez v5, :cond_0

    .line 658
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 661
    const-string v6, "client_context_id"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v5, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 663
    const-string v6, "external_request_id"

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 664
    const-string v0, "external_game_id"

    invoke-virtual {p1}, Lcom/google/android/gms/games/h/a/ee;->b()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 666
    invoke-static {v2}, Lcom/google/android/gms/games/provider/au;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 667
    invoke-static {v0}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 671
    :cond_2
    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v3, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 672
    return-void
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;)Z
    .locals 8

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1377
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1378
    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->d()Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x3e8

    if-ne v3, v4, :cond_0

    .line 1379
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {p2}, Lcom/google/android/gms/games/h/a/dz;->c()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    .line 1381
    invoke-static {v2}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1384
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "RequestAgent"

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move v0, v1

    .line 1407
    :goto_0
    return v0

    .line 1389
    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;Ljava/util/ArrayList;)I

    move-result v3

    .line 1390
    const/4 v4, -0x1

    if-ne v3, v4, :cond_1

    move v0, v2

    .line 1391
    goto :goto_0

    .line 1395
    :cond_1
    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "RequestAgent"

    invoke-static {v4, v0, v5}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1397
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentProviderResult;

    iget-object v0, v0, Landroid/content/ContentProviderResult;->uri:Landroid/net/Uri;

    .line 1398
    if-nez v0, :cond_2

    .line 1399
    const-string v0, "RequestAgent"

    const-string v1, "Failed to store data for request"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1400
    goto :goto_0

    .line 1402
    :cond_2
    invoke-static {v0}, Landroid/content/ContentUris;->parseId(Landroid/net/Uri;)J

    move-result-wide v4

    .line 1403
    const-wide/16 v6, 0x0

    cmp-long v0, v4, v6

    if-gez v0, :cond_3

    .line 1404
    const-string v0, "RequestAgent"

    const-string v1, "Failed to store data for newly created entity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v2

    .line 1405
    goto :goto_0

    :cond_3
    move v0, v1

    .line 1407
    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 1304
    invoke-static {}, Lcom/google/android/gms/games/a/am;->a()Lcom/google/android/gms/games/a/am;

    move-result-object v0

    .line 1305
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v1

    .line 1306
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/a/am;->d(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v3

    .line 1331
    :goto_0
    return v0

    .line 1311
    :cond_0
    new-instance v2, Lcom/google/android/gms/games/a/o;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4, p1}, Lcom/google/android/gms/games/provider/at;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    .line 1318
    new-instance v6, Lcom/google/android/gms/games/request/a;

    invoke-direct {v6, v4}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 1320
    :try_start_0
    invoke-virtual {v6}, Lcom/google/android/gms/games/request/a;->c()I

    move-result v2

    if-lez v2, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    const/4 v2, 0x0

    invoke-virtual {v6, v2}, Lcom/google/android/gms/games/request/a;->a(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/request/GameRequest;

    invoke-interface {v2}, Lcom/google/android/gms/games/request/GameRequest;->g()Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v5, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_1

    .line 1322
    invoke-virtual {v6}, Lcom/google/android/gms/games/request/a;->w_()V

    move v0, v3

    goto :goto_0

    .line 1325
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    move-object v3, p1

    move v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/am;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/data/DataHolder;Z)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v0

    .line 1328
    invoke-virtual {v6}, Lcom/google/android/gms/games/request/a;->w_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v6}, Lcom/google/android/gms/games/request/a;->w_()V

    throw v0
.end method

.method private static a(Ljava/lang/String;Lcom/google/android/gms/games/request/GameRequest;)Z
    .locals 2

    .prologue
    .line 456
    invoke-interface {p1}, Lcom/google/android/gms/games/request/GameRequest;->m()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 457
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 458
    const/4 v0, 0x1

    .line 461
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/google/android/gms/games/a/au;)I
    .locals 14

    .prologue
    const/4 v13, 0x1

    const/4 v6, 0x0

    .line 692
    iget-object v7, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 693
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 695
    invoke-static {v0}, Lcom/google/android/gms/games/provider/au;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 696
    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v8

    .line 697
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 698
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v2, Lcom/google/android/gms/games/a/bw;->a:[Ljava/lang/String;

    const-string v3, "account_name=?"

    new-array v4, v13, [Ljava/lang/String;

    aput-object v8, v4, v6

    const-string v5, "package_name"

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 703
    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    move v1, v6

    .line 706
    :goto_0
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 708
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 709
    const/4 v0, 0x2

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 710
    const/4 v0, 0x4

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 711
    const/4 v2, 0x3

    invoke-interface {v3, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 713
    invoke-static {v0, v2, v8}, Lcom/google/android/gms/games/a/l;->a(ILjava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    .line 718
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 719
    invoke-virtual {v4, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    move-object v2, v0

    .line 727
    :goto_1
    invoke-virtual {v2, v10}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 728
    invoke-virtual {v2, v10}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 735
    :goto_2
    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    add-int/lit8 v1, v1, 0x1

    .line 737
    goto :goto_0

    .line 721
    :cond_0
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 722
    invoke-virtual {v4, v2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-object v2, v0

    goto :goto_1

    .line 730
    :cond_1
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 731
    invoke-virtual {v2, v10, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 739
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 742
    invoke-virtual {v4}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v1

    :goto_3
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    .line 743
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v3, v2

    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 746
    invoke-virtual {p1, v7, v0}, Lcom/google/android/gms/games/a/au;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/av;

    move-result-object v10

    iget-object v2, v10, Lcom/google/android/gms/games/a/av;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v2, 0x0

    :goto_5
    iput-object v2, v10, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    iput-object v1, v10, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v10}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v10

    .line 751
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/ArrayList;

    invoke-direct {p0, v10, v2, v13, v6}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v10

    .line 755
    :try_start_1
    invoke-static {v10}, Lcom/google/android/gms/games/internal/request/b;->a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/internal/request/b;

    move-result-object v11

    .line 758
    invoke-virtual {v4, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/HashMap;

    invoke-virtual {v2, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 759
    invoke-virtual {v11, v1}, Lcom/google/android/gms/games/internal/request/b;->a(Ljava/lang/String;)I

    move-result v12

    if-nez v12, :cond_3

    .line 761
    invoke-static {v0, v1}, Lcom/google/android/gms/games/provider/au;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 763
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v1

    invoke-virtual {v9, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 764
    add-int/lit8 v3, v3, -0x1

    goto :goto_6

    :cond_4
    move-object v2, v1

    .line 746
    goto :goto_5

    .line 768
    :cond_5
    invoke-virtual {v10}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_4

    :catchall_1
    move-exception v0

    invoke-virtual {v10}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    :cond_6
    move v2, v3

    .line 770
    goto/16 :goto_3

    .line 773
    :cond_7
    invoke-virtual {v9}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 774
    invoke-virtual {v7}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v9, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 777
    :cond_8
    if-nez v2, :cond_9

    move v0, v6

    :goto_7
    return v0

    :cond_9
    const/4 v0, 0x5

    goto :goto_7
.end method

.method private static d(Lcom/google/android/gms/games/a/au;)Ljava/util/HashSet;
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x0

    .line 1017
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1018
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 1019
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v2

    .line 1022
    const/4 v5, 0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JII)Lcom/google/android/gms/common/e/b;

    move-result-object v5

    .line 1024
    iget-object v7, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v8, "external_request_id"

    invoke-static {v7, v5, v8}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/e/b;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v5

    .line 1026
    invoke-virtual {v6, v5}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1029
    const/4 v5, 0x2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;JII)Lcom/google/android/gms/common/e/b;

    move-result-object v0

    .line 1031
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "external_request_id"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/e/b;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v0

    .line 1033
    invoke-virtual {v6, v0}, Ljava/util/HashSet;->addAll(Ljava/util/Collection;)Z

    .line 1034
    return-object v6
.end method

.method private static e(Lcom/google/android/gms/games/a/au;)V
    .locals 14

    .prologue
    const/4 v1, 0x0

    .line 1045
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    .line 1046
    sget-object v0, Lcom/google/android/gms/games/c/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v7

    .line 1047
    const-wide/16 v2, -0x1

    .line 1048
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v8

    .line 1050
    const-string v10, "1"

    .line 1053
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v0

    .line 1054
    new-instance v4, Lcom/google/android/gms/common/e/b;

    invoke-direct {v4, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1055
    const-string v0, "status"

    invoke-virtual {v4, v0, v10}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1056
    const-string v0, "sender_id"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v0, v5}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1057
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    sget-object v4, Lcom/google/android/gms/games/a/bz;->a:[Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    const-string v4, "game_id,creation_timestamp DESC"

    iput-object v4, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v11

    move v0, v1

    .line 1063
    :cond_0
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 1064
    const/4 v4, 0x1

    invoke-interface {v11, v4}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 1065
    cmp-long v12, v4, v2

    if-eqz v12, :cond_1

    move v0, v1

    move-wide v2, v4

    .line 1070
    :cond_1
    if-ge v0, v7, :cond_2

    .line 1071
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1072
    :cond_2
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v6, v12}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v12

    if-nez v12, :cond_0

    .line 1073
    const/4 v12, 0x2

    invoke-interface {v11, v12}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    .line 1074
    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v6, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1078
    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_3
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 1083
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    .line 1084
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1085
    invoke-virtual {v6}, Ljava/util/HashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    .line 1086
    invoke-virtual {v6, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 1087
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v1}, Lcom/google/android/gms/games/provider/aw;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v1

    .line 1088
    new-instance v7, Lcom/google/android/gms/common/e/b;

    invoke-direct {v7, v1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 1089
    const-string v11, "status"

    invoke-virtual {v7, v11, v10}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1090
    const-string v11, "creation_timestamp"

    invoke-static {v4, v5}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    const-string v5, "<=?"

    invoke-virtual {v7, v11, v4, v5}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 1091
    const-string v4, "game_id"

    invoke-static {v0}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v4, v0}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1092
    const-string v0, "sender_id"

    invoke-static {v8, v9}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v7, v0, v4}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 1093
    invoke-static {v1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v7}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v4, v7, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v0, v1, v4}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 1099
    :cond_4
    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 1100
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "RequestAgent"

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 1102
    :cond_5
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;)I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 279
    const/4 v0, 0x1

    invoke-static {p1, v2, v0, v2, v2}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 286
    :try_start_0
    new-instance v0, Lcom/google/android/gms/games/request/a;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 287
    invoke-virtual {v0}, Lcom/google/android/gms/games/request/a;->c()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 289
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 293
    const/4 v1, 0x2

    invoke-static {p1, v2, v1, v2, v2}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;IIII)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 297
    :try_start_1
    new-instance v2, Lcom/google/android/gms/games/request/a;

    invoke-direct {v2, v1}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 298
    invoke-virtual {v2}, Lcom/google/android/gms/games/request/a;->c()I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result v2

    add-int/2addr v0, v2

    .line 300
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 303
    return v0

    .line 289
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    .line 300
    :catchall_1
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final bridge synthetic a(Lcom/google/android/gms/games/a/bb;)I
    .locals 1

    .prologue
    .line 95
    invoke-super {p0, p1}, Lcom/google/android/gms/games/a/bb;->a(Lcom/google/android/gms/games/a/bb;)I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;ILjava/lang/Integer;[BLjava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 206
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bv;->c(Lcom/google/android/gms/games/a/au;)I

    .line 211
    if-nez p4, :cond_1

    move-object v3, v0

    .line 212
    :goto_0
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    const/4 v2, -0x1

    if-ne v1, v2, :cond_3

    move-object v2, v0

    .line 215
    :goto_1
    new-instance v0, Lcom/google/android/gms/games/h/a/fc;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/a/bv;->f:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-static {p2}, Lcom/google/android/gms/games/internal/b/g;->a(I)Ljava/lang/String;

    move-result-object v6

    move-object v4, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/h/a/fc;-><init>(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;Ljava/util/ArrayList;Ljava/lang/Long;Ljava/lang/String;)V

    .line 222
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/bv;->e:Lcom/google/android/gms/games/h/a/eh;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "requests/send"

    if-eqz v3, :cond_0

    const-string v5, "language"

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v5, v3}, Lcom/google/android/gms/games/h/a/eh;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    :cond_0
    iget-object v1, v1, Lcom/google/android/gms/games/h/a/eh;->a:Lcom/google/android/gms/common/server/n;

    const/4 v3, 0x2

    const-class v6, Lcom/google/android/gms/games/h/a/dz;

    move-object v5, v0

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/dz;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    .line 230
    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;)Z

    move-result v1

    if-nez v1, :cond_2

    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    :goto_2
    return-object v0

    .line 211
    :cond_1
    invoke-static {p4}, Lcom/google/android/gms/common/util/m;->b([B)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 224
    :catch_0
    move-exception v0

    .line 225
    const-string v1, "RequestAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to send a request: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    const/4 v0, 0x6

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    .line 230
    :cond_2
    new-instance v1, Lcom/google/android/gms/games/a/o;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/dz;->c()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/at;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_2

    :cond_3
    move-object v2, p3

    goto/16 :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 246
    const/4 v0, 0x0

    const/4 v1, 0x1

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/games/a/bb;
    .locals 0

    .prologue
    .line 269
    return-object p0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 313
    and-int/lit8 v0, p1, 0x4

    if-eqz v0, :cond_0

    .line 314
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/a/bv;->h:Z

    .line 316
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Landroid/content/SyncResult;)V
    .locals 6

    .prologue
    .line 686
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/a/bv;->c(Lcom/google/android/gms/games/a/au;)I

    move-result v0

    if-eqz v0, :cond_0

    .line 687
    iget-object v0, p2, Landroid/content/SyncResult;->stats:Landroid/content/SyncStats;

    iget-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, v0, Landroid/content/SyncStats;->numIoExceptions:J

    .line 689
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/a/au;)I
    .locals 17

    .prologue
    .line 798
    invoke-direct/range {p0 .. p1}, Lcom/google/android/gms/games/a/bv;->c(Lcom/google/android/gms/games/a/au;)I

    .line 800
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    .line 801
    move-object/from16 v0, p0

    iget-wide v4, v0, Lcom/google/android/gms/games/a/bv;->g:J

    sub-long v4, v2, v4

    sget-object v2, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v2, v4, v2

    if-gtz v2, :cond_0

    .line 802
    const-string v2, "RequestAgent"

    const-string v3, "Returning cached entities"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 803
    const/4 v2, 0x0

    .line 827
    :goto_0
    return v2

    .line 807
    :cond_0
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->i()J

    .line 810
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    sget-object v4, Lcom/google/android/gms/games/a/bv;->b:[Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 812
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/games/a/by;

    move-result-object v11

    .line 813
    const-string v2, "RequestAgent"

    const-string v3, "Received %s requests during sync"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget-object v6, v11, Lcom/google/android/gms/games/a/by;->a:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 815
    iget v2, v11, Lcom/google/android/gms/games/a/by;->c:I

    if-eqz v2, :cond_1

    .line 816
    iget v2, v11, Lcom/google/android/gms/games/a/by;->c:I

    goto :goto_0

    .line 820
    :cond_1
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/a/bv;->d(Lcom/google/android/gms/games/a/au;)Ljava/util/HashSet;

    move-result-object v12

    new-instance v13, Ljava/util/ArrayList;

    invoke-direct {v13}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, v11, Lcom/google/android/gms/games/a/by;->b:Ljava/lang/String;

    if-eqz v2, :cond_2

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v3}, Lcom/google/android/gms/games/provider/m;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v3

    invoke-static {v3}, Landroid/content/ContentProviderOperation;->newUpdate(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v3

    const-string v4, "request_sync_token"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentProviderOperation$Builder;->withValue(Ljava/lang/String;Ljava/lang/Object;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2, v3, v11}, Lcom/google/android/gms/games/a/bv;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/a/by;)Ljava/util/HashMap;

    move-result-object v14

    iget-object v15, v11, Lcom/google/android/gms/games/a/by;->a:Ljava/util/ArrayList;

    const/4 v2, 0x0

    invoke-virtual {v15}, Ljava/util/ArrayList;->size()I

    move-result v16

    move v10, v2

    :goto_1
    move/from16 v0, v16

    if-ge v10, v0, :cond_6

    invoke-virtual {v15, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ea;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ea;->getRequest()Lcom/google/android/gms/games/h/a/dz;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/dz;->b()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    move-object/from16 v4, p1

    move-object v6, v3

    :goto_2
    move-object/from16 v0, p0

    invoke-direct {v0, v4, v8, v13}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/h/a/dz;Ljava/util/ArrayList;)I

    move-result v3

    const/4 v5, -0x1

    if-eq v3, v5, :cond_3

    iget-object v3, v4, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v4, v4, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ea;->getNotification()Lcom/google/android/gms/games/h/a/bj;

    move-result-object v5

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    invoke-virtual {v8}, Lcom/google/android/gms/games/h/a/dz;->c()Ljava/lang/String;

    move-result-object v8

    const/4 v9, 0x4

    invoke-static/range {v3 .. v9}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/h/a/bj;JLjava/lang/String;I)Landroid/content/ContentProviderOperation;

    move-result-object v2

    invoke-virtual {v13, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_3
    :goto_3
    add-int/lit8 v2, v10, 0x1

    move v10, v2

    goto :goto_1

    :cond_4
    invoke-virtual {v14, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Long;

    if-nez v3, :cond_5

    const-string v2, "RequestAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "No game found matching external game ID "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_3

    :cond_5
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->c()Lcom/google/android/gms/games/a/av;

    move-result-object v5

    iput-object v4, v5, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v4

    move-object v6, v3

    goto :goto_2

    :cond_6
    const/4 v2, 0x1

    invoke-virtual {v13}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-lez v3, :cond_7

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "RequestAgent"

    invoke-static {v2, v13, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    move-result v2

    :cond_7
    if-nez v2, :cond_9

    const-string v2, "RequestAgent"

    const-string v3, "Failed to store requests"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v2, 0x0

    .line 821
    :goto_4
    if-eqz v2, :cond_8

    .line 824
    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/a/by;)V

    .line 825
    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    move-object/from16 v0, p0

    iput-wide v2, v0, Lcom/google/android/gms/games/a/bv;->g:J

    .line 827
    :cond_8
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 820
    :cond_9
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/a/bv;->d(Lcom/google/android/gms/games/a/au;)Ljava/util/HashSet;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    invoke-virtual {v3}, Ljava/util/HashSet;->size()I

    move-result v3

    if-lez v3, :cond_a

    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/gms/games/a/bv;->h:Z

    :cond_a
    invoke-static/range {p1 .. p1}, Lcom/google/android/gms/games/a/bv;->e(Lcom/google/android/gms/games/a/au;)V

    goto :goto_4
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    const/4 v0, 0x1

    .line 263
    invoke-direct {p0, p1, p2, v0, v0}, Lcom/google/android/gms/games/a/bv;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;IZ)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 274
    const-string v0, "inbox_requests_count"

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 308
    iget-boolean v0, p0, Lcom/google/android/gms/games/a/bv;->h:Z

    return v0
.end method

.method public final bridge synthetic d()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->d()V

    return-void
.end method

.method public final bridge synthetic e()Z
    .locals 1

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->e()Z

    move-result v0

    return v0
.end method

.method public final bridge synthetic f()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->f()V

    return-void
.end method

.method public final bridge synthetic g()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->g()V

    return-void
.end method

.method public final bridge synthetic h()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->h()V

    return-void
.end method

.method public final bridge synthetic i()V
    .locals 0

    .prologue
    .line 95
    invoke-super {p0}, Lcom/google/android/gms/games/a/bb;->i()V

    return-void
.end method

.method public final j()V
    .locals 2

    .prologue
    .line 785
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Lcom/google/android/gms/games/a/bv;->g:J

    .line 786
    return-void
.end method
