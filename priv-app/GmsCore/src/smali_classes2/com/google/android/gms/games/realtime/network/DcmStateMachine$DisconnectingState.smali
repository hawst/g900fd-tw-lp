.class final Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;
.super Lcom/google/android/gms/games/realtime/network/s;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field e:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V
    .locals 0

    .prologue
    .line 414
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/realtime/network/s;-><init>(Lcom/google/android/gms/games/realtime/network/DcmStateMachine;)V

    .line 415
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Runnable;)V
    .locals 0

    .prologue
    .line 420
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->e:Ljava/lang/Runnable;

    .line 421
    invoke-virtual {p0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->c()V

    .line 422
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 426
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 449
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->g:Z

    :goto_0
    return v0

    .line 428
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/jingle/k;

    .line 429
    iget-boolean v0, v0, Lcom/google/android/gms/games/jingle/k;->a:Z

    if-eqz v0, :cond_0

    .line 431
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    const-string v1, "DcmStateMachine"

    const-string v2, "Libjingle didn\'t disconnect"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 441
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->f:Z

    goto :goto_0

    .line 438
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 439
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->a:Lcom/google/android/gms/games/realtime/network/u;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/u;->b:Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectedState;->b()V

    goto :goto_1

    .line 444
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->b:Lcom/google/android/gms/games/realtime/network/t;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/t;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    invoke-virtual {v0}, Lcom/google/android/gms/games/jingle/Libjingle;->a()V

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0}, Lcom/google/android/gms/games/service/statemachine/d;->b()V

    .line 446
    sget-boolean v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine$DisconnectingState;->f:Z

    goto :goto_0

    .line 426
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x1771 -> :sswitch_1
    .end sparse-switch
.end method
