.class public Lcom/google/android/gms/car/pw;
.super Lcom/google/android/car/fsm/i;
.source "SourceFile"


# instance fields
.field private a:Landroid/widget/Button;

.field private b:Landroid/widget/Button;

.field private c:Landroid/widget/TextView;

.field private d:Landroid/widget/TextView;

.field private e:Landroid/widget/ProgressBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/car/fsm/i;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/pw;)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/pw;->e:Landroid/widget/ProgressBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ProgressBar;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/pw;->c:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v0, p0, Lcom/google/android/gms/car/pw;->d:Landroid/widget/TextView;

    sget v1, Lcom/google/android/gms/p;->dH:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/pw;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/car/pw;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/pw;->a:Landroid/widget/Button;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/pw;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/pw;->b:Landroid/widget/Button;

    return-object v0
.end method


# virtual methods
.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3

    .prologue
    .line 66
    sget v0, Lcom/google/android/gms/l;->P:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 69
    sget v0, Lcom/google/android/gms/j;->mN:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/car/pw;->a:Landroid/widget/Button;

    .line 70
    sget v0, Lcom/google/android/gms/j;->cq:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/car/pw;->b:Landroid/widget/Button;

    .line 71
    sget v0, Lcom/google/android/gms/j;->pU:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ProgressBar;

    iput-object v0, p0, Lcom/google/android/gms/car/pw;->e:Landroid/widget/ProgressBar;

    .line 72
    sget v0, Lcom/google/android/gms/j;->jw:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/car/pw;->c:Landroid/widget/TextView;

    .line 73
    sget v0, Lcom/google/android/gms/j;->ca:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/car/pw;->d:Landroid/widget/TextView;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/car/pw;->a:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/gms/car/px;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/px;-><init>(Lcom/google/android/gms/car/pw;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/car/pw;->b:Landroid/widget/Button;

    new-instance v2, Lcom/google/android/gms/car/py;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/py;-><init>(Lcom/google/android/gms/car/pw;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 94
    return-object v1
.end method
