.class public final Lcom/google/android/gms/games/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/g;


# instance fields
.field public final a:Z

.field public final b:Z

.field public final c:I

.field public final d:Z

.field public final e:I

.field public final f:Ljava/lang/String;

.field public final g:Ljava/util/ArrayList;


# direct methods
.method private constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 166
    iput-boolean v1, p0, Lcom/google/android/gms/games/h;->a:Z

    .line 167
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/h;->b:Z

    .line 168
    const/16 v0, 0x11

    iput v0, p0, Lcom/google/android/gms/games/h;->c:I

    .line 169
    iput-boolean v1, p0, Lcom/google/android/gms/games/h;->d:Z

    .line 170
    const/16 v0, 0x1110

    iput v0, p0, Lcom/google/android/gms/games/h;->e:I

    .line 171
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/h;->f:Ljava/lang/String;

    .line 172
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h;->g:Ljava/util/ArrayList;

    .line 173
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0}, Lcom/google/android/gms/games/h;-><init>()V

    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/games/i;)V
    .locals 1

    .prologue
    .line 175
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 176
    iget-boolean v0, p1, Lcom/google/android/gms/games/i;->a:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/h;->a:Z

    .line 177
    iget-boolean v0, p1, Lcom/google/android/gms/games/i;->b:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/h;->b:Z

    .line 178
    iget v0, p1, Lcom/google/android/gms/games/i;->c:I

    iput v0, p0, Lcom/google/android/gms/games/h;->c:I

    .line 179
    iget-boolean v0, p1, Lcom/google/android/gms/games/i;->d:Z

    iput-boolean v0, p0, Lcom/google/android/gms/games/h;->d:Z

    .line 180
    iget v0, p1, Lcom/google/android/gms/games/i;->e:I

    iput v0, p0, Lcom/google/android/gms/games/h;->e:I

    .line 181
    iget-object v0, p1, Lcom/google/android/gms/games/i;->f:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/h;->f:Ljava/lang/String;

    .line 182
    iget-object v0, p1, Lcom/google/android/gms/games/i;->g:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/google/android/gms/games/h;->g:Ljava/util/ArrayList;

    .line 183
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/i;B)V
    .locals 0

    .prologue
    .line 133
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/h;-><init>(Lcom/google/android/gms/games/i;)V

    return-void
.end method

.method public static a()Lcom/google/android/gms/games/i;
    .locals 2

    .prologue
    .line 186
    new-instance v0, Lcom/google/android/gms/games/i;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/i;-><init>(B)V

    return-object v0
.end method
