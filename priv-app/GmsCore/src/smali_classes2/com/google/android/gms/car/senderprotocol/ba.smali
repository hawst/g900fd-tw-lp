.class public final Lcom/google/android/gms/car/senderprotocol/ba;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# static fields
.field private static final e:Landroid/util/SparseIntArray;


# instance fields
.field private final a:Lcom/google/android/gms/car/senderprotocol/bb;

.field private volatile b:Z

.field private final c:Ljava/util/concurrent/Semaphore;

.field private volatile d:Z


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x1

    const/16 v3, 0x15

    .line 61
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0, v3}, Landroid/util/SparseIntArray;-><init>(I)V

    .line 64
    sput-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 65
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v2, v4}, Landroid/util/SparseIntArray;->put(II)V

    .line 66
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v4, v5}, Landroid/util/SparseIntArray;->put(II)V

    .line 67
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, v5, v6}, Landroid/util/SparseIntArray;->put(II)V

    .line 68
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    invoke-virtual {v0, v6, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 69
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x5

    const/4 v2, 0x6

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 70
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x6

    const/4 v2, 0x7

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 72
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/4 v1, 0x7

    const/16 v2, 0x8

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 73
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x8

    const/16 v2, 0x9

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 75
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x9

    const/16 v2, 0xa

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 76
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xc

    const/16 v2, 0xb

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 78
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xd

    const/16 v2, 0xc

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 79
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xb

    const/16 v2, 0xd

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 81
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xe

    const/16 v2, 0x13

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 83
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0xf

    const/16 v2, 0xe

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 85
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x10

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 86
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x11

    invoke-virtual {v0, v1, v3}, Landroid/util/SparseIntArray;->put(II)V

    .line 88
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x12

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 89
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x13

    const/16 v2, 0x11

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 90
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x14

    const/16 v2, 0xf

    invoke-virtual {v0, v1, v2}, Landroid/util/SparseIntArray;->put(II)V

    .line 91
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    const/16 v1, 0x12

    invoke-virtual {v0, v3, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 93
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gn;Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 112
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 57
    iput-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->b:Z

    .line 58
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->c:Ljava/util/concurrent/Semaphore;

    .line 113
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    .line 114
    return-void
.end method

.method private a([Lcom/google/android/c/b/aa;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 297
    array-length v7, p1

    move v6, v4

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 298
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x7

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 300
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    iget v2, v8, Lcom/google/android/c/b/aa;->a:I

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 297
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 303
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/ag;)V
    .locals 12

    .prologue
    .line 462
    array-length v7, p1

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_6

    aget-object v8, p1, v6

    .line 463
    invoke-virtual {v8}, Lcom/google/android/c/b/ag;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {v8}, Lcom/google/android/c/b/ag;->b()I

    move-result v0

    .line 468
    :goto_1
    iget-object v1, v8, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    array-length v1, v1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v9

    .line 469
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0x11

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    mul-int/lit8 v4, v9, 0x4

    add-int/lit8 v4, v4, 0x0

    mul-int/lit8 v5, v9, 0x1

    add-int/lit8 v5, v5, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 472
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v2, 0x0

    iget v3, v8, Lcom/google/android/c/b/ag;->a:I

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    .line 474
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v3, 0x1

    invoke-virtual {v8}, Lcom/google/android/c/b/ag;->c()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v8}, Lcom/google/android/c/b/ag;->b()I

    move-result v1

    :goto_2
    int-to-byte v1, v1

    aput-byte v1, v2, v3

    .line 476
    const/4 v1, 0x0

    move v2, v1

    :goto_3
    if-ge v2, v9, :cond_5

    .line 477
    mul-int/lit8 v1, v2, 0x1

    add-int/lit8 v3, v1, 0x2

    .line 478
    mul-int/lit8 v1, v2, 0x4

    add-int/lit8 v4, v1, 0x0

    .line 479
    iget-object v1, v8, Lcom/google/android/c/b/ag;->b:[Lcom/google/android/c/b/af;

    aget-object v5, v1, v2

    .line 480
    iget-object v10, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    iget-boolean v1, v5, Lcom/google/android/c/b/af;->c:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x1

    :goto_4
    int-to-byte v1, v1

    aput-byte v1, v10, v3

    .line 481
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    add-int/lit8 v3, v4, 0x0

    iget v10, v5, Lcom/google/android/c/b/af;->a:I

    int-to-float v10, v10

    aput v10, v1, v3

    .line 483
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    add-int/lit8 v3, v4, 0x1

    iget v10, v5, Lcom/google/android/c/b/af;->b:I

    int-to-float v10, v10

    const v11, 0x3a83126f    # 0.001f

    mul-float/2addr v10, v11

    aput v10, v1, v3

    .line 485
    iget-object v3, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    add-int/lit8 v10, v4, 0x2

    invoke-virtual {v5}, Lcom/google/android/c/b/af;->c()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-virtual {v5}, Lcom/google/android/c/b/af;->b()I

    move-result v1

    int-to-float v1, v1

    const v11, 0x3a83126f    # 0.001f

    mul-float/2addr v1, v11

    :goto_5
    aput v1, v3, v10

    .line 487
    iget-object v3, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    add-int/lit8 v4, v4, 0x3

    invoke-virtual {v5}, Lcom/google/android/c/b/af;->e()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v5}, Lcom/google/android/c/b/af;->d()I

    move-result v1

    int-to-float v1, v1

    const v5, 0x3a83126f    # 0.001f

    mul-float/2addr v1, v5

    :goto_6
    aput v1, v3, v4

    .line 476
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_3

    .line 463
    :cond_0
    const/4 v0, 0x0

    goto/16 :goto_1

    .line 474
    :cond_1
    const/16 v1, 0xff

    goto :goto_2

    .line 480
    :cond_2
    const/4 v1, 0x0

    goto :goto_4

    .line 485
    :cond_3
    const/high16 v1, 0x7fc00000    # NaNf

    goto :goto_5

    .line 487
    :cond_4
    const/high16 v1, 0x7fc00000    # NaNf

    goto :goto_6

    .line 490
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 462
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto/16 :goto_0

    .line 492
    :cond_6
    return-void
.end method

.method private a([Lcom/google/android/c/b/ah;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/high16 v6, 0x7fc00000    # NaNf

    const v10, 0x3a83126f    # 0.001f

    .line 448
    array-length v8, p1

    move v7, v5

    :goto_0
    if-ge v7, v8, :cond_3

    aget-object v9, p1, v7

    .line 449
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0x12

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 451
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->b()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_1
    aput v1, v2, v5

    .line 453
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x1

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->d()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_2
    aput v1, v2, v3

    .line 455
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x2

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v9}, Lcom/google/android/c/b/ah;->f()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_3
    aput v1, v2, v3

    .line 457
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 448
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    move v1, v6

    .line 451
    goto :goto_1

    :cond_1
    move v1, v6

    .line 453
    goto :goto_2

    :cond_2
    move v1, v6

    .line 455
    goto :goto_3

    .line 459
    :cond_3
    return-void
.end method

.method private a([Lcom/google/android/c/b/ai;)V
    .locals 10

    .prologue
    const v9, 0x3a83126f    # 0.001f

    const/4 v5, 0x0

    .line 276
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 277
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xd

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 279
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v8}, Lcom/google/android/c/b/ai;->b()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    aput v2, v1, v5

    .line 281
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v2, 0x1

    invoke-virtual {v8}, Lcom/google/android/c/b/ai;->c()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v9

    aput v3, v1, v2

    .line 283
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 276
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 285
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/at;)V
    .locals 14

    .prologue
    const/4 v6, 0x3

    const/4 v13, 0x1

    const v12, 0x3a83126f    # 0.001f

    const v11, 0x33d6bf95    # 1.0E-7f

    const/4 v8, 0x0

    .line 384
    array-length v9, p1

    move v7, v8

    :goto_0
    if-ge v7, v9, :cond_3

    aget-object v10, p1, v7

    .line 385
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xa

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x6

    const/16 v5, 0x9

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 387
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v2, v10, Lcom/google/android/c/b/at;->b:I

    int-to-float v2, v2

    mul-float/2addr v2, v11

    aput v2, v1, v8

    .line 390
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v2, v10, Lcom/google/android/c/b/at;->c:I

    int-to-float v2, v2

    mul-float/2addr v2, v11

    aput v2, v1, v13

    .line 394
    invoke-virtual {v10}, Lcom/google/android/c/b/at;->c()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 395
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v2, 0x2

    invoke-virtual {v10}, Lcom/google/android/c/b/at;->b()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v12

    aput v3, v1, v2

    .line 397
    const/4 v1, 0x7

    .line 399
    :goto_1
    invoke-virtual {v10}, Lcom/google/android/c/b/at;->e()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 400
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v10}, Lcom/google/android/c/b/at;->d()I

    move-result v3

    int-to-float v3, v3

    const v4, 0x3c23d70a    # 0.01f

    mul-float/2addr v3, v4

    aput v3, v2, v6

    .line 402
    or-int/lit8 v1, v1, 0x8

    int-to-byte v1, v1

    .line 404
    :cond_0
    invoke-virtual {v10}, Lcom/google/android/c/b/at;->g()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 405
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x4

    invoke-virtual {v10}, Lcom/google/android/c/b/at;->f()I

    move-result v4

    int-to-float v4, v4

    mul-float/2addr v4, v12

    aput v4, v2, v3

    .line 407
    or-int/lit8 v1, v1, 0x10

    int-to-byte v1, v1

    .line 409
    :cond_1
    invoke-virtual {v10}, Lcom/google/android/c/b/at;->i()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 410
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x5

    invoke-virtual {v10}, Lcom/google/android/c/b/at;->h()I

    move-result v4

    int-to-float v4, v4

    const v5, 0x358637bd    # 1.0E-6f

    mul-float/2addr v4, v5

    aput v4, v2, v3

    .line 412
    or-int/lit8 v1, v1, 0x20

    int-to-byte v1, v1

    .line 415
    :cond_2
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aput-byte v1, v2, v8

    .line 416
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    iget v2, v10, Lcom/google/android/c/b/at;->b:I

    invoke-static {v1, v13, v2}, Lcom/google/android/gms/car/CarSensorEvent;->a([BII)V

    .line 418
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v2, 0x5

    iget v3, v10, Lcom/google/android/c/b/at;->c:I

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/car/CarSensorEvent;->a([BII)V

    .line 420
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 384
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto/16 :goto_0

    .line 422
    :cond_3
    return-void

    :cond_4
    move v1, v6

    goto :goto_1
.end method

.method private a([Lcom/google/android/c/b/bu;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 288
    array-length v7, p1

    move v6, v4

    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v8, p1, v6

    .line 289
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0x9

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 291
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    invoke-virtual {v8}, Lcom/google/android/c/b/bu;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    move v1, v5

    :goto_1
    int-to-byte v1, v1

    aput-byte v1, v2, v4

    .line 292
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 288
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    move v1, v4

    .line 291
    goto :goto_1

    .line 294
    :cond_1
    return-void
.end method

.method private a([Lcom/google/android/c/b/bv;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 330
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 331
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x4

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 333
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v2, v8, Lcom/google/android/c/b/bv;->a:I

    int-to-float v2, v2

    const v3, 0x3dcccccd    # 0.1f

    mul-float/2addr v2, v3

    aput v2, v1, v5

    .line 334
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 330
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 336
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/bw;)V
    .locals 9

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 306
    array-length v7, p1

    move v6, v4

    :goto_0
    if-ge v6, v7, :cond_1

    aget-object v8, p1, v6

    .line 307
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x6

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 309
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    iget-boolean v1, v8, Lcom/google/android/c/b/bw;->a:Z

    if-eqz v1, :cond_0

    move v1, v5

    :goto_1
    int-to-byte v1, v1

    aput-byte v1, v2, v4

    .line 310
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 306
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :cond_0
    move v1, v4

    .line 309
    goto :goto_1

    .line 312
    :cond_1
    return-void
.end method

.method private a([Lcom/google/android/c/b/ch;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 339
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 340
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x3

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 342
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v2, v8, Lcom/google/android/c/b/ch;->a:I

    int-to-float v2, v2

    const v3, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v3

    aput v2, v1, v5

    .line 343
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 339
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 345
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/cs;)V
    .locals 9

    .prologue
    const/4 v5, 0x0

    .line 348
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 349
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 351
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v2, v8, Lcom/google/android/c/b/cs;->a:I

    int-to-float v2, v2

    const v3, 0x3a83126f    # 0.001f

    mul-float/2addr v2, v3

    aput v2, v1, v5

    .line 352
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 348
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 354
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/d;)V
    .locals 11

    .prologue
    const/4 v5, 0x0

    const/high16 v6, 0x7fc00000    # NaNf

    const v10, 0x3a83126f    # 0.001f

    .line 434
    array-length v8, p1

    move v7, v5

    :goto_0
    if-ge v7, v8, :cond_3

    aget-object v9, p1, v7

    .line 435
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xe

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 437
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->c()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->b()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_1
    aput v1, v2, v5

    .line 439
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x1

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->d()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_2
    aput v1, v2, v3

    .line 441
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v3, 0x2

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->g()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v9}, Lcom/google/android/c/b/d;->f()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v10

    :goto_3
    aput v1, v2, v3

    .line 443
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 434
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    move v1, v6

    .line 437
    goto :goto_1

    :cond_1
    move v1, v6

    .line 439
    goto :goto_2

    :cond_2
    move v1, v6

    .line 441
    goto :goto_3

    .line 445
    :cond_3
    return-void
.end method

.method private a([Lcom/google/android/c/b/s;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    const/high16 v6, 0x7fc00000    # NaNf

    const/4 v5, 0x0

    const v10, 0x358637bd    # 1.0E-6f

    .line 357
    array-length v8, p1

    move v7, v5

    :goto_0
    if-ge v7, v8, :cond_2

    aget-object v9, p1, v7

    .line 358
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 360
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    iget v3, v9, Lcom/google/android/c/b/s;->a:I

    int-to-float v3, v3

    mul-float/2addr v3, v10

    aput v3, v2, v5

    .line 361
    iget-object v3, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v9}, Lcom/google/android/c/b/s;->c()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-virtual {v9}, Lcom/google/android/c/b/s;->b()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v10

    :goto_1
    aput v2, v3, v1

    .line 363
    iget-object v3, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v4, 0x2

    invoke-virtual {v9}, Lcom/google/android/c/b/s;->e()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {v9}, Lcom/google/android/c/b/s;->d()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v10

    :goto_2
    aput v2, v3, v4

    .line 365
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 357
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    :cond_0
    move v2, v6

    .line 361
    goto :goto_1

    :cond_1
    move v2, v6

    .line 363
    goto :goto_2

    .line 367
    :cond_2
    return-void
.end method

.method private a([Lcom/google/android/c/b/x;)V
    .locals 9

    .prologue
    const/4 v4, 0x0

    .line 425
    array-length v7, p1

    move v6, v4

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 426
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xb

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 428
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    iget v2, v8, Lcom/google/android/c/b/x;->a:I

    int-to-byte v2, v2

    aput-byte v2, v1, v4

    .line 429
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 425
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 431
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/y;)V
    .locals 10

    .prologue
    const v9, 0x3a83126f    # 0.001f

    const/4 v5, 0x0

    .line 264
    array-length v7, p1

    move v6, v5

    :goto_0
    if-ge v6, v7, :cond_0

    aget-object v8, p1, v6

    .line 265
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/16 v1, 0xc

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 267
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v8}, Lcom/google/android/c/b/y;->b()I

    move-result v2

    int-to-float v2, v2

    mul-float/2addr v2, v9

    aput v2, v1, v5

    .line 269
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v2, 0x1

    invoke-virtual {v8}, Lcom/google/android/c/b/y;->c()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v9

    aput v3, v1, v2

    .line 271
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 264
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    .line 273
    :cond_0
    return-void
.end method

.method private a([Lcom/google/android/c/b/z;)V
    .locals 10

    .prologue
    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 315
    array-length v8, p1

    move v7, v6

    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v9, p1, v7

    .line 316
    new-instance v0, Lcom/google/android/gms/car/CarSensorEvent;

    const/4 v1, 0x5

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtimeNanos()J

    move-result-wide v2

    const/4 v4, 0x2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/CarSensorEvent;-><init>(IJII)V

    .line 318
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v9}, Lcom/google/android/c/b/z;->b()I

    move-result v2

    int-to-float v2, v2

    aput v2, v1, v6

    .line 319
    iget-object v1, v0, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    invoke-virtual {v9}, Lcom/google/android/c/b/z;->c()I

    move-result v2

    int-to-float v2, v2

    aput v2, v1, v5

    .line 321
    invoke-virtual {v9}, Lcom/google/android/c/b/z;->e()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v9}, Lcom/google/android/c/b/z;->d()Z

    move-result v1

    if-eqz v1, :cond_1

    move v1, v5

    .line 324
    :goto_1
    iget-object v2, v0, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    aput-byte v1, v2, v6

    .line 325
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a(Lcom/google/android/gms/car/CarSensorEvent;)V

    .line 315
    add-int/lit8 v0, v7, 0x1

    move v7, v0

    goto :goto_0

    .line 327
    :cond_0
    return-void

    :cond_1
    move v1, v6

    goto :goto_1
.end method

.method private declared-synchronized a(IJ)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 135
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->d:Z

    if-nez v1, :cond_0

    .line 136
    const-string v1, "CAR.SENSOR"

    const-string v2, "sendSensorRequest on closed channel"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 162
    :goto_0
    monitor-exit p0

    return v0

    .line 139
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v1

    if-eqz v1, :cond_1

    .line 140
    const-string v1, "CAR.SENSOR"

    const-string v2, "sendSensorRequest wait semaphore available, SensorResponse came later?"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 143
    :cond_1
    const-string v1, "CAR.SENSOR"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 144
    const-string v1, "CAR.SENSOR"

    const-string v2, "sendSensorRequest"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_2
    new-instance v1, Lcom/google/android/c/b/ck;

    invoke-direct {v1}, Lcom/google/android/c/b/ck;-><init>()V

    .line 147
    sget-object v2, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v2, p1}, Landroid/util/SparseIntArray;->get(I)I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    if-nez v2, :cond_3

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Unknown sensor type."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 147
    :cond_3
    :try_start_2
    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/android/c/b/ck;->a:I

    .line 148
    iput-wide p2, v1, Lcom/google/android/c/b/ck;->b:J

    .line 149
    const v2, 0x8001

    invoke-static {v1}, Lcom/google/android/c/b/ck;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v2, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a(I[B)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 151
    :try_start_3
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/ba;->c:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x7d0

    sget-object v4, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 152
    const-string v1, "CAR.SENSOR"

    const-string v2, "sendSensorRequest timed-out"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v1

    goto :goto_0

    .line 159
    :cond_4
    :try_start_4
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 160
    const-string v0, "CAR.SENSOR"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sendSensorRequest retuned "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/car/senderprotocol/ba;->b:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->b:Z
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_0
.end method

.method public static d(I)I
    .locals 2

    .prologue
    .line 182
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p0}, Landroid/util/SparseIntArray;->indexOfValue(I)I

    move-result v0

    .line 183
    if-ltz v0, :cond_0

    .line 184
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/ba;->e:Landroid/util/SparseIntArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseIntArray;->keyAt(I)I

    move-result v0

    .line 186
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 513
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->a()V

    .line 514
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->d:Z

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/bb;->a()V

    .line 516
    return-void
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 506
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->d:Z

    .line 507
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->a:Lcom/google/android/gms/car/senderprotocol/bb;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/bb;->b()V

    .line 508
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/az;->a(I)V

    .line 509
    return-void
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 191
    const v0, 0x8002

    if-ne p1, v0, :cond_3

    .line 192
    new-instance v0, Lcom/google/android/c/b/cl;

    invoke-direct {v0}, Lcom/google/android/c/b/cl;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ba;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/cl;

    .line 194
    if-eqz v0, :cond_1

    .line 195
    const-string v1, "CAR.SENSOR"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.SENSOR"

    const-string v2, "handleSensorResponse"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    iget v0, v0, Lcom/google/android/c/b/cl;->a:I

    if-eqz v0, :cond_2

    const-string v1, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "SensorResponse with error "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->b:Z

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->c:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 210
    :cond_1
    :goto_1
    return-void

    .line 195
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->b:Z

    goto :goto_0

    .line 197
    :cond_3
    const v0, 0x8003

    if-ne p1, v0, :cond_13

    .line 198
    new-instance v0, Lcom/google/android/c/b/ci;

    invoke-direct {v0}, Lcom/google/android/c/b/ci;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ba;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/ci;

    .line 200
    if-eqz v0, :cond_1

    .line 201
    const-string v1, "CAR.SENSOR"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    const-string v1, "CAR.SENSOR"

    const-string v2, "handleSensorBatch"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v1, v0, Lcom/google/android/c/b/ci;->a:[Lcom/google/android/c/b/at;

    array-length v1, v1

    if-lez v1, :cond_5

    iget-object v1, v0, Lcom/google/android/c/b/ci;->a:[Lcom/google/android/c/b/at;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/at;)V

    :cond_5
    iget-object v1, v0, Lcom/google/android/c/b/ci;->b:[Lcom/google/android/c/b/s;

    array-length v1, v1

    if-lez v1, :cond_6

    iget-object v1, v0, Lcom/google/android/c/b/ci;->b:[Lcom/google/android/c/b/s;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/s;)V

    :cond_6
    iget-object v1, v0, Lcom/google/android/c/b/ci;->c:[Lcom/google/android/c/b/cs;

    array-length v1, v1

    if-lez v1, :cond_7

    iget-object v1, v0, Lcom/google/android/c/b/ci;->c:[Lcom/google/android/c/b/cs;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/cs;)V

    :cond_7
    iget-object v1, v0, Lcom/google/android/c/b/ci;->d:[Lcom/google/android/c/b/ch;

    array-length v1, v1

    if-lez v1, :cond_8

    iget-object v1, v0, Lcom/google/android/c/b/ci;->d:[Lcom/google/android/c/b/ch;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/ch;)V

    :cond_8
    iget-object v1, v0, Lcom/google/android/c/b/ci;->e:[Lcom/google/android/c/b/bv;

    array-length v1, v1

    if-lez v1, :cond_9

    iget-object v1, v0, Lcom/google/android/c/b/ci;->e:[Lcom/google/android/c/b/bv;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/bv;)V

    :cond_9
    iget-object v1, v0, Lcom/google/android/c/b/ci;->f:[Lcom/google/android/c/b/z;

    array-length v1, v1

    if-lez v1, :cond_a

    iget-object v1, v0, Lcom/google/android/c/b/ci;->f:[Lcom/google/android/c/b/z;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/z;)V

    :cond_a
    iget-object v1, v0, Lcom/google/android/c/b/ci;->g:[Lcom/google/android/c/b/bw;

    array-length v1, v1

    if-lez v1, :cond_b

    iget-object v1, v0, Lcom/google/android/c/b/ci;->g:[Lcom/google/android/c/b/bw;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/bw;)V

    :cond_b
    iget-object v1, v0, Lcom/google/android/c/b/ci;->h:[Lcom/google/android/c/b/aa;

    array-length v1, v1

    if-lez v1, :cond_c

    iget-object v1, v0, Lcom/google/android/c/b/ci;->h:[Lcom/google/android/c/b/aa;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/aa;)V

    :cond_c
    iget-object v1, v0, Lcom/google/android/c/b/ci;->j:[Lcom/google/android/c/b/bu;

    array-length v1, v1

    if-lez v1, :cond_d

    iget-object v1, v0, Lcom/google/android/c/b/ci;->j:[Lcom/google/android/c/b/bu;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/bu;)V

    :cond_d
    iget-object v1, v0, Lcom/google/android/c/b/ci;->k:[Lcom/google/android/c/b/y;

    array-length v1, v1

    if-lez v1, :cond_e

    iget-object v1, v0, Lcom/google/android/c/b/ci;->k:[Lcom/google/android/c/b/y;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/y;)V

    :cond_e
    iget-object v1, v0, Lcom/google/android/c/b/ci;->l:[Lcom/google/android/c/b/ai;

    array-length v1, v1

    if-lez v1, :cond_f

    iget-object v1, v0, Lcom/google/android/c/b/ci;->l:[Lcom/google/android/c/b/ai;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/ai;)V

    :cond_f
    iget-object v1, v0, Lcom/google/android/c/b/ci;->m:[Lcom/google/android/c/b/x;

    array-length v1, v1

    if-lez v1, :cond_10

    iget-object v1, v0, Lcom/google/android/c/b/ci;->m:[Lcom/google/android/c/b/x;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/x;)V

    :cond_10
    iget-object v1, v0, Lcom/google/android/c/b/ci;->s:[Lcom/google/android/c/b/d;

    array-length v1, v1

    if-lez v1, :cond_11

    iget-object v1, v0, Lcom/google/android/c/b/ci;->s:[Lcom/google/android/c/b/d;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/d;)V

    :cond_11
    iget-object v1, v0, Lcom/google/android/c/b/ci;->t:[Lcom/google/android/c/b/ah;

    array-length v1, v1

    if-lez v1, :cond_12

    iget-object v1, v0, Lcom/google/android/c/b/ci;->t:[Lcom/google/android/c/b/ah;

    invoke-direct {p0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/ah;)V

    :cond_12
    iget-object v1, v0, Lcom/google/android/c/b/ci;->u:[Lcom/google/android/c/b/ag;

    array-length v1, v1

    if-lez v1, :cond_1

    iget-object v0, v0, Lcom/google/android/c/b/ci;->u:[Lcom/google/android/c/b/ag;

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/senderprotocol/ba;->a([Lcom/google/android/c/b/ag;)V

    goto/16 :goto_1

    .line 203
    :cond_13
    const v0, 0x8004

    if-ne p1, v0, :cond_1

    .line 204
    new-instance v0, Lcom/google/android/c/b/cj;

    invoke-direct {v0}, Lcom/google/android/c/b/cj;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/ba;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/cj;

    .line 206
    if-eqz v0, :cond_1

    .line 207
    const-string v1, "CAR.SENSOR"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "sensor error, sensor:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, v0, Lcom/google/android/c/b/cj;->a:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " error code:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/c/b/cj;->b:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_1
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/ba;->d:Z

    return v0
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    .line 117
    const-wide/16 v0, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a(IJ)Z

    move-result v0

    return v0
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 121
    const-wide/16 v0, -0x1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/senderprotocol/ba;->a(IJ)Z

    .line 122
    return-void
.end method
