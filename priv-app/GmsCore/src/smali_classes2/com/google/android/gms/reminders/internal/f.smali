.class public abstract Lcom/google/android/gms/reminders/internal/f;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/reminders/internal/e;


# direct methods
.method public static a(Landroid/os/IBinder;)Lcom/google/android/gms/reminders/internal/e;
    .locals 2

    .prologue
    .line 26
    if-nez p0, :cond_0

    .line 27
    const/4 v0, 0x0

    .line 33
    :goto_0
    return-object v0

    .line 29
    :cond_0
    const-string v0, "com.google.android.gms.reminders.internal.IRemindersListener"

    invoke-interface {p0, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    .line 30
    if-eqz v0, :cond_1

    instance-of v1, v0, Lcom/google/android/gms/reminders/internal/e;

    if-eqz v1, :cond_1

    .line 31
    check-cast v0, Lcom/google/android/gms/reminders/internal/e;

    goto :goto_0

    .line 33
    :cond_1
    new-instance v0, Lcom/google/android/gms/reminders/internal/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/reminders/internal/g;-><init>(Landroid/os/IBinder;)V

    goto :goto_0
.end method


# virtual methods
.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 41
    sparse-switch p1, :sswitch_data_0

    .line 81
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v0

    :goto_0
    return v0

    .line 45
    :sswitch_0
    const-string v0, "com.google.android.gms.reminders.internal.IRemindersListener"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    move v0, v1

    .line 46
    goto :goto_0

    .line 50
    :sswitch_1
    const-string v2, "com.google.android.gms.reminders.internal.IRemindersListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_0

    .line 53
    sget-object v0, Lcom/google/android/gms/reminders/model/TaskEntity;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/reminders/model/TaskEntity;

    .line 58
    :cond_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/f;->a(Lcom/google/android/gms/reminders/model/TaskEntity;)V

    move v0, v1

    .line 59
    goto :goto_0

    .line 63
    :sswitch_2
    const-string v0, "com.google.android.gms.reminders.internal.IRemindersListener"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 64
    invoke-virtual {p0}, Lcom/google/android/gms/reminders/internal/f;->a()V

    move v0, v1

    .line 65
    goto :goto_0

    .line 69
    :sswitch_3
    const-string v2, "com.google.android.gms.reminders.internal.IRemindersListener"

    invoke-virtual {p2, v2}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 71
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v2

    if-eqz v2, :cond_1

    .line 72
    sget-object v0, Lcom/google/android/gms/common/data/DataHolder;->CREATOR:Lcom/google/android/gms/common/data/p;

    invoke-static {p2}, Lcom/google/android/gms/common/data/p;->a(Landroid/os/Parcel;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 77
    :cond_1
    invoke-virtual {p0, v0}, Lcom/google/android/gms/reminders/internal/f;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    move v0, v1

    .line 78
    goto :goto_0

    .line 41
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
