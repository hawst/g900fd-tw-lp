.class final Lcom/google/android/gms/drive/internal/ax;
.super Lcom/google/android/gms/drive/internal/bd;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/am;

.field final synthetic b:Lcom/google/android/gms/drive/internal/av;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/av;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ax;->b:Lcom/google/android/gms/drive/internal/av;

    iput-object p3, p0, Lcom/google/android/gms/drive/internal/ax;->a:Lcom/google/android/gms/drive/am;

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/bd;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 4

    .prologue
    .line 207
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ax;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->h()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->a(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/internal/CreateFolderRequest;

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/ax;->b:Lcom/google/android/gms/drive/internal/av;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/internal/av;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/drive/internal/ax;->a:Lcom/google/android/gms/drive/am;

    invoke-virtual {v3}, Lcom/google/android/gms/drive/am;->e()Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/CreateFolderRequest;-><init>(Lcom/google/android/gms/drive/DriveId;Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;)V

    new-instance v2, Lcom/google/android/gms/drive/internal/az;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/az;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/CreateFolderRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
