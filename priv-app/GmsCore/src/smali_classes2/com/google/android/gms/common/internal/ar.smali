.class public final Lcom/google/android/gms/common/internal/ar;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field final a:Ljava/util/ArrayList;

.field private final b:Lcom/google/android/gms/common/internal/as;

.field private final c:Ljava/util/ArrayList;

.field private d:Z

.field private final e:Ljava/util/ArrayList;

.field private final f:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/as;)V
    .locals 1

    .prologue
    .line 63
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    .line 39
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    .line 47
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    .line 64
    iput-object p2, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    .line 65
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    .line 66
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 2

    .prologue
    .line 111
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 112
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/as;->d_()Landroid/os/Bundle;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/internal/ar;->a(Landroid/os/Bundle;)V

    .line 113
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 176
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 178
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    .line 180
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 182
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/x;

    .line 186
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v3}, Lcom/google/android/gms/common/internal/as;->e_()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 187
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 191
    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/x;->f_(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 198
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 196
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 197
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    .line 198
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 122
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v3

    .line 123
    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    invoke-static {v2}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 126
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    const/4 v4, 0x1

    invoke-virtual {v2, v4}, Landroid/os/Handler;->removeMessages(I)V

    .line 129
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    .line 144
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 146
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 148
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/x;

    .line 152
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v2}, Lcom/google/android/gms/common/internal/as;->e_()Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v2}, Lcom/google/android/gms/common/internal/as;->c_()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 153
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 157
    invoke-interface {v0, p1}, Lcom/google/android/gms/common/api/x;->b_(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 164
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    move v2, v1

    .line 123
    goto :goto_0

    :cond_2
    move v0, v1

    .line 144
    goto :goto_1

    .line 162
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 163
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    .line 164
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/x;)V
    .locals 4

    .prologue
    .line 233
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 234
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 236
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 241
    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "registerConnectionCallbacks(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v0}, Lcom/google/android/gms/common/internal/as;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p1}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    .line 252
    :cond_0
    return-void

    .line 244
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->f:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 211
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 212
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 214
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/et;

    .line 218
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v3}, Lcom/google/android/gms/common/internal/as;->e_()Z

    move-result v3

    if-nez v3, :cond_1

    .line 219
    monitor-exit v1

    .line 226
    :goto_1
    return-void

    .line 222
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 223
    invoke-interface {v0, p1}, Lcom/google/android/gms/common/et;->a(Lcom/google/android/gms/common/c;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 226
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/et;)V
    .locals 4

    .prologue
    .line 286
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 287
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 288
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 293
    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "registerConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " is already registered"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :goto_0
    monitor-exit v1

    return-void

    .line 296
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/common/et;)V
    .locals 4

    .prologue
    .line 316
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 317
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    monitor-enter v1

    .line 318
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->e:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 319
    if-nez v0, :cond_0

    .line 322
    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionFailedListener(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 325
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/common/api/x;)Z
    .locals 2

    .prologue
    .line 258
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 259
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 260
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    .line 261
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lcom/google/android/gms/common/api/x;)V
    .locals 4

    .prologue
    .line 268
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 269
    iget-object v1, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v1

    .line 270
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    .line 271
    if-nez v0, :cond_1

    .line 274
    const-string v0, "GmsClientEvents"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "unregisterConnectionCallbacks(): listener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " not found"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 279
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 276
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/common/internal/ar;->d:Z

    if-eqz v0, :cond_0

    .line 277
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ar;->a:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 279
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final handleMessage(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 330
    iget v0, p1, Landroid/os/Message;->what:I

    if-ne v0, v1, :cond_1

    .line 331
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/common/api/x;

    .line 332
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    monitor-enter v2

    .line 333
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v3}, Lcom/google/android/gms/common/internal/as;->e_()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v3}, Lcom/google/android/gms/common/internal/as;->c_()Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->c:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 335
    iget-object v3, p0, Lcom/google/android/gms/common/internal/ar;->b:Lcom/google/android/gms/common/internal/as;

    invoke-interface {v3}, Lcom/google/android/gms/common/internal/as;->d_()Landroid/os/Bundle;

    move-result-object v3

    .line 336
    invoke-interface {v0, v3}, Lcom/google/android/gms/common/api/x;->b_(Landroid/os/Bundle;)V

    .line 338
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move v0, v1

    .line 344
    :goto_0
    return v0

    .line 338
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 343
    :cond_1
    const-string v0, "GmsClientEvents"

    const-string v1, "Don\'t know how to handle this message."

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 344
    const/4 v0, 0x0

    goto :goto_0
.end method
