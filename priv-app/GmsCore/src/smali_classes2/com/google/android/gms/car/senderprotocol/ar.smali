.class public final Lcom/google/android/gms/car/senderprotocol/ar;
.super Lcom/google/android/gms/car/senderprotocol/ap;
.source "SourceFile"


# instance fields
.field private b:Lcom/google/android/gms/car/senderprotocol/as;

.field private c:Ljava/lang/Boolean;

.field private volatile d:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/nt;Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 33
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/ap;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 30
    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->d:I

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/ar;->b:Lcom/google/android/gms/car/senderprotocol/as;

    .line 35
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->c:Ljava/lang/Boolean;

    .line 36
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/c/b/bk;)V
    .locals 1

    .prologue
    .line 72
    invoke-virtual {p1}, Lcom/google/android/c/b/bk;->b()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 73
    invoke-virtual {p1}, Lcom/google/android/c/b/bk;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->a:I

    .line 76
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->b:Lcom/google/android/gms/car/senderprotocol/as;

    iget v0, p1, Lcom/google/android/c/b/bk;->a:I

    .line 78
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 39
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 40
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->d:I

    .line 41
    new-instance v0, Lcom/google/android/c/b/bj;

    invoke-direct {v0}, Lcom/google/android/c/b/bj;-><init>()V

    .line 42
    iput-boolean v2, v0, Lcom/google/android/c/b/bj;->a:Z

    .line 43
    invoke-virtual {v0, v2}, Lcom/google/android/c/b/bj;->a(Z)Lcom/google/android/c/b/bj;

    .line 44
    invoke-virtual {v0, v2}, Lcom/google/android/c/b/bj;->b(Z)Lcom/google/android/c/b/bj;

    .line 45
    invoke-virtual {v0}, Lcom/google/android/c/b/bj;->a()Lcom/google/android/c/b/bj;

    .line 46
    const v1, 0x8005

    invoke-static {v0}, Lcom/google/android/c/b/bj;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/ar;->a(I[B)V

    .line 47
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->c:Ljava/lang/Boolean;

    .line 48
    const-string v0, "CAR.MIC"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 49
    const-string v0, "CAR.MIC"

    const-string v1, "sent microphone open request"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 56
    :cond_0
    :goto_0
    return-void

    .line 52
    :cond_1
    const-string v0, "CAR.MIC"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    const-string v0, "CAR.MIC"

    const-string v1, "Microphone already open"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected final b(Ljava/nio/ByteBuffer;)V
    .locals 1

    .prologue
    .line 82
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->d:I

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->b:Lcom/google/android/gms/car/senderprotocol/as;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/senderprotocol/as;->a(Ljava/nio/ByteBuffer;)V

    .line 84
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->a:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/senderprotocol/ar;->b(I)V

    .line 85
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->c:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 60
    new-instance v0, Lcom/google/android/c/b/bj;

    invoke-direct {v0}, Lcom/google/android/c/b/bj;-><init>()V

    .line 61
    iput-boolean v2, v0, Lcom/google/android/c/b/bj;->a:Z

    .line 62
    const v1, 0x8005

    invoke-static {v0}, Lcom/google/android/c/b/bj;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/ar;->a(I[B)V

    .line 63
    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/ar;->c:Ljava/lang/Boolean;

    .line 64
    const-string v0, "CAR.MIC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 65
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "sent microphone close request, frames received "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/ar;->d:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 68
    :cond_0
    return-void
.end method
