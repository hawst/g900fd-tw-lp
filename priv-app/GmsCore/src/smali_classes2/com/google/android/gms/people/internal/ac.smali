.class final Lcom/google/android/gms/people/internal/ac;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/aj;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/aj;)V
    .locals 0

    .prologue
    .line 315
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 316
    iput-object p1, p0, Lcom/google/android/gms/people/internal/ac;->a:Lcom/google/android/gms/common/api/aj;

    .line 317
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 320
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ac;->a:Lcom/google/android/gms/common/api/aj;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/common/api/aj;->a:Ljava/lang/Object;

    .line 321
    return-void
.end method

.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    .line 325
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 326
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bundle callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nresolution="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\nbundle="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 329
    :cond_0
    if-eqz p1, :cond_1

    .line 330
    const-string v0, "PeopleClient"

    const-string v1, "Non-success data changed callback received."

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 338
    :goto_0
    return-void

    .line 333
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/internal/ac;->a:Lcom/google/android/gms/common/api/aj;

    new-instance v1, Lcom/google/android/gms/people/internal/r;

    const-string v2, "account"

    invoke-virtual {p3, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "pagegaiaid"

    invoke-virtual {p3, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, "scope"

    invoke-virtual {p3, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/people/internal/r;-><init>(Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/aj;->a(Lcom/google/android/gms/common/api/al;)V

    goto :goto_0
.end method
