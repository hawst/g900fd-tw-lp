.class final Lcom/google/android/gms/car/hw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Thread$UncaughtExceptionHandler;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/gx;

.field private final b:Ljava/lang/Thread$UncaughtExceptionHandler;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gx;Ljava/lang/Thread$UncaughtExceptionHandler;)V
    .locals 0

    .prologue
    .line 2033
    iput-object p1, p0, Lcom/google/android/gms/car/hw;->a:Lcom/google/android/gms/car/gx;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2034
    iput-object p2, p0, Lcom/google/android/gms/car/hw;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    .line 2035
    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/Thread$UncaughtExceptionHandler;
    .locals 1

    .prologue
    .line 2081
    iget-object v0, p0, Lcom/google/android/gms/car/hw;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    return-object v0
.end method

.method public final uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 2061
    const-string v0, "CAR.SERVICE"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 2062
    const-string v0, "CAR.SERVICE"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Uncaught exception: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2065
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/hw;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v0}, Lcom/google/android/gms/car/gx;->J(Lcom/google/android/gms/car/gx;)V

    .line 2066
    invoke-static {}, Lcom/google/android/gms/car/gx;->M()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.SERVICE"

    const-string v1, "Thread crash called again!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    .line 2075
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/hw;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    if-eqz v0, :cond_1

    .line 2076
    iget-object v0, p0, Lcom/google/android/gms/car/hw;->b:Ljava/lang/Thread$UncaughtExceptionHandler;

    invoke-interface {v0, p1, p2}, Ljava/lang/Thread$UncaughtExceptionHandler;->uncaughtException(Ljava/lang/Thread;Ljava/lang/Throwable;)V

    .line 2078
    :cond_1
    return-void

    .line 2066
    :cond_2
    :try_start_1
    invoke-static {}, Lcom/google/android/gms/car/gx;->N()Z

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "FATAL EXCEPTION in GmsCore thread: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\nPID: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-static {}, Landroid/os/Process;->myPid()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CAR.SERVICE"

    invoke-static {v1, v0, p2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iget-object v0, p0, Lcom/google/android/gms/car/hw;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->F()Landroid/content/Context;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/hw;->a:Lcom/google/android/gms/car/gx;

    invoke-virtual {v1}, Lcom/google/android/gms/car/gx;->F()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p2}, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0

    .line 2067
    :catch_0
    move-exception v0

    .line 2069
    :try_start_2
    const-string v1, "CAR.SERVICE"

    const-string v2, "Error reporting crash"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0
.end method
