.class final Lcom/google/android/gms/car/dm;
.super Landroid/telecom/Call$Listener;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/dj;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/dj;)V
    .locals 0

    .prologue
    .line 649
    iput-object p1, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-direct {p0}, Landroid/telecom/Call$Listener;-><init>()V

    return-void
.end method


# virtual methods
.method public final onCallDestroyed(Landroid/telecom/Call;)V
    .locals 5

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 807
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 808
    if-nez v0, :cond_0

    .line 809
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 810
    monitor-exit v1

    .line 822
    :goto_0
    return-void

    .line 812
    :cond_0
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 813
    const-string v2, "CAR.TEL.CarCallService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onCallDestroyed ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ")"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 815
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_2

    .line 817
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0}, Lcom/google/android/gms/car/le;->c(Lcom/google/android/gms/car/CarCall;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 822
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 818
    :catch_0
    move-exception v0

    .line 819
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final onCannedTextResponsesLoaded(Landroid/telecom/Call;Ljava/util/List;)V
    .locals 4

    .prologue
    .line 766
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 767
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 768
    if-nez v0, :cond_1

    .line 769
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 770
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 772
    :cond_0
    monitor-exit v1

    .line 782
    :goto_0
    return-void

    .line 774
    :cond_1
    iput-object p2, v0, Lcom/google/android/gms/car/CarCall;->d:Ljava/util/List;

    .line 775
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_2

    .line 777
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/car/le;->b(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 782
    :cond_2
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 778
    :catch_0
    move-exception v0

    .line 779
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final onChildrenChanged(Landroid/telecom/Call;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 701
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 702
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v2

    .line 703
    if-nez v2, :cond_0

    .line 704
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    monitor-exit v1

    .line 729
    :goto_0
    return-void

    .line 707
    :cond_0
    invoke-interface {p2}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    iput-boolean v0, v2, Lcom/google/android/gms/car/CarCall;->h:Z

    .line 709
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_3

    .line 711
    :try_start_1
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 712
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    .line 713
    invoke-static {v0}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v5

    .line 715
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v6, 0x3

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 716
    const-string v6, "CAR.TEL.CarCallService"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v7, "onChildrenChanged ("

    invoke-direct {v0, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v2, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v7, ") :"

    invoke-virtual {v0, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    if-nez v5, :cond_5

    const-string v0, "null"

    :goto_3
    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v6, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 720
    :cond_2
    if-eqz v5, :cond_1

    .line 721
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 725
    :catch_0
    move-exception v0

    .line 726
    :try_start_2
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 729
    :cond_3
    :goto_4
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 707
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 716
    :cond_5
    :try_start_3
    iget v0, v5, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_3

    .line 724
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_4
.end method

.method public final onConferenceableCallsChanged(Landroid/telecom/Call;Ljava/util/List;)V
    .locals 8

    .prologue
    .line 827
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 828
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_3

    .line 830
    :try_start_1
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v2

    .line 831
    if-nez v2, :cond_0

    .line 832
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 833
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 854
    :goto_0
    return-void

    .line 835
    :cond_0
    :try_start_3
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 837
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    .line 838
    invoke-static {v0}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 840
    const-string v5, "CAR.TEL.CarCallService"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_2

    .line 841
    const-string v5, "CAR.TEL.CarCallService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "onConferenceableCallsChanged ("

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v7, v2, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ") : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v0, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 845
    :cond_2
    if-eqz v0, :cond_1

    .line 846
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1

    .line 850
    :catch_0
    move-exception v0

    .line 851
    :try_start_4
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 854
    :cond_3
    :goto_2
    monitor-exit v1
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 849
    :cond_4
    :try_start_5
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/car/le;->c(Lcom/google/android/gms/car/CarCall;Ljava/util/List;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto :goto_2
.end method

.method public final onDetailsChanged(Landroid/telecom/Call;Landroid/telecom/Call$Details;)V
    .locals 11

    .prologue
    const/4 v7, 0x0

    .line 734
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v8

    monitor-enter v8

    .line 735
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v9

    .line 736
    if-nez v9, :cond_0

    .line 737
    const-string v0, "CAR.TEL.CarCallService"

    const-string v1, "Unable to update call. There is no record of it!"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 738
    monitor-exit v8

    .line 761
    :goto_0
    return-void

    .line 740
    :cond_0
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 741
    const-string v0, "CAR.TEL.CarCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDetailsChanged ("

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v9, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") :"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 743
    :cond_1
    invoke-virtual {p2}, Landroid/telecom/Call$Details;->getGatewayInfo()Landroid/telecom/GatewayInfo;

    move-result-object v10

    .line 744
    invoke-virtual {p2}, Landroid/telecom/Call$Details;->getDisconnectCause()Landroid/telecom/DisconnectCause;

    move-result-object v0

    .line 745
    if-nez v0, :cond_3

    move-object v3, v7

    .line 746
    :goto_1
    new-instance v0, Lcom/google/android/gms/car/CarCall$Details;

    invoke-virtual {p2}, Landroid/telecom/Call$Details;->getHandle()Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {p2}, Landroid/telecom/Call$Details;->getCallerDisplayName()Ljava/lang/String;

    move-result-object v2

    if-nez v3, :cond_4

    move-object v3, v7

    :goto_2
    invoke-virtual {p2}, Landroid/telecom/Call$Details;->getConnectTimeMillis()J

    move-result-wide v4

    if-nez v10, :cond_5

    move-object v6, v7

    :goto_3
    if-nez v10, :cond_6

    :goto_4
    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/car/CarCall$Details;-><init>(Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;JLandroid/net/Uri;Landroid/net/Uri;)V

    iput-object v0, v9, Lcom/google/android/gms/car/CarCall;->g:Lcom/google/android/gms/car/CarCall$Details;

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_2

    .line 756
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    iget-object v1, v9, Lcom/google/android/gms/car/CarCall;->g:Lcom/google/android/gms/car/CarCall$Details;

    invoke-interface {v0, v9, v1}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall$Details;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 761
    :cond_2
    :goto_5
    :try_start_2
    monitor-exit v8
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 745
    :cond_3
    :try_start_3
    invoke-virtual {v0}, Landroid/telecom/DisconnectCause;->getLabel()Ljava/lang/CharSequence;

    move-result-object v0

    move-object v3, v0

    goto :goto_1

    .line 746
    :cond_4
    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2

    :cond_5
    invoke-virtual {v10}, Landroid/telecom/GatewayInfo;->getOriginalAddress()Landroid/net/Uri;

    move-result-object v6

    goto :goto_3

    :cond_6
    invoke-virtual {v10}, Landroid/telecom/GatewayInfo;->getGatewayAddress()Landroid/net/Uri;

    move-result-object v7

    goto :goto_4

    .line 757
    :catch_0
    move-exception v0

    .line 758
    const-string v1, "CAR.TEL.CarCallService"

    const-string v2, "RemoteException in CarCallListener."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_5
.end method

.method public final onParentChanged(Landroid/telecom/Call;Landroid/telecom/Call;)V
    .locals 6

    .prologue
    .line 677
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 678
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v2

    .line 679
    if-nez v2, :cond_0

    .line 680
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    monitor-exit v1

    .line 696
    :goto_0
    return-void

    .line 683
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v3

    .line 684
    iput-object v3, v2, Lcom/google/android/gms/car/CarCall;->c:Lcom/google/android/gms/car/CarCall;

    .line 685
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    if-eqz v0, :cond_2

    .line 687
    :try_start_1
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v4, 0x3

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 688
    const-string v4, "CAR.TEL.CarCallService"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v5, "onParentChanged ("

    invoke-direct {v0, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v2, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ") :"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    if-nez v3, :cond_3

    const-string v0, "null"

    :goto_1
    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v4, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 691
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 696
    :cond_2
    :goto_2
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 688
    :cond_3
    :try_start_3
    iget v0, v3, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v0

    goto :goto_1

    .line 692
    :catch_0
    move-exception v0

    .line 693
    :try_start_4
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2
.end method

.method public final onPostDialWait(Landroid/telecom/Call;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 787
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 788
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 789
    if-nez v0, :cond_0

    .line 790
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 791
    monitor-exit v1

    .line 801
    :goto_0
    return-void

    .line 793
    :cond_0
    iput-object p2, v0, Lcom/google/android/gms/car/CarCall;->e:Ljava/lang/String;

    .line 794
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_1

    .line 796
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 801
    :cond_1
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 797
    :catch_0
    move-exception v0

    .line 798
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final onStateChanged(Landroid/telecom/Call;I)V
    .locals 5

    .prologue
    .line 652
    iget-object v0, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 653
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    move-result-object v0

    .line 654
    if-nez v0, :cond_0

    .line 655
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Unable to update call. There is no record of it!"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    monitor-exit v1

    .line 672
    :goto_0
    return-void

    .line 658
    :cond_0
    iput p2, v0, Lcom/google/android/gms/car/CarCall;->f:I

    .line 659
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 660
    const-string v2, "CAR.TEL.CarCallService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "onStateChanged ("

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, v0, Lcom/google/android/gms/car/CarCall;->b:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 662
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    if-eqz v2, :cond_3

    .line 664
    :try_start_1
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 665
    const-string v2, "CAR.TEL.CarCallService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Notifying listener of onStateChanged: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 667
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/car/dm;->a:Lcom/google/android/gms/car/dj;

    invoke-static {v2}, Lcom/google/android/gms/car/dj;->a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v2, v0, p2}, Lcom/google/android/gms/car/le;->a(Lcom/google/android/gms/car/CarCall;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 672
    :cond_3
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 668
    :catch_0
    move-exception v0

    .line 669
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException in CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method
