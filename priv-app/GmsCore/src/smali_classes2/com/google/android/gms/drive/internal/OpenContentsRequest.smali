.class public Lcom/google/android/gms/drive/internal/OpenContentsRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/DriveId;

.field final c:I

.field final d:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/drive/internal/cz;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/cz;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/DriveId;II)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->a:I

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b:Lcom/google/android/gms/drive/DriveId;

    .line 64
    iput p3, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->c:I

    .line 65
    iput p4, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->d:I

    .line 66
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/DriveId;I)V
    .locals 2

    .prologue
    .line 70
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, p2, v1}, Lcom/google/android/gms/drive/internal/OpenContentsRequest;-><init>(ILcom/google/android/gms/drive/DriveId;II)V

    .line 71
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->b:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 78
    iget v0, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->c:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/google/android/gms/drive/internal/OpenContentsRequest;->d:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 43
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/cz;->a(Lcom/google/android/gms/drive/internal/OpenContentsRequest;Landroid/os/Parcel;I)V

    .line 49
    return-void
.end method
