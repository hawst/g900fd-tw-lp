.class final Lcom/google/android/gms/common/api/ad;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/api/i;

.field final synthetic b:Lcom/google/android/gms/common/api/z;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/z;Lcom/google/android/gms/common/api/i;)V
    .locals 0

    .prologue
    .line 427
    iput-object p1, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iput-object p2, p0, Lcom/google/android/gms/common/api/ad;->a:Lcom/google/android/gms/common/api/i;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 433
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 448
    :goto_0
    return-void

    .line 438
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/i;->a()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget v1, v1, Lcom/google/android/gms/common/api/z;->e:I

    if-ge v0, v1, :cond_2

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iput-object p1, v0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    .line 442
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v1, p0, Lcom/google/android/gms/common/api/ad;->a:Lcom/google/android/gms/common/api/i;

    invoke-interface {v1}, Lcom/google/android/gms/common/api/i;->a()I

    move-result v1

    iput v1, v0, Lcom/google/android/gms/common/api/z;->e:I

    .line 445
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    invoke-static {v0}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/z;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 447
    iget-object v0, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v0, v0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/ad;->b:Lcom/google/android/gms/common/api/z;

    iget-object v1, v1, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
