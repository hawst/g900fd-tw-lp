.class public final Lcom/google/android/gms/car/no;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field private a:Landroid/os/Looper;

.field private final b:Ljava/util/concurrent/Semaphore;

.field private final c:I


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 19
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 15
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/no;->b:Ljava/util/concurrent/Semaphore;

    .line 20
    iput v1, p0, Lcom/google/android/gms/car/no;->c:I

    .line 21
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 24
    invoke-direct {p0, p1}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 15
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/no;->b:Ljava/util/concurrent/Semaphore;

    .line 25
    iput p2, p0, Lcom/google/android/gms/car/no;->c:I

    .line 26
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/gms/car/no;->a:Landroid/os/Looper;

    invoke-virtual {v0}, Landroid/os/Looper;->quit()V

    .line 42
    return-void
.end method

.method public final a(Ljava/lang/Runnable;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/car/no;->a:Landroid/os/Looper;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 59
    return-void
.end method

.method public final b()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/no;->a:Landroid/os/Looper;

    return-object v0
.end method

.method public final run()V
    .locals 1

    .prologue
    .line 46
    iget v0, p0, Lcom/google/android/gms/car/no;->c:I

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 47
    invoke-static {}, Landroid/os/Looper;->prepare()V

    .line 48
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/no;->a:Landroid/os/Looper;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/car/no;->b:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 50
    invoke-static {}, Landroid/os/Looper;->loop()V

    .line 51
    return-void
.end method

.method public final start()V
    .locals 4

    .prologue
    .line 30
    invoke-super {p0}, Ljava/lang/Thread;->start()V

    .line 32
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/no;->b:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x5

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 33
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "LooperThread init timeout"

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 35
    :catch_0
    move-exception v0

    .line 36
    new-instance v1, Ljava/lang/RuntimeException;

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 37
    :cond_0
    return-void
.end method
