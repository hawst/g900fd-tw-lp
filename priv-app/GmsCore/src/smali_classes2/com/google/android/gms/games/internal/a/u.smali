.class public final Lcom/google/android/gms/games/internal/a/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/multiplayer/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 71
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3, p4}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/games/internal/a/w;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, p2, v1}, Lcom/google/android/gms/games/internal/a/w;-><init>(Lcom/google/android/gms/games/internal/a/u;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 84
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->n()V

    .line 85
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/g;)V
    .locals 2

    .prologue
    .line 78
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 79
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/aj;)V

    .line 80
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/g;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 132
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 133
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V

    .line 135
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 95
    new-instance v0, Lcom/google/android/gms/games/internal/a/v;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/v;-><init>(Lcom/google/android/gms/games/internal/a/u;Lcom/google/android/gms/common/api/v;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 139
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->c(Ljava/lang/String;)V

    .line 140
    return-void
.end method
