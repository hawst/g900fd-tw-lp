.class public Lcom/google/android/gms/backup/BackupTransportService;
.super Landroid/app/Service;
.source "SourceFile"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x15
.end annotation


# static fields
.field private static final a:Z

.field private static final b:Ljava/lang/String;


# instance fields
.field private c:Landroid/content/SharedPreferences;

.field private d:Lcom/google/android/gms/http/GoogleHttpClient;

.field private e:Lcom/android/a/b;

.field private f:Lcom/android/a/a;

.field private g:Lcom/android/a/a;

.field private h:Lcom/android/a/a;

.field private i:Ljava/lang/Object;

.field private j:Landroid/app/backup/BackupManager;

.field private k:Lcom/google/android/gms/backup/aj;

.field private l:I

.field private final m:Landroid/app/backup/BackupTransport;

.field private final n:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 100
    const-string v0, "GmsBackupTransport"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/backup/BackupTransportService;->a:Z

    .line 107
    const-string v0, "Gms-Backup/%s"

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    const v3, 0x6768a8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/backup/BackupTransportService;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 98
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 159
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 160
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    .line 161
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->f:Lcom/android/a/a;

    .line 162
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->g:Lcom/android/a/a;

    .line 163
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->h:Lcom/android/a/a;

    .line 164
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->i:Ljava/lang/Object;

    .line 165
    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->j:Landroid/app/backup/BackupManager;

    .line 169
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->l:I

    .line 192
    new-instance v0, Lcom/google/android/gms/backup/z;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/z;-><init>(Lcom/google/android/gms/backup/BackupTransportService;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->m:Landroid/app/backup/BackupTransport;

    .line 1209
    new-instance v0, Lcom/google/android/gms/backup/ab;

    invoke-direct {v0, p0}, Lcom/google/android/gms/backup/ab;-><init>(Lcom/google/android/gms/backup/BackupTransportService;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/backup/BackupTransportService;I)I
    .locals 0

    .prologue
    .line 98
    iput p1, p0, Lcom/google/android/gms/backup/BackupTransportService;->l:I

    return p1
.end method

.method static synthetic a(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportService;->c()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method private a(Landroid/content/pm/PackageInfo;Lcom/android/a/a;)Lcom/google/android/gms/backup/i;
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 1460
    new-instance v1, Lcom/google/android/gms/backup/i;

    invoke-direct {v1}, Lcom/google/android/gms/backup/i;-><init>()V

    .line 1461
    const/4 v0, 0x3

    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/i;->b(I)Lcom/google/android/gms/backup/i;

    .line 1463
    new-instance v0, Lcom/google/android/gms/backup/j;

    invoke-direct {v0}, Lcom/google/android/gms/backup/j;-><init>()V

    .line 1464
    iget-object v2, p1, Landroid/content/pm/PackageInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/backup/j;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/j;

    .line 1465
    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    .line 1468
    :cond_0
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportService;->c()Landroid/accounts/Account;

    move-result-object v0

    .line 1479
    if-nez v0, :cond_1

    .line 1480
    :try_start_0
    const-string v0, "GmsBackupTransport"

    const-string v2, "Missing backup account"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1481
    new-instance v0, Lcom/google/android/gms/backup/ae;

    const-string v2, "Backup account is not defined"

    invoke-direct {v0, v2}, Lcom/google/android/gms/backup/ae;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/backup/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 1501
    :catch_0
    move-exception v0

    .line 1502
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    add-long/2addr v2, v4

    .line 1503
    invoke-virtual {p2, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1504
    const-string v1, "GmsBackupTransport"

    const-string v2, "Backup account missing, trying again later"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1505
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Can\'t get Backup account"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1506
    invoke-virtual {v1, v0}, Ljava/lang/Exception;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1507
    throw v1

    .line 1484
    :cond_1
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "android_id"

    const-wide/16 v4, 0x0

    invoke-static {v2, v3, v4, v5}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v2

    .line 1485
    cmp-long v4, v2, v8

    if-nez v4, :cond_2

    .line 1486
    const-string v0, "GmsBackupTransport"

    const-string v2, "Missing android ID"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1487
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v2, "No Android ID available"

    invoke-direct {v0, v2}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/android/gms/backup/ae; {:try_start_1 .. :try_end_1} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 1508
    :catch_1
    move-exception v0

    .line 1509
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    .line 1510
    invoke-virtual {p2, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1511
    new-instance v1, Ljava/lang/Exception;

    const-string v2, "Can\'t get credentials"

    invoke-direct {v1, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    .line 1512
    invoke-virtual {v1, v0}, Ljava/lang/Exception;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1513
    throw v1

    .line 1489
    :cond_2
    :try_start_2
    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/backup/i;->a(J)Lcom/google/android/gms/backup/i;

    .line 1491
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 1493
    const-string v3, "android"

    const/4 v4, 0x1

    invoke-virtual {v2, v0, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 1494
    if-nez v0, :cond_3

    .line 1495
    const-string v0, "GmsBackupTransport"

    const-string v2, "Fail to get auth token"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1496
    new-instance v0, Landroid/accounts/AccountsException;

    const-string v2, "No auth token available"

    invoke-direct {v0, v2}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catch Lcom/google/android/gms/backup/ae; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_2

    .line 1514
    :catch_2
    move-exception v0

    .line 1515
    invoke-virtual {p2}, Lcom/android/a/a;->b()V

    .line 1516
    iget-object v2, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    invoke-virtual {p2, v2}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v2

    .line 1517
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    .line 1518
    const-wide/16 v4, 0x7530

    cmp-long v4, v2, v4

    if-lez v4, :cond_4

    .line 1519
    throw v0

    .line 1498
    :cond_3
    :try_start_3
    invoke-virtual {v1, v0}, Lcom/google/android/gms/backup/i;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/i;
    :try_end_3
    .catch Lcom/google/android/gms/backup/ae; {:try_start_3 .. :try_end_3} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    .line 1500
    return-object v1

    .line 1521
    :cond_4
    const-string v4, "GmsBackupTransport"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Network error - waiting "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "ms to retry: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 1524
    cmp-long v0, v2, v8

    if-lez v0, :cond_0

    .line 1525
    :try_start_4
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    goto/16 :goto_0

    .line 1527
    :catch_3
    move-exception v0

    .line 1528
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
.end method

.method static synthetic a(Lcom/google/android/gms/backup/BackupTransportService;Landroid/content/pm/PackageInfo;Lcom/android/a/a;)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Landroid/content/pm/PackageInfo;Lcom/android/a/a;)Lcom/google/android/gms/backup/i;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/backup/BackupTransportService;Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/backup/BackupTransportService;->a(Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/backup/i;Lcom/android/a/a;)Lcom/google/android/gms/backup/u;
    .locals 8

    .prologue
    .line 1332
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->m:Landroid/app/backup/BackupTransport;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "lock not held"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1333
    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    .line 1334
    const/4 v0, 0x0

    .line 1337
    :cond_1
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportService;->c()Landroid/accounts/Account;

    move-result-object v1

    .line 1340
    iget-object v3, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    invoke-virtual {p2, v3}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 1341
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Not ready to send network request: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1345
    :cond_2
    if-nez v1, :cond_4

    .line 1346
    :try_start_0
    new-instance v1, Lcom/google/android/gms/backup/ae;

    const-string v3, "Backup account is not defined"

    invoke-direct {v1, v3}, Lcom/google/android/gms/backup/ae;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Lcom/google/android/gms/backup/ae; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_0 .. :try_end_0} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_0 .. :try_end_0} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1399
    :catch_0
    move-exception v0

    .line 1400
    :try_start_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x240c8400

    add-long/2addr v2, v4

    .line 1401
    invoke-virtual {p2, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1402
    const-string v1, "GmsBackupTransport"

    const-string v2, "Backup account missing, trying again later"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1403
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Can\'t get Backup account"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1404
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1405
    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1446
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    :cond_3
    throw v0

    .line 1348
    :cond_4
    :try_start_2
    iget-boolean v3, p1, Lcom/google/android/gms/backup/i;->c:Z

    if-nez v3, :cond_6

    .line 1350
    const-string v3, "android"

    const/4 v4, 0x1

    invoke-virtual {v2, v1, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v1

    .line 1351
    if-nez v1, :cond_5

    .line 1352
    new-instance v1, Landroid/accounts/AccountsException;

    const-string v3, "No auth token available"

    invoke-direct {v1, v3}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_2
    .catch Lcom/google/android/gms/backup/ae; {:try_start_2 .. :try_end_2} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_2 .. :try_end_2} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_2 .. :try_end_2} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_5
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 1406
    :catch_1
    move-exception v0

    .line 1407
    :try_start_3
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    .line 1408
    invoke-virtual {p2, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1409
    new-instance v1, Ljava/io/IOException;

    const-string v2, "Can\'t get credentials"

    invoke-direct {v1, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1410
    invoke-virtual {v1, v0}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1411
    throw v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1353
    :cond_5
    :try_start_4
    invoke-virtual {p1, v1}, Lcom/google/android/gms/backup/i;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/i;

    .line 1356
    :cond_6
    iget-boolean v1, p1, Lcom/google/android/gms/backup/i;->a:Z

    if-nez v1, :cond_9

    .line 1357
    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v3, "android_id"

    const-wide/16 v4, 0x0

    invoke-static {v1, v3, v4, v5}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;J)J

    move-result-wide v4

    .line 1358
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-nez v1, :cond_8

    new-instance v1, Landroid/accounts/AccountsException;

    const-string v3, "No Android ID available"

    invoke-direct {v1, v3}, Landroid/accounts/AccountsException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_4
    .catch Lcom/google/android/gms/backup/ae; {:try_start_4 .. :try_end_4} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_4 .. :try_end_4} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_4 .. :try_end_4} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_4 .. :try_end_4} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_5
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 1412
    :catch_2
    move-exception v1

    .line 1413
    :try_start_5
    iget-boolean v3, p1, Lcom/google/android/gms/backup/i;->c:Z

    if-eqz v3, :cond_7

    .line 1414
    iget-object v3, p1, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    .line 1415
    const-string v4, "com.google"

    invoke-virtual {v2, v4, v3}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 1416
    const/4 v3, 0x0

    iput-boolean v3, p1, Lcom/google/android/gms/backup/i;->c:Z

    const-string v3, ""

    iput-object v3, p1, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    .line 1419
    :cond_7
    if-eqz v0, :cond_12

    .line 1420
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const-wide/32 v4, 0x2932e00

    add-long/2addr v2, v4

    .line 1421
    invoke-virtual {p2, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1422
    new-instance v0, Ljava/io/IOException;

    const-string v2, "Repeated authentication failure"

    invoke-direct {v0, v2}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    .line 1423
    invoke-virtual {v0, v1}, Ljava/io/IOException;->initCause(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    .line 1424
    throw v0
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 1359
    :cond_8
    :try_start_6
    invoke-virtual {p1, v4, v5}, Lcom/google/android/gms/backup/i;->a(J)Lcom/google/android/gms/backup/i;

    .line 1362
    :cond_9
    invoke-virtual {p1}, Lcom/google/android/gms/backup/i;->g()[B

    move-result-object v1

    .line 1363
    sget-boolean v3, Lcom/google/android/gms/backup/BackupTransportService;->a:Z

    if-eqz v3, :cond_a

    const-string v3, "GmsBackupTransport"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "sending request: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v5, v1

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " bytes"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1365
    :cond_a
    new-instance v3, Lcom/google/android/gms/http/GoogleHttpClient;

    sget-object v4, Lcom/google/android/gms/backup/BackupTransportService;->b:Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {v3, p0, v4, v5}, Lcom/google/android/gms/http/GoogleHttpClient;-><init>(Landroid/content/Context;Ljava/lang/String;Z)V

    iput-object v3, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    .line 1366
    sget-boolean v3, Lcom/google/android/gms/backup/BackupTransportService;->a:Z

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    const-string v4, "GmsBackupTransport"

    const/4 v5, 0x3

    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/http/GoogleHttpClient;->enableCurlLogging(Ljava/lang/String;I)V

    .line 1369
    :cond_b
    new-instance v3, Lorg/apache/http/client/methods/HttpPost;

    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupTransportService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "backup_server_url"

    const-string v6, "https://android.googleapis.com/backup"

    invoke-static {v4, v5, v6}, Lcom/google/android/gsf/f;->a(Landroid/content/ContentResolver;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Lorg/apache/http/client/methods/HttpPost;-><init>(Ljava/lang/String;)V

    .line 1371
    const-string v4, "Content-Type"

    const-string v5, "application/octet-stream"

    invoke-virtual {v3, v4, v5}, Lorg/apache/http/client/methods/HttpPost;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    new-instance v4, Lorg/apache/http/entity/ByteArrayEntity;

    invoke-direct {v4, v1}, Lorg/apache/http/entity/ByteArrayEntity;-><init>([B)V

    invoke-virtual {v3, v4}, Lorg/apache/http/client/methods/HttpPost;->setEntity(Lorg/apache/http/HttpEntity;)V

    .line 1374
    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/http/GoogleHttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v1

    .line 1375
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v3

    .line 1376
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0x1f7

    if-ne v4, v5, :cond_c

    .line 1377
    const-string v4, "Retry-After"

    invoke-interface {v1, v4}, Lorg/apache/http/HttpResponse;->getFirstHeader(Ljava/lang/String;)Lorg/apache/http/Header;

    move-result-object v4

    .line 1378
    if-eqz v4, :cond_c

    .line 1379
    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {p2, v5}, Lcom/android/a/a;->a(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 1380
    sget-boolean v5, Lcom/google/android/gms/backup/BackupTransportService;->a:Z

    if-eqz v5, :cond_c

    const-string v5, "GmsBackupTransport"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "got 503 Retry-After: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1387
    :cond_c
    :goto_1
    const-string v4, "GmsBackupTransport"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Http Response Code : "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v6

    invoke-interface {v6}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1389
    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v4

    invoke-interface {v4}, Lorg/apache/http/StatusLine;->getStatusCode()I

    move-result v4

    const/16 v5, 0xc8

    if-eq v4, v5, :cond_f

    .line 1390
    if-eqz v3, :cond_d

    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->consumeContent()V

    .line 1391
    :cond_d
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Server rejected backup: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v1

    invoke-static {v1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v3, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_6
    .catch Lcom/google/android/gms/backup/ae; {:try_start_6 .. :try_end_6} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_6 .. :try_end_6} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_6 .. :try_end_6} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_6 .. :try_end_6} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_5
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 1429
    :catch_3
    move-exception v0

    :try_start_7
    throw v0
    :try_end_7
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    .line 1382
    :cond_e
    :try_start_8
    const-string v5, "GmsBackupTransport"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Invalid Retry-After date: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v5, v4}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_8
    .catch Lcom/google/android/gms/backup/ae; {:try_start_8 .. :try_end_8} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_8 .. :try_end_8} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_8 .. :try_end_8} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_8 .. :try_end_8} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto :goto_1

    .line 1431
    :catch_4
    move-exception v0

    :try_start_9
    throw v0
    :try_end_9
    .catchall {:try_start_9 .. :try_end_9} :catchall_0

    .line 1395
    :cond_f
    if-nez v3, :cond_10

    :try_start_a
    new-instance v1, Ljava/io/IOException;

    const-string v3, "Missing response body"

    invoke-direct {v1, v3}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_a
    .catch Lcom/google/android/gms/backup/ae; {:try_start_a .. :try_end_a} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_a .. :try_end_a} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_a .. :try_end_a} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_a .. :try_end_a} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_a .. :try_end_a} :catch_4
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_5
    .catchall {:try_start_a .. :try_end_a} :catchall_0

    .line 1433
    :catch_5
    move-exception v1

    .line 1434
    :try_start_b
    invoke-virtual {p2}, Lcom/android/a/a;->b()V

    .line 1435
    iget-object v3, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    invoke-virtual {p2, v3}, Lcom/android/a/a;->a(Lcom/android/a/b;)J

    move-result-wide v4

    .line 1436
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    sub-long/2addr v4, v6

    .line 1437
    const-wide/16 v6, 0x7530

    cmp-long v3, v4, v6

    if-lez v3, :cond_13

    throw v1
    :try_end_b
    .catchall {:try_start_b .. :try_end_b} :catchall_0

    .line 1396
    :cond_10
    :try_start_c
    invoke-interface {v3}, Lorg/apache/http/HttpEntity;->getContent()Ljava/io/InputStream;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/backup/p;->a(Ljava/io/InputStream;)Lcom/google/android/gms/backup/u;

    move-result-object v1

    .line 1397
    invoke-virtual {p2}, Lcom/android/a/a;->a()V
    :try_end_c
    .catch Lcom/google/android/gms/backup/ae; {:try_start_c .. :try_end_c} :catch_0
    .catch Landroid/accounts/AccountsException; {:try_start_c .. :try_end_c} :catch_1
    .catch Lcom/google/android/gms/backup/r; {:try_start_c .. :try_end_c} :catch_2
    .catch Lcom/google/android/gms/backup/t; {:try_start_c .. :try_end_c} :catch_3
    .catch Lcom/google/android/gms/backup/s; {:try_start_c .. :try_end_c} :catch_4
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_5
    .catchall {:try_start_c .. :try_end_c} :catchall_0

    .line 1446
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    if-eqz v0, :cond_11

    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v0}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    :cond_11
    return-object v1

    .line 1427
    :cond_12
    const/4 v0, 0x1

    .line 1428
    :try_start_d
    const-string v1, "GmsBackupTransport"

    const-string v3, "Authentication error, trying again after invalidating auth token"

    invoke-static {v1, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catchall {:try_start_d .. :try_end_d} :catchall_0

    .line 1446
    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    goto/16 :goto_0

    .line 1438
    :cond_13
    :try_start_e
    const-string v3, "GmsBackupTransport"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Network error - waiting "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "ms to retry: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v3, v6, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_e
    .catchall {:try_start_e .. :try_end_e} :catchall_0

    .line 1441
    const-wide/16 v6, 0x0

    cmp-long v1, v4, v6

    if-lez v1, :cond_14

    :try_start_f
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_f
    .catch Ljava/lang/InterruptedException; {:try_start_f .. :try_end_f} :catch_6
    .catchall {:try_start_f .. :try_end_f} :catchall_0

    .line 1446
    :cond_14
    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->d:Lcom/google/android/gms/http/GoogleHttpClient;

    invoke-virtual {v1}, Lcom/google/android/gms/http/GoogleHttpClient;->close()V

    goto/16 :goto_0

    .line 1442
    :catch_6
    move-exception v0

    .line 1443
    :try_start_10
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1
    :try_end_10
    .catchall {:try_start_10 .. :try_end_10} :catchall_0
.end method

.method static synthetic b(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->f:Lcom/android/a/a;

    return-object v0
.end method

.method static synthetic b()Z
    .locals 1

    .prologue
    .line 98
    sget-boolean v0, Lcom/google/android/gms/backup/BackupTransportService;->a:Z

    return v0
.end method

.method private c()Landroid/accounts/Account;
    .locals 1

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->k:Lcom/google/android/gms/backup/aj;

    invoke-interface {v0}, Lcom/google/android/gms/backup/aj;->a()Landroid/accounts/Account;

    move-result-object v0

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/b;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    return-object v0
.end method

.method private declared-synchronized d()Landroid/app/backup/BackupManager;
    .locals 1

    .prologue
    .line 186
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->j:Landroid/app/backup/BackupManager;

    if-nez v0, :cond_0

    .line 187
    new-instance v0, Landroid/app/backup/BackupManager;

    invoke-direct {v0, p0}, Landroid/app/backup/BackupManager;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->j:Landroid/app/backup/BackupManager;

    .line 189
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->j:Landroid/app/backup/BackupManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    .line 186
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method static synthetic d(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/app/backup/BackupManager;
    .locals 1

    .prologue
    .line 98
    invoke-direct {p0}, Lcom/google/android/gms/backup/BackupTransportService;->d()Landroid/app/backup/BackupManager;

    move-result-object v0

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/backup/BackupTransportService;)I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->l:I

    return v0
.end method

.method static synthetic g(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->h:Lcom/android/a/a;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/backup/BackupTransportService;)Lcom/android/a/a;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->g:Lcom/android/a/a;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/backup/BackupTransportService;)Landroid/content/BroadcastReceiver;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    return-object v0
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    .line 1205
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->f:Lcom/android/a/a;

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1206
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->g:Lcom/android/a/a;

    invoke-virtual {v0, v2, v3}, Lcom/android/a/a;->b(J)V

    .line 1207
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 1265
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->b(Landroid/content/Context;)V

    .line 1267
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1268
    invoke-static {p0}, Lcom/google/android/gms/backup/BackupTransportMigratorService;->a(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/BackupTransportService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 1270
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->m:Landroid/app/backup/BackupTransport;

    invoke-virtual {v0}, Landroid/app/backup/BackupTransport;->getBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1275
    const-string v0, "Backup.RateLimiter"

    invoke-virtual {p0, v0, v2}, Lcom/google/android/gms/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->c:Landroid/content/SharedPreferences;

    .line 1277
    new-instance v0, Lcom/android/a/b;

    invoke-direct {v0}, Lcom/android/a/b;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->e:Lcom/android/a/b;

    .line 1278
    new-instance v0, Lcom/android/a/a;

    const-string v1, "GmsBackupTransport.backupScheduler"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->f:Lcom/android/a/a;

    .line 1280
    new-instance v0, Lcom/android/a/a;

    const-string v1, "GmsBackupTransport.restoreScheduler"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->g:Lcom/android/a/a;

    .line 1282
    new-instance v0, Lcom/android/a/a;

    const-string v1, "GmsBackupTransport.abortScheduler"

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/backup/BackupTransportService;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/android/a/a;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->h:Lcom/android/a/a;

    .line 1285
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1287
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.net.conn.BACKGROUND_DATA_SETTING_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1289
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.accounts.LOGIN_ACCOUNTS_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/backup/BackupTransportService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 1291
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    new-instance v2, Landroid/content/IntentFilter;

    const-string v3, "com.google.android.gms.backup.BackupAccountChanged"

    invoke-direct {v2, v3}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    .line 1293
    const/4 v0, 0x1

    new-instance v1, Lcom/google/android/gms/backup/ac;

    invoke-direct {v1, p0}, Lcom/google/android/gms/backup/ac;-><init>(Lcom/google/android/gms/backup/BackupTransportService;)V

    invoke-static {v0, v1}, Landroid/content/ContentResolver;->addStatusChangeListener(ILandroid/content/SyncStatusObserver;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->i:Ljava/lang/Object;

    .line 1303
    new-instance v0, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/backup/BackupTransportService;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v1, Lcom/google/android/gms/backup/ad;

    invoke-direct {v1, p0}, Lcom/google/android/gms/backup/ad;-><init>(Lcom/google/android/gms/backup/BackupTransportService;)V

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 1312
    new-instance v0, Lcom/google/android/gms/backup/ah;

    invoke-direct {v0}, Lcom/google/android/gms/backup/ah;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->k:Lcom/google/android/gms/backup/aj;

    .line 1313
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 1317
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/BackupTransportService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 1318
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/backup/BackupTransportService;->n:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 1319
    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->i:Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/backup/BackupTransportService;->i:Ljava/lang/Object;

    invoke-static {v0}, Landroid/content/ContentResolver;->removeStatusChangeListener(Ljava/lang/Object;)V

    .line 1320
    :cond_0
    return-void
.end method
