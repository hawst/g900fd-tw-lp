.class final Lcom/google/android/gms/drive/internal/ae;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 445
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 446
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ae;->a:Lcom/google/android/gms/common/api/m;

    .line 447
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/common/api/m;B)V
    .locals 0

    .prologue
    .line 442
    invoke-direct {p0, p1}, Lcom/google/android/gms/drive/internal/ae;-><init>(Lcom/google/android/gms/common/api/m;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ae;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/af;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/gms/drive/internal/af;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/Set;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 462
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;)V
    .locals 5

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ae;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/af;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v3, Ljava/util/HashSet;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnResourceIdSetResponse;->b()Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/drive/internal/af;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/Set;B)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 457
    return-void
.end method
