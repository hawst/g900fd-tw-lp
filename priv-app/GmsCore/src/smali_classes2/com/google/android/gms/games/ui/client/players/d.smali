.class final Lcom/google/android/gms/games/ui/client/players/d;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:I

.field b:Z

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Landroid/net/Uri;

.field f:I


# direct methods
.method constructor <init>(I)V
    .locals 1

    .prologue
    .line 146
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 147
    iput p1, p0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    .line 148
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    .line 149
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 153
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/players/d;-><init>(I)V

    .line 154
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/d;->c:Ljava/lang/String;

    .line 155
    iput-object p2, p0, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    .line 156
    iput-object p3, p0, Lcom/google/android/gms/games/ui/client/players/d;->e:Landroid/net/Uri;

    .line 157
    return-void
.end method


# virtual methods
.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 161
    instance-of v2, p1, Lcom/google/android/gms/games/ui/client/players/d;

    if-nez v2, :cond_1

    .line 183
    :cond_0
    :goto_0
    return v0

    .line 165
    :cond_1
    check-cast p1, Lcom/google/android/gms/games/ui/client/players/d;

    .line 168
    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-nez v2, :cond_2

    .line 169
    iget v2, p1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-nez v2, :cond_0

    move v0, v1

    goto :goto_0

    .line 173
    :cond_2
    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-ne v2, v1, :cond_3

    .line 174
    iget v2, p1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-ne v2, v1, :cond_0

    move v0, v1

    goto :goto_0

    .line 178
    :cond_3
    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-ne v2, v3, :cond_4

    .line 179
    iget v2, p1, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    if-ne v2, v3, :cond_0

    move v0, v1

    goto :goto_0

    .line 183
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/d;->c:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/games/ui/client/players/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 188
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "type"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->a:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "playerId"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "displayName"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "iconImageUri"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->e:Landroid/net/Uri;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "iconImageResId"

    iget v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->f:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "selected"

    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/client/players/d;->b:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
