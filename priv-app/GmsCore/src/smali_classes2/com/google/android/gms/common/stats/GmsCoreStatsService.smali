.class public Lcom/google/android/gms/common/stats/GmsCoreStatsService;
.super Landroid/app/Service;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/stats/j;


# static fields
.field private static final b:Ljava/util/Comparator;


# instance fields
.field private final a:Z

.field private c:I

.field private d:J

.field private final e:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/common/stats/e;

    invoke-direct {v0}, Lcom/google/android/gms/common/stats/e;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->b:Ljava/util/Comparator;

    return-void
.end method

.method public constructor <init>()V
    .locals 4

    .prologue
    .line 78
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 54
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->a:Z

    .line 79
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    move-result-wide v2

    .line 81
    :try_start_0
    sget-object v0, Lcom/google/android/gms/common/stats/d;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->c:I

    .line 82
    sget-object v0, Lcom/google/android/gms/common/stats/d;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->d:J
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 91
    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    .line 93
    new-instance v0, Landroid/support/v4/g/a;

    invoke-direct {v0}, Landroid/support/v4/g/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    .line 94
    return-void

    .line 91
    :catchall_0
    move-exception v0

    invoke-static {v2, v3}, Landroid/os/Binder;->restoreCallingIdentity(J)V

    throw v0
.end method

.method public static a(Landroid/content/Context;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 97
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/stats/GmsCoreStatsService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 98
    const-string v1, "com.google.android.gms.common.stats.START"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 100
    return-object v0
.end method

.method private a(Lcom/google/android/gms/common/stats/ConnectionEvent;)V
    .locals 9

    .prologue
    const/4 v0, 0x1

    const/4 v2, 0x0

    .line 177
    iget v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->c:I

    sget v3, Lcom/google/android/gms/common/stats/g;->d:I

    and-int/2addr v1, v3

    if-eqz v1, :cond_3

    :goto_0
    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a(Z)Ljava/lang/String;

    move-result-object v0

    .line 186
    :try_start_0
    new-instance v4, Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_0 .. :try_end_0} :catch_4
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_b
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    new-instance v1, Ljava/lang/StringBuilder;
    :try_end_1
    .catch Ljava/io/FileNotFoundException; {:try_start_1 .. :try_end_1} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_1 .. :try_end_1} :catch_4
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_c
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V
    :try_end_2
    .catch Ljava/io/FileNotFoundException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_d
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->getFilesDir()Ljava/io/File;
    :try_end_3
    .catch Ljava/io/FileNotFoundException; {:try_start_3 .. :try_end_3} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_3 .. :try_end_3} :catch_4
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_e
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    move-result-object v3

    :try_start_4
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;
    :try_end_4
    .catch Ljava/io/FileNotFoundException; {:try_start_4 .. :try_end_4} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_4 .. :try_end_4} :catch_4
    .catch Ljava/io/IOException; {:try_start_4 .. :try_end_4} :catch_f
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    move-result-object v1

    :try_start_5
    const-string v3, "/service.connections.internal"
    :try_end_5
    .catch Ljava/io/FileNotFoundException; {:try_start_5 .. :try_end_5} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_5 .. :try_end_5} :catch_4
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_10
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    :try_start_6
    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_6
    .catch Ljava/io/FileNotFoundException; {:try_start_6 .. :try_end_6} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_6 .. :try_end_6} :catch_4
    .catch Ljava/io/IOException; {:try_start_6 .. :try_end_6} :catch_11
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v1

    :try_start_7
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_7
    .catch Ljava/io/FileNotFoundException; {:try_start_7 .. :try_end_7} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_7 .. :try_end_7} :catch_4
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_12
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    move-result-object v1

    const/4 v3, 0x1

    :try_start_8
    invoke-direct {v4, v1, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/lang/String;Z)V
    :try_end_8
    .catch Ljava/io/FileNotFoundException; {:try_start_8 .. :try_end_8} :catch_2
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_8 .. :try_end_8} :catch_4
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_13
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    .line 189
    :try_start_9
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;
    :try_end_9
    .catch Ljava/io/FileNotFoundException; {:try_start_9 .. :try_end_9} :catch_20
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_9 .. :try_end_9} :catch_7
    .catch Ljava/io/IOException; {:try_start_9 .. :try_end_9} :catch_14
    .catchall {:try_start_9 .. :try_end_9} :catchall_1

    move-result-object v3

    .line 190
    :try_start_a
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->tryLock()Ljava/nio/channels/FileLock;
    :try_end_a
    .catch Ljava/io/FileNotFoundException; {:try_start_a .. :try_end_a} :catch_21
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_a .. :try_end_a} :catch_8
    .catch Ljava/io/IOException; {:try_start_a .. :try_end_a} :catch_15
    .catchall {:try_start_a .. :try_end_a} :catchall_2

    move-result-object v1

    .line 191
    if-nez v1, :cond_4

    .line 192
    :try_start_b
    const-string v5, "GmsCoreStatsService"
    :try_end_b
    .catch Ljava/io/FileNotFoundException; {:try_start_b .. :try_end_b} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_b .. :try_end_b} :catch_9
    .catch Ljava/io/IOException; {:try_start_b .. :try_end_b} :catch_16
    .catchall {:try_start_b .. :try_end_b} :catchall_3

    :try_start_c
    new-instance v6, Ljava/lang/StringBuilder;
    :try_end_c
    .catch Ljava/io/FileNotFoundException; {:try_start_c .. :try_end_c} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_c .. :try_end_c} :catch_9
    .catch Ljava/io/IOException; {:try_start_c .. :try_end_c} :catch_17
    .catchall {:try_start_c .. :try_end_c} :catchall_3

    :try_start_d
    const-string v7, "Not able to lock file to write log: "
    :try_end_d
    .catch Ljava/io/FileNotFoundException; {:try_start_d .. :try_end_d} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_d .. :try_end_d} :catch_9
    .catch Ljava/io/IOException; {:try_start_d .. :try_end_d} :catch_18
    .catchall {:try_start_d .. :try_end_d} :catchall_3

    :try_start_e
    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V
    :try_end_e
    .catch Ljava/io/FileNotFoundException; {:try_start_e .. :try_end_e} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_e .. :try_end_e} :catch_9
    .catch Ljava/io/IOException; {:try_start_e .. :try_end_e} :catch_19
    .catchall {:try_start_e .. :try_end_e} :catchall_3

    :try_start_f
    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;
    :try_end_f
    .catch Ljava/io/FileNotFoundException; {:try_start_f .. :try_end_f} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_f .. :try_end_f} :catch_9
    .catch Ljava/io/IOException; {:try_start_f .. :try_end_f} :catch_1a
    .catchall {:try_start_f .. :try_end_f} :catchall_3

    move-result-object v0

    :try_start_10
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_10
    .catch Ljava/io/FileNotFoundException; {:try_start_10 .. :try_end_10} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_10 .. :try_end_10} :catch_9
    .catch Ljava/io/IOException; {:try_start_10 .. :try_end_10} :catch_1b
    .catchall {:try_start_10 .. :try_end_10} :catchall_3

    move-result-object v0

    :try_start_11
    invoke-static {v5, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_11
    .catch Ljava/io/FileNotFoundException; {:try_start_11 .. :try_end_11} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_11 .. :try_end_11} :catch_9
    .catch Ljava/io/IOException; {:try_start_11 .. :try_end_11} :catch_1c
    .catchall {:try_start_11 .. :try_end_11} :catchall_3

    .line 206
    if-eqz v1, :cond_0

    .line 207
    :try_start_12
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 209
    :cond_0
    if-eqz v3, :cond_1

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 210
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 212
    :cond_1
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_12
    .catch Ljava/io/IOException; {:try_start_12 .. :try_end_12} :catch_0

    .line 222
    :cond_2
    :goto_1
    return-void

    .line 177
    :cond_3
    const/4 v0, 0x0

    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    .line 216
    const-string v1, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 195
    :cond_4
    :try_start_13
    new-instance v5, Ljava/io/PrintWriter;
    :try_end_13
    .catch Ljava/io/FileNotFoundException; {:try_start_13 .. :try_end_13} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_13 .. :try_end_13} :catch_9
    .catch Ljava/io/IOException; {:try_start_13 .. :try_end_13} :catch_1d
    .catchall {:try_start_13 .. :try_end_13} :catchall_3

    const/4 v6, 0x1

    :try_start_14
    invoke-direct {v5, v4, v6}, Ljava/io/PrintWriter;-><init>(Ljava/io/OutputStream;Z)V
    :try_end_14
    .catch Ljava/io/FileNotFoundException; {:try_start_14 .. :try_end_14} :catch_22
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_14 .. :try_end_14} :catch_9
    .catch Ljava/io/IOException; {:try_start_14 .. :try_end_14} :catch_1e
    .catchall {:try_start_14 .. :try_end_14} :catchall_3

    .line 196
    :try_start_15
    invoke-virtual {v5, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V
    :try_end_15
    .catch Ljava/io/FileNotFoundException; {:try_start_15 .. :try_end_15} :catch_23
    .catch Ljava/nio/channels/OverlappingFileLockException; {:try_start_15 .. :try_end_15} :catch_a
    .catch Ljava/io/IOException; {:try_start_15 .. :try_end_15} :catch_1f
    .catchall {:try_start_15 .. :try_end_15} :catchall_4

    .line 206
    if-eqz v1, :cond_5

    .line 207
    :try_start_16
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 209
    :cond_5
    if-eqz v3, :cond_6

    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 210
    invoke-virtual {v3}, Ljava/nio/channels/FileChannel;->close()V

    .line 212
    :cond_6
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_16
    .catch Ljava/io/IOException; {:try_start_16 .. :try_end_16} :catch_1

    .line 218
    :goto_2
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V

    goto :goto_1

    .line 215
    :catch_1
    move-exception v0

    .line 216
    const-string v1, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 200
    :catch_2
    move-exception v0

    move-object v1, v2

    move-object v3, v2

    move-object v4, v2

    .line 201
    :goto_3
    :try_start_17
    const-string v5, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/io/FileNotFoundException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v5, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_17
    .catchall {:try_start_17 .. :try_end_17} :catchall_5

    .line 206
    if-eqz v1, :cond_7

    .line 207
    :try_start_18
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 209
    :cond_7
    if-eqz v2, :cond_8

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 210
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 212
    :cond_8
    if-eqz v3, :cond_9

    .line 213
    invoke-virtual {v3}, Ljava/io/FileOutputStream;->close()V
    :try_end_18
    .catch Ljava/io/IOException; {:try_start_18 .. :try_end_18} :catch_3

    .line 218
    :cond_9
    :goto_4
    if-eqz v4, :cond_2

    .line 219
    invoke-virtual {v4}, Ljava/io/PrintWriter;->close()V

    goto :goto_1

    .line 215
    :catch_3
    move-exception v0

    .line 216
    const-string v1, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    .line 202
    :catch_4
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    :goto_5
    move-object v1, v2

    move-object v2, v3

    .line 203
    :try_start_19
    const-string v3, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/lang/Exception;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_19
    .catchall {:try_start_19 .. :try_end_19} :catchall_6

    .line 206
    if-eqz v1, :cond_a

    .line 207
    :try_start_1a
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 209
    :cond_a
    if-eqz v2, :cond_b

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 210
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 212
    :cond_b
    if-eqz v4, :cond_c

    .line 213
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1a
    .catch Ljava/io/IOException; {:try_start_1a .. :try_end_1a} :catch_5

    .line 218
    :cond_c
    :goto_6
    if-eqz v5, :cond_2

    .line 219
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V

    goto/16 :goto_1

    .line 215
    :catch_5
    move-exception v0

    .line 216
    const-string v1, "GmsCoreStatsService"

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 205
    :catchall_0
    move-exception v0

    move-object v1, v2

    move-object v4, v2

    move-object v5, v2

    .line 206
    :goto_7
    if-eqz v1, :cond_d

    .line 207
    :try_start_1b
    invoke-virtual {v1}, Ljava/nio/channels/FileLock;->release()V

    .line 209
    :cond_d
    if-eqz v2, :cond_e

    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->isOpen()Z

    move-result v1

    if-eqz v1, :cond_e

    .line 210
    invoke-virtual {v2}, Ljava/nio/channels/FileChannel;->close()V

    .line 212
    :cond_e
    if-eqz v4, :cond_f

    .line 213
    invoke-virtual {v4}, Ljava/io/FileOutputStream;->close()V
    :try_end_1b
    .catch Ljava/io/IOException; {:try_start_1b .. :try_end_1b} :catch_6

    .line 218
    :cond_f
    :goto_8
    if-eqz v5, :cond_10

    .line 219
    invoke-virtual {v5}, Ljava/io/PrintWriter;->close()V

    :cond_10
    throw v0

    .line 215
    :catch_6
    move-exception v1

    .line 216
    const-string v2, "GmsCoreStatsService"

    invoke-virtual {v1}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v2, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_8

    .line 205
    :catchall_1
    move-exception v0

    move-object v1, v2

    move-object v5, v2

    goto :goto_7

    :catchall_2
    move-exception v0

    move-object v1, v2

    move-object v5, v2

    move-object v2, v3

    goto :goto_7

    :catchall_3
    move-exception v0

    move-object v5, v2

    move-object v2, v3

    goto :goto_7

    :catchall_4
    move-exception v0

    move-object v2, v3

    goto :goto_7

    :catchall_5
    move-exception v0

    move-object v5, v4

    move-object v4, v3

    goto :goto_7

    :catchall_6
    move-exception v0

    goto :goto_7

    .line 202
    :catch_7
    move-exception v0

    move-object v3, v2

    move-object v5, v2

    goto :goto_5

    :catch_8
    move-exception v0

    move-object v5, v2

    goto :goto_5

    :catch_9
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto :goto_5

    :catch_a
    move-exception v0

    move-object v2, v1

    goto :goto_5

    :catch_b
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_c
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_d
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_e
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_f
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_10
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_11
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_12
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_13
    move-exception v0

    move-object v3, v2

    move-object v4, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_14
    move-exception v0

    move-object v3, v2

    move-object v5, v2

    goto/16 :goto_5

    :catch_15
    move-exception v0

    move-object v5, v2

    goto/16 :goto_5

    :catch_16
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_17
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_18
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_19
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1a
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1b
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1c
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1d
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1e
    move-exception v0

    move-object v5, v2

    move-object v2, v1

    goto/16 :goto_5

    :catch_1f
    move-exception v0

    move-object v2, v1

    goto/16 :goto_5

    .line 200
    :catch_20
    move-exception v0

    move-object v1, v2

    move-object v3, v4

    move-object v4, v2

    goto/16 :goto_3

    :catch_21
    move-exception v0

    move-object v1, v2

    move-object v8, v3

    move-object v3, v4

    move-object v4, v2

    move-object v2, v8

    goto/16 :goto_3

    :catch_22
    move-exception v0

    move-object v8, v3

    move-object v3, v4

    move-object v4, v2

    move-object v2, v8

    goto/16 :goto_3

    :catch_23
    move-exception v0

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    goto/16 :goto_3
.end method


# virtual methods
.method public final a(J)V
    .locals 5

    .prologue
    .line 156
    iget-object v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    monitor-enter v1

    .line 157
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/stats/h;

    .line 158
    if-nez v0, :cond_1

    .line 159
    monitor-exit v1

    .line 174
    :cond_0
    :goto_0
    return-void

    .line 161
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-interface {v2, v3}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 163
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    iget v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->c:I

    sget v2, Lcom/google/android/gms/common/stats/g;->b:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 172
    iget-object v0, v0, Lcom/google/android/gms/common/stats/h;->c:Lcom/google/android/gms/common/stats/ConnectionEvent;

    iget-wide v2, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->d:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a(J)Lcom/google/android/gms/common/stats/ConnectionEvent;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->a(Lcom/google/android/gms/common/stats/ConnectionEvent;)V

    goto :goto_0

    .line 163
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, -0x1

    const/4 v2, -0x2

    .line 226
    new-instance v4, Lcom/google/android/gms/common/util/ad;

    const-string v0, "  "

    invoke-direct {v4, p2, v0}, Lcom/google/android/gms/common/util/ad;-><init>(Ljava/io/Writer;Ljava/lang/String;)V

    .line 227
    const-string v0, "GMS Core Stats:"

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/16 v1, 0x80

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->getInstalledApplications(I)Ljava/util/List;

    move-result-object v0

    .line 230
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 231
    const-string v1, "Client jar version:"

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 232
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 234
    sget-object v1, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->b:Ljava/util/Comparator;

    invoke-static {v0, v1}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 238
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 239
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/pm/ApplicationInfo;

    .line 240
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    if-eqz v3, :cond_2

    .line 241
    iget-object v3, v0, Landroid/content/pm/ApplicationInfo;->metaData:Landroid/os/Bundle;

    const-string v6, "com.google.android.gms.version"

    invoke-virtual {v3, v6, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    .line 248
    :goto_1
    if-eq v3, v2, :cond_0

    if-eq v3, v7, :cond_0

    .line 249
    if-eq v1, v3, :cond_1

    .line 253
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 254
    packed-switch v3, :pswitch_data_0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v6, ":"

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    :goto_2
    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 256
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->a()V

    move v1, v3

    .line 258
    :cond_1
    iget-object v0, v0, Landroid/content/pm/ApplicationInfo;->packageName:Ljava/lang/String;

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    goto :goto_0

    :cond_2
    move v3, v2

    .line 245
    goto :goto_1

    .line 254
    :pswitch_0
    const-string v1, "EMPTY_METADATA:"

    goto :goto_2

    :pswitch_1
    const-string v1, "UNKNOWN_SDK_VERSION:"

    goto :goto_2

    .line 261
    :cond_3
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 262
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    monitor-enter v1

    .line 266
    :try_start_0
    new-instance v0, Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v2

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 267
    const-string v2, "GmsCoreStatsService"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "There are "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-interface {v5}, Ljava/util/Map;->size()I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " in mMatching during dump"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 268
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 269
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    if-lez v1, :cond_5

    .line 270
    const-string v1, "Open connections: "

    invoke-virtual {v4, v1}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    .line 271
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->a()V

    .line 272
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/stats/h;

    .line 273
    iget-object v0, v0, Lcom/google/android/gms/common/stats/h;->c:Lcom/google/android/gms/common/stats/ConnectionEvent;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a(Z)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/common/util/ad;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 268
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 275
    :cond_4
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->b()V

    .line 277
    :cond_5
    invoke-virtual {v4}, Lcom/google/android/gms/common/util/ad;->println()V

    .line 278
    return-void

    .line 254
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 293
    const/4 v0, 0x0

    return-object v0
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 8

    .prologue
    const/4 v3, 0x1

    .line 106
    if-eqz p1, :cond_1

    .line 107
    const-string v0, "com.google.android.gms.common.stats.EXTRA_LOG_EVENT"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/stats/ConnectionEvent;

    .line 108
    if-eqz v0, :cond_1

    .line 112
    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->b()Ljava/lang/String;

    move-result-object v4

    const/4 v1, -0x1

    invoke-virtual {v4}, Ljava/lang/String;->hashCode()I

    move-result v2

    sparse-switch v2, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v1, :pswitch_data_0

    const-string v1, "GmsCoreStatsService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v5, "Unsupported event type: "

    invoke-direct {v2, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :goto_1
    iget v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->c:I

    sget v2, Lcom/google/android/gms/common/stats/g;->c:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_1

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->a(Lcom/google/android/gms/common/stats/ConnectionEvent;)V

    .line 115
    :cond_1
    return v3

    .line 112
    :sswitch_0
    const-string v2, "BIND"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x0

    goto :goto_0

    :sswitch_1
    const-string v2, "CONNECT"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v1, v3

    goto :goto_0

    :sswitch_2
    const-string v2, "UNBIND"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x2

    goto :goto_0

    :sswitch_3
    const-string v2, "DISCONNECT"

    invoke-virtual {v4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x3

    goto :goto_0

    :pswitch_0
    iget-object v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->h()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/common/stats/h;

    iget-wide v6, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->d:J

    invoke-direct {v5, p0, v0, v6, v7}, Lcom/google/android/gms/common/stats/h;-><init>(Lcom/google/android/gms/common/stats/j;Lcom/google/android/gms/common/stats/ConnectionEvent;J)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_1
    iget-object v2, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    monitor-enter v2

    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->h()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/stats/h;

    if-nez v1, :cond_2

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_1

    :catchall_1
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_2
    :try_start_2
    iget-object v5, p0, Lcom/google/android/gms/common/stats/GmsCoreStatsService;->e:Ljava/util/Map;

    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->h()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-interface {v5, v6}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    iget-object v2, v1, Lcom/google/android/gms/common/stats/h;->c:Lcom/google/android/gms/common/stats/ConnectionEvent;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a(Ljava/lang/String;)Lcom/google/android/gms/common/stats/ConnectionEvent;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a()J

    move-result-wide v6

    iget-object v0, v1, Lcom/google/android/gms/common/stats/h;->c:Lcom/google/android/gms/common/stats/ConnectionEvent;

    invoke-virtual {v0}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a()J

    move-result-wide v0

    sub-long v0, v6, v0

    invoke-virtual {v4, v0, v1}, Lcom/google/android/gms/common/stats/ConnectionEvent;->a(J)Lcom/google/android/gms/common/stats/ConnectionEvent;

    move-object v0, v2

    goto/16 :goto_1

    nop

    :sswitch_data_0
    .sparse-switch
        -0x6a89cdaa -> :sswitch_2
        0x1f1c3d -> :sswitch_0
        0x3c87449c -> :sswitch_3
        0x638004ca -> :sswitch_1
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method
