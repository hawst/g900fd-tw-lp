.class public final Lcom/google/android/gms/icing/aa;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:I

.field public b:I

.field public c:I

.field public d:I

.field public e:[Lcom/google/android/gms/icing/ab;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x2

    .line 12104
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 12105
    const v0, 0x93a80

    iput v0, p0, Lcom/google/android/gms/icing/aa;->a:I

    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/icing/aa;->b:I

    iput v1, p0, Lcom/google/android/gms/icing/aa;->c:I

    iput v1, p0, Lcom/google/android/gms/icing/aa;->d:I

    invoke-static {}, Lcom/google/android/gms/icing/ab;->a()[Lcom/google/android/gms/icing/ab;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/aa;->cachedSize:I

    .line 12106
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x2

    .line 12186
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 12187
    iget v1, p0, Lcom/google/android/gms/icing/aa;->a:I

    const v2, 0x93a80

    if-eq v1, v2, :cond_0

    .line 12188
    iget v1, p0, Lcom/google/android/gms/icing/aa;->a:I

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12191
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/aa;->b:I

    if-eq v1, v4, :cond_1

    .line 12192
    iget v1, p0, Lcom/google/android/gms/icing/aa;->b:I

    invoke-static {v3, v1}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12195
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/aa;->c:I

    if-eq v1, v3, :cond_2

    .line 12196
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/aa;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12199
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/aa;->d:I

    if-eq v1, v3, :cond_3

    .line 12200
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/aa;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 12203
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    array-length v1, v1

    if-lez v1, :cond_6

    .line 12204
    const/4 v1, 0x0

    move v5, v1

    move v1, v0

    move v0, v5

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    array-length v2, v2

    if-ge v0, v2, :cond_5

    .line 12205
    iget-object v2, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    aget-object v2, v2, v0

    .line 12206
    if-eqz v2, :cond_4

    .line 12207
    const/4 v3, 0x5

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 12204
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_5
    move v0, v1

    .line 12212
    :cond_6
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 12120
    if-ne p1, p0, :cond_1

    .line 12143
    :cond_0
    :goto_0
    return v0

    .line 12123
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/aa;

    if-nez v2, :cond_2

    move v0, v1

    .line 12124
    goto :goto_0

    .line 12126
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/aa;

    .line 12127
    iget v2, p0, Lcom/google/android/gms/icing/aa;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/aa;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 12128
    goto :goto_0

    .line 12130
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/aa;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/aa;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 12131
    goto :goto_0

    .line 12133
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/aa;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/aa;->c:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 12134
    goto :goto_0

    .line 12136
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/aa;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/aa;->d:I

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 12137
    goto :goto_0

    .line 12139
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    iget-object v3, p1, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 12141
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 12148
    iget v0, p0, Lcom/google/android/gms/icing/aa;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 12150
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/aa;->b:I

    add-int/2addr v0, v1

    .line 12151
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/aa;->c:I

    add-int/2addr v0, v1

    .line 12152
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/aa;->d:I

    add-int/2addr v0, v1

    .line 12153
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 12155
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 11947
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aa;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aa;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aa;->c:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aa;->d:I

    goto :goto_0

    :sswitch_5
    const/16 v0, 0x2a

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/ab;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/ab;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ab;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/ab;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ab;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 12161
    iget v0, p0, Lcom/google/android/gms/icing/aa;->a:I

    const v1, 0x93a80

    if-eq v0, v1, :cond_0

    .line 12162
    iget v0, p0, Lcom/google/android/gms/icing/aa;->a:I

    invoke-virtual {p1, v3, v0}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12164
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/aa;->b:I

    if-eq v0, v3, :cond_1

    .line 12165
    iget v0, p0, Lcom/google/android/gms/icing/aa;->b:I

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12167
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/aa;->c:I

    if-eq v0, v2, :cond_2

    .line 12168
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/aa;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12170
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/aa;->d:I

    if-eq v0, v2, :cond_3

    .line 12171
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/aa;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 12173
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    array-length v0, v0

    if-lez v0, :cond_5

    .line 12174
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    array-length v1, v1

    if-ge v0, v1, :cond_5

    .line 12175
    iget-object v1, p0, Lcom/google/android/gms/icing/aa;->e:[Lcom/google/android/gms/icing/ab;

    aget-object v1, v1, v0

    .line 12176
    if-eqz v1, :cond_4

    .line 12177
    const/4 v2, 0x5

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 12174
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 12181
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 12182
    return-void
.end method
