.class public final Lcom/google/android/gms/mdm/a/c;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/Class;

.field private final g:Lcom/android/volley/x;

.field private final h:Lcom/google/protobuf/nano/j;

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private k:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 48
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p4}, Lcom/android/volley/p;-><init>(ILjava/lang/String;Lcom/android/volley/w;)V

    .line 44
    iput-boolean v2, p0, Lcom/google/android/gms/mdm/a/c;->k:Z

    .line 50
    if-eqz p2, :cond_0

    .line 51
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    .line 52
    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/util/a;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/util/List;

    move-result-object v0

    .line 54
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 55
    const-string v0, "want to send authenticated request, but no Google account on device"

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 62
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/mdm/a/c;->i:Ljava/lang/String;

    .line 65
    :goto_0
    iput-object p5, p0, Lcom/google/android/gms/mdm/a/c;->f:Ljava/lang/Class;

    .line 66
    iput-object p3, p0, Lcom/google/android/gms/mdm/a/c;->g:Lcom/android/volley/x;

    .line 67
    iput-object p6, p0, Lcom/google/android/gms/mdm/a/c;->h:Lcom/google/protobuf/nano/j;

    .line 68
    return-void

    .line 59
    :cond_1
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/mdm/a/c;->i:Ljava/lang/String;

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 4

    .prologue
    .line 132
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->f:Ljava/lang/Class;

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    const/4 v2, 0x0

    array-length v3, v1

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    .line 134
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 136
    :goto_0
    return-object v0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    new-instance v1, Lcom/android/volley/ac;

    invoke-virtual {v0}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    invoke-static {v1}, Lcom/android/volley/v;->a(Lcom/android/volley/ac;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method public final b(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 115
    instance-of v0, p1, Lcom/android/volley/a;

    if-eqz v0, :cond_0

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->j:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 117
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/mdm/a/c;->j:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 120
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/mdm/a/c;->k:Z

    if-nez v0, :cond_1

    .line 121
    invoke-super {p0, p1}, Lcom/android/volley/p;->b(Lcom/android/volley/ac;)V

    .line 126
    :goto_0
    return-void

    .line 123
    :cond_1
    const-string v0, "Not delivering error response for request=[%s], error=[%s] because response already delivered."

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    aput-object p0, v1, v2

    const/4 v2, 0x1

    aput-object p1, v1, v2

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->b(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 29
    check-cast p1, Lcom/google/protobuf/nano/j;

    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->g:Lcom/android/volley/x;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->g:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/mdm/a/c;->k:Z

    return-void
.end method

.method public final i()Ljava/util/Map;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->i:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 79
    :try_start_0
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/mdm/a/c;->i:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "oauth2:"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->m:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/a/c;->j:Ljava/lang/String;

    .line 81
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 82
    const-string v1, "Authorization"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bearer "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/mdm/a/c;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_1

    .line 101
    :goto_0
    return-object v0

    .line 84
    :catch_0
    move-exception v0

    .line 86
    const-string v1, "GoogleAuthUtil.getToken threw IOException"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/mdm/f/f;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 87
    new-instance v1, Lcom/google/android/gms/mdm/a/a;

    const-string v2, "GoogleAuthUtil.getToken threw IOException"

    const/4 v3, 0x1

    invoke-direct {v1, v2, v0, v3}, Lcom/google/android/gms/mdm/a/a;-><init>(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v1

    .line 89
    :catch_1
    move-exception v0

    .line 95
    const-string v1, "GoogleAuthUtil.getToken threw GoogleAuthException"

    new-array v2, v4, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/mdm/f/f;->a(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    new-instance v1, Lcom/google/android/gms/mdm/a/a;

    const-string v2, "GoogleAuthUtil.getToken threw GoogleAuthException"

    invoke-direct {v1, v2, v0, v4}, Lcom/google/android/gms/mdm/a/a;-><init>(Ljava/lang/String;Ljava/lang/Exception;Z)V

    throw v1

    .line 101
    :cond_0
    invoke-super {p0}, Lcom/android/volley/p;->i()Ljava/util/Map;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()[B
    .locals 1

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/gms/mdm/a/c;->h:Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    return-object v0
.end method
