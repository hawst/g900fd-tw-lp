.class public final Lcom/google/android/gms/games/service/SnapshotEventService;
.super Lcom/google/android/gms/drive/events/g;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    const-string v0, "SnapshotEventService"

    invoke-direct {p0, v0}, Lcom/google/android/gms/drive/events/g;-><init>(Ljava/lang/String;)V

    .line 45
    return-void
.end method

.method private static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 4

    .prologue
    .line 199
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    .line 200
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 201
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 208
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p2, p2, p1}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 211
    const-string v1, "https://www.googleapis.com/auth/drive.appdata"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 212
    const-string v1, "https://www.googleapis.com/auth/games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/server/ClientContext;->a(Ljava/lang/String;)V

    .line 213
    :goto_0
    return-object v0

    .line 202
    :catch_0
    move-exception v0

    .line 203
    const-string v1, "SnapshotEventService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 204
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Ljava/lang/String;
    .locals 5

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    .line 220
    const-string v2, "%s<:>%s<:>%s<:>%d"

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    aput-object p1, v3, v0

    const/4 v0, 0x2

    aput-object v1, v3, v0

    const/4 v0, 0x3

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/drive/events/CompletionEvent;)V
    .locals 13

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v10, 0x0

    .line 49
    if-nez p1, :cond_0

    .line 50
    const-string v0, "SnapshotEventService"

    const-string v1, "Null event received - ignoring"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 60
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;)Lcom/google/android/gms/games/a/t;

    move-result-object v11

    .line 56
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->g()I

    move-result v12

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->f()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    if-lez v0, :cond_2

    move v0, v6

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    const/4 v0, 0x0

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const-string v1, "<:>"

    invoke-virtual {v0, v1}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    array-length v0, v3

    const/4 v1, 0x4

    if-ne v0, v1, :cond_3

    move v0, v6

    :goto_2
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    const/4 v0, 0x0

    aget-object v1, v3, v0

    const/4 v0, 0x1

    aget-object v5, v3, v0

    const/4 v0, 0x2

    aget-object v2, v3, v0

    const/4 v0, 0x3

    aget-object v0, v3, v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->c()Ljava/lang/String;

    move-result-object v3

    packed-switch v12, :pswitch_data_0

    const-string v0, "SnapshotEventService"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v9, "Unknown snapshot action status: "

    invoke-direct {v6, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v0, v6}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v6, v10

    :goto_3
    :pswitch_0
    move-object v0, p0

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;I)V

    invoke-static {p0, v1, v3}, Lcom/google/android/gms/games/service/SnapshotEventService;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v0, "SnapshotEventService"

    const-string v1, "Cannot create client context - ignoring"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 58
    :cond_1
    :goto_4
    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->dismiss()V

    .line 59
    invoke-virtual {v11}, Lcom/google/android/gms/games/a/t;->a()V

    goto :goto_0

    :cond_2
    move v0, v10

    .line 56
    goto :goto_1

    :cond_3
    move v0, v10

    goto :goto_2

    :pswitch_1
    move v6, v8

    goto :goto_3

    :pswitch_2
    move v6, v9

    goto :goto_3

    :cond_4
    :try_start_1
    invoke-virtual {v11, p0, v2, v5}, Lcom/google/android/gms/games/a/t;->g(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    if-ne v12, v8, :cond_1

    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    iput-object v3, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    sget-object v3, Lcom/google/android/gms/drive/b;->g:Lcom/google/android/gms/common/api/c;

    new-instance v4, Lcom/google/android/gms/drive/g;

    invoke-direct {v4}, Lcom/google/android/gms/drive/g;-><init>()V

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/google/android/gms/drive/g;->a(I)Lcom/google/android/gms/drive/g;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/g;->a()Lcom/google/android/gms/drive/f;

    move-result-object v4

    invoke-virtual {v0, v3, v4}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/drive/b;->c:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    sget-object v3, Lcom/google/android/gms/games/d;->b:Lcom/google/android/gms/common/api/Scope;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/Scope;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/c;->b()Z

    move-result v1

    if-nez v1, :cond_5

    const-string v1, "SnapshotEventService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "FAILED TO CONNECT WHILE RESOLVING: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_4

    .line 58
    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->dismiss()V

    .line 59
    invoke-virtual {v11}, Lcom/google/android/gms/games/a/t;->a()V

    throw v0

    .line 56
    :cond_5
    :try_start_2
    new-instance v4, Lcom/google/android/gms/games/snapshot/c;

    invoke-virtual {v11, p0, v2, v5}, Lcom/google/android/gms/games/a/t;->f(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v4, v0}, Lcom/google/android/gms/games/snapshot/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_2
    .catch Lcom/google/android/gms/auth/q; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :try_start_3
    invoke-virtual {v4}, Lcom/google/android/gms/games/snapshot/c;->c()I

    move-result v0

    if-lez v0, :cond_e

    const/4 v0, 0x0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/games/snapshot/c;->b(I)Lcom/google/android/gms/games/snapshot/SnapshotMetadata;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-object v1, v0

    :goto_5
    :try_start_4
    invoke-virtual {v4}, Lcom/google/android/gms/games/snapshot/c;->w_()V

    if-nez v1, :cond_6

    const-string v0, "SnapshotEventService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to load base snapshot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " while recording a conflict"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catch Lcom/google/android/gms/auth/q; {:try_start_4 .. :try_end_4} :catch_0
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto/16 :goto_4

    :catch_0
    move-exception v0

    :try_start_5
    const-string v1, "SnapshotEventService"

    const-string v2, "Error while resolving conflict"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    goto/16 :goto_4

    :catchall_1
    move-exception v0

    :try_start_6
    invoke-virtual {v4}, Lcom/google/android/gms/games/snapshot/c;->w_()V

    throw v0

    :cond_6
    new-instance v4, Lcom/google/android/gms/games/snapshot/e;

    invoke-direct {v4}, Lcom/google/android/gms/games/snapshot/e;-><init>()V

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->k()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->a:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->m()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->b:Ljava/lang/Long;

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->o()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->c:Ljava/lang/Long;

    iget-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->b:Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-nez v0, :cond_7

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->b:Ljava/lang/Long;

    :cond_7
    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->f()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->e:Landroid/net/Uri;

    iget-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->e:Landroid/net/Uri;

    if-eqz v0, :cond_8

    const/4 v0, 0x0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->d:Lcom/google/android/gms/common/data/BitmapTeleporter;

    :cond_8
    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->e()Lcom/google/android/gms/drive/am;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->b()Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_9

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->b()Ljava/lang/String;

    move-result-object v5

    iput-object v5, v4, Lcom/google/android/gms/games/snapshot/e;->a:Ljava/lang/String;

    :cond_9
    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->d()Landroid/graphics/Bitmap;

    move-result-object v5

    if-eqz v5, :cond_a

    new-instance v5, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->d()Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-direct {v5, v6}, Lcom/google/android/gms/common/data/BitmapTeleporter;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v5, v4, Lcom/google/android/gms/games/snapshot/e;->d:Lcom/google/android/gms/common/data/BitmapTeleporter;

    const/4 v5, 0x0

    iput-object v5, v4, Lcom/google/android/gms/games/snapshot/e;->e:Landroid/net/Uri;

    :cond_a
    invoke-virtual {v0}, Lcom/google/android/gms/drive/am;->a()Ljava/util/Map;

    move-result-object v5

    new-instance v0, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v6, "duration"

    const/4 v7, 0x0

    invoke-direct {v0, v6, v7}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_b

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->b:Ljava/lang/Long;

    :cond_b
    new-instance v0, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v6, "progressValue"

    const/4 v7, 0x0

    invoke-direct {v0, v6, v7}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    if-eqz v0, :cond_c

    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/games/snapshot/e;->c:Ljava/lang/Long;

    :cond_c
    invoke-virtual {v4}, Lcom/google/android/gms/games/snapshot/e;->a()Lcom/google/android/gms/games/snapshot/d;

    move-result-object v4

    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, v3}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/i;

    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v5

    if-nez v5, :cond_d

    const-string v1, "SnapshotEventService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to open new conflict contents: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_d
    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->b()Lcom/google/android/gms/drive/m;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/drive/m;->d()Ljava/io/OutputStream;
    :try_end_6
    .catch Lcom/google/android/gms/auth/q; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    move-result-object v5

    :try_start_7
    invoke-virtual {p1}, Lcom/google/android/gms/drive/events/CompletionEvent;->d()Ljava/io/InputStream;

    move-result-object v6

    const/4 v7, 0x0

    const/16 v8, 0x2000

    invoke-static {v6, v5, v7, v8}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;ZI)J

    invoke-virtual {v5}, Ljava/io/OutputStream;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_1
    .catch Lcom/google/android/gms/auth/q; {:try_start_7 .. :try_end_7} :catch_0
    .catchall {:try_start_7 .. :try_end_7} :catchall_0

    :try_start_8
    new-instance v5, Lcom/google/android/gms/games/a/av;

    invoke-direct {v5, p0, v2}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->a()Lcom/google/android/gms/games/Game;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v5, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    invoke-interface {v1}, Lcom/google/android/gms/games/snapshot/SnapshotMetadata;->a()Lcom/google/android/gms/games/Game;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v5, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v5}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v1

    invoke-virtual {v11, v1, v3, v4, v0}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)I

    goto/16 :goto_4

    :catch_1
    move-exception v0

    const-string v0, "SnapshotEventService"

    const-string v1, "Failed to persist conflict contents!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_8
    .catch Lcom/google/android/gms/auth/q; {:try_start_8 .. :try_end_8} :catch_0
    .catchall {:try_start_8 .. :try_end_8} :catchall_0

    goto/16 :goto_4

    :cond_e
    move-object v1, v7

    goto/16 :goto_5

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
