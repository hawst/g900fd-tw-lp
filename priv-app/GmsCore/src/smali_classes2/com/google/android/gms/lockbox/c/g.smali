.class public final Lcom/google/android/gms/lockbox/c/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/lockbox/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 53
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 139
    new-instance v0, Lcom/google/android/gms/lockbox/c/j;

    invoke-direct {v0, p0, p1, v1, v1}, Lcom/google/android/gms/lockbox/c/j;-><init>(Lcom/google/android/gms/lockbox/c/g;Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/gms/lockbox/c/h;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/lockbox/c/h;-><init>(Lcom/google/android/gms/lockbox/c/g;Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;Lcom/google/android/gms/lockbox/g;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/gms/lockbox/c/i;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/lockbox/c/i;-><init>(Lcom/google/android/gms/lockbox/c/g;Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;Lcom/google/android/gms/lockbox/g;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
