.class final Lcom/google/android/gms/common/download/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/view/MenuItem$OnMenuItemClickListener;


# instance fields
.field final synthetic a:Landroid/content/Context;

.field final synthetic b:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 221
    iput-object p1, p0, Lcom/google/android/gms/common/download/o;->b:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    iput-object p2, p0, Lcom/google/android/gms/common/download/o;->a:Landroid/content/Context;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onMenuItemClick(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 224
    iget-object v0, p0, Lcom/google/android/gms/common/download/o;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->f()Lcom/google/android/gms/common/download/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/download/DownloadService;->c(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 225
    iget-object v0, p0, Lcom/google/android/gms/common/download/o;->a:Landroid/content/Context;

    invoke-static {}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->f()Lcom/google/android/gms/common/download/f;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/gms/common/download/f;->a:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 226
    new-instance v0, Lcom/google/android/gms/common/download/q;

    invoke-direct {v0}, Lcom/google/android/gms/common/download/q;-><init>()V

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/common/download/o;->b:Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/common/download/DownloadServiceSettingsActivity;->getSupportFragmentManager()Landroid/support/v4/app/v;

    move-result-object v1

    const-string v2, "image"

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/app/m;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 234
    :goto_0
    const/4 v0, 0x1

    return v0

    .line 229
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/download/o;->a:Landroid/content/Context;

    const-string v1, "Not downloaded."

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0

    .line 232
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/download/o;->a:Landroid/content/Context;

    const-string v1, "Not enabled."

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
