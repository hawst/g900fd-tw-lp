.class final Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 19
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 2

    .prologue
    .line 30
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 42
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->f:Z

    :goto_0
    return v0

    .line 33
    :pswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/eg;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->b()V

    .line 38
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->f:Z

    goto :goto_0

    .line 34
    :catch_0
    move-exception v0

    .line 35
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 30
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 25
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->c()V

    .line 26
    return-void
.end method
