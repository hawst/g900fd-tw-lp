.class final Lcom/google/android/gms/games/a/bx;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Comparable;


# instance fields
.field final a:Ljava/lang/String;

.field final b:Landroid/content/ContentValues;

.field final c:Ljava/util/Set;


# direct methods
.method public constructor <init>(Landroid/content/ContentValues;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 1457
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1458
    iput-object p2, p0, Lcom/google/android/gms/games/a/bx;->a:Ljava/lang/String;

    .line 1459
    iput-object p1, p0, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    .line 1460
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    .line 1461
    iget-object v0, p0, Lcom/google/android/gms/games/a/bx;->c:Ljava/util/Set;

    invoke-interface {v0, p2}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 1462
    return-void
.end method

.method private b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 1465
    iget-object v0, p0, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 1469
    iget-object v0, p0, Lcom/google/android/gms/games/a/bx;->b:Landroid/content/ContentValues;

    const-string v1, "expiration_timestamp"

    invoke-virtual {v0, v1}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    return-wide v0
.end method

.method public final synthetic compareTo(Ljava/lang/Object;)I
    .locals 4

    .prologue
    .line 1452
    check-cast p1, Lcom/google/android/gms/games/a/bx;

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/bx;->a()J

    move-result-wide v0

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/bx;->a()J

    move-result-wide v2

    sub-long/2addr v0, v2

    long-to-int v0, v0

    if-eqz v0, :cond_0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/a/bx;->b()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p1}, Lcom/google/android/gms/games/a/bx;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    goto :goto_0
.end method
