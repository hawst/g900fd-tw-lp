.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/r;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:J

.field private j:Lcom/google/android/gms/common/server/ClientContext;

.field private k:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/google/android/gms/games/c/a;->N:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/r;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;I)V

    .line 32
    return-void
.end method


# virtual methods
.method public final a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V
    .locals 1

    .prologue
    .line 39
    iput-wide p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->e:J

    .line 40
    iput-object p3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->j:Lcom/google/android/gms/common/server/ClientContext;

    .line 41
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->c()V

    .line 43
    return-void
.end method

.method public final b(Landroid/os/Message;)Z
    .locals 5

    .prologue
    .line 47
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 67
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->g:Z

    :goto_0
    return v0

    .line 49
    :sswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/w;

    .line 50
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/w;->a:Lcom/google/android/gms/games/internal/ConnectionInfo;

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(Z)V

    .line 51
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/w;->a:Lcom/google/android/gms/games/internal/ConnectionInfo;

    if-nez v1, :cond_1

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b()V

    .line 58
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->f:Z

    goto :goto_0

    .line 50
    :cond_0
    const/4 v1, 0x0

    goto :goto_1

    .line 55
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->e:J

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->j:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/w;->a:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    goto :goto_2

    .line 61
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 62
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    const/16 v1, 0xa

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 64
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->f:Z

    goto :goto_0

    .line 47
    nop

    :sswitch_data_0
    .sparse-switch
        0x18 -> :sswitch_0
        0x1d -> :sswitch_1
    .end sparse-switch
.end method
