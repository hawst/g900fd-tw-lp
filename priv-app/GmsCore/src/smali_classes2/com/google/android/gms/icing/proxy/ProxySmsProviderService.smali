.class public Lcom/google/android/gms/icing/proxy/ProxySmsProviderService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "Icing.Proxy.ProxySmsProviderService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 23
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 28
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-ge v0, v1, :cond_0

    .line 46
    :goto_0
    return-void

    .line 34
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/icing/proxy/SmsContentProvider;->a(Landroid/content/Context;)V

    .line 36
    new-instance v1, Lcom/google/android/gms/appdatasearch/c;

    invoke-virtual {p0}, Lcom/google/android/gms/icing/proxy/ProxySmsProviderService;->getBaseContext()Landroid/content/Context;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/appdatasearch/c;-><init>(Landroid/content/Context;)V

    .line 37
    const-wide/16 v2, 0x1388

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/appdatasearch/c;->a(J)Lcom/google/android/gms/common/c;

    move-result-object v0

    .line 38
    sget-object v2, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;

    if-eq v0, v2, :cond_1

    .line 39
    const-string v0, "Fail to connect AppDataSearchClient in ProxySMSProviderService."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_0

    .line 43
    :cond_1
    :try_start_0
    const-string v0, "icing.proxy.sms"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/appdatasearch/c;->a(Ljava/lang/String;J)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 45
    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/c;->b()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/c;->b()V

    throw v0
.end method
