.class public final Lcom/google/android/gms/games/service/a/m/a;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/service/b;

.field private final e:Lcom/google/android/gms/games/a/au;

.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/games/snapshot/d;

.field private final h:Lcom/google/android/gms/drive/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V
    .locals 1

    .prologue
    .line 29
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 30
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/a;->c:Lcom/google/android/gms/games/internal/dr;

    .line 31
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/m/a;->d:Lcom/google/android/gms/games/service/b;

    .line 32
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/a;->e:Lcom/google/android/gms/games/a/au;

    .line 33
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/m/a;->f:Ljava/lang/String;

    .line 34
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/m/a;->g:Lcom/google/android/gms/games/snapshot/d;

    .line 35
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/m/a;->h:Lcom/google/android/gms/drive/m;

    .line 36
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/a;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 53
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/a;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->H(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 48
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 41
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/a;->e:Lcom/google/android/gms/games/a/au;

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/a;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/m/a;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/m/a;->g:Lcom/google/android/gms/games/snapshot/d;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/m/a;->h:Lcom/google/android/gms/drive/m;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
