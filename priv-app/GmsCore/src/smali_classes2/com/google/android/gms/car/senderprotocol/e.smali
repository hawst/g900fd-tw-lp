.class public final Lcom/google/android/gms/car/senderprotocol/e;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/car/ii;

.field private static final b:Lcom/google/android/gms/car/ii;


# instance fields
.field private final c:I

.field private final d:Lcom/google/android/gms/car/senderprotocol/f;

.field private final e:I

.field private final f:Lcom/google/android/gms/car/senderprotocol/k;

.field private g:I

.field private h:Z

.field private final i:Lcom/google/android/gms/car/senderprotocol/g;

.field private final j:Ljava/lang/Object;

.field private final k:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 35
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.channel_lock_latency"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/e;->a:Lcom/google/android/gms/car/ii;

    .line 38
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.channel_send_latency"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/e;->b:Lcom/google/android/gms/car/ii;

    return-void
.end method

.method public constructor <init>(IILcom/google/android/gms/car/senderprotocol/k;Lcom/google/android/gms/car/senderprotocol/f;Landroid/os/Handler;)V
    .locals 1

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    .line 70
    iput p1, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    .line 71
    iput p2, p0, Lcom/google/android/gms/car/senderprotocol/e;->e:I

    .line 72
    iput-object p4, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    .line 73
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/g;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/g;-><init>(Lcom/google/android/gms/car/senderprotocol/e;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->i:Lcom/google/android/gms/car/senderprotocol/g;

    .line 74
    iput-object p3, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    .line 75
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    .line 76
    iput-object p5, p0, Lcom/google/android/gms/car/senderprotocol/e;->k:Landroid/os/Handler;

    .line 77
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/senderprotocol/e;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/senderprotocol/e;I)I
    .locals 0

    .prologue
    .line 33
    iput p1, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    return p1
.end method

.method private static a(ILcom/google/protobuf/nano/j;)Ljava/nio/ByteBuffer;
    .locals 4

    .prologue
    .line 207
    invoke-virtual {p1}, Lcom/google/protobuf/nano/j;->getSerializedSize()I

    move-result v0

    add-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 208
    int-to-short v1, p0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 209
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v2

    add-int/lit8 v2, v2, 0x2

    invoke-virtual {p1}, Lcom/google/protobuf/nano/j;->getSerializedSize()I

    move-result v3

    invoke-static {p1, v1, v2, v3}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;[BII)V

    .line 211
    invoke-virtual {p1}, Lcom/google/protobuf/nano/j;->getSerializedSize()I

    move-result v1

    add-int/lit8 v1, v1, 0x2

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 212
    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/senderprotocol/e;)Z
    .locals 1

    .prologue
    .line 33
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->h:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/senderprotocol/e;)Z
    .locals 1

    .prologue
    .line 33
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->h:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/senderprotocol/e;)I
    .locals 1

    .prologue
    .line 33
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/car/senderprotocol/e;)Lcom/google/android/gms/car/senderprotocol/k;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/car/senderprotocol/e;)Lcom/google/android/gms/car/senderprotocol/f;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    return-object v0
.end method

.method static synthetic i()Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 33
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    return-object v0
.end method


# virtual methods
.method public final a()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->k:Landroid/os/Handler;

    return-object v0
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    .line 133
    :goto_0
    return-void

    .line 132
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/senderprotocol/f;->a(Ljava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;Z)V
    .locals 4

    .prologue
    .line 154
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/e;->a:Lcom/google/android/gms/car/ii;

    sget-object v0, Lcom/google/android/gms/car/senderprotocol/e;->b:Lcom/google/android/gms/car/ii;

    .line 156
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 159
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/e;->a:Lcom/google/android/gms/car/ii;

    .line 160
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    const/4 v2, 0x2

    if-eq v0, v2, :cond_0

    .line 166
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Channel must be open before sending a message."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 169
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    iget-boolean v3, v0, Lcom/google/android/gms/car/senderprotocol/k;->f:Z

    if-nez v3, :cond_1

    const/4 v3, 0x0

    invoke-virtual {v0, v2, p1, v3, p2}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILjava/nio/ByteBuffer;ZZ)V

    .line 170
    :cond_1
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/e;->b:Lcom/google/android/gms/car/ii;

    .line 171
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    .line 85
    return-void
.end method

.method public final b(Ljava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 142
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 144
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->slice()Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 145
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/e;->i:Lcom/google/android/gms/car/senderprotocol/g;

    iget-object v3, v2, Lcom/google/android/gms/car/senderprotocol/g;->a:Landroid/util/SparseArray;

    invoke-virtual {v3, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/j;

    if-eqz v0, :cond_0

    iget-object v2, v2, Lcom/google/android/gms/car/senderprotocol/g;->b:Lcom/google/android/gms/car/senderprotocol/e;

    iget-object v2, v2, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    invoke-interface {v0, v1}, Lcom/google/android/gms/car/senderprotocol/j;->a(Ljava/nio/ByteBuffer;)V

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 146
    :cond_0
    return-void

    .line 145
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    return v0
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 95
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 97
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    const/4 v2, 0x4

    if-eq v0, v2, :cond_0

    .line 98
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "Channel needs to be closed before it can be opened."

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 102
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 101
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    .line 102
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final e()V
    .locals 6

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v1

    .line 111
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    if-eqz v0, :cond_0

    .line 112
    monitor-exit v1

    .line 119
    :goto_0
    return-void

    .line 114
    :cond_0
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    iget v3, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    iget v4, p0, Lcom/google/android/gms/car/senderprotocol/e;->e:I

    new-instance v5, Lcom/google/android/c/b/q;

    invoke-direct {v5}, Lcom/google/android/c/b/q;-><init>()V

    iput v4, v5, Lcom/google/android/c/b/q;->a:I

    iput v3, v5, Lcom/google/android/c/b/q;->b:I

    const/4 v3, 0x7

    invoke-static {v3, v5}, Lcom/google/android/gms/car/senderprotocol/e;->a(ILcom/google/protobuf/nano/j;)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILjava/nio/ByteBuffer;ZZ)V

    .line 119
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()V
    .locals 6

    .prologue
    .line 184
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    packed-switch v0, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    const-string v2, "Unhandled Channel State"

    invoke-direct {v0, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :pswitch_0
    :try_start_1
    monitor-exit v1

    :goto_0
    return-void

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/car/senderprotocol/f;->a(I)V

    monitor-exit v1

    goto :goto_0

    :pswitch_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->h:Z

    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    new-instance v3, Lcom/google/android/c/b/p;

    invoke-direct {v3}, Lcom/google/android/c/b/p;-><init>()V

    const/16 v4, 0x9

    invoke-static {v4, v3}, Lcom/google/android/gms/car/senderprotocol/e;->a(ILcom/google/protobuf/nano/j;)Ljava/nio/ByteBuffer;

    move-result-object v3

    const/4 v4, 0x1

    const/4 v5, 0x1

    invoke-virtual {v0, v2, v3, v4, v5}, Lcom/google/android/gms/car/senderprotocol/k;->a(ILjava/nio/ByteBuffer;ZZ)V

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/car/senderprotocol/f;->a(I)V

    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final g()Lcom/google/android/gms/car/senderprotocol/k;
    .locals 1

    .prologue
    .line 259
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->f:Lcom/google/android/gms/car/senderprotocol/k;

    return-object v0
.end method

.method public final h()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 267
    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/e;->j:Ljava/lang/Object;

    monitor-enter v2

    .line 269
    :try_start_0
    const-string v0, "CAR.GAL"

    const/4 v3, 0x3

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 270
    const-string v0, "CAR.GAL"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Force closing channel "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v4, p0, Lcom/google/android/gms/car/senderprotocol/e;->c:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 272
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_2

    move v0, v1

    .line 273
    :goto_0
    const/4 v3, 0x4

    iput v3, p0, Lcom/google/android/gms/car/senderprotocol/e;->g:I

    .line 274
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 275
    if-eqz v0, :cond_1

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/e;->d:Lcom/google/android/gms/car/senderprotocol/f;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/senderprotocol/f;->a(I)V

    .line 278
    :cond_1
    return-void

    .line 272
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
