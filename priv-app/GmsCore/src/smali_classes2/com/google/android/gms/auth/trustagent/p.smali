.class final Lcom/google/android/gms/auth/trustagent/p;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)V
    .locals 0

    .prologue
    .line 160
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/p;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 12

    .prologue
    const-wide/16 v10, 0xa

    const/4 v8, 0x0

    .line 160
    new-instance v0, Lcom/google/android/gms/common/api/w;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/p;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/location/places/q;->b:Lcom/google/android/gms/common/api/c;

    sget-object v2, Lcom/google/android/gms/auth/trustagent/trustlet/aw;->a:Lcom/google/android/gms/location/places/s;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/p;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    sget-object v1, Lcom/google/android/gms/location/places/UserDataType;->b:Lcom/google/android/gms/location/places/UserDataType;

    invoke-interface {v0, v3, v1}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/location/places/UserDataType;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v10, v11, v1}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/personalized/d;

    const/4 v1, 0x0

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/d;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v1, 0x4

    new-array v2, v1, [Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/d;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/location/places/personalized/PlaceUserData;

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->c()Ljava/util/List;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_1
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/personalized/PlaceAlias;

    const-string v6, "Home"

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->c()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->b()Ljava/lang/String;

    move-result-object v6

    sget-object v1, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    new-array v7, v8, [Ljava/lang/String;

    invoke-interface {v1, v3, v6, v7}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v10, v11, v7}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/w;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v7

    if-eqz v7, :cond_1

    aput-object v6, v2, v8

    const/4 v6, 0x1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    goto :goto_0

    :cond_2
    const-string v6, "Work"

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/personalized/PlaceAlias;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/location/places/personalized/PlaceUserData;->b()Ljava/lang/String;

    move-result-object v6

    sget-object v1, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    new-array v7, v8, [Ljava/lang/String;

    invoke-interface {v1, v3, v6, v7}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    sget-object v7, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v10, v11, v7}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/location/places/w;

    if-eqz v1, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v7

    if-eqz v7, :cond_1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v7

    if-eqz v7, :cond_1

    const/4 v7, 0x2

    aput-object v6, v2, v7

    const/4 v6, 0x3

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v2, v6

    goto/16 :goto_0

    :cond_3
    move-object v0, v2

    :goto_1
    return-object v0

    :cond_4
    move-object v0, v1

    goto :goto_1
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 160
    check-cast p1, [Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/p;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Landroid/os/AsyncTask;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/p;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->c(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;)Lcom/google/android/gms/auth/trustagent/bh;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/auth/trustagent/bh;->a([Ljava/lang/String;)V

    :cond_0
    return-void
.end method
