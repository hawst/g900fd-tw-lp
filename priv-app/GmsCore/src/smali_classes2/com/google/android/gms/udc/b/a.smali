.class public final Lcom/google/android/gms/udc/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 8
    const-string v0, "gms.udc.cache_enabled"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->a:Lcom/google/android/gms/common/a/d;

    .line 11
    const-string v0, "gms.udc.verbose_logging"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->b:Lcom/google/android/gms/common/a/d;

    .line 14
    const-string v0, "gms.udc.apiary_trace"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->c:Lcom/google/android/gms/common/a/d;

    .line 17
    const-string v0, "gms.udc.udc_server_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->d:Lcom/google/android/gms/common/a/d;

    .line 20
    const-string v0, "gms.udc.udc_server_api_path"

    const-string v1, "/userdatacontrols/v1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->e:Lcom/google/android/gms/common/a/d;

    .line 23
    const-string v0, "gms.udc.udc_backend_override"

    const-string v1, ""

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->f:Lcom/google/android/gms/common/a/d;

    .line 26
    const-string v0, "gms.udc.udc_auth_scope"

    const-string v1, "https://www.googleapis.com/auth/user_data_controls"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->g:Lcom/google/android/gms/common/a/d;

    .line 29
    const-string v0, "gms.udc.google_settings_visible"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->h:Lcom/google/android/gms/common/a/d;

    .line 32
    const-string v0, "gms.udc.location_setting_alias"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->i:Lcom/google/android/gms/common/a/d;

    .line 38
    const-string v0, "gms.udc.analytics_tracking_id"

    const-string v1, "UA-25279800-3"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->j:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "gms.udc.use_webview_for_links"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/udc/b/a;->k:Lcom/google/android/gms/common/a/d;

    return-void
.end method
