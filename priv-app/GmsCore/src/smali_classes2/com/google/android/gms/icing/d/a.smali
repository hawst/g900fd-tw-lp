.class public final Lcom/google/android/gms/icing/d/a;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method private static a(Ljava/io/File;)Lcom/google/android/gms/icing/r;
    .locals 4

    .prologue
    .line 156
    invoke-virtual {p0}, Ljava/io/File;->exists()Z

    move-result v0

    if-nez v0, :cond_0

    .line 157
    new-instance v0, Ljava/io/IOException;

    const-string v1, "no status"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 159
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->length()J

    move-result-wide v0

    .line 160
    const-wide/16 v2, 0x4

    cmp-long v2, v0, v2

    if-gez v2, :cond_1

    .line 161
    new-instance v0, Ljava/io/IOException;

    const-string v1, "file too small"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 166
    :cond_1
    :try_start_0
    new-instance v2, Ljava/io/FileInputStream;

    invoke-direct {v2, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 171
    long-to-int v0, v0

    new-array v0, v0, [B

    .line 172
    :try_start_1
    invoke-virtual {v2, v0}, Ljava/io/FileInputStream;->read([B)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v1

    .line 176
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 178
    const/4 v2, 0x4

    if-ge v1, v2, :cond_2

    .line 179
    new-instance v0, Ljava/io/IOException;

    const-string v1, "could not read status"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 167
    :catch_0
    move-exception v0

    .line 168
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 176
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    throw v0

    .line 181
    :cond_2
    const/4 v2, 0x0

    add-int/lit8 v1, v1, -0x4

    invoke-static {v0, v2, v1}, Lcom/google/protobuf/nano/a;->a([BII)Lcom/google/protobuf/nano/a;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/r;->a(Lcom/google/protobuf/nano/a;)Lcom/google/android/gms/icing/r;

    move-result-object v0

    return-object v0
.end method

.method private static a(Lcom/google/android/gms/icing/r;Ljava/io/File;)V
    .locals 6

    .prologue
    .line 189
    invoke-virtual {p1}, Ljava/io/File;->getParentFile()Ljava/io/File;

    move-result-object v0

    .line 190
    invoke-virtual {v0}, Ljava/io/File;->mkdirs()Z

    .line 191
    invoke-virtual {v0}, Ljava/io/File;->isDirectory()Z

    move-result v1

    if-nez v1, :cond_0

    .line 192
    new-instance v1, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Creating dirs "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 194
    :cond_0
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 195
    invoke-virtual {p1}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    new-instance v0, Ljava/io/IOException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Deleting "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 201
    :cond_1
    :try_start_0
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, p1}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/FileNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 206
    :try_start_1
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    .line 207
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    .line 208
    const/4 v2, 0x4

    new-array v2, v2, [B

    invoke-static {v2}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-static {}, Ljava/nio/ByteOrder;->nativeOrder()Ljava/nio/ByteOrder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/nio/ByteBuffer;->order(Ljava/nio/ByteOrder;)Ljava/nio/ByteBuffer;

    move-result-object v2

    new-instance v3, Ljava/util/zip/CRC32;

    invoke-direct {v3}, Ljava/util/zip/CRC32;-><init>()V

    invoke-virtual {v3, v0}, Ljava/util/zip/CRC32;->update([B)V

    invoke-virtual {v3}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v4

    long-to-int v0, v4

    const v3, 0x1e10bd66

    xor-int/2addr v0, v3

    invoke-virtual {v2, v0}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    .line 209
    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 211
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    .line 212
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 213
    return-void

    .line 202
    :catch_0
    move-exception v0

    .line 203
    new-instance v1, Ljava/io/IOException;

    invoke-direct {v1, v0}, Ljava/io/IOException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 211
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/FileDescriptor;->sync()V

    .line 212
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    throw v0
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/icing/impl/q;Ljava/io/File;)Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 120
    new-instance v3, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cur/ds.status.flushed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v3, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 124
    :try_start_0
    invoke-static {v3}, Lcom/google/android/gms/icing/d/a;->a(Ljava/io/File;)Lcom/google/android/gms/icing/r;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 130
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    .line 131
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 132
    iget v6, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {v5, v6, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 125
    :catch_0
    move-exception v0

    const-string v1, "Failed to read status proto."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 151
    :goto_1
    return v2

    :cond_0
    move v1, v2

    .line 135
    :goto_2
    iget-object v0, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 136
    iget-object v0, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    aget-object v6, v0, v1

    .line 137
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v5, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 138
    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/aa;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aa;-><init>()V

    .line 140
    :goto_3
    iput-object v0, v6, Lcom/google/android/gms/icing/s;->e:Lcom/google/android/gms/icing/aa;

    .line 141
    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "half time for "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ": "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v0, v0, Lcom/google/android/gms/icing/aa;->a:I

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 135
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 138
    :cond_1
    invoke-virtual {p1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/icing/aa;

    move-result-object v0

    goto :goto_3

    .line 145
    :cond_2
    :try_start_1
    invoke-static {v4, v3}, Lcom/google/android/gms/icing/d/a;->a(Lcom/google/android/gms/icing/r;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 151
    const/4 v2, 0x1

    goto :goto_1

    .line 146
    :catch_1
    move-exception v0

    const-string v1, "Writing status proto failed."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Lcom/google/android/gms/icing/n;Ljava/io/File;)Z
    .locals 8

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 35
    new-instance v4, Lcom/google/android/gms/icing/r;

    invoke-direct {v4}, Lcom/google/android/gms/icing/r;-><init>()V

    .line 38
    const/4 v0, -0x1

    .line 39
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v0

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 40
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v1, v0}, Ljava/lang/Math;->max(II)I

    move-result v0

    move v1, v0

    .line 41
    goto :goto_0

    .line 42
    :cond_0
    if-gez v1, :cond_1

    move v2, v3

    .line 69
    :goto_1
    return v2

    .line 47
    :cond_1
    add-int/lit8 v0, v1, 0x1

    new-array v0, v0, [Lcom/google/android/gms/icing/s;

    iput-object v0, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    move v0, v2

    .line 48
    :goto_2
    iget-object v1, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 49
    iget-object v1, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    new-instance v5, Lcom/google/android/gms/icing/s;

    invoke-direct {v5}, Lcom/google/android/gms/icing/s;-><init>()V

    aput-object v5, v1, v0

    .line 48
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 52
    :cond_2
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 53
    iget-object v5, v4, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    iget-object v6, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v6, v6, Lcom/google/android/gms/icing/g;->a:I

    aget-object v5, v5, v6

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget-wide v6, v0, Lcom/google/android/gms/icing/j;->b:J

    iput-wide v6, v5, Lcom/google/android/gms/icing/s;->a:J

    goto :goto_3

    .line 57
    :cond_3
    iget-wide v0, p1, Lcom/google/android/gms/icing/n;->e:J

    iput-wide v0, v4, Lcom/google/android/gms/icing/r;->a:J

    .line 58
    iget v0, p1, Lcom/google/android/gms/icing/n;->a:I

    iput v0, v4, Lcom/google/android/gms/icing/r;->d:I

    .line 59
    iget-wide v0, p1, Lcom/google/android/gms/icing/n;->b:J

    iput-wide v0, v4, Lcom/google/android/gms/icing/r;->e:J

    .line 62
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v5, "/cur/ds.status.flushed"

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 64
    :try_start_0
    invoke-static {v4, v0}, Lcom/google/android/gms/icing/d/a;->a(Lcom/google/android/gms/icing/r;Ljava/io/File;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 69
    goto :goto_1

    .line 65
    :catch_0
    move-exception v0

    .line 66
    const-string v1, "Writing status proto failed: %s"

    invoke-virtual {v0}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_1
.end method

.method public static a(Ljava/util/List;Ljava/io/File;)Z
    .locals 14

    .prologue
    const/4 v3, 0x1

    const-wide/16 v6, 0x0

    const/4 v2, 0x0

    .line 77
    new-instance v8, Ljava/io/File;

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "/cur/ds.status.flushed"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v8, v0}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    .line 81
    :try_start_0
    invoke-static {v8}, Lcom/google/android/gms/icing/d/a;->a(Ljava/io/File;)Lcom/google/android/gms/icing/r;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 87
    new-instance v10, Ljava/util/HashMap;

    invoke-direct {v10}, Ljava/util/HashMap;-><init>()V

    .line 88
    invoke-interface {p0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 89
    iget v4, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    iget-wide v12, v0, Lcom/google/android/gms/icing/g;->i:J

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-interface {v10, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 82
    :catch_0
    move-exception v0

    const-string v1, "Failed to read status proto."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 111
    :goto_1
    return v2

    :cond_0
    move v1, v2

    .line 92
    :goto_2
    iget-object v0, v9, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    array-length v0, v0

    if-ge v1, v0, :cond_4

    .line 93
    iget-object v0, v9, Lcom/google/android/gms/icing/r;->b:[Lcom/google/android/gms/icing/s;

    aget-object v11, v0, v1

    .line 94
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {v10, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    .line 98
    :goto_3
    iget-wide v12, v11, Lcom/google/android/gms/icing/s;->b:J

    cmp-long v0, v12, v6

    if-eqz v0, :cond_1

    cmp-long v0, v4, v6

    if-nez v0, :cond_3

    :cond_1
    move v0, v3

    :goto_4
    iput-boolean v0, v11, Lcom/google/android/gms/icing/s;->d:Z

    .line 100
    iput-wide v4, v11, Lcom/google/android/gms/icing/s;->b:J

    .line 92
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    :cond_2
    move-wide v4, v6

    .line 94
    goto :goto_3

    :cond_3
    move v0, v2

    .line 98
    goto :goto_4

    .line 105
    :cond_4
    :try_start_1
    invoke-static {v9, v8}, Lcom/google/android/gms/icing/d/a;->a(Lcom/google/android/gms/icing/r;Ljava/io/File;)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    move v2, v3

    .line 111
    goto :goto_1

    .line 106
    :catch_1
    move-exception v0

    const-string v1, "Writing status proto failed."

    new-array v3, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_1
.end method
