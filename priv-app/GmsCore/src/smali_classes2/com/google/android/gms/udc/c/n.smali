.class public final Lcom/google/android/gms/udc/c/n;
.super Lcom/google/android/gms/udc/c/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/c/m;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/udc/c/m;)V
    .locals 0

    .prologue
    .line 273
    iput-object p1, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    invoke-direct {p0}, Lcom/google/android/gms/udc/c/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final c(Lcom/google/android/gms/common/api/Status;[B)V
    .locals 3

    .prologue
    .line 277
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 278
    new-instance v0, Lcom/google/android/gms/udc/e/e;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/e;-><init>()V

    .line 280
    :try_start_0
    invoke-static {v0, p2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    new-instance v2, Lcom/google/android/gms/udc/c/u;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/udc/c/u;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/udc/e/e;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/c/m;->a(Lcom/google/android/gms/common/api/ap;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 290
    :goto_0
    return-void

    .line 284
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v1}, Lcom/google/android/gms/udc/c/m;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/m;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0

    .line 288
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/n;->a:Lcom/google/android/gms/udc/c/m;

    invoke-static {p1}, Lcom/google/android/gms/udc/c/m;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/m;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0
.end method
