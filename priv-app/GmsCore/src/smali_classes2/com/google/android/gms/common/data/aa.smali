.class public final Lcom/google/android/gms/common/data/aa;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/data/d;


# instance fields
.field private final a:Lcom/google/android/gms/common/data/d;

.field private final b:[Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/data/d;Ljava/util/Comparator;)V
    .locals 3

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput-object p1, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    .line 26
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v0

    new-array v0, v0, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    .line 28
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 29
    iget-object v1, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v1, v0

    .line 28
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 32
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    new-instance v1, Lcom/google/android/gms/common/data/ab;

    invoke-direct {v1, p0, p2}, Lcom/google/android/gms/common/data/ab;-><init>(Lcom/google/android/gms/common/data/aa;Ljava/util/Comparator;)V

    invoke-static {v0, v1}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 38
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/aa;)Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 19
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    return-object v0
.end method


# virtual methods
.method public final a(I)Ljava/lang/Object;
    .locals 2

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    iget-object v1, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->b:[Ljava/lang/Integer;

    array-length v0, v0

    return v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 80
    return-void
.end method

.method public final e()Landroid/os/Bundle;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method public final iterator()Ljava/util/Iterator;
    .locals 1

    .prologue
    .line 89
    new-instance v0, Lcom/google/android/gms/common/data/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/common/data/e;-><init>(Lcom/google/android/gms/common/data/d;)V

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/common/data/aa;->a:Lcom/google/android/gms/common/data/d;

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->w_()V

    .line 66
    return-void
.end method
