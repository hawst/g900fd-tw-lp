.class public final Lcom/google/android/gms/games/service/a/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/service/e;


# instance fields
.field private final a:Lcom/google/android/gms/common/server/ClientContext;

.field private final b:Lcom/google/android/gms/games/internal/dr;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;)V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    .line 21
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/i;->b:Lcom/google/android/gms/games/internal/dr;

    .line 22
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 40
    return-void
.end method

.method public final a(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)V
    .locals 1

    .prologue
    .line 26
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/gms/games/i/a;->e(Landroid/content/Context;Ljava/lang/String;)V

    .line 27
    invoke-static {}, Lcom/google/android/gms/games/service/k;->b()V

    .line 28
    invoke-virtual {p2}, Lcom/google/android/gms/games/a/t;->c()V

    .line 31
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i;->b:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/i;->b:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/dr;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 37
    :cond_0
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method
