.class public final Lcom/google/android/gms/udc/ui/e;
.super Lcom/google/android/gms/common/api/ai;
.source "SourceFile"


# instance fields
.field private final f:Ljava/lang/String;

.field private final g:Lcom/google/android/gms/lockbox/g;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;IZ)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/common/api/ai;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;)V

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/udc/ui/e;->f:Ljava/lang/String;

    .line 31
    new-instance v0, Lcom/google/android/gms/lockbox/h;

    invoke-direct {v0}, Lcom/google/android/gms/lockbox/h;-><init>()V

    packed-switch p4, :pswitch_data_0

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/lockbox/h;->a()Lcom/google/android/gms/lockbox/g;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/e;->g:Lcom/google/android/gms/lockbox/g;

    .line 32
    return-void

    .line 31
    :pswitch_0
    invoke-virtual {v0, p5}, Lcom/google/android/gms/lockbox/h;->a(Z)Lcom/google/android/gms/lockbox/h;

    goto :goto_0

    :pswitch_1
    invoke-virtual {v0, p5}, Lcom/google/android/gms/lockbox/h;->b(Z)Lcom/google/android/gms/lockbox/h;

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x7
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 4

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/gms/lockbox/a;->b:Lcom/google/android/gms/lockbox/d;

    new-instance v1, Landroid/accounts/Account;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/e;->f:Ljava/lang/String;

    const-string v3, "com.google"

    invoke-direct {v1, v2, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/e;->g:Lcom/google/android/gms/lockbox/g;

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/lockbox/d;->a(Lcom/google/android/gms/common/api/v;Landroid/accounts/Account;Lcom/google/android/gms/lockbox/g;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method protected final bridge synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 0

    .prologue
    .line 22
    return-object p1
.end method

.method protected final k()Lcom/google/android/gms/common/api/l;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "getApiMethod must not be called since onStartRequest/createFailedResult are overridden."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
