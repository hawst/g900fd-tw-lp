.class final Lcom/google/android/gms/drive/internal/ay;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 286
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ay;->a:Lcom/google/android/gms/common/api/m;

    .line 287
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 298
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ay;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/ba;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/ba;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 299
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V
    .locals 5

    .prologue
    .line 291
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ay;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/ba;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v3, Lcom/google/android/gms/drive/internal/ar;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/internal/ar;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/ba;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/n;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 294
    return-void
.end method
