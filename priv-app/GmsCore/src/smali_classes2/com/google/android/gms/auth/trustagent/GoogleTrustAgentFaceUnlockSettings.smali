.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFaceUnlockSettings;
.super Lcom/google/android/gms/auth/trustagent/as;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 9
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/as;-><init>()V

    return-void
.end method


# virtual methods
.method protected final e()Landroid/preference/PreferenceFragment;
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/gms/auth/trustagent/g;

    invoke-direct {v0}, Lcom/google/android/gms/auth/trustagent/g;-><init>()V

    return-object v0
.end method

.method protected final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    const-string v0, "FACE_UNLOCK_FRAGMENT_TAG"

    return-object v0
.end method

.method final g()V
    .locals 2

    .prologue
    .line 23
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFaceUnlockSettings;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    const-string v1, "FACE_UNLOCK_FRAGMENT_TAG"

    invoke-virtual {v0, v1}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/g;

    .line 25
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/g;->d()V

    .line 26
    return-void
.end method
