.class final Lcom/google/android/gms/car/fq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/fo;

.field private final b:Lcom/google/android/gms/car/lz;

.field private c:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/fo;Lcom/google/android/gms/car/lz;)V
    .locals 0

    .prologue
    .line 448
    iput-object p1, p0, Lcom/google/android/gms/car/fq;->a:Lcom/google/android/gms/car/fo;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 449
    iput-object p2, p0, Lcom/google/android/gms/car/fq;->b:Lcom/google/android/gms/car/lz;

    .line 450
    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/gms/car/lz;
    .locals 1

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/gms/car/fq;->b:Lcom/google/android/gms/car/lz;

    return-object v0
.end method

.method public final a(I)V
    .locals 0

    .prologue
    .line 470
    iput p1, p0, Lcom/google/android/gms/car/fq;->c:I

    .line 471
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/car/fq;->b:Lcom/google/android/gms/car/lz;

    invoke-interface {v0}, Lcom/google/android/gms/car/lz;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 458
    return-void
.end method

.method public final binderDied()V
    .locals 3

    .prologue
    .line 462
    const-string v0, "CAR.MSG"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 463
    const-string v0, "CAR.MSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "binder death of client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/fq;->b:Lcom/google/android/gms/car/lz;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 465
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/fq;->b()V

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/car/fq;->a:Lcom/google/android/gms/car/fo;

    invoke-static {v0, p0}, Lcom/google/android/gms/car/fo;->a(Lcom/google/android/gms/car/fo;Lcom/google/android/gms/car/fq;)V

    .line 467
    return-void
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 474
    iget v0, p0, Lcom/google/android/gms/car/fq;->c:I

    return v0
.end method
