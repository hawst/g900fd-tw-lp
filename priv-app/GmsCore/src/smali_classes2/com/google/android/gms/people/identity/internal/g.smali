.class public final Lcom/google/android/gms/people/identity/internal/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/Map;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 489
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 490
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "home"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 491
    const/4 v1, 0x3

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "other"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 492
    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "work"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 493
    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, "custom"

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 494
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/identity/internal/g;->a:Ljava/util/Map;

    .line 495
    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 3

    .prologue
    .line 509
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/google/android/gms/people/identity/internal/f;->a(Lcom/google/android/gms/people/identity/i;I)Ljava/lang/Integer;

    move-result-object v0

    .line 510
    if-nez v0, :cond_0

    .line 511
    const/4 v0, 0x0

    .line 513
    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    const/4 v2, 0x2

    invoke-virtual {p1, v2}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v0, v2}, Landroid/provider/ContactsContract$CommonDataKinds$StructuredPostal;->getTypeLabel(Landroid/content/res/Resources;ILjava/lang/CharSequence;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 519
    const/4 v0, 0x1

    invoke-static {p0, v0}, Lcom/google/android/gms/people/identity/internal/f;->a(Lcom/google/android/gms/people/identity/i;I)Ljava/lang/Integer;

    move-result-object v0

    .line 520
    if-eqz v0, :cond_0

    sget-object v1, Lcom/google/android/gms/people/identity/internal/g;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 521
    sget-object v1, Lcom/google/android/gms/people/identity/internal/g;->a:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 525
    :goto_0
    return-object v0

    .line 524
    :cond_0
    const-string v1, "ContactData"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid StructuredPostal Type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 525
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final b(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 529
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final c(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 533
    const/4 v0, 0x4

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 541
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final e(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 545
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final f(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 549
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final g(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 553
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 554
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 555
    const/4 v0, 0x0

    .line 557
    :cond_0
    return-object v0
.end method

.method public static final h(Lcom/google/android/gms/people/identity/i;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 562
    const/16 v0, 0x9

    invoke-virtual {p0, v0}, Lcom/google/android/gms/people/identity/i;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 563
    if-eqz v0, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isDigitsOnly(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 566
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
