.class public abstract Lcom/google/android/gms/people/identity/a/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/people/identity/a/c;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/identity/a/c;->a:Ljava/lang/Object;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static a(Landroid/content/Context;)Lcom/google/android/gms/people/identity/a/c;
    .locals 3

    .prologue
    .line 40
    sget-object v1, Lcom/google/android/gms/people/identity/a/c;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 41
    :try_start_0
    sget-object v0, Lcom/google/android/gms/people/identity/a/c;->b:Lcom/google/android/gms/people/identity/a/c;

    if-nez v0, :cond_0

    .line 42
    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    .line 43
    new-instance v2, Lcom/google/android/gms/people/identity/a/d;

    invoke-direct {v2, v0}, Lcom/google/android/gms/people/identity/a/d;-><init>(Landroid/content/Context;)V

    sput-object v2, Lcom/google/android/gms/people/identity/a/c;->b:Lcom/google/android/gms/people/identity/a/c;

    .line 45
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 46
    sget-object v0, Lcom/google/android/gms/people/identity/a/c;->b:Lcom/google/android/gms/people/identity/a/c;

    return-object v0

    .line 45
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public abstract a()Ljava/util/List;
.end method
