.class public final Lcom/google/android/gms/icing/aw;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:Z

.field public c:Lcom/google/android/gms/icing/t;

.field public d:I

.field public e:Ljava/lang/String;

.field public f:Z

.field public g:J

.field public h:I

.field public i:I

.field public j:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 8741
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8742
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/gms/icing/aw;->b:Z

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    iput v2, p0, Lcom/google/android/gms/icing/aw;->d:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    iput-boolean v2, p0, Lcom/google/android/gms/icing/aw;->f:Z

    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/icing/aw;->g:J

    iput v2, p0, Lcom/google/android/gms/icing/aw;->h:I

    iput v2, p0, Lcom/google/android/gms/icing/aw;->i:I

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/aw;->cachedSize:I

    .line 8743
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    .line 8879
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8880
    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 8881
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8884
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aw;->b:Z

    if-eqz v1, :cond_1

    .line 8885
    const/4 v1, 0x2

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aw;->b:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8888
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-eqz v1, :cond_2

    .line 8889
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8892
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/aw;->d:I

    if-eqz v1, :cond_3

    .line 8893
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/aw;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8896
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 8897
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8900
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/icing/aw;->f:Z

    if-eqz v1, :cond_5

    .line 8901
    const/4 v1, 0x6

    iget-boolean v2, p0, Lcom/google/android/gms/icing/aw;->f:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 8904
    :cond_5
    iget-wide v2, p0, Lcom/google/android/gms/icing/aw;->g:J

    const-wide/16 v4, 0x0

    cmp-long v1, v2, v4

    if-eqz v1, :cond_6

    .line 8905
    const/4 v1, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/icing/aw;->g:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 8908
    :cond_6
    iget v1, p0, Lcom/google/android/gms/icing/aw;->h:I

    if-eqz v1, :cond_7

    .line 8909
    const/16 v1, 0x8

    iget v2, p0, Lcom/google/android/gms/icing/aw;->h:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8912
    :cond_7
    iget v1, p0, Lcom/google/android/gms/icing/aw;->i:I

    if-eqz v1, :cond_8

    .line 8913
    const/16 v1, 0x9

    iget v2, p0, Lcom/google/android/gms/icing/aw;->i:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 8916
    :cond_8
    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_9

    .line 8917
    const/16 v1, 0xa

    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8920
    :cond_9
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8762
    if-ne p1, p0, :cond_1

    .line 8817
    :cond_0
    :goto_0
    return v0

    .line 8765
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/aw;

    if-nez v2, :cond_2

    move v0, v1

    .line 8766
    goto :goto_0

    .line 8768
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/aw;

    .line 8769
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 8770
    iget-object v2, p1, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 8771
    goto :goto_0

    .line 8773
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 8774
    goto :goto_0

    .line 8776
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aw;->b:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aw;->b:Z

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 8777
    goto :goto_0

    .line 8779
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-nez v2, :cond_6

    .line 8780
    iget-object v2, p1, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-eqz v2, :cond_7

    move v0, v1

    .line 8781
    goto :goto_0

    .line 8784
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    iget-object v3, p1, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/t;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 8785
    goto :goto_0

    .line 8788
    :cond_7
    iget v2, p0, Lcom/google/android/gms/icing/aw;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/aw;->d:I

    if-eq v2, v3, :cond_8

    move v0, v1

    .line 8789
    goto :goto_0

    .line 8791
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    if-nez v2, :cond_9

    .line 8792
    iget-object v2, p1, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    if-eqz v2, :cond_a

    move v0, v1

    .line 8793
    goto :goto_0

    .line 8795
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_a

    move v0, v1

    .line 8796
    goto :goto_0

    .line 8798
    :cond_a
    iget-boolean v2, p0, Lcom/google/android/gms/icing/aw;->f:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/aw;->f:Z

    if-eq v2, v3, :cond_b

    move v0, v1

    .line 8799
    goto :goto_0

    .line 8801
    :cond_b
    iget-wide v2, p0, Lcom/google/android/gms/icing/aw;->g:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/aw;->g:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_c

    move v0, v1

    .line 8802
    goto :goto_0

    .line 8804
    :cond_c
    iget v2, p0, Lcom/google/android/gms/icing/aw;->h:I

    iget v3, p1, Lcom/google/android/gms/icing/aw;->h:I

    if-eq v2, v3, :cond_d

    move v0, v1

    .line 8805
    goto :goto_0

    .line 8807
    :cond_d
    iget v2, p0, Lcom/google/android/gms/icing/aw;->i:I

    iget v3, p1, Lcom/google/android/gms/icing/aw;->i:I

    if-eq v2, v3, :cond_e

    move v0, v1

    .line 8808
    goto :goto_0

    .line 8810
    :cond_e
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    if-nez v2, :cond_f

    .line 8811
    iget-object v2, p1, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    if-eqz v2, :cond_0

    move v0, v1

    .line 8812
    goto/16 :goto_0

    .line 8814
    :cond_f
    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 8815
    goto/16 :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v3, 0x4d5

    const/16 v2, 0x4cf

    const/4 v1, 0x0

    .line 8822
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8825
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/aw;->b:Z

    if-eqz v0, :cond_1

    move v0, v2

    :goto_1
    add-int/2addr v0, v4

    .line 8826
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v4

    .line 8828
    mul-int/lit8 v0, v0, 0x1f

    iget v4, p0, Lcom/google/android/gms/icing/aw;->d:I

    add-int/2addr v0, v4

    .line 8829
    mul-int/lit8 v4, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    if-nez v0, :cond_3

    move v0, v1

    :goto_3
    add-int/2addr v0, v4

    .line 8831
    mul-int/lit8 v0, v0, 0x1f

    iget-boolean v4, p0, Lcom/google/android/gms/icing/aw;->f:Z

    if-eqz v4, :cond_4

    :goto_4
    add-int/2addr v0, v2

    .line 8832
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/aw;->g:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/aw;->g:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v0, v2

    .line 8834
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/aw;->h:I

    add-int/2addr v0, v2

    .line 8835
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/aw;->i:I

    add-int/2addr v0, v2

    .line 8836
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    if-nez v2, :cond_5

    :goto_5
    add-int/2addr v0, v1

    .line 8838
    return v0

    .line 8822
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    :cond_1
    move v0, v3

    .line 8825
    goto :goto_1

    .line 8826
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/t;->hashCode()I

    move-result v0

    goto :goto_2

    .line 8829
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_3

    :cond_4
    move v2, v3

    .line 8831
    goto :goto_4

    .line 8836
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 8689
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aw;->b:Z

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/t;

    invoke-direct {v0}, Lcom/google/android/gms/icing/t;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aw;->d:I

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/aw;->f:Z

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/aw;->g:J

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    :pswitch_0
    iput v0, p0, Lcom/google/android/gms/icing/aw;->h:I

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/aw;->i:I

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x20 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x38 -> :sswitch_7
        0x40 -> :sswitch_8
        0x48 -> :sswitch_9
        0x52 -> :sswitch_a
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    .line 8844
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 8845
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8847
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aw;->b:Z

    if-eqz v0, :cond_1

    .line 8848
    const/4 v0, 0x2

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aw;->b:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8850
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    if-eqz v0, :cond_2

    .line 8851
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8853
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/aw;->d:I

    if-eqz v0, :cond_3

    .line 8854
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/aw;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8856
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 8857
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8859
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/icing/aw;->f:Z

    if-eqz v0, :cond_5

    .line 8860
    const/4 v0, 0x6

    iget-boolean v1, p0, Lcom/google/android/gms/icing/aw;->f:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 8862
    :cond_5
    iget-wide v0, p0, Lcom/google/android/gms/icing/aw;->g:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_6

    .line 8863
    const/4 v0, 0x7

    iget-wide v2, p0, Lcom/google/android/gms/icing/aw;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 8865
    :cond_6
    iget v0, p0, Lcom/google/android/gms/icing/aw;->h:I

    if-eqz v0, :cond_7

    .line 8866
    const/16 v0, 0x8

    iget v1, p0, Lcom/google/android/gms/icing/aw;->h:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8868
    :cond_7
    iget v0, p0, Lcom/google/android/gms/icing/aw;->i:I

    if-eqz v0, :cond_8

    .line 8869
    const/16 v0, 0x9

    iget v1, p0, Lcom/google/android/gms/icing/aw;->i:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 8871
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 8872
    const/16 v0, 0xa

    iget-object v1, p0, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 8874
    :cond_9
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8875
    return-void
.end method
