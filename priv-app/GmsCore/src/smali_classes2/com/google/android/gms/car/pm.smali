.class final Lcom/google/android/gms/car/pm;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/hq;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ph;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/ph;)V
    .locals 0

    .prologue
    .line 245
    iput-object p1, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/ph;B)V
    .locals 0

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/pm;-><init>(Lcom/google/android/gms/car/ph;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 262
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, "CAR.SETUP"

    const-string v1, "Car disconnected while in setup"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ph;->k_()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    const-string v1, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 266
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 270
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 271
    const-string v0, "CAR.SETUP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDrivingStatusChange: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v0}, Lcom/google/android/gms/car/ph;->c(Lcom/google/android/gms/car/ph;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/ph;->a(Lcom/google/android/gms/car/ph;I)V

    .line 277
    :cond_1
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/qc;)V
    .locals 3

    .prologue
    const/4 v2, 0x2

    .line 248
    const-string v0, "CAR.SETUP"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const-string v0, "CAR.SETUP"

    const-string v1, "onCarConnection"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/ph;->a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/qc;)Lcom/google/android/gms/car/qc;

    .line 253
    const-string v0, "CAR.SETUP"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 254
    const-string v0, "CAR.SETUP"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDrivingStatusChange: mCallbacks.getDrivingStatus()="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v2}, Lcom/google/android/gms/car/ph;->b(Lcom/google/android/gms/car/ph;)Lcom/google/android/gms/car/ht;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->r(Lcom/google/android/gms/car/gx;)I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 257
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/pm;->a:Lcom/google/android/gms/car/ph;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ph;->k_()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    const-string v1, "EVENT_CONNECTED_TO_CAR"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 258
    return-void
.end method
