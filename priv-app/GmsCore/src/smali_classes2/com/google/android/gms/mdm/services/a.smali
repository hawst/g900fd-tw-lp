.class final Lcom/google/android/gms/mdm/services/a;
.super Lcom/google/android/gms/mdm/d/e;
.source "SourceFile"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/mdm/d/e;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/mdm/d/a;)V
    .locals 9

    .prologue
    const/4 v2, 0x1

    const/4 v8, 0x0

    .line 70
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v3

    .line 73
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v7

    new-instance v6, Lcom/google/af/a/c/a/f;

    invoke-direct {v6}, Lcom/google/af/a/c/a/f;-><init>()V

    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    invoke-static {}, Lcom/google/android/gms/common/util/e;->a()J

    move-result-wide v0

    new-array v4, v2, [J

    iput-object v4, v6, Lcom/google/af/a/c/a/f;->b:[J

    iget-object v4, v6, Lcom/google/af/a/c/a/f;->b:[J

    aput-wide v0, v4, v8

    iput-wide v0, v6, Lcom/google/af/a/c/a/f;->d:J

    iput-boolean v2, v6, Lcom/google/af/a/c/a/f;->c:Z

    new-instance v0, Lcom/google/android/gms/mdm/a/c;

    sget-object v1, Lcom/google/android/gms/mdm/b/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-class v5, Lcom/google/af/a/c/a/g;

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/mdm/a/c;-><init>(Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/Class;Lcom/google/protobuf/nano/j;)V

    invoke-virtual {v7, v0}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 76
    :try_start_0
    invoke-virtual {v3}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/af/a/c/a/g;

    .line 77
    iget-object v0, v0, Lcom/google/af/a/c/a/g;->a:[Lcom/google/af/a/c/a/q;

    .line 78
    array-length v1, v0

    if-ne v1, v2, :cond_0

    .line 79
    const/4 v1, 0x0

    aget-object v0, v0, v1

    iget-object v0, v0, Lcom/google/af/a/c/a/q;->a:Lcom/google/af/a/c/a;

    .line 80
    const/4 v1, 0x0

    iget-object v0, v0, Lcom/google/af/a/c/a;->a:Ljava/lang/String;

    invoke-interface {p1, v1, v0}, Lcom/google/android/gms/mdm/d/a;->a(ILjava/lang/String;)V

    .line 89
    :goto_0
    return-void

    .line 82
    :cond_0
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/mdm/d/a;->a(ILjava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 85
    :catch_0
    move-exception v0

    const-string v0, "Failed to retrieve device name"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 87
    :catch_1
    move-exception v0

    const-string v0, "Failed to retrieve device name"

    new-array v1, v8, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/mdm/d/a;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    if-nez p2, :cond_0

    .line 96
    const/16 v0, 0xa

    invoke-interface {p1, v0}, Lcom/google/android/gms/mdm/d/a;->a(I)V

    .line 111
    :goto_0
    return-void

    .line 100
    :cond_0
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v0

    .line 101
    invoke-static {p2, v0, v0}, Lcom/google/android/gms/mdm/a/b;->a(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;

    .line 104
    :try_start_0
    invoke-virtual {v0}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    .line 105
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/gms/mdm/d/a;->a(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    const-string v0, "Failed to rename device name"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 109
    :catch_1
    move-exception v0

    const-string v0, "Failed to rename device name"

    new-array v1, v1, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/mdm/d/a;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 115
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 117
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v0

    .line 118
    const/4 v1, 0x0

    invoke-static {v1, v0, v0}, Lcom/google/android/gms/mdm/a/b;->a(Ljava/lang/String;Lcom/android/volley/x;Lcom/android/volley/w;)Lcom/android/volley/p;

    .line 121
    :try_start_0
    invoke-virtual {v0}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;

    .line 122
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Lcom/google/android/gms/mdm/d/a;->b(I)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 128
    :goto_0
    return-void

    .line 124
    :catch_0
    move-exception v0

    const-string v0, "Failed to clear device name"

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 126
    :catch_1
    move-exception v0

    const-string v0, "Failed to clear device name"

    new-array v1, v2, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method
