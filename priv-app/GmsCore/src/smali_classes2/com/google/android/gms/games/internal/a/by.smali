.class public final Lcom/google/android/gms/games/internal/a/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/request/d;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1

    .prologue
    .line 169
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 235
    new-instance v0, Lcom/google/android/gms/games/internal/a/ca;

    const/4 v3, 0x0

    const/4 v5, 0x1

    move-object v1, p0

    move-object v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ca;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/v;III)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 214
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 215
    invoke-virtual {v0, p2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 216
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/games/internal/a/bz;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/games/internal/a/bz;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/v;[Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;
    .locals 8

    .prologue
    .line 318
    new-instance v0, Lcom/google/android/gms/games/internal/a/cd;

    const/4 v5, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move v6, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/cd;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;III)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 295
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 296
    invoke-virtual {v0, p4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 297
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v1

    new-array v1, v1, [Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v5

    check-cast v5, [Ljava/lang/String;

    new-instance v0, Lcom/google/android/gms/games/internal/a/cc;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/cc;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/util/List;I[BI)Lcom/google/android/gms/common/api/am;
    .locals 8

    .prologue
    .line 279
    invoke-interface {p3}, Ljava/util/List;->size()I

    move-result v0

    new-array v4, v0, [Ljava/lang/String;

    .line 280
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v4

    if-ge v1, v0, :cond_0

    .line 281
    invoke-interface {p3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    aput-object v0, v4, v1

    .line 280
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 283
    :cond_0
    new-instance v0, Lcom/google/android/gms/games/internal/a/cb;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/internal/a/cb;-><init>(Lcom/google/android/gms/games/internal/a/by;Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;I[BI)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 158
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->q()V

    .line 159
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/request/c;)V
    .locals 2

    .prologue
    .line 152
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 153
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/internal/c;->d(Lcom/google/android/gms/common/api/aj;)V

    .line 154
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/request/c;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 250
    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v0

    .line 251
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v1

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/games/internal/c;->d(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V

    .line 253
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)I
    .locals 1

    .prologue
    .line 182
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->u()I

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 257
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->f(Ljava/lang/String;)V

    .line 258
    return-void
.end method
