.class public Lcom/google/android/gms/car/nk;
.super Lcom/google/android/gms/car/y;
.source "SourceFile"


# instance fields
.field protected final b:Lcom/google/android/gms/car/ae;

.field protected c:Lcom/google/android/gms/car/as;

.field protected d:Lcom/google/android/gms/car/oa;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/y;-><init>(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)V

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    .line 32
    return-void
.end method

.method private U()V
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    if-eqz v0, :cond_0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    invoke-virtual {v0}, Lcom/google/android/gms/car/as;->a()V

    .line 150
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    .line 152
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 88
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 89
    const-string v0, "CAR.CAM"

    const-string v1, "onWindowRemoved"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/nk;->u()V

    .line 92
    return-void
.end method

.method public final a(Landroid/content/Intent;)V
    .locals 0

    .prologue
    .line 112
    return-void
.end method

.method final a(Landroid/content/res/Configuration;I)V
    .locals 0

    .prologue
    .line 126
    return-void
.end method

.method public final a(Landroid/view/KeyEvent;)V
    .locals 0

    .prologue
    .line 102
    return-void
.end method

.method public final a(Landroid/view/MotionEvent;)V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    if-eqz v0, :cond_0

    .line 118
    invoke-static {p1}, Landroid/view/MotionEvent;->obtain(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    .line 119
    iget-object v1, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/oa;->a(Landroid/view/MotionEvent;)V

    .line 121
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/ol;II)V
    .locals 2

    .prologue
    .line 76
    const-string v0, "CAR.CAM"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    const-string v0, "CAR.CAM"

    const-string v1, "onWindowSurfaceAvailable called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    if-nez v0, :cond_1

    .line 82
    invoke-virtual {p0}, Lcom/google/android/gms/car/nk;->s()V

    .line 84
    :cond_1
    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 106
    const-class v0, Lcom/google/android/gms/car/nk;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method s()V
    .locals 7

    .prologue
    const/4 v2, 0x2

    .line 36
    const-string v0, "CAR.CAM"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 37
    const-string v0, "CAR.CAM"

    const-string v1, "onProjectionStart called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 39
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/y;->a:Lcom/google/android/gms/car/oi;

    .line 40
    if-nez v0, :cond_2

    .line 54
    :cond_1
    :goto_0
    return-void

    .line 43
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->m()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 44
    const-string v0, "CAR.CAM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 45
    const-string v0, "CAR.CAM"

    const-string v1, "Waiting for surface texture to start client"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->n()Lcom/google/android/gms/car/DrawingSpec;

    move-result-object v0

    .line 50
    iget v3, v0, Lcom/google/android/gms/car/DrawingSpec;->b:I

    iget v4, v0, Lcom/google/android/gms/car/DrawingSpec;->c:I

    iget v5, v0, Lcom/google/android/gms/car/DrawingSpec;->d:I

    iget-object v6, v0, Lcom/google/android/gms/car/DrawingSpec;->e:Landroid/view/Surface;

    const-string v0, "CAR.CAM"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    const-string v0, "CAR.CAM"

    const-string v1, "Create virtual display called"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v0

    const-string v1, "display"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/hardware/display/DisplayManager;

    iget-object v0, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    if-eqz v0, :cond_5

    invoke-direct {p0}, Lcom/google/android/gms/car/nk;->U()V

    :cond_5
    new-instance v0, Lcom/google/android/gms/car/as;

    iget-object v2, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    invoke-virtual {v2}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/as;-><init>(Landroid/hardware/display/DisplayManager;Ljava/lang/String;IIILandroid/view/Surface;)V

    iput-object v0, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    .line 51
    new-instance v0, Lcom/google/android/gms/car/oa;

    iget-object v1, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    invoke-virtual {v1}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/nk;->c:Lcom/google/android/gms/car/as;

    iget-object v2, v2, Lcom/google/android/gms/car/as;->a:Landroid/hardware/display/VirtualDisplay;

    invoke-virtual {v2}, Landroid/hardware/display/VirtualDisplay;->getDisplay()Landroid/view/Display;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/oa;-><init>(Landroid/content/Context;Landroid/view/Display;)V

    iput-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->b(Lcom/google/android/gms/car/y;)V

    goto :goto_0
.end method

.method final u()V
    .locals 2

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    if-eqz v0, :cond_0

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oa;->dismiss()V

    .line 60
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/nk;->d:Lcom/google/android/gms/car/oa;

    .line 62
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/nk;->U()V

    .line 64
    new-instance v0, Lcom/google/android/gms/car/nl;

    invoke-direct {v0, p0, p0}, Lcom/google/android/gms/car/nl;-><init>(Lcom/google/android/gms/car/nk;Lcom/google/android/gms/car/y;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 71
    return-void
.end method

.method final v()V
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/gms/car/nk;->b:Lcom/google/android/gms/car/ae;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/ae;->d(Lcom/google/android/gms/car/y;)V

    .line 131
    return-void
.end method
