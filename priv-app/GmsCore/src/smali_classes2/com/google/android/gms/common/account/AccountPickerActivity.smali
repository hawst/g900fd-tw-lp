.class public Lcom/google/android/gms/common/account/AccountPickerActivity;
.super Landroid/app/Activity;
.source "SourceFile"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# instance fields
.field private a:Ljava/util/Set;

.field private b:Ljava/util/Set;

.field private c:Ljava/lang/String;

.field private d:Z

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/util/ArrayList;

.field private i:I

.field private j:[Landroid/os/Parcelable;

.field private k:I

.field private l:Landroid/widget/Button;

.field private m:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 54
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 72
    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    .line 73
    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    .line 74
    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    .line 75
    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    .line 76
    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    .line 79
    iput v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 80
    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/account/AccountPickerActivity;I)I
    .locals 0

    .prologue
    .line 54
    iput p1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    return p1
.end method

.method private a(Ljava/lang/String;)Landroid/view/View;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 199
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, p1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v1

    .line 200
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 202
    :goto_0
    return-object v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/common/account/AccountPickerActivity;)Landroid/widget/Button;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    return-object v0
.end method

.method private a()Ljava/util/ArrayList;
    .locals 7

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    if-nez v0, :cond_5

    .line 309
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 310
    const/16 v1, 0x12

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->m:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/accounts/AccountManager;->getAccountsByTypeForPackage(Ljava/lang/String;Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v0

    :goto_0
    new-instance v2, Ljava/util/ArrayList;

    array-length v1, v0

    invoke-direct {v2, v1}, Ljava/util/ArrayList;-><init>(I)V

    array-length v3, v0

    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_4

    aget-object v4, v0, v1

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    if-eqz v5, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    invoke-interface {v5, v4}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_0
    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    iget-object v6, v4, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-interface {v5, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_2

    :cond_1
    invoke-virtual {v2, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_3
    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v0

    goto :goto_0

    :cond_4
    iput-object v2, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    .line 312
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    return-object v0
.end method

.method private a(Landroid/content/Intent;)Ljava/util/Set;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 535
    const/4 v0, 0x0

    .line 536
    const-string v2, "allowableAccountTypes"

    invoke-virtual {p1, v2}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 538
    if-eqz v3, :cond_2

    .line 539
    new-instance v0, Ljava/util/HashSet;

    array-length v2, v3

    invoke-direct {v0, v2}, Ljava/util/HashSet;-><init>(I)V

    .line 540
    array-length v4, v3

    move v2, v1

    :goto_0
    if-ge v2, v4, :cond_0

    aget-object v5, v3, v2

    .line 541
    invoke-interface {v0, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 540
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 543
    :cond_0
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/accounts/AccountManager;->getAuthenticatorTypes()[Landroid/accounts/AuthenticatorDescription;

    move-result-object v2

    .line 544
    new-instance v3, Ljava/util/HashSet;

    array-length v4, v2

    invoke-direct {v3, v4}, Ljava/util/HashSet;-><init>(I)V

    .line 545
    array-length v4, v2

    :goto_1
    if-ge v1, v4, :cond_1

    aget-object v5, v2, v1

    .line 546
    iget-object v5, v5, Landroid/accounts/AuthenticatorDescription;->type:Ljava/lang/String;

    invoke-interface {v3, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 545
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 548
    :cond_1
    invoke-interface {v0, v3}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 550
    :cond_2
    return-object v0
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 430
    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->m:Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 431
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->m:Ljava/lang/String;

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/common/internal/ay;->b(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 435
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 436
    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    const-string v1, "accountType"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 438
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 439
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    .line 440
    return-void
.end method

.method private b()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 443
    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 444
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.startChooseAccountTypeActivity()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 446
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/account/AccountTypePickerActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 447
    const/high16 v1, 0x80000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 448
    const-string v1, "allowableAccountTypes"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "allowableAccountTypes"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 451
    const-string v1, "addAccountOptions"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "addAccountOptions"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 454
    const-string v1, "addAccountRequiredFeatures"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "addAccountRequiredFeatures"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/Intent;

    .line 457
    const-string v1, "authTokenType"

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "authTokenType"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 459
    invoke-virtual {p0, v0, v4}, Lcom/google/android/gms/common/account/AccountPickerActivity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 460
    iput v4, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 461
    return-void
.end method

.method private b(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 385
    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    const-string v0, "AccountChooser"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "runAddAccountForAuthenticator: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 388
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addAccountOptions"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getBundleExtra(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v4

    .line 390
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "addAccountRequiredFeatures"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringArrayExtra(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 392
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authTokenType"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 394
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    move-object v1, p1

    move-object v6, p0

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/accounts/AccountManager;->addAccount(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 396
    return-void
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/4 v9, 0x2

    const/4 v3, 0x0

    .line 322
    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 324
    if-nez p2, :cond_1

    .line 327
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    .line 329
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 334
    :cond_1
    const/4 v0, -0x1

    if-ne p2, v0, :cond_4

    .line 335
    const/4 v0, 0x1

    if-ne p1, v0, :cond_6

    .line 336
    if-eqz p3, :cond_2

    .line 337
    const-string v0, "accountType"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 338
    if-eqz v0, :cond_2

    .line 339
    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 343
    :cond_2
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: unable to find account type, pretending the request was canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    :cond_3
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: unable to find added account, pretending the request was canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    :cond_4
    const-string v0, "AccountChooser"

    invoke-static {v0, v9}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 378
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onActivityResult: canceled"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_5
    invoke-virtual {p0, v3}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    .line 381
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    .line 345
    :cond_6
    if-ne p1, v9, :cond_3

    .line 349
    if-eqz p3, :cond_c

    .line 350
    const-string v0, "authAccount"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 351
    const-string v0, "accountType"

    invoke-virtual {p3, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 354
    :goto_1
    if-eqz v2, :cond_7

    if-nez v1, :cond_b

    .line 355
    :cond_7
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v5

    .line 356
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 357
    iget-object v7, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    array-length v8, v7

    move v4, v3

    :goto_2
    if-ge v4, v8, :cond_8

    aget-object v0, v7, v4

    .line 358
    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v6, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 357
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_2

    .line 360
    :cond_8
    array-length v4, v5

    move v0, v3

    :goto_3
    if-ge v0, v4, :cond_b

    aget-object v7, v5, v0

    .line 361
    invoke-interface {v6, v7}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_a

    .line 362
    iget-object v1, v7, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 363
    iget-object v0, v7, Landroid/accounts/Account;->type:Ljava/lang/String;

    .line 369
    :goto_4
    if-nez v1, :cond_9

    if-eqz v0, :cond_3

    .line 370
    :cond_9
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 360
    :cond_a
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_b
    move-object v0, v1

    move-object v1, v2

    goto :goto_4

    :cond_c
    move-object v2, v1

    goto :goto_1
.end method

.method public onCancelButtonClicked(Landroid/view/View;)V
    .locals 0

    .prologue
    .line 294
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->onBackPressed()V

    .line 295
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 87
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v4, "overrideTheme"

    invoke-virtual {v0, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    const-string v4, "overrideTheme"

    invoke-virtual {v0, v4, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    sget v0, Lcom/google/android/gms/q;->D:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setTheme(I)V

    .line 88
    :cond_0
    :goto_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 90
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->m:Ljava/lang/String;

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->m:Ljava/lang/String;

    if-nez v0, :cond_1

    .line 92
    const-string v0, "AccountChooser"

    const-string v4, "Unable to get caller identity"

    invoke-static {v0, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 98
    if-eqz p1, :cond_3

    .line 99
    const-string v0, "pendingRequest"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 100
    const-string v0, "existingAccounts"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    .line 104
    const-string v0, "selectedAccountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    .line 107
    const-string v0, "selectedAddAccount"

    invoke-virtual {p1, v0, v3}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    .line 121
    :cond_2
    :goto_1
    const-string v0, "allowableAccounts"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_9

    new-instance v1, Ljava/util/HashSet;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v5

    invoke-direct {v1, v5}, Ljava/util/HashSet;-><init>(I)V

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_2
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Parcelable;

    check-cast v0, Landroid/accounts/Account;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 87
    :pswitch_0
    sget v0, Lcom/google/android/gms/q;->E:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setTheme(I)V

    goto :goto_0

    .line 110
    :cond_3
    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 111
    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    .line 114
    const-string v0, "selectedAccount"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 116
    if-eqz v0, :cond_2

    .line 117
    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    goto :goto_1

    :cond_4
    move-object v0, v1

    .line 121
    :goto_3
    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->a:Ljava/util/Set;

    .line 122
    invoke-direct {p0, v4}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Landroid/content/Intent;)Ljava/util/Set;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    .line 123
    const-string v0, "alwaysPromptForAccount"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    .line 125
    const-string v0, "descriptionTextOverride"

    invoke-virtual {v4, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    .line 126
    const-string v0, "setGmsCoreAccount"

    invoke-virtual {v4, v0, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->g:Z

    .line 132
    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    if-nez v0, :cond_5

    .line 133
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_7

    const-string v4, "android.permission.GET_ACCOUNTS"

    invoke-virtual {v0, v4, v1}, Landroid/content/pm/PackageManager;->checkPermission(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_6

    const-string v0, "AccountChooser"

    const-string v1, "The calling package does not have the android.permission.GET_ACCOUNTS permission. Will display Chooser."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    :goto_4
    if-nez v0, :cond_8

    move v0, v2

    :goto_5
    iput-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    .line 135
    :cond_5
    return-void

    :cond_6
    move v0, v2

    .line 133
    goto :goto_4

    :cond_7
    const-string v0, "AccountChooser"

    const-string v1, "Could not get calling package."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v3

    goto :goto_4

    :cond_8
    move v0, v3

    goto :goto_5

    :cond_9
    move-object v0, v1

    goto :goto_3

    .line 87
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 268
    const-string v0, "AccountChooser"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 269
    const-string v0, "AccountChooser"

    const-string v1, "AccountPickerActivity.onDestroy()"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_0
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 272
    return-void
.end method

.method public onOkButtonClicked(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 299
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 301
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b()V

    .line 305
    :cond_0
    :goto_0
    return-void

    .line 302
    :cond_1
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 303
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected onResume()V
    .locals 9

    .prologue
    const/16 v8, 0x8

    const/4 v4, -0x1

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 208
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 212
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->h:Ljava/util/ArrayList;

    .line 218
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    if-nez v0, :cond_3

    .line 221
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 222
    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->e(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 224
    sget v0, Lcom/google/android/gms/p;->fb:I

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 227
    const-string v0, "AccountChooser"

    const-string v1, "User doesn\'t have the ability to add an Account. Nothing to choose."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    invoke-virtual {p0, v2}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    .line 230
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    .line 264
    :goto_0
    return-void

    .line 234
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-ne v0, v1, :cond_1

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b(Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->b()V

    goto :goto_0

    .line 243
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->e:Z

    if-nez v0, :cond_3

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ne v0, v1, :cond_3

    .line 244
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    .line 245
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 250
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v5

    invoke-static {p0}, Lcom/google/android/gms/common/util/a;->e(Landroid/content/Context;)Z

    move-result v6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v6, :cond_4

    move v0, v1

    :goto_1
    add-int/2addr v0, v3

    new-array v7, v0, [Ljava/lang/String;

    move v3, v2

    :goto_2
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_5

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v0, v7, v3

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_2

    :cond_4
    move v0, v2

    goto :goto_1

    :cond_5
    if-eqz v6, :cond_6

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/gms/p;->ec:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v7, v0

    .line 251
    :cond_6
    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->c:Ljava/lang/String;

    iget-boolean v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->d:Z

    if-eqz v0, :cond_a

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v3

    :cond_7
    :goto_3
    iput v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    .line 256
    sget v0, Lcom/google/android/gms/l;->X:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setContentView(I)V

    .line 257
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v3, "overrideCustomTheme"

    invoke-virtual {v0, v3}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_8

    const-string v3, "overrideCustomTheme"

    invoke-virtual {v0, v3, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 258
    :cond_8
    :goto_4
    iget-object v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->f:Ljava/lang/String;

    sget v0, Lcom/google/android/gms/j;->eg:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_e

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    :goto_5
    const v0, 0x102000a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ListView;

    new-instance v3, Landroid/widget/ArrayAdapter;

    const v5, 0x109000f

    invoke-direct {v3, p0, v5, v7}, Landroid/widget/ArrayAdapter;-><init>(Landroid/content/Context;I[Ljava/lang/Object;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->setChoiceMode(I)V

    invoke-virtual {v0, v2}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    new-instance v3, Lcom/google/android/gms/common/account/b;

    invoke-direct {v3, p0}, Lcom/google/android/gms/common/account/b;-><init>(Lcom/google/android/gms/common/account/AccountPickerActivity;)V

    invoke-virtual {v0, v3}, Landroid/widget/ListView;->setOnItemClickListener(Landroid/widget/AdapterView$OnItemClickListener;)V

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    if-eq v3, v4, :cond_9

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v3, v1}, Landroid/widget/ListView;->setItemChecked(IZ)V

    const-string v0, "AccountChooser"

    const/4 v3, 0x2

    invoke-static {v0, v3}, Landroid/util/Log;->isLoggable(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    const-string v0, "AccountChooser"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v5, "List item "

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " should be selected"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_9
    const v0, 0x102001a

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->l:Landroid/widget/Button;

    iget v3, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    if-eq v3, v4, :cond_f

    :goto_6
    invoke-virtual {v0, v1}, Landroid/widget/Button;->setEnabled(Z)V

    goto/16 :goto_0

    :cond_a
    move v3, v2

    .line 251
    :goto_7
    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v3, v0, :cond_b

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_7

    add-int/lit8 v0, v3, 0x1

    move v3, v0

    goto :goto_7

    :cond_b
    move v3, v4

    goto/16 :goto_3

    .line 257
    :pswitch_0
    const-string v0, "android:id/title"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    if-eqz v0, :cond_c

    const/16 v3, 0xe

    invoke-static {v3}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v3

    if-eqz v3, :cond_d

    sget v3, Lcom/google/android/gms/h;->p:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    :goto_8
    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setTextColor(I)V

    const/16 v3, 0x10

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setGravity(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    sget v5, Lcom/google/android/gms/g;->m:I

    invoke-virtual {v3, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    invoke-virtual {v0, v3, v2, v3, v2}, Landroid/widget/TextView;->setPadding(IIII)V

    :cond_c
    const-string v0, "android:id/titleDivider"

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a(Ljava/lang/String;)Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_8

    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_4

    :cond_d
    sget v3, Lcom/google/android/gms/h;->q:I

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setBackgroundResource(I)V

    goto :goto_8

    .line 258
    :cond_e
    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_5

    :cond_f
    move v1, v2

    .line 263
    goto :goto_6

    .line 257
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method protected onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 276
    invoke-super {p0, p1}, Landroid/app/Activity;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 277
    const-string v0, "pendingRequest"

    iget v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 278
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 279
    const-string v0, "existingAccounts"

    iget-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putParcelableArray(Ljava/lang/String;[Landroid/os/Parcelable;)V

    .line 281
    :cond_0
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 282
    iget v0, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    .line 283
    const-string v0, "selectedAddAccount"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 290
    :cond_1
    :goto_0
    return-void

    .line 285
    :cond_2
    const-string v0, "selectedAddAccount"

    const/4 v1, 0x0

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 286
    const-string v1, "selectedAccountName"

    invoke-direct {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->a()Ljava/util/ArrayList;

    move-result-object v0

    iget v2, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->k:I

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {p1, v1, v0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 3

    .prologue
    .line 401
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    .line 402
    const-string v1, "intent"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/content/Intent;

    .line 404
    if-eqz v0, :cond_0

    .line 405
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->i:I

    .line 406
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/common/account/AccountPickerActivity;->j:[Landroid/os/Parcelable;

    .line 407
    invoke-virtual {v0}, Landroid/content/Intent;->getFlags()I

    move-result v1

    const v2, -0x10000001

    and-int/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 408
    const/4 v1, 0x2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/common/account/AccountPickerActivity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1

    .line 422
    :goto_0
    return-void

    .line 412
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(I)V

    .line 413
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    :catch_1
    move-exception v0

    .line 418
    :cond_0
    :goto_1
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 419
    const-string v1, "errorMessage"

    const-string v2, "error communicating with server"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    const/4 v1, -0x1

    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    invoke-virtual {v2, v0}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->setResult(ILandroid/content/Intent;)V

    .line 421
    invoke-virtual {p0}, Lcom/google/android/gms/common/account/AccountPickerActivity;->finish()V

    goto :goto_0

    .line 417
    :catch_2
    move-exception v0

    goto :goto_1
.end method
