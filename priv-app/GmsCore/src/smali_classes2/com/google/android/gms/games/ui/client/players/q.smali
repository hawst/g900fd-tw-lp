.class public final Lcom/google/android/gms/games/ui/client/players/q;
.super Lcom/google/android/gms/games/ui/z;
.source "SourceFile"

# interfaces
.implements Landroid/support/v7/widget/bp;
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/gms/games/ui/c;
.implements Lcom/google/android/gms/games/ui/d/z;


# instance fields
.field private A:Lcom/google/android/gms/games/ui/client/players/i;

.field private B:Lcom/google/android/gms/games/ui/cy;

.field private C:Lcom/google/android/gms/games/ui/client/players/i;

.field private final D:Ljava/lang/Runnable;

.field private E:Lcom/google/android/gms/common/data/d;

.field private F:Lcom/google/android/gms/common/data/d;

.field private G:Lcom/google/android/gms/common/data/s;

.field private H:Lcom/google/android/gms/common/data/ah;

.field private I:Z

.field private J:Z

.field private K:Z

.field private L:Lcom/google/android/gms/games/ui/d/w;

.field private M:Ljava/lang/String;

.field private N:Ljava/lang/String;

.field private O:Z

.field private P:Z

.field private Q:Z

.field o:Z

.field private p:Lcom/google/android/gms/games/ui/client/players/w;

.field private q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private r:Lcom/google/android/gms/games/ui/cy;

.field private s:Lcom/google/android/gms/games/ui/bl;

.field private t:Lcom/google/android/gms/games/ui/client/players/u;

.field private u:Lcom/google/android/gms/games/ui/client/players/i;

.field private v:Lcom/google/android/gms/games/ui/bl;

.field private w:Lcom/google/android/gms/games/ui/client/players/i;

.field private x:Lcom/google/android/gms/games/ui/bl;

.field private y:Lcom/google/android/gms/games/ui/client/players/i;

.field private z:Lcom/google/android/gms/games/ui/bl;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/z;-><init>()V

    .line 106
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->o:Z

    .line 109
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/r;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/players/r;-><init>(Lcom/google/android/gms/games/ui/client/players/q;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->D:Ljava/lang/Runnable;

    .line 1078
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/ah;)Lcom/google/android/gms/common/data/ah;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->E:Lcom/google/android/gms/common/data/d;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/s;)Lcom/google/android/gms/common/data/s;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->G:Lcom/google/android/gms/common/data/s;

    return-object p1
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/games/ui/client/players/i;)Lcom/google/android/gms/games/ui/client/players/i;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    return-object p1
.end method

.method private a(Lcom/google/android/gms/common/api/v;Z)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 316
    new-instance v1, Lcom/google/android/gms/common/api/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/common/api/p;-><init>(Lcom/google/android/gms/common/api/v;)V

    .line 319
    invoke-static {p1}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v2

    .line 321
    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->K:Z

    if-eqz v3, :cond_0

    .line 322
    sget-object v3, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v4}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v4

    invoke-interface {v3, p1, v2, v4}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/r;

    move-result-object v5

    .line 332
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v3}, Lcom/google/android/gms/games/ui/client/players/w;->ab()I

    move-result v3

    packed-switch v3, :pswitch_data_0

    move-object v2, v0

    .line 344
    :goto_1
    sget-object v3, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v3, p1, p2}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/r;

    move-result-object v3

    .line 348
    sget-object v4, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v6}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v6

    invoke-interface {v4, p1, v6, p2}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;IZ)Lcom/google/android/gms/common/api/am;

    move-result-object v4

    invoke-virtual {v1, v4}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/r;

    move-result-object v4

    .line 354
    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v6}, Lcom/google/android/gms/games/ui/client/players/w;->aa()Ljava/util/ArrayList;

    move-result-object v6

    .line 356
    if-nez p2, :cond_1

    if-eqz v6, :cond_1

    invoke-virtual {v6}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-lez v7, :cond_1

    .line 357
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, p1, v6}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/r;

    move-result-object v6

    .line 364
    :goto_2
    new-instance v7, Lcom/google/android/gms/common/api/n;

    iget-object v0, v1, Lcom/google/android/gms/common/api/p;->a:Ljava/util/List;

    iget-object v1, v1, Lcom/google/android/gms/common/api/p;->b:Landroid/os/Looper;

    const/4 v8, 0x0

    invoke-direct {v7, v0, v1, v8}, Lcom/google/android/gms/common/api/n;-><init>(Ljava/util/List;Landroid/os/Looper;B)V

    .line 365
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/s;

    move-object v1, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/client/players/s;-><init>(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;Lcom/google/android/gms/common/api/r;)V

    invoke-virtual {v7, v0}, Lcom/google/android/gms/common/api/n;->a(Lcom/google/android/gms/common/api/aq;)V

    .line 548
    return-void

    :cond_0
    move-object v5, v0

    .line 325
    goto :goto_0

    .line 334
    :pswitch_0
    sget-object v3, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v3, p1, v2, p2}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/api/p;->a(Lcom/google/android/gms/common/api/am;)Lcom/google/android/gms/common/api/r;

    move-result-object v2

    goto :goto_1

    :cond_1
    move-object v6, v0

    .line 360
    goto :goto_2

    .line 332
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/players/q;Lcom/google/android/gms/common/data/d;)Lcom/google/android/gms/common/data/d;
    .locals 0

    .prologue
    .line 62
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->F:Lcom/google/android/gms/common/data/d;

    return-object p1
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/client/players/q;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->P:Z

    return v0
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/client/players/q;)Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->P:Z

    return v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/client/players/q;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->O:Z

    return v0
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/q;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    return-object v0
.end method

.method static synthetic j(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    return-object v0
.end method

.method static synthetic k(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->E:Lcom/google/android/gms/common/data/d;

    return-object v0
.end method

.method static synthetic l(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    return-object v0
.end method

.method static synthetic m(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/d;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->F:Lcom/google/android/gms/common/data/d;

    return-object v0
.end method

.method static synthetic n(Lcom/google/android/gms/games/ui/client/players/q;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->I:Z

    return v0
.end method

.method static synthetic o(Lcom/google/android/gms/games/ui/client/players/q;)Z
    .locals 1

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->J:Z

    return v0
.end method

.method static synthetic p(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/s;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->G:Lcom/google/android/gms/common/data/s;

    return-object v0
.end method

.method static synthetic q(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/i;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    return-object v0
.end method

.method static synthetic r(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/common/data/ah;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    return-object v0
.end method

.method static synthetic s(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/d/t;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    return-object v0
.end method

.method static synthetic t(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/d/t;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    return-object v0
.end method


# virtual methods
.method public final G_()V
    .locals 3

    .prologue
    .line 691
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 693
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 695
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onEndOfWindowReached: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 707
    :goto_0
    return-void

    .line 699
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v2}, Lcom/google/android/gms/games/ui/d/ab;->a(Landroid/content/Context;)I

    move-result v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/players/t;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/players/t;-><init>(Lcom/google/android/gms/games/ui/client/players/q;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public final H_()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1011
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->A()V

    .line 1012
    invoke-virtual {p0, v1, v1}, Lcom/google/android/gms/games/ui/client/players/q;->c(ZZ)V

    .line 1013
    return-void
.end method

.method public final T()Z
    .locals 1

    .prologue
    .line 1019
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->isDetached()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/z;->isRemoving()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 269
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    .line 271
    if-nez v0, :cond_0

    .line 273
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->finish()V

    .line 297
    :goto_0
    return-void

    .line 279
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    .line 280
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 282
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onGoogleApiClientConnected: no current account! Bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->finish()V

    goto :goto_0

    .line 287
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Ljava/lang/String;)V

    .line 288
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Ljava/lang/String;)V

    .line 289
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Ljava/lang/String;)V

    .line 290
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Ljava/lang/String;)V

    .line 291
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/common/api/v;Z)V

    .line 293
    sget-object v0, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/k;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->q()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->Q:Z

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->g()V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/z;)V
    .locals 3

    .prologue
    .line 562
    invoke-interface {p1}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 563
    invoke-interface {p1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v1

    .line 567
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->isDetached()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->isRemoving()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 591
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    :cond_1
    :goto_0
    return-void

    .line 572
    :cond_2
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/q;->b(I)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v2

    if-nez v2, :cond_3

    .line 591
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    goto :goto_0

    .line 576
    :cond_3
    :try_start_2
    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/al;->a(I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 577
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->e()V

    .line 580
    :cond_4
    new-instance v0, Lcom/google/android/gms/common/data/ah;

    const-string v2, "profile_name"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/data/ah;-><init>(Lcom/google/android/gms/common/data/a;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/ah;->c()I

    move-result v0

    if-lez v0, :cond_1

    .line 585
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/common/data/d;)V

    .line 586
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 587
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->k()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 592
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    throw v0
.end method

.method final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 921
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    if-nez v2, :cond_0

    .line 976
    :goto_0
    return-void

    .line 926
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    sget-object v4, Lcom/google/android/gms/common/data/ah;->c:Lcom/google/android/gms/common/data/ag;

    invoke-virtual {v2, v3, v4, p1}, Lcom/google/android/gms/common/data/ah;->a(Landroid/content/Context;Lcom/google/android/gms/common/data/ag;Ljava/lang/String;)V

    .line 929
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 930
    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->o:Z

    .line 931
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    if-eqz v2, :cond_1

    .line 932
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->I:Z

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/client/players/i;->b(Z)V

    .line 933
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->J:Z

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    .line 936
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->E:Lcom/google/android/gms/common/data/d;

    invoke-static {v2}, Lcom/google/android/gms/common/data/k;->c(Lcom/google/android/gms/common/data/d;)Z

    move-result v2

    .line 937
    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->K:Z

    if-eqz v3, :cond_4

    .line 938
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 939
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    if-nez v2, :cond_2

    move v0, v1

    :cond_2
    invoke-virtual {v3, v0}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 944
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 945
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 946
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/games/ui/c;)V

    .line 947
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->c()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    .line 948
    const-string v1, "savedStatePageToken"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 949
    if-eqz v1, :cond_3

    .line 950
    const-string v2, "next_page_token"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 953
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->k()V

    .line 975
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    goto :goto_0

    .line 941
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 942
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    goto :goto_1

    .line 955
    :cond_5
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->o:Z

    .line 956
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    if-eqz v1, :cond_6

    .line 957
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->b(Z)V

    .line 958
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->d(Z)V

    .line 961
    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 962
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 963
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 964
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 965
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 966
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 967
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 968
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 969
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/games/ui/c;)V

    .line 970
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->c()Lcom/google/android/gms/common/data/d;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/data/d;->e()Landroid/os/Bundle;

    move-result-object v0

    .line 971
    const-string v1, "next_page_token"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 972
    const-string v2, "savedStatePageToken"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2
.end method

.method final a(Z)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 652
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->K:Z

    .line 653
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/i;->e(Z)V

    .line 655
    if-eqz p1, :cond_0

    .line 656
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/cf;->b(Landroid/content/Context;Ljava/lang/String;)V

    .line 660
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 661
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 662
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 663
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->A()V

    .line 666
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 667
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 668
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;Z)V

    .line 669
    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/common/api/v;Z)V

    .line 671
    :cond_1
    return-void
.end method

.method public final a(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 805
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 806
    if-lez v1, :cond_0

    .line 808
    :goto_0
    if-ge v0, v1, :cond_2

    .line 809
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->w()V

    .line 808
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 811
    :cond_0
    if-gez v1, :cond_1

    .line 813
    :goto_1
    neg-int v2, v1

    if-ge v0, v2, :cond_2

    .line 814
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->x()V

    .line 813
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 818
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v1

    .line 819
    :goto_2
    if-ge v0, v1, :cond_2

    .line 820
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->x()V

    .line 819
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 823
    :cond_2
    const/4 v0, 0x1

    return v0
.end method

.method final b(Z)V
    .locals 1

    .prologue
    .line 897
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/players/i;->f(Z)V

    .line 898
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/players/i;->f(Z)V

    .line 899
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/ui/client/players/i;->f(Z)V

    .line 900
    return-void
.end method

.method public final b(ZZ)V
    .locals 4

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->M:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Ljava/lang/String;Z)V

    .line 229
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/q;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 230
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 231
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "logAddToCirclesAction: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 238
    :goto_0
    return-void

    .line 234
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/k;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->a()Ljava/lang/String;

    move-result-object v0

    .line 236
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    const/4 v3, 0x2

    invoke-static {v1, v0, v2, v3, p2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZ)V

    goto :goto_0
.end method

.method final c(ZZ)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 848
    if-eqz p1, :cond_1

    .line 849
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 850
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->P:Z

    .line 852
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 853
    if-eqz p2, :cond_0

    .line 855
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 856
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 857
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 861
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 862
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 863
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 864
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    .line 867
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 871
    :cond_0
    invoke-direct {p0, v0, v3}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/common/api/v;Z)V

    .line 877
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 878
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 879
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 880
    return-void

    .line 873
    :cond_2
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "refresh: googleApiClient not connected, ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final d(ZZ)V
    .locals 1

    .prologue
    .line 886
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    if-eqz v0, :cond_0

    .line 887
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/games/ui/client/players/i;->b(ZZ)V

    .line 889
    :cond_0
    return-void
.end method

.method protected final i()V
    .locals 2

    .prologue
    .line 301
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 302
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 303
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/players/q;->a(Lcom/google/android/gms/common/api/v;Z)V

    .line 307
    :goto_0
    return-void

    .line 305
    :cond_0
    const-string v0, "SelectPlayersListFrag"

    const-string v1, "onPlayersChanged: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method final j()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 551
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v1}, Lcom/google/android/gms/games/ui/client/players/w;->X()I

    move-result v1

    invoke-virtual {v0, v2, v2, v2, v1}, Landroid/widget/ListView;->setPadding(IIII)V

    .line 553
    return-void
.end method

.method final k()V
    .locals 7

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 603
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->n()I

    move-result v2

    if-nez v2, :cond_0

    .line 604
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    .line 609
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-static {v2}, Lcom/google/android/gms/common/data/k;->c(Lcom/google/android/gms/common/data/d;)Z

    move-result v2

    .line 610
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->G:Lcom/google/android/gms/common/data/s;

    invoke-static {v3}, Lcom/google/android/gms/common/data/k;->c(Lcom/google/android/gms/common/data/d;)Z

    move-result v3

    .line 611
    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/q;->F:Lcom/google/android/gms/common/data/d;

    invoke-static {v4}, Lcom/google/android/gms/common/data/k;->c(Lcom/google/android/gms/common/data/d;)Z

    move-result v4

    .line 612
    iget-object v5, p0, Lcom/google/android/gms/games/ui/client/players/q;->E:Lcom/google/android/gms/common/data/d;

    invoke-static {v5}, Lcom/google/android/gms/common/data/k;->c(Lcom/google/android/gms/common/data/d;)Z

    move-result v5

    .line 614
    iget-boolean v6, p0, Lcom/google/android/gms/games/ui/client/players/q;->K:Z

    if-eqz v6, :cond_2

    .line 615
    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 616
    iget-object v6, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    if-nez v5, :cond_1

    :goto_1
    invoke-virtual {v6, v0}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 622
    :goto_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 623
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v3}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 624
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->t()V

    .line 630
    return-void

    .line 606
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    goto :goto_0

    :cond_1
    move v0, v1

    .line 616
    goto :goto_1

    .line 618
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 619
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    goto :goto_2
.end method

.method final l()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 675
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->K:Z

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/i;->e(Z)V

    .line 678
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 679
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 680
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/i;->a(Z)V

    .line 681
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->B()V

    .line 683
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 684
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 685
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;Z)V

    .line 687
    :cond_0
    return-void
.end method

.method public final m()V
    .locals 2

    .prologue
    .line 833
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->r:Lcom/google/android/gms/games/ui/cy;

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->Q:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v0

    if-lez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->Z()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    .line 836
    return-void

    .line 833
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final n()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 979
    .line 983
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    if-eqz v0, :cond_2

    .line 984
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/ah;->c()I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 988
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->o:Z

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->G:Lcom/google/android/gms/common/data/s;

    if-eqz v2, :cond_1

    .line 989
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->G:Lcom/google/android/gms/common/data/s;

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/s;->c()I

    move-result v2

    add-int/lit8 v2, v2, 0x0

    .line 992
    :goto_1
    iget-boolean v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->o:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->F:Lcom/google/android/gms/common/data/d;

    if-eqz v3, :cond_0

    .line 993
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->F:Lcom/google/android/gms/common/data/d;

    invoke-interface {v1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v1

    add-int/lit8 v1, v1, 0x0

    .line 996
    :cond_0
    add-int/2addr v0, v2

    add-int/2addr v0, v1

    return v0

    :cond_1
    move v2, v1

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method final o()I
    .locals 1

    .prologue
    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->H:Lcom/google/android/gms/common/data/ah;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/ah;->c()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final onActivityCreated(Landroid/os/Bundle;)V
    .locals 13

    .prologue
    const/4 v12, 0x5

    const/4 v11, 0x4

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 153
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/z;->onActivityCreated(Landroid/os/Bundle;)V

    .line 155
    iput-boolean v8, p0, Lcom/google/android/gms/games/ui/client/players/q;->O:Z

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/w;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->V()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->I:Z

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->p:Lcom/google/android/gms/games/ui/client/players/w;

    invoke-interface {v0}, Lcom/google/android/gms/games/ui/client/players/w;->W()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->J:Z

    .line 159
    sget-object v0, Lcom/google/android/gms/games/ui/n;->k:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_0

    .line 161
    iput-boolean v8, p0, Lcom/google/android/gms/games/ui/client/players/q;->J:Z

    .line 163
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getParentFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 167
    new-instance v0, Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->aG:I

    sget v3, Lcom/google/android/gms/p;->kZ:I

    sget v4, Lcom/google/android/gms/p;->mj:I

    const-string v6, "autoMatchTile"

    const-string v7, "gotIt"

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/ui/cy;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->r:Lcom/google/android/gms/games/ui/cy;

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->r:Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    .line 175
    new-instance v0, Lcom/google/android/gms/games/ui/bl;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/bl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    sget v1, Lcom/google/android/gms/p;->lg:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bl;->a(I)V

    .line 177
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-direct {v0, v1, v2, p0, v10}, Lcom/google/android/gms/games/ui/client/players/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    .line 179
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/games/ui/bl;->a(Z)V

    .line 181
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/client/players/u;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    .line 182
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/games/ui/client/players/u;->a(Z)V

    .line 184
    new-instance v0, Lcom/google/android/gms/games/ui/bl;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/bl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    sget v1, Lcom/google/android/gms/p;->ll:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bl;->a(I)V

    .line 186
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-direct {v0, v1, v2, p0, v11}, Lcom/google/android/gms/games/ui/client/players/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    .line 189
    new-instance v0, Lcom/google/android/gms/games/ui/bl;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/bl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    sget v1, Lcom/google/android/gms/p;->lb:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bl;->a(I)V

    .line 191
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-direct {v0, v1, v2, p0, v12}, Lcom/google/android/gms/games/ui/client/players/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    .line 194
    new-instance v0, Lcom/google/android/gms/games/ui/bl;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/bl;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    .line 195
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    sget v1, Lcom/google/android/gms/p;->lf:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bl;->a(I)V

    .line 196
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, p0, v3}, Lcom/google/android/gms/games/ui/client/players/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/view/View$OnClickListener;I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/games/ui/c;)V

    .line 200
    new-instance v0, Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->aH:I

    sget v3, Lcom/google/android/gms/p;->lh:I

    sget v4, Lcom/google/android/gms/p;->ij:I

    const-string v6, "findPeople"

    const-string v7, "findPeople"

    move-object v5, p0

    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/games/ui/cy;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    .line 205
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    invoke-virtual {v0, v8}, Lcom/google/android/gms/games/ui/cy;->a(Z)V

    .line 207
    new-instance v0, Lcom/google/android/gms/games/ui/bt;

    const/16 v1, 0xb

    new-array v1, v1, [Landroid/widget/BaseAdapter;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->r:Lcom/google/android/gms/games/ui/cy;

    aput-object v2, v1, v8

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->s:Lcom/google/android/gms/games/ui/bl;

    aput-object v2, v1, v9

    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->t:Lcom/google/android/gms/games/ui/client/players/u;

    aput-object v3, v1, v2

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    aput-object v2, v1, v10

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->v:Lcom/google/android/gms/games/ui/bl;

    aput-object v2, v1, v11

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    aput-object v2, v1, v12

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->x:Lcom/google/android/gms/games/ui/bl;

    aput-object v3, v1, v2

    const/4 v2, 0x7

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    aput-object v3, v1, v2

    const/16 v2, 0x8

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->z:Lcom/google/android/gms/games/ui/bl;

    aput-object v3, v1, v2

    const/16 v2, 0x9

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    aput-object v3, v1, v2

    const/16 v2, 0xa

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->B:Lcom/google/android/gms/games/ui/cy;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/ui/bt;-><init>([Landroid/widget/BaseAdapter;)V

    .line 212
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/players/q;->a(Landroid/widget/ListAdapter;)V

    .line 215
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v9}, Landroid/widget/ListView;->setItemsCanFocus(Z)V

    .line 216
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    invoke-virtual {v0, v8}, Landroid/widget/ListView;->setBackgroundColor(I)V

    .line 217
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 8

    .prologue
    const/4 v5, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 711
    invoke-static {p1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    .line 712
    instance-of v1, v0, Lcom/google/android/gms/games/Player;

    if-eqz v1, :cond_3

    .line 716
    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 717
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v2

    .line 719
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    sget v3, Lcom/google/android/gms/j;->kg:I

    if-ne v1, v3, :cond_1

    .line 720
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->L:Lcom/google/android/gms/games/ui/d/w;

    if-eqz v1, :cond_0

    const-string v1, "SelectPlayersListFrag"

    const-string v2, "onManageCircles(): canceling the helper that was already running..."

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->L:Lcom/google/android/gms/games/ui/d/w;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/d/w;->b()V

    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->M:Ljava/lang/String;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    move-object v1, p0

    move-object v2, p0

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/ui/d/w;->a(Lcom/google/android/gms/games/Player;Lcom/google/android/gms/games/ui/d/z;Landroid/support/v4/app/Fragment;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/games/ui/d/w;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->L:Lcom/google/android/gms/games/ui/d/w;

    .line 801
    :goto_0
    return-void

    .line 725
    :cond_1
    sget v1, Lcom/google/android/gms/j;->oV:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    .line 726
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->a(Lcom/google/android/gms/games/Player;I)Z

    .line 729
    invoke-virtual {p1}, Landroid/view/View;->isInTouchMode()Z

    move-result v0

    if-nez v0, :cond_2

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/client/players/i;->b(Ljava/lang/String;)V

    .line 737
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    goto :goto_0

    .line 740
    :cond_3
    instance-of v1, v0, Ljava/lang/String;

    if-eqz v1, :cond_c

    move-object v1, v0

    .line 741
    check-cast v1, Ljava/lang/String;

    .line 742
    const-string v2, "auto_pick_item_add_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->w()V

    goto :goto_0

    .line 746
    :cond_4
    const-string v2, "auto_pick_item_remove_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 748
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->x()V

    goto :goto_0

    .line 750
    :cond_5
    const-string v2, "gotIt"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 751
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/d/al;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/ui/q;)Z

    move-result v1

    if-eqz v1, :cond_6

    const-string v0, "SelectPlayersListFrag"

    const-string v1, "setIdentitySharingConfirmed: not connected; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 752
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->m()V

    goto :goto_0

    .line 751
    :cond_6
    invoke-static {v0}, Lcom/google/android/gms/games/d;->f(Lcom/google/android/gms/common/api/v;)V

    iput-boolean v7, p0, Lcom/google/android/gms/games/ui/client/players/q;->Q:Z

    goto :goto_1

    .line 754
    :cond_7
    const-string v2, "autoMatchTile"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_8

    .line 756
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->e()V

    goto/16 :goto_0

    .line 758
    :cond_8
    const-string v2, "findPeople"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 760
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->J_()Z

    goto/16 :goto_0

    .line 762
    :cond_9
    const-string v2, "nearby_players_tag"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_f

    .line 763
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->h()Z

    move-result v0

    if-nez v0, :cond_b

    .line 765
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    const/16 v1, 0x14

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(I)V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->N:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/gms/games/ui/cf;->a(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-virtual {p0, v6}, Lcom/google/android/gms/games/ui/client/players/q;->a(Z)V

    goto/16 :goto_0

    :cond_a
    new-instance v0, Lcom/google/android/gms/games/ui/client/players/v;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/client/players/v;-><init>()V

    invoke-virtual {v0, p0, v6}, Lcom/google/android/gms/games/ui/b/a;->setTargetFragment(Landroid/support/v4/app/Fragment;I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->i:Lcom/google/android/gms/games/ui/q;

    const-string v2, "confirmationDialog"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/ui/d/a;->a(Landroid/support/v4/app/q;Landroid/support/v4/app/m;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 767
    :cond_b
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->l()V

    goto/16 :goto_0

    .line 772
    :cond_c
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 773
    sget v2, Lcom/google/android/gms/j;->nd:I

    if-ne v1, v2, :cond_f

    .line 774
    new-instance v0, Landroid/support/v7/widget/bn;

    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v7/widget/bn;-><init>(Landroid/content/Context;Landroid/view/View;)V

    .line 775
    sget v1, Lcom/google/android/gms/m;->d:I

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/bn;->a(I)V

    .line 778
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->z()I

    move-result v1

    .line 779
    if-nez v1, :cond_e

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v1

    if-lez v1, :cond_e

    .line 782
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v1

    if-lez v1, :cond_d

    .line 783
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/n;->k:I

    new-array v3, v7, [Ljava/lang/Object;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v6

    invoke-virtual {v1, v2, v7, v3}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 785
    iget-object v2, v0, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/i;

    const/4 v3, -0x1

    invoke-interface {v2, v6, v3, v6, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 787
    :cond_d
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->q:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v1

    if-lt v1, v5, :cond_e

    .line 788
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->ld:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 790
    iget-object v2, v0, Landroid/support/v7/widget/bn;->a:Landroid/support/v7/internal/view/menu/i;

    invoke-interface {v2, v6, v6, v7, v1}, Landroid/view/Menu;->add(IIILjava/lang/CharSequence;)Landroid/view/MenuItem;

    .line 793
    :cond_e
    iput-object p0, v0, Landroid/support/v7/widget/bn;->c:Landroid/support/v7/widget/bp;

    .line 794
    iget-object v0, v0, Landroid/support/v7/widget/bn;->b:Landroid/support/v7/internal/view/menu/v;

    invoke-virtual {v0}, Landroid/support/v7/internal/view/menu/v;->b()V

    goto/16 :goto_0

    .line 799
    :cond_f
    const-string v1, "SelectPlayersListFrag"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "onClick: unexpected tag \'"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "\'; View: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", id "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method public final onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 7

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/q;->j:Landroid/view/LayoutInflater;

    .line 142
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/client/players/q;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 143
    new-instance v0, Lcom/google/android/gms/games/ui/d/t;

    sget v2, Lcom/google/android/gms/j;->sm:I

    sget v3, Lcom/google/android/gms/j;->ln:I

    sget v4, Lcom/google/android/gms/j;->fw:I

    sget v5, Lcom/google/android/gms/j;->mv:I

    move-object v6, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/ui/d/t;-><init>(Landroid/view/View;IIIILcom/google/android/gms/games/ui/d/v;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->k:Lcom/google/android/gms/games/ui/d/t;

    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 148
    return-object v1
.end method

.method public final onDestroyView()V
    .locals 2

    .prologue
    .line 258
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->O:Z

    .line 259
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->w:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 260
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->y:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->A:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->u:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->a()V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->a()Landroid/widget/ListView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/q;->D:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/widget/ListView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 264
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onDestroyView()V

    .line 265
    return-void
.end method

.method public final onDetach()V
    .locals 0

    .prologue
    .line 251
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onDetach()V

    .line 253
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->h()V

    .line 254
    return-void
.end method

.method public final onStop()V
    .locals 3

    .prologue
    .line 242
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/q;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 243
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 244
    sget-object v1, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    const/4 v2, 0x0

    invoke-interface {v1, v0, v2}, Lcom/google/android/gms/games/y;->b(Lcom/google/android/gms/common/api/v;Z)V

    .line 246
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/z;->onStop()V

    .line 247
    return-void
.end method

.method final p()Z
    .locals 1

    .prologue
    .line 1098
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    if-nez v0, :cond_0

    .line 1099
    const/4 v0, 0x0

    .line 1101
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/q;->C:Lcom/google/android/gms/games/ui/client/players/i;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/i;->h()Z

    move-result v0

    goto :goto_0
.end method
