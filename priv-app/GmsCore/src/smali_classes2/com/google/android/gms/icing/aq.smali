.class public final Lcom/google/android/gms/icing/aq;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/aq;


# instance fields
.field public a:[Lcom/google/android/gms/icing/ar;

.field public b:[Lcom/google/android/gms/icing/as;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 6064
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 6065
    invoke-static {}, Lcom/google/android/gms/icing/ar;->a()[Lcom/google/android/gms/icing/ar;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    invoke-static {}, Lcom/google/android/gms/icing/as;->a()[Lcom/google/android/gms/icing/as;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/aq;->cachedSize:I

    .line 6066
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/aq;
    .locals 2

    .prologue
    .line 6047
    sget-object v0, Lcom/google/android/gms/icing/aq;->c:[Lcom/google/android/gms/icing/aq;

    if-nez v0, :cond_1

    .line 6048
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 6050
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/aq;->c:[Lcom/google/android/gms/icing/aq;

    if-nez v0, :cond_0

    .line 6051
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/aq;

    sput-object v0, Lcom/google/android/gms/icing/aq;->c:[Lcom/google/android/gms/icing/aq;

    .line 6053
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6055
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/aq;->c:[Lcom/google/android/gms/icing/aq;

    return-object v0

    .line 6053
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 6129
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 6130
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    array-length v2, v2

    if-lez v2, :cond_2

    move v2, v0

    move v0, v1

    .line 6131
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 6132
    iget-object v3, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v3, v3, v0

    .line 6133
    if-eqz v3, :cond_0

    .line 6134
    const/4 v4, 0x1

    invoke-static {v4, v3}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v3

    add-int/2addr v2, v3

    .line 6131
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v2

    .line 6139
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    array-length v2, v2

    if-lez v2, :cond_4

    .line 6140
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    array-length v2, v2

    if-ge v1, v2, :cond_4

    .line 6141
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    aget-object v2, v2, v1

    .line 6142
    if-eqz v2, :cond_3

    .line 6143
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v0, v2

    .line 6140
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6148
    :cond_4
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 6077
    if-ne p1, p0, :cond_1

    .line 6092
    :cond_0
    :goto_0
    return v0

    .line 6080
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/aq;

    if-nez v2, :cond_2

    move v0, v1

    .line 6081
    goto :goto_0

    .line 6083
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/aq;

    .line 6084
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    iget-object v3, p1, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3

    move v0, v1

    .line 6086
    goto :goto_0

    .line 6088
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    iget-object v3, p1, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 6090
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 6097
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    invoke-static {v0}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v0

    add-int/lit16 v0, v0, 0x20f

    .line 6100
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 6102
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 5754
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/ar;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    new-instance v3, Lcom/google/android/gms/icing/ar;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ar;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    array-length v0, v0

    goto :goto_1

    :cond_3
    new-instance v3, Lcom/google/android/gms/icing/ar;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ar;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    if-nez v0, :cond_5

    move v0, v1

    :goto_3
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/as;

    if-eqz v0, :cond_4

    iget-object v3, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_4
    :goto_4
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_6

    new-instance v3, Lcom/google/android/gms/icing/as;

    invoke-direct {v3}, Lcom/google/android/gms/icing/as;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    array-length v0, v0

    goto :goto_3

    :cond_6
    new-instance v3, Lcom/google/android/gms/icing/as;

    invoke-direct {v3}, Lcom/google/android/gms/icing/as;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    goto/16 :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 6108
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    array-length v0, v0

    if-lez v0, :cond_1

    move v0, v1

    .line 6109
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 6110
    iget-object v2, p0, Lcom/google/android/gms/icing/aq;->a:[Lcom/google/android/gms/icing/ar;

    aget-object v2, v2, v0

    .line 6111
    if-eqz v2, :cond_0

    .line 6112
    const/4 v3, 0x1

    invoke-virtual {p1, v3, v2}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6109
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 6116
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    array-length v0, v0

    if-lez v0, :cond_3

    .line 6117
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    array-length v0, v0

    if-ge v1, v0, :cond_3

    .line 6118
    iget-object v0, p0, Lcom/google/android/gms/icing/aq;->b:[Lcom/google/android/gms/icing/as;

    aget-object v0, v0, v1

    .line 6119
    if-eqz v0, :cond_2

    .line 6120
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 6117
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 6124
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 6125
    return-void
.end method
