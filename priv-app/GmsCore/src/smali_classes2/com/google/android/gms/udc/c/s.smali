.class final Lcom/google/android/gms/udc/c/s;
.super Lcom/google/android/gms/udc/c/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/c/r;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/udc/c/r;)V
    .locals 0

    .prologue
    .line 309
    iput-object p1, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    invoke-direct {p0}, Lcom/google/android/gms/udc/c/l;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/udc/c/r;B)V
    .locals 0

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/google/android/gms/udc/c/s;-><init>(Lcom/google/android/gms/udc/c/r;)V

    return-void
.end method


# virtual methods
.method public final d(Lcom/google/android/gms/common/api/Status;[B)V
    .locals 3

    .prologue
    .line 313
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 314
    new-instance v0, Lcom/google/android/gms/udc/e/z;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/z;-><init>()V

    .line 316
    :try_start_0
    invoke-static {v0, p2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 317
    iget-object v1, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    new-instance v2, Lcom/google/android/gms/udc/c/x;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/udc/c/x;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/udc/e/z;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/c/r;->a(Lcom/google/android/gms/common/api/ap;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 326
    :goto_0
    return-void

    .line 320
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v1}, Lcom/google/android/gms/udc/c/r;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/r;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0

    .line 324
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/s;->a:Lcom/google/android/gms/udc/c/r;

    invoke-static {p1}, Lcom/google/android/gms/udc/c/r;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/r;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0
.end method
