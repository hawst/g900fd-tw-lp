.class final Lcom/google/android/gms/udc/ui/v;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

.field private final b:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;I)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/v;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput p2, p0, Lcom/google/android/gms/udc/ui/v;->b:I

    .line 206
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 6

    .prologue
    .line 210
    new-instance v0, Lcom/google/android/gms/udc/ui/e;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/v;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/v;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/v;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->b(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Ljava/lang/String;

    move-result-object v3

    iget v4, p0, Lcom/google/android/gms/udc/ui/v;->b:I

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/udc/ui/e;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Ljava/lang/String;IZ)V

    return-object v0
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 224
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 5

    .prologue
    .line 200
    check-cast p2, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "UdcSettingDetailActivity"

    const-string v1, "Error disabling device-setting %d status:%s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget v4, p0, Lcom/google/android/gms/udc/ui/v;->b:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    return-void
.end method
