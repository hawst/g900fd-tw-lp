.class final Lcom/google/android/gms/car/im;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ij;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ij;)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/gms/car/im;->a:Lcom/google/android/gms/car/ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 404
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/car/im;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v2}, Lcom/google/android/gms/car/ij;->g(Lcom/google/android/gms/car/ij;)J

    move-result-wide v2

    sub-long/2addr v0, v2

    .line 405
    const-wide/16 v2, 0x708

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/im;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->d(Lcom/google/android/gms/car/ij;)Landroid/location/Location;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 406
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 407
    const-string v0, "CAR.SENSOR"

    const-string v1, "Location is stale."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 409
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/im;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->h(Lcom/google/android/gms/car/ij;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 411
    :cond_1
    return-void
.end method
