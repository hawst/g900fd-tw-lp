.class public abstract Lcom/google/android/gms/people/internal/a/b;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static a:Lcom/google/android/gms/people/internal/a/d;

.field public static b:Lcom/google/android/gms/people/internal/a/c;


# instance fields
.field private final c:C

.field private final d:C

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/os/Bundle;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 233
    new-instance v0, Lcom/google/android/gms/people/internal/a/d;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/internal/a/d;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/people/internal/a/b;->a:Lcom/google/android/gms/people/internal/a/d;

    .line 234
    new-instance v0, Lcom/google/android/gms/people/internal/a/c;

    sget-object v1, Landroid/os/Bundle;->EMPTY:Landroid/os/Bundle;

    invoke-direct {v0, v1}, Lcom/google/android/gms/people/internal/a/c;-><init>(Landroid/os/Bundle;)V

    sput-object v0, Lcom/google/android/gms/people/internal/a/b;->b:Lcom/google/android/gms/people/internal/a/c;

    return-void
.end method

.method constructor <init>(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/people/internal/a/b;->g:Landroid/os/Bundle;

    .line 35
    const/4 v0, 0x1

    iput-char v0, p0, Lcom/google/android/gms/people/internal/a/b;->c:C

    .line 36
    const/4 v0, 0x2

    iput-char v0, p0, Lcom/google/android/gms/people/internal/a/b;->d:C

    .line 38
    iget-char v0, p0, Lcom/google/android/gms/people/internal/a/b;->c:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/internal/a/b;->e:Ljava/lang/String;

    .line 39
    iget-char v0, p0, Lcom/google/android/gms/people/internal/a/b;->d:C

    invoke-static {v0}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/util/regex/Pattern;->quote(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/internal/a/b;->f:Ljava/lang/String;

    .line 40
    return-void
.end method
