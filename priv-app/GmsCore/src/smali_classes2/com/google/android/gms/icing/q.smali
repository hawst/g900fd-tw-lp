.class public final Lcom/google/android/gms/icing/q;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile g:[Lcom/google/android/gms/icing/q;


# instance fields
.field public a:I

.field public b:I

.field public c:[B

.field public d:Ljava/lang/String;

.field public e:Lcom/google/android/gms/icing/av;

.field public f:[B


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 3810
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 3811
    iput v0, p0, Lcom/google/android/gms/icing/q;->a:I

    iput v0, p0, Lcom/google/android/gms/icing/q;->b:I

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->c:[B

    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    sget-object v0, Lcom/google/protobuf/nano/m;->h:[B

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->f:[B

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/q;->cachedSize:I

    .line 3812
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/q;
    .locals 2

    .prologue
    .line 3781
    sget-object v0, Lcom/google/android/gms/icing/q;->g:[Lcom/google/android/gms/icing/q;

    if-nez v0, :cond_1

    .line 3782
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 3784
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/q;->g:[Lcom/google/android/gms/icing/q;

    if-nez v0, :cond_0

    .line 3785
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/q;

    sput-object v0, Lcom/google/android/gms/icing/q;->g:[Lcom/google/android/gms/icing/q;

    .line 3787
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 3789
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/q;->g:[Lcom/google/android/gms/icing/q;

    return-object v0

    .line 3787
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 3905
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 3906
    iget v1, p0, Lcom/google/android/gms/icing/q;->a:I

    if-eqz v1, :cond_0

    .line 3907
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/q;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3910
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/q;->b:I

    if-eqz v1, :cond_1

    .line 3911
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/q;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 3914
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/q;->c:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_2

    .line 3915
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3918
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    .line 3919
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3922
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-eqz v1, :cond_4

    .line 3923
    const/4 v1, 0x5

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 3926
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/icing/q;->f:[B

    sget-object v2, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v1, v2}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v1

    if-nez v1, :cond_5

    .line 3927
    const/4 v1, 0x6

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->f:[B

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(I[B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3930
    :cond_5
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 3827
    if-ne p1, p0, :cond_1

    .line 3862
    :cond_0
    :goto_0
    return v0

    .line 3830
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/q;

    if-nez v2, :cond_2

    move v0, v1

    .line 3831
    goto :goto_0

    .line 3833
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/q;

    .line 3834
    iget v2, p0, Lcom/google/android/gms/icing/q;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/q;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 3835
    goto :goto_0

    .line 3837
    :cond_3
    iget v2, p0, Lcom/google/android/gms/icing/q;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/q;->b:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 3838
    goto :goto_0

    .line 3840
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->c:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 3841
    goto :goto_0

    .line 3843
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    if-nez v2, :cond_6

    .line 3844
    iget-object v2, p1, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    if-eqz v2, :cond_7

    move v0, v1

    .line 3845
    goto :goto_0

    .line 3847
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_7

    move v0, v1

    .line 3848
    goto :goto_0

    .line 3850
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-nez v2, :cond_8

    .line 3851
    iget-object v2, p1, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-eqz v2, :cond_9

    move v0, v1

    .line 3852
    goto :goto_0

    .line 3855
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    iget-object v3, p1, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/av;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_9

    move v0, v1

    .line 3856
    goto :goto_0

    .line 3859
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/q;->f:[B

    iget-object v3, p1, Lcom/google/android/gms/icing/q;->f:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 3860
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 3867
    iget v0, p0, Lcom/google/android/gms/icing/q;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 3869
    mul-int/lit8 v0, v0, 0x1f

    iget v2, p0, Lcom/google/android/gms/icing/q;->b:I

    add-int/2addr v0, v2

    .line 3870
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int/2addr v0, v2

    .line 3871
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/2addr v0, v2

    .line 3873
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-nez v2, :cond_1

    :goto_1
    add-int/2addr v0, v1

    .line 3875
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/q;->f:[B

    invoke-static {v1}, Ljava/util/Arrays;->hashCode([B)I

    move-result v1

    add-int/2addr v0, v1

    .line 3876
    return v0

    .line 3871
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 3873
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/av;->hashCode()I

    move-result v1

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 3775
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/q;->a:I

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/q;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->c:[B

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_5
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/av;

    invoke-direct {v0}, Lcom/google/android/gms/icing/av;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->f()[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/q;->f:[B

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x32 -> :sswitch_6
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 3882
    iget v0, p0, Lcom/google/android/gms/icing/q;->a:I

    if-eqz v0, :cond_0

    .line 3883
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/q;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3885
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/q;->b:I

    if-eqz v0, :cond_1

    .line 3886
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/q;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 3888
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->c:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_2

    .line 3889
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/q;->c:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 3891
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 3892
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/q;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 3894
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    if-eqz v0, :cond_4

    .line 3895
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 3897
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/icing/q;->f:[B

    sget-object v1, Lcom/google/protobuf/nano/m;->h:[B

    invoke-static {v0, v1}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_5

    .line 3898
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/gms/icing/q;->f:[B

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(I[B)V

    .line 3900
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 3901
    return-void
.end method
