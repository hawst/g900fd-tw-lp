.class public final Lcom/google/android/gms/car/a/h;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/a/g;


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x13
.end annotation


# instance fields
.field a:Landroid/bluetooth/BluetoothDevice;

.field public b:Landroid/bluetooth/BluetoothHeadset;

.field public c:Landroid/bluetooth/BluetoothA2dp;

.field private final d:Landroid/bluetooth/BluetoothAdapter;

.field private final e:Ljava/lang/Runnable;

.field private f:Z

.field private g:I


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/bluetooth/BluetoothAdapter;Landroid/bluetooth/BluetoothDevice;Ljava/lang/Runnable;)V
    .locals 3

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/a/h;->f:Z

    .line 41
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/a/h;->g:I

    .line 54
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothProfileUtilImpl"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    :cond_0
    iput-object p2, p0, Lcom/google/android/gms/car/a/h;->d:Landroid/bluetooth/BluetoothAdapter;

    .line 59
    iput-object p3, p0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    .line 60
    iput-object p4, p0, Lcom/google/android/gms/car/a/h;->e:Ljava/lang/Runnable;

    .line 64
    new-instance v0, Lcom/google/android/gms/car/a/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/i;-><init>(Lcom/google/android/gms/car/a/h;)V

    .line 86
    iget-object v1, p0, Lcom/google/android/gms/car/a/h;->d:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x1

    invoke-virtual {v1, p1, v0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 91
    new-instance v0, Lcom/google/android/gms/car/a/j;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/j;-><init>(Lcom/google/android/gms/car/a/h;)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/car/a/h;->d:Landroid/bluetooth/BluetoothAdapter;

    const/4 v2, 0x2

    invoke-virtual {v1, p1, v0, v2}, Landroid/bluetooth/BluetoothAdapter;->getProfileProxy(Landroid/content/Context;Landroid/bluetooth/BluetoothProfile$ServiceListener;I)Z

    .line 115
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/a/h;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->c:Landroid/bluetooth/BluetoothA2dp;

    if-nez v0, :cond_2

    :cond_0
    const-string v0, "CAR.BT"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.BT"

    const-string v1, "checkInitialized not initialized yet"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    :goto_0
    return-void

    :cond_2
    const-string v0, "CAR.BT"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    const-string v0, "CAR.BT"

    const-string v1, "BluetoothProfileUtilImpl initialized"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->e:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    goto :goto_0
.end method

.method private a(Landroid/bluetooth/BluetoothDevice;Z)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 263
    if-eqz p2, :cond_0

    const-string v0, "connect"

    move-object v1, v0

    .line 265
    :goto_0
    :try_start_0
    const-class v0, Landroid/bluetooth/BluetoothHeadset;

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/bluetooth/BluetoothDevice;

    aput-object v5, v3, v4

    invoke-virtual {v0, v1, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 267
    if-nez v0, :cond_1

    .line 268
    const-string v0, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "setHfpConnection(connect="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "): connectOrDisconnectMethod is null"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    move v0, v2

    .line 282
    :goto_1
    return v0

    .line 263
    :cond_0
    const-string v0, "disconnect"

    move-object v1, v0

    goto :goto_0

    .line 273
    :cond_1
    :try_start_1
    iget-object v3, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    aput-object p1, v4, v5

    invoke-virtual {v0, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    move-result v0

    goto :goto_1

    .line 275
    :catch_0
    move-exception v0

    const-string v0, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Cannot find BluetoothHeadset#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 276
    goto :goto_1

    .line 278
    :catch_1
    move-exception v0

    const-string v0, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "IllegalAccessException from BluetoothHeadset#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 279
    goto :goto_1

    .line 281
    :catch_2
    move-exception v0

    const-string v0, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "InvocationTargetException from BluetoothHeadset#"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    move v0, v2

    .line 282
    goto :goto_1
.end method


# virtual methods
.method public final a()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 152
    iget-object v1, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    iget-object v2, p0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v1, v2}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    iget-object v1, p0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothHeadset;->getConnectionState(Landroid/bluetooth/BluetoothDevice;)I

    move-result v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 5

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothHeadset;->getConnectedDevices()Ljava/util/List;

    move-result-object v0

    .line 163
    if-nez v0, :cond_1

    .line 172
    :cond_0
    return-void

    .line 166
    :cond_1
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 167
    const-string v2, "CAR.BT"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 168
    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Disconnecting HFP for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    :cond_2
    const/4 v2, 0x0

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/car/a/h;->a(Landroid/bluetooth/BluetoothDevice;Z)Z

    goto :goto_0
.end method

.method public final d()Z
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    :try_start_0
    const-class v1, Landroid/bluetooth/BluetoothHeadset;

    const-string v2, "setPriority"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    const-class v5, Landroid/bluetooth/BluetoothDevice;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    if-nez v1, :cond_0

    const-string v0, "CAR.BT"

    const-string v1, "BluetoothHeadset.setPriority is null"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    .line 177
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->a:Landroid/bluetooth/BluetoothDevice;

    invoke-direct {p0, v0, v6}, Lcom/google/android/gms/car/a/h;->a(Landroid/bluetooth/BluetoothDevice;Z)Z

    move-result v0

    return v0

    .line 176
    :cond_0
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    aput-object v0, v3, v4

    const/4 v0, 0x1

    const/16 v4, 0x64

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-virtual {v1, v2, v3}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_1 .. :try_end_1} :catch_2

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "Cannot find BluetoothHeadset#setPriority"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_1
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "IllegalAccessException from BluetoothHeadset#setPriority"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v0, "CAR.BT"

    const-string v1, "InvocationTargetException from BluetoothHeadset#setPriority"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final e()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 139
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 140
    const-string v0, "CAR.BT"

    const-string v1, "cleanup"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->d:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 144
    iput-object v3, p0, Lcom/google/android/gms/car/a/h;->b:Landroid/bluetooth/BluetoothHeadset;

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/car/a/h;->d:Landroid/bluetooth/BluetoothAdapter;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/car/a/h;->c:Landroid/bluetooth/BluetoothA2dp;

    invoke-virtual {v0, v1, v2}, Landroid/bluetooth/BluetoothAdapter;->closeProfileProxy(ILandroid/bluetooth/BluetoothProfile;)V

    .line 147
    iput-object v3, p0, Lcom/google/android/gms/car/a/h;->c:Landroid/bluetooth/BluetoothA2dp;

    .line 148
    return-void
.end method
