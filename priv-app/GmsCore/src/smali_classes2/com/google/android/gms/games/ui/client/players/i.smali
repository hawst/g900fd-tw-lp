.class public final Lcom/google/android/gms/games/ui/client/players/i;
.super Lcom/google/android/gms/games/ui/ce;
.source "SourceFile"


# static fields
.field private static final g:I


# instance fields
.field private final h:Landroid/view/LayoutInflater;

.field private final i:Landroid/view/View$OnClickListener;

.field private final j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:Z

.field private n:Z

.field private o:Z

.field private final p:I

.field private final q:I

.field private final r:I

.field private s:Z

.field private t:Z

.field private u:Ljava/lang/String;

.field private v:Lcom/google/android/gms/common/data/y;

.field private w:Lcom/google/android/gms/common/data/y;

.field private final x:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 60
    sget v0, Lcom/google/android/gms/j;->oV:I

    sput v0, Lcom/google/android/gms/games/ui/client/players/i;->g:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;Landroid/view/View$OnClickListener;I)V
    .locals 2

    .prologue
    .line 99
    sget v0, Lcom/google/android/gms/k;->g:I

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/ce;-><init>(Landroid/content/Context;I)V

    .line 76
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->o:Z

    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->u:Ljava/lang/String;

    .line 101
    iput-object p2, p0, Lcom/google/android/gms/games/ui/client/players/i;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    .line 102
    const-string v0, "layout_inflater"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->h:Landroid/view/LayoutInflater;

    .line 103
    iput-object p3, p0, Lcom/google/android/gms/games/ui/client/players/i;->i:Landroid/view/View$OnClickListener;

    .line 105
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 106
    sget v1, Lcom/google/android/gms/f;->E:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->p:I

    .line 107
    sget v1, Lcom/google/android/gms/f;->F:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->q:I

    .line 110
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    .line 111
    iput p4, p0, Lcom/google/android/gms/games/ui/client/players/i;->r:I

    .line 112
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/players/i;)Landroid/view/View$OnClickListener;
    .locals 1

    .prologue
    .line 46
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->i:Landroid/view/View$OnClickListener;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/players/i;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/client/players/i;)Z
    .locals 1

    .prologue
    .line 46
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    return v0
.end method


# virtual methods
.method public final synthetic a(Landroid/view/View;ILjava/lang/Object;)V
    .locals 11

    .prologue
    const/16 v10, 0x8

    const/4 v9, 0x0

    const/4 v4, 0x1

    const/4 v3, 0x4

    const/4 v2, 0x0

    .line 46
    check-cast p3, Lcom/google/android/gms/games/Player;

    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/j;

    iget v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->l:I

    if-ne p2, v1, :cond_7

    if-nez p3, :cond_7

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->l:Z

    if-nez v1, :cond_0

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->m:Z

    if-eqz v1, :cond_2

    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v5, Lcom/google/android/gms/h;->ad:I

    invoke-virtual {v0, v1, v9, v5}, Lcom/google/android/gms/games/ui/client/players/j;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget v5, v5, Lcom/google/android/gms/games/ui/client/players/i;->p:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    :goto_0
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v1

    if-lez v1, :cond_4

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v5, v5, Lcom/google/android/gms/games/ui/client/players/i;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v5}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->v()I

    move-result v5

    invoke-static {v5}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->z()I

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_1
    invoke-virtual {v5, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v6, Lcom/google/android/gms/p;->kb:I

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v5, Lcom/google/android/gms/f;->x:I

    invoke-virtual {v1, v5}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    :goto_2
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->l:Z

    if-eqz v1, :cond_5

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->kY:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    const-string v2, "auto_pick_item_add_tag"

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->kY:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    :goto_3
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    :goto_4
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->u:Ljava/lang/String;

    if-eqz v0, :cond_1

    if-nez v1, :cond_15

    :cond_1
    :goto_5
    return-void

    :cond_2
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v5, Lcom/google/android/gms/h;->ac:I

    invoke-virtual {v0, v1, v9, v5}, Lcom/google/android/gms/games/ui/client/players/j;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget v5, v5, Lcom/google/android/gms/games/ui/client/players/i;->q:I

    invoke-virtual {v1, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    :cond_3
    move v1, v3

    goto :goto_1

    :cond_4
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    goto :goto_2

    :cond_5
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->m:Z

    if-eqz v1, :cond_6

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->lc:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    const-string v2, "auto_pick_item_remove_tag"

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->lc:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_6
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    sget v4, Lcom/google/android/gms/p;->la:I

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v4, Lcom/google/android/gms/p;->la:I

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    :cond_7
    iget v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->m:I

    if-ne p2, v1, :cond_a

    if-nez p3, :cond_a

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    invoke-virtual {v5}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v1, v5, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->n:Z

    if-eqz v1, :cond_9

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->jV:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/p;->jV:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v1}, Landroid/widget/ImageView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Lcom/google/android/gms/h;->ab:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/AnimationDrawable;

    const/16 v2, 0x10

    invoke-static {v2}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v2

    if-eqz v2, :cond_8

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    :goto_6
    invoke-virtual {v1}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    :goto_7
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    const-string v2, "nearby_players_tag"

    invoke-virtual {v1, v2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    goto/16 :goto_4

    :cond_8
    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_6

    :cond_9
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->jU:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    sget v5, Lcom/google/android/gms/p;->jU:I

    invoke-virtual {v2, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    sget v2, Lcom/google/android/gms/h;->Z:I

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_7

    :cond_a
    if-eqz p3, :cond_14

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    invoke-virtual {v1, v10}, Landroid/widget/TextView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v1, v1, Lcom/google/android/gms/games/ui/client/players/i;->j:Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->u()Lcom/google/android/gms/games/ui/d/ah;

    move-result-object v1

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v1, v5}, Lcom/google/android/gms/games/ui/d/ah;->a(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_b

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-boolean v5, v5, Lcom/google/android/gms/games/ui/client/players/i;->o:Z

    if-eqz v5, :cond_c

    :cond_b
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v5

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v6, v6, Lcom/google/android/gms/games/ui/client/players/i;->k:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    :cond_c
    move v5, v4

    :goto_8
    if-eqz v5, :cond_f

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v7, Lcom/google/android/gms/f;->H:I

    invoke-virtual {v6, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v7, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget v7, v7, Lcom/google/android/gms/games/ui/client/players/i;->q:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    :goto_9
    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->g()Landroid/net/Uri;

    move-result-object v7

    sget v8, Lcom/google/android/gms/h;->ag:I

    invoke-virtual {v0, v6, v7, v8}, Lcom/google/android/gms/games/ui/client/players/j;->a(Lcom/google/android/gms/common/images/internal/LoadingImageView;Landroid/net/Uri;I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->e:Landroid/database/CharArrayBuffer;

    invoke-interface {p3, v6}, Lcom/google/android/gms/games/Player;->a(Landroid/database/CharArrayBuffer;)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v7, v0, Lcom/google/android/gms/games/ui/client/players/j;->e:Landroid/database/CharArrayBuffer;

    iget-object v7, v7, Landroid/database/CharArrayBuffer;->data:[C

    iget-object v8, v0, Lcom/google/android/gms/games/ui/client/players/j;->e:Landroid/database/CharArrayBuffer;

    iget v8, v8, Landroid/database/CharArrayBuffer;->sizeCopied:I

    invoke-virtual {v6, v7, v2, v8}, Landroid/widget/TextView;->setText([CII)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    invoke-virtual {v6, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v7, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    invoke-virtual {v7}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v7

    if-eqz v1, :cond_11

    move v1, v4

    :goto_a
    invoke-virtual {v6, v7, v1}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v1, v6}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    if-eqz v5, :cond_12

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v9}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    :goto_b
    invoke-interface {p3}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v5, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v5, v5, Lcom/google/android/gms/games/ui/client/players/i;->k:Ljava/lang/String;

    invoke-virtual {v1, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v6, v6, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    invoke-virtual {v6, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    if-nez v5, :cond_d

    if-eqz v1, :cond_13

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    if-ne v1, v4, :cond_13

    :cond_d
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v9}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_e
    move v5, v2

    goto/16 :goto_8

    :cond_f
    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    iget-object v7, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget v7, v7, Lcom/google/android/gms/games/ui/client/players/i;->p:I

    invoke-virtual {v6, v7}, Landroid/widget/TextView;->setTextColor(I)V

    if-eqz v1, :cond_10

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v7, Lcom/google/android/gms/f;->x:I

    invoke-virtual {v6, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v6, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    :cond_10
    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v6, v2}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(I)V

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    invoke-virtual {v6, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_9

    :cond_11
    move v1, v2

    goto :goto_a

    :cond_12
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setEnabled(Z)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-virtual {v1, p3}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    sget v5, Lcom/google/android/gms/games/ui/client/players/i;->g:I

    iget-object v6, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget v6, v6, Lcom/google/android/gms/games/ui/client/players/i;->r:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v1, v5, v6}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    goto :goto_b

    :cond_13
    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setVisibility(I)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    invoke-virtual {v1, p3}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    iget-object v1, v0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    iget-object v2, v0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    iget-object v2, v2, Lcom/google/android/gms/games/ui/client/players/i;->i:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    goto/16 :goto_4

    :cond_14
    const-string v1, "SelectPlayersAdapter"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unhandled tile entryPosition "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    :cond_15
    invoke-static {v1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/view/View;)Ljava/lang/Object;

    move-result-object v0

    instance-of v2, v0, Lcom/google/android/gms/games/Player;

    if-eqz v2, :cond_1

    check-cast v0, Lcom/google/android/gms/games/Player;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/i;->u:Ljava/lang/String;

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-virtual {v1}, Landroid/view/View;->requestFocus()Z

    goto/16 :goto_5
.end method

.method public final a(Lcom/google/android/gms/common/data/d;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v0, 0x0

    const/4 v4, 0x1

    .line 228
    if-eqz p1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    if-nez v1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    if-nez v1, :cond_1

    .line 230
    :cond_0
    new-array v1, v4, [Lcom/google/android/gms/common/data/d;

    aput-object p1, v1, v0

    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/ce;->a([Lcom/google/android/gms/common/data/d;)V

    .line 253
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    invoke-virtual {v1}, Ljava/util/HashMap;->clear()V

    .line 254
    if-eqz p1, :cond_6

    .line 255
    invoke-interface {p1}, Lcom/google/android/gms/common/data/d;->c()I

    move-result v2

    move v1, v0

    :goto_1
    if-ge v1, v2, :cond_6

    .line 256
    invoke-interface {p1, v1}, Lcom/google/android/gms/common/data/d;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Player;

    .line 257
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v4

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->k()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v3, v4, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    .line 233
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    if-eqz v1, :cond_2

    .line 234
    new-instance v1, Lcom/google/android/gms/common/data/y;

    new-instance v2, Lcom/google/android/gms/common/data/w;

    new-array v3, v4, [Lcom/google/android/gms/games/Player;

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/data/w;-><init>([Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/data/y;-><init>(Lcom/google/android/gms/common/data/a;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    .line 238
    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    if-eqz v1, :cond_3

    .line 239
    new-instance v1, Lcom/google/android/gms/common/data/y;

    new-instance v2, Lcom/google/android/gms/common/data/w;

    new-array v3, v4, [Lcom/google/android/gms/games/Player;

    invoke-direct {v2, v3}, Lcom/google/android/gms/common/data/w;-><init>([Ljava/lang/Object;)V

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/data/y;-><init>(Lcom/google/android/gms/common/data/a;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->w:Lcom/google/android/gms/common/data/y;

    .line 243
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    if-eqz v1, :cond_4

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    if-nez v1, :cond_4

    .line 244
    new-array v1, v5, [Lcom/google/android/gms/common/data/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    aput-object v2, v1, v0

    aput-object p1, v1, v4

    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/ce;->a([Lcom/google/android/gms/common/data/d;)V

    goto :goto_0

    .line 245
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    if-eqz v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    if-nez v1, :cond_5

    .line 246
    new-array v1, v5, [Lcom/google/android/gms/common/data/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/i;->w:Lcom/google/android/gms/common/data/y;

    aput-object v2, v1, v0

    aput-object p1, v1, v4

    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/ce;->a([Lcom/google/android/gms/common/data/d;)V

    goto :goto_0

    .line 248
    :cond_5
    const/4 v1, 0x3

    new-array v1, v1, [Lcom/google/android/gms/common/data/d;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    aput-object v2, v1, v0

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/players/i;->w:Lcom/google/android/gms/common/data/y;

    aput-object v2, v1, v4

    aput-object p1, v1, v5

    invoke-super {p0, v1}, Lcom/google/android/gms/games/ui/ce;->a([Lcom/google/android/gms/common/data/d;)V

    goto/16 :goto_0

    .line 260
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 261
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 126
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->k:Ljava/lang/String;

    .line 127
    return-void
.end method

.method public final a(Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/players/i;->x:Ljava/util/HashMap;

    if-eqz p2, :cond_1

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 223
    :cond_0
    return-void

    .line 219
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final varargs a([Lcom/google/android/gms/common/data/d;)V
    .locals 2

    .prologue
    .line 265
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Use setDataBuffer instead"

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final b(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 295
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->u:Ljava/lang/String;

    .line 296
    return-void
.end method

.method public final b(Z)V
    .locals 1

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    if-eqz v0, :cond_0

    .line 154
    if-nez p1, :cond_1

    .line 155
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/y;->a()V

    .line 159
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 161
    :cond_0
    return-void

    .line 157
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->v:Lcom/google/android/gms/common/data/y;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/y;->f()V

    goto :goto_0
.end method

.method public final b(ZZ)V
    .locals 1

    .prologue
    .line 139
    if-eqz p1, :cond_0

    if-nez p2, :cond_3

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 141
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->l:Z

    if-ne v0, p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->m:Z

    if-eq v0, p2, :cond_2

    .line 143
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->l:Z

    .line 144
    iput-boolean p2, p0, Lcom/google/android/gms/games/ui/client/players/i;->m:Z

    .line 145
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 147
    :cond_2
    return-void

    .line 139
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 0

    .prologue
    .line 169
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->s:Z

    .line 170
    return-void
.end method

.method public final d(Z)V
    .locals 0

    .prologue
    .line 178
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    .line 179
    return-void
.end method

.method public final e(Z)V
    .locals 1

    .prologue
    .line 185
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->t:Z

    if-eqz v0, :cond_0

    .line 186
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->n:Z

    .line 187
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 189
    :cond_0
    return-void
.end method

.method public final f(Z)V
    .locals 0

    .prologue
    .line 204
    iput-boolean p1, p0, Lcom/google/android/gms/games/ui/client/players/i;->o:Z

    .line 205
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/i;->notifyDataSetChanged()V

    .line 206
    return-void
.end method

.method public final g()Landroid/view/View;
    .locals 3

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->h:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->bX:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 272
    new-instance v1, Lcom/google/android/gms/games/ui/client/players/j;

    invoke-direct {v1, p0, p0, v0}, Lcom/google/android/gms/games/ui/client/players/j;-><init>(Lcom/google/android/gms/games/ui/client/players/i;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V

    .line 273
    invoke-virtual {v0, v1}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 275
    return-object v0
.end method

.method public final h()Z
    .locals 1

    .prologue
    .line 195
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->n:Z

    return v0
.end method

.method public final i()V
    .locals 1

    .prologue
    .line 302
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/i;->u:Ljava/lang/String;

    .line 303
    return-void
.end method
