.class public final Lcom/google/android/gms/common/internal/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field private final a:Lcom/google/android/gms/common/es;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/es;)V
    .locals 0

    .prologue
    .line 259
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 260
    iput-object p1, p0, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    .line 261
    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 265
    iget-object v0, p0, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    invoke-interface {v0}, Lcom/google/android/gms/common/es;->T_()V

    .line 266
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 275
    instance-of v0, p1, Lcom/google/android/gms/common/internal/am;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    check-cast p1, Lcom/google/android/gms/common/internal/am;

    iget-object v1, p1, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 278
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 1

    .prologue
    .line 270
    iget-object v0, p0, Lcom/google/android/gms/common/internal/am;->a:Lcom/google/android/gms/common/es;

    invoke-interface {v0}, Lcom/google/android/gms/common/es;->W_()V

    .line 271
    return-void
.end method
