.class public final Lcom/google/android/gms/car/qo;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final k:Lcom/google/android/gms/car/qu;


# instance fields
.field private final a:Lcom/google/android/gms/car/on;

.field private b:Lcom/google/android/gms/car/oi;

.field private c:Lcom/google/android/gms/car/qp;

.field private d:Lcom/google/android/gms/car/oi;

.field private e:Lcom/google/android/gms/car/qp;

.field private f:Z

.field private g:J

.field private h:Landroid/os/HandlerThread;

.field private i:Lcom/google/android/gms/car/qr;

.field private j:Lcom/google/android/gms/car/ou;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/car/qu;

    invoke-direct {v0}, Lcom/google/android/gms/car/qu;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/qo;->k:Lcom/google/android/gms/car/qu;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/on;)V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    iput-object p1, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    .line 61
    return-void
.end method

.method static synthetic a(II)I
    .locals 1

    .prologue
    .line 20
    mul-int v0, p1, p0

    div-int/lit16 v0, v0, 0xa0

    return v0
.end method

.method static synthetic a(Lcom/google/android/gms/car/qo;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/car/qo;->c()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/qo;Lcom/google/android/gms/car/oi;)V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/qo;->d(Lcom/google/android/gms/car/oi;)V

    return-void
.end method

.method private declared-synchronized a(ZZ)V
    .locals 3

    .prologue
    .line 168
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 192
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 171
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->i:Lcom/google/android/gms/car/qr;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/qr;->a(ZZ)V

    .line 172
    if-eqz p1, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->o()V

    .line 174
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 175
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startup animation cancelled for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    :cond_2
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    .line 180
    :cond_3
    if-eqz p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->o()V

    .line 182
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 183
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removal animation cancelled for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 185
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/car/qo;->f:Z

    if-eqz v0, :cond_5

    .line 186
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->g(Lcom/google/android/gms/car/oi;)V

    .line 188
    :cond_5
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/qo;->f:Z

    .line 189
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    .line 190
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 168
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private b()V
    .locals 4

    .prologue
    .line 271
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->i:Lcom/google/android/gms/car/qr;

    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/qr;->a(J)V

    .line 272
    return-void
.end method

.method private declared-synchronized c()V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 299
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 322
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 302
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->f()V

    .line 303
    const/4 v0, 0x0

    .line 304
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 305
    iget-object v4, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    if-eqz v4, :cond_2

    .line 306
    iget-object v4, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/car/qp;->a(J)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 307
    invoke-direct {p0}, Lcom/google/android/gms/car/qo;->d()V

    .line 312
    :cond_2
    :goto_1
    iget-object v4, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    if-eqz v4, :cond_3

    .line 313
    iget-object v4, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    invoke-virtual {v4, v2, v3}, Lcom/google/android/gms/car/qp;->a(J)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 314
    invoke-direct {p0}, Lcom/google/android/gms/car/qo;->e()V

    .line 319
    :cond_3
    :goto_2
    if-eqz v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->i:Lcom/google/android/gms/car/qr;

    iget-wide v2, p0, Lcom/google/android/gms/car/qo;->g:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/qr;->a(J)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 299
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_4
    move v0, v1

    .line 309
    goto :goto_1

    :cond_5
    move v0, v1

    .line 316
    goto :goto_2
.end method

.method private declared-synchronized d()V
    .locals 3

    .prologue
    .line 329
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 340
    :goto_0
    monitor-exit p0

    return-void

    .line 332
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->o()V

    .line 333
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 334
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startup animation ended for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    .line 337
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 329
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized d(Lcom/google/android/gms/car/oi;)V
    .locals 3

    .prologue
    .line 158
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    if-ne p1, v0, :cond_0

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qp;->a()V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/oi;->a(Lcom/google/android/gms/car/qp;Z)V

    .line 161
    invoke-direct {p0}, Lcom/google/android/gms/car/qo;->b()V

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 164
    :cond_0
    monitor-exit p0

    return-void

    .line 158
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e()V
    .locals 3

    .prologue
    .line 343
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 358
    :goto_0
    monitor-exit p0

    return-void

    .line 346
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0}, Lcom/google/android/gms/car/oi;->o()V

    .line 347
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 348
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "removal animation ended for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/qo;->f:Z

    if-eqz v0, :cond_2

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/on;->g(Lcom/google/android/gms/car/oi;)V

    .line 353
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/qo;->f:Z

    .line 354
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    .line 355
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    .line 357
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->f()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 343
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized e(Lcom/google/android/gms/car/oi;)Z
    .locals 1

    .prologue
    .line 227
    monitor-enter p0

    if-eqz p1, :cond_0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-ne p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 2

    .prologue
    .line 244
    monitor-enter p0

    const/4 v0, 0x1

    const/4 v1, 0x1

    :try_start_0
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/qo;->a(ZZ)V

    .line 245
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 246
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;

    .line 247
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 248
    monitor-exit p0

    return-void

    .line 244
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(JLcom/google/android/gms/car/ou;)V
    .locals 3

    .prologue
    .line 236
    monitor-enter p0

    :try_start_0
    iput-wide p1, p0, Lcom/google/android/gms/car/qo;->g:J

    .line 237
    iput-object p3, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    .line 238
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "animation"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 240
    new-instance v0, Lcom/google/android/gms/car/qr;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/qr;-><init>(Lcom/google/android/gms/car/qo;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->i:Lcom/google/android/gms/car/qr;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 241
    monitor-exit p0

    return-void

    .line 236
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/oi;)V
    .locals 12

    .prologue
    const-wide/16 v0, 0xfa

    const-wide/16 v10, 0x0

    const/4 v5, 0x1

    .line 105
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 155
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 108
    :cond_1
    if-eqz p1, :cond_0

    .line 111
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    if-eq p1, v2, :cond_0

    .line 114
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-ne p1, v2, :cond_2

    .line 115
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startStartupAnimation for already removed window "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 120
    :cond_2
    :try_start_2
    const-string v2, "CAR.WM.ANIM"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 121
    const-string v2, "CAR.WM.ANIM"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "startStartupAnimation "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-eqz v2, :cond_4

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    invoke-virtual {v2}, Lcom/google/android/gms/car/oi;->a()I

    move-result v2

    if-ne v2, v5, :cond_4

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v2

    if-ne v2, v5, :cond_4

    .line 128
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-eq p1, v2, :cond_8

    .line 129
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    .line 130
    iget-object v4, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    invoke-virtual {v4}, Lcom/google/android/gms/car/qp;->b()J

    move-result-wide v4

    .line 131
    sub-long/2addr v2, v4

    .line 132
    sub-long v2, v0, v2

    .line 133
    cmp-long v4, v2, v0

    if-lez v4, :cond_7

    .line 137
    :goto_1
    const/4 v2, 0x1

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3}, Lcom/google/android/gms/car/qo;->a(ZZ)V

    move-wide v8, v0

    .line 141
    :goto_2
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->j()Lcom/google/android/gms/car/ok;

    move-result-object v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/gms/car/qo;->k:Lcom/google/android/gms/car/qu;

    :goto_3
    iput-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qp;->c()J

    move-result-wide v0

    cmp-long v0, v0, v10

    if-nez v0, :cond_5

    .line 143
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->c:Lcom/google/android/gms/car/qp;

    .line 144
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->b()V

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->f()V

    goto/16 :goto_0

    .line 139
    :cond_4
    const/4 v0, 0x1

    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/qo;->a(ZZ)V

    move-wide v8, v10

    goto :goto_2

    .line 141
    :pswitch_0
    new-instance v0, Lcom/google/android/gms/car/ra;

    sget-object v1, Lcom/google/android/gms/car/qp;->b:Lcom/google/android/gms/car/pd;

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0xfa

    iget-object v6, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v6, v6, Lcom/google/android/gms/car/ou;->h:I

    neg-int v6, v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/car/ra;-><init>(Lcom/google/android/gms/car/au;JJI)V

    goto :goto_3

    :pswitch_1
    new-instance v0, Lcom/google/android/gms/car/qt;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/qt;-><init>(I)V

    goto :goto_3

    :pswitch_2
    new-instance v0, Lcom/google/android/gms/car/qt;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/qt;-><init>(I)V

    goto :goto_3

    :pswitch_3
    new-instance v0, Lcom/google/android/gms/car/qy;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/qy;-><init>(I)V

    goto :goto_3

    :pswitch_4
    new-instance v0, Lcom/google/android/gms/car/qw;

    invoke-direct {v0}, Lcom/google/android/gms/car/qw;-><init>()V

    goto :goto_3

    :pswitch_5
    new-instance v0, Lcom/google/android/gms/car/rc;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/rc;-><init>(Lcom/google/android/gms/car/ok;)V

    goto :goto_3

    .line 148
    :cond_5
    iput-object p1, p0, Lcom/google/android/gms/car/qo;->b:Lcom/google/android/gms/car/oi;

    .line 149
    cmp-long v0, v8, v10

    if-gtz v0, :cond_6

    .line 150
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/qo;->d(Lcom/google/android/gms/car/oi;)V

    goto/16 :goto_0

    .line 152
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->i:Lcom/google/android/gms/car/qr;

    invoke-virtual {v0, v8, v9, p1}, Lcom/google/android/gms/car/qr;->a(JLcom/google/android/gms/car/oi;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    :cond_7
    move-wide v0, v2

    goto/16 :goto_1

    :cond_8
    move-wide v0, v10

    goto/16 :goto_1

    .line 141
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/oi;ZZ)V
    .locals 4

    .prologue
    .line 65
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_1

    .line 102
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 68
    :cond_1
    if-eqz p1, :cond_0

    .line 71
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-ne p1, v0, :cond_3

    .line 72
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startRemovalAnimation called twice for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 75
    :cond_2
    if-eqz p2, :cond_0

    .line 76
    iput-boolean p2, p0, Lcom/google/android/gms/car/qo;->f:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 65
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 80
    :cond_3
    :try_start_2
    const-string v0, "CAR.WM.ANIM"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 81
    const-string v0, "CAR.WM.ANIM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "startRemovalAnimation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_4
    const/4 v0, 0x1

    invoke-direct {p0, p3, v0}, Lcom/google/android/gms/car/qo;->a(ZZ)V

    .line 87
    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->a()I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/gms/car/oi;->j()Lcom/google/android/gms/car/ok;

    move-result-object v1

    packed-switch v0, :pswitch_data_0

    sget-object v0, Lcom/google/android/gms/car/qo;->k:Lcom/google/android/gms/car/qu;

    :goto_1
    iput-object v0, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    .line 88
    iput-boolean p2, p0, Lcom/google/android/gms/car/qo;->f:Z

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    invoke-virtual {v0}, Lcom/google/android/gms/car/qp;->c()J

    move-result-wide v0

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_6

    .line 90
    if-eqz p2, :cond_5

    .line 91
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/on;->g(Lcom/google/android/gms/car/oi;)V

    .line 93
    :cond_5
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    .line 94
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    .line 96
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->a:Lcom/google/android/gms/car/on;

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->f()V

    goto :goto_0

    .line 87
    :pswitch_0
    sget-object v0, Lcom/google/android/gms/car/qo;->k:Lcom/google/android/gms/car/qu;

    goto :goto_1

    :pswitch_1
    new-instance v0, Lcom/google/android/gms/car/qs;

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/qs;-><init>(Lcom/google/android/gms/car/ok;I)V

    goto :goto_1

    :pswitch_2
    new-instance v0, Lcom/google/android/gms/car/qs;

    iget-object v2, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v2, v2, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/qs;-><init>(Lcom/google/android/gms/car/ok;I)V

    goto :goto_1

    :pswitch_3
    new-instance v0, Lcom/google/android/gms/car/qx;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->j:Lcom/google/android/gms/car/ou;

    iget v1, v1, Lcom/google/android/gms/car/ou;->g:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/qx;-><init>(I)V

    goto :goto_1

    :pswitch_4
    new-instance v0, Lcom/google/android/gms/car/qv;

    invoke-direct {v0}, Lcom/google/android/gms/car/qv;-><init>()V

    goto :goto_1

    :pswitch_5
    new-instance v0, Lcom/google/android/gms/car/rb;

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/rb;-><init>(Lcom/google/android/gms/car/ok;)V

    goto :goto_1

    .line 98
    :cond_6
    iput-object p1, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    iget-object v1, p0, Lcom/google/android/gms/car/qo;->e:Lcom/google/android/gms/car/qp;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/car/oi;->a(Lcom/google/android/gms/car/qp;Z)V

    .line 100
    invoke-direct {p0}, Lcom/google/android/gms/car/qo;->b()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 87
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_5
        :pswitch_4
    .end packed-switch
.end method

.method public final declared-synchronized b(Lcom/google/android/gms/car/oi;)Z
    .locals 3

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 201
    monitor-enter p0

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->h:Landroid/os/HandlerThread;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_1

    .line 208
    :cond_0
    :goto_0
    monitor-exit p0

    return v0

    .line 204
    :cond_1
    if-eqz p1, :cond_0

    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/qo;->d:Lcom/google/android/gms/car/oi;

    if-ne p1, v2, :cond_0

    .line 207
    const/4 v0, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v2}, Lcom/google/android/gms/car/qo;->a(ZZ)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move v0, v1

    .line 208
    goto :goto_0

    .line 201
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized c(Lcom/google/android/gms/car/oi;)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 219
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/qo;->e(Lcom/google/android/gms/car/oi;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 220
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/car/qo;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 223
    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 219
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
