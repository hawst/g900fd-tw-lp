.class final Lcom/google/android/gms/games/realtime/network/aa;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/games/realtime/network/b;

.field final b:Ljava/util/concurrent/atomic/AtomicBoolean;

.field final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field final e:Landroid/net/LocalServerSocket;

.field f:Landroid/net/LocalSocket;

.field private final g:Lcom/google/android/gms/games/realtime/network/ab;


# direct methods
.method constructor <init>(Ljava/lang/String;Lcom/google/android/gms/games/realtime/network/b;Ljava/lang/String;Landroid/net/LocalServerSocket;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 152
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->b:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 153
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/aa;->c:Ljava/lang/String;

    .line 154
    iput-object p2, p0, Lcom/google/android/gms/games/realtime/network/aa;->a:Lcom/google/android/gms/games/realtime/network/b;

    .line 155
    iput-object p4, p0, Lcom/google/android/gms/games/realtime/network/aa;->e:Landroid/net/LocalServerSocket;

    .line 156
    iput-object p3, p0, Lcom/google/android/gms/games/realtime/network/aa;->d:Ljava/lang/String;

    .line 157
    new-instance v0, Lcom/google/android/gms/games/realtime/network/ab;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/games/realtime/network/ab;-><init>(Lcom/google/android/gms/games/realtime/network/aa;B)V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->g:Lcom/google/android/gms/games/realtime/network/ab;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->g:Lcom/google/android/gms/games/realtime/network/ab;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/ab;->setPriority(I)V

    .line 159
    return-void
.end method


# virtual methods
.method final a()V
    .locals 1

    .prologue
    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 139
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->f:Landroid/net/LocalSocket;

    invoke-virtual {v0}, Landroid/net/LocalSocket;->close()V

    .line 141
    :cond_0
    return-void
.end method

.method final b()V
    .locals 1

    .prologue
    .line 166
    monitor-enter p0

    .line 167
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/aa;->g:Lcom/google/android/gms/games/realtime/network/ab;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/ab;->start()V

    .line 168
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V

    .line 169
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
