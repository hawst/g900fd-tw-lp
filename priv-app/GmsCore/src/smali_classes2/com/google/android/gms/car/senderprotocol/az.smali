.class public abstract Lcom/google/android/gms/car/senderprotocol/az;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/f;


# instance fields
.field private final a:Lcom/google/android/gms/car/gw;

.field h:Z

.field protected i:Lcom/google/android/gms/car/senderprotocol/e;

.field final j:Lcom/google/android/gms/car/gx;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->h:Z

    .line 30
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/az;->a:Lcom/google/android/gms/car/gw;

    .line 31
    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/az;->j:Lcom/google/android/gms/car/gx;

    .line 32
    return-void
.end method

.method protected static a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;
    .locals 3

    .prologue
    .line 165
    :try_start_0
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v1

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    invoke-static {p0, v0, v1, v2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[BII)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    return-object v0

    .line 168
    :catch_0
    move-exception v0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unable to parse protocol buffer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method


# virtual methods
.method public a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 68
    const-string v0, "CAR.GAL"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 69
    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onChannelOpened() for service "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/az;->a:Lcom/google/android/gms/car/gw;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->a:Lcom/google/android/gms/car/gw;

    if-eqz v0, :cond_3

    .line 72
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->a:Lcom/google/android/gms/car/gw;

    invoke-interface {v0, p0}, Lcom/google/android/gms/car/gw;->a(Lcom/google/android/gms/car/senderprotocol/az;)V

    .line 79
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->j:Lcom/google/android/gms/car/gx;

    if-eqz v0, :cond_4

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->j:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->v()V

    .line 86
    :cond_2
    :goto_1
    return-void

    .line 74
    :cond_3
    const-string v0, "CAR.GAL"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 75
    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "skipping onEndPointReady() for endpoint "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 82
    :cond_4
    const-string v0, "CAR.GAL"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 83
    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "skipping onServiceStarted() for endpoint "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public a(I)V
    .locals 0

    .prologue
    .line 64
    return-void
.end method

.method protected abstract a(ILjava/nio/ByteBuffer;)V
.end method

.method protected final a(I[B)V
    .locals 6

    .prologue
    .line 126
    const/4 v3, 0x1

    const/4 v4, 0x0

    array-length v5, p2

    move-object v0, p0

    move v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/az;->a(I[BZII)V

    .line 127
    return-void
.end method

.method final a(I[BZII)V
    .locals 3

    .prologue
    .line 139
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->h:Z

    if-eqz v0, :cond_0

    .line 150
    :goto_0
    return-void

    .line 142
    :cond_0
    array-length v0, p2

    add-int/lit8 v0, v0, 0x2

    invoke-static {v0}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 143
    int-to-short v1, p1

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    .line 144
    invoke-virtual {v0, p2, p4, p5}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 146
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/az;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v1, v0, p3}, Lcom/google/android/gms/car/senderprotocol/e;->a(Ljava/nio/ByteBuffer;Z)V
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 148
    :catch_0
    move-exception v0

    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to send message type = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because channel was closed"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/e;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/az;->i:Lcom/google/android/gms/car/senderprotocol/e;

    .line 58
    return-void
.end method

.method public a(Ljava/nio/ByteBuffer;)V
    .locals 5

    .prologue
    .line 90
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 91
    const-string v0, "CAR.GAL"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 92
    const-string v0, "CAR.GAL"

    const-string v1, "Received message without type header."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    :goto_0
    return-void

    .line 96
    :cond_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    .line 98
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    add-int/lit8 v1, v1, -0x2

    invoke-static {v1}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 99
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v3

    add-int/lit8 v3, v3, 0x2

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v4

    add-int/lit8 v4, v4, -0x2

    invoke-virtual {v1, v2, v3, v4}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 100
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    .line 101
    invoke-static {p1}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 102
    const v2, 0xffff

    and-int/2addr v0, v2

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/az;->a(ILjava/nio/ByteBuffer;)V

    goto :goto_0
.end method

.method public c()V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->h:Z

    .line 111
    return-void
.end method

.method protected final c(Ljava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->i:Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/car/senderprotocol/e;->a(Ljava/nio/ByteBuffer;Z)V

    .line 157
    return-void
.end method
