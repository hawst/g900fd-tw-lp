.class public final Lcom/google/android/gms/games/service/a/n/j;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:[B

.field private final h:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 27
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/n/j;->c:Lcom/google/android/gms/games/internal/dr;

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/n/j;->d:Lcom/google/android/gms/games/a/au;

    .line 30
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/n/j;->e:Ljava/lang/String;

    .line 31
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/n/j;->f:Ljava/lang/String;

    .line 32
    if-nez p5, :cond_0

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/n/j;->g:[B

    .line 38
    :goto_0
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/n/j;->h:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    .line 39
    return-void

    .line 35
    :cond_0
    array-length v0, p5

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/n/j;->g:[B

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/j;->g:[B

    array-length v1, p5

    invoke-static {p5, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/n/j;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->p(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 51
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 44
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/n/j;->d:Lcom/google/android/gms/games/a/au;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/n/j;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/n/j;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/n/j;->g:[B

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/n/j;->h:[Lcom/google/android/gms/games/multiplayer/ParticipantResult;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Ljava/lang/String;[B[Lcom/google/android/gms/games/multiplayer/ParticipantResult;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
