.class public final Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/gms/games/multiplayer/realtime/h;
.implements Lcom/google/android/gms/games/ui/d/v;


# static fields
.field private static M:I

.field private static N:I

.field private static O:I

.field private static P:I

.field private static final n:I

.field private static final o:I

.field private static final p:[I


# instance fields
.field private A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

.field private B:I

.field private C:Ljava/lang/String;

.field private D:Landroid/os/Handler;

.field private E:Ljava/lang/Runnable;

.field private F:Ljava/lang/Runnable;

.field private G:I

.field private H:I

.field private I:Landroid/content/Intent;

.field private J:Z

.field private K:Z

.field private L:Z

.field private Q:Landroid/app/AlertDialog;

.field private R:I

.field private S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

.field private q:Lcom/google/android/gms/games/ui/d/t;

.field private r:Lcom/google/android/gms/games/ui/client/matches/j;

.field private s:Landroid/widget/TextView;

.field private t:Landroid/view/View;

.field private u:Landroid/view/View;

.field private v:Landroid/view/View;

.field private w:Landroid/widget/TextView;

.field private x:Landroid/widget/TextView;

.field private y:Landroid/view/animation/Animation;

.field private z:Landroid/view/animation/Animation;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 118
    sget v0, Lcom/google/android/gms/l;->bB:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n:I

    .line 121
    sget v0, Lcom/google/android/gms/m;->g:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->o:I

    .line 132
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->p:[I

    .line 192
    const/4 v0, 0x0

    sput v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    .line 193
    const/4 v0, 0x1

    sput v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    .line 194
    const/4 v0, 0x2

    sput v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:I

    .line 195
    sput v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:I

    return-void

    .line 132
    :array_0
    .array-data 4
        0x78
        0x1e0
        0x4b0
    .end array-data
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 205
    sget v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->o:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 182
    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    .line 187
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Z

    .line 206
    return-void
.end method

.method static synthetic T()I
    .locals 1

    .prologue
    .line 110
    sget v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    return v0
.end method

.method private U()V
    .locals 6

    .prologue
    .line 387
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    sget-object v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->p:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_1

    .line 409
    :cond_0
    :goto_0
    return-void

    .line 391
    :cond_1
    new-instance v0, Lcom/google/android/gms/games/ui/client/matches/d;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/matches/d;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Ljava/lang/Runnable;

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Ljava/lang/Runnable;

    sget-object v2, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->p:[I

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    add-int/lit8 v4, v3, 0x1

    iput v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    aget v2, v2, v3

    int-to-long v2, v2

    const-wide/16 v4, 0x3e8

    mul-long/2addr v2, v4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private V()V
    .locals 2

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 415
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->E:Ljava/lang/Runnable;

    .line 416
    return-void
.end method

.method private W()V
    .locals 2

    .prologue
    .line 440
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Ljava/lang/Runnable;

    if-eqz v0, :cond_0

    .line 441
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 443
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Ljava/lang/Runnable;

    .line 444
    return-void
.end method

.method private X()V
    .locals 9

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 630
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    .line 631
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 693
    :goto_0
    return-void

    .line 638
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 675
    const-string v0, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateHeader: unexpected room status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 676
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "updateHeader: unexpected room status: "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    move v0, v1

    move-object v2, v3

    .line 681
    :goto_1
    if-eqz v2, :cond_2

    .line 682
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v3, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 687
    :goto_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->t:Landroid/view/View;

    if-eqz v0, :cond_3

    move v0, v1

    :goto_3
    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 640
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v0

    if-le v0, v2, :cond_1

    .line 641
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/n;->m:I

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v5

    new-array v6, v2, [Ljava/lang/Object;

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v1

    invoke-virtual {v0, v4, v5, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    :goto_4
    move v8, v2

    move-object v2, v0

    move v0, v8

    .line 649
    goto :goto_1

    .line 645
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->mE:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    .line 652
    :pswitch_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->mC:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v8, v2

    move-object v2, v0

    move v0, v8

    .line 655
    goto :goto_1

    .line 658
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v4, Lcom/google/android/gms/p;->mD:I

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move v8, v2

    move-object v2, v0

    move v0, v8

    .line 661
    goto :goto_1

    .line 664
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v2, Lcom/google/android/gms/p;->mB:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    move-object v2, v0

    move v0, v1

    .line 666
    goto :goto_1

    .line 670
    :pswitch_4
    const-string v0, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "updateHeader: unexpected DELETED status: "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    move-object v2, v3

    .line 672
    goto :goto_1

    .line 684
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->s:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 687
    :cond_3
    const/16 v0, 0x8

    goto :goto_3

    .line 638
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method private Y()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 788
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ac()Z

    move-result v0

    if-nez v0, :cond_2

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    if-eqz v0, :cond_2

    .line 789
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    if-nez v0, :cond_0

    .line 790
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 791
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 794
    iput-boolean v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    .line 803
    :cond_1
    :goto_0
    return-void

    .line 795
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ac()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    if-nez v0, :cond_1

    .line 796
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    if-nez v0, :cond_3

    .line 797
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 798
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 800
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 801
    iput-boolean v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    goto :goto_0
.end method

.method private Z()V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 807
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    if-eqz v0, :cond_0

    .line 808
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v0

    .line 809
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    sub-int/2addr v1, v0

    .line 810
    if-lez v1, :cond_1

    .line 811
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Landroid/widget/TextView;

    const/16 v2, 0x50

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setGravity(I)V

    .line 812
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Lcom/google/android/gms/n;->n:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 814
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Landroid/widget/TextView;

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 820
    :cond_0
    :goto_0
    return-void

    .line 816
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Landroid/widget/TextView;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setGravity(I)V

    .line 817
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    return-object v0
.end method

.method private a(ILandroid/content/Intent;)V
    .locals 0

    .prologue
    .line 209
    iput p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    .line 210
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(ILandroid/content/Intent;)V

    .line 211
    return-void
.end method

.method private a(Landroid/app/AlertDialog;I)V
    .locals 1

    .prologue
    .line 836
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Landroid/app/AlertDialog;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->isShowing()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 837
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 839
    :cond_0
    if-eqz p1, :cond_1

    .line 840
    invoke-static {p0, p1}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;Landroid/app/Dialog;)V

    .line 842
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->b(Landroid/app/AlertDialog;I)V

    .line 843
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/v;)V
    .locals 5

    .prologue
    .line 553
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    iput-object v1, v0, Lcom/google/android/gms/games/g/as;->a:Ljava/lang/String;

    .line 556
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b()Z

    move-result v0

    if-nez v0, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 557
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->i:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v3

    invoke-static {p1}, Lcom/google/android/gms/games/d;->c(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v4

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b:Landroid/content/Context;

    iput-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->e:Ljava/lang/String;

    iput-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->c:Ljava/lang/String;

    iput-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->d:Ljava/lang/String;

    .line 562
    :cond_1
    return-void
.end method

.method private a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 726
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 730
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-eqz v0, :cond_0

    .line 731
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 733
    const-string v0, "WaitingRoom"

    const-string v1, "updateRoom: room changed out from under us!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 734
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "- previous: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 735
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "-      new: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 736
    const-string v0, "updateRoom: room changed out from under us!"

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 740
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    .line 744
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 747
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->X()V

    .line 750
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->r:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/matches/j;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 753
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ab()V

    .line 755
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ae()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 756
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->V()V

    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/f;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/matches/f;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    sget v0, Lcom/google/android/gms/p;->mr:I

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v3, Lcom/google/android/gms/l;->aH:I

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    sget v0, Lcom/google/android/gms/j;->gK:I

    invoke-virtual {v3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    sget v4, Lcom/google/android/gms/p;->ms:I

    invoke-virtual {v0, v4}, Landroid/widget/TextView;->setText(I)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const v2, 0x104000a

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNeutralButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->N:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Landroid/app/AlertDialog;I)V

    .line 760
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ac()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 761
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->aa()V

    .line 762
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->W()V

    new-instance v0, Lcom/google/android/gms/games/ui/client/matches/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/matches/e;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Ljava/lang/Runnable;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->F:Ljava/lang/Runnable;

    const-wide/16 v2, 0x2710

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 783
    :goto_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Z()V

    .line 784
    return-void

    .line 764
    :cond_2
    if-eqz p2, :cond_5

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:I

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:I

    if-eq v0, v1, :cond_3

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:I

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:I

    if-ne v0, v1, :cond_5

    .line 771
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->aa()V

    .line 773
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    .line 774
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    if-gez v0, :cond_4

    .line 775
    iput v5, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    .line 777
    :cond_4
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->U()V

    .line 780
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->W()V

    .line 781
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Y()V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->d(I)V

    return-void
.end method

.method private aa()V
    .locals 2

    .prologue
    .line 826
    const/4 v0, 0x0

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->M:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Landroid/app/AlertDialog;I)V

    .line 827
    return-void
.end method

.method private ab()V
    .locals 2

    .prologue
    .line 981
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()Lcom/google/android/gms/common/api/v;
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 989
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_0

    .line 994
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:Lcom/google/android/gms/games/ui/d/t;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 1008
    :goto_0
    return-void

    .line 983
    :catch_0
    move-exception v0

    const-string v0, "WaitingRoom"

    const-string v1, "updateLoadingDataView: couldn\'t get GoogleApiClient"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 997
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v0

    .line 998
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    .line 1000
    if-nez v0, :cond_1

    .line 1001
    const-string v0, "WaitingRoom"

    const-string v1, "displayParticipants: room has no participants!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:Lcom/google/android/gms/games/ui/d/t;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    goto :goto_0

    .line 1005
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:Lcom/google/android/gms/games/ui/d/t;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    goto :goto_0
.end method

.method private ac()Z
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 1051
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    move v0, v2

    .line 1089
    :goto_0
    return v0

    .line 1058
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    .line 1059
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1073
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v6

    move v3, v2

    move v1, v2

    .line 1075
    :goto_1
    if-ge v3, v5, :cond_1

    .line 1076
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1077
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v7

    .line 1078
    if-eqz v7, :cond_3

    invoke-interface {v7}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Ljava/lang/String;

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 1080
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->f()Z

    move-result v0

    .line 1075
    :goto_2
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_1

    .line 1084
    :cond_1
    if-eqz v1, :cond_2

    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    if-lt v6, v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v2

    .line 1089
    goto :goto_0

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method private ad()I
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 1093
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1094
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_1

    .line 1113
    :cond_0
    return v1

    .line 1098
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v3

    .line 1099
    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    .line 1102
    :goto_0
    if-ge v2, v4, :cond_0

    .line 1103
    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1104
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v5

    .line 1108
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    const/4 v0, 0x3

    if-eq v5, v0, :cond_2

    const/4 v0, 0x4

    if-eq v5, v0, :cond_2

    .line 1110
    add-int/lit8 v0, v1, 0x1

    .line 1102
    :goto_1
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    move v1, v0

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private ae()Z
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 1121
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 1124
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    .line 1125
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    .line 1126
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    invoke-static {v0, v5}, Ljava/lang/Math;->min(II)I

    move-result v6

    move v3, v2

    move v1, v2

    .line 1128
    :goto_0
    if-ge v3, v5, :cond_1

    .line 1129
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    .line 1130
    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->a()I

    move-result v0

    .line 1131
    const/4 v7, 0x3

    if-eq v0, v7, :cond_0

    const/4 v7, 0x4

    if-ne v0, v7, :cond_3

    .line 1132
    :cond_0
    add-int/lit8 v0, v1, 0x1

    .line 1128
    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    .line 1136
    :cond_1
    sub-int v0, v5, v1

    if-ge v0, v6, :cond_2

    const/4 v0, 0x1

    .line 1142
    :goto_2
    return v0

    :cond_2
    move v0, v2

    goto :goto_2

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method private af()V
    .locals 2

    .prologue
    .line 1203
    const-string v0, "WaitingRoom"

    const-string v1, "Stale room! We\'re being restarted after having been previously stopped."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1204
    const-string v0, "WaitingRoom"

    const-string v1, "We were disconnected from the games service while stopped, so our Room object may now be out of date."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1206
    const-string v0, "WaitingRoom"

    const-string v1, "We can\'t display the room correctly any more, so bailing out with RESULT_CANCELED..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1208
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->d(I)V

    .line 1209
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    .line 1210
    return-void
.end method

.method private ag()V
    .locals 8

    .prologue
    const/16 v1, 0x400

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 1559
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->aa()V

    .line 1562
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0, v1, v1}, Landroid/view/Window;->setFlags(II)V

    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->e()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->py:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    sget v3, Lcom/google/android/gms/j;->pA:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    sget v4, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    sget v5, Lcom/google/android/gms/j;->ng:I

    invoke-virtual {v3, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    sget v5, Lcom/google/android/gms/p;->mA:I

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setText(I)V

    invoke-virtual {v2, v7}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v1, v6}, Landroid/widget/TextView;->setVisibility(I)V

    invoke-virtual {v4, v6}, Landroid/view/View;->setVisibility(I)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    if-eqz v1, :cond_1

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->k()Landroid/net/Uri;

    move-result-object v0

    if-nez v0, :cond_0

    invoke-interface {v1}, Lcom/google/android/gms/games/Game;->i()Landroid/net/Uri;

    move-result-object v0

    :cond_0
    sget v1, Lcom/google/android/gms/h;->af:I

    invoke-virtual {v3, v0, v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(Landroid/net/Uri;I)V

    invoke-virtual {v3, v7}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/d/ap;->a(Landroid/view/View;)V

    .line 1564
    new-instance v0, Lcom/google/android/gms/games/ui/client/matches/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/matches/i;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    .line 1581
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    const-wide/16 v2, 0x3e8

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 1582
    return-void

    .line 1562
    :cond_1
    const/4 v0, 0x4

    invoke-virtual {v3, v0}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->setVisibility(I)V

    goto :goto_0
.end method

.method private b(Landroid/app/AlertDialog;I)V
    .locals 0

    .prologue
    .line 847
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Q:Landroid/app/AlertDialog;

    .line 848
    iput p2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->R:I

    .line 849
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;I)V
    .locals 1

    .prologue
    .line 110
    const/4 v0, 0x0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->b(Landroid/app/AlertDialog;I)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z
    .locals 1

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ac()Z

    move-result v0

    return v0
.end method

.method private c(I)V
    .locals 0

    .prologue
    .line 214
    iput p1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    .line 215
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->setResult(I)V

    .line 216
    return-void
.end method

.method static synthetic c(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 110
    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/g;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/matches/g;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    sget v0, Lcom/google/android/gms/p;->mH:I

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ad()I

    move-result v3

    sub-int v3, v0, v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Lcom/google/android/gms/n;->o:I

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {v4, v5, v3, v6}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lcom/google/android/gms/l;->aH:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v0, Lcom/google/android/gms/j;->gK:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v0, v8}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->mt:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->mF:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v9}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->P:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Landroid/app/AlertDialog;I)V

    return-void
.end method

.method private d(I)V
    .locals 1

    .prologue
    .line 1636
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Landroid/content/Intent;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(ILandroid/content/Intent;)V

    .line 1637
    return-void
.end method

.method static synthetic d(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 6

    .prologue
    .line 110
    new-instance v1, Lcom/google/android/gms/games/ui/client/matches/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/matches/h;-><init>(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V

    sget v0, Lcom/google/android/gms/p;->mH:I

    invoke-static {p0, v0}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v2

    sget v0, Lcom/google/android/gms/p;->mG:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v0

    sget v4, Lcom/google/android/gms/l;->aH:I

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v0, Lcom/google/android/gms/j;->gK:I

    invoke-virtual {v4, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    new-instance v0, Landroid/app/AlertDialog$Builder;

    invoke-direct {v0, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const/4 v3, 0x0

    invoke-virtual {v0, v3}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v2}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->mt:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    sget v2, Lcom/google/android/gms/p;->mq:I

    invoke-virtual {v0, v2, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0, v4}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    move-result-object v0

    sget v1, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->O:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Landroid/app/AlertDialog;I)V

    return-void
.end method

.method private e(I)Landroid/view/animation/Animation;
    .locals 2

    .prologue
    .line 1860
    invoke-static {p0, p1}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    .line 1861
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/view/animation/Animation;->setFillAfter(Z)V

    .line 1862
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1863
    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Y()V

    return-void
.end method

.method static synthetic f(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->Z()V

    return-void
.end method

.method static synthetic g(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ag()V

    return-void
.end method

.method static synthetic h(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)V
    .locals 0

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->U()V

    return-void
.end method

.method static synthetic i(Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;)Z
    .locals 1

    .prologue
    .line 110
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Z

    return v0
.end method

.method static synthetic m(Lcom/google/android/gms/games/multiplayer/realtime/Room;)I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 110
    invoke-static {p0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->l()Ljava/util/ArrayList;

    move-result-object v4

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v3, v2

    move v1, v2

    :goto_0
    if-ge v3, v5, :cond_0

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/Participant;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-eqz v0, :cond_2

    add-int/lit8 v0, v1, 0x1

    :goto_1
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->i()Landroid/os/Bundle;

    move-result-object v0

    if-eqz v0, :cond_1

    const-string v2, "max_automatch_players"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    :goto_2
    add-int/2addr v0, v1

    return v0

    :cond_1
    move v0, v2

    goto :goto_2

    :cond_2
    move v0, v1

    goto :goto_1
.end method

.method private n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 1643
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v2

    .line 1644
    if-eqz p1, :cond_1

    invoke-interface {p1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;

    .line 1645
    :goto_0
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 1646
    if-nez v0, :cond_2

    .line 1649
    const-string v0, "room"

    invoke-virtual {v3, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1659
    :cond_0
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Landroid/content/Intent;

    .line 1660
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Landroid/content/Intent;

    invoke-virtual {v0, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 1661
    iget v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->I:Landroid/content/Intent;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(ILandroid/content/Intent;)V

    .line 1662
    :goto_1
    return-void

    :cond_1
    move-object v0, v1

    .line 1644
    goto :goto_0

    .line 1650
    :cond_2
    const-string v1, "room"

    iget v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v1, v0, v2, v4}, Lcom/google/android/gms/common/internal/v;->a(Landroid/os/Bundle;Ljava/lang/String;Lcom/google/android/gms/common/internal/DowngradeableSafeParcel;Landroid/content/Context;Ljava/lang/Integer;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1652
    const-string v0, "WaitingRoom"

    const-string v1, "Unable to return room to game. Something has gone very wrong."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1653
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->c(I)V

    .line 1654
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_1
.end method


# virtual methods
.method public final H_()V
    .locals 0

    .prologue
    .line 1904
    return-void
.end method

.method protected final N()I
    .locals 1

    .prologue
    .line 381
    const/16 v0, 0x9

    return v0
.end method

.method public final a(ILcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 3

    .prologue
    .line 1699
    const-string v0, "WaitingRoom"

    const-string v1, "CALLBACK: onRoomConnected()..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1700
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onRoomConnected: statusCode = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for room ID: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {p2}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1707
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->V()V

    .line 1708
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->W()V

    .line 1709
    const/4 v0, 0x0

    invoke-direct {p0, p2, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1710
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ag()V

    .line 1711
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1822
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1823
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 1692
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onLeftRoom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 1693
    return-void
.end method

.method public final b(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1830
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1833
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 493
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->b_(Landroid/os/Bundle;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    .line 499
    const-string v0, "WaitingRoom"

    const-string v1, "onConnected: no valid room; ignoring this callback..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 546
    :goto_0
    return-void

    .line 503
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v3

    .line 505
    invoke-direct {p0, v3}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/common/api/v;)V

    .line 507
    invoke-interface {v3}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 508
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-eqz v0, :cond_1

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 510
    sget-object v0, Lcom/google/android/gms/games/d;->o:Lcom/google/android/gms/games/y;

    invoke-interface {v0, v3}, Lcom/google/android/gms/games/y;->a(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Ljava/lang/String;

    .line 511
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Ljava/lang/String;

    if-nez v0, :cond_2

    .line 513
    const-string v0, "WaitingRoom"

    const-string v1, "We don\'t have a current player, something went wrong. Finishing the activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const/16 v0, 0x2711

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->c(I)V

    .line 516
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    :cond_1
    move v0, v2

    .line 508
    goto :goto_1

    .line 519
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->r:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->C:Ljava/lang/String;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/client/matches/j;->a(Ljava/lang/String;)V

    .line 524
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->r:Lcom/google/android/gms/games/ui/client/matches/j;

    iget-object v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/ui/client/matches/j;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 527
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v0

    .line 528
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    :goto_2
    invoke-static {v1}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 529
    sget-object v1, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v1, v3, p0, v0}, Lcom/google/android/gms/games/multiplayer/realtime/b;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/multiplayer/realtime/h;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v1

    .line 532
    if-eqz v1, :cond_5

    .line 533
    const-string v0, "WaitingRoom"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Room status after registering listener: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 534
    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 535
    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->f()I

    move-result v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 536
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ag()V

    .line 539
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ab()V

    goto/16 :goto_0

    :cond_4
    move v1, v2

    .line 528
    goto :goto_2

    .line 542
    :cond_5
    const-string v1, "WaitingRoom"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Room "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " invalid.  Finishing activity."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 543
    const/16 v0, 0x2718

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->d(I)V

    .line 544
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto/16 :goto_0
.end method

.method public final c(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1786
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1787
    return-void
.end method

.method public final d(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1793
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1794
    return-void
.end method

.method public final e(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1800
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1801
    return-void
.end method

.method public final f(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1807
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1808
    return-void
.end method

.method public final f()Z
    .locals 1

    .prologue
    .line 362
    const/4 v0, 0x1

    return v0
.end method

.method public final g(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1728
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1729
    return-void
.end method

.method public final h(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    .prologue
    .line 1734
    const-string v0, "WaitingRoom"

    const-string v1, "CALLBACK: onDisconnectedFromRoom()..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 1744
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1745
    return-void
.end method

.method public final i(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1751
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1752
    return-void
.end method

.method public final j()I
    .locals 1

    .prologue
    .line 372
    const/4 v0, 0x2

    return v0
.end method

.method public final j(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 1

    .prologue
    .line 1758
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 1759
    return-void
.end method

.method public final k(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    .prologue
    .line 1678
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onRoomCreated: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 1679
    return-void
.end method

.method public final l(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V
    .locals 2

    .prologue
    .line 1685
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "unexpected callback: onJoinedRoom: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    .line 1686
    return-void
.end method

.method public final onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 1873
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 1874
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    if-nez v0, :cond_1

    .line 1876
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 1877
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1883
    :cond_0
    :goto_0
    return-void

    .line 1878
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    if-eqz v0, :cond_0

    .line 1880
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 1881
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0
.end method

.method public final onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1888
    return-void
.end method

.method public final onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0

    .prologue
    .line 1869
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 4

    .prologue
    .line 1892
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->tU:I

    if-ne v0, v1, :cond_0

    .line 1894
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    new-instance v1, Lcom/google/android/gms/games/g/at;

    invoke-direct {v1}, Lcom/google/android/gms/games/g/at;-><init>()V

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/android/gms/games/g/at;->a:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a()J

    move-result-wide v2

    iput-wide v2, v1, Lcom/google/android/gms/games/g/at;->b:J

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1895
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->ag()V

    .line 1899
    :goto_0
    return-void

    .line 1897
    :cond_0
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onClick: unexpected view: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", id "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    .line 220
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 222
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    .line 229
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->b(Landroid/app/Activity;)Landroid/content/Context;

    move-result-object v0

    .line 230
    if-nez v0, :cond_0

    .line 231
    const-string v0, "WaitingRoom"

    const-string v1, "Could not find calling context. Aborting activity."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-direct {p0, v4}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->c(I)V

    .line 233
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    .line 358
    :goto_0
    return-void

    .line 238
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 239
    const-string v2, "room"

    iget v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->j:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v1, v2, v0, v3}, Lcom/google/android/gms/common/internal/v;->a(Landroid/content/Intent;Ljava/lang/String;Landroid/content/Context;Ljava/lang/Integer;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    .line 241
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 244
    if-nez v0, :cond_1

    .line 245
    const-string v0, "WaitingRoom"

    const-string v1, "EXTRA_ROOM extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const/16 v0, 0x2718

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->c(I)V

    .line 247
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    .line 254
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    const/16 v3, 0x80

    invoke-virtual {v2, v3}, Landroid/view/Window;->addFlags(I)V

    .line 261
    if-eqz p1, :cond_2

    .line 281
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n(Lcom/google/android/gms/games/multiplayer/realtime/Room;)V

    .line 283
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->af()V

    goto :goto_0

    .line 287
    :cond_2
    const-string v2, "com.google.android.gms.games.MIN_PARTICIPANTS_TO_START"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    .line 293
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    if-gez v1, :cond_3

    .line 294
    const-string v0, "WaitingRoom"

    const-string v1, "EXTRA_MIN_PARTICIPANTS_TO_START extra missing; bailing out..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 295
    const/16 v0, 0x2714

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->c(I)V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    goto :goto_0

    .line 302
    :cond_3
    iget v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    if-ge v1, v5, :cond_4

    .line 304
    iput v5, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->B:I

    .line 313
    :cond_4
    sget v1, Lcom/google/android/gms/j;->sd:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->s:Landroid/widget/TextView;

    .line 314
    sget v1, Lcom/google/android/gms/j;->pT:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->t:Landroid/view/View;

    .line 317
    sget v1, Lcom/google/android/gms/j;->qo:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->u:Landroid/view/View;

    .line 319
    sget v1, Lcom/google/android/gms/j;->ck:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    .line 320
    sget v1, Lcom/google/android/gms/j;->tU:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 321
    sget v1, Lcom/google/android/gms/j;->tW:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w:Landroid/widget/TextView;

    .line 322
    sget v1, Lcom/google/android/gms/j;->tV:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->x:Landroid/widget/TextView;

    .line 323
    sget v1, Lcom/google/android/gms/b;->h:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->e(I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->y:Landroid/view/animation/Animation;

    .line 324
    sget v1, Lcom/google/android/gms/b;->i:I

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->e(I)Landroid/view/animation/Animation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->z:Landroid/view/animation/Animation;

    .line 326
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->v:Landroid/view/View;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 327
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->K:Z

    .line 328
    iput-boolean v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->L:Z

    .line 329
    iput v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->G:I

    .line 331
    sget v1, Lcom/google/android/gms/j;->oX:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 332
    new-instance v2, Lcom/google/android/gms/games/ui/d/t;

    invoke-direct {v2, v1, p0}, Lcom/google/android/gms/games/ui/d/t;-><init>(Landroid/view/View;Lcom/google/android/gms/games/ui/d/v;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:Lcom/google/android/gms/games/ui/d/t;

    .line 334
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->q:Lcom/google/android/gms/games/ui/d/t;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/gms/games/ui/d/t;->a(I)V

    .line 336
    const v2, 0x102000a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ListView;

    .line 341
    invoke-virtual {v1, v4}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 343
    new-instance v2, Lcom/google/android/gms/games/ui/client/matches/j;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/ui/client/matches/j;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->r:Lcom/google/android/gms/games/ui/client/matches/j;

    .line 344
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->r:Lcom/google/android/gms/games/ui/client/matches/j;

    invoke-virtual {v1, v2}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 346
    new-instance v1, Landroid/os/Handler;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->D:Landroid/os/Handler;

    .line 349
    invoke-direct {p0, v0, v4}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/games/multiplayer/realtime/Room;Z)V

    .line 351
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->U()V

    .line 354
    sget v0, Lcom/google/android/gms/j;->oJ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 355
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 356
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Landroid/content/Context;)I

    move-result v2

    iput v2, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 357
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    goto/16 :goto_0
.end method

.method public final onDestroy()V
    .locals 0

    .prologue
    .line 376
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onDestroy()V

    .line 377
    return-void
.end method

.method public final onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    .line 1037
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    sget v1, Lcom/google/android/gms/j;->lY:I

    if-ne v0, v1, :cond_0

    .line 1038
    const-string v0, "WaitingRoom"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "User explicitly asked to leave the room! item = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1039
    const/16 v0, 0x2715

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->d(I)V

    .line 1040
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->finish()V

    .line 1041
    const/4 v0, 0x1

    .line 1043
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method public final onPause()V
    .locals 1

    .prologue
    .line 486
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Z

    .line 487
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onPause()V

    .line 488
    return-void
.end method

.method public final onResume()V
    .locals 1

    .prologue
    .line 470
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->J:Z

    .line 471
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onResume()V

    .line 473
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    .line 482
    :goto_0
    return-void

    .line 481
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->X()V

    goto :goto_0
.end method

.method protected final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 566
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 569
    const-string v0, "savedStateRecreatedFlag"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 570
    return-void
.end method

.method public final onStart()V
    .locals 1

    .prologue
    .line 450
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->a(Lcom/google/android/gms/common/api/v;)V

    .line 451
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_0

    .line 457
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->af()V

    .line 463
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStart()V

    .line 464
    return-void
.end method

.method public final onStop()V
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 578
    iget-object v3, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    iget v4, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->H:I

    iget-boolean v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->f:Z

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    const-string v5, " Cannot log the same log twice!"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    iput-boolean v1, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->f:Z

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    iput v4, v0, Lcom/google/android/gms/games/g/as;->c:I

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    invoke-virtual {v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a()J

    move-result-wide v4

    iput-wide v4, v0, Lcom/google/android/gms/games/g/as;->d:J

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    iget-object v1, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->g:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    new-array v1, v1, [Lcom/google/android/gms/games/g/at;

    iput-object v1, v0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_1
    if-ge v2, v1, :cond_1

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    iget-object v4, v0, Lcom/google/android/gms/games/g/as;->e:[Lcom/google/android/gms/games/g/at;

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->g:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/g/at;

    aput-object v0, v4, v2

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_0
    move v0, v2

    goto :goto_0

    :cond_1
    invoke-virtual {v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b()Z

    move-result v0

    const-string v1, "Can\'t dispatch the log without calling setLoggingContext"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    iget-object v0, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->b:Landroid/content/Context;

    iget-object v1, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->c:Ljava/lang/String;

    iget-object v2, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->e:Ljava/lang/String;

    iget-object v4, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->d:Ljava/lang/String;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;->a:Lcom/google/android/gms/games/g/as;

    invoke-static {v0, v1, v2, v4, v3}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/g/as;)V

    .line 581
    :cond_2
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ci;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->S:Lcom/google/android/gms/games/service/statemachine/roomclient/ci;

    .line 586
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->V()V

    .line 587
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->W()V

    .line 589
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    if-nez v0, :cond_3

    .line 597
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStop()V

    .line 626
    :goto_2
    return-void

    .line 607
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 608
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 609
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v1}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->a()Ljava/lang/String;

    move-result-object v1

    .line 610
    sget-object v2, Lcom/google/android/gms/games/d;->m:Lcom/google/android/gms/games/multiplayer/realtime/b;

    invoke-interface {v2, v0, v1}, Lcom/google/android/gms/games/multiplayer/realtime/b;->c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I

    .line 623
    :cond_4
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->A:Lcom/google/android/gms/games/multiplayer/realtime/Room;

    .line 625
    invoke-super {p0}, Lcom/google/android/gms/games/ui/client/a;->onStop()V

    goto :goto_2
.end method

.method protected final u()I
    .locals 1

    .prologue
    .line 367
    sget v0, Lcom/google/android/gms/games/ui/client/matches/RealTimeWaitingRoomActivity;->n:I

    return v0
.end method
