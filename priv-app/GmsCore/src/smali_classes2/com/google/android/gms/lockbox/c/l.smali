.class public final Lcom/google/android/gms/lockbox/c/l;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/lockbox/c/m;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V
    .locals 6

    .prologue
    .line 77
    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 78
    return-void
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 28
    invoke-static {p1}, Lcom/google/android/gms/lockbox/c/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/lockbox/c/a;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Ljava/lang/String;)Lcom/google/android/gms/lockbox/c/o;
    .locals 1

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->j()V

    .line 91
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/lockbox/c/a;

    invoke-interface {v0, p1}, Lcom/google/android/gms/lockbox/c/a;->a(Ljava/lang/String;)Lcom/google/android/gms/lockbox/c/o;

    move-result-object v0

    return-object v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 214
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 215
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->l(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 218
    return-void
.end method

.method protected final a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V
    .locals 1

    .prologue
    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->j()V

    .line 110
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/lockbox/c/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/lockbox/c/a;->a(Ljava/lang/String;Lcom/google/android/gms/lockbox/g;)V

    .line 111
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->j()V

    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/lockbox/c/a;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/lockbox/c/a;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 152
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 198
    const-string v0, "com.google.android.gms.lockbox.service.START"

    return-object v0
.end method

.method public final b()V
    .locals 3

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/lockbox/c/l;->a:Lcom/google/android/gms/lockbox/c/m;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/lockbox/c/l;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/lockbox/c/a;

    invoke-interface {v0}, Lcom/google/android/gms/lockbox/c/a;->a()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/lockbox/c/l;->a:Lcom/google/android/gms/lockbox/c/m;

    .line 181
    :cond_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->b()V

    .line 182
    return-void

    .line 179
    :catch_0
    move-exception v0

    const-string v1, "LockboxClientImpl"

    const-string v2, "Problem calling into service."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "com.google.android.gms.lockbox.internal.ILockboxService"

    return-object v0
.end method
