.class public final Lcom/google/android/gms/icing/impl/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/icing/impl/a/x;


# static fields
.field public static final a:I

.field private static final c:Ljava/util/List;

.field private static final d:Ljava/util/Map;

.field private static final e:Landroid/util/SparseArray;


# instance fields
.field final b:Lcom/google/android/gms/icing/impl/a/f;

.field private final f:Ljava/lang/String;

.field private final g:Landroid/content/SharedPreferences;

.field private final h:Lcom/google/android/gms/icing/impl/n;

.field private final i:Ljava/util/Map;

.field private j:Z

.field private final k:Ljava/io/File;

.field private final l:Lcom/google/android/gms/icing/impl/e/f;

.field private m:Ljavax/crypto/Mac;

.field private n:Ljava/security/SecureRandom;

.field private final o:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    const/4 v3, 0x4

    .line 110
    new-array v0, v3, [Ljava/lang/String;

    const-string v1, "from_address"

    aput-object v1, v0, v4

    const-string v1, "to_addresses"

    aput-object v1, v0, v5

    const-string v1, "subject"

    aput-object v1, v0, v6

    const-string v1, "body"

    aput-object v1, v0, v7

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/a/j;->c:Ljava/util/List;

    .line 121
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0, v3}, Ljava/util/HashMap;-><init>(I)V

    .line 122
    sput-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const/4 v1, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 123
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "plain"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "html"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "rfc822"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 129
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "url"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 131
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "app-indexing-out-links"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    const-string v1, "blob"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 136
    new-instance v0, Landroid/util/SparseArray;

    sget-object v1, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/util/SparseArray;-><init>(I)V

    sput-object v0, Lcom/google/android/gms/icing/impl/a/j;->e:Landroid/util/SparseArray;

    .line 137
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 138
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 139
    sget-object v3, Lcom/google/android/gms/icing/impl/a/j;->e:Landroid/util/SparseArray;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3, v1, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    goto :goto_0

    .line 164
    :cond_1
    sget-object v0, Ljava/util/concurrent/TimeUnit;->DAYS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x5a

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toSeconds(J)J

    move-result-wide v0

    long-to-int v0, v0

    sput v0, Lcom/google/android/gms/icing/impl/a/j;->a:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/impl/a/f;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    .locals 7

    .prologue
    .line 235
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/icing/impl/a/j;-><init>(Ljava/util/Map;Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/impl/a/f;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V

    .line 236
    return-void
.end method

.method private constructor <init>(Ljava/util/Map;Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/impl/a/f;Landroid/content/Context;Ljava/lang/String;Ljava/io/File;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 241
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    iput-boolean v1, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 202
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    .line 242
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    .line 243
    iput-object p5, p0, Lcom/google/android/gms/icing/impl/a/j;->f:Ljava/lang/String;

    .line 244
    invoke-virtual {p4, p5, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    .line 245
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/a/j;->h:Lcom/google/android/gms/icing/impl/n;

    .line 246
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v1, "created"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 250
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 254
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->w()Ljavax/crypto/Mac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    .line 257
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 258
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const-string v3, "corpuskey:"

    invoke-virtual {v1, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 259
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    const/16 v3, 0xa

    invoke-virtual {v1, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    .line 262
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/icing/i;

    invoke-direct {v3}, Lcom/google/android/gms/icing/i;-><init>()V

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 265
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/i;)V

    goto :goto_0

    .line 269
    :cond_2
    iput-object p6, p0, Lcom/google/android/gms/icing/impl/a/j;->k:Ljava/io/File;

    .line 272
    new-instance v0, Ljava/io/File;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "-corpus-scratch-data.tmp"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p6, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    const-class v1, Lcom/google/android/gms/icing/m;

    new-instance v2, Lcom/google/android/gms/icing/impl/e/f;

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/icing/impl/e/f;-><init>(Ljava/io/File;Ljava/lang/Class;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/e/f;->a()Ljava/util/List;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/util/Collection;)V

    .line 276
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J
    .locals 8

    .prologue
    .line 461
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 462
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V

    .line 463
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update([B)V

    .line 464
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update(B)V

    .line 465
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update([B)V

    .line 466
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    const/16 v2, 0x2d

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update(B)V

    .line 467
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {p3}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update([B)V

    .line 468
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v4

    .line 471
    const/4 v0, 0x7

    aget-byte v0, v4, v0

    and-int/lit8 v0, v0, 0x7f

    int-to-long v2, v0

    .line 472
    const/4 v0, 0x6

    :goto_0
    if-ltz v0, :cond_0

    .line 473
    const/16 v5, 0x8

    shl-long/2addr v2, v5

    aget-byte v5, v4, v0

    and-int/lit16 v5, v5, 0xff

    int-to-long v6, v5

    add-long/2addr v2, v6

    .line 472
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 475
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 476
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Z)Lcom/google/android/gms/icing/av;
    .locals 8

    .prologue
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 1668
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->d:Ljava/util/Map;

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-interface {v0, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 1669
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    if-eqz v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    .line 1672
    :goto_0
    new-instance v5, Lcom/google/android/gms/icing/av;

    invoke-direct {v5}, Lcom/google/android/gms/icing/av;-><init>()V

    .line 1673
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    iput-object v1, v5, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    .line 1674
    iget-boolean v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    if-nez v1, :cond_7

    move v1, v2

    :goto_1
    iput-boolean v1, v5, Lcom/google/android/gms/icing/av;->b:Z

    .line 1675
    iput v4, v5, Lcom/google/android/gms/icing/av;->c:I

    .line 1676
    iget v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->e:I

    iput v1, v5, Lcom/google/android/gms/icing/av;->d:I

    .line 1677
    iget-boolean v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->f:Z

    iput-boolean v1, v5, Lcom/google/android/gms/icing/av;->e:Z

    .line 1678
    iput-object v0, v5, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    .line 1681
    if-nez p1, :cond_0

    invoke-virtual {p0, v2}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_8

    move v0, v2

    :goto_2
    if-eqz v0, :cond_1

    .line 1682
    :cond_0
    new-array v0, v2, [I

    aput v3, v0, v3

    iput-object v0, v5, Lcom/google/android/gms/icing/av;->g:[I

    .line 1686
    :cond_1
    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/aw;->b(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v0

    .line 1688
    cmpg-double v4, v0, v6

    if-gez v4, :cond_2

    .line 1689
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(D)I

    move-result v0

    .line 1690
    iput v0, v5, Lcom/google/android/gms/icing/av;->h:I

    .line 1694
    :cond_2
    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/aw;->c(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v0

    .line 1696
    cmpg-double v4, v0, v6

    if-gez v4, :cond_3

    .line 1697
    invoke-static {v0, v1}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(D)I

    move-result v0

    .line 1698
    iput v0, v5, Lcom/google/android/gms/icing/av;->i:I

    .line 1701
    :cond_3
    const-string v0, "url"

    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-static {p0}, Lcom/google/android/gms/appdatasearch/aw;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 1703
    iput v2, v5, Lcom/google/android/gms/icing/av;->l:I

    .line 1709
    :cond_4
    :goto_3
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_5

    .line 1710
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->j:Ljava/lang/String;

    iput-object v0, v5, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    .line 1713
    :cond_5
    return-object v5

    .line 1669
    :cond_6
    const-string v0, ""

    goto :goto_0

    :cond_7
    move v1, v3

    .line 1674
    goto :goto_1

    :cond_8
    move v0, v3

    .line 1681
    goto :goto_2

    .line 1704
    :cond_9
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(I)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_a

    :goto_4
    if-eqz v2, :cond_4

    .line 1705
    const/4 v0, 0x2

    iput v0, v5, Lcom/google/android/gms/icing/av;->l:I

    goto :goto_3

    :cond_a
    move v2, v3

    .line 1704
    goto :goto_4
.end method

.method public static a(ILjava/lang/String;Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/o;Lcom/google/android/gms/icing/ag;)Lcom/google/android/gms/icing/c/a/b;
    .locals 16

    .prologue
    .line 2152
    new-instance v10, Lcom/google/android/gms/icing/c/a/b;

    invoke-direct {v10}, Lcom/google/android/gms/icing/c/a/b;-><init>()V

    .line 2153
    move-object/from16 v0, p3

    iget-wide v2, v0, Lcom/google/android/gms/icing/o;->f:J

    iput-wide v2, v10, Lcom/google/android/gms/icing/c/a/b;->a:J

    .line 2154
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iput-object v2, v10, Lcom/google/android/gms/icing/c/a/b;->b:Ljava/lang/String;

    .line 2155
    move/from16 v0, p0

    iput v0, v10, Lcom/google/android/gms/icing/c/a/b;->e:I

    .line 2156
    move-object/from16 v0, p1

    iput-object v0, v10, Lcom/google/android/gms/icing/c/a/b;->c:Ljava/lang/String;

    .line 2158
    const/4 v9, 0x0

    .line 2159
    const/4 v8, 0x0

    .line 2160
    const/4 v7, 0x0

    .line 2161
    const/4 v6, 0x0

    .line 2162
    const/4 v5, 0x0

    .line 2163
    const/4 v4, 0x0

    .line 2164
    const/4 v3, 0x0

    .line 2165
    const/4 v2, 0x0

    move v15, v2

    move-object v2, v3

    move-object v3, v4

    move-object v4, v5

    move-object v5, v6

    move-object v6, v7

    move-object v7, v8

    move-object v8, v9

    move v9, v15

    :goto_0
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v11, v11

    if-ge v9, v11, :cond_9

    .line 2166
    move-object/from16 v0, p3

    iget-object v11, v0, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    aget-object v11, v11, v9

    .line 2167
    iget-object v12, v11, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    iget-object v12, v12, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    .line 2168
    iget-object v13, v11, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    iget-object v13, v13, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    .line 2169
    const-string v14, "name"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_1

    .line 2170
    iget-object v11, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v11}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/google/android/gms/icing/c/a/b;->i:Ljava/lang/String;

    .line 2165
    :cond_0
    :goto_1
    add-int/lit8 v9, v9, 0x1

    goto :goto_0

    .line 2171
    :cond_1
    const-string v14, "url"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_2

    .line 2172
    iget-object v11, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v11}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v11

    iput-object v11, v10, Lcom/google/android/gms/icing/c/a/b;->k:Ljava/lang/String;

    goto :goto_1

    .line 2173
    :cond_2
    const-string v14, ".private:outLinks"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_3

    .line 2176
    :try_start_0
    iget-object v11, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v11}, Lcom/google/android/gms/icing/c/a/d;->a([B)Lcom/google/android/gms/icing/c/a/d;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_1

    .line 2178
    :catch_0
    move-exception v11

    const-string v11, "Failed to parse outLinks proto."

    invoke-static {v11}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_1

    .line 2180
    :cond_3
    const-string v14, ".private:action"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v14

    if-eqz v14, :cond_4

    .line 2182
    :try_start_1
    iget-object v11, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v11}, Lcom/google/android/gms/icing/c/a/v;->a([B)Lcom/google/android/gms/icing/c/a/v;

    move-result-object v11

    .line 2183
    iget-object v6, v11, Lcom/google/android/gms/icing/c/a/v;->a:Ljava/lang/String;

    .line 2184
    iget-object v7, v11, Lcom/google/android/gms/icing/c/a/v;->b:[Lcom/google/android/gms/icing/c/a/u;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 2186
    :catch_1
    move-exception v11

    const-string v11, "Failed to parse schemaOrgAction proto."

    invoke-static {v11}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    goto :goto_1

    .line 2188
    :cond_4
    const-string v14, ".private:actionId"

    invoke-virtual {v14, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_5

    .line 2189
    iget-object v11, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v11}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v11

    .line 2190
    if-eqz v11, :cond_0

    .line 2191
    iput-object v11, v10, Lcom/google/android/gms/icing/c/a/b;->h:Ljava/lang/String;

    goto :goto_1

    .line 2193
    :cond_5
    const-string v13, "intent_action"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_6

    .line 2194
    iget-object v5, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v5}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 2195
    :cond_6
    const-string v13, "intent_data"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_7

    .line 2196
    iget-object v4, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    goto :goto_1

    .line 2197
    :cond_7
    const-string v13, "intent_activity"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v13

    if-eqz v13, :cond_8

    .line 2198
    iget-object v3, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_1

    .line 2199
    :cond_8
    const-string v13, "intent_extra_data"

    invoke-virtual {v13, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v12

    if-eqz v12, :cond_0

    .line 2200
    iget-object v2, v11, Lcom/google/android/gms/icing/q;->c:[B

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/e/d;->a([B)Ljava/lang/String;

    move-result-object v2

    goto/16 :goto_1

    .line 2203
    :cond_9
    new-instance v9, Landroid/content/Intent;

    invoke-direct {v9, v5, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 2204
    if-nez v3, :cond_e

    .line 2206
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v9, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 2210
    :goto_2
    if-eqz v2, :cond_a

    .line 2211
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 2212
    const-string v4, "intent_extra_data_key"

    invoke-virtual {v3, v4, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 2213
    invoke-virtual {v9, v3}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 2215
    :cond_a
    invoke-static {v9}, Lcom/google/android/gms/appdatasearch/a/m;->a(Landroid/content/Intent;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    iput-object v2, v10, Lcom/google/android/gms/icing/c/a/b;->j:Ljava/lang/String;

    .line 2216
    move-object/from16 v0, p3

    iget-object v2, v0, Lcom/google/android/gms/icing/o;->b:Ljava/lang/String;

    iput-object v2, v10, Lcom/google/android/gms/icing/c/a/b;->l:Ljava/lang/String;

    .line 2217
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    iput-object v2, v10, Lcom/google/android/gms/icing/c/a/b;->m:Ljava/lang/String;

    .line 2218
    iput-object v8, v10, Lcom/google/android/gms/icing/c/a/b;->n:Lcom/google/android/gms/icing/c/a/d;

    .line 2220
    if-eqz v6, :cond_b

    .line 2221
    iput-object v6, v10, Lcom/google/android/gms/icing/c/a/b;->g:Ljava/lang/String;

    .line 2224
    :cond_b
    if-eqz v7, :cond_c

    .line 2225
    iput-object v7, v10, Lcom/google/android/gms/icing/c/a/b;->o:[Lcom/google/android/gms/icing/c/a/u;

    .line 2228
    :cond_c
    if-eqz p4, :cond_d

    .line 2229
    move-object/from16 v0, p4

    iget v2, v0, Lcom/google/android/gms/icing/ag;->g:I

    iput v2, v10, Lcom/google/android/gms/icing/c/a/b;->p:I

    .line 2232
    :cond_d
    return-object v10

    .line 2208
    :cond_e
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v9, v4, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2
.end method

.method public static a(Lcom/google/android/gms/icing/impl/a/e;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Lcom/google/android/gms/icing/g;
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1593
    new-instance v4, Lcom/google/android/gms/icing/g;

    invoke-direct {v4}, Lcom/google/android/gms/icing/g;-><init>()V

    .line 1594
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    .line 1595
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 1596
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    .line 1597
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    .line 1598
    iget-boolean v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->g:Z

    iput-boolean v0, v4, Lcom/google/android/gms/icing/g;->g:Z

    .line 1601
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    if-eqz v0, :cond_c

    .line 1602
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;->c:[Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {v1, v0}, Lcom/google/android/gms/appdatasearch/Feature;->a(I[Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    if-eqz v0, :cond_7

    move v0, v1

    .line 1605
    :goto_0
    iput-boolean v0, v4, Lcom/google/android/gms/icing/g;->m:Z

    .line 1606
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    if-eqz v0, :cond_8

    .line 1607
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->type:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    .line 1608
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    iget-object v0, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    .line 1616
    :cond_0
    :goto_1
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    if-eqz v0, :cond_9

    .line 1617
    new-instance v0, Lcom/google/android/gms/icing/h;

    invoke-direct {v0}, Lcom/google/android/gms/icing/h;-><init>()V

    .line 1618
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->b:I

    iput v3, v0, Lcom/google/android/gms/icing/h;->a:I

    .line 1619
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->c:[Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    .line 1620
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->d:Ljava/lang/String;

    if-eqz v3, :cond_1

    .line 1621
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->d:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    .line 1623
    :cond_1
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    if-eqz v3, :cond_2

    .line 1624
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    .line 1626
    :cond_2
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    if-eqz v3, :cond_3

    .line 1627
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    .line 1629
    :cond_3
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->g:Ljava/lang/String;

    if-eqz v3, :cond_4

    .line 1630
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget-object v3, v3, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->g:Ljava/lang/String;

    iput-object v3, v0, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    .line 1633
    :cond_4
    iput-object v0, v4, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    .line 1644
    :cond_5
    :goto_2
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->j:Ljava/lang/String;

    if-eqz v0, :cond_6

    .line 1645
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->j:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    .line 1649
    :cond_6
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/icing/av;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    move v0, v2

    .line 1650
    :goto_3
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v3, v3

    if-ge v0, v3, :cond_b

    .line 1651
    iget-object v3, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    aget-object v5, v3, v0

    .line 1654
    iget-object v3, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v6, "com.google.android.googlequicksearchbox"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v4, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    const-string v6, "contacts"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v4, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    const-string v6, "2"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    iget-object v3, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    const-string v6, "givennames"

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_a

    move v3, v1

    .line 1660
    :goto_4
    iget-object v6, v4, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    invoke-static {v5, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;Z)Lcom/google/android/gms/icing/av;

    move-result-object v3

    aput-object v3, v6, v0

    .line 1650
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_7
    move v0, v2

    .line 1602
    goto/16 :goto_0

    .line 1609
    :cond_8
    iget-object v0, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1611
    const-string v0, "com.google"

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    .line 1612
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    goto/16 :goto_1

    .line 1634
    :cond_9
    iget-object v0, v4, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v3, "com.google.android.gm"

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1636
    new-instance v3, Lcom/google/android/gms/icing/h;

    invoke-direct {v3}, Lcom/google/android/gms/icing/h;-><init>()V

    .line 1637
    iput v2, v3, Lcom/google/android/gms/icing/h;->a:I

    .line 1638
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->c:Ljava/util/List;

    sget-object v5, Lcom/google/android/gms/icing/impl/a/j;->c:Ljava/util/List;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v5

    new-array v5, v5, [Ljava/lang/String;

    invoke-interface {v0, v5}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    iput-object v0, v3, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    .line 1640
    const-string v0, "^f"

    iput-object v0, v3, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    .line 1642
    iput-object v3, v4, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    goto/16 :goto_2

    :cond_a
    move v3, v2

    .line 1654
    goto :goto_4

    .line 1663
    :cond_b
    return-object v4

    :cond_c
    move v0, v2

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;Lcom/google/android/gms/icing/o;)Lcom/google/android/gms/icing/g;
    .locals 8

    .prologue
    const/4 v1, 0x0

    .line 1733
    new-instance v2, Lcom/google/android/gms/icing/g;

    invoke-direct {v2}, Lcom/google/android/gms/icing/g;-><init>()V

    .line 1735
    const-string v0, ""

    iput-object v0, v2, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    .line 1736
    iget-object v0, v2, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, ".implicit:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    .line 1737
    iput-object p0, v2, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 1740
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 1743
    iget-object v0, p1, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v0, v0

    new-array v0, v0, [Lcom/google/android/gms/icing/av;

    iput-object v0, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    move v0, v1

    .line 1744
    :goto_0
    iget-object v4, p1, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    array-length v4, v4

    if-ge v0, v4, :cond_1

    .line 1745
    iget-object v4, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget-object v5, p1, Lcom/google/android/gms/icing/o;->d:[Lcom/google/android/gms/icing/q;

    aget-object v5, v5, v0

    iget-object v5, v5, Lcom/google/android/gms/icing/q;->e:Lcom/google/android/gms/icing/av;

    aput-object v5, v4, v0

    .line 1748
    iget-object v4, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v4, v4, v0

    iget v4, v4, Lcom/google/android/gms/icing/av;->k:I

    .line 1749
    sget v5, Lcom/google/android/gms/appdatasearch/DocumentSection;->a:I

    if-eq v4, v5, :cond_0

    .line 1750
    new-instance v5, Lcom/google/android/gms/icing/az;

    invoke-direct {v5}, Lcom/google/android/gms/icing/az;-><init>()V

    iput v4, v5, Lcom/google/android/gms/icing/az;->a:I

    new-instance v4, Lcom/google/android/gms/icing/ba;

    invoke-direct {v4}, Lcom/google/android/gms/icing/ba;-><init>()V

    new-instance v6, Lcom/google/android/gms/icing/bb;

    invoke-direct {v6}, Lcom/google/android/gms/icing/bb;-><init>()V

    new-instance v7, Lcom/google/android/gms/icing/bd;

    invoke-direct {v7}, Lcom/google/android/gms/icing/bd;-><init>()V

    iput-object v7, v6, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    iget-object v7, v6, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    iput v0, v7, Lcom/google/android/gms/icing/bd;->a:I

    const/4 v7, 0x1

    new-array v7, v7, [Lcom/google/android/gms/icing/bb;

    aput-object v6, v7, v1

    iput-object v7, v4, Lcom/google/android/gms/icing/ba;->a:[Lcom/google/android/gms/icing/bb;

    iput-object v4, v5, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    .line 1751
    invoke-interface {v3, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1744
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1755
    :cond_1
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_2

    .line 1756
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/icing/az;

    invoke-interface {v3, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/az;

    iput-object v0, v2, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    .line 1759
    :cond_2
    return-object v2
.end method

.method private a(Ljava/lang/String;ZZZ)Lcom/google/android/gms/icing/j;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 1015
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1016
    if-nez v0, :cond_1

    .line 1017
    if-eqz p2, :cond_0

    new-instance v0, Lcom/google/android/gms/icing/j;

    invoke-direct {v0}, Lcom/google/android/gms/icing/j;-><init>()V

    .line 1029
    :goto_0
    return-object v0

    :cond_0
    move-object v0, v1

    .line 1017
    goto :goto_0

    .line 1019
    :cond_1
    iget-object v2, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-nez v2, :cond_2

    .line 1020
    if-eqz p3, :cond_3

    .line 1021
    new-instance v1, Lcom/google/android/gms/icing/j;

    invoke-direct {v1}, Lcom/google/android/gms/icing/j;-><init>()V

    iput-object v1, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 1029
    :cond_2
    if-eqz p4, :cond_5

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/j;

    goto :goto_0

    .line 1023
    :cond_3
    if-eqz p2, :cond_4

    .line 1024
    new-instance v0, Lcom/google/android/gms/icing/j;

    invoke-direct {v0}, Lcom/google/android/gms/icing/j;-><init>()V

    goto :goto_0

    :cond_4
    move-object v0, v1

    .line 1026
    goto :goto_0

    .line 1029
    :cond_5
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/i;JJ)Ljava/lang/String;
    .locals 14

    .prologue
    .line 1428
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    .line 1429
    new-instance v5, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-direct {v5, v0, v1}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1430
    new-instance v6, Ljava/util/HashSet;

    invoke-direct {v6}, Ljava/util/HashSet;-><init>()V

    .line 1431
    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v0, :cond_0

    .line 1432
    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    iget-object v0, v0, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    .line 1434
    :cond_0
    const/4 v1, 0x0

    .line 1435
    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v7, v0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v8, v7

    const/4 v0, 0x0

    move v2, v0

    move v0, v1

    :goto_0
    if-ge v2, v8, :cond_7

    aget-object v9, v7, v2

    .line 1436
    const-string v1, "    "

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    add-int/lit8 v3, v0, 0x1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x20

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, v9, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1440
    iget-boolean v0, v9, Lcom/google/android/gms/icing/av;->b:Z

    if-nez v0, :cond_1

    .line 1441
    const-string v0, "(noindex)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1443
    :cond_1
    iget v0, v9, Lcom/google/android/gms/icing/av;->c:I

    packed-switch v0, :pswitch_data_0

    .line 1453
    const-string v0, "(unknown)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1457
    :goto_1
    :pswitch_0
    iget-object v0, v9, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    iget v1, v9, Lcom/google/android/gms/icing/av;->d:I

    invoke-interface {p0, v5, v0, v1}, Lcom/google/android/gms/icing/impl/n;->a(Lcom/google/android/gms/appdatasearch/CorpusId;Ljava/lang/String;I)I

    move-result v0

    iput v0, v9, Lcom/google/android/gms/icing/av;->d:I

    .line 1458
    iget v0, v9, Lcom/google/android/gms/icing/av;->d:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 1459
    const-string v0, " w:"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v9, Lcom/google/android/gms/icing/av;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1461
    :cond_2
    iget-object v0, v9, Lcom/google/android/gms/icing/av;->g:[I

    array-length v0, v0

    if-lez v0, :cond_4

    .line 1462
    const-string v0, " (variants"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1463
    iget-object v10, v9, Lcom/google/android/gms/icing/av;->g:[I

    array-length v11, v10

    const/4 v0, 0x0

    move v1, v0

    :goto_2
    if-ge v1, v11, :cond_3

    aget v0, v10, v1

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    .line 1464
    const-string v0, "unknown"

    .line 1465
    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    packed-switch v12, :pswitch_data_1

    .line 1470
    :goto_3
    const/16 v12, 0x20

    invoke-virtual {v4, v12}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1463
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_2

    .line 1447
    :pswitch_1
    const-string v0, "(html)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1450
    :pswitch_2
    const-string v0, "(rfc822)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_1

    .line 1467
    :pswitch_3
    const-string v0, "nick"

    goto :goto_3

    .line 1472
    :cond_3
    const/16 v0, 0x29

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1474
    :cond_4
    iget-object v0, v9, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-interface {v6, v0}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 1475
    const-string v0, " (ime)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1477
    :cond_5
    iget v0, v9, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v0, :cond_6

    .line 1478
    const-string v0, " (omni "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1479
    iget v0, v9, Lcom/google/android/gms/icing/av;->l:I

    packed-switch v0, :pswitch_data_2

    .line 1487
    const-string v0, "?)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1491
    :cond_6
    :goto_4
    const/16 v0, 0xa

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1435
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    move v0, v3

    goto/16 :goto_0

    .line 1481
    :pswitch_4
    const-string v0, "title)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1484
    :pswitch_5
    const-string v0, "url)"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_4

    .line 1493
    :cond_7
    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v2, v0

    .line 1494
    const-string v0, "Global Search Sections: "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1497
    if-lez v2, :cond_d

    .line 1498
    const/4 v0, 0x0

    move v1, v0

    :goto_5
    if-ge v1, v2, :cond_d

    .line 1499
    iget-object v0, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    aget-object v3, v0, v1

    .line 1500
    const-string v0, "    "

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, v3, Lcom/google/android/gms/icing/az;->a:I

    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, ": 0x"

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v5, v3, Lcom/google/android/gms/icing/az;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v5, " parts: \""

    invoke-virtual {v0, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1505
    const/4 v0, 0x0

    :goto_6
    iget-object v5, v3, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    iget-object v5, v5, Lcom/google/android/gms/icing/ba;->a:[Lcom/google/android/gms/icing/bb;

    array-length v5, v5

    if-ge v0, v5, :cond_c

    .line 1506
    iget-object v5, v3, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    iget-object v5, v5, Lcom/google/android/gms/icing/ba;->a:[Lcom/google/android/gms/icing/bb;

    aget-object v5, v5, v0

    .line 1507
    iget-object v6, v5, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-eqz v6, :cond_8

    .line 1508
    const-string v5, "%$bestMatch"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1505
    :goto_7
    add-int/lit8 v0, v0, 0x1

    goto :goto_6

    .line 1509
    :cond_8
    iget-object v6, v5, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-eqz v6, :cond_9

    .line 1510
    const-string v6, "$"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v7, v7, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    iget-object v5, v5, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    iget v5, v5, Lcom/google/android/gms/icing/bd;->a:I

    aget-object v5, v7, v5

    iget-object v5, v5, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1513
    :cond_9
    iget-object v6, v5, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_a

    .line 1514
    iget-object v5, v5, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1515
    :cond_a
    iget-object v5, v5, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-eqz v5, :cond_b

    .line 1516
    const-string v5, "%$uri"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1518
    :cond_b
    const-string v5, "[?unknown template part]"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_7

    .line 1521
    :cond_c
    const-string v0, "\"\n"

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1498
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto/16 :goto_5

    .line 1525
    :cond_d
    const/4 v0, 0x0

    .line 1527
    iget-object v1, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_f

    .line 1528
    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "eng"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_e

    sget-object v0, Landroid/os/Build;->TYPE:Ljava/lang/String;

    const-string v1, "userdebug"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 1529
    :cond_e
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "  account:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")\n"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 1535
    :cond_f
    :goto_8
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    .line 1536
    iget-object v1, p1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget-object v2, v1, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v5, v2

    const/4 v1, 0x0

    :goto_9
    if-ge v1, v5, :cond_11

    aget-object v6, v2, v1

    .line 1537
    const-string v7, "    "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1538
    iget-object v7, v6, Lcom/google/android/gms/icing/k;->a:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1539
    const-string v7, ": "

    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 1540
    iget v6, v6, Lcom/google/android/gms/icing/k;->b:I

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 1541
    const/16 v6, 0xa

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 1536
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1532
    :cond_10
    const-string v0, "  account:<redacted>\n"

    goto :goto_8

    .line 1545
    :cond_11
    iget-object v1, p1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    packed-switch v1, :pswitch_data_3

    .line 1556
    const-string v1, "UNKNOWN"

    .line 1559
    :goto_a
    sget-object v5, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v6, "  id:%d\n  name:%s\n  version:\"%s\"\n  package:%s\n  uri:%s\n  trimmable:%s\n  state:%s\n  last indexed/committed:%d/%d\n  source:%s\n  usageReportId:%d\n%s%s  sections:\n%s  counters:\n%s"

    const/16 v2, 0xf

    new-array v7, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v8, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v8, v8, Lcom/google/android/gms/icing/g;->a:I

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x1

    iget-object v8, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v8, v8, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    aput-object v8, v7, v2

    const/4 v2, 0x2

    iget-object v8, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v8, v8, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    aput-object v8, v7, v2

    const/4 v2, 0x3

    iget-object v8, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v8, v8, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    aput-object v8, v7, v2

    const/4 v8, 0x4

    iget-object v2, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_12

    const-string v2, "(push)"

    :goto_b
    aput-object v2, v7, v8

    const/4 v2, 0x5

    iget-object v8, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-boolean v8, v8, Lcom/google/android/gms/icing/g;->g:Z

    invoke-static {v8}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v8

    aput-object v8, v7, v2

    const/4 v2, 0x6

    aput-object v1, v7, v2

    const/4 v1, 0x7

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    const/16 v1, 0x8

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    const/16 v1, 0x9

    iget-object v2, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v2, v2, Lcom/google/android/gms/icing/g;->l:I

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/a/ac;->b(I)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v7, v1

    const/16 v1, 0xa

    iget-object v2, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v8, v2, Lcom/google/android/gms/icing/g;->i:J

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v7, v1

    const/16 v2, 0xb

    iget-object v1, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v1, :cond_13

    const-string v1, "  ime-enabled\n"

    :goto_c
    aput-object v1, v7, v2

    const/16 v1, 0xc

    if-eqz v0, :cond_14

    :goto_d
    aput-object v0, v7, v1

    const/16 v0, 0xd

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    const/16 v0, 0xe

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    aput-object v1, v7, v0

    invoke-static {v5, v6, v7}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    .line 1547
    :pswitch_6
    const-string v1, "active"

    goto/16 :goto_a

    .line 1550
    :pswitch_7
    const-string v1, "LIMBO"

    goto/16 :goto_a

    .line 1553
    :pswitch_8
    const-string v1, "DELETED"

    goto/16 :goto_a

    .line 1559
    :cond_12
    iget-object v2, p1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    goto :goto_b

    :cond_13
    const-string v1, ""

    goto :goto_c

    :cond_14
    const-string v0, ""

    goto :goto_d

    .line 1443
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch

    .line 1465
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_3
    .end packed-switch

    .line 1479
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_5
        :pswitch_4
    .end packed-switch

    .line 1545
    :pswitch_data_3
    .packed-switch 0x0
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 429
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 430
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->reset()V

    .line 431
    invoke-virtual {p1}, Ljava/lang/String;->getBytes()[B

    move-result-object v0

    .line 432
    invoke-virtual {p2}, Ljava/lang/String;->getBytes()[B

    move-result-object v2

    .line 433
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v3, v0}, Ljavax/crypto/Mac;->update([B)V

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    const/16 v3, 0x2d

    invoke-virtual {v0, v3}, Ljavax/crypto/Mac;->update(B)V

    .line 435
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v0, v2}, Ljavax/crypto/Mac;->update([B)V

    .line 437
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    invoke-virtual {v0}, Ljavax/crypto/Mac;->doFinal()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a([B)Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 438
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/impl/a/u;)Ljava/util/Map;
    .locals 6

    .prologue
    .line 975
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v1, v0

    .line 976
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 977
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_1

    .line 978
    iget-object v3, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v3, v3, v0

    .line 979
    invoke-interface {p1, p0, v3}, Lcom/google/android/gms/icing/impl/a/u;->a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/av;)Ljava/lang/String;

    move-result-object v4

    .line 980
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 981
    new-instance v5, Lcom/google/android/gms/icing/impl/a/t;

    invoke-direct {v5, v0, v3}, Lcom/google/android/gms/icing/impl/a/t;-><init>(ILcom/google/android/gms/icing/av;)V

    invoke-interface {v2, v4, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 977
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 984
    :cond_1
    return-object v2
.end method

.method private final a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;
    .locals 5

    .prologue
    .line 502
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 503
    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/util/HashSet;-><init>(I)V

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 505
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    invoke-interface {p1, v1}, Lcom/google/android/gms/icing/impl/a/q;->a(Lcom/google/android/gms/icing/i;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 507
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 511
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 510
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/i;)V
    .locals 2

    .prologue
    .line 802
    iget-object v0, p2, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    if-nez v0, :cond_0

    .line 803
    new-instance v0, Lcom/google/android/gms/icing/g;

    invoke-direct {v0}, Lcom/google/android/gms/icing/g;-><init>()V

    iput-object v0, p2, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 805
    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    if-nez v0, :cond_1

    .line 806
    new-instance v0, Lcom/google/android/gms/icing/j;

    invoke-direct {v0}, Lcom/google/android/gms/icing/j;-><init>()V

    iput-object v0, p2, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 808
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 809
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 810
    iget-object v0, p2, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 811
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/icing/j;Landroid/content/SharedPreferences$Editor;Z)V
    .locals 3

    .prologue
    .line 1109
    const-string v0, "Setting status for %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1110
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1111
    if-nez v0, :cond_0

    .line 1112
    const-string v0, "Cannot set status for non-existent key: %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1119
    :goto_0
    return-void

    .line 1115
    :cond_0
    iput-object p2, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 1116
    iget-boolean v1, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    or-int/2addr v1, p4

    iput-boolean v1, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1117
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1118
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "corpuskey:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {p3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_0
.end method

.method private a(Ljava/util/Collection;)V
    .locals 5

    .prologue
    .line 286
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 287
    if-nez p1, :cond_1

    .line 308
    :cond_0
    return-void

    .line 291
    :cond_1
    new-instance v2, Landroid/util/SparseArray;

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 293
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/m;

    .line 294
    iget v3, v0, Lcom/google/android/gms/icing/m;->a:I

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    goto :goto_0

    .line 298
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 299
    iget-object v1, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v1, v1, Lcom/google/android/gms/icing/g;->a:I

    .line 300
    invoke-virtual {v2, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/m;

    .line 301
    if-eqz v1, :cond_3

    .line 303
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 304
    iget-object v4, v1, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    iput-object v4, v0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    .line 305
    iget v1, v1, Lcom/google/android/gms/icing/m;->b:I

    iput v1, v0, Lcom/google/android/gms/icing/j;->f:I

    goto :goto_1
.end method

.method private a(Ljava/lang/String;I)Z
    .locals 6

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1216
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1217
    if-nez v0, :cond_0

    .line 1218
    const-string v0, "Can\'t find corpus with key %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    .line 1232
    :goto_0
    return v0

    .line 1221
    :cond_0
    iget-object v3, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v3, v3, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    .line 1222
    iget-object v4, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v4, v4, Lcom/google/android/gms/icing/j;->d:I

    if-eq v4, p2, :cond_1

    .line 1223
    const-string v4, "Can\'t remove corpus %s, not in expected state %s, actual state=%s"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    aput-object v3, v5, v2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    aput-object v3, v5, v1

    const/4 v1, 0x2

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v0, v0, Lcom/google/android/gms/icing/j;->d:I

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    aput-object v0, v5, v1

    invoke-static {v4, v5}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;[Ljava/lang/Object;)I

    move v0, v2

    .line 1225
    goto :goto_0

    .line 1227
    :cond_1
    const-string v4, "Removing inactive corpus %s"

    invoke-static {v4, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1228
    iput-boolean v1, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1229
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    .line 1230
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v3, v0}, Lcom/google/android/gms/icing/impl/a/f;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v0

    .line 1231
    iget-object v3, v0, Lcom/google/android/gms/icing/impl/a/e;->f:Ljava/lang/Object;

    monitor-enter v3

    :try_start_0
    iget-object v4, v0, Lcom/google/android/gms/icing/impl/a/e;->d:Ljava/util/Set;

    invoke-interface {v4, p1}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1232
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/a/e;->c()I

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    goto :goto_0

    .line 1231
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v2

    .line 1232
    goto :goto_0
.end method

.method private b(I)Ljava/util/Map;
    .locals 4

    .prologue
    .line 1186
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    .line 1187
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1188
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-ne v1, p1, :cond_0

    .line 1189
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-interface {v2, v1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 1192
    :cond_1
    return-object v2
.end method

.method public static b(Lcom/google/android/gms/icing/g;)Z
    .locals 1

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 949
    sget-object v0, Lcom/google/android/gms/icing/impl/a/u;->a:Lcom/google/android/gms/icing/impl/a/u;

    invoke-static {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/impl/a/u;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static final d(Lcom/google/android/gms/icing/g;)Ljava/util/Map;
    .locals 1

    .prologue
    .line 953
    sget-object v0, Lcom/google/android/gms/icing/impl/a/u;->b:Lcom/google/android/gms/icing/impl/a/u;

    invoke-static {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/g;Lcom/google/android/gms/icing/impl/a/u;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public static e(Lcom/google/android/gms/icing/g;)Z
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1766
    iget-object v2, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    aget-object v4, v2, v1

    .line 1767
    iget v4, v4, Lcom/google/android/gms/icing/av;->l:I

    if-eqz v4, :cond_1

    .line 1768
    const/4 v0, 0x1

    .line 1771
    :cond_0
    return v0

    .line 1766
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static f(Lcom/google/android/gms/icing/g;)Z
    .locals 2

    .prologue
    .line 1778
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    const-string v1, ".implicit:"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static g(Lcom/google/android/gms/icing/g;)[Lcom/google/android/gms/appdatasearch/Feature;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1825
    iget-boolean v0, p0, Lcom/google/android/gms/icing/g;->m:Z

    if-eqz v0, :cond_0

    .line 1826
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-static {}, Lcom/google/android/gms/appdatasearch/w;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v1

    aput-object v1, v0, v2

    .line 1830
    :goto_0
    return-object v0

    .line 1828
    :cond_0
    new-array v0, v2, [Lcom/google/android/gms/appdatasearch/Feature;

    goto :goto_0
.end method

.method public static h(Lcom/google/android/gms/icing/g;)Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;
    .locals 12

    .prologue
    const/4 v9, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 1838
    .line 1839
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v0, :cond_e

    .line 1840
    iget-object v6, p0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    .line 1841
    new-instance v0, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    iget v1, v6, Lcom/google/android/gms/icing/h;->a:I

    iget-object v2, v6, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    iget-object v3, v6, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v6, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, v6, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    if-eqz v5, :cond_0

    array-length v10, v5

    if-nez v10, :cond_1

    :cond_0
    move-object v5, v7

    :cond_1
    check-cast v5, [Ljava/lang/String;

    iget-object v6, v6, Lcom/google/android/gms/icing/h;->g:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;-><init>(I[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    move-object v1, v0

    .line 1849
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ai;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/appdatasearch/ai;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    if-nez v0, :cond_2

    :goto_1
    iput-object v7, v2, Lcom/google/android/gms/appdatasearch/ai;->b:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-static {p0}, Lcom/google/android/gms/icing/impl/a/j;->g(Lcom/google/android/gms/icing/g;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/appdatasearch/u;

    invoke-direct {v5}, Lcom/google/android/gms/appdatasearch/u;-><init>()V

    array-length v6, v3

    move v0, v8

    :goto_2
    if-ge v0, v6, :cond_3

    aget-object v7, v3, v0

    iget v10, v7, Lcom/google/android/gms/icing/az;->a:I

    invoke-static {v10}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v10

    iget v7, v7, Lcom/google/android/gms/icing/az;->c:I

    invoke-static {v10}, Lcom/google/android/gms/appdatasearch/y;->a(Ljava/lang/String;)I

    move-result v10

    iget-object v11, v5, Lcom/google/android/gms/appdatasearch/u;->a:[I

    aput v7, v11, v10

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v7

    goto :goto_1

    :cond_3
    if-eqz v4, :cond_4

    move v0, v8

    :goto_3
    array-length v3, v4

    if-ge v0, v3, :cond_4

    iget-object v3, v5, Lcom/google/android/gms/appdatasearch/u;->b:Ljava/util/ArrayList;

    aget-object v6, v4, v0

    invoke-static {v3, v6}, Lcom/google/android/gms/appdatasearch/Feature;->a(Ljava/util/List;Lcom/google/android/gms/appdatasearch/Feature;)V

    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    :cond_4
    new-instance v3, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v4, v5, Lcom/google/android/gms/appdatasearch/u;->a:[I

    iget-object v0, v5, Lcom/google/android/gms/appdatasearch/u;->b:Ljava/util/ArrayList;

    iget-object v5, v5, Lcom/google/android/gms/appdatasearch/u;->b:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    new-array v5, v5, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/appdatasearch/Feature;

    invoke-direct {v3, v4, v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;-><init>([I[Lcom/google/android/gms/appdatasearch/Feature;)V

    iput-object v3, v2, Lcom/google/android/gms/appdatasearch/ai;->d:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-boolean v0, p0, Lcom/google/android/gms/icing/g;->g:Z

    iput-boolean v0, v2, Lcom/google/android/gms/appdatasearch/ai;->e:Z

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/e/d;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/appdatasearch/ai;->h:Ljava/lang/String;

    iput-object v1, v2, Lcom/google/android/gms/appdatasearch/ai;->g:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    .line 1858
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 1859
    :cond_5
    new-instance v0, Landroid/accounts/Account;

    iget-object v1, p0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-direct {v0, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, v2, Lcom/google/android/gms/appdatasearch/ai;->f:Landroid/accounts/Account;

    :cond_6
    move v1, v8

    .line 1862
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v0, v0

    if-ge v1, v0, :cond_d

    .line 1863
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v3, v0, v1

    .line 1864
    sget-object v0, Lcom/google/android/gms/icing/impl/a/j;->e:Landroid/util/SparseArray;

    iget v4, v3, Lcom/google/android/gms/icing/av;->c:I

    invoke-virtual {v0, v4}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1865
    if-nez v0, :cond_7

    .line 1866
    const-string v0, "plain"

    .line 1868
    :cond_7
    iget-object v4, v3, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/ak;

    move-result-object v4

    iput-object v0, v4, Lcom/google/android/gms/appdatasearch/ak;->a:Ljava/lang/String;

    iget-boolean v0, v3, Lcom/google/android/gms/icing/av;->b:Z

    if-nez v0, :cond_b

    move v0, v9

    :goto_5
    iput-boolean v0, v4, Lcom/google/android/gms/appdatasearch/ak;->b:Z

    iget v0, v3, Lcom/google/android/gms/icing/av;->d:I

    iput v0, v4, Lcom/google/android/gms/appdatasearch/ak;->c:I

    iget-boolean v0, v3, Lcom/google/android/gms/icing/av;->e:Z

    iput-boolean v0, v4, Lcom/google/android/gms/appdatasearch/ak;->d:Z

    iget-object v0, v3, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/appdatasearch/ak;->e:Ljava/lang/String;

    .line 1874
    iget-object v0, v3, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    .line 1875
    iget-object v0, v3, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/appdatasearch/ak;->f:Ljava/lang/String;

    .line 1877
    :cond_8
    iget-object v0, v3, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a([I)I

    move-result v0

    const/4 v5, -0x1

    if-eq v0, v5, :cond_9

    .line 1879
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->a()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/appdatasearch/ak;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/ak;

    .line 1881
    :cond_9
    iget v0, v3, Lcom/google/android/gms/icing/av;->l:I

    if-ne v0, v9, :cond_c

    .line 1882
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->b()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/appdatasearch/ak;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/ak;

    .line 1886
    :cond_a
    :goto_6
    invoke-virtual {v2, v4}, Lcom/google/android/gms/appdatasearch/ai;->a(Lcom/google/android/gms/appdatasearch/ak;)Lcom/google/android/gms/appdatasearch/ai;

    .line 1862
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_4

    :cond_b
    move v0, v8

    .line 1868
    goto :goto_5

    .line 1883
    :cond_c
    iget v0, v3, Lcom/google/android/gms/icing/av;->l:I

    const/4 v3, 0x2

    if-ne v0, v3, :cond_a

    .line 1884
    invoke-static {}, Lcom/google/android/gms/appdatasearch/aw;->c()Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/appdatasearch/ak;->a(Lcom/google/android/gms/appdatasearch/Feature;)Lcom/google/android/gms/appdatasearch/ak;

    goto :goto_6

    .line 1889
    :cond_d
    invoke-virtual {v2}, Lcom/google/android/gms/appdatasearch/ai;->a()Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;

    move-result-object v0

    return-object v0

    :cond_e
    move-object v1, v7

    goto/16 :goto_0
.end method

.method private v()Ljava/security/SecureRandom;
    .locals 1

    .prologue
    .line 340
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->n:Ljava/security/SecureRandom;

    if-nez v0, :cond_0

    .line 341
    new-instance v0, Ljava/security/SecureRandom;

    invoke-direct {v0}, Ljava/security/SecureRandom;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->n:Ljava/security/SecureRandom;

    .line 343
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->n:Ljava/security/SecureRandom;

    return-object v0
.end method

.method private w()Ljavax/crypto/Mac;
    .locals 4

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v1, "hmackey"

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->contains(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v1, "hmackey"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 361
    :goto_0
    :try_start_0
    const-string v1, "HmacSHA1"

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bp;->c(Ljava/lang/String;)Ljavax/crypto/Mac;

    move-result-object v1

    .line 362
    if-nez v1, :cond_1

    .line 363
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Cannot find algo"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_0 .. :try_end_0} :catch_1

    .line 369
    :catch_0
    move-exception v0

    .line 370
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "CannotHappenException"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1

    .line 353
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->v()Ljava/security/SecureRandom;

    move-result-object v0

    .line 354
    const/16 v1, 0x14

    new-array v1, v1, [B

    .line 355
    invoke-virtual {v0, v1}, Ljava/security/SecureRandom;->nextBytes([B)V

    .line 356
    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bp;->a([B)Ljava/lang/String;

    move-result-object v0

    .line 357
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "hmackey"

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0

    .line 365
    :cond_1
    :try_start_1
    const-string v2, "UTF-8"

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v0

    .line 366
    new-instance v2, Ljavax/crypto/spec/SecretKeySpec;

    const-string v3, "HmacSHA1"

    invoke-direct {v2, v0, v3}, Ljavax/crypto/spec/SecretKeySpec;-><init>([BLjava/lang/String;)V

    .line 367
    invoke-virtual {v1, v2}, Ljavax/crypto/Mac;->init(Ljava/security/Key;)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/security/InvalidKeyException; {:try_start_1 .. :try_end_1} :catch_1

    .line 368
    return-object v1

    .line 371
    :catch_1
    move-exception v0

    .line 372
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Bad key type"

    invoke-direct {v1, v2, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method private x()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 378
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    iget-object v0, v0, Lcom/google/android/gms/icing/impl/e/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 379
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    iget-object v0, v2, Lcom/google/android/gms/icing/impl/e/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->exists()Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "Deleting scratch file %s"

    iget-object v3, v2, Lcom/google/android/gms/icing/impl/e/f;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    iget-object v0, v2, Lcom/google/android/gms/icing/impl/e/f;->b:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move-result v0

    if-nez v0, :cond_0

    const-string v3, "Failed to delete scratch file %s"

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/e/f;->a:Ljava/lang/String;

    invoke-static {v3, v2}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :cond_0
    :goto_0
    if-nez v0, :cond_1

    .line 380
    const-string v0, "Failed to delete stale scratch file."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 383
    :cond_1
    iput-boolean v1, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 384
    return-void

    .line 379
    :cond_2
    const-string v0, "Scratch file %s already deleted"

    iget-object v2, v2, Lcom/google/android/gms/icing/impl/e/f;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    move v0, v1

    goto :goto_0
.end method

.method private y()I
    .locals 3

    .prologue
    .line 786
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->b(Z)V

    .line 788
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    .line 789
    const/4 v0, -0x1

    .line 791
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 792
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    .line 793
    if-le v0, v1, :cond_1

    :goto_1
    move v1, v0

    .line 796
    goto :goto_0

    .line 797
    :cond_0
    return v1

    :cond_1
    move v0, v1

    goto :goto_1
.end method

.method private z()Z
    .locals 9

    .prologue
    .line 2318
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 2319
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 2320
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2321
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 2322
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 2323
    iget-object v5, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v6, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v6, v6, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v7, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v7, v7, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    iget-object v8, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v8, v8, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-direct {p0, v6, v7, v8}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v6

    iput-wide v6, v5, Lcom/google/android/gms/icing/g;->i:J

    .line 2325
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "corpuskey:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2332
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2327
    :cond_0
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    if-nez v0, :cond_1

    .line 2328
    const-string v0, "Failed to write backfilled corpus configs to preferences."

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 2329
    const/4 v0, 0x0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 2331
    :goto_1
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v2

    goto :goto_1
.end method


# virtual methods
.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)I
    .locals 5

    .prologue
    const/4 v1, -0x1

    .line 1130
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 1131
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/google/android/gms/icing/impl/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1132
    monitor-exit v2

    move v0, v1

    .line 1145
    :goto_0
    return v0

    .line 1135
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1136
    if-eqz v0, :cond_1

    .line 1137
    iget-object v1, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v1, v1, Lcom/google/android/gms/icing/g;->a:I

    .line 1138
    iget-object v3, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v3, v3, Lcom/google/android/gms/icing/j;->d:I

    if-nez v3, :cond_1

    .line 1139
    iget-object v3, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    const/4 v4, 0x1

    iput v4, v3, Lcom/google/android/gms/icing/j;->d:I

    .line 1140
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 1141
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    const/4 v4, 0x0

    invoke-direct {p0, p1, v0, v3, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/j;Landroid/content/SharedPreferences$Editor;Z)V

    .line 1142
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    :cond_1
    move v0, v1

    .line 1145
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1146
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final a(Ljava/lang/Runnable;)Lcom/google/android/gms/icing/impl/a/p;
    .locals 1

    .prologue
    .line 279
    new-instance v0, Lcom/google/android/gms/icing/impl/a/p;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/icing/impl/a/p;-><init>(Lcom/google/android/gms/icing/impl/a/j;Ljava/lang/Runnable;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 411
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/g;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 425
    iget-object v0, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v1, p1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 406
    iget-object v0, p1, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-direct {p0, v0, p2}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 6

    .prologue
    .line 449
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 450
    :try_start_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/j;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v0

    .line 451
    if-nez v0, :cond_0

    .line 452
    const/4 v0, 0x0

    monitor-exit v1

    .line 455
    :goto_0
    return-object v0

    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "-"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, v0, Lcom/google/android/gms/icing/g;->h:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 456
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Z)Ljava/util/List;
    .locals 7

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Thread;->holdsLock(Ljava/lang/Object;)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 637
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/j;->c()Ljava/util/List;

    move-result-object v0

    .line 640
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v1, v1, Lcom/google/android/gms/icing/impl/a/f;->a:Lcom/google/android/gms/icing/impl/a/g;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/g;->a()Z

    move-result v1

    .line 642
    new-instance v2, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(I)V

    .line 644
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 645
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v5, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v4, v5}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v4

    .line 646
    if-eqz v4, :cond_0

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    iget-object v6, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/gms/icing/impl/a/f;->e(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    if-nez p2, :cond_1

    invoke-virtual {v4, v1}, Lcom/google/android/gms/icing/impl/a/e;->b(Z)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_1
    iget-object v4, v0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v4, v4

    if-lez v4, :cond_0

    if-eqz p1, :cond_2

    iget-object v4, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v5, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {p1, v4, v5}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a(Ljava/lang/String;Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    :cond_2
    if-eqz p1, :cond_3

    iget v4, p1, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_3

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->e(Lcom/google/android/gms/icing/g;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 656
    :cond_3
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 660
    :cond_4
    return-object v2
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/aa;I)Ljava/util/List;
    .locals 6

    .prologue
    .line 603
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 604
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 605
    const/4 v0, 0x0

    const/4 v3, 0x0

    invoke-virtual {p0, p1, v0, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    .line 606
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 607
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v4

    .line 610
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    invoke-virtual {v0, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_1

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v0

    if-ne p2, v0, :cond_0

    .line 614
    :cond_1
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 618
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 617
    :cond_2
    :try_start_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/List;
    .locals 5

    .prologue
    .line 586
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 587
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v0

    .line 588
    new-instance v3, Ljava/util/ArrayList;

    invoke-interface {v0}, Ljava/util/Collection;->size()I

    move-result v1

    invoke-direct {v3, v1}, Ljava/util/ArrayList;-><init>(I)V

    .line 589
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 590
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    .line 591
    if-eqz v1, :cond_0

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_0

    .line 593
    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 597
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 596
    :cond_1
    :try_start_1
    invoke-static {v3}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;
    .locals 6

    .prologue
    .line 1336
    iget-boolean v0, p1, Lcom/google/android/gms/icing/impl/a/h;->b:Z

    if-eqz v0, :cond_1

    .line 1337
    sget-object v0, Lcom/google/android/gms/icing/impl/a/q;->b:Lcom/google/android/gms/icing/impl/a/q;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    move-object v1, v0

    .line 1362
    :cond_0
    :goto_0
    if-eqz p2, :cond_4

    .line 1363
    new-instance v2, Ljava/util/ArrayList;

    array-length v0, p2

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 1364
    array-length v3, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v3, :cond_3

    aget-object v4, p2, v0

    .line 1365
    iget-object v5, p1, Lcom/google/android/gms/icing/impl/a/aa;->e:Ljava/lang/String;

    invoke-direct {p0, v5, v4}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {v2, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 1364
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1339
    :cond_1
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 1340
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/h;)Ljava/util/Set;

    move-result-object v0

    .line 1341
    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 1342
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/List;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_2

    .line 1346
    :cond_2
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/aa;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    .line 1347
    new-instance v0, Lcom/google/android/gms/icing/impl/a/n;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/a/n;-><init>(Lcom/google/android/gms/icing/impl/a/j;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Set;->addAll(Ljava/util/Collection;)Z

    goto :goto_0

    .line 1369
    :cond_3
    invoke-interface {v1, v2}, Ljava/util/Set;->retainAll(Ljava/util/Collection;)Z

    .line 1372
    :cond_4
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 6

    .prologue
    .line 390
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 391
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->clear()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v2, "created"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 393
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->x()V

    .line 394
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 395
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->w()Ljavax/crypto/Mac;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->m:Ljavax/crypto/Mac;

    .line 396
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;[Lcom/google/android/gms/icing/s;[Lcom/google/android/gms/icing/s;)V
    .locals 13

    .prologue
    .line 1385
    iget-object v8, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v8

    .line 1386
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 1387
    const-string v2, "No corpora\n"

    invoke-virtual {p1, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 1417
    :cond_0
    monitor-exit v8

    return-void

    .line 1390
    :cond_1
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 1392
    new-instance v3, Lcom/google/android/gms/icing/impl/a/o;

    invoke-direct {v3, p0}, Lcom/google/android/gms/icing/impl/a/o;-><init>(Lcom/google/android/gms/icing/impl/a/j;)V

    invoke-static {v2, v3}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 1406
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_0
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v0, v2

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 1407
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/i;

    iget-object v2, v2, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v2, v2, Lcom/google/android/gms/icing/g;->a:I

    .line 1408
    array-length v4, p2

    if-ge v2, v4, :cond_2

    aget-object v4, p2, v2

    iget-wide v4, v4, Lcom/google/android/gms/icing/s;->a:J

    .line 1410
    :goto_1
    move-object/from16 v0, p3

    array-length v6, v0

    if-ge v2, v6, :cond_3

    aget-object v2, p3, v2

    iget-wide v6, v2, Lcom/google/android/gms/icing/s;->a:J

    .line 1413
    :goto_2
    const-string v10, "%s:\n%s\n"

    const/4 v2, 0x2

    new-array v11, v2, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v12

    aput-object v12, v11, v2

    const/4 v12, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->h:Lcom/google/android/gms/icing/impl/n;

    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/icing/i;

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/i;JJ)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v11, v12

    invoke-virtual {p1, v10, v11}, Ljava/io/PrintWriter;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintWriter;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1417
    :catchall_0
    move-exception v2

    monitor-exit v8

    throw v2

    .line 1408
    :cond_2
    const-wide/16 v4, 0x0

    goto :goto_1

    .line 1410
    :cond_3
    const-wide/16 v6, 0x0

    goto :goto_2
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)V
    .locals 1

    .prologue
    .line 839
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, p2, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;Lcom/google/android/gms/icing/impl/a/s;)V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/d; {:try_start_0 .. :try_end_0} :catch_0

    .line 843
    :goto_0
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;Lcom/google/android/gms/icing/impl/a/s;)V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 858
    iget-object v5, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v5

    .line 859
    :try_start_0
    const-string v0, "Setting config for %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 860
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 864
    if-nez v0, :cond_4

    .line 866
    new-instance v1, Ljava/util/BitSet;

    invoke-direct {v1}, Ljava/util/BitSet;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v1, v0}, Ljava/util/BitSet;->set(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 910
    :catchall_0
    move-exception v0

    monitor-exit v5

    throw v0

    .line 866
    :cond_0
    :try_start_1
    invoke-virtual {v1}, Ljava/util/BitSet;->length()I

    move-result v0

    invoke-virtual {v1}, Ljava/util/BitSet;->cardinality()I

    move-result v3

    if-ne v0, v3, :cond_2

    invoke-virtual {v1}, Ljava/util/BitSet;->length()I

    move-result v2

    .line 867
    :cond_1
    const v0, 0xfffe

    if-le v2, v0, :cond_3

    .line 869
    new-instance v0, Lcom/google/android/gms/icing/impl/b/a;

    const-string v1, "Too many corpora"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 866
    :cond_2
    :goto_1
    invoke-virtual {v1}, Ljava/util/BitSet;->length()I

    move-result v0

    if-ge v2, v0, :cond_1

    invoke-virtual {v1, v2}, Ljava/util/BitSet;->get(I)Z

    move-result v0

    if-eqz v0, :cond_1

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 873
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->v()Ljava/security/SecureRandom;

    move-result-object v0

    .line 874
    invoke-virtual {v0}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v0

    .line 876
    const/4 v3, 0x0

    new-instance v4, Lcom/google/android/gms/icing/i;

    invoke-direct {v4}, Lcom/google/android/gms/icing/i;-><init>()V

    const-wide/16 v6, 0x0

    invoke-static {v3, v4, v6, v7}, Lcom/google/android/gms/icing/impl/a/ac;->a(ILjava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v3

    move-object v4, v3

    move v8, v2

    move-wide v2, v0

    move v1, v8

    .line 883
    :goto_2
    invoke-virtual {v4, p2}, Lcom/google/android/gms/icing/impl/a/ac;->a(Lcom/google/android/gms/icing/impl/a/ac;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 884
    new-instance v0, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "CorpusConfig: cannot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " when previously "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 878
    :cond_4
    iget-object v1, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v1, v1, Lcom/google/android/gms/icing/g;->a:I

    .line 879
    iget-object v2, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v2, v2, Lcom/google/android/gms/icing/g;->h:J

    .line 880
    iget-object v4, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v4, v4, Lcom/google/android/gms/icing/g;->l:I

    iget-object v6, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v6, v6, Lcom/google/android/gms/icing/g;->n:J

    invoke-static {v4, v0, v6, v7}, Lcom/google/android/gms/icing/impl/a/ac;->a(ILjava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    move-object v4, v0

    goto :goto_2

    .line 887
    :cond_5
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v6

    iput v6, v0, Lcom/google/android/gms/icing/g;->l:I

    .line 888
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->c()J

    move-result-wide v6

    iput-wide v6, v0, Lcom/google/android/gms/icing/g;->n:J

    .line 889
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iput v1, v0, Lcom/google/android/gms/icing/g;->a:I

    .line 890
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 892
    :cond_6
    new-instance v0, Lcom/google/android/gms/icing/impl/b/a;

    const-string v1, "Need name and package name"

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v0

    .line 894
    :cond_7
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    iput-wide v2, v0, Lcom/google/android/gms/icing/g;->h:J

    .line 896
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v2, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v3, v1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-direct {p0, v2, v3, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/icing/g;->i:J

    .line 899
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/protobuf/nano/j;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    iput-object v1, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 900
    if-eqz p3, :cond_8

    .line 902
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    invoke-interface {p3, v0}, Lcom/google/android/gms/icing/impl/a/s;->a(Lcom/google/android/gms/icing/g;)V

    .line 905
    :cond_8
    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/i;)V

    .line 907
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 908
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "corpuskey:"

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v4}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v2, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 909
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 910
    monitor-exit v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/j;)V
    .locals 4

    .prologue
    .line 1057
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1058
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1059
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/j;

    const/4 v3, 0x1

    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/j;Landroid/content/SharedPreferences$Editor;Z)V

    .line 1060
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 1061
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;[Lcom/google/android/gms/icing/k;Ljava/lang/Integer;Lcom/google/android/gms/icing/l;)V
    .locals 4

    .prologue
    .line 1085
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1086
    const/4 v0, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    :try_start_0
    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;ZZZ)Lcom/google/android/gms/icing/j;

    move-result-object v0

    .line 1087
    if-nez v0, :cond_1

    .line 1088
    const-string v0, "Cannot set status for non-existent key: %s"

    invoke-static {v0, p1}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    .line 1103
    :cond_0
    :goto_0
    monitor-exit v1

    return-void

    .line 1090
    :cond_1
    if-eqz p2, :cond_2

    iget-object v2, v0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    invoke-static {v2, p2}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1091
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1092
    iput-object p2, v0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    .line 1094
    :cond_2
    if-eqz p3, :cond_3

    iget v2, v0, Lcom/google/android/gms/icing/j;->f:I

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    if-eq v2, v3, :cond_3

    .line 1095
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1096
    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v0, Lcom/google/android/gms/icing/j;->f:I

    .line 1098
    :cond_3
    if-eqz p4, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;

    invoke-virtual {p4, v2}, Lcom/google/android/gms/icing/l;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1099
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1100
    iput-object p4, v0, Lcom/google/android/gms/icing/j;->g:Lcom/google/android/gms/icing/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1103
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(I)Z
    .locals 11

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2262
    .line 2263
    add-int/lit8 v0, p1, 0x1

    move v6, v0

    move v5, v4

    :goto_0
    const/16 v0, 0x35

    if-gt v6, v0, :cond_6

    .line 2264
    sparse-switch v6, :sswitch_data_0

    :goto_1
    move v0, v5

    .line 2306
    :goto_2
    if-nez v0, :cond_5

    move v0, v3

    .line 2314
    :goto_3
    return v0

    .line 2266
    :sswitch_0
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->z()Z

    move-result v0

    goto :goto_2

    .line 2271
    :sswitch_1
    new-instance v0, Ljava/io/File;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->k:Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v7, p0, Lcom/google/android/gms/icing/impl/a/j;->f:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v7, "-corpus-counters-scratch.tmp"

    invoke-virtual {v2, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/File;->delete()Z

    move v0, v5

    .line 2274
    goto :goto_2

    .line 2277
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v7

    .line 2278
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 2279
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :cond_0
    :goto_4
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2280
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v9, "com.google.android.gm"

    invoke-virtual {v1, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 2281
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_4

    .line 2284
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v8

    :goto_5
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2285
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    .line 2287
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    if-nez v2, :cond_2

    .line 2288
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    const-string v9, "com.google"

    iput-object v9, v2, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    .line 2289
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v9, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v9, v9, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    iput-object v9, v2, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    .line 2292
    :cond_2
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v2, v2, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-nez v2, :cond_3

    .line 2293
    new-instance v9, Lcom/google/android/gms/icing/h;

    invoke-direct {v9}, Lcom/google/android/gms/icing/h;-><init>()V

    .line 2294
    iput v3, v9, Lcom/google/android/gms/icing/h;->a:I

    .line 2295
    sget-object v2, Lcom/google/android/gms/icing/impl/a/j;->c:Ljava/util/List;

    sget-object v10, Lcom/google/android/gms/icing/impl/a/j;->c:Ljava/util/List;

    invoke-interface {v10}, Ljava/util/List;->size()I

    move-result v10

    new-array v10, v10, [Ljava/lang/String;

    invoke-interface {v2, v10}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    iput-object v2, v9, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    .line 2298
    const-string v2, "^f"

    iput-object v2, v9, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    .line 2299
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iput-object v9, v2, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    .line 2303
    :cond_3
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v9, "corpuskey:"

    invoke-direct {v2, v9}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v1

    invoke-interface {v7, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    goto :goto_5

    .line 2305
    :cond_4
    invoke-interface {v7}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_1

    .line 2263
    :cond_5
    add-int/lit8 v1, v6, 0x1

    move v6, v1

    move v5, v0

    goto/16 :goto_0

    :cond_6
    move v0, v4

    .line 2314
    goto/16 :goto_3

    .line 2264
    nop

    :sswitch_data_0
    .sparse-switch
        0x1f -> :sswitch_0
        0x27 -> :sswitch_1
        0x28 -> :sswitch_2
    .end sparse-switch
.end method

.method public final a(Lcom/google/android/gms/icing/impl/a/h;Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 1315
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1316
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->b:Lcom/google/android/gms/icing/impl/a/f;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/icing/impl/a/f;->a(Lcom/google/android/gms/icing/impl/a/h;)Ljava/util/Set;

    move-result-object v0

    .line 1317
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/impl/a/e;

    .line 1318
    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/List;

    move-result-object v3

    .line 1319
    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1320
    invoke-virtual {p0, v0, p2}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/e;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 1321
    invoke-interface {v3, v0}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1322
    const/4 v0, 0x0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1326
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 1327
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;Lcom/google/android/gms/icing/impl/a/ac;)Z
    .locals 12

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 1905
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 1906
    invoke-virtual {p3}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/g;

    .line 1907
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/icing/g;

    .line 1916
    invoke-virtual {p2}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v3

    invoke-virtual {p3}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v6

    if-eq v3, v6, :cond_11

    .line 1917
    invoke-virtual {p3}, Lcom/google/android/gms/icing/impl/a/ac;->b()I

    move-result v3

    iput v3, v2, Lcom/google/android/gms/icing/g;->l:I

    move v3, v4

    .line 1922
    :goto_0
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_1

    .line 2043
    :cond_0
    :goto_1
    return v5

    .line 1927
    :cond_1
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1932
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1937
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v6

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v7

    if-ne v6, v7, :cond_0

    .line 1941
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    if-eqz v6, :cond_2

    iget-object v6, v0, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1944
    iget-object v3, v1, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    iput-object v3, v2, Lcom/google/android/gms/icing/g;->e:Ljava/lang/String;

    move v3, v4

    .line 1948
    :cond_2
    iget v6, v0, Lcom/google/android/gms/icing/g;->f:I

    iget v7, v1, Lcom/google/android/gms/icing/g;->f:I

    if-eq v6, v7, :cond_3

    .line 1950
    iget v3, v1, Lcom/google/android/gms/icing/g;->f:I

    iput v3, v2, Lcom/google/android/gms/icing/g;->f:I

    move v3, v4

    .line 1954
    :cond_3
    iget-boolean v6, v0, Lcom/google/android/gms/icing/g;->g:Z

    iget-boolean v7, v1, Lcom/google/android/gms/icing/g;->g:Z

    if-eq v6, v7, :cond_4

    .line 1956
    iget-boolean v3, v1, Lcom/google/android/gms/icing/g;->g:Z

    iput-boolean v3, v2, Lcom/google/android/gms/icing/g;->g:Z

    move v3, v4

    .line 1962
    :cond_4
    iget-object v6, v0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v6, v6

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v7, v7

    if-ne v6, v7, :cond_0

    move v6, v3

    move v3, v5

    .line 1965
    :goto_2
    iget-object v7, v0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v7, v7

    if-ge v3, v7, :cond_9

    .line 1966
    iget-object v7, v0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v7, v7, v3

    .line 1967
    iget-object v8, v1, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v8, v8, v3

    .line 1970
    iget-object v9, v7, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    iget-object v10, v8, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_0

    iget-boolean v9, v7, Lcom/google/android/gms/icing/av;->b:Z

    iget-boolean v10, v8, Lcom/google/android/gms/icing/av;->b:Z

    if-ne v9, v10, :cond_0

    iget v9, v7, Lcom/google/android/gms/icing/av;->c:I

    iget v10, v8, Lcom/google/android/gms/icing/av;->c:I

    if-ne v9, v10, :cond_0

    iget-boolean v9, v7, Lcom/google/android/gms/icing/av;->e:Z

    iget-boolean v10, v8, Lcom/google/android/gms/icing/av;->e:Z

    if-ne v9, v10, :cond_0

    iget-object v9, v7, Lcom/google/android/gms/icing/av;->g:[I

    iget-object v10, v8, Lcom/google/android/gms/icing/av;->g:[I

    invoke-static {v9, v10}, Ljava/util/Arrays;->equals([I[I)Z

    move-result v9

    if-eqz v9, :cond_0

    iget v9, v7, Lcom/google/android/gms/icing/av;->h:I

    iget v10, v8, Lcom/google/android/gms/icing/av;->h:I

    if-ne v9, v10, :cond_0

    iget v9, v7, Lcom/google/android/gms/icing/av;->i:I

    iget v10, v8, Lcom/google/android/gms/icing/av;->i:I

    if-ne v9, v10, :cond_0

    iget v9, v7, Lcom/google/android/gms/icing/av;->l:I

    iget v10, v8, Lcom/google/android/gms/icing/av;->l:I

    if-ne v9, v10, :cond_0

    .line 1982
    iget-object v9, v7, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    iget-object v10, v8, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_5

    .line 1984
    iget-object v6, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v6, v6, v3

    iget-object v9, v8, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    iput-object v9, v6, Lcom/google/android/gms/icing/av;->f:Ljava/lang/String;

    move v6, v4

    .line 1988
    :cond_5
    iget v9, v7, Lcom/google/android/gms/icing/av;->d:I

    iget v10, v8, Lcom/google/android/gms/icing/av;->d:I

    if-eq v9, v10, :cond_6

    .line 1990
    iget-object v6, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v6, v6, v3

    iget v9, v8, Lcom/google/android/gms/icing/av;->d:I

    iput v9, v6, Lcom/google/android/gms/icing/av;->d:I

    move v6, v4

    .line 1994
    :cond_6
    iget-object v9, v7, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    iget-object v10, v8, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-nez v9, :cond_7

    .line 1996
    iget-object v6, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v6, v6, v3

    iget-object v9, v8, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    iput-object v9, v6, Lcom/google/android/gms/icing/av;->j:Ljava/lang/String;

    move v6, v4

    .line 2000
    :cond_7
    iget v7, v7, Lcom/google/android/gms/icing/av;->k:I

    iget v9, v8, Lcom/google/android/gms/icing/av;->k:I

    if-eq v7, v9, :cond_8

    .line 2002
    iget-object v6, v2, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v6, v6, v3

    iget v7, v8, Lcom/google/android/gms/icing/av;->k:I

    iput v7, v6, Lcom/google/android/gms/icing/av;->k:I

    move v6, v4

    .line 1965
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_2

    .line 2007
    :cond_9
    iget-object v3, v0, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    invoke-static {v3, v7}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 2010
    iget-object v3, v1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    iput-object v3, v2, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    move v6, v4

    .line 2013
    :cond_a
    iget-boolean v3, v0, Lcom/google/android/gms/icing/g;->m:Z

    iget-boolean v7, v1, Lcom/google/android/gms/icing/g;->m:Z

    if-eq v3, v7, :cond_b

    .line 2015
    iget-boolean v3, v1, Lcom/google/android/gms/icing/g;->m:Z

    iput-boolean v3, v2, Lcom/google/android/gms/icing/g;->m:Z

    move v6, v4

    .line 2018
    :cond_b
    iget-wide v8, v0, Lcom/google/android/gms/icing/g;->n:J

    iget-wide v10, v1, Lcom/google/android/gms/icing/g;->n:J

    cmp-long v3, v8, v10

    if-eqz v3, :cond_c

    .line 2020
    iget-wide v6, v1, Lcom/google/android/gms/icing/g;->n:J

    iput-wide v6, v2, Lcom/google/android/gms/icing/g;->n:J

    move v6, v4

    .line 2024
    :cond_c
    iget-object v3, v0, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    iget-object v3, v0, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 2030
    iget-object v3, v0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    iget-object v7, v1, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-nez v3, :cond_10

    if-nez v7, :cond_f

    move v3, v4

    :goto_3
    if-eqz v3, :cond_0

    .line 2035
    iget-object v0, v0, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    iget-object v3, v1, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_d

    .line 2037
    iget-object v0, v1, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    iput-object v0, v2, Lcom/google/android/gms/icing/g;->r:Ljava/lang/String;

    move v6, v4

    .line 2040
    :cond_d
    if-eqz v6, :cond_e

    .line 2041
    invoke-virtual {p3, v2}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/ac;)V

    :cond_e
    move v5, v4

    .line 2043
    goto/16 :goto_1

    :cond_f
    move v3, v5

    .line 2030
    goto :goto_3

    :cond_10
    invoke-virtual {v3, v7}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    goto :goto_3

    :cond_11
    move v3, v5

    goto/16 :goto_0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;
    .locals 2

    .prologue
    .line 675
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 676
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 677
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    monitor-exit v1

    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 678
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 401
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/icing/impl/a/e;)Z
    .locals 4

    .prologue
    .line 1783
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1784
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v0

    .line 1785
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1786
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1787
    if-eqz v0, :cond_0

    iget-object v3, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v3, v3, Lcom/google/android/gms/icing/j;->d:I

    if-nez v3, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1790
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1793
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 1794
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;Lcom/google/android/gms/icing/impl/a/e;)Z
    .locals 6

    .prologue
    const/4 v4, 0x1

    .line 1158
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 1159
    :try_start_0
    invoke-virtual {p2, p1}, Lcom/google/android/gms/icing/impl/a/e;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 1160
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Key "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " doesn\'t exist for pkg "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v3, p2, Lcom/google/android/gms/icing/impl/a/e;->a:Ljava/lang/String;

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1182
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1163
    :cond_0
    const/4 v1, 0x0

    .line 1164
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1165
    if-eqz v0, :cond_1

    iget-object v3, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v3, v3, Lcom/google/android/gms/icing/j;->d:I

    if-ne v3, v4, :cond_1

    .line 1167
    const/4 v1, 0x1

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;I)Z

    .line 1168
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 1169
    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "corpuskey:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1172
    iget-object v3, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    const/4 v4, 0x2

    iput v4, v3, Lcom/google/android/gms/icing/j;->d:I

    .line 1176
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "-"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v4, v4, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 1177
    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/i;)V

    .line 1178
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "corpuskey:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 1179
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    move-result v0

    .line 1181
    :goto_0
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/icing/impl/a/aa;I)[Ljava/lang/String;
    .locals 4

    .prologue
    .line 665
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/aa;I)Ljava/util/List;

    move-result-object v2

    .line 666
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    new-array v3, v0, [Ljava/lang/String;

    .line 667
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, v3

    if-ge v1, v0, :cond_0

    .line 668
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    aput-object v0, v3, v1

    .line 667
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 670
    :cond_0
    return-object v3
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 4

    .prologue
    .line 683
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v0

    .line 684
    if-nez v0, :cond_0

    .line 685
    const/4 v0, 0x0

    .line 689
    :goto_0
    return-object v0

    .line 687
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v1, v1, Lcom/google/android/gms/icing/g;->l:I

    .line 688
    iget-object v2, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-wide v2, v2, Lcom/google/android/gms/icing/g;->n:J

    .line 689
    invoke-static {v1, v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/ac;->a(ILjava/lang/Object;J)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    goto :goto_0
.end method

.method public final c()Ljava/util/List;
    .locals 5

    .prologue
    .line 516
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 517
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 518
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 519
    iget-object v4, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v4, v4, Lcom/google/android/gms/icing/j;->d:I

    if-nez v4, :cond_0

    .line 520
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 524
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 523
    :cond_1
    :try_start_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/icing/impl/a/e;)Ljava/util/Map;
    .locals 5

    .prologue
    .line 1807
    new-instance v1, Ljava/util/HashMap;

    invoke-direct {v1}, Ljava/util/HashMap;-><init>()V

    .line 1808
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 1809
    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/gms/icing/impl/a/e;->b()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1810
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v4, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 1811
    if-eqz v0, :cond_0

    iget-object v4, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v4, v4, Lcom/google/android/gms/icing/j;->d:I

    if-nez v4, :cond_0

    .line 1812
    iget-object v4, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v4, v4, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    .line 1813
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/a/j;->g(Lcom/google/android/gms/icing/g;)[Lcom/google/android/gms/appdatasearch/Feature;

    move-result-object v0

    invoke-interface {v1, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1816
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1817
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableMap(Ljava/util/Map;)Ljava/util/Map;

    move-result-object v0

    return-object v0
.end method

.method public final d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;
    .locals 2

    .prologue
    .line 816
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 817
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 818
    if-nez v0, :cond_0

    .line 819
    const/4 v0, 0x0

    monitor-exit v1

    .line 821
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 823
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final d()Ljava/util/List;
    .locals 4

    .prologue
    .line 529
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 530
    :try_start_0
    new-instance v2, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 531
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 532
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 535
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 534
    :cond_0
    :try_start_1
    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final e(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;
    .locals 2

    .prologue
    .line 827
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/impl/a/j;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v1

    .line 828
    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/a/ac;->d()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/icing/impl/a/ac;->a(Ljava/lang/Object;)Lcom/google/android/gms/icing/impl/a/ac;

    move-result-object v0

    goto :goto_0
.end method

.method public final e()Ljava/util/Set;
    .locals 1

    .prologue
    .line 557
    sget-object v0, Lcom/google/android/gms/icing/impl/a/q;->b:Lcom/google/android/gms/icing/impl/a/q;

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final f(Ljava/lang/String;)Lcom/google/android/gms/icing/j;
    .locals 4

    .prologue
    .line 995
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 996
    const/4 v0, 0x1

    const/4 v2, 0x0

    const/4 v3, 0x1

    :try_start_0
    invoke-direct {p0, p1, v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;ZZZ)Lcom/google/android/gms/icing/j;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 997
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final f()Ljava/util/Set;
    .locals 1

    .prologue
    .line 564
    new-instance v0, Lcom/google/android/gms/icing/impl/a/k;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/a/k;-><init>(Lcom/google/android/gms/icing/impl/a/j;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final g()Ljava/util/Set;
    .locals 1

    .prologue
    .line 573
    new-instance v0, Lcom/google/android/gms/icing/impl/a/l;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/a/l;-><init>(Lcom/google/android/gms/icing/impl/a/j;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final g(Ljava/lang/String;)Z
    .locals 3

    .prologue
    .line 2141
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 2142
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 2143
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v2, v2, Lcom/google/android/gms/icing/j;->d:I

    if-nez v2, :cond_0

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 2146
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final h()Ljava/util/Set;
    .locals 2

    .prologue
    .line 623
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 624
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/a/m;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/a/m;-><init>(Lcom/google/android/gms/icing/impl/a/j;)V

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/q;)Ljava/util/Set;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 630
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final i()[J
    .locals 8

    .prologue
    .line 730
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 731
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 732
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->y()I

    move-result v1

    .line 734
    add-int/lit8 v1, v1, 0x1

    new-array v3, v1, [J

    .line 735
    new-instance v4, Ljava/util/zip/CRC32;

    invoke-direct {v4}, Ljava/util/zip/CRC32;-><init>()V

    .line 738
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 739
    invoke-virtual {v4}, Ljava/util/zip/CRC32;->reset()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 741
    :try_start_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v6, "UTF-8"

    invoke-virtual {v1, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v1

    invoke-virtual {v4, v1}, Ljava/util/zip/CRC32;->update([B)V
    :try_end_1
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 745
    :try_start_2
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    .line 746
    invoke-virtual {v4}, Ljava/util/zip/CRC32;->getValue()J

    move-result-wide v6

    aput-wide v6, v3, v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 749
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 743
    :catch_0
    move-exception v0

    :try_start_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CannotHappenException"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 748
    :cond_0
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    return-object v3
.end method

.method public final j()Landroid/util/SparseArray;
    .locals 5

    .prologue
    .line 754
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 755
    :try_start_0
    new-instance v2, Landroid/util/SparseArray;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v2, v0}, Landroid/util/SparseArray;-><init>(I)V

    .line 756
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/a/j;->c()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 757
    iget v4, v0, Lcom/google/android/gms/icing/g;->a:I

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v2, v4, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 760
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 759
    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final k()[I
    .locals 5

    .prologue
    .line 767
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 768
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    .line 769
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->y()I

    move-result v1

    .line 771
    add-int/lit8 v1, v1, 0x1

    new-array v3, v1, [I

    .line 774
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 776
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->f(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 777
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    sget v1, Lcom/google/android/gms/icing/impl/a/j;->a:I

    aput v1, v3, v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 781
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 780
    :cond_1
    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v3
.end method

.method public final l()V
    .locals 6

    .prologue
    .line 1039
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 1040
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 1041
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1042
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    .line 1043
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-direct {p0, v0, v1, v3, v5}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;Lcom/google/android/gms/icing/j;Landroid/content/SharedPreferences$Editor;Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1047
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1045
    :cond_0
    :try_start_1
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1046
    invoke-direct {p0}, Lcom/google/android/gms/icing/impl/a/j;->x()V

    .line 1047
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final m()Ljava/util/Map;
    .locals 5

    .prologue
    .line 1202
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1203
    const/4 v0, 0x1

    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->b(I)Ljava/util/Map;

    move-result-object v2

    .line 1204
    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 1205
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v4

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    invoke-interface {v2, v4, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1208
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1207
    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v2
.end method

.method public final n()Z
    .locals 7

    .prologue
    .line 1241
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v3

    .line 1242
    const/4 v1, 0x0

    .line 1243
    const/4 v0, 0x0

    .line 1244
    const/4 v2, 0x2

    :try_start_0
    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/a/j;->b(I)Ljava/util/Map;

    move-result-object v2

    .line 1246
    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move-object v2, v0

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1247
    const/4 v5, 0x2

    invoke-direct {p0, v0, v5}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 1248
    const/4 v1, 0x1

    .line 1250
    :cond_0
    if-nez v2, :cond_1

    .line 1251
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 1253
    :cond_1
    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "corpuskey:"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-interface {v2, v0}, Landroid/content/SharedPreferences$Editor;->remove(Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1260
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 1255
    :cond_2
    if-eqz v2, :cond_3

    .line 1256
    :try_start_1
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 1258
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    or-int/2addr v0, v1

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1259
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v1
.end method

.method public final o()V
    .locals 7

    .prologue
    .line 1274
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 1275
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    if-eqz v0, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->l:Lcom/google/android/gms/icing/impl/e/f;

    new-instance v4, Ljava/util/ArrayList;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    invoke-direct {v4, v0}, Ljava/util/ArrayList;-><init>(I)V

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :cond_0
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    if-nez v0, :cond_1

    const/4 v1, 0x0

    :goto_1
    if-eqz v1, :cond_0

    iget v6, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v6, :cond_0

    new-instance v6, Lcom/google/android/gms/icing/m;

    invoke-direct {v6}, Lcom/google/android/gms/icing/m;-><init>()V

    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    iput v0, v6, Lcom/google/android/gms/icing/m;->a:I

    iget-object v0, v1, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    iput-object v0, v6, Lcom/google/android/gms/icing/m;->c:[Lcom/google/android/gms/icing/k;

    iget v0, v1, Lcom/google/android/gms/icing/j;->f:I

    iput v0, v6, Lcom/google/android/gms/icing/m;->b:I

    invoke-interface {v4, v6}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 1276
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 1275
    :cond_1
    :try_start_1
    iget-object v1, v0, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    goto :goto_1

    :cond_2
    invoke-virtual {v3, v4}, Lcom/google/android/gms/icing/impl/e/f;->a(Ljava/lang/Iterable;)Z

    move-result v0

    if-eqz v0, :cond_3

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/a/j;->j:Z

    .line 1276
    :cond_3
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final p()Lcom/google/android/gms/icing/n;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1287
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1288
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v2, "flushstatus"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/icing/n;

    invoke-direct {v2}, Lcom/google/android/gms/icing/n;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/n;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1291
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final q()Lcom/google/android/gms/icing/f;
    .locals 4
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 1300
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1301
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v2, "compactstatus"

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v2, Lcom/google/android/gms/icing/f;

    invoke-direct {v2}, Lcom/google/android/gms/icing/f;-><init>()V

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/f;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v0

    .line 1304
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final r()J
    .locals 6

    .prologue
    .line 1377
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v1

    .line 1378
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->g:Landroid/content/SharedPreferences;

    const-string v2, "created"

    const-wide/16 v4, 0x0

    invoke-interface {v0, v2, v4, v5}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v2

    .line 1379
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final s()[I
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 2050
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v3

    .line 2051
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v4

    .line 2053
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v1, v2

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 2054
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 2055
    iget-boolean v0, v0, Lcom/google/android/gms/icing/g;->g:Z

    if-nez v0, :cond_3

    .line 2056
    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    .line 2058
    goto :goto_0

    .line 2059
    :cond_0
    new-array v5, v1, [I

    .line 2061
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v4

    move v1, v2

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/i;

    .line 2062
    iget-object v0, v0, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 2063
    iget-boolean v2, v0, Lcom/google/android/gms/icing/g;->g:Z

    if-nez v2, :cond_2

    .line 2064
    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    aput v0, v5, v1

    .line 2065
    add-int/lit8 v0, v1, 0x1

    :goto_3
    move v1, v0

    .line 2067
    goto :goto_2

    .line 2068
    :cond_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v5

    .line 2069
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_2
    move v0, v1

    goto :goto_3

    :cond_3
    move v0, v1

    goto :goto_1
.end method

.method public final t()Landroid/util/SparseArray;
    .locals 15

    .prologue
    const/4 v7, 0x0

    .line 2074
    iget-object v8, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v8

    .line 2075
    :try_start_0
    new-instance v9, Landroid/util/SparseArray;

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->size()I

    move-result v1

    invoke-direct {v9, v1}, Landroid/util/SparseArray;-><init>(I)V

    .line 2077
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v1

    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v10

    :cond_0
    :goto_0
    invoke-interface {v10}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_5

    invoke-interface {v10}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    move-object v0, v1

    check-cast v0, Ljava/util/Map$Entry;

    move-object v3, v0

    .line 2078
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_0

    .line 2079
    invoke-interface {v3}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v11, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    .line 2083
    iget-object v4, v11, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    .line 2084
    if-eqz v4, :cond_0

    .line 2085
    new-instance v6, Lcom/google/android/gms/icing/x;

    invoke-direct {v6}, Lcom/google/android/gms/icing/x;-><init>()V

    .line 2089
    iget v1, v11, Lcom/google/android/gms/icing/g;->a:I

    iput v1, v6, Lcom/google/android/gms/icing/x;->a:I

    .line 2090
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->c:Ljava/lang/String;

    iput-object v1, v6, Lcom/google/android/gms/icing/x;->b:Ljava/lang/String;

    .line 2093
    const/4 v1, 0x1

    iput-boolean v1, v6, Lcom/google/android/gms/icing/x;->f:Z

    .line 2096
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v1, v1

    new-array v1, v1, [Lcom/google/android/gms/icing/y;

    iput-object v1, v6, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    .line 2098
    invoke-static {v11}, Lcom/google/android/gms/icing/impl/a/j;->c(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v5

    .line 2099
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 2100
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->d:Ljava/lang/String;

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/impl/a/t;

    iget v1, v1, Lcom/google/android/gms/icing/impl/a/t;->a:I

    iput v1, v6, Lcom/google/android/gms/icing/x;->c:I

    .line 2102
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    array-length v1, v1

    if-lez v1, :cond_1

    .line 2103
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->f:[Ljava/lang/String;

    iput-object v1, v6, Lcom/google/android/gms/icing/x;->d:[Ljava/lang/String;

    :cond_1
    move v2, v7

    .line 2106
    :goto_1
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v2, v1, :cond_3

    .line 2107
    new-instance v12, Lcom/google/android/gms/icing/y;

    invoke-direct {v12}, Lcom/google/android/gms/icing/y;-><init>()V

    .line 2109
    iget-object v1, v4, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    aget-object v1, v1, v2

    invoke-interface {v5, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/impl/a/t;

    iget v1, v1, Lcom/google/android/gms/icing/impl/a/t;->a:I

    iput v1, v12, Lcom/google/android/gms/icing/y;->a:I

    .line 2113
    iget-object v1, v11, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    const-string v13, "com.google.android.gm"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, v4, Lcom/google/android/gms/icing/h;->b:[Ljava/lang/String;

    aget-object v1, v1, v2

    const-string v13, "body"

    invoke-virtual {v1, v13}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2115
    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/String;

    const/4 v13, 0x0

    const-string v14, "GmailBodyStripQuoted"

    aput-object v14, v1, v13

    iput-object v1, v12, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    .line 2120
    :cond_2
    iget-object v1, v6, Lcom/google/android/gms/icing/x;->e:[Lcom/google/android/gms/icing/y;

    aput-object v12, v1, v2

    .line 2106
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 2123
    :cond_3
    iget v12, v11, Lcom/google/android/gms/icing/g;->a:I

    new-instance v1, Lcom/google/android/gms/icing/impl/a/y;

    iget v2, v4, Lcom/google/android/gms/icing/h;->a:I

    invoke-interface {v3}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {p0, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, v11, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iget-object v5, v11, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    invoke-virtual {v5}, Ljava/lang/String;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_4

    const/4 v5, 0x0

    :goto_2
    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/icing/impl/a/y;-><init>(ILjava/lang/String;Ljava/lang/String;Landroid/accounts/Account;Lcom/google/android/gms/icing/x;)V

    invoke-virtual {v9, v12, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto/16 :goto_0

    .line 2134
    :catchall_0
    move-exception v1

    monitor-exit v8

    throw v1

    .line 2123
    :cond_4
    :try_start_1
    new-instance v5, Landroid/accounts/Account;

    iget-object v13, v11, Lcom/google/android/gms/icing/g;->p:Ljava/lang/String;

    iget-object v11, v11, Lcom/google/android/gms/icing/g;->o:Ljava/lang/String;

    invoke-direct {v5, v13, v11}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 2133
    :cond_5
    monitor-exit v8
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v9
.end method

.method public final u()[Ljava/lang/String;
    .locals 5

    .prologue
    .line 2237
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/a/j;->o:Ljava/lang/Object;

    monitor-enter v2

    .line 2238
    :try_start_0
    new-instance v3, Ljava/util/HashSet;

    invoke-direct {v3}, Ljava/util/HashSet;-><init>()V

    .line 2240
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/j;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_0
    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 2241
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget v1, v1, Lcom/google/android/gms/icing/j;->d:I

    if-nez v1, :cond_0

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/i;

    iget-object v1, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v1, v1, Lcom/google/android/gms/icing/g;->q:Lcom/google/android/gms/icing/h;

    if-eqz v1, :cond_0

    .line 2243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/j;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v3, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 2248
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 2247
    :cond_1
    :try_start_1
    invoke-interface {v3}, Ljava/util/Set;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Set;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method
