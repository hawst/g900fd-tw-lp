.class public Lcom/google/android/gms/car/ja;
.super Lcom/google/android/gms/car/nk;
.source "SourceFile"


# static fields
.field private static final e:Lcom/google/android/gms/car/ii;


# instance fields
.field private f:Ljava/lang/Throwable;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.stack_in_fallback"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/ja;->e:Lcom/google/android/gms/car/ii;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/ae;)V
    .locals 3

    .prologue
    .line 30
    new-instance v0, Landroid/content/ComponentName;

    invoke-virtual {p1}, Lcom/google/android/gms/car/ae;->c()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/google/android/gms/car/ja;

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/nk;-><init>(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)V

    .line 31
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ja;)V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/gms/car/ja;->e:Lcom/google/android/gms/car/ii;

    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 3

    .prologue
    .line 64
    packed-switch p1, :pswitch_data_0

    .line 78
    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "No state that matches "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 66
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/ja;->s()V

    .line 76
    :goto_0
    :pswitch_1
    return-void

    .line 69
    :pswitch_2
    invoke-virtual {p0}, Lcom/google/android/gms/car/ja;->v()V

    goto :goto_0

    .line 75
    :pswitch_3
    invoke-virtual {p0}, Lcom/google/android/gms/car/ja;->u()V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public final b(Ljava/lang/Throwable;)V
    .locals 1

    .prologue
    .line 83
    iput-object p1, p0, Lcom/google/android/gms/car/ja;->f:Ljava/lang/Throwable;

    .line 84
    sget-object v0, Lcom/google/android/gms/car/ja;->e:Lcom/google/android/gms/car/ii;

    .line 85
    return-void
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 55
    const-class v0, Lcom/google/android/gms/car/ja;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method final s()V
    .locals 2

    .prologue
    .line 35
    invoke-super {p0}, Lcom/google/android/gms/car/nk;->s()V

    .line 38
    iget-object v0, p0, Lcom/google/android/gms/car/ja;->d:Lcom/google/android/gms/car/oa;

    if-eqz v0, :cond_0

    .line 39
    new-instance v0, Lcom/google/android/gms/car/jb;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/jb;-><init>(Lcom/google/android/gms/car/ja;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 51
    :cond_0
    return-void
.end method
