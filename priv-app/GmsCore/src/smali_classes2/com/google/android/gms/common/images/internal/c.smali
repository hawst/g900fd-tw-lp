.class final Lcom/google/android/gms/common/images/internal/c;
.super Landroid/graphics/drawable/Drawable;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/common/images/internal/c;

.field private static final b:Lcom/google/android/gms/common/images/internal/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 338
    new-instance v0, Lcom/google/android/gms/common/images/internal/c;

    invoke-direct {v0}, Lcom/google/android/gms/common/images/internal/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/images/internal/c;->a:Lcom/google/android/gms/common/images/internal/c;

    .line 339
    new-instance v0, Lcom/google/android/gms/common/images/internal/d;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/images/internal/d;-><init>(B)V

    sput-object v0, Lcom/google/android/gms/common/images/internal/c;->b:Lcom/google/android/gms/common/images/internal/d;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 336
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 360
    return-void
.end method

.method static synthetic a()Lcom/google/android/gms/common/images/internal/c;
    .locals 1

    .prologue
    .line 336
    sget-object v0, Lcom/google/android/gms/common/images/internal/c;->a:Lcom/google/android/gms/common/images/internal/c;

    return-object v0
.end method


# virtual methods
.method public final draw(Landroid/graphics/Canvas;)V
    .locals 0

    .prologue
    .line 342
    return-void
.end method

.method public final getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 357
    sget-object v0, Lcom/google/android/gms/common/images/internal/c;->b:Lcom/google/android/gms/common/images/internal/d;

    return-object v0
.end method

.method public final getOpacity()I
    .locals 1

    .prologue
    .line 352
    const/4 v0, -0x2

    return v0
.end method

.method public final setAlpha(I)V
    .locals 0

    .prologue
    .line 345
    return-void
.end method

.method public final setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0

    .prologue
    .line 348
    return-void
.end method
