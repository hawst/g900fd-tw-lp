.class public final Lcom/google/android/gms/udc/c/p;
.super Lcom/google/android/gms/udc/c/l;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/c/o;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/udc/c/o;)V
    .locals 0

    .prologue
    .line 236
    iput-object p1, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    invoke-direct {p0}, Lcom/google/android/gms/udc/c/l;-><init>()V

    return-void
.end method


# virtual methods
.method public final b(Lcom/google/android/gms/common/api/Status;[B)V
    .locals 3

    .prologue
    .line 240
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 241
    new-instance v0, Lcom/google/android/gms/udc/e/g;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/g;-><init>()V

    .line 243
    :try_start_0
    invoke-static {v0, p2}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    .line 244
    iget-object v1, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    new-instance v2, Lcom/google/android/gms/udc/c/v;

    invoke-direct {v2, p1, v0}, Lcom/google/android/gms/udc/c/v;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/udc/e/g;)V

    invoke-virtual {v1, v2}, Lcom/google/android/gms/udc/c/o;->a(Lcom/google/android/gms/common/api/ap;)V
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    :goto_0
    return-void

    .line 247
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    invoke-static {v1}, Lcom/google/android/gms/udc/c/o;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/o;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0

    .line 251
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    iget-object v1, p0, Lcom/google/android/gms/udc/c/p;->a:Lcom/google/android/gms/udc/c/o;

    invoke-static {p1}, Lcom/google/android/gms/udc/c/o;->c(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/udc/f;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/c/o;->a(Lcom/google/android/gms/common/api/ap;)V

    goto :goto_0
.end method
