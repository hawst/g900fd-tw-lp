.class final Lcom/google/android/gms/car/jc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/FirstActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/FirstActivity;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v3, -0x1

    const/4 v6, 0x2

    .line 95
    const-string v0, "CAR.FIRST"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "CAR.FIRST"

    const-string v1, "Connected to CarService"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    iget-object v1, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/gx;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/FirstActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 100
    invoke-virtual {v0}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    .line 101
    const-string v2, "car_service_emulator_command"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 103
    if-eq v2, v3, :cond_1

    .line 104
    iget-object v1, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Landroid/content/Intent;I)V

    .line 137
    :goto_0
    return-void

    .line 105
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v2}, Lcom/google/android/gms/car/FirstActivity;->b(Lcom/google/android/gms/car/FirstActivity;)Lcom/google/android/gms/car/gx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/car/gx;->g()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->c(Lcom/google/android/gms/car/FirstActivity;)V

    goto :goto_0

    .line 108
    :cond_2
    const-string v2, "CAR.FIRST"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 109
    const-string v2, "CAR.FIRST"

    const-string v3, "Not connected to car, starting connection."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v2}, Lcom/google/android/gms/car/FirstActivity;->d(Lcom/google/android/gms/car/FirstActivity;)V

    .line 113
    const-string v2, "android.hardware.usb.action.USB_ACCESSORY_ATTACHED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 114
    const-string v1, "accessory"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/hardware/usb/UsbAccessory;

    .line 115
    if-eqz v0, :cond_5

    .line 116
    const-string v1, "CAR.FIRST"

    invoke-static {v1, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 117
    const-string v1, "CAR.FIRST"

    const-string v2, "attached"

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Landroid/hardware/usb/UsbAccessory;)V

    .line 135
    :cond_5
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->g(Lcom/google/android/gms/car/FirstActivity;)V

    goto :goto_0

    .line 121
    :cond_6
    const-string v2, "android.nfc.action.NDEF_DISCOVERED"

    invoke-virtual {v2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_7

    invoke-virtual {v0}, Landroid/content/Intent;->getComponent()Landroid/content/ComponentName;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ComponentName;->getClassName()Ljava/lang/String;

    move-result-object v0

    const-string v1, "com.google.android.gms.car.FirstActivityWifi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 123
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->e(Lcom/google/android/gms/car/FirstActivity;)V

    goto :goto_1

    .line 125
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->f(Lcom/google/android/gms/car/FirstActivity;)Landroid/hardware/usb/UsbManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/hardware/usb/UsbManager;->getAccessoryList()[Landroid/hardware/usb/UsbAccessory;

    move-result-object v1

    .line 126
    if-eqz v1, :cond_5

    .line 127
    array-length v2, v1

    const/4 v0, 0x0

    :goto_2
    if-ge v0, v2, :cond_5

    aget-object v3, v1, v0

    .line 128
    const-string v4, "CAR.FIRST"

    invoke-static {v4, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v4

    if-eqz v4, :cond_9

    .line 129
    const-string v4, "CAR.FIRST"

    const-string v5, "scanned"

    invoke-static {v4, v5}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    :cond_9
    iget-object v4, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v4, v3}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Landroid/hardware/usb/UsbAccessory;)V

    .line 127
    add-int/lit8 v0, v0, 0x1

    goto :goto_2
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 141
    const-string v0, "CAR.FIRST"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 142
    const-string v0, "CAR.FIRST"

    const-string v1, "onConnectionSuspended"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 144
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Lcom/google/android/gms/car/gx;)Lcom/google/android/gms/car/gx;

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->h(Lcom/google/android/gms/car/FirstActivity;)I

    move-result v0

    const/4 v1, 0x1

    if-eq v0, v1, :cond_1

    .line 146
    iget-object v0, p0, Lcom/google/android/gms/car/jc;->a:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/FirstActivity;->c(Lcom/google/android/gms/car/FirstActivity;)V

    .line 148
    :cond_1
    return-void
.end method
