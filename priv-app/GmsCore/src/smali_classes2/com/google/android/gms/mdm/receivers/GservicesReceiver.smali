.class public Lcom/google/android/gms/mdm/receivers/GservicesReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 21
    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    const-class v1, Lcom/google/android/gms/mdm/receivers/GoogleAccountsAddedReceiver;

    invoke-static {p1, v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/Context;Ljava/lang/Class;)I

    move-result v1

    if-ne v1, v0, :cond_1

    move v1, v0

    .line 27
    :goto_0
    sget-object v0, Lcom/google/android/gms/mdm/b/a;->s:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eq v0, v1, :cond_0

    .line 28
    invoke-static {p1}, Lcom/google/android/gms/mdm/f/g;->a(Landroid/content/Context;)V

    .line 31
    :cond_0
    return-void

    .line 24
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    goto :goto_0
.end method
