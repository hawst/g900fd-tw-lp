.class public abstract Lcom/google/android/gms/games/ui/client/a;
.super Lcom/google/android/gms/games/ui/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/cg;


# instance fields
.field protected i:Ljava/lang/String;

.field protected j:I

.field public k:Lcom/google/android/gms/games/GameEntity;

.field protected l:Z

.field protected m:Z

.field private final n:Z

.field private o:Ljava/util/ArrayList;

.field private p:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(I)V
    .locals 1

    .prologue
    .line 148
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(II)V

    .line 149
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 164
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZ)V

    .line 165
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 182
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(IIZZ)V

    .line 184
    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 202
    const/4 v2, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/q;-><init>(IIIIZ)V

    .line 114
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/a;->l:Z

    .line 121
    iput-boolean v1, p0, Lcom/google/android/gms/games/ui/client/a;->m:Z

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->o:Ljava/util/ArrayList;

    .line 204
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/client/a;->n:Z

    .line 205
    return-void
.end method

.method private a(Lcom/google/android/gms/games/Game;)V
    .locals 6

    .prologue
    .line 522
    invoke-interface {p1}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    .line 523
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/a;->l:Z

    if-eqz v0, :cond_0

    .line 525
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/GameEntity;->s_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->setTitle(Ljava/lang/CharSequence;)V

    .line 528
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/GameEntity;->z()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->O()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const v0, -0x27ffba95

    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    .line 531
    :goto_1
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->o:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    :goto_2
    if-ge v0, v1, :cond_4

    .line 532
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->o:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    .line 531
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 528
    :cond_1
    const/16 v0, 0x10

    invoke-static {v2, v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;I)J

    move-result-wide v0

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v3, 0x6

    if-eq v2, v3, :cond_2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_2

    const-wide/16 v0, 0x456b

    :cond_2
    const-wide/32 v2, -0x28000000

    const-wide/32 v4, 0xffffff

    and-long/2addr v0, v4

    or-long/2addr v0, v2

    long-to-int v0, v0

    goto :goto_0

    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->ae:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    goto :goto_1

    .line 534
    :cond_4
    return-void
.end method


# virtual methods
.method protected final K()V
    .locals 2

    .prologue
    .line 340
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->K()V

    .line 342
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/a;->n:Z

    if-eqz v0, :cond_0

    .line 350
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 351
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 353
    :cond_0
    return-void
.end method

.method protected abstract N()I
.end method

.method protected O()Z
    .locals 1

    .prologue
    .line 562
    const/4 v0, 0x0

    return v0
.end method

.method public final P()I
    .locals 1

    .prologue
    .line 646
    iget v0, p0, Lcom/google/android/gms/games/ui/client/a;->j:I

    return v0
.end method

.method public final Q()Ljava/lang/String;
    .locals 1

    .prologue
    .line 650
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method public final R()V
    .locals 2

    .prologue
    .line 662
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    .line 663
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    .line 667
    :goto_0
    return-void

    .line 665
    :cond_0
    const-string v0, "ClientUiFragAct"

    const-string v1, "trackFragments: should only be called once per activity"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final S()Z
    .locals 3

    .prologue
    .line 680
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_1

    .line 682
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 683
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v4/app/Fragment;

    .line 684
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v4/app/Fragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v2

    if-ne v2, p0, :cond_0

    instance-of v2, v0, Lcom/google/android/gms/games/ui/cr;

    if-eqz v2, :cond_0

    .line 686
    check-cast v0, Lcom/google/android/gms/games/ui/cr;

    .line 687
    invoke-interface {v0}, Lcom/google/android/gms/games/ui/cr;->J_()Z

    move-result v0

    .line 693
    :goto_0
    return v0

    .line 691
    :cond_1
    const-string v0, "ClientUiFragAct"

    const-string v1, "onFragmentSearchRequested: need to call trackFragments first"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 693
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 383
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    if-eqz v0, :cond_0

    .line 384
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    .line 385
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {p0, v1, v2, v0, p1}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 388
    :cond_0
    return-void
.end method

.method public a(Lcom/google/android/gms/common/api/w;)V
    .locals 2

    .prologue
    .line 306
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/api/w;)V

    .line 307
    invoke-static {}, Lcom/google/android/gms/games/h;->a()Lcom/google/android/gms/games/i;

    move-result-object v0

    .line 308
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/i;)V

    .line 309
    sget-object v1, Lcom/google/android/gms/games/d;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i;->a()Lcom/google/android/gms/games/h;

    move-result-object v0

    invoke-virtual {p1, v1, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    .line 310
    return-void
.end method

.method public a(Lcom/google/android/gms/common/c;)V
    .locals 4

    .prologue
    const/16 v3, 0x2711

    .line 414
    const-string v0, "ClientUiFragAct"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Connection failed: result = "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 418
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    .line 419
    const-string v0, "ClientUiFragAct"

    const-string v1, "Developer error."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 420
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 421
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 437
    :goto_0
    return-void

    .line 423
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_1

    .line 424
    const-string v0, "ClientUiFragAct"

    const-string v1, "License check failed."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 425
    const/16 v0, 0x2713

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 426
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0

    .line 428
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_2

    .line 429
    const-string v0, "ClientUiFragAct"

    const-string v1, "Not signed in. To launch Client UI activities, you must be connected to the games service AND signed in."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 431
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 432
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0

    .line 436
    :cond_2
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->a(Lcom/google/android/gms/common/c;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/m;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 487
    invoke-interface {p1}, Lcom/google/android/gms/games/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    .line 488
    invoke-interface {p1}, Lcom/google/android/gms/games/m;->c()Lcom/google/android/gms/games/a;

    move-result-object v2

    .line 493
    :try_start_0
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/a;->b(I)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_0

    .line 516
    invoke-virtual {v2}, Lcom/google/android/gms/games/a;->w_()V

    .line 517
    :goto_0
    return-void

    .line 499
    :cond_0
    const/4 v1, 0x0

    :try_start_1
    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    .line 501
    invoke-virtual {v2}, Lcom/google/android/gms/games/a;->c()I

    move-result v1

    if-lez v1, :cond_1

    .line 502
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/a;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/Game;

    .line 506
    :cond_1
    if-nez v0, :cond_2

    .line 509
    const-string v0, "ClientUiFragAct"

    const-string v1, "onGamesLoaded: couldn\'t load gameId "

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 516
    invoke-virtual {v2}, Lcom/google/android/gms/games/a;->w_()V

    goto :goto_0

    .line 514
    :cond_2
    :try_start_2
    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/Game;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 516
    invoke-virtual {v2}, Lcom/google/android/gms/games/a;->w_()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/games/a;->w_()V

    throw v0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 611
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/games/d;->b(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;

    move-result-object v0

    .line 612
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->k:Lcom/google/android/gms/games/GameEntity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/GameEntity;->a()Ljava/lang/String;

    move-result-object v1

    const/16 v2, 0x17

    invoke-static {p0, p2, v1, v0, v2}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 614
    invoke-super {p0, p1, p2}, Lcom/google/android/gms/games/ui/q;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 615
    return-void
.end method

.method public final b(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 398
    const/4 v1, 0x2

    if-ne p1, v1, :cond_0

    .line 399
    const/16 v1, 0x2711

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 400
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 409
    :goto_0
    return v0

    .line 402
    :cond_0
    const/4 v1, 0x7

    if-ne p1, v1, :cond_1

    .line 405
    const/16 v1, 0x2713

    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 406
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0

    .line 409
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 370
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->b_(Landroid/os/Bundle;)V

    .line 371
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/k;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;

    move-result-object v1

    if-eqz v1, :cond_0

    invoke-direct {p0, v1}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/games/Game;)V

    .line 372
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->N()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->a(I)V

    .line 373
    return-void

    .line 371
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/d;->f:Lcom/google/android/gms/games/k;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/k;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/games/ui/client/b;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/ui/client/b;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 4

    .prologue
    const/16 v3, 0x2711

    const/4 v2, -0x1

    .line 619
    const/16 v0, 0x384

    if-ne p1, v0, :cond_0

    if-ne p2, v3, :cond_0

    .line 621
    const-string v0, "ClientUiFragAct"

    const-string v1, "onActivityResult: Reconnect required."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 622
    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/client/a;->setResult(I)V

    .line 623
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    .line 633
    :goto_0
    return-void

    .line 625
    :cond_0
    const/16 v0, 0x3e8

    if-ne p1, v0, :cond_1

    if-ne p2, v2, :cond_1

    .line 626
    const-string v0, "ClientUiFragAct"

    const-string v1, "onActivityResult: RESULT_OK received from a public items UI. Forwarding..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 628
    invoke-virtual {p0, v2, p3}, Lcom/google/android/gms/games/ui/client/a;->setResult(ILandroid/content/Intent;)V

    .line 629
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->finish()V

    goto :goto_0

    .line 632
    :cond_1
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/q;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onAttachFragment(Landroid/support/v4/app/Fragment;)V
    .locals 2

    .prologue
    .line 698
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onAttachFragment(Landroid/support/v4/app/Fragment;)V

    .line 699
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 701
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->p:Ljava/util/ArrayList;

    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 703
    :cond_0
    return-void
.end method

.method public onAttachedToWindow()V
    .locals 2

    .prologue
    .line 331
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->onAttachedToWindow()V

    .line 335
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    sget v1, Lcom/google/android/gms/h;->ae:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundResource(I)V

    .line 336
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 315
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onCreate(Landroid/os/Bundle;)V

    .line 317
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/client/a;->n:Z

    if-eqz v0, :cond_0

    .line 325
    const-string v0, ""

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->setTitle(Ljava/lang/CharSequence;)V

    .line 327
    :cond_0
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 468
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onCreateOptionsMenu(Landroid/view/Menu;)Z

    .line 470
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.EXTRA_DISABLE_SETTINGS"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 472
    if-ne v0, v3, :cond_0

    .line 473
    sget v0, Lcom/google/android/gms/j;->lX:I

    invoke-interface {p1, v0}, Landroid/view/Menu;->removeItem(I)V

    .line 475
    :cond_0
    return v3
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 569
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 570
    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 577
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->onBackPressed()V

    .line 583
    :goto_0
    return v0

    .line 579
    :cond_0
    sget v2, Lcom/google/android/gms/j;->lX:I

    if-ne v1, v2, :cond_1

    .line 580
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->t()V

    goto :goto_0

    .line 583
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final r()Lcom/google/android/gms/common/api/v;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 213
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 214
    invoke-static {v1}, Lcom/google/android/gms/games/internal/dp;->a(Landroid/content/Intent;)I

    move-result v2

    iput v2, p0, Lcom/google/android/gms/games/ui/client/a;->j:I

    .line 247
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->a(Landroid/app/Activity;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    .line 249
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    if-nez v2, :cond_0

    .line 250
    const-string v1, "ClientUiFragAct"

    const-string v2, "Client UI activities must be started with startActivityForResult"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    :goto_0
    return-object v0

    .line 254
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    const-string v3, "com.google.android.gms"

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 257
    const-string v2, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    .line 260
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    const-string v2, "EXTRA_GAME_PACKAGE_NAME missing when coming from Games UI"

    invoke-static {v1, v2}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 262
    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 263
    const-string v1, "ClientUiFragAct"

    const-string v2, "EXTRA_GAME_PACKAGE_NAME missing when coming from Games UI"

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 266
    :cond_1
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    const/16 v2, 0x80

    invoke-virtual {v1, v0, v2}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    if-eqz v2, :cond_2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationIcon(Landroid/content/pm/ApplicationInfo;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    check-cast v1, Landroid/graphics/drawable/BitmapDrawable;

    invoke-virtual {v1}, Landroid/graphics/drawable/BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v1

    new-instance v2, Landroid/app/ActivityManager$TaskDescription;

    invoke-direct {v2, v0, v1}, Landroid/app/ActivityManager$TaskDescription;-><init>(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/client/a;->setTaskDescription(Landroid/app/ActivityManager$TaskDescription;)V

    .line 269
    :cond_2
    :goto_1
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    iget-object v1, p0, Lcom/google/android/gms/games/ui/client/a;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    .line 272
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/a;->a(Lcom/google/android/gms/common/api/w;)V

    .line 274
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    goto :goto_0

    .line 266
    :catch_0
    move-exception v0

    goto :goto_1
.end method

.method public final t()V
    .locals 2

    .prologue
    .line 600
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    .line 601
    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 602
    invoke-static {v0}, Lcom/google/android/gms/games/d;->d(Lcom/google/android/gms/common/api/v;)Landroid/content/Intent;

    move-result-object v0

    .line 603
    const/16 v1, 0x384

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/ui/client/a;->startActivityForResult(Landroid/content/Intent;I)V

    .line 607
    :goto_0
    return-void

    .line 605
    :cond_0
    const-string v0, "ClientUiFragAct"

    const-string v1, "onShowSettings: googleApiClient not connected; ignoring menu click"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
