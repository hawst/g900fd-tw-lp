.class public final Lcom/google/android/gms/games/ui/client/players/v;
.super Lcom/google/android/gms/games/ui/b/a;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1051
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/a;-><init>()V

    return-void
.end method


# virtual methods
.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1056
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/players/v;->getTargetFragment()Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/client/players/q;

    .line 1058
    packed-switch p2, :pswitch_data_0

    .line 1072
    const-string v0, "SelectPlayersListFrag"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unhandled dialog action "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 1075
    :goto_0
    return-void

    .line 1060
    :pswitch_0
    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v1

    const/16 v2, 0x15

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(I)V

    .line 1062
    iget-object v1, p0, Lcom/google/android/gms/games/ui/b/a;->j:Landroid/widget/CheckBox;

    invoke-virtual {v1}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/client/players/q;->a(Z)V

    goto :goto_0

    .line 1066
    :pswitch_1
    invoke-static {v0}, Lcom/google/android/gms/games/ui/client/players/q;->b(Lcom/google/android/gms/games/ui/client/players/q;)Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;

    move-result-object v1

    const/16 v2, 0x16

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/ui/client/players/SelectPlayersFragment;->b(I)V

    .line 1068
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/client/players/q;->l()V

    goto :goto_0

    .line 1058
    :pswitch_data_0
    .packed-switch -0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
