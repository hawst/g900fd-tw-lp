.class public final Lcom/google/android/gms/games/ui/signin/h;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x7

    return v0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 5

    .prologue
    const/4 v4, 0x4

    const/16 v3, 0x2712

    .line 19
    check-cast p1, Lcom/google/android/gms/games/z;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/h;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/z;->c()Lcom/google/android/gms/games/t;

    move-result-object v1

    if-ne v0, v4, :cond_1

    invoke-virtual {p0, v3, v0}, Lcom/google/android/gms/games/ui/signin/h;->a(II)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    const/16 v2, 0x3eb

    if-ne v0, v2, :cond_2

    const/16 v0, 0x4e20

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/h;->d(I)V

    goto :goto_0

    :cond_2
    const/16 v2, 0x3e9

    if-ne v0, v2, :cond_3

    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/h;->b(I)V

    goto :goto_0

    :cond_3
    const/16 v2, 0x3e8

    if-ne v0, v2, :cond_4

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/h;->d(I)V

    goto :goto_0

    :cond_4
    const/16 v2, 0x5dc

    if-ne v0, v2, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-virtual {p0, v4}, Lcom/google/android/gms/games/ui/signin/h;->b(I)V

    goto :goto_0

    :cond_5
    const-string v0, "LoadSelfFragment"

    const-string v1, "Too many out-of-box exceptions - bailing out of sign-in."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/h;->d(I)V

    goto :goto_0

    :cond_6
    const/4 v2, 0x2

    if-ne v0, v2, :cond_7

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/h;->b(I)V

    goto :goto_0

    :cond_7
    const/4 v2, 0x7

    if-ne v0, v2, :cond_8

    const/16 v1, 0x2713

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/signin/h;->a(II)V

    goto :goto_0

    :cond_8
    const/16 v2, 0x3ea

    if-ne v0, v2, :cond_9

    const-string v0, "LoadSelfFragment"

    const-string v1, "Unable to sign in - application does not have a registered client ID"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v0, 0x2714

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/h;->d(I)V

    goto :goto_0

    :cond_9
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->c()I

    move-result v2

    if-lez v2, :cond_a

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->a()Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    :cond_a
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    if-eqz v0, :cond_c

    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v1

    if-eqz v1, :cond_b

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->b(Ljava/lang/String;)V

    :cond_b
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/h;->b(I)V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    throw v0

    :cond_c
    const-string v0, "LoadSelfFragment"

    const-string v1, "No player found when signing in"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {p0, v3}, Lcom/google/android/gms/games/ui/signin/h;->d(I)V

    goto/16 :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 6

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v1, Lcom/google/android/gms/games/i/t;

    invoke-direct {v1, p1, p0}, Lcom/google/android/gms/games/i/t;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/common/api/aq;)V

    iget-object v4, p1, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/games/i/b;->e:[Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 38
    :goto_0
    return-void

    .line 37
    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 32
    const/16 v0, 0xb

    return v0
.end method
