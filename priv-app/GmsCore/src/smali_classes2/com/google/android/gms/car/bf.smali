.class public final Lcom/google/android/gms/car/bf;
.super Lcom/google/android/gms/car/kk;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field private final a:Lcom/google/android/gms/car/bj;

.field private final b:Lcom/google/android/gms/car/bg;

.field private final c:Lcom/google/android/gms/car/nt;

.field private d:Lcom/google/android/gms/car/km;

.field private e:Ljava/io/OutputStream;

.field private final f:Ljava/util/concurrent/atomic/AtomicBoolean;

.field private final g:Ljava/util/concurrent/atomic/AtomicInteger;

.field private final h:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/bj;Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/nt;Landroid/content/Context;)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 40
    invoke-direct {p0}, Lcom/google/android/gms/car/kk;-><init>()V

    .line 32
    new-instance v0, Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    .line 36
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 41
    iput-object p1, p0, Lcom/google/android/gms/car/bf;->a:Lcom/google/android/gms/car/bj;

    .line 42
    iput-object p2, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    .line 43
    iput-object p3, p0, Lcom/google/android/gms/car/bf;->c:Lcom/google/android/gms/car/nt;

    .line 44
    iput-object p4, p0, Lcom/google/android/gms/car/bf;->h:Landroid/content/Context;

    .line 45
    const-string v0, "CAR.MIC"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 46
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "constructed "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 48
    :cond_0
    return-void
.end method

.method private c()V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 71
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 72
    if-eqz v0, :cond_3

    .line 74
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->e:Ljava/io/OutputStream;

    .line 75
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Ljava/io/OutputStream;->close()V

    .line 78
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/bf;->e:Ljava/io/OutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    :goto_0
    const-string v0, "CAR.MIC"

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 83
    const-string v0, "CAR.MIC"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopped "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 85
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/bf;->c:Lcom/google/android/gms/car/nt;

    invoke-virtual {v2}, Lcom/google/android/gms/car/nt;->c()V

    iget-object v3, v2, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    monitor-enter v3

    :try_start_1
    iget-object v0, v2, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    iget-object v0, v2, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-nez v0, :cond_5

    const/4 v0, 0x1

    :goto_1
    monitor-exit v3
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    if-eqz v0, :cond_4

    iput-boolean v1, v2, Lcom/google/android/gms/car/nt;->f:Z

    invoke-virtual {v2}, Lcom/google/android/gms/car/nt;->b()V

    iget-object v0, v2, Lcom/google/android/gms/car/nt;->c:Lcom/google/android/gms/car/senderprotocol/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/ar;->d()V

    .line 86
    :cond_2
    :goto_2
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->e()V

    .line 88
    :cond_3
    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_4
    const-string v0, "CAR.MIC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    const-string v0, "CAR.MIC"

    const-string v1, "Microphone still being used by another service."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    :catch_0
    move-exception v0

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method private d()V
    .locals 3

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->e()V

    .line 117
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->c()V

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    if-eqz v0, :cond_0

    :try_start_0
    invoke-interface {v0}, Lcom/google/android/gms/car/km;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z
    :try_end_0
    .catch Ljava/util/NoSuchElementException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-static {p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bf;)V

    .line 120
    const-string v0, "CAR.MIC"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 121
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "released "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 123
    :cond_1
    return-void

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method private declared-synchronized e()V
    .locals 1

    .prologue
    .line 165
    monitor-enter p0

    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notifyAll()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 166
    monitor-exit p0

    return-void

    .line 165
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private f(Lcom/google/android/gms/car/km;)V
    .locals 2

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    if-nez v0, :cond_0

    .line 170
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "access after release"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    if-nez p1, :cond_1

    .line 173
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "callback is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 175
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    invoke-interface {v0}, Lcom/google/android/gms/car/km;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/car/km;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_2

    .line 176
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "invalid client token"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 178
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 185
    const-string v0, "CAR.MIC"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 186
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onDisconnected "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 188
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->e()V

    .line 189
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->c()V

    .line 190
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/km;)V
    .locals 5

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 53
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->getAndSet(Z)Z

    move-result v0

    .line 55
    if-eqz v0, :cond_0

    .line 56
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already started"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 58
    :cond_0
    iget-object v3, p0, Lcom/google/android/gms/car/bf;->c:Lcom/google/android/gms/car/nt;

    invoke-virtual {v3}, Lcom/google/android/gms/car/nt;->c()V

    iget-object v4, v3, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    monitor-enter v4

    :try_start_0
    iget-object v0, v3, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0, p0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v0, v3, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-ne v0, v1, :cond_4

    move v0, v1

    :goto_0
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_3

    iput-boolean v1, v3, Lcom/google/android/gms/car/nt;->f:Z

    iput v2, v3, Lcom/google/android/gms/car/nt;->e:I

    iget-object v0, v3, Lcom/google/android/gms/car/nt;->c:Lcom/google/android/gms/car/senderprotocol/ar;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/ar;->b()V

    invoke-virtual {v3}, Lcom/google/android/gms/car/nt;->a()V

    .line 59
    :cond_1
    :goto_1
    const-string v0, "CAR.MIC"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 60
    const-string v0, "CAR.MIC"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "started "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :cond_2
    return-void

    .line 58
    :catchall_0
    move-exception v0

    monitor-exit v4

    throw v0

    :cond_3
    const-string v0, "CAR.MIC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v0, "CAR.MIC"

    const-string v1, "Microphone already open."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    :cond_4
    move v0, v2

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/km;I)V
    .locals 2

    .prologue
    .line 141
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V

    .line 142
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    neg-int v1, p2

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 143
    return-void
.end method

.method public final declared-synchronized a(Ljava/nio/ByteBuffer;)V
    .locals 6

    .prologue
    .line 223
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_0

    .line 249
    :goto_0
    monitor-exit p0

    return-void

    .line 226
    :cond_0
    :try_start_1
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    .line 227
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v1

    sub-int/2addr v1, v0

    .line 228
    iget-object v2, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v2

    .line 229
    add-int/2addr v2, v1

    const/16 v3, 0x4000

    if-le v2, v3, :cond_1

    .line 230
    const-string v0, "CAR.MIC"

    const-string v1, "client q limit exceeded. throw away data"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 247
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.MIC"

    const-string v1, "Error writing audio to OutputStream"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 223
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_1
    :try_start_3
    iget-object v2, p0, Lcom/google/android/gms/car/bf;->e:Ljava/io/OutputStream;

    .line 234
    if-eqz v2, :cond_3

    .line 235
    const-string v3, "CAR.MIC"

    const/4 v4, 0x3

    invoke-static {v3, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 236
    const-string v3, "CAR.MIC"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "write "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " len:"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    :cond_2
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v3

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    add-int/2addr v0, v4

    invoke-virtual {v2, v3, v0, v1}, Ljava/io/OutputStream;->write([BII)V

    .line 244
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->addAndGet(I)I

    .line 245
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->e()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method final b()Lcom/google/android/gms/car/bj;
    .locals 1

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->a:Lcom/google/android/gms/car/bj;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/car/km;)V
    .locals 0

    .prologue
    .line 66
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V

    .line 67
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->c()V

    .line 68
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/km;I)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 147
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V

    .line 148
    monitor-enter p0

    .line 149
    :goto_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-ge v1, p2, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-eqz v1, :cond_1

    .line 151
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->wait()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 155
    :try_start_2
    iget-object v1, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-virtual {v1}, Lcom/google/android/gms/car/bg;->c()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 157
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 153
    :catch_0
    move-exception v1

    :try_start_3
    monitor-exit p0

    .line 161
    :cond_0
    :goto_1
    return v0

    .line 157
    :cond_1
    monitor-exit p0
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 158
    iget-object v1, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I

    move-result v1

    if-lt v1, p2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/car/bf;->f:Ljava/util/concurrent/atomic/AtomicBoolean;

    invoke-virtual {v1}, Ljava/util/concurrent/atomic/AtomicBoolean;->get()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 161
    const/4 v0, 0x1

    goto :goto_1
.end method

.method public final binderDied()V
    .locals 0

    .prologue
    .line 218
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->d()V

    .line 219
    return-void
.end method

.method public final declared-synchronized c(Lcom/google/android/gms/car/km;)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 93
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 94
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 97
    :try_start_1
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 98
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    iput-object v1, p0, Lcom/google/android/gms/car/bf;->e:Ljava/io/OutputStream;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/car/bf;->g:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 100
    const/4 v1, 0x0

    aget-object v0, v0, v1
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 106
    :goto_0
    monitor-exit p0

    return-object v0

    .line 102
    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "CAR.MIC"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    const-string v0, "CAR.MIC"

    const-string v1, "Error creating pipe"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 93
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final d(Lcom/google/android/gms/car/km;)V
    .locals 0

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bf;->f(Lcom/google/android/gms/car/km;)V

    .line 112
    invoke-direct {p0}, Lcom/google/android/gms/car/bf;->d()V

    .line 113
    return-void
.end method

.method public final e(Lcom/google/android/gms/car/km;)V
    .locals 2

    .prologue
    .line 202
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-virtual {v0}, Lcom/google/android/gms/car/bg;->c()V

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    if-eqz v0, :cond_0

    .line 204
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "callback already registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/bf;->h:Landroid/content/Context;

    const-string v1, "android.permission.RECORD_AUDIO"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ie;->a(Landroid/content/Context;Ljava/lang/String;)V

    .line 208
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/car/km;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 213
    iput-object p1, p0, Lcom/google/android/gms/car/bf;->d:Lcom/google/android/gms/car/km;

    .line 214
    :goto_0
    return-void

    .line 210
    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/car/bf;->b:Lcom/google/android/gms/car/bg;

    invoke-static {p0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bf;)V

    goto :goto_0
.end method
