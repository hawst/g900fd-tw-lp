.class final Lcom/google/android/gms/car/senderprotocol/aa;
.super Ljava/lang/Thread;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/senderprotocol/y;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/y;)V
    .locals 1

    .prologue
    .line 211
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    .line 212
    const-string v0, "ReaderThread"

    invoke-direct {p0, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    .line 213
    return-void
.end method

.method private a()V
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->a(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Object;

    move-result-object v1

    monitor-enter v1

    .line 217
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->b(Lcom/google/android/gms/car/senderprotocol/y;)Z

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->c(Lcom/google/android/gms/car/senderprotocol/y;)Ljava/lang/Thread;

    .line 219
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public final run()V
    .locals 11

    .prologue
    const/4 v10, 0x4

    .line 224
    const/16 v0, -0x13

    invoke-static {v0}, Landroid/os/Process;->setThreadPriority(I)V

    .line 226
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->d(Lcom/google/android/gms/car/senderprotocol/y;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->e(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/ab;

    move-result-object v3

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->a:Lcom/google/android/c/a/b;

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    const/4 v2, 0x4

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/ab;->a(Lcom/google/android/c/a/b;Ljava/nio/ByteBuffer;I)I

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    and-int/lit16 v4, v0, 0xff

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->get()B

    move-result v0

    const v1, 0xffff

    iget-object v2, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v2

    and-int/2addr v2, v1

    const/4 v1, 0x1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ab;->a(II)Z

    move-result v5

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ab;->a(II)Z

    move-result v6

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ab;->a(II)Z

    move-result v7

    const/16 v1, 0x8

    invoke-static {v0, v1}, Lcom/google/android/gms/car/senderprotocol/ab;->a(II)Z

    move-result v8

    if-eqz v5, :cond_6

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->c:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v4

    if-eqz v0, :cond_2

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/x;

    const-string v1, "Received a fragment identifying itself as the first fragment, expected continuation"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/x;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Lcom/google/android/gms/car/senderprotocol/x; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    .line 230
    :catch_0
    move-exception v0

    .line 232
    const-string v1, "CAR.GAL"

    invoke-static {v1, v10}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 233
    const-string v1, "CAR.GAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Frag exception "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/x;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 235
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/aa;->a()V

    .line 236
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->f(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/af;->c()V

    .line 252
    :cond_1
    :goto_1
    return-void

    .line 227
    :cond_2
    if-eqz v6, :cond_5

    move v1, v2

    :goto_2
    :try_start_1
    invoke-static {v1}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_3
    if-eqz v8, :cond_9

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->f:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/y;->l(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    move-result-object v1

    if-nez v1, :cond_7

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/x;

    const-string v1, "Received an encrypted frame with no ssl context."

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/x;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_1
    .catch Lcom/google/android/gms/car/senderprotocol/x; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 237
    :catch_1
    move-exception v0

    .line 238
    const-string v1, "CAR.GAL"

    invoke-static {v1, v10}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 239
    const-string v1, "CAR.GAL"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "IO exception: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Ljava/io/IOException;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 243
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->d(Lcom/google/android/gms/car/senderprotocol/y;)Z

    move-result v0

    .line 245
    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/aa;->a()V

    .line 247
    if-eqz v0, :cond_1

    .line 248
    const-string v0, "CAR.GAL"

    const-string v1, "ReaderThread: exiting due to IO error"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 249
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aa;->a:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v0}, Lcom/google/android/gms/car/senderprotocol/y;->f(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/af;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/af;->b()V

    goto :goto_1

    .line 227
    :cond_5
    :try_start_2
    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->a:Lcom/google/android/c/a/b;

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    const/4 v9, 0x4

    invoke-static {v0, v1, v9}, Lcom/google/android/gms/car/senderprotocol/ab;->a(Lcom/google/android/c/a/b;Ljava/nio/ByteBuffer;I)I

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    add-int/lit8 v1, v1, -0x4

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->d:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    move-result v0

    move v1, v0

    goto :goto_2

    :cond_6
    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->c:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v4

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/x;

    const-string v1, "Received a fragment identifying itself as a continuation fragment, but there are no previous fragments."

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/x;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_7
    invoke-static {v2}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    iget-object v8, v3, Lcom/google/android/gms/car/senderprotocol/ab;->a:Lcom/google/android/c/a/b;

    invoke-static {v8, v1, v2}, Lcom/google/android/gms/car/senderprotocol/ab;->a(Lcom/google/android/c/a/b;Ljava/nio/ByteBuffer;I)I

    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->flip()Ljava/nio/Buffer;

    iget-object v2, v3, Lcom/google/android/gms/car/senderprotocol/ab;->f:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v2}, Lcom/google/android/gms/car/senderprotocol/y;->l(Lcom/google/android/gms/car/senderprotocol/y;)Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    move-result-object v2

    invoke-virtual {v2, v1}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    if-eqz v6, :cond_8

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v8

    invoke-virtual {v0, v8}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    :cond_8
    invoke-static {v1}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    invoke-static {v2}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    :goto_3
    if-eqz v6, :cond_d

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    if-eq v1, v2, :cond_a

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/x;

    const-string v1, "Received a fragment identifying itself as the last one in a message, but expected more bytes."

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/x;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_9
    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->a:Lcom/google/android/c/a/b;

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/car/senderprotocol/ab;->a(Lcom/google/android/c/a/b;Ljava/nio/ByteBuffer;I)I

    goto :goto_3

    :cond_a
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->rewind()Ljava/nio/Buffer;

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->e:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-static {v1}, Lcom/google/android/gms/car/senderprotocol/y;->o(Lcom/google/android/gms/car/senderprotocol/y;)Z

    move-result v1

    if-eqz v1, :cond_c

    sget-object v1, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->b:Lcom/google/android/gms/car/senderprotocol/z;

    invoke-interface {v1, v4, v0, v7}, Lcom/google/android/gms/car/senderprotocol/z;->a(ILjava/nio/ByteBuffer;Z)V

    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-object v0, v3, Lcom/google/android/gms/car/senderprotocol/ab;->c:[Ljava/nio/ByteBuffer;

    const/4 v1, 0x0

    aput-object v1, v0, v4

    :cond_b
    :goto_4
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto/16 :goto_0

    :cond_c
    new-instance v0, Ljava/io/IOException;

    const-string v1, "should stop"

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_d
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->position()I

    move-result v1

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->limit()I

    move-result v2

    if-lt v1, v2, :cond_e

    new-instance v0, Lcom/google/android/gms/car/senderprotocol/x;

    const-string v1, "Received all the data for this message, but fragment doesn\'t identify itself as the final fragment."

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/senderprotocol/x;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_e
    if-eqz v5, :cond_b

    iget-object v1, v3, Lcom/google/android/gms/car/senderprotocol/ab;->c:[Ljava/nio/ByteBuffer;

    aput-object v0, v1, v4
    :try_end_2
    .catch Lcom/google/android/gms/car/senderprotocol/x; {:try_start_2 .. :try_end_2} :catch_0
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_4
.end method
