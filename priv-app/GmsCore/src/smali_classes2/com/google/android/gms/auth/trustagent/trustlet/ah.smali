.class final Lcom/google/android/gms/auth/trustagent/trustlet/ah;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/nfc/NfcAdapter$NfcUnlockHandler;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ag;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onUnlockAttempted(Landroid/nfc/Tag;)Z
    .locals 2

    .prologue
    .line 77
    new-instance v0, Ljava/util/concurrent/FutureTask;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/ai;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/ai;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ah;Landroid/nfc/Tag;)V

    invoke-direct {v0, v1}, Ljava/util/concurrent/FutureTask;-><init>(Ljava/util/concurrent/Callable;)V

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/ah;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ag;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/trustlet/ag;->b:Landroid/os/Handler;

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 101
    :try_start_0
    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 107
    :goto_0
    return v0

    .line 105
    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_1
.end method
