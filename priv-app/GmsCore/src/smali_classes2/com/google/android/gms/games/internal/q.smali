.class final Lcom/google/android/gms/games/internal/q;
.super Lcom/google/android/gms/games/internal/d/a;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/internal/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/internal/c;)V
    .locals 2

    .prologue
    .line 163
    iput-object p1, p0, Lcom/google/android/gms/games/internal/q;->a:Lcom/google/android/gms/games/internal/c;

    .line 164
    invoke-virtual {p1}, Lcom/google/android/gms/games/internal/c;->h()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    const/16 v1, 0x3e8

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/internal/d/a;-><init>(Landroid/os/Looper;I)V

    .line 165
    return-void
.end method


# virtual methods
.method protected final a(Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 170
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/q;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/games/internal/q;->a:Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->e(Ljava/lang/String;I)V

    .line 180
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v0, "GamesClientImpl"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unable to increment event "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " by "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " because the games client is no longer connected"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 178
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
