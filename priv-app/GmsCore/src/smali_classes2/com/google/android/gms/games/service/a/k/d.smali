.class public final Lcom/google/android/gms/games/service/a/k/d;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/service/f;

.field private final d:Ljava/lang/String;

.field private final e:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/k/d;->c:Lcom/google/android/gms/games/service/f;

    .line 27
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/k/d;->d:Ljava/lang/String;

    .line 28
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/k/d;->e:Ljava/util/ArrayList;

    .line 29
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 39
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/d;->c:Lcom/google/android/gms/games/service/f;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/f;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 40
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 3

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/d;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k/d;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/k/d;->e:Ljava/util/ArrayList;

    invoke-virtual {p2, p1, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/util/List;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
