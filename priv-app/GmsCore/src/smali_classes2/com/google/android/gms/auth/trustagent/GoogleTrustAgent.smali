.class public Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;
.super Landroid/service/trust/TrustAgentService;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/bo;


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

.field private b:Landroid/content/BroadcastReceiver;

.field private c:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/service/trust/TrustAgentService;-><init>()V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Landroid/content/SharedPreferences;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->c:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)Lcom/google/android/gms/auth/trustagent/trustlet/bk;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    return-object v0
.end method


# virtual methods
.method public final a(Z)V
    .locals 3

    .prologue
    .line 167
    const-string v0, "Coffee - GoogleTrustAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can provide trust state change: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 168
    invoke-virtual {p0, p1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->setManagingTrust(Z)V

    .line 169
    return-void
.end method

.method public final a(ZZLjava/lang/String;)V
    .locals 3

    .prologue
    .line 155
    const-string v0, "Coffee - GoogleTrustAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trust state changed, trusted: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " reason: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    if-eqz p1, :cond_1

    .line 158
    if-nez p3, :cond_0

    const-string p3, ""

    .line 159
    :cond_0
    const-wide/16 v0, 0x0

    invoke-virtual {p0, p3, v0, v1, p2}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->grantTrust(Ljava/lang/CharSequence;JZ)V

    .line 163
    :goto_0
    return-void

    .line 161
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->revokeTrust()V

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 39
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "GoogleTrustAgent created."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 40
    invoke-super {p0}, Landroid/service/trust/TrustAgentService;->onCreate()V

    .line 42
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    .line 43
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V

    .line 44
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/f;

    invoke-direct {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/f;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z

    .line 45
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/am;

    invoke-direct {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/am;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/ay;

    invoke-direct {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ay;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/k;

    invoke-direct {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/k;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z

    .line 48
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/trustlet/av;

    invoke-direct {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/av;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/util/List;)V

    .line 51
    invoke-static {p0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->c:Landroid/content/SharedPreferences;

    .line 52
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "Registering broadcast receiver more than once."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 53
    :goto_0
    return-void

    .line 52
    :cond_0
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_BACKGROUND"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    new-instance v1, Lcom/google/android/gms/auth/trustagent/m;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/m;-><init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;)V

    iput-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 57
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "GoogleTrustAgent destroyed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    invoke-super {p0}, Landroid/service/trust/TrustAgentService;->onDestroy()V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V

    .line 60
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->d:Lcom/google/android/gms/auth/trustagent/trustlet/u;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/u;->b()V

    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v2}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v2, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v2}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    :cond_0
    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->k()V

    goto :goto_0

    :cond_1
    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/lang/String;)V

    iget-object v0, v1, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->k:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b(Z)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    if-nez v0, :cond_2

    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "Attempting to unregister broadcast receiver when none is registered."

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 62
    :goto_1
    return-void

    .line 61
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    iput-object v3, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->b:Landroid/content/BroadcastReceiver;

    goto :goto_1
.end method

.method public onTrustTimeout()V
    .locals 4

    .prologue
    .line 66
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "GoogleTrustAgent received trust timeout notification."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 67
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bk;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    if-eqz v0, :cond_0

    .line 68
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "Trust granted again."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const-string v0, ""

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v2, v3, v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgent;->grantTrust(Ljava/lang/CharSequence;JZ)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    const-string v0, "Coffee - GoogleTrustAgent"

    const-string v1, "Trust not renewed."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
