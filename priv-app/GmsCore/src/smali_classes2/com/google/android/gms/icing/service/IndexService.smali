.class public Lcom/google/android/gms/icing/service/IndexService;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/icing/service/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method protected dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->c()Lcom/google/android/gms/icing/impl/u;

    move-result-object v0

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/icing/impl/u;->dump(Ljava/io/FileDescriptor;Ljava/io/PrintWriter;[Ljava/lang/String;)V

    .line 65
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    .prologue
    .line 51
    const-string v0, "%s: Binding with intent %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 52
    new-instance v0, Lcom/google/android/gms/icing/service/a;

    iget-object v1, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/service/p;->b()Lcom/google/android/gms/icing/b/a;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/service/p;->c()Lcom/google/android/gms/icing/impl/u;

    move-result-object v2

    invoke-direct {v0, v1, v2, p0}, Lcom/google/android/gms/icing/service/a;-><init>(Lcom/google/android/gms/icing/b/a;Lcom/google/android/gms/icing/impl/u;Landroid/content/Context;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/a;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 25
    const-string v0, "%s: IndexService onCreate"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 26
    const-string v0, "main"

    invoke-static {v0, p0}, Lcom/google/android/gms/icing/service/p;->a(Ljava/lang/String;Landroid/app/Service;)Lcom/google/android/gms/icing/service/p;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lcom/google/android/gms/icing/service/p;

    .line 27
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 28
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 32
    const-string v0, "%s: IndexService onDestroy"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/icing/service/IndexService;->a:Lcom/google/android/gms/icing/service/p;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/service/p;->a()V

    .line 34
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 35
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 2

    .prologue
    .line 39
    const-string v0, "%s: IndexService: onStartCommand with %s"

    const-string v1, "main"

    invoke-static {v0, v1, p1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    .line 40
    if-eqz p1, :cond_0

    const-string v0, "com.google.android.gms.icing.INDEX_SERVICE"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    const-class v0, Lcom/google/android/gms/icing/service/IndexWorkerService;

    invoke-virtual {p1, p0, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 44
    invoke-virtual {p0, p1}, Lcom/google/android/gms/icing/service/IndexService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 46
    :cond_0
    const/4 v0, 0x2

    return v0
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 2

    .prologue
    .line 58
    const-string v0, "%s: Unbind"

    const-string v1, "main"

    invoke-static {v0, v1}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    .line 59
    const/4 v0, 0x0

    return v0
.end method
