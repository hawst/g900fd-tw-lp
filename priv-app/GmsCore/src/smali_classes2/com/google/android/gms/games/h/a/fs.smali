.class public final Lcom/google/android/gms/games/h/a/fs;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# instance fields
.field private final c:Ljava/util/HashMap;

.field private final d:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 45
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 47
    sput-object v0, Lcom/google/android/gms/games/h/a/fs;->b:Ljava/util/HashMap;

    const-string v1, "data"

    const-string v2, "data"

    const-class v3, Lcom/google/android/gms/games/h/a/fo;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->a(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    sget-object v0, Lcom/google/android/gms/games/h/a/fs;->b:Ljava/util/HashMap;

    const-string v1, "matchVersion"

    const-string v2, "matchVersion"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 51
    sget-object v0, Lcom/google/android/gms/games/h/a/fs;->b:Ljava/util/HashMap;

    const-string v1, "results"

    const-string v2, "results"

    const-class v3, Lcom/google/android/gms/games/h/a/ch;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->b(Ljava/lang/String;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 53
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->c:Ljava/util/HashMap;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->d:Ljava/util/HashMap;

    .line 60
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/h/a/fo;Ljava/lang/Integer;Ljava/util/ArrayList;)V
    .locals 2

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->c:Ljava/util/HashMap;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->d:Ljava/util/HashMap;

    .line 67
    if-eqz p1, :cond_0

    .line 68
    const-string v0, "data"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/fs;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 70
    :cond_0
    if-eqz p2, :cond_1

    .line 71
    const-string v0, "matchVersion"

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/fs;->a(Ljava/lang/String;I)V

    .line 73
    :cond_1
    if-eqz p3, :cond_2

    .line 74
    const-string v0, "results"

    invoke-virtual {p0, v0, p3}, Lcom/google/android/gms/games/h/a/fs;->a(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 76
    :cond_2
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 57
    sget-object v0, Lcom/google/android/gms/games/h/a/fs;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 84
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/util/ArrayList;)V
    .locals 1

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    return-void
.end method

.method protected final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->d:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "matchVersion"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    return-object v0
.end method

.method protected final d(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->c:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final getData()Lcom/google/android/gms/games/h/a/fo;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->c:Ljava/util/HashMap;

    const-string v1, "data"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fo;

    return-object v0
.end method

.method public final getResults()Ljava/util/ArrayList;
    .locals 2
    .annotation build Lcom/google/android/gms/common/util/RetainForClient;
    .end annotation

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/fs;->d:Ljava/util/HashMap;

    const-string v1, "results"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    return-object v0
.end method
