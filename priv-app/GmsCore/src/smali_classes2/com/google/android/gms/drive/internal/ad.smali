.class final Lcom/google/android/gms/drive/internal/ad;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 390
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 391
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/ad;->a:Lcom/google/android/gms/common/api/m;

    .line 392
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 403
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/ad;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/aa;

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-direct {v1, p1, v2, v3}, Lcom/google/android/gms/drive/internal/aa;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/ak;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 405
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnListEntriesResponse;)V
    .locals 5

    .prologue
    .line 396
    new-instance v0, Lcom/google/android/gms/drive/ak;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->b()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/drive/ak;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 397
    iget-object v1, p0, Lcom/google/android/gms/drive/internal/ad;->a:Lcom/google/android/gms/common/api/m;

    new-instance v2, Lcom/google/android/gms/drive/internal/aa;

    sget-object v3, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnListEntriesResponse;->c()Z

    move-result v4

    invoke-direct {v2, v3, v0, v4}, Lcom/google/android/gms/drive/internal/aa;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/ak;Z)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 399
    return-void
.end method
