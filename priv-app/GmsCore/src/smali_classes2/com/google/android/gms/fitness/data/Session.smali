.class public Lcom/google/android/gms/fitness/data/Session;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:J

.field private final c:J

.field private final d:Ljava/lang/String;

.field private final e:Ljava/lang/String;

.field private final f:Ljava/lang/String;

.field private final g:I

.field private final h:Lcom/google/android/gms/fitness/data/Application;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 580
    new-instance v0, Lcom/google/android/gms/fitness/data/s;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/data/s;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/data/Session;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/fitness/data/Application;)V
    .locals 0

    .prologue
    .line 127
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 128
    iput p1, p0, Lcom/google/android/gms/fitness/data/Session;->a:I

    .line 129
    iput-wide p2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    .line 130
    iput-wide p4, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    .line 131
    iput-object p6, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    .line 132
    iput-object p7, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    .line 133
    iput-object p8, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    .line 134
    iput p9, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    .line 135
    iput-object p10, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    .line 136
    return-void
.end method

.method public constructor <init>(JJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3

    .prologue
    .line 147
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 148
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/fitness/data/Session;->a:I

    .line 149
    iput-wide p1, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    .line 150
    iput-wide p3, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    .line 151
    iput-object p5, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    .line 152
    iput-object p6, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    .line 153
    iput-object p7, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    .line 154
    iput p8, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    .line 155
    new-instance v0, Lcom/google/android/gms/fitness/data/Application;

    const/4 v1, 0x0

    invoke-direct {v0, p9, v1}, Lcom/google/android/gms/fitness/data/Application;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    .line 156
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/fitness/data/r;)V
    .locals 2

    .prologue
    .line 158
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 159
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/fitness/data/Session;->a:I

    .line 160
    iget-wide v0, p1, Lcom/google/android/gms/fitness/data/r;->a:J

    iput-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    .line 161
    iget-wide v0, p1, Lcom/google/android/gms/fitness/data/r;->b:J

    iput-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    .line 162
    iget-object v0, p1, Lcom/google/android/gms/fitness/data/r;->c:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    .line 163
    iget-object v0, p1, Lcom/google/android/gms/fitness/data/r;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    .line 164
    iget-object v0, p1, Lcom/google/android/gms/fitness/data/r;->e:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    .line 165
    iget v0, p1, Lcom/google/android/gms/fitness/data/r;->f:I

    iput v0, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    .line 166
    iget-object v0, p1, Lcom/google/android/gms/fitness/data/r;->g:Lcom/google/android/gms/fitness/data/Application;

    iput-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    .line 167
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/fitness/data/r;B)V
    .locals 0

    .prologue
    .line 69
    invoke-direct {p0, p1}, Lcom/google/android/gms/fitness/data/Session;-><init>(Lcom/google/android/gms/fitness/data/r;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/fitness/data/Session;)J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    return-wide v0
.end method

.method static synthetic c(Lcom/google/android/gms/fitness/data/Session;)J
    .locals 2

    .prologue
    .line 69
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    return-wide v0
.end method

.method static synthetic d(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/fitness/data/Session;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/fitness/data/Session;)I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/fitness/data/Session;)Lcom/google/android/gms/fitness/data/Application;
    .locals 1

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    return-object v0
.end method


# virtual methods
.method public final a(Ljava/util/concurrent/TimeUnit;)J
    .locals 3

    .prologue
    .line 427
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/Application;)Lcom/google/android/gms/fitness/data/Session;
    .locals 11

    .prologue
    .line 418
    new-instance v0, Lcom/google/android/gms/fitness/data/Session;

    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    iget-object v6, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    iget v9, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    move-object v10, p1

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/fitness/data/Session;-><init>(IJJLjava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/fitness/data/Application;)V

    return-object v0
.end method

.method public final a()Z
    .locals 4

    .prologue
    .line 442
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/fitness/data/Session;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 534
    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    :goto_0
    if-nez v2, :cond_0

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/fitness/data/Session;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v0

    :goto_1
    if-eqz v2, :cond_3

    :cond_0
    :goto_2
    return v0

    :cond_1
    move v2, v1

    goto :goto_0

    :cond_2
    move v2, v1

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2
.end method

.method public final b(Ljava/util/concurrent/TimeUnit;)J
    .locals 3

    .prologue
    .line 435
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    sget-object v2, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p1, v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->convert(JLjava/util/concurrent/TimeUnit;)J

    move-result-wide v0

    return-wide v0
.end method

.method public final b()Ljava/lang/String;
    .locals 1

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/String;
    .locals 1

    .prologue
    .line 457
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 465
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 584
    const/4 v0, 0x0

    return v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 472
    iget v0, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    invoke-static {v0}, Lcom/google/android/gms/fitness/e;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 514
    if-eq p1, p0, :cond_0

    instance-of v2, p1, Lcom/google/android/gms/fitness/data/Session;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/google/android/gms/fitness/data/Session;

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/fitness/data/Session;->b:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/fitness/data/Session;->c:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    iget-object v3, p1, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    iget v3, p1, Lcom/google/android/gms/fitness/data/Session;->g:I

    if-ne v2, v3, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public final f()I
    .locals 1

    .prologue
    .line 475
    iget v0, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    return v0
.end method

.method public final g()Lcom/google/android/gms/fitness/data/Application;
    .locals 1

    .prologue
    .line 484
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    return-object v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 493
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    invoke-virtual {v0}, Lcom/google/android/gms/fitness/data/Application;->a()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 552
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public final i()I
    .locals 2

    .prologue
    .line 498
    const/16 v0, 0x1c

    .line 499
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    mul-int/lit8 v0, v0, 0x4

    add-int/lit8 v0, v0, 0x1c

    .line 502
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 503
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 505
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 506
    iget-object v1, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->length()I

    move-result v1

    mul-int/lit8 v1, v1, 0x4

    add-int/2addr v0, v1

    .line 508
    :cond_2
    add-int/lit8 v0, v0, 0x4

    .line 509
    return v0
.end method

.method final j()I
    .locals 1

    .prologue
    .line 568
    iget v0, p0, Lcom/google/android/gms/fitness/data/Session;->a:I

    return v0
.end method

.method public final k()J
    .locals 2

    .prologue
    .line 572
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    return-wide v0
.end method

.method public final l()J
    .locals 2

    .prologue
    .line 577
    iget-wide v0, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 556
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "startTime"

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->b:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "endTime"

    iget-wide v2, p0, Lcom/google/android/gms/fitness/data/Session;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "name"

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->d:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "identifier"

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->e:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "description"

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->f:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "activity"

    iget v2, p0, Lcom/google/android/gms/fitness/data/Session;->g:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "application"

    iget-object v2, p0, Lcom/google/android/gms/fitness/data/Session;->h:Lcom/google/android/gms/fitness/data/Application;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 590
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/fitness/data/s;->a(Lcom/google/android/gms/fitness/data/Session;Landroid/os/Parcel;I)V

    .line 591
    return-void
.end method
