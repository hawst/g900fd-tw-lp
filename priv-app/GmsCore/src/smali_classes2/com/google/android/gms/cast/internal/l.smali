.class public Lcom/google/android/gms/cast/internal/l;
.super Lcom/google/android/gms/cast/internal/b;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:J

.field private static final c:J

.field private static final d:J

.field private static final e:J


# instance fields
.field private f:J

.field private g:Lcom/google/android/gms/cast/k;

.field private final h:Landroid/os/Handler;

.field private final i:Lcom/google/android/gms/cast/internal/p;

.field private final j:Lcom/google/android/gms/cast/internal/p;

.field private final k:Lcom/google/android/gms/cast/internal/p;

.field private final l:Lcom/google/android/gms/cast/internal/p;

.field private final o:Lcom/google/android/gms/cast/internal/p;

.field private final p:Lcom/google/android/gms/cast/internal/p;

.field private final q:Lcom/google/android/gms/cast/internal/p;

.field private final r:Lcom/google/android/gms/cast/internal/p;

.field private final s:Lcom/google/android/gms/cast/internal/p;

.field private final t:Lcom/google/android/gms/cast/internal/p;

.field private final u:Ljava/util/List;

.field private final v:Ljava/lang/Runnable;

.field private w:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x18

    .line 29
    const-string v0, "com.google.cast.media"

    invoke-static {v0}, Lcom/google/android/gms/cast/internal/c;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/cast/internal/l;->a:Ljava/lang/String;

    .line 95
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/internal/l;->b:J

    .line 98
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/internal/l;->c:J

    .line 101
    sget-object v0, Ljava/util/concurrent/TimeUnit;->HOURS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/internal/l;->d:J

    .line 104
    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v0

    sput-wide v0, Lcom/google/android/gms/cast/internal/l;->e:J

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/cast/internal/l;->a:Ljava/lang/String;

    const-string v1, "MediaControlChannel"

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/cast/internal/b;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 139
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->h:Landroid/os/Handler;

    .line 140
    new-instance v0, Lcom/google/android/gms/cast/internal/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/cast/internal/m;-><init>(Lcom/google/android/gms/cast/internal/l;B)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->v:Ljava/lang/Runnable;

    .line 141
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    .line 143
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->c:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 146
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->j:Lcom/google/android/gms/cast/internal/p;

    .line 147
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->j:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 149
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->k:Lcom/google/android/gms/cast/internal/p;

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->k:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 152
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->l:Lcom/google/android/gms/cast/internal/p;

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->l:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 155
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->d:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 158
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->p:Lcom/google/android/gms/cast/internal/p;

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->p:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 161
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->q:Lcom/google/android/gms/cast/internal/p;

    .line 162
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->q:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->r:Lcom/google/android/gms/cast/internal/p;

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->r:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 167
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->s:Lcom/google/android/gms/cast/internal/p;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->s:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 170
    new-instance v0, Lcom/google/android/gms/cast/internal/p;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->b:J

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/cast/internal/p;-><init>(J)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->t:Lcom/google/android/gms/cast/internal/p;

    .line 171
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->t:Lcom/google/android/gms/cast/internal/p;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/l;->j()V

    .line 174
    return-void
.end method

.method private a(JLorg/json/JSONObject;)V
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 665
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/internal/p;->a(J)Z

    move-result v3

    .line 666
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/cast/internal/p;->a(J)Z

    move-result v0

    if-nez v0, :cond_7

    move v0, v1

    .line 667
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/cast/internal/l;->p:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v4

    if-eqz v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/cast/internal/l;->p:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/cast/internal/p;->a(J)Z

    move-result v4

    if-eqz v4, :cond_1

    :cond_0
    iget-object v4, p0, Lcom/google/android/gms/cast/internal/l;->q:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v4}, Lcom/google/android/gms/cast/internal/p;->b()Z

    move-result v4

    if-eqz v4, :cond_8

    iget-object v4, p0, Lcom/google/android/gms/cast/internal/l;->q:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v4, p1, p2}, Lcom/google/android/gms/cast/internal/p;->a(J)Z

    move-result v4

    if-nez v4, :cond_8

    .line 674
    :cond_1
    :goto_1
    if-eqz v0, :cond_b

    .line 675
    const/4 v0, 0x2

    .line 679
    :goto_2
    if-eqz v1, :cond_2

    .line 680
    or-int/lit8 v0, v0, 0x1

    .line 683
    :cond_2
    if-nez v3, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    if-nez v1, :cond_9

    .line 685
    :cond_3
    new-instance v0, Lcom/google/android/gms/cast/k;

    invoke-direct {v0, p3}, Lcom/google/android/gms/cast/k;-><init>(Lorg/json/JSONObject;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    .line 686
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    .line 687
    const/4 v0, 0x7

    .line 695
    :goto_3
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_4

    .line 696
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    .line 697
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->h()V

    .line 699
    :cond_4
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_5

    .line 700
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    .line 701
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->h()V

    .line 703
    :cond_5
    and-int/lit8 v0, v0, 0x4

    if-eqz v0, :cond_6

    .line 704
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->i()V

    .line 708
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/p;

    .line 709
    invoke-virtual {v0, p1, p2, v2}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z

    goto :goto_4

    :cond_7
    move v0, v2

    .line 666
    goto :goto_0

    :cond_8
    move v1, v2

    .line 667
    goto :goto_1

    .line 691
    :cond_9
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    invoke-virtual {v1, p3, v0}, Lcom/google/android/gms/cast/k;->a(Lorg/json/JSONObject;I)I

    move-result v0

    goto :goto_3

    .line 711
    :cond_a
    return-void

    :cond_b
    move v0, v2

    goto :goto_2
.end method

.method static synthetic a(Lcom/google/android/gms/cast/internal/l;Z)V
    .locals 0

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    return-void
.end method

.method private a(Z)V
    .locals 4

    .prologue
    .line 783
    iget-boolean v0, p0, Lcom/google/android/gms/cast/internal/l;->w:Z

    if-eq v0, p1, :cond_0

    .line 784
    iput-boolean p1, p0, Lcom/google/android/gms/cast/internal/l;->w:Z

    .line 785
    if-eqz p1, :cond_1

    .line 786
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->v:Ljava/lang/Runnable;

    sget-wide v2, Lcom/google/android/gms/cast/internal/l;->e:J

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 791
    :cond_0
    :goto_0
    return-void

    .line 788
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->v:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/cast/internal/l;)Z
    .locals 1

    .prologue
    .line 28
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/cast/internal/l;->w:Z

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/cast/internal/l;)Ljava/util/List;
    .locals 1

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    return-object v0
.end method

.method private j()V
    .locals 2

    .prologue
    .line 744
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 745
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    .line 746
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->a()V

    .line 748
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->a()V

    .line 749
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->p:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v0}, Lcom/google/android/gms/cast/internal/p;->a()V

    .line 750
    return-void
.end method


# virtual methods
.method public final a()J
    .locals 12

    .prologue
    const-wide/16 v2, 0x0

    .line 536
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->f()Lcom/google/android/gms/cast/g;

    move-result-object v8

    .line 537
    if-nez v8, :cond_1

    .line 576
    :cond_0
    :goto_0
    return-wide v2

    .line 542
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 547
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget-wide v10, v0, Lcom/google/android/gms/cast/k;->c:D

    .line 548
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget-wide v4, v0, Lcom/google/android/gms/cast/k;->f:J

    .line 549
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget v0, v0, Lcom/google/android/gms/cast/k;->d:I

    .line 551
    const-wide/16 v6, 0x0

    cmpl-double v1, v10, v6

    if-eqz v1, :cond_2

    const/4 v1, 0x2

    if-eq v0, v1, :cond_3

    :cond_2
    move-wide v2, v4

    .line 553
    goto :goto_0

    .line 557
    :cond_3
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v6, p0, Lcom/google/android/gms/cast/internal/l;->f:J

    sub-long/2addr v0, v6

    .line 558
    cmp-long v6, v0, v2

    if-gez v6, :cond_7

    move-wide v6, v2

    .line 562
    :goto_1
    cmp-long v0, v6, v2

    if-nez v0, :cond_4

    move-wide v2, v4

    .line 563
    goto :goto_0

    .line 568
    :cond_4
    iget-wide v0, v8, Lcom/google/android/gms/cast/g;->e:J

    .line 569
    long-to-double v6, v6

    mul-double/2addr v6, v10

    double-to-long v6, v6

    add-long/2addr v4, v6

    .line 571
    cmp-long v6, v0, v2

    if-lez v6, :cond_5

    cmp-long v6, v4, v0

    if-lez v6, :cond_5

    :goto_2
    move-wide v2, v0

    .line 576
    goto :goto_0

    .line 573
    :cond_5
    cmp-long v0, v4, v2

    if-gez v0, :cond_6

    move-wide v0, v2

    .line 574
    goto :goto_2

    :cond_6
    move-wide v0, v4

    goto :goto_2

    :cond_7
    move-wide v6, v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/cast/internal/o;)J
    .locals 6

    .prologue
    .line 443
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 445
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 446
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->r:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 447
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 450
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 451
    const-string v1, "type"

    const-string v4, "GET_STATUS"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 452
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    if-eqz v1, :cond_0

    .line 453
    const-string v1, "mediaSessionId"

    iget-object v4, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget-wide v4, v4, Lcom/google/android/gms/cast/k;->a:J

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 459
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 460
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/o;JLorg/json/JSONObject;)J
    .locals 8

    .prologue
    .line 329
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 331
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 332
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->o:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 333
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 336
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 337
    const-string v1, "type"

    const-string v4, "SEEK"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 338
    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 339
    const-string v1, "currentTime"

    long-to-double v4, p2

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 341
    if-eqz p4, :cond_0

    .line 348
    const-string v1, "customData"

    invoke-virtual {v0, v1, p4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 353
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 354
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/o;Lcom/google/android/gms/cast/g;JLorg/json/JSONObject;)J
    .locals 9

    .prologue
    const/4 v4, 0x1

    .line 191
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 193
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 194
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 195
    invoke-direct {p0, v4}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 198
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 199
    const-string v1, "type"

    const-string v4, "LOAD"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 200
    const-string v1, "media"

    invoke-virtual {p2}, Lcom/google/android/gms/cast/g;->a()Lorg/json/JSONObject;

    move-result-object v4

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 201
    const-string v1, "autoplay"

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Z)Lorg/json/JSONObject;

    .line 202
    const-string v1, "currentTime"

    long-to-double v4, p3

    const-wide v6, 0x408f400000000000L    # 1000.0

    div-double/2addr v4, v6

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;D)Lorg/json/JSONObject;

    .line 203
    if-eqz p5, :cond_0

    .line 212
    const-string v1, "customData"

    invoke-virtual {v0, v1, p5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 217
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 218
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J
    .locals 6

    .prologue
    .line 230
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 232
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 233
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->j:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 234
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 237
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 238
    const-string v1, "type"

    const-string v4, "PAUSE"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 239
    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 241
    if-eqz p2, :cond_0

    .line 242
    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 247
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 249
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final a_(Ljava/lang/String;)V
    .locals 8

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 610
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "message received: %s"

    new-array v2, v7, [Ljava/lang/Object;

    aput-object p1, v2, v6

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->b(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 612
    :try_start_0
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0, p1}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 614
    const-string v1, "type"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 615
    const-string v2, "requestId"

    const-wide/16 v4, -0x1

    invoke-virtual {v0, v2, v4, v5}, Lorg/json/JSONObject;->optLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 618
    const-string v4, "MEDIA_STATUS"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 619
    const-string v1, "status"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v0

    .line 620
    invoke-virtual {v0}, Lorg/json/JSONArray;->length()I

    move-result v1

    if-lez v1, :cond_1

    .line 621
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v0

    invoke-direct {p0, v2, v3, v0}, Lcom/google/android/gms/cast/internal/l;->a(JLorg/json/JSONObject;)V

    .line 651
    :cond_0
    :goto_0
    return-void

    .line 624
    :cond_1
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    .line 625
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->h()V

    .line 626
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->i()V

    .line 627
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->r:Lcom/google/android/gms/cast/internal/p;

    const/4 v1, 0x0

    invoke-virtual {v0, v2, v3, v1}, Lcom/google/android/gms/cast/internal/p;->a(JI)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 648
    :catch_0
    move-exception v0

    .line 649
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v2, "Message is malformed (%s); ignoring: %s"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    invoke-virtual {v0}, Lorg/json/JSONException;->getMessage()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v6

    aput-object p1, v3, v7

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0

    .line 629
    :cond_2
    :try_start_1
    const-string v4, "INVALID_PLAYER_STATE"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 630
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "received unexpected error: Invalid Player State."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 631
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 632
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/p;

    .line 633
    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lcom/google/android/gms/cast/internal/p;->a(JILorg/json/JSONObject;)Z

    goto :goto_1

    .line 635
    :cond_3
    const-string v4, "LOAD_FAILED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 636
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 637
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    const/16 v4, 0x834

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/cast/internal/p;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    .line 638
    :cond_4
    const-string v4, "LOAD_CANCELLED"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_5

    .line 639
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v0

    .line 640
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->i:Lcom/google/android/gms/cast/internal/p;

    const/16 v4, 0x835

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/cast/internal/p;->a(JILorg/json/JSONObject;)Z

    goto :goto_0

    .line 641
    :cond_5
    const-string v4, "INVALID_REQUEST"

    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 642
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v4, "received unexpected error: Invalid Request."

    const/4 v5, 0x0

    new-array v5, v5, [Ljava/lang/Object;

    invoke-virtual {v1, v4, v5}, Lcom/google/android/gms/cast/internal/k;->d(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 643
    const-string v1, "customData"

    invoke-virtual {v0, v1}, Lorg/json/JSONObject;->optJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 644
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->u:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/cast/internal/p;

    .line 645
    const/16 v5, 0x834

    invoke-virtual {v0, v2, v3, v5, v1}, Lcom/google/android/gms/cast/internal/p;->a(JILorg/json/JSONObject;)Z
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 585
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->f()Lcom/google/android/gms/cast/g;

    move-result-object v0

    .line 586
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/google/android/gms/cast/g;->e:J

    :goto_0
    return-wide v0

    :cond_0
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J
    .locals 6

    .prologue
    .line 261
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 263
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 264
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->l:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 265
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 268
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 269
    const-string v1, "type"

    const-string v4, "STOP"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 270
    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 272
    if-eqz p2, :cond_0

    .line 273
    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 278
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 280
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/cast/internal/o;Lorg/json/JSONObject;)J
    .locals 6

    .prologue
    .line 294
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->c()J

    move-result-wide v2

    .line 297
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/l;->k:Lcom/google/android/gms/cast/internal/p;

    invoke-virtual {v1, v2, v3, p1}, Lcom/google/android/gms/cast/internal/p;->a(JLcom/google/android/gms/cast/internal/o;)V

    .line 298
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/gms/cast/internal/l;->a(Z)V

    .line 301
    :try_start_0
    const-string v1, "requestId"

    invoke-virtual {v0, v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 302
    const-string v1, "type"

    const-string v4, "PLAY"

    invoke-virtual {v0, v1, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 303
    const-string v1, "mediaSessionId"

    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/l;->g()J

    move-result-wide v4

    invoke-virtual {v0, v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 305
    if-eqz p2, :cond_0

    .line 306
    const-string v1, "customData"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 311
    :cond_0
    :goto_0
    invoke-virtual {v0}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0, v2, v3}, Lcom/google/android/gms/cast/internal/l;->a(Ljava/lang/String;J)V

    .line 313
    return-wide v2

    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final d()V
    .locals 0

    .prologue
    .line 754
    invoke-direct {p0}, Lcom/google/android/gms/cast/internal/l;->j()V

    .line 755
    return-void
.end method

.method public final e()Lcom/google/android/gms/cast/k;
    .locals 1

    .prologue
    .line 593
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/cast/g;
    .locals 1

    .prologue
    .line 605
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget-object v0, v0, Lcom/google/android/gms/cast/k;->b:Lcom/google/android/gms/cast/g;

    goto :goto_0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    if-nez v0, :cond_0

    .line 720
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "No current media session"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 722
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/l;->g:Lcom/google/android/gms/cast/k;

    iget-wide v0, v0, Lcom/google/android/gms/cast/k;->a:J

    return-wide v0
.end method

.method protected h()V
    .locals 0

    .prologue
    .line 731
    return-void
.end method

.method protected i()V
    .locals 0

    .prologue
    .line 738
    return-void
.end method
