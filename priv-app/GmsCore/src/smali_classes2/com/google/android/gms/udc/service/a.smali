.class final Lcom/google/android/gms/udc/service/a;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/service/UdcApiService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/udc/service/UdcApiService;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 47
    iput-object p1, p0, Lcom/google/android/gms/udc/service/a;->a:Lcom/google/android/gms/udc/service/UdcApiService;

    .line 48
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 49
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/udc/service/a;->a:Lcom/google/android/gms/udc/service/UdcApiService;

    invoke-static {v0, p3}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 58
    const-string v0, "UdcApiService"

    const-string v1, "Verified Package Name."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/udc/service/a;->a:Lcom/google/android/gms/udc/service/UdcApiService;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/service/UdcApiService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/ew;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)V

    .line 62
    const-string v0, "UdcApiService"

    const-string v1, "Verified Package is Google Signed."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 64
    const/4 v0, 0x0

    :try_start_0
    new-instance v1, Lcom/google/android/gms/udc/service/b;

    iget-object v2, p0, Lcom/google/android/gms/udc/service/a;->a:Lcom/google/android/gms/udc/service/UdcApiService;

    invoke-direct {v1, v2, p4}, Lcom/google/android/gms/udc/service/b;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/udc/service/b;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    const/4 v2, 0x0

    invoke-interface {p1, v0, v1, v2}, Lcom/google/android/gms/common/internal/bg;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 69
    :goto_0
    return-void

    .line 67
    :catch_0
    move-exception v0

    const-string v0, "UdcApiService"

    const-string v1, "client died while brokering service"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
