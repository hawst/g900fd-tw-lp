.class public final Lcom/google/android/gms/icing/impl/r;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/icing/impl/q;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/q;)V
    .locals 0

    .prologue
    .line 281
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/r;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Z
    .locals 2

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/r;->a:Lcom/google/android/gms/icing/impl/q;

    iget-object v1, v0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 284
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/r;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/impl/q;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/g;

    .line 285
    if-eqz v0, :cond_0

    iget-boolean v0, v0, Lcom/google/android/gms/icing/c/a/g;->c:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 286
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Ljava/lang/String;)D
    .locals 3

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/r;->a:Lcom/google/android/gms/icing/impl/q;

    iget-object v2, v0, Lcom/google/android/gms/icing/impl/q;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 291
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/r;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/q;->a(Lcom/google/android/gms/icing/impl/q;)Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/c/a/g;

    .line 292
    if-eqz v0, :cond_0

    iget-wide v0, v0, Lcom/google/android/gms/icing/c/a/g;->d:D

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-wide v0

    :cond_0
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    goto :goto_0

    .line 293
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
