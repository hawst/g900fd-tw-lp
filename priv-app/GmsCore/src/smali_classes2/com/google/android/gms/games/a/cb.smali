.class final Lcom/google/android/gms/games/a/cb;
.super Lcom/google/android/gms/games/a/bb;
.source "SourceFile"


# static fields
.field private static final a:Ljava/util/concurrent/locks/ReentrantLock;

.field private static final b:Lcom/google/android/gms/games/provider/a;


# instance fields
.field private final c:Lcom/google/android/gms/games/h/a/fi;

.field private final d:Lcom/google/android/gms/games/h/a/fj;

.field private final e:Lcom/google/android/gms/drive/f/g;

.field private final f:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 111
    new-instance v0, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v0}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/a/cb;->a:Ljava/util/concurrent/locks/ReentrantLock;

    .line 136
    invoke-static {}, Lcom/google/android/gms/games/provider/a;->a()Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->i:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->a:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/k/a;->e:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/c;->a(Lcom/google/android/gms/games/provider/a;)Lcom/google/android/gms/games/provider/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/c;->a()Lcom/google/android/gms/games/provider/a;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/a/cb;->b:Lcom/google/android/gms/games/provider/a;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/bb;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;Lcom/google/android/gms/common/server/n;)V
    .locals 2

    .prologue
    .line 171
    const-string v0, "SnapshotAgent"

    sget-object v1, Lcom/google/android/gms/games/a/cb;->a:Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/games/a/bb;-><init>(Ljava/lang/String;Ljava/util/concurrent/locks/ReentrantLock;Lcom/google/android/gms/games/a/bb;)V

    .line 172
    new-instance v0, Lcom/google/android/gms/games/h/a/fi;

    invoke-direct {v0, p2}, Lcom/google/android/gms/games/h/a/fi;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cb;->c:Lcom/google/android/gms/games/h/a/fi;

    .line 173
    new-instance v0, Lcom/google/android/gms/games/h/a/fj;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/h/a/fj;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cb;->d:Lcom/google/android/gms/games/h/a/fj;

    .line 174
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cb;->f:Ljava/util/HashMap;

    .line 177
    new-instance v0, Lcom/google/android/gms/drive/f/g;

    invoke-direct {v0, p4}, Lcom/google/android/gms/drive/f/g;-><init>(Lcom/google/android/gms/common/server/n;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/cb;->e:Lcom/google/android/gms/drive/f/g;

    .line 178
    return-void
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)I
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 721
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 723
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 724
    if-nez v1, :cond_0

    .line 725
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find Drive ID for snapshot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 726
    const/16 v0, 0xfa0

    .line 743
    :goto_0
    return v0

    .line 730
    :cond_0
    sget-object v2, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v2, p1, v1}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v1

    .line 731
    invoke-interface {v1, p1}, Lcom/google/android/gms/drive/n;->c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 734
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v3, v3}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    .line 738
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x3

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 743
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;Z)Landroid/content/ContentValues;
    .locals 12

    .prologue
    .line 1185
    invoke-interface {p3, p2}, Lcom/google/android/gms/drive/n;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/z;

    .line 1186
    invoke-interface {v0}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1187
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to retrieve metadata: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    const-string v3, "SnapshotAgent"

    invoke-static {v1, v3, v0, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 1189
    const/4 v0, 0x0

    .line 1240
    :goto_0
    return-object v0

    .line 1193
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/drive/z;->b()Lcom/google/android/gms/drive/aj;

    move-result-object v8

    .line 1194
    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->a()Ljava/util/Map;

    move-result-object v3

    .line 1195
    new-instance v0, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v1, "duration"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1197
    new-instance v1, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v2, "progressValue"

    const/4 v4, 0x0

    invoke-direct {v1, v2, v4}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 1199
    new-instance v2, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v4, "coverImageHeight"

    const/4 v5, 0x0

    invoke-direct {v2, v4, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 1201
    new-instance v4, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v5, "coverImageWidth"

    const/4 v6, 0x0

    invoke-direct {v4, v5, v6}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    invoke-interface {v3, v4}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    .line 1203
    if-nez v0, :cond_4

    const-wide/16 v4, -0x1

    move-wide v6, v4

    .line 1205
    :goto_1
    if-nez v1, :cond_5

    const-wide/16 v0, -0x1

    .line 1207
    :goto_2
    invoke-interface {p3}, Lcom/google/android/gms/drive/n;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    .line 1208
    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->m()Z

    move-result v5

    if-nez v5, :cond_6

    const/4 v4, 0x0

    .line 1212
    :goto_3
    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    .line 1213
    const-string v9, "title"

    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->l()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1214
    const-string v9, "unique_name"

    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->r()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1215
    const-string v9, "description"

    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->d()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1216
    const-string v9, "last_modified_timestamp"

    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->i()Ljava/util/Date;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Date;->getTime()J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v10

    invoke-virtual {v5, v9, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1217
    const-string v9, "duration"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v5, v9, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1218
    const-string v6, "progress_value"

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 1219
    if-eqz p4, :cond_1

    .line 1220
    const-string v0, "pending_change_count"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1223
    :cond_1
    if-eqz v4, :cond_2

    .line 1224
    const-string v0, "cover_icon_image_url"

    invoke-virtual {v5, v0, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1225
    if-nez v2, :cond_7

    const/4 v0, 0x0

    .line 1227
    :goto_4
    const-string v1, "cover_icon_image_height"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1228
    if-nez v3, :cond_8

    const/4 v0, 0x0

    .line 1230
    :goto_5
    const-string v1, "cover_icon_image_width"

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v5, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 1234
    :cond_2
    sget-object v0, Lcom/google/android/gms/games/c/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 1235
    invoke-virtual {v8}, Lcom/google/android/gms/drive/aj;->g()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1236
    const-string v0, "visible"

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    :cond_3
    move-object v0, v5

    .line 1240
    goto/16 :goto_0

    .line 1203
    :cond_4
    invoke-static {v0}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v4

    move-wide v6, v4

    goto/16 :goto_1

    .line 1205
    :cond_5
    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v0

    goto/16 :goto_2

    .line 1208
    :cond_6
    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_3

    .line 1225
    :cond_7
    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_4

    .line 1228
    :cond_8
    invoke-static {v3}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    goto :goto_5
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 805
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 806
    new-instance v1, Lcom/google/android/gms/games/a/o;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;ILcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    .line 499
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    .line 501
    invoke-static/range {p1 .. p3}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    .line 502
    if-nez v2, :cond_0

    .line 503
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not find Drive ID for snapshot "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 504
    const/16 v2, 0xfa0

    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 516
    :goto_0
    return-object v2

    .line 508
    :cond_0
    sget-object v2, Lcom/google/android/gms/games/c/a;->I:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v2}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    move-object/from16 v0, p4

    invoke-static {v0, v2}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/snapshot/d;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v11

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iget-object v5, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v5}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/snapshot/d;->c()Landroid/graphics/Bitmap;

    move-result-object v6

    if-nez v6, :cond_1

    const-string v8, ""

    :goto_1
    const/4 v9, 0x4

    move/from16 v6, p5

    move-object/from16 v7, p3

    invoke-static/range {v2 .. v9}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;I)V

    move-object/from16 v0, p3

    move/from16 v1, p5

    invoke-static {p1, v0, v1}, Lcom/google/android/gms/games/service/SnapshotEventService;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/drive/ae;

    invoke-direct {v3}, Lcom/google/android/gms/drive/ae;-><init>()V

    const/4 v4, 0x1

    iput-boolean v4, v3, Lcom/google/android/gms/drive/ae;->a:Z

    invoke-virtual {v3}, Lcom/google/android/gms/drive/ae;->a()Lcom/google/android/gms/drive/ae;

    move-result-object v3

    invoke-virtual {v3, v2}, Lcom/google/android/gms/drive/ae;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ae;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object v2

    invoke-virtual {v11}, Lcom/google/android/gms/drive/an;->a()Lcom/google/android/gms/drive/am;

    move-result-object v3

    move-object/from16 v0, p6

    invoke-interface {v0, p2, v3, v2}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-nez v3, :cond_3

    const-string v3, "SnapshotAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to update snapshot contents: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v3, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/16 v2, 0xfa3

    .line 510
    :goto_2
    if-eqz v2, :cond_4

    .line 511
    invoke-static {v2}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto :goto_0

    .line 508
    :cond_1
    const-string v7, "MD5"

    invoke-static {v7}, Lcom/google/android/gms/common/util/e;->b(Ljava/lang/String;)Ljava/security/MessageDigest;

    move-result-object v7

    if-nez v7, :cond_2

    const-string v8, ""

    goto :goto_1

    :cond_2
    const/16 v8, 0x80

    const/16 v9, 0x80

    const/4 v12, 0x0

    invoke-static {v6, v8, v9, v12}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/gms/common/util/ac;->a(Landroid/graphics/Bitmap;)I

    move-result v8

    invoke-static {v8}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v8

    invoke-virtual {v6, v8}, Landroid/graphics/Bitmap;->copyPixelsToBuffer(Ljava/nio/Buffer;)V

    invoke-virtual {v8}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v6

    invoke-virtual {v7, v6}, Ljava/security/MessageDigest;->digest([B)[B

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/google/android/gms/common/util/e;->a([BZ)Ljava/lang/String;

    move-result-object v8

    goto/16 :goto_1

    :cond_3
    const/4 v2, 0x0

    goto :goto_2

    .line 515
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/games/a/cb;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 516
    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/games/snapshot/d;->e()Landroid/content/ContentValues;

    move-result-object v3

    const-string v4, "last_modified_timestamp"

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v4, "visible"

    const/4 v5, 0x1

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    const-string v4, "pending_change_count"

    const/4 v5, 0x0

    invoke-static {v2, v10, v4, v5}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v4

    const-string v5, "pending_change_count"

    add-int/lit8 v4, v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {v3, v5, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-virtual {v4, v10, v3, v5, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    new-instance v3, Lcom/google/android/gms/games/a/o;

    invoke-direct {v3, v2}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v10}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    goto/16 :goto_0
.end method

.method private static a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1115
    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v3

    add-int/2addr v3, v0

    .line 1116
    sget-object v0, Lcom/google/android/gms/games/c/a;->T:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-gt v3, v0, :cond_1

    move v0, v1

    :goto_0
    const-string v3, "Property %s is too long!"

    new-array v1, v1, [Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v2

    invoke-static {v3, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 1118
    const-string v0, "key"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    const-string v0, "value"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;->a()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/drive/an;->a(Ljava/lang/String;)I

    move-result v0

    invoke-static {p2}, Lcom/google/android/gms/drive/an;->a(Ljava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    const-string v1, "The total size of key string and value string of a custom property"

    const/16 v2, 0x7c

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/drive/an;->a(Ljava/lang/String;II)V

    iget-object v0, p0, Lcom/google/android/gms/drive/an;->b:Lcom/google/android/gms/drive/metadata/internal/a;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v0}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/an;->b:Lcom/google/android/gms/drive/metadata/internal/a;

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/drive/an;->b:Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/internal/a;

    .line 1119
    return-object p0

    :cond_1
    move v0, v2

    .line 1116
    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/games/snapshot/d;Ljava/lang/String;)Lcom/google/android/gms/drive/an;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1081
    new-instance v0, Lcom/google/android/gms/drive/an;

    invoke-direct {v0}, Lcom/google/android/gms/drive/an;-><init>()V

    invoke-virtual {v0, p1}, Lcom/google/android/gms/drive/an;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v0

    .line 1083
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1084
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->a()Ljava/lang/String;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/gms/drive/an;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->d:Lcom/google/android/gms/drive/metadata/f;

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 1086
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->b()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 1087
    new-instance v1, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v2, "duration"

    invoke-direct {v1, v2, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    .line 1089
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->b()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 1092
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->d()Ljava/lang/Long;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 1093
    new-instance v1, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v2, "progressValue"

    invoke-direct {v1, v2, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    .line 1095
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->d()Ljava/lang/Long;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 1098
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/gms/games/snapshot/d;->c()Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1100
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getRowBytes()I

    move-result v2

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    mul-int/2addr v2, v3

    const-string v3, "Thumbnail size"

    const v4, 0xc8000

    invoke-static {v3, v4, v2}, Lcom/google/android/gms/drive/an;->a(Ljava/lang/String;II)V

    iget-object v2, v0, Lcom/google/android/gms/drive/an;->a:Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;

    sget-object v3, Lcom/google/android/gms/drive/metadata/internal/a/a;->z:Lcom/google/android/gms/drive/metadata/f;

    new-instance v4, Lcom/google/android/gms/common/data/BitmapTeleporter;

    invoke-direct {v4, v1}, Lcom/google/android/gms/common/data/BitmapTeleporter;-><init>(Landroid/graphics/Bitmap;)V

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/drive/metadata/internal/MetadataBundle;->b(Lcom/google/android/gms/drive/metadata/f;Ljava/lang/Object;)V

    .line 1101
    new-instance v2, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v3, "coverImageHeight"

    invoke-direct {v2, v3, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    .line 1103
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v3

    invoke-static {v3}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 1104
    new-instance v2, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v3, "coverImageWidth"

    invoke-direct {v2, v3, v5}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    .line 1106
    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 1109
    :cond_3
    return-object v0
.end method

.method private static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;)Lcom/google/android/gms/games/a/cd;
    .locals 6

    .prologue
    .line 318
    const/4 v2, 0x0

    .line 319
    const/4 v1, 0x0

    .line 320
    const/high16 v0, 0x30000000

    invoke-interface {p1, p0, v0}, Lcom/google/android/gms/drive/n;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/i;

    .line 322
    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 323
    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->b()Lcom/google/android/gms/drive/m;

    move-result-object v0

    move v1, v2

    .line 329
    :goto_0
    new-instance v2, Lcom/google/android/gms/games/a/cd;

    invoke-static {v1}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/games/a/cd;-><init>(Ljava/lang/Object;Lcom/google/android/gms/common/api/Status;)V

    return-object v2

    .line 325
    :cond_0
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to open contents. Result: "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/i;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 326
    const/16 v0, 0xfa2

    move-object v5, v1

    move v1, v0

    move-object v0, v5

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;Z)Lcom/google/android/gms/games/a/cd;
    .locals 7

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 1312
    .line 1313
    :goto_0
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/k;

    .line 1314
    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    .line 1315
    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v4

    if-nez v4, :cond_0

    .line 1316
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Failed to list children: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1317
    new-instance v0, Lcom/google/android/gms/games/a/cd;

    invoke-direct {v0, v2, v3}, Lcom/google/android/gms/games/a/cd;-><init>(Ljava/lang/Object;Lcom/google/android/gms/common/api/Status;)V

    .line 1342
    :goto_1
    return-object v0

    .line 1322
    :cond_0
    const-string v4, "SnapshotAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "More may exist: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->c()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1323
    if-eqz p3, :cond_1

    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->c()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1324
    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->b()Lcom/google/android/gms/drive/ak;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/ak;->w_()V

    .line 1325
    invoke-virtual {p2, p1}, Lcom/google/android/gms/drive/query/Query;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move p3, v1

    .line 1326
    goto :goto_0

    .line 1330
    :cond_1
    invoke-interface {v0}, Lcom/google/android/gms/drive/k;->b()Lcom/google/android/gms/drive/ak;

    move-result-object v1

    .line 1332
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->c()I

    move-result v0

    .line 1333
    if-lez v0, :cond_3

    .line 1334
    const/4 v2, 0x1

    if-le v0, v2, :cond_2

    .line 1335
    const-string v0, "SnapshotAgent"

    const-string v2, "Found multiple files with title, taking the oldest one"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 1337
    :cond_2
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/drive/ak;->b(I)Lcom/google/android/gms/drive/aj;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/drive/aj;->e()Lcom/google/android/gms/drive/DriveId;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    .line 1340
    :goto_2
    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->w_()V

    .line 1342
    new-instance v1, Lcom/google/android/gms/games/a/cd;

    invoke-direct {v1, v0, v3}, Lcom/google/android/gms/games/a/cd;-><init>(Ljava/lang/Object;Lcom/google/android/gms/common/api/Status;)V

    move-object v0, v1

    goto :goto_1

    .line 1340
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/drive/ak;->w_()V

    throw v0

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;Lcom/google/android/gms/drive/n;Ljava/lang/String;Landroid/net/Uri;I)Lcom/google/android/gms/games/service/a/m/e;
    .locals 18

    .prologue
    .line 336
    const/4 v4, 0x0

    .line 337
    new-instance v5, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    move-object/from16 v0, p6

    invoke-virtual {v5, v0}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v6

    .line 341
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v5

    if-eqz v5, :cond_8

    .line 342
    sget-object v4, Lcom/google/android/gms/games/a/cb;->b:Lcom/google/android/gms/games/provider/a;

    invoke-virtual {v4, v6}, Lcom/google/android/gms/games/provider/a;->a(Landroid/database/Cursor;)Landroid/content/ContentValues;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    move-object v5, v4

    .line 345
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 349
    if-nez v5, :cond_0

    .line 350
    const-string v4, "SnapshotAgent"

    const-string v5, "Failed to load metadata while resolving conflict"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    new-instance v5, Lcom/google/android/gms/games/service/a/m/e;

    const/4 v4, 0x1

    invoke-direct {v5, v4}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    .line 450
    :goto_1
    return-object v5

    .line 345
    :catchall_0
    move-exception v4

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v4

    .line 356
    :cond_0
    invoke-interface/range {p4 .. p4}, Lcom/google/android/gms/drive/n;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v7

    .line 357
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6, v5}, Landroid/content/ContentValues;-><init>(Landroid/content/ContentValues;)V

    .line 358
    const/4 v4, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p4

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;Z)Landroid/content/ContentValues;

    move-result-object v4

    invoke-virtual {v6, v4}, Landroid/content/ContentValues;->putAll(Landroid/content/ContentValues;)V

    .line 362
    const-string v4, "cover_icon_image_url"

    invoke-virtual {v6, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 363
    if-eqz v4, :cond_1

    .line 364
    new-instance v8, Landroid/content/ContentValues;

    invoke-direct {v8}, Landroid/content/ContentValues;-><init>()V

    .line 365
    const-string v9, "url"

    invoke-virtual {v8, v9, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 366
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    move-object/from16 v0, p1

    iget-object v10, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v10}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v9, v10, v8}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    .line 368
    const-string v9, "cover_icon_image_uri"

    invoke-virtual {v8}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v6, v9, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 370
    invoke-virtual {v8, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)V

    .line 375
    :cond_1
    new-instance v12, Lcom/google/android/gms/common/data/ai;

    sget-object v4, Lcom/google/android/gms/games/a/cb;->b:Lcom/google/android/gms/games/provider/a;

    iget-object v4, v4, Lcom/google/android/gms/games/provider/a;->b:[Ljava/lang/String;

    invoke-direct {v12, v4}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;)V

    .line 376
    invoke-virtual {v12, v5}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    .line 379
    const/high16 v4, 0x10000000

    move-object/from16 v0, p3

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v4}, Lcom/google/android/gms/drive/n;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v4

    .line 381
    const/high16 v8, 0x10000000

    move-object/from16 v0, p4

    move-object/from16 v1, p2

    invoke-interface {v0, v1, v8}, Lcom/google/android/gms/drive/n;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v9

    .line 383
    sget-object v8, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    const/high16 v10, 0x30000000

    move-object/from16 v0, p2

    invoke-interface {v8, v0, v10}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;

    move-result-object v10

    .line 385
    invoke-interface {v4}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/drive/i;

    invoke-interface {v4}, Lcom/google/android/gms/drive/i;->b()Lcom/google/android/gms/drive/m;

    move-result-object v8

    .line 386
    invoke-interface {v9}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/drive/i;

    invoke-interface {v4}, Lcom/google/android/gms/drive/i;->b()Lcom/google/android/gms/drive/m;

    move-result-object v11

    .line 387
    invoke-interface {v10}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v4

    check-cast v4, Lcom/google/android/gms/drive/i;

    invoke-interface {v4}, Lcom/google/android/gms/drive/i;->b()Lcom/google/android/gms/drive/m;

    move-result-object v4

    .line 390
    if-eqz v11, :cond_2

    if-nez v4, :cond_4

    .line 391
    :cond_2
    const-string v4, "SnapshotAgent"

    const-string v5, "Failed to open contents while resolving conflict"

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    if-nez v8, :cond_3

    const/16 v4, 0xfa2

    .line 396
    :goto_2
    new-instance v5, Lcom/google/android/gms/games/service/a/m/e;

    invoke-virtual {v12, v4}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    invoke-direct {v5, v4, v8}, Lcom/google/android/gms/games/service/a/m/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/m;)V

    goto/16 :goto_1

    .line 393
    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 400
    :cond_4
    const/4 v10, 0x0

    .line 401
    const/4 v9, 0x0

    .line 402
    packed-switch p7, :pswitch_data_0

    .line 441
    :goto_3
    if-eqz v10, :cond_7

    .line 442
    move-object/from16 v0, p2

    invoke-interface {v4, v0}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;)V

    move-object/from16 v4, p0

    move-object/from16 v5, p1

    move-object/from16 v6, p2

    move-object/from16 v8, p5

    .line 443
    invoke-virtual/range {v4 .. v10}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/games/service/a/m/e;

    move-result-object v5

    goto/16 :goto_1

    .line 404
    :pswitch_0
    const-string v9, "duration"

    invoke-virtual {v5, v9}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 405
    const-string v9, "duration"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 406
    cmp-long v9, v16, v14

    if-lez v9, :cond_5

    .line 407
    const-string v5, "SnapshotAgent"

    const-string v9, "Resolved conflict using longest (most recent)"

    invoke-static {v5, v9}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    invoke-static {v6}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v11

    goto :goto_3

    .line 411
    :cond_5
    const-string v9, "SnapshotAgent"

    const-string v10, "Resolved conflict using longest (last known good)"

    invoke-static {v9, v10}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 413
    invoke-static {v5}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v8

    .line 415
    goto :goto_3

    .line 417
    :pswitch_1
    const-string v9, "progress_value"

    invoke-virtual {v5, v9}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    .line 418
    const-string v9, "progress_value"

    invoke-virtual {v6, v9}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Long;->longValue()J

    move-result-wide v16

    .line 419
    cmp-long v9, v16, v14

    if-lez v9, :cond_6

    .line 420
    const-string v5, "SnapshotAgent"

    const-string v9, "Resolved conflict using progress (most recent)"

    invoke-static {v5, v9}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 422
    invoke-static {v6}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v11

    goto :goto_3

    .line 424
    :cond_6
    const-string v9, "SnapshotAgent"

    const-string v10, "Resolved conflict using progress (last known good)"

    invoke-static {v9, v10}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 426
    invoke-static {v5}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v8

    .line 428
    goto :goto_3

    .line 430
    :pswitch_2
    const-string v9, "SnapshotAgent"

    const-string v10, "Resolved conflict using LAST_KNOWN_GOOD"

    invoke-static {v9, v10}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    invoke-static {v5}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v8

    .line 433
    goto/16 :goto_3

    .line 435
    :pswitch_3
    const-string v5, "SnapshotAgent"

    const-string v9, "Resolved conflict using MOST_RECENTLY_MODIFIED"

    invoke-static {v5, v9}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 437
    invoke-static {v6}, Lcom/google/android/gms/games/a/cb;->a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;

    move-result-object v9

    move-object v10, v11

    goto/16 :goto_3

    .line 449
    :cond_7
    invoke-virtual {v12, v6}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    .line 450
    new-instance v5, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v6, 0xfa4

    invoke-virtual {v12, v6}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v6

    move-object v9, v11

    move-object v10, v4

    invoke-direct/range {v5 .. v10}, Lcom/google/android/gms/games/service/a/m/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/m;)V

    goto/16 :goto_1

    :cond_8
    move-object v5, v4

    goto/16 :goto_0

    .line 402
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_1
    .end packed-switch
.end method

.method private static a(Landroid/content/ContentValues;)Lcom/google/android/gms/games/snapshot/d;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 455
    new-instance v2, Lcom/google/android/gms/games/snapshot/e;

    invoke-direct {v2}, Lcom/google/android/gms/games/snapshot/e;-><init>()V

    .line 456
    const-string v0, "description"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/games/snapshot/e;->a:Ljava/lang/String;

    .line 457
    const-string v0, "duration"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/games/snapshot/e;->b:Ljava/lang/Long;

    .line 458
    const-string v0, "progress_value"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsLong(Ljava/lang/String;)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/games/snapshot/e;->c:Ljava/lang/Long;

    .line 459
    const-string v0, "cover_icon_image_uri"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 460
    if-nez v0, :cond_0

    move-object v0, v1

    :goto_0
    iput-object v1, v2, Lcom/google/android/gms/games/snapshot/e;->d:Lcom/google/android/gms/common/data/BitmapTeleporter;

    iput-object v0, v2, Lcom/google/android/gms/games/snapshot/e;->e:Landroid/net/Uri;

    .line 461
    invoke-virtual {v2}, Lcom/google/android/gms/games/snapshot/e;->a()Lcom/google/android/gms/games/snapshot/d;

    move-result-object v0

    return-object v0

    .line 460
    :cond_0
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 1249
    if-nez p2, :cond_0

    move-object v0, v8

    .line 1265
    :goto_0
    return-object v0

    .line 1255
    :cond_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/cb;->e:Lcom/google/android/gms/drive/f/g;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    const-string v5, "0"

    const/4 v2, 0x0

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v6

    const/4 v7, 0x0

    move-object v2, p2

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/drive/f/g;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/Boolean;Ljava/lang/Boolean;Ljava/lang/String;Ljava/lang/Boolean;Lcom/google/android/gms/drive/f/k;)Lcom/google/android/gms/drive/internal/model/File;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 1265
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/internal/model/File;->K()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/h/e/g;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1259
    :catch_0
    move-exception v0

    .line 1260
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1261
    const-string v1, "SnapshotAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    :cond_1
    move-object v0, v8

    .line 1263
    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/common/api/v;Landroid/net/Uri;Ljava/util/HashSet;Ljava/util/ArrayList;)V
    .locals 5

    .prologue
    .line 950
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, p0, p2}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Ljava/util/Set;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/l;

    .line 952
    invoke-interface {v0}, Lcom/google/android/gms/drive/l;->b()Ljava/util/Set;

    move-result-object v1

    .line 953
    invoke-interface {v0}, Lcom/google/android/gms/drive/l;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    if-nez v1, :cond_1

    .line 968
    :cond_0
    return-void

    .line 959
    :cond_1
    invoke-virtual {p2, v1}, Ljava/util/HashSet;->removeAll(Ljava/util/Collection;)Z

    .line 960
    invoke-virtual {p2}, Ljava/util/HashSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 961
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Deleting snapshot "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 962
    new-instance v2, Lcom/google/android/gms/common/e/b;

    invoke-direct {v2, p1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 963
    const-string v3, "drive_resource_id_string"

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/common/e/b;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 964
    invoke-static {p1}, Landroid/content/ContentProviderOperation;->newDelete(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v2, v2, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    invoke-virtual {v0, v3, v2}, Landroid/content/ContentProviderOperation$Builder;->withSelection(Ljava/lang/String;[Ljava/lang/String;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v0

    invoke-virtual {p3, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;)V
    .locals 16

    .prologue
    .line 880
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 882
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v4, "drive_resource_id_string"

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/util/HashSet;

    move-result-object v8

    .line 884
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v4, "external_snapshot_id"

    const-string v5, "last_modified_timestamp"

    const/4 v6, 0x0

    sget-object v7, Lcom/google/android/gms/games/a/l;->a:Lcom/google/android/gms/games/a/p;

    invoke-static/range {v2 .. v7}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;Ljava/util/Collection;Lcom/google/android/gms/games/a/p;)Ljava/util/HashMap;

    move-result-object v6

    .line 890
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 891
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    .line 892
    const/4 v2, 0x0

    invoke-virtual/range {p3 .. p3}, Ljava/util/ArrayList;->size()I

    move-result v10

    move v5, v2

    :goto_0
    if-ge v5, v10, :cond_3

    .line 893
    move-object/from16 v0, p3

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/h/a/ff;

    .line 894
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ff;->b()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v8, v4}, Ljava/util/HashSet;->remove(Ljava/lang/Object;)Z

    .line 897
    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ff;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v6, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Long;

    .line 898
    if-eqz v4, :cond_1

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ff;->d()Ljava/lang/Long;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v14

    cmp-long v4, v12, v14

    if-gez v4, :cond_1

    .line 899
    const-string v4, "SnapshotAgent"

    new-instance v11, Ljava/lang/StringBuilder;

    const-string v12, "Ignoring locally modified snapshot "

    invoke-direct {v11, v12}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/gms/games/h/a/ff;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v4, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 892
    :cond_0
    :goto_1
    add-int/lit8 v2, v5, 0x1

    move v5, v2

    goto :goto_0

    .line 904
    :cond_1
    iget-object v11, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v12

    const-string v4, "game_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v12

    const-wide/16 v14, -0x1

    cmp-long v4, v12, v14

    if-eqz v4, :cond_2

    const/4 v4, 0x1

    :goto_2
    const-string v14, "Attempting to store snapshot for unknown player!"

    invoke-static {v4, v14}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    const-string v4, "owner_id"

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v4, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v4}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v4

    invoke-static {v4}, Landroid/content/ContentProviderOperation;->newInsert(Landroid/net/Uri;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4, v11}, Landroid/content/ContentProviderOperation$Builder;->withValues(Landroid/content/ContentValues;)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v11

    invoke-static {v11}, Lcom/google/android/gms/games/a/l;->a(I)Z

    move-result v11

    invoke-virtual {v4, v11}, Landroid/content/ContentProviderOperation$Builder;->withYieldAllowed(Z)Landroid/content/ContentProviderOperation$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/content/ContentProviderOperation$Builder;->build()Landroid/content/ContentProviderOperation;

    move-result-object v4

    invoke-virtual {v9, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 905
    iget-object v2, v2, Lcom/google/android/gms/common/server/response/a;->a:Landroid/content/ContentValues;

    const-string v4, "cover_icon_image_url"

    invoke-virtual {v2, v4}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 906
    if-eqz v2, :cond_0

    .line 907
    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 904
    :cond_2
    const/4 v4, 0x0

    goto :goto_2

    .line 914
    :cond_3
    invoke-virtual {v8}, Ljava/util/HashSet;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 915
    const-string v2, "SnapshotAgent"

    const-string v4, "Trimming deleted snapshots"

    invoke-static {v2, v4}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 916
    if-eqz p2, :cond_6

    .line 919
    invoke-virtual {v8}, Ljava/util/HashSet;->size()I

    move-result v2

    new-array v2, v2, [Ljava/lang/String;

    invoke-virtual {v8, v2}, Ljava/util/HashSet;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Ljava/lang/String;

    .line 921
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 922
    const/4 v4, 0x0

    :goto_3
    array-length v6, v2

    if-ge v4, v6, :cond_5

    .line 923
    aget-object v6, v2, v4

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 924
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v6

    const/16 v8, 0x32

    if-ne v6, v8, :cond_4

    .line 925
    move-object/from16 v0, p2

    invoke-static {v0, v3, v5, v9}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/common/api/v;Landroid/net/Uri;Ljava/util/HashSet;Ljava/util/ArrayList;)V

    .line 927
    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    .line 922
    :cond_4
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 932
    :cond_5
    invoke-virtual {v5}, Ljava/util/HashSet;->size()I

    move-result v2

    if-lez v2, :cond_6

    .line 933
    move-object/from16 v0, p2

    invoke-static {v0, v3, v5, v9}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/common/api/v;Landroid/net/Uri;Ljava/util/HashSet;Ljava/util/ArrayList;)V

    .line 934
    invoke-virtual {v5}, Ljava/util/HashSet;->clear()V

    .line 940
    :cond_6
    invoke-virtual {v9}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_7

    .line 941
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "SnapshotAgent"

    invoke-static {v2, v9, v3}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/ContentResolver;Ljava/util/ArrayList;Ljava/lang/String;)Z

    .line 943
    :cond_7
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/games/a/cb;->f:Ljava/util/HashMap;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v3

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 944
    move-object/from16 v0, p1

    invoke-static {v0, v7}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)V

    .line 945
    return-void
.end method

.method private static a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)V
    .locals 3

    .prologue
    .line 993
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;)Lcom/google/android/gms/common/images/a;

    move-result-object v0

    .line 994
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v2}, Lcom/google/android/gms/games/provider/ad;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/gms/common/images/a;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/util/ArrayList;)V

    .line 996
    return-void
.end method

.method private static b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;
    .locals 7

    .prologue
    const/4 v2, 0x0

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    .line 1005
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v0

    sget-object v1, Lcom/google/android/gms/games/a/cc;->a:[Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v3

    .line 1010
    :try_start_0
    invoke-interface {v3}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1011
    const/4 v0, 0x1

    invoke-interface {v3, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1012
    const/4 v1, 0x0

    invoke-interface {v3, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    move-object v6, v0

    move-object v0, v1

    move-object v1, v6

    .line 1015
    :goto_0
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 1019
    if-eqz v0, :cond_0

    .line 1020
    invoke-static {v0}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    .line 1044
    :goto_1
    return-object v2

    .line 1015
    :catchall_0
    move-exception v0

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v0

    .line 1024
    :cond_0
    if-nez v1, :cond_1

    .line 1025
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "No Drive resource ID for snapshot "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1030
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/j;

    .line 1031
    invoke-interface {v0}, Lcom/google/android/gms/drive/j;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    .line 1032
    if-nez v3, :cond_2

    .line 1033
    const-string v3, "SnapshotAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Failed to resolve drive ID "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-interface {v0}, Lcom/google/android/gms/drive/j;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v0}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 1040
    :cond_2
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 1041
    const-string v1, "drive_resolved_id_string"

    invoke-virtual {v3}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v1, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 1042
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v4, v0, v2, v2}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-object v2, v3

    .line 1044
    goto :goto_1

    :cond_3
    move-object v1, v2

    move-object v0, v2

    goto :goto_0
.end method

.method private static b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 1277
    const-string v0, "drive://%s/%s/%s"

    const/4 v1, 0x3

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p1, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 820
    invoke-static {p1, p2}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 821
    const-string v1, "pending_change_count"

    const/4 v2, -0x1

    invoke-static {p0, v0, v1, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)I

    move-result v1

    .line 827
    if-gez v1, :cond_0

    .line 828
    const-string v0, "SnapshotAgent"

    const-string v1, "Snapshot %s deleted before op completed"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 842
    :goto_0
    return-void

    .line 831
    :cond_0
    if-nez v1, :cond_1

    .line 833
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Attempting to underflow ref count for "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->b(Ljava/lang/Object;)V

    goto :goto_0

    .line 838
    :cond_1
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 839
    const-string v3, "pending_change_count"

    add-int/lit8 v1, v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v3, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 840
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v0, v2, v4, v4}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    goto :goto_0
.end method

.method private static c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/a/cd;
    .locals 4

    .prologue
    .line 204
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, p1}, Lcom/google/android/gms/drive/h;->c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/drive/q;

    move-result-object v2

    .line 205
    if-nez v2, :cond_0

    .line 206
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find the app folder for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    new-instance v0, Lcom/google/android/gms/games/a/cd;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/cd;-><init>()V

    .line 220
    :goto_0
    return-object v0

    .line 212
    :cond_0
    sget-object v0, Lcom/google/android/gms/games/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    sget-object v1, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v1, p1, v0}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/drive/j;

    invoke-interface {v1}, Lcom/google/android/gms/drive/j;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v3

    if-eqz v3, :cond_1

    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v1}, Lcom/google/android/gms/drive/j;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/q;

    move-result-object v0

    move-object v1, v0

    .line 213
    :goto_1
    if-nez v1, :cond_3

    .line 214
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find the games folder for game "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 216
    new-instance v0, Lcom/google/android/gms/games/a/cd;

    invoke-direct {v0}, Lcom/google/android/gms/games/a/cd;-><init>()V

    goto :goto_0

    .line 212
    :cond_1
    new-instance v3, Lcom/google/android/gms/drive/an;

    invoke-direct {v3}, Lcom/google/android/gms/drive/an;-><init>()V

    sget-object v1, Lcom/google/android/gms/games/c/a;->G:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-virtual {v3, v1}, Lcom/google/android/gms/drive/an;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/drive/an;->a()Lcom/google/android/gms/drive/am;

    move-result-object v1

    invoke-interface {v2, p1, v0, v1}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/drive/am;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/s;

    invoke-interface {v0}, Lcom/google/android/gms/drive/s;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Folder could not be created: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/s;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-interface {v0}, Lcom/google/android/gms/drive/s;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v0, 0x0

    move-object v1, v0

    goto :goto_1

    :cond_2
    const-string v1, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Created folder: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/s;->b()Lcom/google/android/gms/drive/q;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/drive/q;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/s;->b()Lcom/google/android/gms/drive/q;

    move-result-object v0

    move-object v1, v0

    goto/16 :goto_1

    .line 220
    :cond_3
    new-instance v0, Lcom/google/android/gms/games/a/cd;

    const/4 v2, 0x0

    invoke-static {v2}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/a/cd;-><init>(Ljava/lang/Object;Lcom/google/android/gms/common/api/Status;)V

    goto/16 :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)I
    .locals 1

    .prologue
    .line 190
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/cb;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/a/cd;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/gms/games/a/cd;->b:Lcom/google/android/gms/common/api/Status;

    .line 191
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    return v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)I
    .locals 11

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 534
    invoke-static {p1, p2}, Lcom/google/android/gms/games/a/cb;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/a/cd;

    move-result-object v1

    .line 535
    iget-object v0, v1, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/drive/q;

    .line 536
    if-nez v6, :cond_0

    .line 537
    const-string v0, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Could not open snapshot folder for game "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 539
    iget-object v0, v1, Lcom/google/android/gms/games/a/cd;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 586
    :goto_0
    return v0

    .line 543
    :cond_0
    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 544
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 546
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "unique_name"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->f(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    .line 548
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "external_snapshot_id"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->f(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 552
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v3}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x5

    if-nez v7, :cond_2

    const-string v5, ""

    :goto_1
    invoke-static/range {v0 .. v5}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 562
    invoke-static {v10}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 563
    :cond_1
    const-string v0, "SnapshotAgent"

    const-string v1, "Snapshot was deleted before a conflict could be notified!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v9

    .line 564
    goto :goto_0

    :cond_2
    move-object v5, v7

    .line 552
    goto :goto_1

    .line 568
    :cond_3
    sget-object v0, Lcom/google/android/gms/games/c/a;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {p3, v0}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/snapshot/d;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v0

    .line 571
    invoke-virtual {v0, v10}, Lcom/google/android/gms/drive/an;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 572
    new-instance v1, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v2, "conflictsWith"

    invoke-direct {v1, v2, v9}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    .line 574
    invoke-static {v0, v1, v10}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/drive/an;Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    .line 578
    invoke-virtual {v0}, Lcom/google/android/gms/drive/an;->a()Lcom/google/android/gms/drive/am;

    move-result-object v0

    invoke-interface {v6, p2, v0, p4}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/r;

    .line 580
    invoke-interface {v0}, Lcom/google/android/gms/drive/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-nez v1, :cond_4

    .line 581
    const-string v1, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to create conflict file: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v0}, Lcom/google/android/gms/drive/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 582
    invoke-interface {v0}, Lcom/google/android/gms/drive/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    :pswitch_0
    const-string v1, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No remapping for status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v8

    goto/16 :goto_0

    :pswitch_1
    const/4 v0, 0x6

    goto/16 :goto_0

    :pswitch_2
    move v0, v8

    goto/16 :goto_0

    :pswitch_3
    const/4 v0, 0x2

    goto/16 :goto_0

    :pswitch_4
    const/16 v0, 0x8

    goto/16 :goto_0

    :cond_4
    move v0, v9

    .line 586
    goto/16 :goto_0

    .line 582
    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_3
        :pswitch_0
        :pswitch_3
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 479
    const/4 v5, 0x2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;ILcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/games/service/a/m/e;
    .locals 9

    .prologue
    .line 630
    invoke-static {p1, p2, p4}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v1

    .line 631
    if-nez v1, :cond_0

    .line 632
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Could not find Drive ID for snapshot "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    new-instance v0, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v1, 0xfa0

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    .line 706
    :goto_0
    return-object v0

    .line 637
    :cond_0
    invoke-static {p3}, Lcom/google/android/gms/drive/DriveId;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/DriveId;

    move-result-object v0

    .line 638
    sget-object v2, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v2, p2, v0}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v8

    .line 639
    invoke-interface {v8, p2}, Lcom/google/android/gms/drive/n;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/drive/z;

    .line 640
    invoke-interface {v0}, Lcom/google/android/gms/drive/z;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-nez v0, :cond_1

    .line 641
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Conflict not found when resolving conflict "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 642
    new-instance v0, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v1, 0xfa6

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto :goto_0

    .line 647
    :cond_1
    sget-object v0, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v0, p2, v1}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v0

    .line 648
    invoke-static {p2, v0}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;)Lcom/google/android/gms/games/a/cd;

    move-result-object v0

    .line 650
    iget-object v1, v0, Lcom/google/android/gms/games/a/cd;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    .line 651
    if-eqz v1, :cond_2

    .line 652
    const-string v0, "SnapshotAgent"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to open content while resolving conflict for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 653
    new-instance v0, Lcom/google/android/gms/games/service/a/m/e;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto :goto_0

    .line 657
    :cond_2
    iget-object v6, v0, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/gms/drive/m;

    .line 658
    invoke-interface {v6}, Lcom/google/android/gms/drive/m;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 659
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 660
    invoke-interface {p6}, Lcom/google/android/gms/drive/m;->c()Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 661
    new-instance v2, Ljava/io/FileInputStream;

    invoke-virtual {v0}, Landroid/os/ParcelFileDescriptor;->getFileDescriptor()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-direct {v2, v0}, Ljava/io/FileInputStream;-><init>(Ljava/io/FileDescriptor;)V

    .line 663
    :try_start_0
    invoke-virtual {v2}, Ljava/io/FileInputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    const-wide/16 v4, 0x0

    invoke-virtual {v0, v4, v5}, Ljava/nio/channels/FileChannel;->position(J)Ljava/nio/channels/FileChannel;

    .line 664
    const/4 v0, 0x0

    const/16 v3, 0x2000

    invoke-static {v2, v1, v0, v3}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;Ljava/io/OutputStream;ZI)J

    move-result-wide v4

    .line 668
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getChannel()Ljava/nio/channels/FileChannel;

    move-result-object v0

    .line 669
    invoke-virtual {v0, v4, v5}, Ljava/nio/channels/FileChannel;->truncate(J)Ljava/nio/channels/FileChannel;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 676
    invoke-interface {p6, p2}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 677
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    .line 681
    const/4 v7, 0x0

    .line 684
    const/4 v5, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p4

    move-object v4, p5

    :try_start_1
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;ILcom/google/android/gms/drive/m;)Lcom/google/android/gms/common/data/DataHolder;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v1

    .line 686
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->f()I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    move-result v2

    .line 688
    if-eqz v1, :cond_3

    .line 689
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 694
    :cond_3
    if-eqz v2, :cond_5

    .line 695
    new-instance v0, Lcom/google/android/gms/games/service/a/m/e;

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto/16 :goto_0

    .line 671
    :catch_0
    move-exception v0

    :try_start_3
    const-string v0, "SnapshotAgent"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Failed to write contents while resolving "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 672
    new-instance v0, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v1, 0xfa2

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 676
    invoke-interface {p6, p2}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 677
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    goto/16 :goto_0

    .line 676
    :catchall_0
    move-exception v0

    invoke-interface {p6, p2}, Lcom/google/android/gms/drive/m;->a(Lcom/google/android/gms/common/api/v;)V

    .line 677
    invoke-static {v2}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/Closeable;)V

    throw v0

    .line 688
    :catchall_1
    move-exception v0

    move-object v1, v7

    :goto_1
    if-eqz v1, :cond_4

    .line 689
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    :cond_4
    throw v0

    .line 699
    :cond_5
    invoke-interface {v8, p2}, Lcom/google/android/gms/drive/n;->c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 702
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v0, p4}, Lcom/google/android/gms/games/provider/ax;->b(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 704
    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    const-string v2, "unique_name"

    invoke-static {v1, v0, v2}, Lcom/google/android/gms/games/a/l;->f(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 706
    const/4 v4, 0x0

    const/4 v5, -0x1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;ZI)Lcom/google/android/gms/games/service/a/m/e;

    move-result-object v0

    goto/16 :goto_0

    .line 688
    :catchall_2
    move-exception v0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;ZI)Lcom/google/android/gms/games/service/a/m/e;
    .locals 15

    .prologue
    .line 239
    invoke-static/range {p1 .. p2}, Lcom/google/android/gms/games/a/cb;->c(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/a/cd;

    move-result-object v3

    .line 240
    iget-object v2, v3, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/drive/q;

    .line 241
    if-nez v2, :cond_0

    .line 242
    const-string v2, "SnapshotAgent"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Could not open snapshot folder for game "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 244
    new-instance v2, Lcom/google/android/gms/games/service/a/m/e;

    iget-object v3, v3, Lcom/google/android/gms/games/a/cd;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v3

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    .line 310
    :goto_0
    return-object v2

    .line 248
    :cond_0
    const/4 v4, 0x0

    .line 249
    sget-object v3, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-interface {v3, v0, v1}, Lcom/google/android/gms/drive/h;->b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/drive/j;

    invoke-interface {v3}, Lcom/google/android/gms/drive/j;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v5

    invoke-virtual {v5}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v5

    if-eqz v5, :cond_1

    sget-object v5, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    invoke-interface {v3}, Lcom/google/android/gms/drive/j;->b()Lcom/google/android/gms/drive/DriveId;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-interface {v5, v0, v3}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v5

    .line 250
    :goto_1
    if-nez v5, :cond_a

    .line 251
    if-nez p4, :cond_2

    .line 253
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to find snapshot file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 254
    new-instance v2, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v3, 0xfa0

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto :goto_0

    .line 249
    :cond_1
    const/4 v5, 0x0

    goto :goto_1

    .line 258
    :cond_2
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 259
    const/4 v4, 0x1

    move-object/from16 v0, p1

    invoke-static {v0, v3, v4}, Lcom/google/android/gms/games/service/SnapshotEventService;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v4

    .line 261
    const-string v3, "SnapshotAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Creating new snapshot "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/drive/an;

    invoke-direct {v3}, Lcom/google/android/gms/drive/an;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/drive/an;->c(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v5

    sget-object v3, Lcom/google/android/gms/games/c/a;->H:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v5, v3}, Lcom/google/android/gms/drive/an;->b(Ljava/lang/String;)Lcom/google/android/gms/drive/an;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/an;->a()Lcom/google/android/gms/drive/am;

    move-result-object v3

    new-instance v5, Lcom/google/android/gms/drive/ae;

    invoke-direct {v5}, Lcom/google/android/gms/drive/ae;-><init>()V

    const/4 v6, 0x1

    iput-boolean v6, v5, Lcom/google/android/gms/drive/ae;->a:Z

    invoke-virtual {v5}, Lcom/google/android/gms/drive/ae;->a()Lcom/google/android/gms/drive/ae;

    move-result-object v5

    invoke-virtual {v5, v4}, Lcom/google/android/gms/drive/ae;->a(Ljava/lang/String;)Lcom/google/android/gms/drive/ae;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/drive/ae;->b()Lcom/google/android/gms/drive/ad;

    move-result-object v4

    move-object/from16 v0, p2

    move-object/from16 v1, p3

    invoke-interface {v2, v0, v1, v3, v4}, Lcom/google/android/gms/drive/q;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Lcom/google/android/gms/drive/am;Lcom/google/android/gms/drive/ad;)Lcom/google/android/gms/common/api/am;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/common/api/am;->a()Lcom/google/android/gms/common/api/ap;

    move-result-object v3

    check-cast v3, Lcom/google/android/gms/drive/r;

    invoke-interface {v3}, Lcom/google/android/gms/drive/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v4

    if-nez v4, :cond_3

    const-string v4, "SnapshotAgent"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Failed to create snapshot file: "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v3}, Lcom/google/android/gms/drive/r;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v4, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    const/4 v3, 0x0

    .line 262
    :goto_2
    const/4 v4, 0x1

    move-object v5, v3

    move v3, v4

    .line 266
    :goto_3
    if-nez v5, :cond_4

    .line 267
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Failed to create snapshot file "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v2, Lcom/google/android/gms/games/service/a/m/e;

    const/16 v3, 0xfa1

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto/16 :goto_0

    .line 261
    :cond_3
    invoke-interface {v3}, Lcom/google/android/gms/drive/r;->b()Lcom/google/android/gms/drive/n;

    move-result-object v3

    goto :goto_2

    .line 273
    :cond_4
    move-object/from16 v0, p1

    move-object/from16 v1, p2

    invoke-direct {p0, v0, v1, v5, v3}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;Z)Landroid/content/ContentValues;

    move-result-object v3

    .line 275
    if-nez v3, :cond_6

    .line 276
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Could not open snapshot "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    if-eqz p4, :cond_5

    const/16 v2, 0xfa1

    .line 279
    :goto_4
    new-instance v3, Lcom/google/android/gms/games/service/a/m/e;

    invoke-direct {v3, v2}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    move-object v2, v3

    goto/16 :goto_0

    .line 277
    :cond_5
    const/16 v2, 0xfa0

    goto :goto_4

    .line 282
    :cond_6
    const-string v4, "SnapshotAgent"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "SNAPSHOT FOUND: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v5}, Lcom/google/android/gms/drive/n;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 285
    invoke-interface {v5}, Lcom/google/android/gms/drive/n;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->j()J

    move-result-wide v6

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/gms/games/a/au;->i()J

    move-result-wide v8

    const-string v10, "unique_name"

    invoke-virtual {v3, v10}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const-string v11, "game_id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {v3, v11, v6}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "owner_id"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    const-string v6, "external_snapshot_id"

    invoke-virtual {v3, v6, v10}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "drive_resolved_id_string"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->d()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v6, "drive_resource_id_string"

    invoke-virtual {v4}, Lcom/google/android/gms/drive/DriveId;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v6, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, p1

    iget-object v4, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-static {v6}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;

    move-result-object v6

    invoke-virtual {v4, v6, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v8

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    const-string v6, "cover_icon_image_url"

    invoke-virtual {v3, v6}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    move-object/from16 v0, p1

    invoke-static {v0, v4}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Ljava/util/ArrayList;)V

    .line 286
    if-nez v8, :cond_7

    .line 287
    new-instance v2, Lcom/google/android/gms/games/service/a/m/e;

    const/4 v3, 0x1

    invoke-direct {v2, v3}, Lcom/google/android/gms/games/service/a/m/e;-><init>(I)V

    goto/16 :goto_0

    .line 291
    :cond_7
    move-object/from16 v0, p1

    move-object/from16 v1, p3

    invoke-static {v0, v1}, Lcom/google/android/gms/games/a/cb;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 292
    new-instance v3, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;

    const-string v4, "conflictsWith"

    const/4 v6, 0x0

    invoke-direct {v3, v4, v6}, Lcom/google/android/gms/drive/metadata/CustomPropertyKey;-><init>(Ljava/lang/String;I)V

    new-instance v4, Lcom/google/android/gms/drive/query/f;

    invoke-direct {v4}, Lcom/google/android/gms/drive/query/f;-><init>()V

    sget-object v6, Lcom/google/android/gms/drive/query/i;->d:Lcom/google/android/gms/drive/metadata/h;

    invoke-interface {v2}, Lcom/google/android/gms/drive/q;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v2

    invoke-static {v6, v2}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/h;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v2

    invoke-virtual {v4, v2}, Lcom/google/android/gms/drive/query/f;->a(Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/f;

    move-result-object v2

    sget-object v4, Lcom/google/android/gms/drive/query/i;->b:Lcom/google/android/gms/drive/metadata/i;

    sget-object v6, Lcom/google/android/gms/games/c/a;->J:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v6}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/gms/drive/query/c;->a(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)Lcom/google/android/gms/drive/query/Filter;

    move-result-object v4

    invoke-virtual {v2, v4}, Lcom/google/android/gms/drive/query/f;->a(Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/f;

    move-result-object v2

    new-instance v4, Lcom/google/android/gms/drive/query/internal/HasFilter;

    sget-object v6, Lcom/google/android/gms/drive/query/i;->j:Lcom/google/android/gms/drive/metadata/i;

    new-instance v9, Lcom/google/android/gms/drive/metadata/internal/a;

    invoke-direct {v9}, Lcom/google/android/gms/drive/metadata/internal/a;-><init>()V

    move-object/from16 v0, p3

    invoke-virtual {v9, v3, v0}, Lcom/google/android/gms/drive/metadata/internal/a;->a(Lcom/google/android/gms/drive/metadata/CustomPropertyKey;Ljava/lang/String;)Lcom/google/android/gms/drive/metadata/internal/a;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/metadata/internal/a;->a()Lcom/google/android/gms/drive/metadata/internal/AppVisibleCustomProperties;

    move-result-object v3

    invoke-direct {v4, v6, v3}, Lcom/google/android/gms/drive/query/internal/HasFilter;-><init>(Lcom/google/android/gms/drive/metadata/i;Ljava/lang/Object;)V

    invoke-virtual {v2, v4}, Lcom/google/android/gms/drive/query/f;->a(Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/f;

    move-result-object v2

    new-instance v3, Lcom/google/android/gms/drive/query/k;

    invoke-direct {v3}, Lcom/google/android/gms/drive/query/k;-><init>()V

    sget-object v4, Lcom/google/android/gms/drive/metadata/internal/a/l;->a:Lcom/google/android/gms/drive/metadata/internal/a/m;

    invoke-virtual {v3, v4}, Lcom/google/android/gms/drive/query/k;->a(Lcom/google/android/gms/drive/metadata/k;)Lcom/google/android/gms/drive/query/k;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/drive/query/k;->a()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v3

    iput-object v3, v2, Lcom/google/android/gms/drive/query/f;->a:Lcom/google/android/gms/drive/query/SortOrder;

    invoke-virtual {v2}, Lcom/google/android/gms/drive/query/f;->a()Lcom/google/android/gms/drive/query/Query;

    move-result-object v2

    const/4 v3, 0x1

    move-object/from16 v0, p2

    invoke-direct {p0, v0, v2, v3}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/query/Query;Z)Lcom/google/android/gms/games/a/cd;

    move-result-object v2

    iget-object v3, v2, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    if-eqz v3, :cond_8

    sget-object v3, Lcom/google/android/gms/drive/b;->h:Lcom/google/android/gms/drive/h;

    iget-object v2, v2, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/drive/DriveId;

    move-object/from16 v0, p2

    invoke-interface {v3, v0, v2}, Lcom/google/android/gms/drive/h;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/DriveId;)Lcom/google/android/gms/drive/n;

    move-result-object v6

    .line 293
    :goto_5
    if-eqz v6, :cond_9

    .line 294
    const-string v2, "SnapshotAgent"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Found conflict for snapshot "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    move-object v2, p0

    move-object/from16 v3, p1

    move-object/from16 v4, p2

    move/from16 v9, p5

    .line 295
    invoke-direct/range {v2 .. v9}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;Lcom/google/android/gms/drive/n;Ljava/lang/String;Landroid/net/Uri;I)Lcom/google/android/gms/games/service/a/m/e;

    move-result-object v2

    goto/16 :goto_0

    .line 292
    :cond_8
    const/4 v6, 0x0

    goto :goto_5

    .line 300
    :cond_9
    move-object/from16 v0, p1

    iget-object v9, v0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->d()Ljava/lang/String;

    move-result-object v10

    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v2}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x1

    move-object v14, v7

    invoke-static/range {v9 .. v14}, Lcom/google/android/gms/games/f/a;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 307
    move-object/from16 v0, p2

    invoke-static {v0, v5}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/drive/n;)Lcom/google/android/gms/games/a/cd;

    move-result-object v2

    .line 309
    iget-object v3, v2, Lcom/google/android/gms/games/a/cd;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v4

    .line 310
    new-instance v3, Lcom/google/android/gms/games/service/a/m/e;

    new-instance v5, Lcom/google/android/gms/games/a/o;

    move-object/from16 v0, p1

    invoke-direct {v5, v0}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    invoke-virtual {v5, v8}, Lcom/google/android/gms/games/a/o;->a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;

    move-result-object v5

    iput v4, v5, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v5}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v4

    iget-object v2, v2, Lcom/google/android/gms/games/a/cd;->a:Ljava/lang/Object;

    check-cast v2, Lcom/google/android/gms/drive/m;

    invoke-direct {v3, v4, v2}, Lcom/google/android/gms/games/service/a/m/e;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/m;)V

    move-object v2, v3

    goto/16 :goto_0

    :cond_a
    move v3, v4

    goto/16 :goto_3
.end method

.method public final b(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 13

    .prologue
    const/4 v1, 0x1

    const/4 v7, 0x0

    const/4 v8, 0x0

    .line 758
    .line 759
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/android/gms/games/provider/ax;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 761
    new-instance v9, Lcom/google/android/gms/common/e/b;

    invoke-direct {v9, v0}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    .line 762
    const-string v0, "visible"

    const-string v2, "0"

    const-string v3, ">?"

    invoke-virtual {v9, v0, v2, v3}, Lcom/google/android/gms/common/e/b;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 763
    iget-boolean v0, p1, Lcom/google/android/gms/games/a/au;->g:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/games/a/cb;->f:Ljava/util/HashMap;

    invoke-virtual {p1}, Lcom/google/android/gms/games/a/au;->d()Lcom/google/android/gms/games/b/m;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    if-eqz v0, :cond_0

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/common/util/p;->a()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/32 v4, 0x36ee80

    cmp-long v0, v2, v4

    if-gtz v0, :cond_0

    move v0, v1

    :goto_0
    if-eqz v0, :cond_1

    .line 764
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v9, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    const-string v1, "last_modified_timestamp DESC"

    iput-object v1, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v8, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 788
    :goto_1
    return-object v0

    :cond_0
    move v0, v8

    .line 763
    goto :goto_0

    .line 774
    :cond_1
    :try_start_0
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v10

    move-object v0, v7

    :goto_2
    iget-boolean v1, p1, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v1, :cond_6

    iget-object v2, p0, Lcom/google/android/gms/games/a/cb;->d:Lcom/google/android/gms/games/h/a/fj;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    iget-object v4, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v5, "players/%1$s/applications/%2$s/snapshots"

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v11, v12

    const/4 v3, 0x1

    invoke-static {v4}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v11, v3

    invoke-static {v5, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v10, :cond_2

    const-string v4, "language"

    invoke-static {v10}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_2
    if-eqz v0, :cond_3

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fj;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_3
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fj;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fh;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fh;

    :goto_3
    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fh;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/games/h/a/fh;->getItems()Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {v6, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    :cond_4
    if-nez v1, :cond_a

    move-object v0, v6

    move v1, v8

    .line 783
    :goto_4
    if-eqz v0, :cond_5

    .line 784
    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/a/cb;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;)V

    .line 788
    :cond_5
    new-instance v0, Lcom/google/android/gms/games/a/o;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/a/o;-><init>(Lcom/google/android/gms/games/a/au;)V

    iput-object v9, v0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    const-string v2, "last_modified_timestamp DESC"

    iput-object v2, v0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    iput v1, v0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/o;->a()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_1

    .line 774
    :cond_6
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/cb;->c:Lcom/google/android/gms/games/h/a/fi;

    iget-object v1, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p1, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    const-string v4, "players/%1$s/snapshots"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-static {v3}, Lcom/google/android/gms/games/h/a/fi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v5, v11

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz v10, :cond_7

    const-string v4, "language"

    invoke-static {v10}, Lcom/google/android/gms/games/h/a/fi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/gms/games/h/a/fi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_7
    if-eqz v0, :cond_8

    const-string v4, "pageToken"

    invoke-static {v0}, Lcom/google/android/gms/games/h/a/fi;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v3, v4, v0}, Lcom/google/android/gms/games/h/a/fi;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_8
    iget-object v0, v2, Lcom/google/android/gms/games/h/a/fi;->a:Lcom/google/android/gms/common/server/n;

    const/4 v2, 0x0

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/fh;

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/fh;
    :try_end_1
    .catch Lcom/android/volley/ac; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_3

    .line 775
    :catch_0
    move-exception v0

    .line 776
    invoke-static {}, Lcom/google/android/gms/games/internal/dq;->a()Z

    move-result v1

    if-eqz v1, :cond_9

    .line 777
    const-string v1, "SnapshotAgent"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/server/b/c;->a(Lcom/android/volley/ac;Ljava/lang/String;)Lcom/google/android/gms/common/server/b/b;

    .line 779
    :cond_9
    const/16 v0, 0x1f4

    move v1, v0

    move-object v0, v7

    goto :goto_4

    :cond_a
    move-object v0, v1

    goto/16 :goto_2
.end method
