.class public abstract Lcom/google/android/gms/auth/trustagent/trustlet/bh;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

.field f:Landroid/content/Context;

.field g:Z

.field h:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    return-void
.end method

.method private a(ZLjava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    if-nez v0, :cond_0

    .line 146
    const-string v0, "Coffee - Trustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trustlet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " attempted to change trust state when canProvideTrust state is false (ignored)."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 158
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-ne v0, p1, :cond_1

    .line 152
    const-string v0, "Coffee - Trustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignoring trustlet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\'s\' attempt to change its trust state to the current state which is "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 156
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

    invoke-interface {v0, p0, p1, p2, p3}, Lcom/google/android/gms/auth/trustagent/trustlet/bi;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bh;ZLjava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method


# virtual methods
.method final a(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/bi;)V
    .locals 0

    .prologue
    .line 165
    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

    .line 166
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->f:Landroid/content/Context;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->b()V

    .line 168
    return-void
.end method

.method protected final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 88
    return-void
.end method

.method protected b()V
    .locals 0

    .prologue
    .line 70
    return-void
.end method

.method protected final b(Z)V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    if-ne v0, p1, :cond_0

    .line 133
    const-string v0, "Coffee - Trustlet"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trustlet "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " attempted to change its canProvideTrust state to  the current state (ignored)."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    :goto_0
    return-void

    .line 137
    :cond_0
    if-nez p1, :cond_1

    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v0, :cond_1

    .line 138
    const/4 v0, 0x0

    invoke-direct {p0, v0, v1, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 140
    :cond_1
    iput-boolean p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    .line 141
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bi;->a()V

    goto :goto_0
.end method

.method protected c()V
    .locals 0

    .prologue
    .line 77
    return-void
.end method

.method protected abstract d()Ljava/lang/String;
.end method

.method protected abstract e()I
.end method

.method protected final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 96
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a(ZLjava/lang/String;Ljava/lang/String;)V

    .line 97
    return-void
.end method

.method protected g()Z
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    return v0
.end method

.method protected final j()Z
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bi;->b()Z

    move-result v0

    return v0
.end method

.method public final k()V
    .locals 1

    .prologue
    .line 171
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->c()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bi;

    .line 173
    return-void
.end method
