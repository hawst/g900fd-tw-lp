.class public final Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/requests/c;
.implements Lcom/google/android/gms/games/ui/common/requests/m;


# static fields
.field private static final n:I


# instance fields
.field private o:Lcom/google/android/gms/games/ui/client/requests/a;

.field private p:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

.field private q:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    sget v0, Lcom/google/android/gms/l;->by:I

    sput v0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(I)V

    .line 34
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 67
    const/16 v0, 0xe

    return v0
.end method

.method public final O_()Lcom/google/android/gms/games/ui/common/requests/l;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->o:Lcom/google/android/gms/games/ui/client/requests/a;

    return-object v0
.end method

.method public final T()Lcom/google/android/gms/games/internal/request/GameRequestCluster;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->p:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    return-object v0
.end method

.method public final U()Ljava/lang/String;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->l:Z

    .line 43
    new-instance v0, Lcom/google/android/gms/games/ui/client/requests/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/requests/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->o:Lcom/google/android/gms/games/ui/client/requests/a;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.GAME_REQUEST_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->p:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->q:Ljava/lang/String;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->p:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->i()I

    move-result v0

    .line 50
    packed-switch v0, :pswitch_data_0

    .line 60
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid request type: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 52
    :pswitch_0
    sget v0, Lcom/google/android/gms/p;->kO:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->setTitle(I)V

    .line 62
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->p:Lcom/google/android/gms/games/internal/request/GameRequestCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/request/GameRequestCluster;->g()Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->v_()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->a(Ljava/lang/CharSequence;)V

    .line 63
    return-void

    .line 56
    :pswitch_1
    sget v0, Lcom/google/android/gms/p;->kQ:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/requests/ClientPublicRequestActivity;->setTitle(I)V

    goto :goto_0

    .line 50
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
