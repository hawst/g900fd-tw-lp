.class public final Lcom/google/android/gms/common/images/m;
.super Lcom/google/android/gms/common/images/k;
.source "SourceFile"


# instance fields
.field private j:Ljava/lang/ref/WeakReference;


# direct methods
.method public constructor <init>(Landroid/widget/ImageView;Landroid/net/Uri;)V
    .locals 1

    .prologue
    .line 284
    invoke-direct {p0, p2}, Lcom/google/android/gms/common/images/k;-><init>(Landroid/net/Uri;)V

    .line 285
    invoke-static {p1}, Lcom/google/android/gms/common/internal/e;->a(Ljava/lang/Object;)V

    .line 286
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/common/images/m;->j:Ljava/lang/ref/WeakReference;

    .line 287
    return-void
.end method


# virtual methods
.method protected final a(Landroid/graphics/drawable/Drawable;ZZZ)V
    .locals 8

    .prologue
    const/4 v4, 0x0

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 334
    iget-object v0, p0, Lcom/google/android/gms/common/images/m;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 335
    if-eqz v0, :cond_6

    .line 336
    if-nez p3, :cond_7

    if-nez p4, :cond_7

    move v7, v2

    :goto_0
    if-eqz v7, :cond_0

    instance-of v1, v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v1, :cond_0

    move-object v1, v0

    check-cast v1, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b()I

    move-result v1

    iget v5, p0, Lcom/google/android/gms/common/images/m;->c:I

    if-eqz v5, :cond_0

    iget v5, p0, Lcom/google/android/gms/common/images/m;->c:I

    if-eq v1, v5, :cond_6

    :cond_0
    iget-boolean v1, p0, Lcom/google/android/gms/common/images/k;->f:Z

    if-eqz v1, :cond_8

    if-nez p3, :cond_8

    if-eqz p2, :cond_1

    iget-boolean v1, p0, Lcom/google/android/gms/common/images/k;->g:Z

    if-eqz v1, :cond_8

    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/gms/common/images/m;->d:Z

    if-eqz v1, :cond_b

    if-eqz p1, :cond_b

    invoke-virtual {p1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object p1

    move-object v6, p1

    :goto_2
    if-eqz v2, :cond_a

    invoke-virtual {v0}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    if-eqz v1, :cond_9

    instance-of v5, v1, Lcom/google/android/gms/common/images/internal/b;

    if-eqz v5, :cond_2

    check-cast v1, Lcom/google/android/gms/common/images/internal/b;

    invoke-virtual {v1}, Lcom/google/android/gms/common/images/internal/b;->a()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    :cond_2
    :goto_3
    new-instance v5, Lcom/google/android/gms/common/images/internal/b;

    invoke-direct {v5, v1, v6}, Lcom/google/android/gms/common/images/internal/b;-><init>(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)V

    move-object v1, v5

    :goto_4
    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    instance-of v5, v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz v5, :cond_5

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    if-eqz p4, :cond_3

    iget-object v4, p0, Lcom/google/android/gms/common/images/m;->a:Lcom/google/android/gms/common/images/l;

    iget-object v4, v4, Lcom/google/android/gms/common/images/l;->a:Landroid/net/Uri;

    :cond_3
    invoke-virtual {v0, v4}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->b(Landroid/net/Uri;)V

    if-eqz v7, :cond_4

    iget v3, p0, Lcom/google/android/gms/common/images/m;->c:I

    :cond_4
    invoke-virtual {v0, v3}, Lcom/google/android/gms/common/images/internal/LoadingImageView;->a(I)V

    :cond_5
    if-eqz v2, :cond_6

    move-object v0, v1

    check-cast v0, Lcom/google/android/gms/common/images/internal/b;

    invoke-virtual {v0}, Lcom/google/android/gms/common/images/internal/b;->b()V

    .line 339
    :cond_6
    return-void

    :cond_7
    move v7, v3

    .line 336
    goto :goto_0

    :cond_8
    move v2, v3

    goto :goto_1

    :cond_9
    move-object v1, v4

    goto :goto_3

    :cond_a
    move-object v1, v6

    goto :goto_4

    :cond_b
    move-object v6, p1

    goto :goto_2
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 315
    instance-of v0, p1, Lcom/google/android/gms/common/images/m;

    if-nez v0, :cond_0

    move v0, v2

    .line 327
    :goto_0
    return v0

    .line 318
    :cond_0
    if-ne p0, p1, :cond_1

    move v0, v3

    .line 319
    goto :goto_0

    .line 322
    :cond_1
    check-cast p1, Lcom/google/android/gms/common/images/m;

    .line 323
    iget-object v0, p0, Lcom/google/android/gms/common/images/m;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    .line 324
    iget-object v1, p1, Lcom/google/android/gms/common/images/m;->j:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 327
    if-eqz v1, :cond_2

    if-eqz v0, :cond_2

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v3

    goto :goto_0

    :cond_2
    move v0, v2

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 310
    const/4 v0, 0x0

    return v0
.end method
