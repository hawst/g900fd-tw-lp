.class final Lcom/google/android/gms/auth/trustagent/trustlet/o;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Landroid/location/Location;

.field final synthetic b:Ljava/util/Set;

.field final synthetic c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;Landroid/location/Location;Ljava/util/Set;)V
    .locals 0

    .prologue
    .line 140
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    iput-object p2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    iput-object p3, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->b:Ljava/util/Set;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 14

    .prologue
    .line 140
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    sget-object v0, Lcom/google/android/location/x;->t:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v11

    const/4 v0, 0x2

    new-array v8, v0, [F

    const-string v0, "Coffee - GeofenceLogIntentService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Current Location: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Accuracy: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getAccuracy()F

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_0
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    move-object v9, v0

    check-cast v9, Ljava/lang/String;

    sget-object v0, Lcom/google/android/gms/location/places/q;->c:Lcom/google/android/gms/location/places/u;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->c(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Lcom/google/android/gms/common/api/v;

    move-result-object v1

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/String;

    invoke-interface {v0, v1, v9, v2}, Lcom/google/android/gms/location/places/u;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    const-wide/16 v2, 0xa

    sget-object v1, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/gms/common/api/am;->a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/api/ap;

    move-result-object v0

    move-object v6, v0

    check-cast v6, Lcom/google/android/gms/location/places/w;

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/w;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v13, Lcom/google/android/gms/auth/f/g;

    invoke-direct {v13}, Lcom/google/android/gms/auth/f/g;-><init>()V

    invoke-virtual {v13, v11}, Lcom/google/android/gms/auth/f/g;->a(F)Lcom/google/android/gms/auth/f/g;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    invoke-virtual {v0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v0

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->a:Landroid/location/Location;

    invoke-virtual {v2}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v4

    iget-wide v4, v4, Lcom/google/android/gms/maps/model/LatLng;->a:D

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/w;->b()Lcom/google/android/gms/location/places/f;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/gms/location/places/f;->e()Lcom/google/android/gms/maps/model/LatLng;

    move-result-object v6

    iget-wide v6, v6, Lcom/google/android/gms/maps/model/LatLng;->b:D

    invoke-static/range {v0 .. v8}, Landroid/location/Location;->distanceBetween(DDDD[F)V

    const/4 v0, 0x0

    aget v0, v8, v0

    invoke-virtual {v13, v0}, Lcom/google/android/gms/auth/f/g;->b(F)Lcom/google/android/gms/auth/f/g;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->d(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    invoke-virtual {v13, v0}, Lcom/google/android/gms/auth/f/g;->a(Z)Lcom/google/android/gms/auth/f/g;

    invoke-interface {v10, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    const-string v0, "Coffee - GeofenceLogIntentService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Nearby Alert Trigger: Radius:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v11}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", Distance between:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const/4 v2, 0x0

    aget v2, v8, v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isTrigger:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-static {v2}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->d(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2, v9}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_1
    return-object v10
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 4

    .prologue
    .line 140
    check-cast p1, Ljava/util/List;

    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/p;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/o;)V

    invoke-static {p1, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    new-instance v1, Lcom/google/android/gms/auth/e/f;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-direct {v1, v0}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    const/4 v0, 0x6

    iput v0, v1, Lcom/google/android/gms/auth/e/f;->d:I

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, v1, Lcom/google/android/gms/auth/e/f;->m:Ljava/util/List;

    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/f/g;

    iget-object v3, v1, Lcom/google/android/gms/auth/e/f;->m:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/auth/e/f;->a()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/o;->c:Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;->a(Lcom/google/android/gms/auth/trustagent/trustlet/GeofenceLogsService;)V

    return-void
.end method
