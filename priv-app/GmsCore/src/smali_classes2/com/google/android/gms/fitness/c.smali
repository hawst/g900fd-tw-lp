.class public final Lcom/google/android/gms/fitness/c;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/c;

.field public static final c:Lcom/google/android/gms/fitness/j;

.field public static final d:Lcom/google/android/gms/fitness/i;

.field public static final e:Lcom/google/android/gms/fitness/k;

.field public static final f:Lcom/google/android/gms/fitness/h;

.field public static final g:Lcom/google/android/gms/fitness/b;

.field public static final h:Lcom/google/android/gms/fitness/a;

.field public static final i:Lcom/google/android/gms/fitness/internal/ai;

.field public static final j:Lcom/google/android/gms/common/api/Scope;

.field public static final k:Lcom/google/android/gms/common/api/Scope;

.field public static final l:Lcom/google/android/gms/common/api/Scope;

.field public static final m:Lcom/google/android/gms/common/api/Scope;

.field public static final n:Lcom/google/android/gms/common/api/Scope;

.field public static final o:Lcom/google/android/gms/common/api/Scope;

.field private static final p:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 156
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->a:Lcom/google/android/gms/common/api/j;

    .line 158
    new-instance v0, Lcom/google/android/gms/fitness/d;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->p:Lcom/google/android/gms/common/api/i;

    .line 195
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/fitness/c;->p:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/fitness/c;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->b:Lcom/google/android/gms/common/api/c;

    .line 200
    new-instance v0, Lcom/google/android/gms/fitness/internal/at;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/at;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->c:Lcom/google/android/gms/fitness/j;

    .line 205
    new-instance v0, Lcom/google/android/gms/fitness/internal/as;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/as;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->d:Lcom/google/android/gms/fitness/i;

    .line 210
    new-instance v0, Lcom/google/android/gms/fitness/internal/au;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/au;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->e:Lcom/google/android/gms/fitness/k;

    .line 215
    new-instance v0, Lcom/google/android/gms/fitness/internal/ap;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/ap;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->f:Lcom/google/android/gms/fitness/h;

    .line 220
    new-instance v0, Lcom/google/android/gms/fitness/internal/ao;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/ao;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->g:Lcom/google/android/gms/fitness/b;

    .line 225
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/gms/fitness/internal/ak;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/ak;-><init>()V

    :goto_0
    sput-object v0, Lcom/google/android/gms/fitness/c;->h:Lcom/google/android/gms/fitness/a;

    .line 232
    new-instance v0, Lcom/google/android/gms/fitness/internal/aq;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/aq;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/c;->i:Lcom/google/android/gms/fitness/internal/ai;

    .line 250
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.activity.read"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->j:Lcom/google/android/gms/common/api/Scope;

    .line 268
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.activity.write"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->k:Lcom/google/android/gms/common/api/Scope;

    .line 279
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.location.read"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->l:Lcom/google/android/gms/common/api/Scope;

    .line 289
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.location.write"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->m:Lcom/google/android/gms/common/api/Scope;

    .line 300
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.body.read"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->n:Lcom/google/android/gms/common/api/Scope;

    .line 310
    new-instance v0, Lcom/google/android/gms/common/api/Scope;

    const-string v1, "https://www.googleapis.com/auth/fitness.body.write"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Scope;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/fitness/c;->o:Lcom/google/android/gms/common/api/Scope;

    return-void

    .line 225
    :cond_0
    new-instance v0, Lcom/google/android/gms/fitness/internal/av;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/internal/av;-><init>()V

    goto :goto_0
.end method
