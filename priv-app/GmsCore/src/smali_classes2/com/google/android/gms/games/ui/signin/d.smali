.class public final Lcom/google/android/gms/games/ui/signin/d;
.super Lcom/google/android/gms/games/ui/signin/g;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/aq;
.implements Lcom/google/android/gms/games/internal/game/c;
.implements Lcom/google/android/gms/games/internal/game/d;


# instance fields
.field private b:Lcom/google/android/gms/games/i/b;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/signin/g;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 42
    const/4 v0, 0x6

    return v0
.end method

.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 165
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/d;->g()Z

    move-result v0

    if-nez v0, :cond_0

    .line 180
    :goto_0
    return-void

    .line 169
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    .line 170
    if-nez v0, :cond_1

    .line 172
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0

    .line 173
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 175
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0

    .line 177
    :cond_2
    const-string v1, "CheckGameplayAcl"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error when updating gameplay ACL; status code "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 178
    invoke-static {v0}, Lcom/google/android/gms/games/ui/signin/d;->c(I)I

    move-result v1

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/games/ui/signin/d;->a(II)V

    goto :goto_0
.end method

.method public final synthetic a(Lcom/google/android/gms/common/api/ap;)V
    .locals 6

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/gms/games/m;

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    invoke-interface {p1}, Lcom/google/android/gms/games/m;->c()Lcom/google/android/gms/games/a;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/d;->g()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    :goto_0
    return-void

    :cond_0
    const/4 v2, 0x2

    if-ne v0, v2, :cond_2

    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->e()Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0

    :cond_1
    const/16 v0, 0x2712

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->d(I)V

    goto :goto_0

    :cond_2
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->c()I

    move-result v2

    if-lez v2, :cond_4

    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->t()I

    move-result v0

    if-nez v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v3}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->c()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :try_start_1
    iget-object v4, v0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v5, Lcom/google/android/gms/games/i/k;

    invoke-direct {v5, v0, p0}, Lcom/google/android/gms/games/i/k;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/internal/game/c;)V

    invoke-interface {v4, v5, v2, v3}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :goto_1
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_2
    const-string v0, "SignInClient"

    const-string v2, "service died"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    throw v0

    :cond_3
    const/4 v0, 0x7

    :try_start_3
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_1

    :cond_4
    const-string v2, "CheckGameplayAcl"

    const-string v3, "Unable to load metadata for game"

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/games/ui/signin/d;->c(I)I

    move-result v2

    invoke-virtual {p0, v2, v0}, Lcom/google/android/gms/games/ui/signin/d;->a(II)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/games/i/b;)V
    .locals 5

    .prologue
    .line 52
    iput-object p1, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->c()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 58
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    .line 64
    :goto_0
    return-void

    .line 62
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_0
    iget-object v3, v0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v4, Lcom/google/android/gms/games/i/i;

    invoke-direct {v4, v0, p0}, Lcom/google/android/gms/games/i/i;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/common/api/aq;)V

    iget-object v0, v0, Lcom/google/android/gms/games/i/b;->d:Ljava/lang/String;

    invoke-interface {v3, v4, v1, v2, v0}, Lcom/google/android/gms/games/internal/ea;->a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/game/b;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 126
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/b;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    .line 127
    invoke-interface {p1}, Lcom/google/android/gms/games/internal/game/b;->c()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 129
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/signin/d;->g()Z

    move-result v3

    if-nez v3, :cond_0

    .line 130
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 160
    :goto_0
    return-void

    .line 135
    :cond_0
    const/4 v3, 0x2

    if-ne v1, v3, :cond_1

    .line 136
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 137
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0

    .line 143
    :cond_1
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    if-lez v3, :cond_2

    .line 144
    const/4 v0, 0x0

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    .line 145
    const-string v1, "specified_by_user"

    const/4 v3, 0x0

    invoke-virtual {v2, v1, v3, v0}, Lcom/google/android/gms/common/data/DataHolder;->d(Ljava/lang/String;II)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    .line 150
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 153
    if-eqz v0, :cond_3

    .line 155
    const/4 v0, 0x7

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0

    .line 147
    :cond_2
    :try_start_1
    invoke-static {v1}, Lcom/google/android/gms/games/ui/signin/d;->c(I)I

    move-result v3

    invoke-virtual {p0, v3, v1}, Lcom/google/android/gms/games/ui/signin/d;->a(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 150
    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0

    .line 157
    :cond_3
    const/16 v0, 0xa

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->e(I)V

    .line 158
    invoke-super {p0}, Lcom/google/android/gms/games/ui/signin/g;->i()Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->o()V

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->b()Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/games/ui/signin/d;->b:Lcom/google/android/gms/games/i/b;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->j()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/games/ui/signin/g;->a:Lcom/google/android/gms/games/ui/signin/SignInActivity;

    invoke-virtual {v2}, Lcom/google/android/gms/games/ui/signin/SignInActivity;->g()Ljava/lang/String;

    move-result-object v2

    const-string v3, ""

    invoke-virtual {v0}, Lcom/google/android/gms/games/i/b;->c()V

    :try_start_2
    iget-object v4, v0, Lcom/google/android/gms/games/i/b;->f:Lcom/google/android/gms/games/internal/ea;

    new-instance v5, Lcom/google/android/gms/games/i/m;

    invoke-direct {v5, v0, p0}, Lcom/google/android/gms/games/i/m;-><init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/internal/game/d;)V

    invoke-interface {v4, v5, v1, v2, v3}, Lcom/google/android/gms/games/internal/ea;->b(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SignInClient"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    :cond_5
    const/4 v0, 0x6

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/signin/d;->b(I)V

    goto :goto_0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 47
    const/16 v0, 0x9

    return v0
.end method

.method public final onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/signin/g;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 38
    return-void
.end method
