.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/cj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

.field private final b:Landroid/content/Context;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->b:Landroid/content/Context;

    .line 66
    return-void
.end method


# virtual methods
.method public final a()Landroid/content/Context;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->b:Landroid/content/Context;

    return-object v0
.end method

.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bu;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/service/statemachine/roomclient/bu;-><init>(Landroid/content/ComponentName;Landroid/os/IBinder;)V

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 72
    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/cb;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/cj;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;

    const-string v1, "Service disconnected"

    invoke-static {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/UnboundState;->a(Ljava/lang/String;)Landroid/os/RemoteException;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->b(Landroid/os/RemoteException;)V

    .line 79
    return-void
.end method
