.class final Lcom/google/android/gms/common/widget/a/s;
.super Lcom/google/android/gms/common/widget/a/q;
.source "SourceFile"


# instance fields
.field private final r:Landroid/widget/CompoundButton;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/q;-><init>(Landroid/view/View;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/s;->q:Landroid/view/ViewGroup;

    sget v1, Lcom/google/android/gms/common/widget/d;->b:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    iput-object v0, p0, Lcom/google/android/gms/common/widget/a/s;->r:Landroid/widget/CompoundButton;

    .line 63
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/a/e;)V
    .locals 2

    .prologue
    .line 67
    instance-of v0, p1, Lcom/google/android/gms/common/widget/a/r;

    if-nez v0, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "settingItem must be ToggleSettingItem"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    check-cast p1, Lcom/google/android/gms/common/widget/a/r;

    .line 72
    invoke-super {p0, p1}, Lcom/google/android/gms/common/widget/a/q;->a(Lcom/google/android/gms/common/widget/a/e;)V

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/s;->r:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/r;->k()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/common/widget/a/s;->r:Landroid/widget/CompoundButton;

    invoke-virtual {p1}, Lcom/google/android/gms/common/widget/a/r;->isChecked()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 76
    return-void
.end method
