.class public Lcom/google/android/gms/drive/internal/QueryRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/query/Query;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    new-instance v0, Lcom/google/android/gms/drive/internal/dc;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/dc;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/QueryRequest;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/query/Query;)V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iput p1, p0, Lcom/google/android/gms/drive/internal/QueryRequest;->a:I

    .line 50
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/QueryRequest;->b:Lcom/google/android/gms/drive/query/Query;

    .line 51
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/query/Query;)V
    .locals 1

    .prologue
    .line 55
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/drive/internal/QueryRequest;-><init>(ILcom/google/android/gms/drive/query/Query;)V

    .line 56
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Query;
    .locals 1

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/QueryRequest;->b:Lcom/google/android/gms/drive/query/Query;

    return-object v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 32
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 37
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/dc;->a(Lcom/google/android/gms/drive/internal/QueryRequest;Landroid/os/Parcel;I)V

    .line 38
    return-void
.end method
