.class public final Lcom/google/android/gms/icing/bb;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/icing/bb;


# instance fields
.field public a:Ljava/lang/String;

.field public b:Lcom/google/android/gms/icing/bd;

.field public c:Lcom/google/android/gms/icing/bc;

.field public d:Lcom/google/android/gms/icing/be;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 876
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 877
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    iput-object v1, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    iput-object v1, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bb;->cachedSize:I

    .line 878
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/bb;
    .locals 2

    .prologue
    .line 853
    sget-object v0, Lcom/google/android/gms/icing/bb;->e:[Lcom/google/android/gms/icing/bb;

    if-nez v0, :cond_1

    .line 854
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 856
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/bb;->e:[Lcom/google/android/gms/icing/bb;

    if-nez v0, :cond_0

    .line 857
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/bb;

    sput-object v0, Lcom/google/android/gms/icing/bb;->e:[Lcom/google/android/gms/icing/bb;

    .line 859
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 861
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/bb;->e:[Lcom/google/android/gms/icing/bb;

    return-object v0

    .line 859
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 969
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 970
    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 971
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 974
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-eqz v1, :cond_1

    .line 975
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 978
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-eqz v1, :cond_2

    .line 979
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 982
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-eqz v1, :cond_3

    .line 983
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 986
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 891
    if-ne p1, p0, :cond_1

    .line 932
    :cond_0
    :goto_0
    return v0

    .line 894
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bb;

    if-nez v2, :cond_2

    move v0, v1

    .line 895
    goto :goto_0

    .line 897
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bb;

    .line 898
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 899
    iget-object v2, p1, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 900
    goto :goto_0

    .line 902
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 903
    goto :goto_0

    .line 905
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-nez v2, :cond_5

    .line 906
    iget-object v2, p1, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-eqz v2, :cond_6

    move v0, v1

    .line 907
    goto :goto_0

    .line 910
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    iget-object v3, p1, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/bd;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    move v0, v1

    .line 911
    goto :goto_0

    .line 914
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-nez v2, :cond_7

    .line 915
    iget-object v2, p1, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-eqz v2, :cond_8

    move v0, v1

    .line 916
    goto :goto_0

    .line 919
    :cond_7
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    iget-object v3, p1, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/bc;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_8

    move v0, v1

    .line 920
    goto :goto_0

    .line 923
    :cond_8
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-nez v2, :cond_9

    .line 924
    iget-object v2, p1, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-eqz v2, :cond_0

    move v0, v1

    .line 925
    goto :goto_0

    .line 928
    :cond_9
    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    iget-object v3, p1, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 929
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 937
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    move v0, v1

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 940
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-nez v0, :cond_1

    move v0, v1

    :goto_1
    add-int/2addr v0, v2

    .line 942
    mul-int/lit8 v2, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-nez v0, :cond_2

    move v0, v1

    :goto_2
    add-int/2addr v0, v2

    .line 944
    mul-int/lit8 v0, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-nez v2, :cond_3

    :goto_3
    add-int/2addr v0, v1

    .line 946
    return v0

    .line 937
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 940
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/bd;->hashCode()I

    move-result v0

    goto :goto_1

    .line 942
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/bc;->hashCode()I

    move-result v0

    goto :goto_2

    .line 944
    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    invoke-virtual {v1}, Ljava/lang/Object;->hashCode()I

    move-result v1

    goto :goto_3
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 454
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/bd;

    invoke-direct {v0}, Lcom/google/android/gms/icing/bd;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/icing/bc;

    invoke-direct {v0}, Lcom/google/android/gms/icing/bc;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_4
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-nez v0, :cond_3

    new-instance v0, Lcom/google/android/gms/icing/be;

    invoke-direct {v0}, Lcom/google/android/gms/icing/be;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 952
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 953
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 955
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    if-eqz v0, :cond_1

    .line 956
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->b:Lcom/google/android/gms/icing/bd;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 958
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    if-eqz v0, :cond_2

    .line 959
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->c:Lcom/google/android/gms/icing/bc;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 961
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    if-eqz v0, :cond_3

    .line 962
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/icing/bb;->d:Lcom/google/android/gms/icing/be;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 964
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 965
    return-void
.end method
