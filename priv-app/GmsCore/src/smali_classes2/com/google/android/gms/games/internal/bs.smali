.class final Lcom/google/android/gms/games/internal/bs;
.super Lcom/google/android/gms/games/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 1

    .prologue
    .line 1488
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/a;-><init>()V

    .line 1489
    const-string v0, "Holder must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/m;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/bs;->a:Lcom/google/android/gms/common/api/m;

    .line 1491
    return-void
.end method


# virtual methods
.method public final b(ILandroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 1496
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 1497
    invoke-static {p1}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    .line 1498
    iget-object v1, p0, Lcom/google/android/gms/games/internal/bs;->a:Lcom/google/android/gms/common/api/m;

    new-instance v2, Lcom/google/android/gms/games/internal/aq;

    invoke-direct {v2, v0, p2}, Lcom/google/android/gms/games/internal/aq;-><init>(Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V

    invoke-interface {v1, v2}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 1499
    return-void
.end method
