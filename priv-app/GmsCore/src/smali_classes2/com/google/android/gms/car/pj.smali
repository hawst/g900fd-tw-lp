.class final Lcom/google/android/gms/car/pj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ph;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ph;)V
    .locals 0

    .prologue
    .line 66
    iput-object p1, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 69
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 70
    const-string v0, "CAR.SETUP"

    const-string v1, "Connected to CarService"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 72
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v0}, Lcom/google/android/gms/car/ph;->a(Lcom/google/android/gms/car/ph;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v0

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ph;->a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/id;)Lcom/google/android/gms/car/id;

    .line 74
    iget-object v1, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->t()Lcom/google/android/gms/car/ht;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/car/ph;->a(Lcom/google/android/gms/car/ph;Lcom/google/android/gms/car/ht;)Lcom/google/android/gms/car/ht;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-static {v0}, Lcom/google/android/gms/car/ph;->b(Lcom/google/android/gms/car/ph;)Lcom/google/android/gms/car/ht;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/car/pm;

    iget-object v2, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/pm;-><init>(Lcom/google/android/gms/car/ph;B)V

    iget-object v2, v0, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v2}, Lcom/google/android/gms/car/gx;->i(Lcom/google/android/gms/car/gx;)Ljava/lang/Object;

    move-result-object v2

    monitor-enter v2

    :try_start_0
    iget-object v3, v0, Lcom/google/android/gms/car/ht;->a:Lcom/google/android/gms/car/gx;

    invoke-static {v3, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/hq;)Lcom/google/android/gms/car/hq;

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    new-instance v1, Lcom/google/android/gms/car/hu;

    invoke-direct {v1, v0}, Lcom/google/android/gms/car/hu;-><init>(Lcom/google/android/gms/car/ht;)V

    invoke-static {v1}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;)V

    .line 76
    return-void

    .line 75
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 80
    const-string v0, "CAR.SETUP"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 81
    const-string v0, "CAR.SETUP"

    const-string v1, "onConnectionSuspended"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/pj;->a:Lcom/google/android/gms/car/ph;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ph;->k_()Lcom/google/android/car/fsm/FsmController;

    move-result-object v0

    const-string v1, "EVENT_ERROR"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 84
    return-void
.end method
