.class public final Lcom/google/android/gms/common/internal/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/common/internal/aw;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/internal/aw;)V
    .locals 0

    .prologue
    .line 216
    iput-object p1, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 220
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/au;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 221
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iput-object p2, v0, Lcom/google/android/gms/common/internal/aw;->e:Landroid/os/IBinder;

    .line 222
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iput-object p1, v0, Lcom/google/android/gms/common/internal/aw;->g:Landroid/content/ComponentName;

    .line 223
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    .line 224
    invoke-interface {v0, p1, p2}, Landroid/content/ServiceConnection;->onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 227
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 226
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    const/4 v2, 0x1

    iput v2, v0, Lcom/google/android/gms/common/internal/aw;->c:I

    .line 227
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/aw;->h:Lcom/google/android/gms/common/internal/au;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/au;->a(Lcom/google/android/gms/common/internal/au;)Ljava/util/HashMap;

    move-result-object v1

    monitor-enter v1

    .line 233
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/common/internal/aw;->e:Landroid/os/IBinder;

    .line 234
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iput-object p1, v0, Lcom/google/android/gms/common/internal/aw;->g:Landroid/content/ComponentName;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    iget-object v0, v0, Lcom/google/android/gms/common/internal/aw;->b:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ServiceConnection;

    .line 236
    invoke-interface {v0, p1}, Landroid/content/ServiceConnection;->onServiceDisconnected(Landroid/content/ComponentName;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 239
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 238
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ax;->a:Lcom/google/android/gms/common/internal/aw;

    const/4 v2, 0x2

    iput v2, v0, Lcom/google/android/gms/common/internal/aw;->c:I

    .line 239
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method
