.class public final Lcom/google/android/gms/games/internal/a/n;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/k;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/v;IZZ)Lcom/google/android/gms/common/api/am;
    .locals 7

    .prologue
    .line 343
    new-instance v0, Lcom/google/android/gms/games/internal/a/p;

    const/16 v3, 0x19

    const/4 v4, 0x7

    const/4 v6, 0x0

    move-object v1, p0

    move-object v2, p1

    move v5, p3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/internal/a/p;-><init>(Lcom/google/android/gms/games/internal/a/n;Lcom/google/android/gms/common/api/v;IIZZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Game;
    .locals 1

    .prologue
    .line 141
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->m()Lcom/google/android/gms/games/Game;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 146
    new-instance v0, Lcom/google/android/gms/games/internal/a/o;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/internal/a/o;-><init>(Lcom/google/android/gms/games/internal/a/n;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 316
    const/16 v0, 0x19

    invoke-direct {p0, p1, v0, v1, v1}, Lcom/google/android/gms/games/internal/a/n;->a(Lcom/google/android/gms/common/api/v;IZZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method

.method public final d(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 3

    .prologue
    .line 323
    const/16 v0, 0x19

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/games/internal/a/n;->a(Lcom/google/android/gms/common/api/v;IZZ)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    return-object v0
.end method
