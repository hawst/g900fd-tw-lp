.class public final Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;
.super Lcom/google/android/gms/games/ui/client/a;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/common/matches/ab;
.implements Lcom/google/android/gms/games/ui/common/matches/v;


# static fields
.field private static final n:I


# instance fields
.field private o:Lcom/google/android/gms/games/ui/client/matches/a;

.field private p:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

.field private q:Ljava/lang/String;

.field private r:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget v0, Lcom/google/android/gms/l;->bx:I

    sput v0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->n:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    sget v0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->n:I

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/ui/client/a;-><init>(I)V

    .line 34
    return-void
.end method


# virtual methods
.method protected final N()I
    .locals 1

    .prologue
    .line 55
    const/16 v0, 0xd

    return v0
.end method

.method public final T()Lcom/google/android/gms/games/ui/common/matches/u;
    .locals 1

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->o:Lcom/google/android/gms/games/ui/client/matches/a;

    return-object v0
.end method

.method public final U()Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->p:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    return-object v0
.end method

.method public final V()Ljava/lang/String;
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->q:Ljava/lang/String;

    return-object v0
.end method

.method public final W()Ljava/lang/String;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->r:Ljava/lang/String;

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 38
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/client/a;->onCreate(Landroid/os/Bundle;)V

    .line 41
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->l:Z

    .line 43
    new-instance v0, Lcom/google/android/gms/games/ui/client/matches/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/client/matches/a;-><init>(Lcom/google/android/gms/games/ui/client/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->o:Lcom/google/android/gms/games/ui/client/matches/a;

    .line 45
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.INVITATION_CLUSTER"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->p:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    .line 46
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->q:Ljava/lang/String;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.PLAYER_ID"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->r:Ljava/lang/String;

    .line 49
    sget v0, Lcom/google/android/gms/p;->iN:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->setTitle(I)V

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->p:Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;->g()Lcom/google/android/gms/games/multiplayer/Participant;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/Participant;->g()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/client/matches/ClientPublicInvitationActivity;->a(Ljava/lang/CharSequence;)V

    .line 51
    return-void
.end method
