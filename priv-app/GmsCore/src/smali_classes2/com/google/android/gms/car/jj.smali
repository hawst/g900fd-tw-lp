.class final Lcom/google/android/gms/car/jj;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Ljava/lang/String;

.field final synthetic b:I

.field final synthetic c:Lcom/google/android/gms/car/no;

.field final synthetic d:Lcom/google/android/gms/car/FirstActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/FirstActivity;Ljava/lang/String;ILcom/google/android/gms/car/no;)V
    .locals 1

    .prologue
    .line 646
    iput-object p1, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    iput-object p2, p0, Lcom/google/android/gms/car/jj;->a:Ljava/lang/String;

    const/16 v0, 0x7733

    iput v0, p0, Lcom/google/android/gms/car/jj;->b:I

    iput-object p4, p0, Lcom/google/android/gms/car/jj;->c:Lcom/google/android/gms/car/no;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 649
    new-instance v0, Ljava/net/Socket;

    invoke-direct {v0}, Ljava/net/Socket;-><init>()V

    .line 651
    const/4 v1, 0x0

    :try_start_0
    invoke-virtual {v0, v1}, Ljava/net/Socket;->bind(Ljava/net/SocketAddress;)V

    .line 652
    new-instance v1, Ljava/net/InetSocketAddress;

    iget-object v2, p0, Lcom/google/android/gms/car/jj;->a:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/car/jj;->b:I

    invoke-direct {v1, v2, v3}, Ljava/net/InetSocketAddress;-><init>(Ljava/lang/String;I)V

    invoke-virtual {v0, v1}, Ljava/net/Socket;->connect(Ljava/net/SocketAddress;)V

    .line 653
    const-string v1, "CAR.FIRST"

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 654
    const-string v1, "CAR.FIRST"

    const-string v2, "Connected."

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 656
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/FirstActivity;->s(Lcom/google/android/gms/car/FirstActivity;)Z

    .line 657
    iget-object v1, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    invoke-static {v1, v0}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/Closeable;)Ljava/io/Closeable;

    .line 658
    iget-object v1, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    invoke-virtual {v0}, Ljava/net/Socket;->getInputStream()Ljava/io/InputStream;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/InputStream;)Ljava/io/InputStream;

    .line 659
    iget-object v1, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    invoke-virtual {v0}, Ljava/net/Socket;->getOutputStream()Ljava/io/OutputStream;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/car/FirstActivity;->a(Lcom/google/android/gms/car/FirstActivity;Ljava/io/OutputStream;)Ljava/io/OutputStream;

    .line 660
    iget-object v0, p0, Lcom/google/android/gms/car/jj;->d:Lcom/google/android/gms/car/FirstActivity;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/FirstActivity;->a(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 664
    iget-object v0, p0, Lcom/google/android/gms/car/jj;->c:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 665
    return-void

    .line 661
    :catch_0
    move-exception v0

    .line 662
    :try_start_1
    new-instance v1, Ljava/lang/RuntimeException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Error connecting to sink: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 664
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/car/jj;->c:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->a()V

    throw v0
.end method
