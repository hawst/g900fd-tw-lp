.class public Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_FRX_OPT_IN_ACCEPTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupDoneState;
            c = Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_FRX_OPT_IN_CANCELED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$FrxOptInState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 505
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 508
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    .line 509
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->j()Landroid/content/Intent;

    move-result-object v0

    .line 510
    iget-object v1, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v1, v0}, Lcom/google/android/car/fsm/FsmController;->a(Landroid/content/Intent;)V

    .line 511
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 7

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 515
    const-string v0, "EVENT_ACTIVITY_RESULT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 516
    if-eqz p2, :cond_7

    .line 517
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    invoke-virtual {v0}, Lcom/google/android/car/fsm/FsmController;->c()Lcom/google/android/car/fsm/g;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/pg;

    .line 520
    check-cast p2, Lcom/google/android/car/fsm/ActivityResult;

    .line 521
    iget-object v1, p2, Lcom/google/android/car/fsm/ActivityResult;->b:Landroid/content/Intent;

    if-nez v1, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_FRX_OPT_IN_CANCELED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    .line 564
    :cond_0
    :goto_0
    return v2

    .line 526
    :cond_1
    const-string v1, "CAR.SETUP.SetupFsm"

    const/4 v4, 0x3

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 527
    const-string v1, "CAR.SETUP.SetupFsm"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Intent: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v5, p2, Lcom/google/android/car/fsm/ActivityResult;->b:Landroid/content/Intent;

    invoke-virtual {v5, v3}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    :cond_2
    iget v1, p2, Lcom/google/android/car/fsm/ActivityResult;->a:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_3

    move v1, v2

    .line 531
    :goto_1
    iget-object v4, p2, Lcom/google/android/car/fsm/ActivityResult;->b:Landroid/content/Intent;

    const-string v5, "extra_frx_need_bluetooth_pairing"

    invoke-virtual {v4, v5, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v4

    .line 533
    iget-object v5, p2, Lcom/google/android/car/fsm/ActivityResult;->b:Landroid/content/Intent;

    const-string v6, "EXTRA_FRX_HAS_ERROR"

    invoke-virtual {v5, v6, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v3

    .line 536
    if-eqz v3, :cond_4

    .line 537
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_ERROR"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0

    :cond_3
    move v1, v3

    .line 530
    goto :goto_1

    .line 541
    :cond_4
    if-nez v1, :cond_5

    .line 542
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_FRX_OPT_IN_CANCELED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 546
    :cond_5
    invoke-interface {v0}, Lcom/google/android/gms/car/pg;->l()Z

    move-result v1

    if-nez v1, :cond_6

    .line 547
    invoke-interface {v0, v4}, Lcom/google/android/gms/car/pg;->b(Z)V

    .line 550
    :cond_6
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-string v1, "EVENT_FRX_OPT_IN_ACCEPTED"

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/String;)V

    goto :goto_0

    .line 555
    :cond_7
    const-string v0, "EVENT_ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "EVENT_FRX_OPT_IN_ACCEPTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_8

    const-string v0, "EVENT_FRX_OPT_IN_CANCELED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    :cond_8
    move v2, v3

    .line 560
    goto/16 :goto_0
.end method
