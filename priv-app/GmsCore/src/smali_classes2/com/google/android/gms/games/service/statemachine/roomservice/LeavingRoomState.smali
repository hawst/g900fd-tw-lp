.class final Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;
.super Lcom/google/android/gms/games/service/statemachine/roomservice/u;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private a:Ljava/lang/String;

.field private b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/u;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 25
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 31
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->a:Ljava/lang/String;

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->b:Ljava/lang/String;

    .line 33
    invoke-static {p1}, Lcom/google/android/gms/games/multiplayer/k;->a(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Expecting player id!"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 35
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->c()V

    .line 36
    return-void

    .line 33
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 3

    .prologue
    .line 40
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->k:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 55
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->f:Z

    :goto_0
    return v0

    .line 42
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/h;

    .line 45
    iget-boolean v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/h;->a:Z

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->j:Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->b:Lcom/google/android/gms/games/realtime/network/b;

    invoke-interface {v0}, Lcom/google/android/gms/games/realtime/network/b;->c()V

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->l:Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->f:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->b:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->f:Z

    goto :goto_0

    .line 40
    :pswitch_data_0
    .packed-switch 0x14
        :pswitch_0
    .end packed-switch
.end method
