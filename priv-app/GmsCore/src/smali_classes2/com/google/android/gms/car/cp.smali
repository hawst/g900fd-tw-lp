.class final Lcom/google/android/gms/car/cp;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final a:Lcom/google/android/gms/car/ky;

.field final synthetic b:Lcom/google/android/gms/car/br;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/ky;)V
    .locals 2

    .prologue
    .line 663
    iput-object p1, p0, Lcom/google/android/gms/car/cp;->b:Lcom/google/android/gms/car/br;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 664
    iput-object p2, p0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    .line 665
    invoke-interface {p2}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 666
    return-void
.end method


# virtual methods
.method final a()V
    .locals 3

    .prologue
    .line 669
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 670
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "ClientWrapper.unlink: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v2}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 672
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/cp;->a:Lcom/google/android/gms/car/ky;

    invoke-interface {v0}, Lcom/google/android/gms/car/ky;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 673
    return-void
.end method

.method public final binderDied()V
    .locals 1

    .prologue
    .line 677
    new-instance v0, Lcom/google/android/gms/car/cq;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/cq;-><init>(Lcom/google/android/gms/car/cp;)V

    invoke-static {v0}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;)V

    .line 684
    return-void
.end method
