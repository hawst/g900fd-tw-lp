.class public abstract Lcom/google/android/gms/udc/d/a;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field protected final a:Landroid/content/Context;

.field protected final b:Lcom/google/android/gms/udc/c/c;

.field protected final c:Lcom/google/android/gms/udc/a/a;

.field protected final d:Lcom/google/android/gms/common/server/ClientContext;

.field protected final e:[B

.field protected final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/udc/c/c;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/udc/a/a;[B)V
    .locals 1

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->a:Landroid/content/Context;

    .line 30
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/c/c;

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->b:Lcom/google/android/gms/udc/c/c;

    .line 31
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->d:Lcom/google/android/gms/common/server/ClientContext;

    .line 32
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/a/a;

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->c:Lcom/google/android/gms/udc/a/a;

    .line 33
    invoke-static {p5}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->e:[B

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/udc/d/a;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v0}, Lcom/google/android/gms/common/server/ClientContext;->c()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/d/a;->f:Ljava/lang/String;

    .line 35
    return-void
.end method


# virtual methods
.method final a(Lcom/android/volley/ac;)V
    .locals 2

    .prologue
    .line 38
    iget-object v0, p1, Lcom/android/volley/ac;->a:Lcom/android/volley/m;

    if-nez v0, :cond_0

    .line 39
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1196

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/a;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 43
    :goto_0
    return-void

    .line 41
    :cond_0
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x1199

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/udc/d/a;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
