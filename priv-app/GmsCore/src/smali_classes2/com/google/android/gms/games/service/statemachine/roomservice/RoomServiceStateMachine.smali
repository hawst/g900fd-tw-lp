.class public final Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;
.super Lcom/google/android/gms/games/service/statemachine/h;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# static fields
.field protected static final h:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 47
    sget-object v0, Lcom/google/android/gms/games/c/a;->p:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    sput-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/internal/eg;Lcom/google/android/gms/games/realtime/network/d;Z)V
    .locals 2

    .prologue
    .line 51
    const-string v0, "RoomServiceStateMachine"

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/statemachine/h;-><init>(Ljava/lang/String;)V

    .line 52
    invoke-virtual {p0, p3}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Z)V

    .line 53
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/w;-><init>(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Ljava/lang/Object;)V

    .line 54
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    invoke-direct {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/y;-><init>()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a(Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iput-object p2, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->c:Lcom/google/android/gms/games/realtime/network/d;

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iput-object p1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    .line 57
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;->e()V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->b:Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->c:Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LeavingRoomState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 60
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->d:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesLoadedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->f:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectedState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 63
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->g:Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/NetworkConnectingState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->h:Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->a:Lcom/google/android/gms/games/service/statemachine/roomservice/DefaultHandlerState;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/WaitNetworkDisconnectState;->a(Lcom/google/android/gms/games/service/statemachine/m;)V

    .line 65
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->j:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/y;->e:Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/LibrariesUnloadedState;->f()V

    .line 66
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x1000

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(I)V

    .line 67
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->h:Z

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->b(Z)V

    .line 68
    return-void

    .line 66
    :cond_0
    const/16 v0, 0x80

    goto :goto_0
.end method

.method static a(Landroid/os/RemoteException;)V
    .locals 2

    .prologue
    .line 98
    const-string v0, "RoomServiceStateMachine"

    const-string v1, "Unable to communicate with GamesAndroidService -- exiting"

    invoke-static {v0, v1, p0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 99
    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/System;->exit(I)V

    .line 100
    return-void
.end method


# virtual methods
.method public final a(Ljava/io/PrintWriter;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 74
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 75
    const-string v0, "State machine: "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 76
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Ljava/io/PrintWriter;)V

    .line 77
    return-void
.end method

.method protected final b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 82
    packed-switch p1, :pswitch_data_0

    const/4 v0, 0x0

    :goto_0
    if-eqz v0, :cond_0

    .line 85
    :goto_1
    return-object v0

    .line 82
    :pswitch_0
    const-string v0, "NETWORK_DISCONNECTED"

    goto :goto_0

    :pswitch_1
    const-string v0, "LEAVE_ROOM"

    goto :goto_0

    :pswitch_2
    const-string v0, "DONE_LEAVING_ROOM"

    goto :goto_0

    :pswitch_3
    const-string v0, "ENTER_ROOM"

    goto :goto_0

    :pswitch_4
    const-string v0, "STATUS_NOTIFICATION"

    goto :goto_0

    :pswitch_5
    const-string v0, "PEER_JOINED"

    goto :goto_0

    :pswitch_6
    const-string v0, "P2P_CONNECTION_SUCCEEDED"

    goto :goto_0

    :pswitch_7
    const-string v0, "P2P_CONNECTION_FAILED"

    goto :goto_0

    :pswitch_8
    const-string v0, "MESSAGE_RECEIVED"

    goto :goto_0

    :pswitch_9
    const-string v0, "MESSAGE_SEND_RESULT"

    goto :goto_0

    :pswitch_a
    const-string v0, "SEND_RELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_b
    const-string v0, "SEND_UNRELIABLE_MESSAGE"

    goto :goto_0

    :pswitch_c
    const-string v0, "P2P_STATUS_UPDATED"

    goto :goto_0

    :pswitch_d
    const-string v0, "CONNECT_NETWORK"

    goto :goto_0

    :pswitch_e
    const-string v0, "LOAD_LIBRARIES"

    goto :goto_0

    :pswitch_f
    const-string v0, "DCM_CONNECT_FAILED"

    goto :goto_0

    :pswitch_10
    const-string v0, "DCM_CONNECT_OK"

    goto :goto_0

    :pswitch_11
    const-string v0, "DISCONNECT_NETWORK"

    goto :goto_0

    :pswitch_12
    const-string v0, "UNLOAD_LIBRARIES"

    goto :goto_0

    :pswitch_13
    const-string v0, "CREATE_NATIVE_SOCKET_CONNECTION"

    goto :goto_0

    :pswitch_14
    const-string v0, "CREATE_SOCKET_CONNECTION"

    goto :goto_0

    .line 85
    :cond_0
    invoke-super {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 82
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_14
        :pswitch_13
        :pswitch_0
        :pswitch_1
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_12
        :pswitch_11
        :pswitch_2
    .end packed-switch
.end method

.method protected final h()V
    .locals 3

    .prologue
    .line 91
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->k:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomservice/w;->d:Lcom/google/android/gms/games/internal/eg;

    const/4 v1, 0x0

    const/4 v2, 0x0

    new-array v2, v2, [B

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/eg;->a(Ljava/lang/String;[B)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 95
    :goto_0
    return-void

    .line 92
    :catch_0
    move-exception v0

    invoke-static {v0}, Lcom/google/android/gms/games/service/statemachine/roomservice/RoomServiceStateMachine;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method
