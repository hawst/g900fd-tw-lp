.class final Lcom/google/android/gms/auth/trustagent/o;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)V
    .locals 0

    .prologue
    .line 57
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 4

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    sget v1, Lcom/google/android/gms/j;->br:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 62
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->a(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)Z

    move-result v1

    if-nez v1, :cond_0

    if-eqz v0, :cond_0

    .line 63
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    sget v2, Lcom/google/android/gms/p;->cB:I

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 65
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    sget v3, Lcom/google/android/gms/p;->cz:I

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 67
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " "

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->announceForAccessibility(Ljava/lang/CharSequence;)V

    .line 70
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/o;->a:Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;->b(Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentFirstNotificationActivity;)Z

    .line 71
    return-void
.end method
