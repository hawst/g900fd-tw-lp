.class final Lcom/google/android/gms/auth/trustagent/trustlet/at;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ar;)V
    .locals 0

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/at;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ar;B)V
    .locals 0

    .prologue
    .line 156
    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/at;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/ar;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    .line 159
    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->a(Landroid/content/Intent;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 160
    const-string v0, "PhonePositionTracker"

    const-string v1, "Received intent %s %s without AR result"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    const/4 v3, 0x1

    invoke-virtual {p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :goto_0
    return-void

    .line 165
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/at;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ar;

    invoke-static {p2}, Lcom/google/android/gms/location/ActivityRecognitionResult;->b(Landroid/content/Intent;)Lcom/google/android/gms/location/ActivityRecognitionResult;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ar;->a(Lcom/google/android/gms/location/ActivityRecognitionResult;)V

    goto :goto_0
.end method
