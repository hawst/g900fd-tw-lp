.class public final Lcom/google/android/gms/backup/v;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Z

.field c:J

.field d:Z

.field e:Ljava/lang/String;

.field f:I

.field g:Ljava/util/List;

.field private h:Z

.field private i:Ljava/util/List;

.field private j:Z

.field private k:Ljava/lang/String;

.field private l:Z

.field private m:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 28
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    .line 45
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/backup/v;->c:J

    .line 61
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    .line 95
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->e:Ljava/lang/String;

    .line 112
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->k:Ljava/lang/String;

    .line 129
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/backup/v;->f:I

    .line 145
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    .line 223
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/v;->m:I

    .line 23
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 226
    iget v0, p0, Lcom/google/android/gms/backup/v;->m:I

    if-gez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/gms/backup/v;->b()I

    .line 230
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/v;->m:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/backup/x;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/x;

    return-object v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 20
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/v;->h:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/gms/backup/x;

    invoke-direct {v0}, Lcom/google/android/gms/backup/x;-><init>()V

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;)V

    iget-object v1, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/v;->l:Z

    iput v0, p0, Lcom/google/android/gms/backup/v;->f:I

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/v;->b:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/v;->c:J

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/v;->d:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->e:Ljava/lang/String;

    goto :goto_0

    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/v;->j:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/v;->k:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1a -> :sswitch_1
        0x22 -> :sswitch_2
        0x28 -> :sswitch_3
        0x32 -> :sswitch_4
        0x60 -> :sswitch_5
        0x92 -> :sswitch_6
        0xa2 -> :sswitch_7
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 200
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->h:Z

    if-eqz v0, :cond_0

    .line 201
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 203
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/x;

    .line 204
    const/4 v2, 0x4

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->b(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 206
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->l:Z

    if-eqz v0, :cond_2

    .line 207
    const/4 v0, 0x5

    iget v1, p0, Lcom/google/android/gms/backup/v;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 209
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 210
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 212
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->b:Z

    if-eqz v0, :cond_4

    .line 213
    const/16 v0, 0xc

    iget-wide v2, p0, Lcom/google/android/gms/backup/v;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->a(IJ)V

    .line 215
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->d:Z

    if-eqz v0, :cond_5

    .line 216
    const/16 v0, 0x12

    iget-object v1, p0, Lcom/google/android/gms/backup/v;->e:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 218
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->j:Z

    if-eqz v0, :cond_6

    .line 219
    const/16 v0, 0x14

    iget-object v1, p0, Lcom/google/android/gms/backup/v;->k:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 221
    :cond_6
    return-void
.end method

.method public final b()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 235
    .line 236
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->h:Z

    if-eqz v0, :cond_6

    .line 237
    const/4 v0, 0x3

    iget-object v2, p0, Lcom/google/android/gms/backup/v;->a:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 240
    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move v2, v0

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/x;

    .line 241
    const/4 v4, 0x4

    invoke-static {v4, v0}, Lcom/google/protobuf/a/c;->d(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v2

    move v2, v0

    .line 243
    goto :goto_1

    .line 244
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/backup/v;->l:Z

    if-eqz v0, :cond_1

    .line 245
    const/4 v0, 0x5

    iget v3, p0, Lcom/google/android/gms/backup/v;->f:I

    invoke-static {v0, v3}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/2addr v2, v0

    .line 250
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 251
    invoke-static {v0}, Lcom/google/protobuf/a/c;->a(Ljava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 253
    goto :goto_2

    .line 254
    :cond_2
    add-int v0, v2, v1

    .line 255
    iget-object v1, p0, Lcom/google/android/gms/backup/v;->g:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    mul-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 257
    iget-boolean v1, p0, Lcom/google/android/gms/backup/v;->b:Z

    if-eqz v1, :cond_3

    .line 258
    const/16 v1, 0xc

    iget-wide v2, p0, Lcom/google/android/gms/backup/v;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/a/c;->d(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 261
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/gms/backup/v;->d:Z

    if-eqz v1, :cond_4

    .line 262
    const/16 v1, 0x12

    iget-object v2, p0, Lcom/google/android/gms/backup/v;->e:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 265
    :cond_4
    iget-boolean v1, p0, Lcom/google/android/gms/backup/v;->j:Z

    if-eqz v1, :cond_5

    .line 266
    const/16 v1, 0x14

    iget-object v2, p0, Lcom/google/android/gms/backup/v;->k:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 269
    :cond_5
    iput v0, p0, Lcom/google/android/gms/backup/v;->m:I

    .line 270
    return v0

    :cond_6
    move v0, v1

    goto :goto_0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/backup/v;->i:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
