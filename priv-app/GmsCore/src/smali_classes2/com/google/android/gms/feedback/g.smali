.class public final Lcom/google/android/gms/feedback/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/c;

.field private static final b:Lcom/google/android/gms/common/api/j;

.field private static final c:Lcom/google/android/gms/common/api/i;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 53
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/g;->b:Lcom/google/android/gms/common/api/j;

    .line 56
    new-instance v0, Lcom/google/android/gms/feedback/h;

    invoke-direct {v0}, Lcom/google/android/gms/feedback/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/feedback/g;->c:Lcom/google/android/gms/common/api/i;

    .line 75
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/feedback/g;->c:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/feedback/g;->b:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/feedback/g;->a:Lcom/google/android/gms/common/api/c;

    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/feedback/FeedbackOptions;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 197
    new-instance v0, Lcom/google/android/gms/feedback/i;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/feedback/i;-><init>(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/feedback/FeedbackOptions;)V

    invoke-interface {p0, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a()Lcom/google/android/gms/common/api/j;
    .locals 1

    .prologue
    .line 43
    sget-object v0, Lcom/google/android/gms/feedback/g;->b:Lcom/google/android/gms/common/api/j;

    return-object v0
.end method
