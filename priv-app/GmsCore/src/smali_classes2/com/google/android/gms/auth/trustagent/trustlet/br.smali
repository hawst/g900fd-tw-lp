.class final Lcom/google/android/gms/auth/trustagent/trustlet/br;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/auth/trustagent/trustlet/bq;

.field private b:Z


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/bq;)V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    .line 59
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/br;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bq;

    .line 60
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/br;->b:Z

    .line 61
    return-void
.end method

.method private a(Landroid/nfc/tech/IsoDep;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;
    .locals 8

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x0

    .line 88
    invoke-virtual {p1}, Landroid/nfc/tech/IsoDep;->getTag()Landroid/nfc/Tag;

    move-result-object v0

    invoke-virtual {v0}, Landroid/nfc/Tag;->getId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a([B)Ljava/lang/String;

    move-result-object v2

    .line 90
    new-instance v7, Lcom/google/android/gms/auth/trustagent/trustlet/bb;

    invoke-direct {v7, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;-><init>(Landroid/nfc/tech/IsoDep;)V

    .line 91
    invoke-virtual {v7, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->a(Z)Z

    .line 93
    :try_start_0
    const-string v0, "Coffee - UnlockTag"

    const-string v1, "Trying Precious protocol..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->a()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/q;->a([B)Ljava/lang/String;

    move-result-object v4

    .line 95
    const-string v0, "Coffee - UnlockTag"

    const-string v1, "Paired Precious tag."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/nfc/tech/IsoDep;->getTag()Landroid/nfc/Tag;

    move-result-object v3

    invoke-virtual {v3}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v3

    const-string v5, "NFC Unlock Tag"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;-><init>(ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 121
    :try_start_1
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_0

    .line 124
    :goto_0
    return-object v0

    .line 123
    :catch_0
    move-exception v1

    const-string v1, "Coffee - UnlockTag"

    const-string v2, "failed to close tag"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 101
    :catch_1
    move-exception v0

    :try_start_2
    const-string v0, "Coffee - UnlockTag"

    const-string v1, "Precious not selected. Attempting to select..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 102
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->a(Z)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 103
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->a()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/q;->a([B)Ljava/lang/String;

    move-result-object v4

    .line 104
    const-string v0, "Coffee - UnlockTag"

    const-string v1, "Paired Precious tag."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    const/4 v1, 0x1

    invoke-virtual {p1}, Landroid/nfc/tech/IsoDep;->getTag()Landroid/nfc/Tag;

    move-result-object v3

    invoke-virtual {v3}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v3

    const-string v5, "NFC Unlock Tag"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;-><init>(ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 121
    :try_start_3
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 123
    :catch_2
    move-exception v1

    const-string v1, "Coffee - UnlockTag"

    const-string v2, "failed to close tag"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 109
    :cond_0
    :try_start_4
    invoke-virtual {p1}, Landroid/nfc/tech/IsoDep;->getTag()Landroid/nfc/Tag;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->b(Landroid/nfc/Tag;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 111
    const-string v0, "Coffee - UnlockTag"

    const-string v1, "Precious not available. Using UID."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 112
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    const/4 v1, 0x0

    invoke-virtual {p1}, Landroid/nfc/tech/IsoDep;->getTag()Landroid/nfc/Tag;

    move-result-object v3

    invoke-virtual {v3}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "NFC Unlock Tag"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;-><init>(ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 121
    :try_start_5
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->close()V
    :try_end_5
    .catch Ljava/io/IOException; {:try_start_5 .. :try_end_5} :catch_3

    goto :goto_0

    .line 123
    :catch_3
    move-exception v1

    const-string v1, "Coffee - UnlockTag"

    const-string v2, "failed to close tag"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 115
    :cond_1
    const/4 v0, 0x0

    :try_start_6
    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/br;->b:Z
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 116
    :try_start_7
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->close()V
    :try_end_7
    .catch Ljava/io/IOException; {:try_start_7 .. :try_end_7} :catch_4

    :goto_1
    move-object v0, v6

    .line 124
    goto :goto_0

    .line 123
    :catch_4
    move-exception v0

    const-string v0, "Coffee - UnlockTag"

    const-string v1, "failed to close tag"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 120
    :catchall_0
    move-exception v0

    .line 121
    :try_start_8
    invoke-virtual {v7}, Lcom/google/android/gms/auth/trustagent/trustlet/bb;->close()V
    :try_end_8
    .catch Ljava/io/IOException; {:try_start_8 .. :try_end_8} :catch_5

    .line 124
    :goto_2
    throw v0

    .line 123
    :catch_5
    move-exception v1

    const-string v1, "Coffee - UnlockTag"

    const-string v2, "failed to close tag"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2
.end method

.method private varargs a([Landroid/nfc/Tag;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;
    .locals 7

    .prologue
    const/4 v1, 0x0

    const/4 v6, 0x0

    .line 65
    array-length v0, p1

    if-nez v0, :cond_0

    move-object v0, v6

    .line 83
    :goto_0
    return-object v0

    .line 69
    :cond_0
    aget-object v3, p1, v1

    .line 71
    :try_start_0
    invoke-static {v3}, Landroid/nfc/tech/IsoDep;->get(Landroid/nfc/Tag;)Landroid/nfc/tech/IsoDep;

    move-result-object v0

    .line 72
    if-eqz v0, :cond_1

    .line 73
    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/br;->a(Landroid/nfc/tech/IsoDep;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    move-result-object v0

    goto :goto_0

    .line 75
    :cond_1
    invoke-virtual {v3}, Landroid/nfc/Tag;->getId()[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;->a([B)Ljava/lang/String;

    move-result-object v2

    .line 76
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    const/4 v1, 0x0

    invoke-virtual {v3}, Landroid/nfc/Tag;->getTechList()[Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    const-string v5, "NFC Unlock Tag"

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;-><init>(ILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 80
    :catch_0
    move-exception v0

    .line 82
    const-string v1, "Coffee - UnlockTag"

    const-string v2, "failed to open precious."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    move-object v0, v6

    .line 83
    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 53
    check-cast p1, [Landroid/nfc/Tag;

    invoke-direct {p0, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/br;->a([Landroid/nfc/Tag;)Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 53
    check-cast p1, Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/br;->a:Lcom/google/android/gms/auth/trustagent/trustlet/bq;

    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/br;->b:Z

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bq;->a(Lcom/google/android/gms/auth/trustagent/trustlet/UnlockTag;Z)V

    return-void
.end method
