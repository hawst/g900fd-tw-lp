.class public abstract Lcom/google/android/gms/udc/ui/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field protected final a:Landroid/os/Bundle;


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 95
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/k;->a:Landroid/os/Bundle;

    return-void
.end method


# virtual methods
.method protected abstract a()Lcom/google/android/gms/udc/ui/j;
.end method

.method public final a(Landroid/support/v4/app/v;)Lcom/google/android/gms/udc/ui/j;
    .locals 2

    .prologue
    .line 106
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/k;->b()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    .line 107
    const-string v1, "UdcDialog"

    invoke-virtual {v0, p1, v1}, Lcom/google/android/gms/udc/ui/j;->a(Landroid/support/v4/app/v;Ljava/lang/String;)V

    .line 108
    return-object v0
.end method

.method public final b()Lcom/google/android/gms/udc/ui/j;
    .locals 2

    .prologue
    .line 100
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/k;->a()Lcom/google/android/gms/udc/ui/j;

    move-result-object v0

    .line 101
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/k;->a:Landroid/os/Bundle;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/j;->setArguments(Landroid/os/Bundle;)V

    .line 102
    return-object v0
.end method
