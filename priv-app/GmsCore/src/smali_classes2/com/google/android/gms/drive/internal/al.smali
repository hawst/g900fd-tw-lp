.class final Lcom/google/android/gms/drive/internal/al;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 380
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/al;->a:Lcom/google/android/gms/common/api/m;

    .line 381
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/al;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/am;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/am;-><init>(Lcom/google/android/gms/common/api/Status;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 392
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;)V
    .locals 4

    .prologue
    .line 385
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/al;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/am;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnSyncMoreResponse;->a()Z

    move-result v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/am;-><init>(Lcom/google/android/gms/common/api/Status;Z)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 387
    return-void
.end method
