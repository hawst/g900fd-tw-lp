.class final Lcom/google/android/gms/people/internal/am;
.super Lcom/google/android/gms/people/internal/a;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 1017
    invoke-direct {p0}, Lcom/google/android/gms/people/internal/a;-><init>()V

    .line 1018
    iput-object p1, p0, Lcom/google/android/gms/people/internal/am;->a:Lcom/google/android/gms/common/api/m;

    .line 1019
    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 1023
    const/4 v1, 0x3

    invoke-static {v1}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1024
    const-string v1, "PeopleClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bundle callback: status="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nresolution="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nbundle="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1027
    :cond_0
    invoke-static {p1, p2}, Lcom/google/android/gms/people/internal/p;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    .line 1030
    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 1031
    const-string v0, "added_circles"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 1032
    const-string v0, "removed_circles"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 1035
    :goto_0
    iget-object v3, p0, Lcom/google/android/gms/people/internal/am;->a:Lcom/google/android/gms/common/api/m;

    new-instance v4, Lcom/google/android/gms/people/internal/an;

    invoke-direct {v4, v2, v1, v0}, Lcom/google/android/gms/people/internal/an;-><init>(Lcom/google/android/gms/common/api/Status;Ljava/util/List;Ljava/util/List;)V

    invoke-interface {v3, v4}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 1036
    return-void

    :cond_1
    move-object v1, v0

    goto :goto_0
.end method
