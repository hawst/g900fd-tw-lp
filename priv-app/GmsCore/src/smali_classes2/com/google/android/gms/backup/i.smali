.class public final Lcom/google/android/gms/backup/i;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Z

.field b:J

.field c:Z

.field d:Ljava/lang/String;

.field e:I

.field private f:Z

.field private g:J

.field private h:Z

.field private i:Z

.field private j:I

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/lang/String;

.field private o:Ljava/util/List;

.field private p:Z

.field private q:Lcom/google/android/gms/backup/o;

.field private r:Z

.field private s:Lcom/google/android/gms/backup/n;

.field private t:I


# direct methods
.method public constructor <init>()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 857
    iput-wide v4, p0, Lcom/google/android/gms/backup/i;->b:J

    .line 874
    iput-wide v4, p0, Lcom/google/android/gms/backup/i;->g:J

    .line 891
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    .line 908
    iput v1, p0, Lcom/google/android/gms/backup/i;->e:I

    .line 925
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/backup/i;->j:I

    .line 942
    iput-boolean v1, p0, Lcom/google/android/gms/backup/i;->l:Z

    .line 959
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/backup/i;->n:Ljava/lang/String;

    .line 975
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    .line 1009
    iput-object v2, p0, Lcom/google/android/gms/backup/i;->q:Lcom/google/android/gms/backup/o;

    .line 1029
    iput-object v2, p0, Lcom/google/android/gms/backup/i;->s:Lcom/google/android/gms/backup/n;

    .line 1105
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/i;->t:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 1108
    iget v0, p0, Lcom/google/android/gms/backup/i;->t:I

    if-gez v0, :cond_0

    .line 1110
    invoke-virtual {p0}, Lcom/google/android/gms/backup/i;->b()I

    .line 1112
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/i;->t:I

    return v0
.end method

.method public final a(I)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 912
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->h:Z

    .line 913
    iput p1, p0, Lcom/google/android/gms/backup/i;->e:I

    .line 914
    return-object p0
.end method

.method public final a(J)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 861
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->a:Z

    .line 862
    iput-wide p1, p0, Lcom/google/android/gms/backup/i;->b:J

    .line 863
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 992
    if-nez p1, :cond_0

    .line 993
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 995
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 996
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    .line 998
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 999
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/n;)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 1033
    if-nez p1, :cond_0

    .line 1034
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1036
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->r:Z

    .line 1037
    iput-object p1, p0, Lcom/google/android/gms/backup/i;->s:Lcom/google/android/gms/backup/n;

    .line 1038
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/backup/o;)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 1013
    if-nez p1, :cond_0

    .line 1014
    new-instance v0, Ljava/lang/NullPointerException;

    invoke-direct {v0}, Ljava/lang/NullPointerException;-><init>()V

    throw v0

    .line 1016
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->p:Z

    .line 1017
    iput-object p1, p0, Lcom/google/android/gms/backup/i;->q:Lcom/google/android/gms/backup/o;

    .line 1018
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 895
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->c:Z

    .line 896
    iput-object p1, p0, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    .line 897
    return-object p0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 5
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/backup/i;->a(J)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_2
    new-instance v0, Lcom/google/android/gms/backup/j;

    invoke-direct {v0}, Lcom/google/android/gms/backup/j;-><init>()V

    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/j;)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_3
    new-instance v0, Lcom/google/android/gms/backup/o;

    invoke-direct {v0}, Lcom/google/android/gms/backup/o;-><init>()V

    const/16 v1, 0xd

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/o;)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->a(Ljava/lang/String;)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->a(I)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_6
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->i()J

    move-result-wide v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/i;->f:Z

    iput-wide v0, p0, Lcom/google/android/gms/backup/i;->g:J

    goto :goto_0

    :sswitch_7
    new-instance v0, Lcom/google/android/gms/backup/n;

    invoke-direct {v0}, Lcom/google/android/gms/backup/n;-><init>()V

    const/16 v1, 0x11

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->a(Lcom/google/android/gms/backup/n;)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_8
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/backup/i;->b(I)Lcom/google/android/gms/backup/i;

    goto :goto_0

    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->d()Z

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/i;->k:Z

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->l:Z

    goto :goto_0

    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->e()Ljava/lang/String;

    move-result-object v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/i;->m:Z

    iput-object v0, p0, Lcom/google/android/gms/backup/i;->n:Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x6b -> :sswitch_3
        0x72 -> :sswitch_4
        0x78 -> :sswitch_5
        0x80 -> :sswitch_6
        0x8b -> :sswitch_7
        0x98 -> :sswitch_8
        0xb0 -> :sswitch_9
        0xba -> :sswitch_a
    .end sparse-switch
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 4

    .prologue
    .line 1073
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->a:Z

    if-eqz v0, :cond_0

    .line 1074
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/backup/i;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1076
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/j;

    .line 1077
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 1079
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->p:Z

    if-eqz v0, :cond_2

    .line 1080
    const/16 v0, 0xd

    iget-object v1, p0, Lcom/google/android/gms/backup/i;->q:Lcom/google/android/gms/backup/o;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    .line 1082
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->c:Z

    if-eqz v0, :cond_3

    .line 1083
    const/16 v0, 0xe

    iget-object v1, p0, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1085
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->h:Z

    if-eqz v0, :cond_4

    .line 1086
    const/16 v0, 0xf

    iget v1, p0, Lcom/google/android/gms/backup/i;->e:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1088
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->f:Z

    if-eqz v0, :cond_5

    .line 1089
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/android/gms/backup/i;->g:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/a/c;->b(IJ)V

    .line 1091
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->r:Z

    if-eqz v0, :cond_6

    .line 1092
    const/16 v0, 0x11

    iget-object v1, p0, Lcom/google/android/gms/backup/i;->s:Lcom/google/android/gms/backup/n;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    .line 1094
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->i:Z

    if-eqz v0, :cond_7

    .line 1095
    const/16 v0, 0x13

    iget v1, p0, Lcom/google/android/gms/backup/i;->j:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 1097
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->k:Z

    if-eqz v0, :cond_8

    .line 1098
    const/16 v0, 0x16

    iget-boolean v1, p0, Lcom/google/android/gms/backup/i;->l:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(IZ)V

    .line 1100
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->m:Z

    if-eqz v0, :cond_9

    .line 1101
    const/16 v0, 0x17

    iget-object v1, p0, Lcom/google/android/gms/backup/i;->n:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(ILjava/lang/String;)V

    .line 1103
    :cond_9
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 1117
    const/4 v0, 0x0

    .line 1118
    iget-boolean v1, p0, Lcom/google/android/gms/backup/i;->a:Z

    if-eqz v1, :cond_0

    .line 1119
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/backup/i;->b:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 1122
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/backup/i;->o:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/j;

    .line 1123
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 1125
    goto :goto_0

    .line 1126
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->p:Z

    if-eqz v0, :cond_2

    .line 1127
    const/16 v0, 0xd

    iget-object v2, p0, Lcom/google/android/gms/backup/i;->q:Lcom/google/android/gms/backup/o;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1130
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->c:Z

    if-eqz v0, :cond_3

    .line 1131
    const/16 v0, 0xe

    iget-object v2, p0, Lcom/google/android/gms/backup/i;->d:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1134
    :cond_3
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->h:Z

    if-eqz v0, :cond_4

    .line 1135
    const/16 v0, 0xf

    iget v2, p0, Lcom/google/android/gms/backup/i;->e:I

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 1138
    :cond_4
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->f:Z

    if-eqz v0, :cond_5

    .line 1139
    const/16 v0, 0x10

    iget-wide v2, p0, Lcom/google/android/gms/backup/i;->g:J

    invoke-static {v0, v2, v3}, Lcom/google/protobuf/a/c;->e(IJ)I

    move-result v0

    add-int/2addr v1, v0

    .line 1142
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->r:Z

    if-eqz v0, :cond_6

    .line 1143
    const/16 v0, 0x11

    iget-object v2, p0, Lcom/google/android/gms/backup/i;->s:Lcom/google/android/gms/backup/n;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1146
    :cond_6
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->i:Z

    if-eqz v0, :cond_7

    .line 1147
    const/16 v0, 0x13

    iget v2, p0, Lcom/google/android/gms/backup/i;->j:I

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 1150
    :cond_7
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->k:Z

    if-eqz v0, :cond_8

    .line 1151
    const/16 v0, 0x16

    iget-boolean v2, p0, Lcom/google/android/gms/backup/i;->l:Z

    invoke-static {v0}, Lcom/google/protobuf/a/c;->b(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    add-int/2addr v1, v0

    .line 1154
    :cond_8
    iget-boolean v0, p0, Lcom/google/android/gms/backup/i;->m:Z

    if-eqz v0, :cond_9

    .line 1155
    const/16 v0, 0x17

    iget-object v2, p0, Lcom/google/android/gms/backup/i;->n:Ljava/lang/String;

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->b(ILjava/lang/String;)I

    move-result v0

    add-int/2addr v1, v0

    .line 1158
    :cond_9
    iput v1, p0, Lcom/google/android/gms/backup/i;->t:I

    .line 1159
    return v1
.end method

.method public final b(I)Lcom/google/android/gms/backup/i;
    .locals 1

    .prologue
    .line 929
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/i;->i:Z

    .line 930
    iput p1, p0, Lcom/google/android/gms/backup/i;->j:I

    .line 931
    return-object p0
.end method
