.class final Lcom/google/android/gms/car/senderprotocol/s;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:[Lcom/google/android/c/b/co;

.field final synthetic b:Lcom/google/android/gms/car/senderprotocol/q;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/senderprotocol/q;[Lcom/google/android/c/b/co;)V
    .locals 0

    .prologue
    .line 266
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/s;->b:Lcom/google/android/gms/car/senderprotocol/q;

    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/s;->a:[Lcom/google/android/c/b/co;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 14

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/s;->b:Lcom/google/android/gms/car/senderprotocol/q;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/q;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->g()Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v5

    .line 270
    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/s;->a:[Lcom/google/android/c/b/co;

    array-length v7, v6

    const/4 v0, 0x0

    move v4, v0

    :goto_0
    if-ge v4, v7, :cond_1c

    aget-object v8, v6, v4

    .line 271
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v9

    const/4 v0, 0x0

    if-eqz v9, :cond_0

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Service id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v1, v8, Lcom/google/android/c/b/co;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v1, " type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_0
    const/4 v1, 0x0

    iget-object v2, v8, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    if-eqz v2, :cond_5

    if-eqz v9, :cond_2

    const-string v2, "SensorSourceService {"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v8, Lcom/google/android/c/b/co;->b:Lcom/google/android/c/b/cm;

    iget-object v3, v2, Lcom/google/android/c/b/cm;->a:[Lcom/google/android/c/b/cn;

    array-length v10, v3

    const/4 v2, 0x0

    :goto_1
    if-ge v2, v10, :cond_1

    aget-object v11, v3, v2

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " type="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v11, Lcom/google/android/c/b/cn;->a:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    :cond_1
    const-string v2, "} "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_2
    :goto_2
    if-eqz v1, :cond_3

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/google/android/gms/car/senderprotocol/p;->a:Z

    iget v2, v8, Lcom/google/android/c/b/co;->a:I

    iput v2, v1, Lcom/google/android/gms/car/senderprotocol/p;->b:I

    :cond_3
    if-eqz v9, :cond_4

    const-string v1, "CAR.GAL"

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 270
    :cond_4
    add-int/lit8 v0, v4, 0x1

    move v4, v0

    goto :goto_0

    .line 271
    :cond_5
    iget-object v2, v8, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    if-eqz v2, :cond_9

    iget-object v2, v8, Lcom/google/android/c/b/co;->c:Lcom/google/android/c/b/bd;

    if-eqz v9, :cond_6

    const-string v3, "MediaSinkService {"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " codec type="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->a()I

    move-result v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_6
    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->c()Z

    move-result v3

    if-eqz v3, :cond_7

    iget-object v3, v2, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v3, v3

    if-eqz v3, :cond_7

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->i:[Lcom/google/android/gms/car/senderprotocol/p;

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->b()I

    move-result v3

    add-int/lit8 v3, v3, -0x1

    aget-object v1, v1, v3

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->a()I

    move-result v10

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->b()I

    move-result v11

    iget-object v12, v2, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    invoke-interface {v3, v10, v11, v12}, Lcom/google/android/gms/car/senderprotocol/o;->a(II[Lcom/google/android/c/b/f;)V

    if-eqz v9, :cond_8

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " audioStreamType="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->b()I

    move-result v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/google/android/c/b/bd;->a:[Lcom/google/android/c/b/f;

    array-length v10, v3

    const/4 v2, 0x0

    :goto_3
    if-ge v2, v10, :cond_8

    aget-object v11, v3, v2

    const-string v12, " {"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " samplingRate="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, v11, Lcom/google/android/c/b/f;->a:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " numBits="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v13, v11, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " channels="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v11, v11, Lcom/google/android/c/b/f;->c:I

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "}"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    :cond_7
    iget-object v3, v2, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v3, v3

    if-eqz v3, :cond_8

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->h:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-virtual {v2}, Lcom/google/android/c/b/bd;->a()I

    iget-object v10, v2, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    invoke-interface {v3, v10}, Lcom/google/android/gms/car/senderprotocol/o;->a([Lcom/google/android/c/b/cz;)V

    if-eqz v9, :cond_8

    iget-object v3, v2, Lcom/google/android/c/b/bd;->b:[Lcom/google/android/c/b/cz;

    array-length v10, v3

    const/4 v2, 0x0

    :goto_4
    if-ge v2, v10, :cond_8

    aget-object v11, v3, v2

    const-string v12, " {"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " codecResolution="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/google/android/c/b/cz;->b()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " widthMargin="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/google/android/c/b/cz;->e()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " heightMargin="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/google/android/c/b/cz;->g()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v12, Ljava/lang/StringBuilder;

    const-string v13, " density="

    invoke-direct {v12, v13}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v11}, Lcom/google/android/c/b/cz;->i()I

    move-result v11

    invoke-virtual {v12, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v11, "}"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    :cond_8
    if-eqz v9, :cond_2

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_9
    iget-object v2, v8, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    if-eqz v2, :cond_10

    iget-object v10, v8, Lcom/google/android/c/b/co;->d:Lcom/google/android/c/b/ak;

    iget-object v1, v10, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    if-eqz v1, :cond_b

    iget-object v1, v10, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v1, v1

    if-eqz v1, :cond_b

    const/4 v1, 0x1

    :goto_5
    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->l:Lcom/google/android/gms/car/senderprotocol/p;

    const/4 v2, 0x0

    iget-object v11, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    if-eqz v11, :cond_a

    iget-object v11, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v11, v11

    if-lez v11, :cond_a

    new-instance v2, Landroid/graphics/Point;

    iget-object v11, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    const/4 v12, 0x0

    aget-object v11, v11, v12

    iget v11, v11, Lcom/google/android/c/b/al;->a:I

    iget-object v12, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    const/4 v13, 0x0

    aget-object v12, v12, v13

    iget v12, v12, Lcom/google/android/c/b/al;->b:I

    invoke-direct {v2, v11, v12}, Landroid/graphics/Point;-><init>(II)V

    :cond_a
    iget-object v11, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    iget-object v12, v10, Lcom/google/android/c/b/ak;->a:[I

    invoke-interface {v11, v1, v12, v2}, Lcom/google/android/gms/car/senderprotocol/o;->a(Z[ILandroid/graphics/Point;)V

    if-eqz v9, :cond_f

    const-string v1, "InputSourceService "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v1, "{ keycodes=["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v10, Lcom/google/android/c/b/ak;->a:[I

    array-length v11, v2

    const/4 v1, 0x0

    :goto_6
    if-ge v1, v11, :cond_c

    aget v12, v2, v1

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    new-instance v13, Ljava/lang/StringBuilder;

    invoke-direct {v13}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v13, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ","

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_6

    :cond_b
    const/4 v1, 0x0

    goto :goto_5

    :cond_c
    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v2, v10, Lcom/google/android/c/b/ak;->b:[Lcom/google/android/c/b/am;

    array-length v11, v2

    const/4 v1, 0x0

    :goto_7
    if-ge v1, v11, :cond_d

    aget-object v12, v2, v1

    const-string v13, " { touchscreen width="

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v13, v12, Lcom/google/android/c/b/am;->a:I

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v13, " height="

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v12, v12, Lcom/google/android/c/b/am;->b:I

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, " }"

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    :cond_d
    iget-object v1, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    if-eqz v1, :cond_e

    iget-object v2, v10, Lcom/google/android/c/b/ak;->c:[Lcom/google/android/c/b/al;

    array-length v10, v2

    const/4 v1, 0x0

    :goto_8
    if-ge v1, v10, :cond_e

    aget-object v11, v2, v1

    const-string v12, " { touchpad width="

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v12, v11, Lcom/google/android/c/b/al;->a:I

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v12, " height="

    invoke-virtual {v0, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget v11, v11, Lcom/google/android/c/b/al;->b:I

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, " }"

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v1, v1, 0x1

    goto :goto_8

    :cond_e
    const-string v1, "}"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_f
    move-object v1, v3

    goto/16 :goto_2

    :cond_10
    iget-object v2, v8, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    if-eqz v2, :cond_13

    iget-object v2, v8, Lcom/google/android/c/b/co;->e:Lcom/google/android/c/b/bi;

    if-eqz v9, :cond_11

    const-string v3, "MediaSourceService "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "{"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " codec type="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Lcom/google/android/c/b/bi;->a()I

    move-result v10

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_11
    iget-object v3, v2, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    if-eqz v3, :cond_12

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->j:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-virtual {v2}, Lcom/google/android/c/b/bi;->a()I

    iget-object v10, v2, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    invoke-interface {v3, v10}, Lcom/google/android/gms/car/senderprotocol/o;->a(Lcom/google/android/c/b/f;)V

    if-eqz v9, :cond_12

    iget-object v2, v2, Lcom/google/android/c/b/bi;->a:Lcom/google/android/c/b/f;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " samplingRate="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcom/google/android/c/b/f;->a:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " numBits="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " channels="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, v2, Lcom/google/android/c/b/f;->c:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_12
    if-eqz v9, :cond_2

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_13
    iget-object v2, v8, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    if-eqz v2, :cond_15

    iget-object v2, v8, Lcom/google/android/c/b/co;->f:Lcom/google/android/c/b/m;

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->m:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    iget-object v10, v2, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    iget-object v11, v2, Lcom/google/android/c/b/m;->b:[I

    invoke-interface {v3, v10, v11}, Lcom/google/android/gms/car/senderprotocol/o;->a(Ljava/lang/String;[I)V

    if-eqz v9, :cond_2

    const-string v3, "BluetoothService "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, "{"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " carAddress="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v2, Lcom/google/android/c/b/m;->a:Ljava/lang/String;

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, " supportedPairingMethods=["

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/google/android/c/b/m;->b:[I

    array-length v10, v3

    const/4 v2, 0x0

    :goto_9
    if-ge v2, v10, :cond_14

    aget v11, v3, v2

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    const-string v11, ","

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v2, v2, 0x1

    goto :goto_9

    :cond_14
    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_15
    iget-object v2, v8, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    if-eqz v2, :cond_17

    iget-object v2, v8, Lcom/google/android/c/b/co;->h:Lcom/google/android/c/b/bq;

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->n:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    iget v10, v2, Lcom/google/android/c/b/bq;->a:I

    iget v11, v2, Lcom/google/android/c/b/bq;->b:I

    iget-object v12, v2, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    invoke-interface {v3, v10, v11, v12}, Lcom/google/android/gms/car/senderprotocol/o;->a(IILcom/google/android/c/b/br;)V

    if-eqz v9, :cond_2

    const-string v3, "NavigationStatusService {"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " minimumIntervalMs="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcom/google/android/c/b/bq;->a:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " type="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v10, v2, Lcom/google/android/c/b/bq;->b:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    iget-object v3, v2, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    if-eqz v3, :cond_16

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " height="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v2, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    iget v10, v10, Lcom/google/android/c/b/br;->a:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " width="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v10, v2, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    iget v10, v10, Lcom/google/android/c/b/br;->b:I

    invoke-virtual {v3, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " colourDepthBits="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/c/b/bq;->c:Lcom/google/android/c/b/br;

    iget v2, v2, Lcom/google/android/c/b/br;->c:I

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    :cond_16
    const-string v2, " }"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_17
    iget-object v2, v8, Lcom/google/android/c/b/co;->i:Lcom/google/android/c/b/bb;

    if-eqz v2, :cond_18

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->o:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-interface {v2}, Lcom/google/android/gms/car/senderprotocol/o;->a()V

    if-eqz v9, :cond_2

    const-string v2, "MediaPlaybackService"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_18
    iget-object v2, v8, Lcom/google/android/c/b/co;->k:Lcom/google/android/c/b/av;

    if-eqz v2, :cond_19

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->p:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-interface {v2}, Lcom/google/android/gms/car/senderprotocol/o;->b()V

    if-eqz v9, :cond_2

    const-string v2, "MediaBrowserService"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_19
    iget-object v2, v8, Lcom/google/android/c/b/co;->j:Lcom/google/android/c/b/cb;

    if-eqz v2, :cond_1a

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->q:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-interface {v2}, Lcom/google/android/gms/car/senderprotocol/o;->c()V

    if-eqz v9, :cond_2

    const-string v2, "PhoneStatusService"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_1a
    iget-object v2, v8, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    if-eqz v2, :cond_1b

    iget-object v2, v8, Lcom/google/android/c/b/co;->l:Lcom/google/android/c/b/cy;

    new-instance v1, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v1, v5}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    iget-object v10, v2, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    iget-object v11, v2, Lcom/google/android/c/b/cy;->b:[Ljava/lang/String;

    invoke-virtual {v2}, Lcom/google/android/c/b/cy;->a()[B

    move-result-object v12

    invoke-interface {v3, v10, v11, v12}, Lcom/google/android/gms/car/senderprotocol/o;->a(Ljava/lang/String;[Ljava/lang/String;[B)V

    iget-object v3, v5, Lcom/google/android/gms/car/senderprotocol/k;->s:Ljava/util/HashMap;

    iget-object v10, v2, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    invoke-virtual {v3, v10, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    if-eqz v9, :cond_2

    const-string v3, "VendorExtensionService {"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v10, " name="

    invoke-direct {v3, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, v2, Lcom/google/android/c/b/cy;->a:Ljava/lang/String;

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v2, "}"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    :cond_1b
    iget-object v2, v8, Lcom/google/android/c/b/co;->m:Lcom/google/android/c/b/ad;

    if-eqz v2, :cond_2

    iget-object v1, v5, Lcom/google/android/gms/car/senderprotocol/k;->r:Lcom/google/android/gms/car/senderprotocol/p;

    iget-object v2, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-interface {v2}, Lcom/google/android/gms/car/senderprotocol/o;->d()V

    if-eqz v9, :cond_2

    const-string v2, "NotificationService"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto/16 :goto_2

    .line 273
    :cond_1c
    const/4 v0, 0x1

    iput-boolean v0, v5, Lcom/google/android/gms/car/senderprotocol/k;->g:Z

    iget-object v0, v5, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/o;->e()V

    .line 274
    return-void
.end method
