.class public Lcom/google/android/gms/drive/internal/OnContentsResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Lcom/google/android/gms/drive/Contents;

.field final c:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/internal/cm;

    invoke-direct {v0}, Lcom/google/android/gms/drive/internal/cm;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/Contents;Z)V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput p1, p0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->a:I

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->b:Lcom/google/android/gms/drive/Contents;

    .line 56
    iput-boolean p3, p0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->c:Z

    .line 57
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/Contents;Z)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/drive/internal/OnContentsResponse;-><init>(ILcom/google/android/gms/drive/Contents;Z)V

    .line 61
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/Contents;
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->b:Lcom/google/android/gms/drive/Contents;

    return-object v0
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 67
    iget-boolean v0, p0, Lcom/google/android/gms/drive/internal/OnContentsResponse;->c:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 41
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/drive/internal/cm;->a(Lcom/google/android/gms/drive/internal/OnContentsResponse;Landroid/os/Parcel;I)V

    .line 42
    return-void
.end method
