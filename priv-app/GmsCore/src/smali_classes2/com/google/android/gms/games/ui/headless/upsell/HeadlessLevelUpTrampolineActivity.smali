.class public final Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/ui/a/b;
.implements Lcom/google/android/gms/games/ui/bj;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    return-void
.end method

.method private a()V
    .locals 4

    .prologue
    .line 62
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_LEVEL_UP_UPSELL_INTERNAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 63
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v3, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->startActivity(Landroid/content/Intent;)V

    .line 66
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->finish()V

    .line 67
    return-void
.end method


# virtual methods
.method public final B()Lcom/google/android/gms/games/ui/a/a;
    .locals 1

    .prologue
    .line 77
    new-instance v0, Lcom/google/android/gms/games/ui/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/ui/a/a;-><init>(Landroid/support/v4/app/q;)V

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 25
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->b(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->a()V

    .line 33
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->isFinishing()Z

    move-result v0

    const-string v1, "The HeadlessLevelUpTrampolineActivity should be finishing once the API has been handled"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/e;->a(ZLjava/lang/Object;)V

    .line 35
    return-void

    .line 29
    :cond_0
    invoke-static {p0}, Lcom/google/android/gms/games/ui/d/al;->c(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_1

    invoke-direct {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->a()V

    goto :goto_0

    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    const-string v2, "com.google.android.gms.games.NOTIFICATION_OPENED"

    invoke-virtual {v1, v2, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    invoke-static {p0, v0, v3, v1}, Lcom/google/android/gms/games/ui/a/c;->a(Landroid/app/Activity;Ljava/lang/String;ZLandroid/os/Bundle;)V

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpTrampolineActivity;->finish()V

    goto :goto_0
.end method

.method public final y()Lcom/google/android/gms/games/ui/bh;
    .locals 4

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/gms/games/ui/bh;

    const/4 v1, 0x0

    const/4 v2, 0x2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/games/ui/bh;-><init>(Landroid/app/Activity;II)V

    return-object v0
.end method
