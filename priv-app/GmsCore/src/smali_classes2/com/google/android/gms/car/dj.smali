.class public final Lcom/google/android/gms/car/dj;
.super Lcom/google/android/gms/car/lc;
.source "SourceFile"


# static fields
.field private static f:Ljava/lang/reflect/Field;

.field private static g:Ljava/lang/reflect/Field;

.field private static h:Ljava/lang/reflect/Field;


# instance fields
.field private final a:Ljava/lang/Object;

.field private final b:Lcom/google/android/gms/car/dq;

.field private c:Lcom/google/android/gms/car/dn;

.field private volatile d:Landroid/telecom/Phone;

.field private final e:Landroid/content/Context;

.field private final i:Landroid/telecom/Phone$Listener;

.field private final j:Landroid/telecom/Call$Listener;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 49
    :try_start_0
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "isMuted"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 58
    :goto_0
    :try_start_1
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "getSupportedRouteMask"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_1
    .catch Ljava/lang/NoSuchMethodException; {:try_start_1 .. :try_end_1} :catch_2

    .line 67
    :goto_1
    :try_start_2
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "getRoute"

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;
    :try_end_2
    .catch Ljava/lang/NoSuchMethodException; {:try_start_2 .. :try_end_2} :catch_4

    .line 74
    :goto_2
    return-void

    :catch_0
    move-exception v0

    .line 52
    :try_start_3
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "isMuted"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/dj;->f:Ljava/lang/reflect/Field;
    :try_end_3
    .catch Ljava/lang/NoSuchFieldException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Ljava/lang/SecurityException; {:try_start_3 .. :try_end_3} :catch_8

    goto :goto_0

    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    .line 61
    :try_start_4
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "supportedRouteMask"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/dj;->g:Ljava/lang/reflect/Field;
    :try_end_4
    .catch Ljava/lang/NoSuchFieldException; {:try_start_4 .. :try_end_4} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_4 .. :try_end_4} :catch_7

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1

    :catch_4
    move-exception v0

    .line 70
    :try_start_5
    const-class v0, Landroid/telecom/AudioState;

    const-string v1, "route"

    invoke-virtual {v0, v1}, Ljava/lang/Class;->getField(Ljava/lang/String;)Ljava/lang/reflect/Field;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/dj;->h:Ljava/lang/reflect/Field;
    :try_end_5
    .catch Ljava/lang/NoSuchFieldException; {:try_start_5 .. :try_end_5} :catch_5
    .catch Ljava/lang/SecurityException; {:try_start_5 .. :try_end_5} :catch_6

    goto :goto_2

    .line 74
    :catch_5
    move-exception v0

    goto :goto_2

    :catch_6
    move-exception v0

    goto :goto_2

    :catch_7
    move-exception v0

    goto :goto_1

    :catch_8
    move-exception v0

    goto :goto_0
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v2, 0x3

    .line 76
    invoke-direct {p0}, Lcom/google/android/gms/car/lc;-><init>()V

    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->a:Ljava/lang/Object;

    .line 593
    new-instance v0, Lcom/google/android/gms/car/dl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dl;-><init>(Lcom/google/android/gms/car/dj;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->i:Landroid/telecom/Phone$Listener;

    .line 649
    new-instance v0, Lcom/google/android/gms/car/dm;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dm;-><init>(Lcom/google/android/gms/car/dj;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->j:Landroid/telecom/Call$Listener;

    .line 77
    const-string v0, "CAR.TEL.CarCallService"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 78
    const-string v0, "CAR.TEL.CarCallService"

    const-string v1, "Creating a new CarCallService."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    .line 81
    new-instance v0, Lcom/google/android/gms/car/dk;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/dk;-><init>(Lcom/google/android/gms/car/dj;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->b:Lcom/google/android/gms/car/dq;

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->a(Landroid/content/Context;)Lcom/google/android/gms/car/do;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->b:Lcom/google/android/gms/car/dq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/do;->a(Lcom/google/android/gms/car/dq;)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->a(Landroid/content/Context;)Lcom/google/android/gms/car/do;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/do;->b()Landroid/telecom/Phone;

    move-result-object v0

    .line 98
    const-string v1, "CAR.TEL.CarCallService"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 99
    const-string v1, "CAR.TEL.CarCallService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Starting CarCallService with initial phone "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    :cond_1
    if-eqz v0, :cond_2

    .line 102
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/dj;->a(Landroid/telecom/Phone;)V

    .line 104
    :cond_2
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/dj;)Lcom/google/android/gms/car/dn;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    return-object v0
.end method

.method static synthetic a(Landroid/telecom/AudioState;)Z
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/gms/car/dj;->d(Landroid/telecom/AudioState;)Z

    move-result v0

    return v0
.end method

.method static synthetic b(Landroid/telecom/AudioState;)I
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/gms/car/dj;->f(Landroid/telecom/AudioState;)I

    move-result v0

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/dj;)Landroid/telecom/Call$Listener;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->j:Landroid/telecom/Call$Listener;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Z
    .locals 5

    .prologue
    .line 405
    :try_start_0
    const-class v0, Landroid/telephony/PhoneNumberUtils;

    const-string v1, "isPotentialLocalEmergencyNumber"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    const-class v4, Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    const-class v4, Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    .line 407
    const/4 v1, 0x0

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    move-result v0

    .line 412
    :goto_0
    return v0

    .line 408
    :catch_0
    move-exception v0

    .line 409
    :goto_1
    const-string v1, "CAR.TEL.CarCallService"

    const-string v2, "Failed to call isPotentialLocalEmergencyNumber"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 412
    invoke-static {p1}, Landroid/telephony/PhoneNumberUtils;->isEmergencyNumber(Ljava/lang/String;)Z

    move-result v0

    goto :goto_0

    .line 408
    :catch_1
    move-exception v0

    goto :goto_1

    :catch_2
    move-exception v0

    goto :goto_1
.end method

.method static synthetic c(Landroid/telecom/AudioState;)I
    .locals 1

    .prologue
    .line 33
    invoke-static {p0}, Lcom/google/android/gms/car/dj;->e(Landroid/telecom/AudioState;)I

    move-result v0

    return v0
.end method

.method static synthetic c(Lcom/google/android/gms/car/dj;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/car/dj;)V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/google/android/gms/car/dj;->f()V

    return-void
.end method

.method private static d(Landroid/telecom/AudioState;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 417
    if-nez p0, :cond_0

    .line 423
    :goto_0
    return v0

    .line 421
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/gms/car/dj;->f:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/telecom/AudioState;->isMuted()Z

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/car/dj;->f:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->getBoolean(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 423
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static e(Landroid/telecom/AudioState;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 429
    if-nez p0, :cond_0

    .line 436
    :goto_0
    return v0

    .line 433
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/gms/car/dj;->g:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/telecom/AudioState;->getSupportedRouteMask()I

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/car/dj;->g:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 436
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private static f(Landroid/telecom/AudioState;)I
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 442
    if-nez p0, :cond_0

    .line 448
    :goto_0
    return v0

    .line 446
    :cond_0
    :try_start_0
    sget-object v1, Lcom/google/android/gms/car/dj;->h:Ljava/lang/reflect/Field;

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/telecom/AudioState;->getRoute()I

    move-result v0

    goto :goto_0

    :cond_1
    sget-object v1, Lcom/google/android/gms/car/dj;->h:Ljava/lang/reflect/Field;

    invoke-virtual {v1, p0}, Ljava/lang/reflect/Field;->getInt(Ljava/lang/Object;)I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    goto :goto_0

    .line 448
    :catch_0
    move-exception v1

    goto :goto_0

    :catch_1
    move-exception v1

    goto :goto_0
.end method

.method private f()V
    .locals 3

    .prologue
    .line 304
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v0}, Lcom/google/android/gms/car/le;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 305
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    .line 306
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 144
    const-string v0, "CAR.TEL.CarCallService"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 145
    const-string v0, "CAR.TEL.CarCallService"

    const-string v1, "onDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 147
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/car/do;->a(Landroid/content/Context;)Lcom/google/android/gms/car/do;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->b:Lcom/google/android/gms/car/dq;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/do;->b(Lcom/google/android/gms/car/dq;)V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    if-eqz v0, :cond_3

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    invoke-virtual {v0}, Landroid/telecom/Phone;->getCalls()Ljava/util/List;

    move-result-object v2

    .line 150
    const-string v0, "CAR.TEL.CarCallService"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 151
    const-string v0, "CAR.TEL.CarCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Clearing "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, " call listeners."

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    :cond_1
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 154
    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    iget-object v3, p0, Lcom/google/android/gms/car/dj;->j:Landroid/telecom/Call$Listener;

    invoke-virtual {v0, v3}, Landroid/telecom/Call;->removeListener(Landroid/telecom/Call$Listener;)V

    .line 153
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 156
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->i:Landroid/telecom/Phone$Listener;

    invoke-virtual {v0, v1}, Landroid/telecom/Phone;->removeListener(Landroid/telecom/Phone$Listener;)V

    .line 158
    :cond_3
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    if-nez v0, :cond_0

    .line 234
    const-string v0, "CAR.TEL.CarCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Can\'t set the audio route to "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Phone is null."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    invoke-virtual {v0, p1}, Landroid/telecom/Phone;->setAudioRoute(I)V

    goto :goto_0
.end method

.method final a(Landroid/telecom/Phone;)V
    .locals 4

    .prologue
    const/4 v3, 0x3

    .line 115
    const-string v0, "CAR.TEL.CarCallService"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 116
    const-string v0, "CAR.TEL.CarCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "setPhone: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 120
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    if-eqz v0, :cond_1

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->i:Landroid/telecom/Phone$Listener;

    invoke-virtual {v0, v1}, Landroid/telecom/Phone;->removeListener(Landroid/telecom/Phone$Listener;)V

    .line 122
    invoke-static {}, Lcom/google/android/gms/car/dr;->b()V

    .line 124
    :cond_1
    invoke-static {}, Lcom/google/android/gms/car/dr;->b()V

    .line 125
    iput-object p1, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    .line 126
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    if-eqz v0, :cond_3

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    iget-object v1, p0, Lcom/google/android/gms/car/dj;->i:Landroid/telecom/Phone$Listener;

    invoke-virtual {v0, v1}, Landroid/telecom/Phone;->addListener(Landroid/telecom/Phone$Listener;)V

    .line 128
    const-string v0, "CAR.TEL.CarCallService"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 129
    const-string v0, "CAR.TEL.CarCallService"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Setting a new phone with "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    invoke-virtual {v2}, Landroid/telecom/Phone;->getCalls()Ljava/util/List;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " calls in progress."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 132
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    invoke-virtual {v0}, Landroid/telecom/Phone;->getCalls()Ljava/util/List;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telecom/Call;

    .line 133
    invoke-static {v0}, Lcom/google/android/gms/car/dr;->a(Landroid/telecom/Call;)Lcom/google/android/gms/car/CarCall;

    .line 134
    iget-object v2, p0, Lcom/google/android/gms/car/dj;->j:Landroid/telecom/Call$Listener;

    invoke-virtual {v0, v2}, Landroid/telecom/Call;->addListener(Landroid/telecom/Call$Listener;)V

    goto :goto_0

    .line 137
    :cond_3
    return-void
.end method

.method final a(Landroid/view/KeyEvent;)V
    .locals 4

    .prologue
    .line 161
    iget-object v1, p0, Lcom/google/android/gms/car/dj;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    if-nez v0, :cond_1

    .line 163
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 164
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "no listener to dispatch a media key event"

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    :goto_0
    return-void

    .line 169
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    iget-object v0, v0, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/le;->a(Landroid/view/KeyEvent;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 173
    :goto_1
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 170
    :catch_0
    move-exception v0

    .line 171
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException dispatching phone key event to CarCallListener"

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;)V
    .locals 2

    .prologue
    .line 310
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 311
    if-eqz v0, :cond_0

    .line 312
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/telecom/Call;->answer(I)V

    .line 314
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;C)V
    .locals 1

    .prologue
    .line 350
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 351
    if-eqz v0, :cond_0

    .line 352
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->playDtmfTone(C)V

    .line 354
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Lcom/google/android/gms/car/CarCall;)V
    .locals 2

    .prologue
    .line 374
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 375
    invoke-static {p2}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v1

    .line 376
    if-eqz v0, :cond_0

    if-eqz v1, :cond_0

    .line 377
    invoke-virtual {v0, v1}, Landroid/telecom/Call;->conference(Landroid/telecom/Call;)V

    .line 379
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;Z)V
    .locals 1

    .prologue
    .line 366
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 367
    if-eqz v0, :cond_0

    .line 368
    invoke-virtual {v0, p2}, Landroid/telecom/Call;->postDialContinue(Z)V

    .line 370
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/CarCall;ZLjava/lang/String;)V
    .locals 1

    .prologue
    .line 318
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 319
    if-eqz v0, :cond_0

    .line 320
    invoke-virtual {v0, p2, p3}, Landroid/telecom/Call;->reject(ZLjava/lang/String;)V

    .line 322
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 392
    const-string v0, "tel"

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 393
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/dj;->b(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 394
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL_EMERGENCY"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 398
    :goto_0
    const/high16 v1, 0x10000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 399
    invoke-static {}, Landroid/os/Binder;->clearCallingIdentity()J

    .line 400
    iget-object v1, p0, Lcom/google/android/gms/car/dj;->e:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 401
    return-void

    .line 396
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-string v2, "android.intent.action.CALL"

    invoke-direct {v0, v2, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 1

    .prologue
    .line 198
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    .line 199
    if-eqz v0, :cond_0

    .line 200
    invoke-virtual {v0, p1}, Landroid/telecom/Phone;->setMuted(Z)V

    .line 202
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/le;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x3

    .line 248
    const-string v1, "CAR.TEL.CarCallService"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 249
    const-string v1, "CAR.TEL.CarCallService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "setListener "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 251
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/dj;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 252
    if-nez p1, :cond_2

    .line 253
    :try_start_0
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 254
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "You must specify a CarPhoneListener."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 256
    :cond_1
    monitor-exit v1

    .line 270
    :goto_0
    return v0

    .line 258
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    if-eqz v0, :cond_4

    .line 259
    const-string v0, "CAR.TEL.CarCallService"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 260
    const-string v0, "CAR.TEL.CarCallService"

    const-string v2, "Removing existing CarCallListener."

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/car/dj;->f()V

    .line 264
    :cond_4
    new-instance v0, Lcom/google/android/gms/car/dn;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/dn;-><init>(Lcom/google/android/gms/car/dj;Lcom/google/android/gms/car/le;)V

    iput-object v0, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 266
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/car/le;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 270
    :goto_1
    const/4 v0, 0x1

    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 271
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 267
    :catch_0
    move-exception v0

    .line 268
    :try_start_3
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "RemoteException setting death recipient for CarCallListener."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_1
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 178
    invoke-static {}, Lcom/google/android/gms/car/dr;->a()Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/car/CarCall;)V
    .locals 1

    .prologue
    .line 326
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 327
    if-eqz v0, :cond_0

    .line 328
    invoke-virtual {v0}, Landroid/telecom/Call;->disconnect()V

    .line 330
    :cond_0
    return-void
.end method

.method public final b(Lcom/google/android/gms/car/le;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    const/4 v2, 0x3

    .line 281
    const-string v1, "CAR.TEL.CarCallService"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 282
    const-string v1, "CAR.TEL.CarCallService"

    const-string v2, "removeListener"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 284
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/dj;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 285
    if-nez p1, :cond_2

    .line 286
    :try_start_0
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 287
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "You must specify a CarPhoneListener."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 289
    :cond_1
    monitor-exit v1

    .line 298
    :goto_0
    return v0

    .line 291
    :cond_2
    invoke-interface {p1}, Lcom/google/android/gms/car/le;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/dj;->c:Lcom/google/android/gms/car/dn;

    iget-object v3, v3, Lcom/google/android/gms/car/dn;->a:Lcom/google/android/gms/car/le;

    invoke-interface {v3}, Lcom/google/android/gms/car/le;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 292
    invoke-direct {p0}, Lcom/google/android/gms/car/dj;->f()V

    .line 293
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 300
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 295
    :cond_3
    :try_start_1
    const-string v2, "CAR.TEL.CarCallService"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 296
    const-string v2, "CAR.TEL.CarCallService"

    const-string v3, "Can\'t remove listener. It is not the active listener."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 298
    :cond_4
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/car/CarCall;)V
    .locals 1

    .prologue
    .line 334
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 335
    if-eqz v0, :cond_0

    .line 336
    invoke-virtual {v0}, Landroid/telecom/Call;->hold()V

    .line 338
    :cond_0
    return-void
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 186
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    .line 187
    if-eqz v0, :cond_0

    .line 188
    invoke-virtual {v0}, Landroid/telecom/Phone;->getAudioState()Landroid/telecom/AudioState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->d(Landroid/telecom/AudioState;)Z

    move-result v0

    .line 190
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final d()I
    .locals 1

    .prologue
    .line 209
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    .line 210
    if-nez v0, :cond_0

    .line 211
    const/4 v0, 0x0

    .line 213
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/telecom/Phone;->getAudioState()Landroid/telecom/AudioState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->e(Landroid/telecom/AudioState;)I

    move-result v0

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/car/CarCall;)V
    .locals 1

    .prologue
    .line 342
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 343
    if-eqz v0, :cond_0

    .line 344
    invoke-virtual {v0}, Landroid/telecom/Call;->unhold()V

    .line 346
    :cond_0
    return-void
.end method

.method public final e()I
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/gms/car/dj;->d:Landroid/telecom/Phone;

    .line 222
    if-nez v0, :cond_0

    .line 223
    const/4 v0, 0x0

    .line 225
    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Landroid/telecom/Phone;->getAudioState()Landroid/telecom/AudioState;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/car/dj;->f(Landroid/telecom/AudioState;)I

    move-result v0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/car/CarCall;)V
    .locals 1

    .prologue
    .line 358
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 359
    if-eqz v0, :cond_0

    .line 360
    invoke-virtual {v0}, Landroid/telecom/Call;->stopDtmfTone()V

    .line 362
    :cond_0
    return-void
.end method

.method public final f(Lcom/google/android/gms/car/CarCall;)V
    .locals 1

    .prologue
    .line 383
    invoke-static {p1}, Lcom/google/android/gms/car/dr;->a(Lcom/google/android/gms/car/CarCall;)Landroid/telecom/Call;

    move-result-object v0

    .line 384
    if-eqz v0, :cond_0

    .line 385
    invoke-virtual {v0}, Landroid/telecom/Call;->splitFromConference()V

    .line 387
    :cond_0
    return-void
.end method
