.class public final Lcom/google/android/gms/games/service/statemachine/roomclient/b;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/os/IBinder$DeathRecipient;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

.field private final b:Landroid/os/IBinder;

.field private final c:Ljava/lang/String;

.field private d:Z


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;Ljava/lang/String;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 886
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 884
    iput-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->d:Z

    .line 887
    iput-object p3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->b:Landroid/os/IBinder;

    .line 888
    iput-object p2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->c:Ljava/lang/String;

    .line 890
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->b:Landroid/os/IBinder;

    const/4 v1, 0x0

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V

    .line 891
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->d:Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 898
    :goto_0
    return-void

    .line 892
    :catch_0
    move-exception v0

    .line 893
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "When registering for game death binder"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 896
    iget-object v0, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 901
    iget-boolean v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->d:Z

    if-eqz v0, :cond_0

    .line 902
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->b:Landroid/os/IBinder;

    invoke-interface {v0, p0, v1}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 903
    iput-boolean v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->d:Z

    .line 905
    :cond_0
    return-void
.end method

.method public final binderDied()V
    .locals 3

    .prologue
    .line 909
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "The game process died without disconnecting the games client."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 911
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->c:Ljava/lang/String;

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;-><init>(Ljava/lang/String;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 912
    return-void
.end method
