.class final Lcom/google/android/gms/car/nz;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final b:Ljava/util/concurrent/ConcurrentLinkedQueue;

.field private final c:I

.field private final d:I


# direct methods
.method public constructor <init>(II)V
    .locals 3

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/nz;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 38
    new-instance v0, Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    .line 44
    iput p1, p0, Lcom/google/android/gms/car/nz;->c:I

    .line 45
    iput p2, p0, Lcom/google/android/gms/car/nz;->d:I

    .line 47
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/google/android/gms/car/nz;->d:I

    if-ge v0, v1, :cond_0

    .line 48
    invoke-direct {p0}, Lcom/google/android/gms/car/nz;->d()Lcom/google/android/gms/car/e;

    move-result-object v1

    .line 49
    iget-object v2, p0, Lcom/google/android/gms/car/nz;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 47
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 51
    :cond_0
    return-void
.end method

.method private d()Lcom/google/android/gms/car/e;
    .locals 2

    .prologue
    .line 114
    new-instance v0, Lcom/google/android/gms/car/e;

    iget v1, p0, Lcom/google/android/gms/car/nz;->c:I

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/e;-><init>(I)V

    return-object v0
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/car/e;
    .locals 1

    .prologue
    .line 57
    iget-object v0, p0, Lcom/google/android/gms/car/nz;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    .line 58
    if-nez v0, :cond_0

    .line 59
    invoke-direct {p0}, Lcom/google/android/gms/car/nz;->d()Lcom/google/android/gms/car/e;

    move-result-object v0

    .line 61
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/car/e;)V
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 66
    return-void
.end method

.method public final b()Lcom/google/android/gms/car/e;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/car/e;)V
    .locals 2

    .prologue
    .line 72
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/nz;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->size()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/car/nz;->d:I

    if-ge v0, v1, :cond_0

    .line 73
    const-wide/16 v0, 0x0

    iput-wide v0, p1, Lcom/google/android/gms/car/e;->a:J

    .line 74
    iget-object v0, p1, Lcom/google/android/gms/car/e;->b:Ljava/nio/ByteBuffer;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->clear()Ljava/nio/Buffer;

    .line 75
    iget-object v0, p0, Lcom/google/android/gms/car/nz;->b:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentLinkedQueue;->add(Ljava/lang/Object;)Z

    .line 77
    :cond_0
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/car/nz;->a:Ljava/util/concurrent/ConcurrentLinkedQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/ConcurrentLinkedQueue;->poll()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/e;

    .line 92
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/nz;->b(Lcom/google/android/gms/car/e;)V

    .line 93
    return-void
.end method
