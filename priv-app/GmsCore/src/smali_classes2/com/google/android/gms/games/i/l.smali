.class final Lcom/google/android/gms/games/i/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/f;
.implements Lcom/google/android/gms/games/internal/game/b;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/i/b;

.field private final b:Lcom/google/android/gms/games/internal/game/c;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/common/data/DataHolder;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/games/internal/game/c;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 270
    iput-object p1, p0, Lcom/google/android/gms/games/i/l;->a:Lcom/google/android/gms/games/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 271
    iput-object p2, p0, Lcom/google/android/gms/games/i/l;->b:Lcom/google/android/gms/games/internal/game/c;

    .line 272
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/i/l;->c:Lcom/google/android/gms/common/api/Status;

    .line 273
    iput-object p3, p0, Lcom/google/android/gms/games/i/l;->d:Lcom/google/android/gms/common/data/DataHolder;

    .line 274
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/games/i/l;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 278
    iget-object v0, p0, Lcom/google/android/gms/games/i/l;->b:Lcom/google/android/gms/games/internal/game/c;

    invoke-interface {v0, p0}, Lcom/google/android/gms/games/internal/game/c;->a(Lcom/google/android/gms/games/internal/game/b;)V

    .line 279
    return-void
.end method

.method public final c()Lcom/google/android/gms/common/data/DataHolder;
    .locals 1

    .prologue
    .line 295
    iget-object v0, p0, Lcom/google/android/gms/games/i/l;->d:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 288
    iget-object v0, p0, Lcom/google/android/gms/games/i/l;->d:Lcom/google/android/gms/common/data/DataHolder;

    if-eqz v0, :cond_0

    .line 289
    iget-object v0, p0, Lcom/google/android/gms/games/i/l;->d:Lcom/google/android/gms/common/data/DataHolder;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    .line 291
    :cond_0
    return-void
.end method
