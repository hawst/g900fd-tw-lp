.class public final Lcom/google/android/gms/games/realtime/network/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/realtime/network/b;


# instance fields
.field private final a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/games/realtime/network/a;I)V
    .locals 1

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    new-instance v0, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-direct {v0, p1, p3, p2}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;-><init>(Landroid/content/Context;ILcom/google/android/gms/games/realtime/network/a;)V

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    .line 81
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->d()V

    .line 82
    return-void
.end method


# virtual methods
.method public final a([BLjava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 180
    new-instance v0, Lcom/google/android/gms/games/realtime/network/m;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/realtime/network/m;-><init>([BLjava/lang/String;)V

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 183
    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 187
    :goto_0
    return v0

    .line 185
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 187
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/games/realtime/network/c;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 208
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v2, "JID must not be empty or null"

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 209
    new-instance v0, Lcom/google/android/gms/games/realtime/network/j;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/realtime/network/j;-><init>(Ljava/lang/String;)V

    .line 210
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 212
    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/network/c;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 216
    :goto_1
    return-object v0

    .line 208
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_1

    .line 216
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 250
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/t;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/t;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 251
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/realtime/network/o;)V
    .locals 1

    .prologue
    .line 242
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 243
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/k;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/realtime/network/k;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 93
    return-void
.end method

.method public final a(Ljava/lang/String;ZI)V
    .locals 2

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/f;

    invoke-direct {v1, p1, p2, p3}, Lcom/google/android/gms/games/realtime/network/f;-><init>(Ljava/lang/String;ZI)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 158
    return-void
.end method

.method public final a([B[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 200
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/n;

    invoke-direct {v1, p1, p2}, Lcom/google/android/gms/games/realtime/network/n;-><init>([B[Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 201
    return-void
.end method

.method public final b(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v1, -0x1

    .line 135
    new-instance v0, Lcom/google/android/gms/games/realtime/network/g;

    invoke-direct {v0, p1}, Lcom/google/android/gms/games/realtime/network/g;-><init>(Ljava/lang/String;)V

    .line 136
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 138
    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v0

    .line 142
    :goto_0
    return v0

    .line 140
    :catch_0
    move-exception v0

    move v0, v1

    goto :goto_0

    .line 142
    :catch_1
    move-exception v0

    move v0, v1

    goto :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 100
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/p;

    invoke-direct {v1}, Lcom/google/android/gms/games/realtime/network/p;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 101
    return-void
.end method

.method public final c(Ljava/lang/String;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 115
    new-instance v0, Lcom/google/android/gms/games/realtime/network/h;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/games/realtime/network/h;-><init>(Ljava/lang/String;Lcom/google/android/gms/games/realtime/network/b;)V

    .line 116
    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 118
    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/a;->c:Lcom/google/android/gms/games/service/statemachine/b;

    invoke-virtual {v0}, Ljava/util/concurrent/FutureTask;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 122
    :goto_0
    return-object v0

    .line 120
    :catch_0
    move-exception v0

    move-object v0, v1

    goto :goto_0

    .line 122
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 234
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/q;

    invoke-direct {v1}, Lcom/google/android/gms/games/realtime/network/q;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 235
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/i;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/realtime/network/i;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 168
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 226
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/w;->a:Lcom/google/android/gms/games/realtime/network/DcmStateMachine;

    new-instance v1, Lcom/google/android/gms/games/realtime/network/l;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/realtime/network/l;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/realtime/network/DcmStateMachine;->a(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 227
    return-void
.end method
