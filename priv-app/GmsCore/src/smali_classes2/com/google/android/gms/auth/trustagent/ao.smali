.class final Lcom/google/android/gms/auth/trustagent/ao;
.super Landroid/os/AsyncTask;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/am;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/am;)V
    .locals 0

    .prologue
    .line 153
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method

.method private varargs a()Landroid/os/Bundle;
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 157
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v2, v1, Lcom/google/android/gms/auth/trustagent/am;->g:Ljava/lang/Object;

    monitor-enter v2
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 158
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    if-nez v1, :cond_0

    .line 159
    const-string v1, "Coffee - PreferenceServiceClient"

    const-string v3, "Failed to retrieve preferences, preference service is null."

    invoke-static {v1, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 161
    monitor-exit v2

    .line 168
    :goto_0
    return-object v0

    .line 163
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v1, v1, Lcom/google/android/gms/auth/trustagent/am;->f:Lcom/google/android/gms/auth/trustagent/q;

    iget-object v3, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v3, v3, Lcom/google/android/gms/auth/trustagent/am;->b:Ljava/lang/String;

    invoke-interface {v1, v3}, Lcom/google/android/gms/auth/trustagent/q;->a(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-object v0, v1

    goto :goto_0

    .line 164
    :catchall_0
    move-exception v1

    :try_start_2
    monitor-exit v2

    throw v1
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0

    .line 165
    :catch_0
    move-exception v1

    .line 166
    const-string v2, "Coffee - PreferenceServiceClient"

    const-string v3, "Failed to retrieve preferences."

    invoke-static {v2, v3, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method


# virtual methods
.method protected final synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 153
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/ao;->a()Landroid/os/Bundle;

    move-result-object v0

    return-object v0
.end method

.method protected final synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 153
    check-cast p1, Landroid/os/Bundle;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/auth/trustagent/am;->d:Landroid/os/AsyncTask;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iput-object p1, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/ao;->a:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->c:Lcom/google/android/gms/auth/trustagent/aq;

    invoke-interface {v0}, Lcom/google/android/gms/auth/trustagent/aq;->a()V

    :cond_0
    return-void
.end method
