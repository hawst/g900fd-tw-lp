.class public final Lcom/google/android/gms/icing/e;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:J

.field public c:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const-wide/16 v0, 0x0

    .line 9030
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 9031
    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->a:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->b:J

    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->c:J

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/e;->cachedSize:I

    .line 9032
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 9092
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 9093
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->a:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 9094
    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->a:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9097
    :cond_0
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->b:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    .line 9098
    const/4 v1, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->b:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->e(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9101
    :cond_1
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->c:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_2

    .line 9102
    const/4 v1, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->c:J

    invoke-static {v1, v2, v3}, Lcom/google/protobuf/nano/b;->f(IJ)I

    move-result v1

    add-int/2addr v0, v1

    .line 9105
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 9044
    if-ne p1, p0, :cond_1

    .line 9060
    :cond_0
    :goto_0
    return v0

    .line 9047
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/e;

    if-nez v2, :cond_2

    move v0, v1

    .line 9048
    goto :goto_0

    .line 9050
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/e;

    .line 9051
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->a:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/e;->a:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_3

    move v0, v1

    .line 9052
    goto :goto_0

    .line 9054
    :cond_3
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->b:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/e;->b:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_4

    move v0, v1

    .line 9055
    goto :goto_0

    .line 9057
    :cond_4
    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->c:J

    iget-wide v4, p1, Lcom/google/android/gms/icing/e;->c:J

    cmp-long v2, v2, v4

    if-eqz v2, :cond_0

    move v0, v1

    .line 9058
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 7

    .prologue
    const/16 v6, 0x20

    .line 9065
    iget-wide v0, p0, Lcom/google/android/gms/icing/e;->a:J

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->a:J

    ushr-long/2addr v2, v6

    xor-long/2addr v0, v2

    long-to-int v0, v0

    add-int/lit16 v0, v0, 0x20f

    .line 9068
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->b:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/e;->b:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 9070
    mul-int/lit8 v0, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->c:J

    iget-wide v4, p0, Lcom/google/android/gms/icing/e;->c:J

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v1, v2

    add-int/2addr v0, v1

    .line 9072
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 9004
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->a:J

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->b:J

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->j()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/icing/e;->c:J

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 9078
    iget-wide v0, p0, Lcom/google/android/gms/icing/e;->a:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_0

    .line 9079
    const/4 v0, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->a:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 9081
    :cond_0
    iget-wide v0, p0, Lcom/google/android/gms/icing/e;->b:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_1

    .line 9082
    const/4 v0, 0x2

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->b:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->a(IJ)V

    .line 9084
    :cond_1
    iget-wide v0, p0, Lcom/google/android/gms/icing/e;->c:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_2

    .line 9085
    const/4 v0, 0x3

    iget-wide v2, p0, Lcom/google/android/gms/icing/e;->c:J

    invoke-virtual {p1, v0, v2, v3}, Lcom/google/protobuf/nano/b;->b(IJ)V

    .line 9087
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 9088
    return-void
.end method
