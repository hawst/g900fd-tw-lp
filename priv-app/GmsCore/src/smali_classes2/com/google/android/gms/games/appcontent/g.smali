.class public final Lcom/google/android/gms/games/appcontent/g;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/ArrayList;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 8

    .prologue
    .line 39
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 40
    new-instance v5, Lcom/google/android/gms/games/appcontent/h;

    invoke-direct {v5, v7}, Lcom/google/android/gms/games/appcontent/h;-><init>(Ljava/util/ArrayList;)V

    .line 45
    const/4 v1, 0x1

    const-string v3, "action_id"

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/appcontent/g;->a(Lcom/google/android/gms/common/data/DataHolder;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/appcontent/l;Ljava/util/ArrayList;)V

    .line 48
    return-object v7
.end method

.method static a(Lcom/google/android/gms/common/data/DataHolder;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/appcontent/l;Ljava/util/ArrayList;)V
    .locals 6

    .prologue
    .line 137
    invoke-virtual {p6, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 139
    invoke-virtual {p0, p4}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v1

    invoke-virtual {p0, p2, p4, v1}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v1

    .line 140
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 141
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v2

    .line 142
    const-string v3, ","

    invoke-virtual {v1, v3}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v3

    .line 145
    const/4 v1, 0x0

    :goto_0
    if-ge v1, v2, :cond_1

    .line 146
    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v4

    invoke-virtual {v0, p3, v1, v4}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v4

    .line 147
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    invoke-static {v3, v4}, Lcom/google/android/gms/common/util/h;->a([Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 148
    invoke-interface {p5, p6, v1}, Lcom/google/android/gms/games/appcontent/l;->a(Ljava/util/ArrayList;I)V

    .line 145
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 152
    :cond_1
    return-void
.end method

.method public static b(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/ArrayList;Ljava/lang/String;I)Ljava/util/ArrayList;
    .locals 8

    .prologue
    .line 87
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 88
    new-instance v5, Lcom/google/android/gms/games/appcontent/j;

    invoke-direct {v5, v7}, Lcom/google/android/gms/games/appcontent/j;-><init>(Ljava/util/ArrayList;)V

    .line 93
    const/4 v1, 0x4

    const-string v3, "condition_id"

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/appcontent/g;->a(Lcom/google/android/gms/common/data/DataHolder;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/appcontent/l;Ljava/util/ArrayList;)V

    .line 96
    return-object v7
.end method

.method public static c(Lcom/google/android/gms/common/data/DataHolder;Ljava/util/ArrayList;Ljava/lang/String;I)Landroid/os/Bundle;
    .locals 8

    .prologue
    const/4 v1, 0x3

    .line 110
    new-instance v7, Landroid/os/Bundle;

    invoke-direct {v7}, Landroid/os/Bundle;-><init>()V

    .line 111
    invoke-virtual {p1, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 112
    new-instance v5, Lcom/google/android/gms/games/appcontent/k;

    invoke-direct {v5, v0, v7}, Lcom/google/android/gms/games/appcontent/k;-><init>(Lcom/google/android/gms/common/data/DataHolder;Landroid/os/Bundle;)V

    .line 118
    const-string v3, "tuple_id"

    move-object v0, p0

    move-object v2, p2

    move v4, p3

    move-object v6, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/gms/games/appcontent/g;->a(Lcom/google/android/gms/common/data/DataHolder;ILjava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/appcontent/l;Ljava/util/ArrayList;)V

    .line 120
    return-object v7
.end method
