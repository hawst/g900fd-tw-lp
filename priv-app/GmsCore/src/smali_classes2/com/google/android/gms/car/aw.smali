.class public final Lcom/google/android/gms/car/aw;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/u;


# instance fields
.field final a:[Lcom/google/android/gms/car/g;

.field final b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

.field c:I

.field volatile d:I

.field e:Lcom/google/android/gms/car/bb;

.field final f:Landroid/os/HandlerThread;

.field final g:Ljava/lang/Object;

.field volatile h:[I

.field volatile i:Z

.field j:I

.field k:I

.field l:I

.field m:J

.field final n:Ljava/lang/Runnable;

.field private final o:Landroid/media/AudioManager;

.field private final p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private final q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

.field private r:Lcom/google/android/gms/car/senderprotocol/q;

.field private s:Z

.field private final t:Landroid/os/HandlerThread;

.field private final u:Ljava/util/LinkedList;

.field private final v:Landroid/telephony/TelephonyManager;

.field private final w:Landroid/telephony/PhoneStateListener;

.field private x:Z

.field private y:I


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4

    .prologue
    const/4 v3, -0x1

    const/4 v2, 0x3

    const/4 v1, 0x0

    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    new-instance v0, Lcom/google/android/gms/car/ax;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ax;-><init>(Lcom/google/android/gms/car/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 45
    new-instance v0, Lcom/google/android/gms/car/ay;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ay;-><init>(Lcom/google/android/gms/car/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 53
    new-array v0, v2, [Lcom/google/android/gms/car/g;

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    .line 55
    new-array v0, v2, [Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 59
    iput v3, p0, Lcom/google/android/gms/car/aw;->c:I

    .line 60
    iput v3, p0, Lcom/google/android/gms/car/aw;->d:I

    .line 62
    iput-boolean v1, p0, Lcom/google/android/gms/car/aw;->s:Z

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    .line 73
    new-array v0, v2, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->h:[I

    .line 76
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    .line 80
    iput-boolean v1, p0, Lcom/google/android/gms/car/aw;->i:Z

    .line 81
    new-instance v0, Lcom/google/android/gms/car/az;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/az;-><init>(Lcom/google/android/gms/car/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->w:Landroid/telephony/PhoneStateListener;

    .line 100
    iput v1, p0, Lcom/google/android/gms/car/aw;->j:I

    .line 101
    iput v1, p0, Lcom/google/android/gms/car/aw;->k:I

    .line 103
    iput v1, p0, Lcom/google/android/gms/car/aw;->l:I

    .line 104
    const-wide/16 v2, -0x1

    iput-wide v2, p0, Lcom/google/android/gms/car/aw;->m:J

    .line 109
    iput-boolean v1, p0, Lcom/google/android/gms/car/aw;->x:Z

    .line 916
    new-instance v0, Lcom/google/android/gms/car/ba;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ba;-><init>(Lcom/google/android/gms/car/aw;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->n:Ljava/lang/Runnable;

    .line 115
    const-string v0, "audio"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    .line 116
    const-string v0, "phone"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/telephony/TelephonyManager;

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->v:Landroid/telephony/TelephonyManager;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->v:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/aw;->i:Z

    .line 119
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FOCUS_HANDLER"

    const/16 v2, -0x13

    invoke-direct {v0, v1, v2}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->t:Landroid/os/HandlerThread;

    .line 121
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "FOCUS_RELEASE"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->f:Landroid/os/HandlerThread;

    .line 122
    return-void

    :cond_0
    move v0, v1

    .line 118
    goto :goto_0

    .line 73
    nop

    :array_0
    .array-data 4
        0x0
        0x0
        0x0
    .end array-data
.end method

.method static synthetic a(Lcom/google/android/gms/car/aw;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->f()V

    return-void
.end method

.method private static a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 0

    .prologue
    .line 764
    invoke-virtual {p1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->j()V

    .line 765
    invoke-virtual {p1, p0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    .line 766
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/g;->b(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    .line 767
    return-void
.end method

.method private a(ZZ)V
    .locals 12

    .prologue
    .line 643
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/aw;->d(I)Z

    move-result v5

    .line 644
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/aw;->d(I)Z

    move-result v6

    .line 645
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v1, 0x0

    aget-object v7, v0, v1

    .line 647
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v1, 0x1

    aget-object v8, v0, v1

    .line 649
    const/4 v3, 0x0

    .line 650
    const/4 v2, 0x0

    .line 651
    const/4 v0, 0x0

    .line 652
    if-eqz v5, :cond_1a

    if-eqz v7, :cond_1a

    .line 653
    invoke-virtual {v7}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v1

    .line 654
    if-eqz v1, :cond_19

    .line 655
    iget v0, v1, Lcom/google/android/gms/car/g;->b:I

    const/4 v3, 0x3

    if-eq v0, v3, :cond_3

    const/4 v0, 0x1

    :goto_0
    move-object v3, v1

    move v4, v0

    .line 659
    :goto_1
    const/4 v0, 0x0

    .line 660
    if-eqz v6, :cond_18

    if-eqz v8, :cond_18

    .line 661
    invoke-virtual {v8}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v1

    .line 662
    if-eqz v1, :cond_17

    .line 663
    iget v0, v1, Lcom/google/android/gms/car/g;->b:I

    const/4 v2, 0x5

    if-eq v0, v2, :cond_4

    const/4 v0, 0x1

    :goto_2
    move-object v11, v1

    move v1, v0

    move-object v0, v11

    .line 666
    :goto_3
    if-eqz p1, :cond_5

    .line 667
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/aw;->m(I)V

    .line 671
    :goto_4
    if-eqz p2, :cond_6

    .line 672
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/aw;->m(I)V

    .line 676
    :goto_5
    const-string v2, "CAR.AUDIO"

    const/4 v9, 0x3

    invoke-static {v2, v9}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 677
    const-string v2, "CAR.AUDIO"

    new-instance v9, Ljava/lang/StringBuilder;

    const-string v10, "handleStreamFocusChange old state, media:"

    invoke-direct {v9, v10}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v9, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v9, " tts:"

    invoke-virtual {v5, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " new state media:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " tts:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 681
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    const/4 v5, 0x0

    aget-object v2, v2, v5

    .line 682
    iget-object v5, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    const/4 v6, 0x1

    aget-object v5, v5, v6

    .line 683
    if-eqz p1, :cond_11

    .line 684
    if-eqz p2, :cond_b

    .line 685
    if-eqz v3, :cond_7

    if-eqz v0, :cond_7

    .line 686
    if-eqz v4, :cond_1

    .line 687
    const-string v1, "CAR.AUDIO"

    const-string v2, "media - tts channel swapped fully"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 688
    invoke-virtual {v3}, Lcom/google/android/gms/car/g;->b()V

    .line 689
    invoke-static {v0, v7}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    .line 756
    :cond_1
    :goto_6
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 757
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->i()V

    .line 759
    :cond_2
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->k()V

    .line 760
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->f()V

    .line 761
    return-void

    .line 655
    :cond_3
    const/4 v0, 0x0

    goto/16 :goto_0

    .line 663
    :cond_4
    const/4 v0, 0x0

    goto/16 :goto_2

    .line 669
    :cond_5
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/aw;->n(I)V

    goto :goto_4

    .line 674
    :cond_6
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/aw;->n(I)V

    goto :goto_5

    .line 692
    :cond_7
    if-eqz v0, :cond_9

    if-eqz v2, :cond_9

    if-ne v2, v0, :cond_9

    .line 695
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 696
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch media from tts to media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 698
    :cond_8
    invoke-static {v2, v7}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto :goto_6

    .line 699
    :cond_9
    if-eqz v3, :cond_1

    if-eqz v5, :cond_1

    if-ne v5, v3, :cond_1

    .line 702
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 703
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch tts from media to tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 705
    :cond_a
    invoke-static {v5, v8}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto :goto_6

    .line 709
    :cond_b
    if-eqz v1, :cond_d

    if-eqz v2, :cond_d

    if-ne v2, v0, :cond_d

    .line 712
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 713
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch media from tts to media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 715
    :cond_c
    invoke-static {v2, v7}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto :goto_6

    .line 716
    :cond_d
    if-eqz v2, :cond_e

    if-eq v2, v3, :cond_1

    .line 719
    :cond_e
    if-eqz v4, :cond_f

    if-eqz v5, :cond_f

    if-eq v5, v3, :cond_1

    .line 722
    :cond_f
    if-eqz v5, :cond_1

    if-ne v5, v0, :cond_1

    .line 725
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 726
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch tts from tts to media"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 728
    :cond_10
    invoke-static {v5, v7}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto/16 :goto_6

    .line 733
    :cond_11
    if-eqz p2, :cond_1

    .line 734
    if-eqz v4, :cond_13

    if-eqz v5, :cond_13

    if-ne v5, v3, :cond_13

    .line 736
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 737
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch tts from media to tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 739
    :cond_12
    invoke-static {v5, v8}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto/16 :goto_6

    .line 740
    :cond_13
    if-eqz v5, :cond_14

    if-eq v5, v0, :cond_1

    .line 743
    :cond_14
    if-eqz v1, :cond_15

    if-eqz v2, :cond_15

    if-eq v2, v0, :cond_1

    .line 746
    :cond_15
    if-eqz v2, :cond_1

    if-ne v2, v3, :cond_1

    .line 749
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_16

    .line 750
    const-string v0, "CAR.AUDIO"

    const-string v1, "switch media from media to tts"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 752
    :cond_16
    invoke-static {v2, v8}, Lcom/google/android/gms/car/aw;->a(Lcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto/16 :goto_6

    :cond_17
    move-object v0, v1

    move v1, v2

    goto/16 :goto_3

    :cond_18
    move v1, v2

    goto/16 :goto_3

    :cond_19
    move v4, v3

    move-object v3, v1

    goto/16 :goto_1

    :cond_1a
    move v4, v3

    move-object v3, v0

    goto/16 :goto_1
.end method

.method private b(IZ)V
    .locals 3

    .prologue
    .line 354
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 355
    if-eqz p2, :cond_0

    .line 356
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v2, v0, p1

    or-int/lit8 v2, v2, 0x2

    aput v2, v0, p1

    .line 360
    :goto_0
    monitor-exit v1

    return-void

    .line 358
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v2, v0, p1

    and-int/lit8 v2, v2, -0x3

    aput v2, v0, p1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 360
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private f()V
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 181
    .line 183
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 184
    :goto_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 185
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->getFirst()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 186
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/aw;->i(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 187
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    iget-object v4, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v4, v4, v3

    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    .line 226
    :goto_1
    if-eqz v0, :cond_5

    .line 227
    iget-object v4, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    invoke-virtual {v4}, Ljava/util/LinkedList;->removeFirst()Ljava/lang/Object;

    .line 228
    iget-object v4, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v3, v4, v3

    invoke-virtual {v3, v0}, Lcom/google/android/gms/car/g;->a(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 235
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 192
    :cond_0
    if-nez v3, :cond_1

    const/4 v0, 0x1

    .line 193
    :goto_2
    :try_start_1
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->k(I)Z

    move-result v4

    if-eqz v4, :cond_7

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->i(I)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 195
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/aw;->k(I)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 196
    iget-object v4, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v4, v4, v3

    invoke-virtual {v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v4

    .line 198
    if-nez v4, :cond_2

    .line 199
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    iget-object v4, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v4, v4, v3

    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 201
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    goto :goto_1

    .line 192
    :cond_1
    const/4 v0, 0x0

    goto :goto_2

    .line 203
    :cond_2
    iget v5, v4, Lcom/google/android/gms/car/g;->b:I

    invoke-static {v5}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v5

    if-ne v5, v0, :cond_7

    .line 207
    iget-object v5, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v5, v5, v0

    invoke-virtual {v5, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v5

    if-eqz v5, :cond_3

    .line 209
    iget-object v5, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v5, v0

    invoke-virtual {v4, v0}, Lcom/google/android/gms/car/g;->b(Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    .line 213
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    iget-object v4, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v4, v4, v3

    invoke-virtual {v0, v4}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v0

    if-eqz v0, :cond_7

    .line 215
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, v3

    goto :goto_1

    .line 219
    :cond_4
    iget-object v4, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v4, v4, v0

    iget-object v5, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v5, v5, v3

    invoke-virtual {v4, v5}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 221
    iget-object v4, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v4, v0

    goto/16 :goto_1

    .line 235
    :cond_5
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 236
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 237
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->i()V

    .line 239
    :cond_6
    return-void

    :cond_7
    move-object v0, v1

    goto/16 :goto_1
.end method

.method private declared-synchronized g()V
    .locals 3

    .prologue
    .line 495
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/aw;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-eqz v0, :cond_1

    .line 508
    :cond_0
    :goto_0
    monitor-exit p0

    return-void

    .line 498
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/aw;->s:Z

    .line 499
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 500
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->t:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 501
    new-instance v0, Lcom/google/android/gms/car/bb;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->t:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/car/bb;-><init>(Lcom/google/android/gms/car/aw;Landroid/os/Looper;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    .line 502
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->d(Lcom/google/android/gms/car/bb;)V

    .line 504
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->v:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->w:Landroid/telephony/PhoneStateListener;

    const/16 v2, 0x20

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 505
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 506
    const-string v0, "start"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/aw;->a(Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 495
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private declared-synchronized h()V
    .locals 2

    .prologue
    .line 571
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/aw;->s:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 584
    :goto_0
    monitor-exit p0

    return-void

    .line 574
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/aw;->s:Z

    .line 575
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 576
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 577
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->n(I)V

    .line 578
    const/4 v0, 0x5

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->n(I)V

    .line 579
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->n(I)V

    .line 580
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->k()V

    .line 581
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->f(Lcom/google/android/gms/car/bb;)V

    .line 582
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->t:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 583
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->f:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 571
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private h(I)Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 300
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/aw;->k(I)Z

    move-result v1

    if-nez v1, :cond_1

    .line 305
    :cond_0
    :goto_0
    return v0

    .line 302
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v1, v1, p1

    invoke-virtual {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m()Z

    move-result v1

    if-nez v1, :cond_0

    .line 305
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private i()V
    .locals 6

    .prologue
    .line 770
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    .line 772
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    const/4 v2, 0x1

    aget-object v2, v1, v2

    .line 774
    if-eqz v0, :cond_1

    .line 777
    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v0

    .line 778
    if-eqz v0, :cond_0

    .line 779
    iget v0, v0, Lcom/google/android/gms/car/g;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    move-object v1, v0

    .line 786
    :goto_0
    if-eqz v2, :cond_3

    .line 789
    invoke-virtual {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->h()Lcom/google/android/gms/car/g;

    move-result-object v0

    .line 790
    if-eqz v0, :cond_2

    .line 791
    iget v0, v0, Lcom/google/android/gms/car/g;->b:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    .line 798
    :goto_1
    const/4 v2, 0x2

    const-string v3, "CAR.AUDIO"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "media client "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v4, " tts client "

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v3, v0}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 799
    return-void

    .line 781
    :cond_0
    const-string v0, "none"

    move-object v1, v0

    goto :goto_0

    .line 784
    :cond_1
    const-string v0, "none"

    move-object v1, v0

    goto :goto_0

    .line 793
    :cond_2
    const-string v0, "none"

    goto :goto_1

    .line 796
    :cond_3
    const-string v0, "none"

    goto :goto_1
.end method

.method private i(I)Z
    .locals 1

    .prologue
    .line 309
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/aw;->k(I)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v0, p1

    invoke-virtual {v0}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->g()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 311
    const/4 v0, 0x1

    .line 313
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private j()V
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 821
    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-eq v0, v2, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    const/4 v1, -0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    const/4 v1, -0x3

    if-ne v0, v1, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->e()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 825
    :cond_1
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 826
    const-string v0, "CAR.AUDIO"

    const-string v1, "releasing android audio focus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 828
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 829
    iput v2, p0, Lcom/google/android/gms/car/aw;->d:I

    .line 831
    :cond_3
    return-void
.end method

.method private j(I)Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v0, v0, p1

    and-int/lit8 v0, v0, 0x2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private k()V
    .locals 2

    .prologue
    .line 859
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x3

    if-ge v0, v1, :cond_1

    .line 860
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->k(I)Z

    move-result v1

    if-nez v1, :cond_0

    .line 861
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v1, v1, v0

    .line 862
    if-eqz v1, :cond_0

    .line 863
    invoke-virtual {v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->e()V

    .line 859
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 867
    :cond_1
    return-void
.end method

.method private k(I)Z
    .locals 2

    .prologue
    .line 373
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v0, v0, p1

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private l(I)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/4 v3, 0x1

    .line 808
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 809
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "request android audio focus:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 811
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v4, p1}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 813
    if-ne v0, v3, :cond_1

    .line 814
    iput v3, p0, Lcom/google/android/gms/car/aw;->d:I

    .line 816
    :cond_1
    return-void
.end method

.method private m(I)V
    .locals 5

    .prologue
    .line 834
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 835
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->j(I)Z

    move-result v1

    .line 836
    const-string v2, "CAR.AUDIO"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 837
    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "enableStream, stream:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " oldstate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 839
    :cond_0
    if-eqz v1, :cond_1

    .line 843
    :goto_0
    return-void

    .line 842
    :cond_1
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/aw;->b(IZ)V

    goto :goto_0
.end method

.method private n(I)V
    .locals 5

    .prologue
    .line 846
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 847
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->j(I)Z

    move-result v1

    .line 848
    const-string v2, "CAR.AUDIO"

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 849
    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "disableStream, stream:"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " oldstate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 851
    :cond_0
    if-nez v1, :cond_1

    .line 856
    :goto_0
    return-void

    .line 854
    :cond_1
    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/aw;->b(IZ)V

    goto :goto_0
.end method

.method private o(I)V
    .locals 1

    .prologue
    .line 1030
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->r:Lcom/google/android/gms/car/senderprotocol/q;

    if-eqz v0, :cond_0

    .line 1031
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0, p1}, Lcom/google/android/gms/car/bb;->c(Lcom/google/android/gms/car/bb;I)V

    .line 1032
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->r:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/senderprotocol/q;->d(I)V

    .line 1034
    :cond_0
    return-void
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 3

    .prologue
    .line 131
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->h()V

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->v:Landroid/telephony/TelephonyManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->w:Landroid/telephony/PhoneStateListener;

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/telephony/TelephonyManager;->listen(Landroid/telephony/PhoneStateListener;I)V

    .line 133
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/aw;->r:Lcom/google/android/gms/car/senderprotocol/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 134
    monitor-exit p0

    return-void

    .line 131
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 138
    monitor-enter p0

    .line 139
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/aw;->s:Z

    if-nez v0, :cond_1

    .line 142
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "focus change from car while focus handling is not started. This will be ignored "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 146
    :cond_0
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 151
    :goto_0
    return-void

    .line 148
    :cond_1
    monitor-exit p0

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->b(Lcom/google/android/gms/car/bb;)V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    const/4 v1, 0x0

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/car/bb;->a(Lcom/google/android/gms/car/bb;IZ)V

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(IJ)V
    .locals 8

    .prologue
    const/4 v0, 0x1

    .line 874
    const-string v1, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 875
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "waitForMediaStreamToStop "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 877
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v1

    .line 878
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v2, v2, v1

    .line 879
    if-eqz v2, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v1, v3, v1

    and-int/lit8 v1, v1, 0x1

    if-ne v1, v0, :cond_1

    :goto_0
    if-eqz v0, :cond_2

    .line 880
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    move-wide v0, p2

    .line 882
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->m()Z

    move-result v3

    if-nez v3, :cond_2

    const-wide/16 v6, 0x0

    cmp-long v3, v0, v6

    if-lez v3, :cond_2

    .line 883
    invoke-virtual {v2, v0, v1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(J)V

    .line 884
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    sub-long/2addr v0, v4

    sub-long v0, p2, v0

    goto :goto_1

    .line 879
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 887
    :cond_2
    return-void
.end method

.method public final declared-synchronized a(ILcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V
    .locals 1

    .prologue
    .line 484
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aput-object p2, v0, p1

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aput-object p3, v0, p1

    .line 486
    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->g()V

    .line 487
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->m(I)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    monitor-exit p0

    return-void

    .line 484
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method final a(IZ)V
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x3

    const/4 v1, 0x0

    const/4 v2, 0x1

    .line 420
    iget-object v3, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v3

    .line 421
    :try_start_0
    iget-boolean v4, p0, Lcom/google/android/gms/car/aw;->x:Z

    .line 422
    iget v5, p0, Lcom/google/android/gms/car/aw;->y:I

    .line 423
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/aw;->x:Z

    .line 424
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 428
    if-eqz p2, :cond_4

    .line 429
    iget p1, p0, Lcom/google/android/gms/car/aw;->c:I

    .line 437
    :cond_0
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 438
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "audio focus change from car "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " forced handling "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/aw;->a(Ljava/lang/String;)V

    .line 443
    :cond_1
    if-nez p2, :cond_2

    if-nez v4, :cond_8

    :cond_2
    move v0, v2

    .line 444
    :goto_0
    packed-switch p1, :pswitch_data_0

    .line 467
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown focus state from car "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 479
    :cond_3
    :goto_1
    return-void

    .line 424
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 430
    :cond_4
    iget v0, p0, Lcom/google/android/gms/car/aw;->c:I

    if-eq v0, p1, :cond_5

    move v0, v2

    :goto_2
    if-nez v0, :cond_0

    .line 431
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 432
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "focus change from car ignored as there is no change necessary "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 430
    :cond_5
    packed-switch p1, :pswitch_data_1

    :cond_6
    move v0, v1

    goto :goto_2

    :pswitch_0
    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-eq v0, v2, :cond_7

    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-ne v0, v7, :cond_6

    :cond_7
    move v0, v2

    goto :goto_2

    :pswitch_1
    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-eq v0, v2, :cond_6

    move v0, v2

    goto :goto_2

    :pswitch_2
    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-eq v0, v6, :cond_6

    move v0, v2

    goto :goto_2

    :pswitch_3
    iget v0, p0, Lcom/google/android/gms/car/aw;->d:I

    if-eq v0, v7, :cond_6

    move v0, v2

    goto :goto_2

    :cond_8
    move v0, v1

    .line 443
    goto :goto_0

    .line 446
    :pswitch_4
    invoke-direct {p0, v2, v2}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->j()V

    .line 470
    :cond_9
    :goto_3
    if-nez p2, :cond_3

    .line 471
    iput p1, p0, Lcom/google/android/gms/car/aw;->c:I

    .line 472
    if-eqz v4, :cond_3

    .line 473
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 474
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "handling pending focus request:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 476
    :cond_a
    invoke-virtual {p0, v5}, Lcom/google/android/gms/car/aw;->g(I)V

    goto :goto_1

    .line 449
    :pswitch_5
    invoke-direct {p0, v2, v2}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->j()V

    goto :goto_3

    .line 452
    :pswitch_6
    invoke-direct {p0, v1, v1}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/aw;->l(I)V

    goto :goto_3

    .line 455
    :pswitch_7
    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0, v6}, Lcom/google/android/gms/car/aw;->l(I)V

    goto :goto_3

    .line 458
    :pswitch_8
    invoke-direct {p0, v1, v1}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0, v7}, Lcom/google/android/gms/car/aw;->l(I)V

    goto :goto_3

    .line 461
    :pswitch_9
    invoke-direct {p0, v2, v1}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->j()V

    goto :goto_3

    .line 464
    :pswitch_a
    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/car/aw;->a(ZZ)V

    iget v1, p0, Lcom/google/android/gms/car/aw;->d:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_b

    const-string v1, "CAR.AUDIO"

    invoke-static {v1, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "CAR.AUDIO"

    const-string v2, "Car gave transient guidance only for permanent focus request"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    if-eqz v0, :cond_9

    invoke-direct {p0}, Lcom/google/android/gms/car/aw;->j()V

    goto :goto_3

    .line 444
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch

    .line 430
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/car/senderprotocol/q;)V
    .locals 1

    .prologue
    .line 126
    monitor-enter p0

    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/aw;->r:Lcom/google/android/gms/car/senderprotocol/q;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 127
    monitor-exit p0

    return-void

    .line 126
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 6

    .prologue
    .line 1219
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "car focus state:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/aw;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " android focus state:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/aw;->d:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1221
    const-string v0, " per channel focus states:"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1222
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->h:[I

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget v3, v1, v0

    .line 1223
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, " "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v3}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 1222
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1225
    :cond_0
    const-string v0, " "

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1226
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 1227
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "num focus response fails:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/aw;->l:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " last focus response fail time:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-wide v4, p0, Lcom/google/android/gms/car/aw;->m:J

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " while now is:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 1230
    return-void
.end method

.method final a(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 1051
    const/4 v0, 0x3

    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ": mFocusStateFromCar "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/aw;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", mAndroidAudioManagerFocusState "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/aw;->d:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Landroid/util/Log;->println(ILjava/lang/String;Ljava/lang/String;)I

    .line 1053
    return-void
.end method

.method public final a(J)Z
    .locals 7

    .prologue
    const-wide/16 v4, 0xa

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 267
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->b()Z

    move-result v2

    if-nez v2, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v0

    .line 269
    :cond_1
    const-wide/16 v2, 0x0

    cmp-long v2, p1, v2

    if-gtz v2, :cond_2

    move v0, v1

    .line 270
    goto :goto_0

    .line 272
    :cond_2
    div-long v2, p1, v4

    long-to-int v3, v2

    move v2, v1

    .line 274
    :goto_1
    if-ge v2, v3, :cond_3

    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->b()Z

    move-result v4

    if-eqz v4, :cond_3

    .line 277
    const-wide/16 v4, 0xa

    :try_start_0
    invoke-static {v4, v5}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 281
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 284
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->b()Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :catch_0
    move-exception v4

    goto :goto_2
.end method

.method public final b(I)Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 160
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 161
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 162
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->i(I)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 163
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v2, v2, v0

    iget-object v3, p0, Lcom/google/android/gms/car/aw;->a:[Lcom/google/android/gms/car/g;

    aget-object v3, v3, v0

    invoke-virtual {v2, v3}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Lcom/google/android/gms/car/g;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 164
    const-string v2, "CAR.AUDIO"

    const/4 v3, 0x3

    invoke-static {v2, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 165
    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "requestBottomHalfForStream grant to stream "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 167
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->b:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v0, v2, v0

    monitor-exit v1

    .line 177
    :goto_0
    return-object v0

    .line 170
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 171
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 172
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 173
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "requestBottomHalfForStream no stream available for stream "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->c(Lcom/google/android/gms/car/bb;)V

    .line 177
    const/4 v0, 0x0

    goto :goto_0

    .line 171
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final b()Z
    .locals 8

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 288
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v2

    .line 289
    const/4 v3, 0x1

    :try_start_0
    invoke-direct {p0, v3}, Lcom/google/android/gms/car/aw;->h(I)Z

    move-result v3

    .line 290
    const/4 v4, 0x0

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/aw;->h(I)Z

    move-result v4

    .line 291
    const-string v5, "CAR.AUDIO"

    const/4 v6, 0x3

    invoke-static {v5, v6}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 292
    const-string v5, "CAR.AUDIO"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "isSilent tts: "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " media:"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    :cond_0
    if-eqz v3, :cond_1

    if-eqz v4, :cond_1

    :goto_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 295
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method final c()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x1

    .line 515
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->q:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v3, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 517
    if-ne v0, v2, :cond_1

    .line 518
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/aw;->j:I

    .line 519
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->d()V

    .line 540
    :cond_0
    :goto_0
    return-void

    .line 520
    :cond_1
    if-nez v0, :cond_0

    .line 521
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    .line 522
    if-eqz v0, :cond_2

    .line 523
    const-string v1, "CAR.AUDIO"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 524
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestAndroidFocusForBottomListener failed, mode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 530
    :cond_2
    iget v0, p0, Lcom/google/android/gms/car/aw;->j:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/aw;->j:I

    .line 531
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->e()Z

    move-result v0

    if-nez v0, :cond_3

    .line 533
    const-string v0, "CAR.AUDIO"

    const-string v1, "bottom focus request failed while not in call, retry"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->e(Lcom/google/android/gms/car/bb;)V

    goto :goto_0

    .line 537
    :cond_3
    const-string v0, "CAR.AUDIO"

    const-string v1, "bottom focus request failed while in call, retry once call ends"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public final c(I)V
    .locals 3

    .prologue
    .line 242
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 243
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 244
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->u:Ljava/util/LinkedList;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 245
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method final d()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x3

    .line 543
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/aw;->p:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v2, v3}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    move-result v0

    .line 545
    if-ne v0, v3, :cond_2

    .line 546
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/aw;->k:I

    .line 547
    iput v3, p0, Lcom/google/android/gms/car/aw;->d:I

    .line 548
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/aw;->e(I)V

    .line 549
    const/4 v0, 0x5

    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/aw;->e(I)V

    .line 550
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 551
    const-string v0, "CAR.AUDIO"

    const-string v1, "send audio focus release to car"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 553
    :cond_0
    const/4 v0, 0x4

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/aw;->o(I)V

    .line 568
    :cond_1
    :goto_0
    return-void

    .line 554
    :cond_2
    if-nez v0, :cond_1

    .line 555
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->o:Landroid/media/AudioManager;

    invoke-virtual {v0}, Landroid/media/AudioManager;->getMode()I

    move-result v0

    .line 556
    if-eqz v0, :cond_3

    .line 557
    const-string v1, "CAR.AUDIO"

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 558
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "requestAndroidFocusForTopListener failed, mode:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    :cond_3
    iget v0, p0, Lcom/google/android/gms/car/aw;->k:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/aw;->k:I

    .line 562
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->e()Z

    move-result v0

    if-nez v0, :cond_1

    iget v0, p0, Lcom/google/android/gms/car/aw;->k:I

    const/16 v1, 0x14

    if-ge v0, v1, :cond_1

    .line 565
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v0}, Lcom/google/android/gms/car/bb;->e(Lcom/google/android/gms/car/bb;)V

    goto :goto_0
.end method

.method public final d(I)Z
    .locals 2

    .prologue
    .line 368
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 369
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->h:[I

    aget v0, v1, v0

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final e(I)V
    .locals 2

    .prologue
    .line 870
    const-wide/16 v0, 0x1388

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/car/aw;->a(IJ)V

    .line 871
    return-void
.end method

.method final e()Z
    .locals 1

    .prologue
    .line 1060
    iget-object v0, p0, Lcom/google/android/gms/car/aw;->v:Landroid/telephony/TelephonyManager;

    invoke-virtual {v0}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/car/aw;->i:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final declared-synchronized f(I)V
    .locals 1

    .prologue
    .line 970
    monitor-enter p0

    packed-switch p1, :pswitch_data_0

    .line 975
    :goto_0
    monitor-exit p0

    return-void

    .line 972
    :pswitch_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->d()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 970
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method final g(I)V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 1004
    iget-object v2, p0, Lcom/google/android/gms/car/aw;->e:Lcom/google/android/gms/car/bb;

    invoke-static {v2}, Lcom/google/android/gms/car/bb;->h(Lcom/google/android/gms/car/bb;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1006
    iget-object v1, p0, Lcom/google/android/gms/car/aw;->g:Ljava/lang/Object;

    monitor-enter v1

    .line 1007
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/aw;->x:Z

    .line 1008
    iput p1, p0, Lcom/google/android/gms/car/aw;->y:I

    .line 1009
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1010
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1011
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "new focus while waiting for car\'s response, type:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 1027
    :cond_0
    :goto_0
    return-void

    .line 1009
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 1015
    :cond_1
    iget v2, p0, Lcom/google/android/gms/car/aw;->c:I

    packed-switch p1, :pswitch_data_0

    new-instance v0, Ljava/lang/RuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong request type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v3, 0x3

    if-eq v2, v3, :cond_2

    const/4 v3, 0x5

    if-ne v2, v3, :cond_4

    :cond_2
    :goto_1
    if-eqz v0, :cond_0

    .line 1018
    invoke-virtual {p0}, Lcom/google/android/gms/car/aw;->e()Z

    move-result v0

    if-nez v0, :cond_0

    .line 1023
    const-string v0, "CAR.AUDIO"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 1024
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send audio focus request to car:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1026
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/aw;->o(I)V

    goto :goto_0

    .line 1015
    :pswitch_1
    if-eq v2, v1, :cond_2

    :cond_4
    move v0, v1

    goto :goto_1

    :pswitch_2
    if-ne v2, v4, :cond_4

    goto :goto_1

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_2
        :pswitch_0
    .end packed-switch
.end method
