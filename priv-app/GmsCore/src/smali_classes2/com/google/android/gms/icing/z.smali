.class public final Lcom/google/android/gms/icing/z;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/icing/u;

.field public b:[Lcom/google/android/gms/icing/v;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 8309
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 8310
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-static {}, Lcom/google/android/gms/icing/v;->a()[Lcom/google/android/gms/icing/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/z;->cachedSize:I

    .line 8311
    return-void
.end method

.method public static a([B)Lcom/google/android/gms/icing/z;
    .locals 1

    .prologue
    .line 8439
    new-instance v0, Lcom/google/android/gms/icing/z;

    invoke-direct {v0}, Lcom/google/android/gms/icing/z;-><init>()V

    invoke-static {v0, p0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/z;

    return-object v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    .line 8374
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 8375
    iget-object v1, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-eqz v1, :cond_0

    .line 8376
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8379
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v1, v1

    if-lez v1, :cond_3

    .line 8380
    const/4 v1, 0x0

    move v4, v1

    move v1, v0

    move v0, v4

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 8381
    iget-object v2, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    aget-object v2, v2, v0

    .line 8382
    if-eqz v2, :cond_1

    .line 8383
    const/4 v3, 0x2

    invoke-static {v3, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v2

    add-int/2addr v1, v2

    .line 8380
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 8388
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 8322
    if-ne p1, p0, :cond_1

    .line 8342
    :cond_0
    :goto_0
    return v0

    .line 8325
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/z;

    if-nez v2, :cond_2

    move v0, v1

    .line 8326
    goto :goto_0

    .line 8328
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/z;

    .line 8329
    iget-object v2, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-nez v2, :cond_3

    .line 8330
    iget-object v2, p1, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-eqz v2, :cond_4

    move v0, v1

    .line 8331
    goto :goto_0

    .line 8334
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    iget-object v3, p1, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/u;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 8335
    goto :goto_0

    .line 8338
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    iget-object v3, p1, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 8340
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 8347
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 8350
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 8352
    return v0

    .line 8347
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/u;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 8286
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/u;

    invoke-direct {v0}, Lcom/google/android/gms/icing/u;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    if-nez v0, :cond_3

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Lcom/google/android/gms/icing/v;

    if-eqz v0, :cond_2

    iget-object v3, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_2
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_4

    new-instance v3, Lcom/google/android/gms/icing/v;

    invoke-direct {v3}, Lcom/google/android/gms/icing/v;-><init>()V

    aput-object v3, v2, v0

    aget-object v3, v2, v0

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v0, v0

    goto :goto_1

    :cond_4
    new-instance v3, Lcom/google/android/gms/icing/v;

    invoke-direct {v3}, Lcom/google/android/gms/icing/v;-><init>()V

    aput-object v3, v2, v0

    aget-object v0, v2, v0

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    iput-object v2, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 8358
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    if-eqz v0, :cond_0

    .line 8359
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/z;->a:Lcom/google/android/gms/icing/u;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8361
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 8362
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 8363
    iget-object v1, p0, Lcom/google/android/gms/icing/z;->b:[Lcom/google/android/gms/icing/v;

    aget-object v1, v1, v0

    .line 8364
    if-eqz v1, :cond_1

    .line 8365
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 8362
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 8369
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 8370
    return-void
.end method
