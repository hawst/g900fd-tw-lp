.class public Lcom/google/android/gms/udc/SettingState;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/udc/b;


# instance fields
.field private final a:I

.field private b:I

.field private c:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 50
    new-instance v0, Lcom/google/android/gms/udc/b;

    invoke-direct {v0}, Lcom/google/android/gms/udc/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    .line 70
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 89
    const/4 v0, 0x2

    invoke-direct {p0, v0, p1, p2}, Lcom/google/android/gms/udc/SettingState;-><init>(III)V

    .line 90
    return-void
.end method

.method constructor <init>(III)V
    .locals 0

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    .line 81
    iput p2, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    .line 82
    iput p3, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    .line 83
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->a:I

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->b:I

    return v0
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 129
    iget v0, p0, Lcom/google/android/gms/udc/SettingState;->c:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 97
    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1

    .prologue
    .line 105
    sget-object v0, Lcom/google/android/gms/udc/SettingState;->CREATOR:Lcom/google/android/gms/udc/b;

    invoke-static {p0, p1}, Lcom/google/android/gms/udc/b;->a(Lcom/google/android/gms/udc/SettingState;Landroid/os/Parcel;)V

    .line 106
    return-void
.end method
