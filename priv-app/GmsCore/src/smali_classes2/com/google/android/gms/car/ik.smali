.class final Lcom/google/android/gms/car/ik;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ij;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ij;)V
    .locals 0

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 7

    .prologue
    .line 84
    :try_start_0
    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "CAR.SENSOR"

    const-string v1, "location injection enabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 90
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    new-instance v1, Lcom/google/android/gms/car/jq;

    iget-object v2, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v2}, Lcom/google/android/gms/car/ij;->a(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gx;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/car/gx;->F()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v3}, Lcom/google/android/gms/car/ij;->a(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gx;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/car/gx;->J()Lcom/google/android/gms/car/id;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/jq;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/id;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ij;->a(Lcom/google/android/gms/car/ij;Lcom/google/android/gms/car/jq;)Lcom/google/android/gms/car/jq;

    .line 93
    iget-object v0, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v0}, Lcom/google/android/gms/car/ij;->b(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gn;

    move-result-object v0

    const/16 v1, 0xa

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/gn;->a(IILcom/google/android/gms/car/na;)Z

    .line 97
    invoke-static {}, Lcom/google/android/gms/car/ij;->d()[I

    move-result-object v1

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_1

    aget v3, v1, v0

    .line 98
    iget-object v4, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-static {v4}, Lcom/google/android/gms/car/ij;->b(Lcom/google/android/gms/car/ij;)Lcom/google/android/gms/car/gn;

    move-result-object v4

    const/4 v5, 0x3

    iget-object v6, p0, Lcom/google/android/gms/car/ik;->a:Lcom/google/android/gms/car/ij;

    invoke-virtual {v4, v3, v5, v6}, Lcom/google/android/gms/car/gn;->a(IILcom/google/android/gms/car/na;)Z
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 97
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 105
    :catch_0
    move-exception v0

    const-string v0, "CAR.SENSOR"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 106
    const-string v0, "CAR.SENSOR"

    const-string v1, "car already disconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    :cond_1
    return-void
.end method
