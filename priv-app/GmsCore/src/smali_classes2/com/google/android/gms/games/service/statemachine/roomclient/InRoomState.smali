.class final Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private final e:Ljava/util/Hashtable;

.field private final j:Ljava/util/Hashtable;

.field private final k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

.field private final l:Ljava/util/Hashtable;

.field private m:I

.field private n:Lcom/google/android/gms/common/server/ClientContext;

.field private o:J

.field private p:Landroid/content/Context;

.field private q:Lcom/google/android/gms/games/service/statemachine/roomclient/b;

.field private r:Lcom/google/android/gms/games/service/statemachine/roomclient/by;

.field private s:Lcom/google/android/gms/games/internal/ConnectionInfo;

.field private t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 1

    .prologue
    .line 135
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 118
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->e:Ljava/util/Hashtable;

    .line 120
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->j:Ljava/util/Hashtable;

    .line 122
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    .line 123
    new-instance v0, Ljava/util/Hashtable;

    invoke-direct {v0}, Ljava/util/Hashtable;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->l:Ljava/util/Hashtable;

    .line 125
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->m:I

    .line 136
    return-void
.end method

.method private a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;
    .locals 6

    .prologue
    .line 591
    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    const/4 v0, 0x0

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-direct {v1, p1, p2, v0, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 593
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->o:J

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->s:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/al;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/u;JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 596
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/ed;->d()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 600
    :goto_0
    return-object v0

    .line 597
    :catch_0
    move-exception v1

    .line 598
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)Lcom/google/android/gms/games/service/statemachine/roomclient/ce;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    return-object v0
.end method

.method private a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V
    .locals 3

    .prologue
    .line 197
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b(I)V

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;

    packed-switch p3, :pswitch_data_0

    const-string v0, "PLAYER_LEFT"

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1, v0, p1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->a(Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/al;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V

    .line 200
    if-nez p2, :cond_0

    sget-object v0, Lcom/google/android/gms/games/c/a;->O:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    .line 201
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 203
    :cond_1
    return-void

    .line 198
    :pswitch_0
    const-string v0, "PLAYER_LEFT"

    goto :goto_0

    :pswitch_1
    const-string v0, "REALTIME_SERVER_CONNECTION_FAILURE"

    goto :goto_0

    :pswitch_2
    const-string v0, "REALTIME_CLIENT_DISCONNECTING"

    goto :goto_0

    :pswitch_3
    const-string v0, "REALTIME_SIGN_OUT"

    goto :goto_0

    :pswitch_4
    const-string v0, "REALTIME_GAME_CRASHED"

    goto :goto_0

    :pswitch_5
    const-string v0, "REALTIME_ROOM_SERVICE_CRASHED"

    goto :goto_0

    :pswitch_6
    const-string v0, "REALTIME_DIFFERENT_CLIENT_ROOM_OPERATION"

    goto :goto_0

    :pswitch_7
    const-string v0, "REALTIME_SAME_CLIENT_ROOM_OPERATION"

    goto :goto_0

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method static synthetic b(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)Lcom/google/android/gms/games/service/statemachine/roomclient/by;
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->r:Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    return-object v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 605
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->j:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;

    .line 606
    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;->a(Ljava/lang/Object;)V

    goto :goto_0

    .line 608
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->j:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 609
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->e:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/y;

    .line 610
    invoke-virtual {v0, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/y;->a(Ljava/lang/Object;)V

    goto :goto_1

    .line 612
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->e:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 614
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->l:Ljava/util/Hashtable;

    invoke-virtual {v0}, Ljava/util/Hashtable;->clear()V

    .line 615
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->q:Lcom/google/android/gms/games/service/statemachine/roomclient/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/b;->a()V

    .line 616
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a()V

    .line 617
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;JLandroid/content/Context;Ljava/lang/String;Landroid/os/IBinder;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/internal/ConnectionInfo;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V
    .locals 4

    .prologue
    .line 144
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/server/ClientContext;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    .line 145
    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->o:J

    .line 146
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    .line 147
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;

    invoke-direct {v0, p0, p5, p6}, Lcom/google/android/gms/games/service/statemachine/roomclient/b;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;Ljava/lang/String;Landroid/os/IBinder;)V

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/b;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->q:Lcom/google/android/gms/games/service/statemachine/roomclient/b;

    .line 148
    invoke-static {p8}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/ConnectionInfo;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->s:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 149
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a()V

    .line 150
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    const-string v2, "mGamesCallBack"

    invoke-direct {v1, v0, p7, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/q;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b()V

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iput-object p5, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    .line 152
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    invoke-direct {v0, p4, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/by;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->r:Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    .line 153
    invoke-static {p9}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-object p5, v0, Lcom/google/android/gms/games/g/ag;->a:Ljava/lang/String;

    .line 155
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c()V

    .line 157
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v6, 0x1

    .line 233
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 585
    :pswitch_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->g:Z

    :goto_0
    return v0

    .line 235
    :pswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;

    .line 237
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->b:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->a:Lcom/google/android/gms/games/internal/dr;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    const-string v4, "mWaitingRoomCb"

    invoke-direct {v3, v1, v2, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/q;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    iput-object v3, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b()V

    .line 240
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->r:Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a(Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->a(Ljava/lang/Object;)V

    .line 248
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto :goto_0

    .line 242
    :cond_0
    invoke-static {v6}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->a(Ljava/lang/Object;)V

    .line 243
    const-string v1, "RoomServiceClientStateMachine"

    const-string v3, "REGISTER_WAITING_ROOM_LISTENER_RESTRICTED for room {0} not the active room {1}"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bp;->b:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1

    .line 251
    :pswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bw;

    .line 253
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bw;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 254
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    const/4 v1, 0x0

    iput-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->b()V

    .line 261
    :goto_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto :goto_0

    .line 256
    :cond_1
    const-string v1, "RoomServiceClientStateMachine"

    const-string v3, "UNREGISTER_WAITING_ROOM_LISTENER_RESTRICTED for room {0} not the active room {1}"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bw;->a:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 264
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bc;

    const-string v2, "onPeerJoinedRoom"

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bc;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/i;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/i;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/service/statemachine/roomclient/bc;)V

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 265
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 267
    :pswitch_4
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/az;

    const-string v2, "onPeerDeclined"

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/az;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/j;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/j;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/service/statemachine/roomclient/az;)V

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 268
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 270
    :pswitch_5
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bh;

    const-string v2, "onRoomConnecting"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bh;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/k;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/k;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 271
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 273
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/aw;

    const-string v2, "onP2pDisconnected"

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    :try_start_0
    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;->a:Lcom/google/android/gms/games/internal/dr;

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/aw;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Lcom/google/android/gms/games/internal/dr;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_3

    :catch_0
    move-exception v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Landroid/os/RemoteException;)V

    goto :goto_3

    .line 274
    :cond_2
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 276
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/av;

    const-string v2, "onP2pConnected"

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_4
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    :try_start_1
    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;->a:Lcom/google/android/gms/games/internal/dr;

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/av;->a:Ljava/lang/String;

    invoke-interface {v1, v4}, Lcom/google/android/gms/games/internal/dr;->d(Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_4

    :catch_1
    move-exception v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Landroid/os/RemoteException;)V

    goto :goto_4

    .line 277
    :cond_3
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 279
    :pswitch_8
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ao;

    const-string v2, "onConnectedToRoom"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ao;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/l;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/l;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/m;

    invoke-direct {v4, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/m;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 280
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 282
    :pswitch_9
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ay;

    const-string v2, "onPeerConnected"

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ay;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/n;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/n;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/service/statemachine/roomclient/ay;)V

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 283
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 285
    :pswitch_a
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bg;

    const-string v2, "onRoomConnected"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bg;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/o;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/o;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    iget-object v4, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 286
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 288
    :pswitch_b
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/as;

    const-string v2, "onDisconnectedFromRoom"

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/as;->a:Ljava/lang/String;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/p;

    invoke-direct {v3, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/p;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;)V

    iget-object v4, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v0, v3, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 289
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 291
    :pswitch_c
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bd;

    const-string v2, "onPeerLeftRoom"

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bd;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/g;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/g;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/service/statemachine/roomclient/bd;)V

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 292
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 294
    :pswitch_d
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ba;

    const-string v2, "onPeerDisconnected"

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ba;->a:Ljava/lang/String;

    new-instance v4, Lcom/google/android/gms/games/service/statemachine/roomclient/h;

    invoke-direct {v4, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/h;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/e;Lcom/google/android/gms/games/service/statemachine/roomclient/ba;)V

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/c;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/d;Lcom/google/android/gms/games/service/statemachine/roomclient/c;)V

    .line 295
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 297
    :pswitch_e
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/be;

    const-string v2, "onRealtimeMessageRecieved"

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->e:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_5
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;

    :try_start_2
    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/q;->a:Lcom/google/android/gms/games/internal/dr;

    new-instance v4, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/be;->a:Ljava/lang/String;

    iget-object v6, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/be;->b:[B

    iget v7, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/be;->c:I

    invoke-direct {v4, v5, v6, v7}, Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;-><init>(Ljava/lang/String;[BI)V

    invoke-interface {v1, v4}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/games/multiplayer/realtime/RealTimeMessage;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_5

    :catch_2
    move-exception v1

    invoke-static {v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a(Ljava/lang/String;Landroid/os/RemoteException;)V

    goto :goto_5

    .line 298
    :cond_4
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 300
    :pswitch_f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;

    .line 301
    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->o:J

    iget-wide v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;->b:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_5

    .line 302
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;->c:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/an;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v0

    invoke-direct {p0, v0, v6, v7}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V

    .line 306
    :cond_5
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 309
    :pswitch_10
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;

    .line 310
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;->a:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/server/ClientContext;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 311
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;->b:Landroid/content/Context;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bo;->a:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v0

    const/4 v1, 0x4

    invoke-direct {p0, v0, v6, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V

    .line 315
    :cond_6
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 318
    :pswitch_11
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;

    .line 319
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 321
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v0

    const/4 v1, 0x5

    invoke-direct {p0, v0, v6, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V

    .line 330
    :goto_6
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 325
    :cond_7
    const-string v1, "RoomServiceClientStateMachine"

    const-string v3, "GAME_DEATH_BINDER_DIED for room {0} not the active room {1}"

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ah;->a:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_6

    .line 333
    :pswitch_12
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;

    .line 334
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/br;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/g;->a()Z

    move-result v0

    if-eqz v0, :cond_8

    .line 337
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v0

    const/4 v1, 0x6

    invoke-direct {p0, v0, v6, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V

    .line 340
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 343
    :cond_8
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 350
    :pswitch_13
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 352
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1, v7}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 353
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v1, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 355
    iget-wide v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    iget-wide v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->o:J

    cmp-long v0, v0, v4

    if-eqz v0, :cond_9

    .line 356
    const/4 v0, 0x7

    .line 360
    :goto_7
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v1, v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v1

    invoke-direct {p0, v1, v2, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V

    .line 363
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 358
    :cond_9
    const/16 v0, 0x8

    goto :goto_7

    .line 367
    :pswitch_14
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v1, "We shouldn\'t be able to get LEAVE_ROOM in InRoomState"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 368
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 370
    :pswitch_15
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;

    .line 371
    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-static {v0, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_b

    .line 372
    const-string v0, "RoomServiceClientStateMachine"

    const-string v3, "InRoomState: LEAVE_ROOM - Sending to room {0} while in room {1} "

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 376
    :try_start_3
    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_a

    .line 377
    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->c:Lcom/google/android/gms/games/internal/dr;

    const/16 v2, 0x1774

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/u;->d:Ljava/lang/String;

    invoke-interface {v0, v2, v1}, Lcom/google/android/gms/games/internal/dr;->d(ILjava/lang/String;)V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_3

    .line 395
    :cond_a
    :goto_8
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 381
    :catch_3
    move-exception v0

    .line 382
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "InRoomState: LEAVE_ROOM"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_8

    .line 385
    :cond_b
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-wide v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->o:J

    iget-object v4, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->s:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/al;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/u;JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 388
    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    invoke-interface {v1}, Lcom/google/android/gms/games/internal/ed;->d()V

    .line 389
    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/al;ZI)V
    :try_end_4
    .catch Landroid/os/RemoteException; {:try_start_4 .. :try_end_4} :catch_4

    goto :goto_8

    .line 391
    :catch_4
    move-exception v0

    .line 392
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    goto :goto_8

    .line 398
    :pswitch_16
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;

    .line 399
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_c

    .line 400
    const-string v1, "RoomServiceClientStateMachine"

    const-string v3, "InRoomState: SEND_UNRELIABLE_MESSAGE - Sending to room {0} while in room {1} "

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;->b:Ljava/lang/String;

    aput-object v0, v4, v2

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v0, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 413
    :goto_9
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 406
    :cond_c
    :try_start_5
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;->a:[B

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bt;->c:[Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/internal/ed;->a([B[Ljava/lang/String;)V
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_5

    goto :goto_9

    .line 408
    :catch_5
    move-exception v0

    .line 409
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "InRoomState: SEND_UNRELIABLE_MESSAGE"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_9

    .line 416
    :pswitch_17
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;

    .line 417
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->c:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    invoke-static {v1, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_d

    .line 418
    const-string v1, "RoomServiceClientStateMachine"

    const-string v3, "InRoomState: SEND_UNRELIABLE_MESSAGE - Sending to room {0} while in room {1} "

    new-array v4, v4, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->c:Ljava/lang/String;

    aput-object v5, v4, v2

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/e;->a:Ljava/lang/String;

    aput-object v2, v4, v6

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    :try_start_6
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->a:Lcom/google/android/gms/games/internal/dr;

    const/16 v2, 0x1774

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->d:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/dr;->a(IILjava/lang/String;)V
    :try_end_6
    .catch Landroid/os/RemoteException; {:try_start_6 .. :try_end_6} :catch_6

    .line 439
    :goto_a
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 426
    :catch_6
    move-exception v0

    .line 427
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "InRoomState: SEND_RELIABLE_MESSAGE"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_a

    .line 432
    :cond_d
    :try_start_7
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->b:[B

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->d:Ljava/lang/String;

    iget v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    invoke-interface {v1, v2, v3, v4}, Lcom/google/android/gms/games/internal/ed;->a([BLjava/lang/String;I)V

    .line 434
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->l:Ljava/util/Hashtable;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->e:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bs;->a:Lcom/google/android/gms/games/internal/dr;

    invoke-virtual {v1, v2, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_7
    .catch Landroid/os/RemoteException; {:try_start_7 .. :try_end_7} :catch_7

    goto :goto_a

    .line 435
    :catch_7
    move-exception v0

    .line 436
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    goto :goto_a

    .line 444
    :pswitch_18
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;

    .line 445
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->l:Ljava/util/Hashtable;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/dr;

    .line 446
    if-nez v1, :cond_e

    .line 447
    const-string v1, "RoomServiceClientStateMachine"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "InRoomState: ON_SENT_RELIABLE_MESSAGE - can\'t find token "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;->b:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 458
    :goto_b
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 452
    :cond_e
    :try_start_8
    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;->a:I

    iget v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;->b:I

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bn;->c:Ljava/lang/String;

    invoke-interface {v1, v2, v3, v0}, Lcom/google/android/gms/games/internal/dr;->a(IILjava/lang/String;)V
    :try_end_8
    .catch Landroid/os/RemoteException; {:try_start_8 .. :try_end_8} :catch_8

    goto :goto_b

    .line 453
    :catch_8
    move-exception v0

    .line 454
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "InRoomState: ON_SENT_RELIABLE_MESSAGE"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_b

    .line 461
    :pswitch_19
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/games/service/statemachine/roomclient/bq;

    .line 462
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 464
    :try_start_9
    new-instance v1, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v1}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    move v0, v2

    .line 465
    :goto_c
    iget-object v2, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/bq;->b:[Ljava/lang/String;

    array-length v2, v2

    if-ge v0, v2, :cond_f

    .line 466
    new-instance v2, Lcom/google/android/gms/games/h/a/es;

    invoke-direct {v2}, Lcom/google/android/gms/games/h/a/es;-><init>()V

    .line 467
    iget-object v3, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/bq;->b:[Ljava/lang/String;

    aget-object v3, v3, v0

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 468
    invoke-virtual {v5, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 465
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 470
    :cond_f
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->c:Lcom/google/android/gms/games/service/statemachine/roomclient/bz;

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    new-instance v3, Lcom/google/android/gms/games/service/statemachine/roomclient/a;

    invoke-direct {v3, p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/a;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;)V

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/bq;->a:Ljava/lang/String;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/bz;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/f;Ljava/lang/String;Ljava/util/ArrayList;)V
    :try_end_9
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_9 .. :try_end_9} :catch_9

    .line 491
    :goto_d
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 486
    :catch_9
    move-exception v0

    .line 487
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v2, "Unable to report P2P status."

    invoke-interface {v1, v2, v0}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 488
    const-string v1, "RoomServiceClientStateMachine"

    const-string v2, "Unable to report P2P status."

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_d

    .line 494
    :pswitch_1a
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ax;

    .line 495
    iget v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ax;->a:I

    if-nez v1, :cond_10

    .line 497
    :try_start_a
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ax;->b:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/gms/games/internal/ed;->b(Ljava/lang/String;)V
    :try_end_a
    .catch Landroid/os/RemoteException; {:try_start_a .. :try_end_a} :catch_a

    .line 511
    :goto_e
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 498
    :catch_a
    move-exception v0

    .line 499
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    goto :goto_e

    .line 502
    :cond_10
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v2, "Unable to send P2P status."

    invoke-interface {v1, v2}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    .line 503
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean v6, v1, Lcom/google/android/gms/games/g/ag;->D:Z

    .line 506
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->p:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->n:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    move-result-object v1

    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ax;->a:I

    sparse-switch v0, :sswitch_data_0

    const-string v0, "REALTIME_SERVER_CONNECTION_FAILURE"

    :goto_f
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v2, v4}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b(I)V

    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v2, v0, v1, v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitLeaveDiagnosticsState;->a(Ljava/lang/String;Lcom/google/android/gms/games/service/statemachine/roomclient/al;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V

    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 509
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Unable to send P2P status."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_e

    .line 506
    :sswitch_0
    const-string v0, "REALTIME_SERVER_ERROR"

    goto :goto_f

    :sswitch_1
    const-string v0, "REALTIME_SERVER_CONNECTION_FAILURE"

    goto :goto_f

    .line 514
    :pswitch_1b
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bm;

    .line 515
    new-instance v1, Lcom/google/android/gms/common/server/response/d;

    invoke-direct {v1}, Lcom/google/android/gms/common/server/response/d;-><init>()V

    .line 517
    :try_start_b
    new-instance v2, Lcom/google/android/gms/games/h/a/ev;

    invoke-direct {v2}, Lcom/google/android/gms/games/h/a/ev;-><init>()V

    .line 518
    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bm;->a:Ljava/lang/String;

    invoke-virtual {v1, v3, v2}, Lcom/google/android/gms/common/server/response/d;->a(Ljava/lang/String;Lcom/google/android/gms/common/server/response/FastJsonResponse;)V

    .line 519
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->r:Lcom/google/android/gms/games/service/statemachine/roomclient/by;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/by;->a(Lcom/google/android/gms/games/h/a/ev;)Z

    .line 520
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget v2, v1, Lcom/google/android/gms/games/g/ag;->E:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Lcom/google/android/gms/games/g/ag;->E:I
    :try_end_b
    .catch Lcom/google/android/gms/common/server/response/m; {:try_start_b .. :try_end_b} :catch_b

    .line 525
    :goto_10
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 521
    :catch_b
    move-exception v1

    .line 522
    const-string v2, "RoomServiceClientStateMachine"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Unable to parse DATA_MANAGER_UPDATE_ROOM "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bm;->a:Ljava/lang/String;

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_10

    .line 531
    :pswitch_1c
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 532
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 538
    :pswitch_1d
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 539
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 541
    :pswitch_1e
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean v6, v0, Lcom/google/android/gms/games/g/ag;->B:Z

    .line 542
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/y;

    .line 543
    iget v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->m:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->m:I

    .line 545
    :try_start_c
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/y;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/games/internal/ed;->a(Ljava/lang/String;I)V

    .line 547
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->e:Ljava/util/Hashtable;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_c
    .catch Landroid/os/RemoteException; {:try_start_c .. :try_end_c} :catch_c

    .line 552
    :goto_11
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 548
    :catch_c
    move-exception v0

    .line 549
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    .line 550
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    goto :goto_11

    .line 555
    :pswitch_1f
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;

    .line 556
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->t:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iput-boolean v6, v1, Lcom/google/android/gms/games/g/ag;->C:Z

    .line 557
    iget v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->m:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->m:I

    .line 559
    :try_start_d
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;->a:Ljava/lang/String;

    invoke-interface {v2, v3, v1}, Lcom/google/android/gms/games/internal/ed;->b(Ljava/lang/String;I)V

    .line 560
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->j:Ljava/util/Hashtable;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v2, v1, v0}, Ljava/util/Hashtable;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_d
    .catch Landroid/os/RemoteException; {:try_start_d .. :try_end_d} :catch_d

    .line 565
    :goto_12
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 561
    :catch_d
    move-exception v0

    .line 562
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->a(Landroid/os/RemoteException;)V

    .line 563
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    goto :goto_12

    .line 568
    :pswitch_20
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ap;

    .line 570
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->e:Ljava/util/Hashtable;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ap;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/y;

    .line 571
    if-eqz v1, :cond_11

    .line 572
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ap;->a:Landroid/os/ParcelFileDescriptor;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/y;->a(Ljava/lang/Object;)V

    .line 574
    :cond_11
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 577
    :pswitch_21
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/aq;

    .line 578
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->j:Ljava/util/Hashtable;

    iget v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/aq;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Hashtable;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;

    .line 579
    if-eqz v1, :cond_12

    .line 580
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/aq;->a:Ljava/lang/String;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ac;->a(Ljava/lang/Object;)V

    .line 582
    :cond_12
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->f:Z

    goto/16 :goto_0

    .line 233
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_c
        :pswitch_4
        :pswitch_9
        :pswitch_d
        :pswitch_7
        :pswitch_6
        :pswitch_0
        :pswitch_19
        :pswitch_1c
        :pswitch_1b
        :pswitch_0
        :pswitch_12
        :pswitch_0
        :pswitch_0
        :pswitch_15
        :pswitch_17
        :pswitch_16
        :pswitch_1f
        :pswitch_1e
        :pswitch_1
        :pswitch_2
        :pswitch_10
        :pswitch_0
        :pswitch_0
        :pswitch_14
        :pswitch_0
        :pswitch_f
        :pswitch_13
        :pswitch_0
        :pswitch_18
        :pswitch_e
        :pswitch_5
        :pswitch_0
        :pswitch_a
        :pswitch_8
        :pswitch_b
        :pswitch_0
        :pswitch_1d
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_11
        :pswitch_20
        :pswitch_21
        :pswitch_0
        :pswitch_1a
    .end packed-switch

    .line 506
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_1
        0x1774 -> :sswitch_0
    .end sparse-switch
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 161
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/InRoomState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->a:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 162
    return-void
.end method
