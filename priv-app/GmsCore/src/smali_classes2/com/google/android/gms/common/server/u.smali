.class public final Lcom/google/android/gms/common/server/u;
.super Lcom/android/volley/p;
.source "SourceFile"


# instance fields
.field private final f:Landroid/content/Context;

.field private final g:Landroid/net/Uri;

.field private final h:Lcom/android/volley/x;

.field private final i:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;ZLcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0, p3, p6}, Lcom/android/volley/p;-><init>(Ljava/lang/String;Lcom/android/volley/w;)V

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/common/server/u;->f:Landroid/content/Context;

    .line 43
    iput-object p2, p0, Lcom/google/android/gms/common/server/u;->g:Landroid/net/Uri;

    .line 44
    iput-object p5, p0, Lcom/google/android/gms/common/server/u;->h:Lcom/android/volley/x;

    .line 45
    iput-boolean p4, p0, Lcom/google/android/gms/common/server/u;->i:Z

    .line 46
    return-void
.end method


# virtual methods
.method protected final a(Lcom/android/volley/m;)Lcom/android/volley/v;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 50
    iget-object v1, p0, Lcom/google/android/gms/common/server/u;->g:Landroid/net/Uri;

    if-eqz v1, :cond_0

    .line 52
    iget-object v1, p0, Lcom/google/android/gms/common/server/u;->f:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 53
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 54
    const-string v3, "url"

    iget-object v4, p0, Lcom/android/volley/p;->b:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 55
    const-string v3, "image_data"

    iget-object v4, p1, Lcom/android/volley/m;->b:[B

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 56
    iget-object v3, p0, Lcom/google/android/gms/common/server/u;->g:Landroid/net/Uri;

    invoke-virtual {v1, v3, v2}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    .line 59
    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    invoke-static {v1, v0}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    .line 62
    :goto_0
    return-object v0

    :cond_0
    iget-object v1, p1, Lcom/android/volley/m;->b:[B

    iget-boolean v2, p0, Lcom/google/android/gms/common/server/u;->i:Z

    if-eqz v2, :cond_1

    invoke-static {p1}, Lcom/android/volley/toolbox/i;->a(Lcom/android/volley/m;)Lcom/android/volley/c;

    move-result-object v0

    :cond_1
    invoke-static {v1, v0}, Lcom/android/volley/v;->a(Ljava/lang/Object;Lcom/android/volley/c;)Lcom/android/volley/v;

    move-result-object v0

    goto :goto_0
.end method

.method protected final synthetic b(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 21
    check-cast p1, [B

    iget-object v0, p0, Lcom/google/android/gms/common/server/u;->h:Lcom/android/volley/x;

    invoke-interface {v0, p1}, Lcom/android/volley/x;->a(Ljava/lang/Object;)V

    return-void
.end method

.method public final o()Lcom/android/volley/r;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/android/volley/r;->a:Lcom/android/volley/r;

    return-object v0
.end method
