.class public final Lcom/google/android/gms/games/service/k;
.super Lcom/google/android/gms/common/internal/b;
.source "SourceFile"


# static fields
.field static final a:Ljava/util/HashMap;

.field static final d:Ljava/lang/Object;

.field public static final e:Ljava/util/HashMap;

.field private static final f:Ljava/util/regex/Pattern;

.field private static final g:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 142
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    .line 145
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    .line 148
    const-string v0, "[a-zA-Z0-9\\.\\-_~]{0,64}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/service/k;->f:Ljava/util/regex/Pattern;

    .line 151
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/k;->e:Ljava/util/HashMap;

    .line 168
    const-string v0, "[0-9a-zA-Z-._~]{1,100}"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/service/k;->g:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/internal/b;-><init>(Landroid/content/Context;)V

    .line 173
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/service/l;
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 223
    sget-object v3, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 224
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0, p0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 225
    if-nez v0, :cond_0

    .line 226
    monitor-exit v3

    move-object v0, v2

    .line 241
    :goto_0
    return-object v0

    .line 233
    :cond_0
    sget-object v1, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    invoke-static {v1, p0}, Lcom/google/android/gms/common/util/ah;->a(Ljava/util/Map;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/common/server/ClientContext;

    .line 236
    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/server/ClientContext;->c(Lcom/google/android/gms/common/server/ClientContext;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 238
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/l;

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 243
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 241
    :cond_1
    monitor-exit v3

    move-object v0, v2

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;)V
    .locals 3

    .prologue
    .line 253
    sget-object v1, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 254
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    new-instance v2, Ljava/lang/ref/WeakReference;

    invoke-direct {v2, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    invoke-virtual {v0, p0, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 255
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 291
    sget-object v1, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 292
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 293
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 294
    :goto_1
    if-eqz v0, :cond_0

    .line 295
    invoke-virtual {v0, p0}, Lcom/google/android/gms/games/service/l;->a(Ljava/io/PrintWriter;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 298
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 293
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/l;

    goto :goto_1

    .line 298
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public static a(Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 273
    invoke-static {}, Lcom/google/android/gms/games/service/l;->I()V

    .line 274
    sget-object v1, Lcom/google/android/gms/games/service/k;->e:Ljava/util/HashMap;

    monitor-enter v1

    .line 275
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->e:Ljava/util/HashMap;

    invoke-virtual {v0, p0, p1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static b()V
    .locals 2

    .prologue
    .line 262
    sget-object v1, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 263
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 264
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static c()V
    .locals 3

    .prologue
    .line 280
    sget-object v1, Lcom/google/android/gms/games/service/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 281
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/k;->a:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/ref/WeakReference;

    .line 282
    if-nez v0, :cond_1

    const/4 v0, 0x0

    .line 283
    :goto_1
    if-eqz v0, :cond_0

    .line 284
    invoke-virtual {v0}, Lcom/google/android/gms/games/service/l;->F()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 287
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 282
    :cond_1
    :try_start_1
    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/l;

    goto :goto_1

    .line 287
    :cond_2
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method static synthetic d()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/games/service/k;->f:Ljava/util/regex/Pattern;

    return-object v0
.end method

.method static synthetic e()Ljava/util/regex/Pattern;
    .locals 1

    .prologue
    .line 137
    sget-object v0, Lcom/google/android/gms/games/service/k;->g:Ljava/util/regex/Pattern;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 16

    .prologue
    .line 179
    const-string v1, "GamesServiceBroker"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "client connected with version: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    invoke-static/range {p3 .. p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid package name"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 185
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/k;->b:Landroid/content/Context;

    move-object/from16 v0, p3

    invoke-static {v1, v0}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;Ljava/lang/String;)V

    .line 187
    const-string v1, "com.google.android.gms.games.key.isHeadless"

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v9

    .line 188
    const-string v1, "com.google.android.gms.games.key.retryingSignIn"

    const/4 v2, 0x0

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v12

    .line 193
    const-string v1, "com.google.android.play.games"

    move-object/from16 v0, p3

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x1

    .line 195
    :goto_0
    const-string v2, "com.google.android.gms.games.key.showConnectingPopup"

    move-object/from16 v0, p9

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v10

    .line 197
    const-string v1, "com.google.android.gms.games.key.connectingPopupGravity"

    const/16 v2, 0x11

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v11

    .line 199
    const-string v1, "com.google.android.gms.games.key.sdkVariant"

    const/16 v2, 0x1110

    move-object/from16 v0, p9

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v13

    .line 201
    const-string v1, "com.google.android.gms.games.key.forceResolveAccountKey"

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 203
    const-string v1, "com.google.android.gms.games.key.proxyApis"

    move-object/from16 v0, p9

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v15

    .line 208
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/gms/games/service/k;->b:Landroid/content/Context;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v3

    move-object/from16 v2, p1

    move-object/from16 v4, p3

    move/from16 v5, p2

    move-object/from16 v6, p4

    move-object/from16 v7, p6

    move-object/from16 v8, p5

    invoke-static/range {v1 .. v15}, Lcom/google/android/gms/games/service/GamesIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;ZZIZILjava/lang/String;Ljava/util/ArrayList;)V

    .line 213
    return-void

    .line 193
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method
