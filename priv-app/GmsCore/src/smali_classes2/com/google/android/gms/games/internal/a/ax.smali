.class public final Lcom/google/android/gms/games/internal/a/ax;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/y;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 25
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 156
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 533
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->r()Landroid/content/Intent;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 535
    const-string v1, "com.google.android.gms.games.GAME_PACKAGE_NAME"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    :cond_0
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 229
    new-instance v0, Lcom/google/android/gms/games/internal/a/bf;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bf;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;IZ)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 217
    new-instance v0, Lcom/google/android/gms/games/internal/a/be;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/be;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 291
    new-instance v0, Lcom/google/android/gms/games/internal/a/bh;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/bh;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;Z)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 316
    new-instance v0, Lcom/google/android/gms/games/internal/a/ay;

    const/4 v4, 0x4

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/ay;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/util/ArrayList;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 205
    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v0, v0, [Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/String;

    .line 206
    new-instance v1, Lcom/google/android/gms/games/internal/a/bd;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/gms/games/internal/a/bd;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;[Ljava/lang/String;)V

    invoke-interface {p1, v1}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 265
    new-instance v0, Lcom/google/android/gms/games/internal/a/bg;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bg;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 172
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->g()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;
    .locals 6

    .prologue
    .line 412
    new-instance v0, Lcom/google/android/gms/games/internal/a/az;

    const/4 v5, 0x0

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/internal/a/az;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Ljava/lang/String;IZ)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 177
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->l()Lcom/google/android/gms/games/Player;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Z)V
    .locals 1

    .prologue
    .line 285
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->a(Z)V

    .line 286
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 543
    new-instance v0, Lcom/google/android/gms/games/internal/a/bb;

    const/4 v1, 0x1

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/bb;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 424
    new-instance v0, Lcom/google/android/gms/games/internal/a/ba;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/ba;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;Z)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 554
    new-instance v0, Lcom/google/android/gms/games/internal/a/bc;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/bc;-><init>(Lcom/google/android/gms/games/internal/a/ax;Lcom/google/android/gms/common/api/v;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
