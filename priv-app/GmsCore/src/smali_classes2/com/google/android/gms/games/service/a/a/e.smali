.class public final Lcom/google/android/gms/games/service/a/a/e;
.super Lcom/google/android/gms/games/service/a/c;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Ljava/lang/String;

.field private final e:Lcom/google/android/gms/games/internal/el;

.field private final f:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;ILjava/lang/String;Lcom/google/android/gms/games/internal/el;)V
    .locals 1

    .prologue
    .line 28
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/c;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 29
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/a/e;->b:Lcom/google/android/gms/games/internal/dr;

    .line 30
    iput p3, p0, Lcom/google/android/gms/games/service/a/a/e;->f:I

    .line 31
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/a/e;->c:Lcom/google/android/gms/games/a/au;

    .line 32
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/a/e;->d:Ljava/lang/String;

    .line 33
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/a/e;->e:Lcom/google/android/gms/games/internal/el;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(I)V
    .locals 2

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/e;->b:Lcom/google/android/gms/games/internal/dr;

    if-eqz v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/e;->b:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a/e;->d:Ljava/lang/String;

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/dr;->b(ILjava/lang/String;)V

    .line 56
    :cond_0
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)I
    .locals 3

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/gms/games/service/a/a/e;->f:I

    packed-switch v0, :pswitch_data_0

    .line 47
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "unknown operation "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/service/a/a/e;->f:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 41
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/e;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a/e;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/a/e;->e:Lcom/google/android/gms/games/internal/el;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)I

    move-result v0

    .line 44
    :goto_0
    return v0

    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/a/e;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/a/e;->d:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/a/e;->e:Lcom/google/android/gms/games/internal/el;

    invoke-virtual {p2, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Lcom/google/android/gms/games/internal/el;)I

    move-result v0

    goto :goto_0

    .line 39
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
