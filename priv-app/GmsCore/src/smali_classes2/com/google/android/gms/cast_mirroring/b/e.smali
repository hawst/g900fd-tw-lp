.class public abstract Lcom/google/android/gms/cast_mirroring/b/e;
.super Landroid/os/Binder;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/cast_mirroring/b/d;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/os/Binder;-><init>()V

    .line 20
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p0, p0, v0}, Lcom/google/android/gms/cast_mirroring/b/e;->attachInterface(Landroid/os/IInterface;Ljava/lang/String;)V

    .line 21
    return-void
.end method


# virtual methods
.method public asBinder()Landroid/os/IBinder;
    .locals 0

    .prologue
    .line 39
    return-object p0
.end method

.method public onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z
    .locals 7

    .prologue
    const/4 v4, 0x0

    const/4 v6, 0x1

    .line 43
    sparse-switch p1, :sswitch_data_0

    .line 119
    invoke-super {p0, p1, p2, p3, p4}, Landroid/os/Binder;->onTransact(ILandroid/os/Parcel;Landroid/os/Parcel;I)Z

    move-result v6

    :goto_0
    return v6

    .line 47
    :sswitch_0
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p3, v0}, Landroid/os/Parcel;->writeString(Ljava/lang/String;)V

    goto :goto_0

    .line 52
    :sswitch_1
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 54
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/b/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/cast_mirroring/b/a;

    move-result-object v1

    .line 56
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_1

    move v2, v6

    .line 58
    :goto_1
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 60
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    .line 61
    sget-object v0, Landroid/view/Surface;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Surface;

    move-object v4, v0

    .line 67
    :cond_0
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move-object v0, p0

    .line 68
    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/cast_mirroring/b/e;->a(Lcom/google/android/gms/cast_mirroring/b/a;ZLjava/lang/String;Landroid/view/Surface;I)V

    goto :goto_0

    .line 56
    :cond_1
    const/4 v2, 0x0

    goto :goto_1

    .line 73
    :sswitch_2
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/gms/cast_mirroring/b/e;->a()V

    goto :goto_0

    .line 79
    :sswitch_3
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 80
    invoke-virtual {p0}, Lcom/google/android/gms/cast_mirroring/b/e;->b()V

    goto :goto_0

    .line 85
    :sswitch_4
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 87
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/b/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/cast_mirroring/b/a;

    move-result-object v2

    .line 89
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v1

    if-nez v1, :cond_2

    move-object v1, v4

    .line 91
    :goto_2
    invoke-virtual {p2}, Landroid/os/Parcel;->readString()Ljava/lang/String;

    move-result-object v3

    .line 93
    invoke-virtual {p2}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_4

    .line 94
    sget-object v0, Landroid/app/PendingIntent;->CREATOR:Landroid/os/Parcelable$Creator;

    invoke-interface {v0, p2}, Landroid/os/Parcelable$Creator;->createFromParcel(Landroid/os/Parcel;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    .line 99
    :goto_3
    invoke-virtual {p0, v2, v1, v3, v0}, Lcom/google/android/gms/cast_mirroring/b/e;->a(Lcom/google/android/gms/cast_mirroring/b/a;Lcom/google/android/gms/cast_mirroring/b/f;Ljava/lang/String;Landroid/app/PendingIntent;)V

    goto :goto_0

    .line 89
    :cond_2
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringSessionCallbacks"

    invoke-interface {v1, v0}, Landroid/os/IBinder;->queryLocalInterface(Ljava/lang/String;)Landroid/os/IInterface;

    move-result-object v0

    if-eqz v0, :cond_3

    instance-of v3, v0, Lcom/google/android/gms/cast_mirroring/b/f;

    if-eqz v3, :cond_3

    check-cast v0, Lcom/google/android/gms/cast_mirroring/b/f;

    move-object v1, v0

    goto :goto_2

    :cond_3
    new-instance v0, Lcom/google/android/gms/cast_mirroring/b/h;

    invoke-direct {v0, v1}, Lcom/google/android/gms/cast_mirroring/b/h;-><init>(Landroid/os/IBinder;)V

    move-object v1, v0

    goto :goto_2

    :cond_4
    move-object v0, v4

    .line 97
    goto :goto_3

    .line 104
    :sswitch_5
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 106
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/b/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/cast_mirroring/b/a;

    move-result-object v0

    .line 107
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast_mirroring/b/e;->a(Lcom/google/android/gms/cast_mirroring/b/a;)V

    goto/16 :goto_0

    .line 112
    :sswitch_6
    const-string v0, "com.google.android.gms.cast_mirroring.internal.ICastMirroringService"

    invoke-virtual {p2, v0}, Landroid/os/Parcel;->enforceInterface(Ljava/lang/String;)V

    .line 114
    invoke-virtual {p2}, Landroid/os/Parcel;->readStrongBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/cast_mirroring/b/b;->a(Landroid/os/IBinder;)Lcom/google/android/gms/cast_mirroring/b/a;

    move-result-object v0

    .line 115
    invoke-virtual {p0, v0}, Lcom/google/android/gms/cast_mirroring/b/e;->b(Lcom/google/android/gms/cast_mirroring/b/a;)V

    goto/16 :goto_0

    .line 43
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0x3 -> :sswitch_3
        0x4 -> :sswitch_4
        0x5 -> :sswitch_5
        0x6 -> :sswitch_6
        0x5f4e5446 -> :sswitch_0
    .end sparse-switch
.end method
