.class public final Lcom/google/android/gms/games/service/GamesSyncServiceNotification;
.super Landroid/app/Service;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/Object;

.field private static b:Lcom/google/android/gms/games/service/ac;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 16
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->a:Ljava/lang/Object;

    .line 18
    const/4 v0, 0x0

    sput-object v0, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->b:Lcom/google/android/gms/games/service/ac;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    return-void
.end method


# virtual methods
.method public final onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 32
    sget-object v0, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->b:Lcom/google/android/gms/games/service/ac;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/ac;->getSyncAdapterBinder()Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate()V
    .locals 3

    .prologue
    .line 22
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 23
    sget-object v1, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 24
    :try_start_0
    sget-object v0, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->b:Lcom/google/android/gms/games/service/ac;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/google/android/gms/games/service/ac;

    invoke-virtual {p0}, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/games/service/ac;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/gms/games/service/GamesSyncServiceNotification;->b:Lcom/google/android/gms/games/service/ac;

    .line 27
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
