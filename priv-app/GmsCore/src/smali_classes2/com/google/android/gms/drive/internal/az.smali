.class final Lcom/google/android/gms/drive/internal/az;
.super Lcom/google/android/gms/drive/internal/c;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/common/api/m;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/api/m;)V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Lcom/google/android/gms/drive/internal/c;-><init>()V

    .line 338
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/az;->a:Lcom/google/android/gms/common/api/m;

    .line 339
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 3

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/az;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bc;

    const/4 v2, 0x0

    invoke-direct {v1, p1, v2}, Lcom/google/android/gms/drive/internal/bc;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/q;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 352
    return-void
.end method

.method public final a(Lcom/google/android/gms/drive/internal/OnDriveIdResponse;)V
    .locals 5

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/az;->a:Lcom/google/android/gms/common/api/m;

    new-instance v1, Lcom/google/android/gms/drive/internal/bc;

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    new-instance v3, Lcom/google/android/gms/drive/internal/av;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/OnDriveIdResponse;->a()Lcom/google/android/gms/drive/DriveId;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/gms/drive/internal/av;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/internal/bc;-><init>(Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/drive/q;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    .line 346
    return-void
.end method
