.class public abstract Lcom/google/android/gms/cast/internal/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/cast/internal/n;

.field protected final m:Lcom/google/android/gms/cast/internal/k;

.field public final n:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace cannot be null or empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_0
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x80

    if-le v0, v1, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid namespace length"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    const-string v0, "urn:x-cast:"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_2

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace must begin with the prefix \"urn:x-cast:\""

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_2
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0xb

    if-ne v0, v1, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Namespace must begin with the prefix \"urn:x-cast:\" and have non-empty suffix"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 31
    :cond_3
    iput-object p1, p0, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    .line 32
    new-instance v0, Lcom/google/android/gms/cast/internal/k;

    invoke-direct {v0, p2}, Lcom/google/android/gms/cast/internal/k;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/cast/internal/b;->m:Lcom/google/android/gms/cast/internal/k;

    .line 33
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 34
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->m:Lcom/google/android/gms/cast/internal/k;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;)V

    .line 36
    :cond_4
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/cast/internal/n;)V
    .locals 1

    .prologue
    .line 49
    iput-object p1, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    if-nez v0, :cond_0

    .line 51
    invoke-virtual {p0}, Lcom/google/android/gms/cast/internal/b;->d()V

    .line 53
    :cond_0
    return-void
.end method

.method protected final a(Ljava/lang/String;J)V
    .locals 8

    .prologue
    const/4 v6, 0x0

    .line 95
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object v6, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 96
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    iget-object v2, p0, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    move-object v3, p1

    move-wide v4, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/cast/internal/n;->b(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 97
    return-void
.end method

.method protected final a(Ljava/lang/String;JLjava/lang/String;)V
    .locals 8

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Sending text message: %s to: %s"

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    const/4 v3, 0x1

    aput-object p4, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 67
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    iget-object v2, p0, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    move-object v3, p1

    move-wide v4, p2

    move-object v6, p4

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/cast/internal/n;->a(Ljava/lang/String;Ljava/lang/String;JLjava/lang/String;)V

    .line 68
    return-void
.end method

.method public a([B)V
    .locals 0

    .prologue
    .line 139
    return-void
.end method

.method protected final a([BLjava/lang/String;)V
    .locals 7

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->m:Lcom/google/android/gms/cast/internal/k;

    const-string v1, "Sending binary message to: %s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/cast/internal/k;->a(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 81
    iget-object v1, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    iget-object v2, p0, Lcom/google/android/gms/cast/internal/b;->n:Ljava/lang/String;

    const-wide/16 v4, 0x0

    move-object v3, p1

    move-object v6, p2

    invoke-interface/range {v1 .. v6}, Lcom/google/android/gms/cast/internal/n;->a(Ljava/lang/String;[BJLjava/lang/String;)V

    .line 82
    return-void
.end method

.method public a_(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 120
    return-void
.end method

.method protected final c()J
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/cast/internal/b;->a:Lcom/google/android/gms/cast/internal/n;

    invoke-interface {v0}, Lcom/google/android/gms/cast/internal/n;->a()J

    move-result-wide v0

    return-wide v0
.end method

.method public d()V
    .locals 0

    .prologue
    .line 154
    return-void
.end method
