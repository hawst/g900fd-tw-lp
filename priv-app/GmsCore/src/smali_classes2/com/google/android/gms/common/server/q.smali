.class public Lcom/google/android/gms/common/server/q;
.super Lcom/google/android/gms/common/server/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 39
    invoke-direct/range {p0 .. p7}, Lcom/google/android/gms/common/server/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/String;Ljava/lang/String;)V

    .line 41
    return-void
.end method

.method private a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    .locals 13

    .prologue
    .line 119
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/q;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v9

    .line 120
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v10

    .line 121
    invoke-static {}, Lcom/android/volley/toolbox/aa;->a()Lcom/android/volley/toolbox/aa;

    move-result-object v7

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/common/server/q;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p1, v9}, Lcom/google/android/gms/common/server/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v12

    .line 123
    move/from16 v0, p3

    invoke-static {v0, v12}, Lcom/google/android/gms/common/server/q;->a(ILjava/util/Map;)I

    move-result v3

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/q;->b()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p4

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/common/server/q;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 125
    new-instance v1, Lcom/google/android/gms/common/server/z;

    const/4 v2, 0x0

    iget-boolean v11, p0, Lcom/google/android/gms/common/server/q;->b:Z

    move-object/from16 v5, p5

    move-object/from16 v6, p6

    move-object v8, v7

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/common/server/z;-><init>(IILjava/lang/String;[BLjava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    invoke-direct {p0, v1, v9}, Lcom/google/android/gms/common/server/q;->b(Lcom/android/volley/p;Ljava/lang/String;)V

    .line 128
    :try_start_0
    invoke-virtual {v7}, Lcom/android/volley/toolbox/aa;->get()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v1

    return-object v1

    .line 130
    :catch_0
    move-exception v1

    new-instance v1, Lcom/android/volley/ac;

    const-string v2, "Thread interrupted"

    invoke-direct {v1, v2}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    throw v1

    .line 131
    :catch_1
    move-exception v1

    .line 132
    invoke-virtual {p0, v1, v9}, Lcom/google/android/gms/common/server/q;->a(Ljava/util/concurrent/ExecutionException;Ljava/lang/String;)V

    .line 133
    new-instance v2, Lcom/android/volley/ac;

    const-string v3, "Error executing network request"

    invoke-direct {v2, v3, v1}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2
.end method

.method private b(Lcom/android/volley/p;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 271
    invoke-virtual {p0, p1, p2}, Lcom/google/android/gms/common/server/q;->a(Lcom/android/volley/p;Ljava/lang/String;)V

    .line 272
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/app/GmsApplication;->c()Lcom/android/volley/s;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/android/volley/s;->a(Lcom/android/volley/p;)Lcom/android/volley/p;

    .line 273
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 89
    const/4 v2, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    :try_start_0
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Lcom/android/volley/ac; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    .line 91
    :catch_0
    move-exception v0

    .line 92
    invoke-static {v0}, Lcom/google/android/gms/common/server/q;->a(Lcom/android/volley/ac;)Z

    move-result v1

    if-eqz v1, :cond_0

    move-object v0, p0

    move-object v1, p1

    move v2, v7

    move v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    .line 94
    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;IILjava/lang/String;[BLjava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 97
    :cond_0
    throw v0
.end method

.method public bridge synthetic a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;[BLjava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;)V
    .locals 13

    .prologue
    .line 58
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/server/q;->a(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v9

    .line 59
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v10

    .line 60
    if-eqz v9, :cond_0

    .line 61
    iget-object v1, p0, Lcom/google/android/gms/common/server/q;->a:Landroid/content/Context;

    invoke-virtual {p0, v1, p1, v9}, Lcom/google/android/gms/common/server/q;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Ljava/util/HashMap;

    move-result-object v12

    .line 62
    const/4 v1, 0x1

    invoke-static {v1, v12}, Lcom/google/android/gms/common/server/q;->a(ILjava/util/Map;)I

    move-result v3

    .line 63
    invoke-virtual {p0}, Lcom/google/android/gms/common/server/q;->b()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v1, p2}, Lcom/google/android/gms/common/server/q;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 64
    new-instance v1, Lcom/google/android/gms/common/server/z;

    const/4 v2, 0x0

    iget-boolean v11, p0, Lcom/google/android/gms/common/server/q;->b:Z

    move-object/from16 v5, p3

    move-object/from16 v6, p4

    move-object/from16 v7, p5

    move-object/from16 v8, p6

    invoke-direct/range {v1 .. v12}, Lcom/google/android/gms/common/server/z;-><init>(IILjava/lang/String;[BLjava/lang/Object;Lcom/android/volley/x;Lcom/android/volley/w;Ljava/lang/String;Ljava/lang/String;ZLjava/util/HashMap;)V

    invoke-direct {p0, v1, v9}, Lcom/google/android/gms/common/server/q;->b(Lcom/android/volley/p;Ljava/lang/String;)V

    .line 70
    :goto_0
    return-void

    .line 68
    :cond_0
    new-instance v1, Lcom/android/volley/ac;

    const-string v2, "Unable to obtain auth token - is the device online?"

    invoke-direct {v1, v2}, Lcom/android/volley/ac;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p6

    invoke-interface {v0, v1}, Lcom/android/volley/w;->a(Lcom/android/volley/ac;)V

    goto :goto_0
.end method

.method public final bridge synthetic b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->b(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final bridge synthetic d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 22
    invoke-super {p0, p1}, Lcom/google/android/gms/common/server/a;->d(Lcom/google/android/gms/common/server/ClientContext;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
