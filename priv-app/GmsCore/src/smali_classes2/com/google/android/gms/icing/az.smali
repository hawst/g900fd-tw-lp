.class public final Lcom/google/android/gms/icing/az;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile d:[Lcom/google/android/gms/icing/az;


# instance fields
.field public a:I

.field public b:Lcom/google/android/gms/icing/ba;

.field public c:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 1199
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 1200
    iput v1, p0, Lcom/google/android/gms/icing/az;->a:I

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    iput v1, p0, Lcom/google/android/gms/icing/az;->c:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/az;->cachedSize:I

    .line 1201
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/az;
    .locals 2

    .prologue
    .line 1179
    sget-object v0, Lcom/google/android/gms/icing/az;->d:[Lcom/google/android/gms/icing/az;

    if-nez v0, :cond_1

    .line 1180
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 1182
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/az;->d:[Lcom/google/android/gms/icing/az;

    if-nez v0, :cond_0

    .line 1183
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/az;

    sput-object v0, Lcom/google/android/gms/icing/az;->d:[Lcom/google/android/gms/icing/az;

    .line 1185
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1187
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/az;->d:[Lcom/google/android/gms/icing/az;

    return-object v0

    .line 1185
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 1265
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 1266
    iget v1, p0, Lcom/google/android/gms/icing/az;->a:I

    if-eqz v1, :cond_0

    .line 1267
    const/4 v1, 0x1

    iget v2, p0, Lcom/google/android/gms/icing/az;->a:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1270
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-eqz v1, :cond_1

    .line 1271
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 1274
    :cond_1
    iget v1, p0, Lcom/google/android/gms/icing/az;->c:I

    if-eqz v1, :cond_2

    .line 1275
    const/4 v1, 0x3

    iget v2, p0, Lcom/google/android/gms/icing/az;->c:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 1278
    :cond_2
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 1213
    if-ne p1, p0, :cond_1

    .line 1235
    :cond_0
    :goto_0
    return v0

    .line 1216
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/az;

    if-nez v2, :cond_2

    move v0, v1

    .line 1217
    goto :goto_0

    .line 1219
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/az;

    .line 1220
    iget v2, p0, Lcom/google/android/gms/icing/az;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/az;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 1221
    goto :goto_0

    .line 1223
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-nez v2, :cond_4

    .line 1224
    iget-object v2, p1, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-eqz v2, :cond_5

    move v0, v1

    .line 1225
    goto :goto_0

    .line 1228
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    iget-object v3, p1, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/icing/ba;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_5

    move v0, v1

    .line 1229
    goto :goto_0

    .line 1232
    :cond_5
    iget v2, p0, Lcom/google/android/gms/icing/az;->c:I

    iget v3, p1, Lcom/google/android/gms/icing/az;->c:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 1233
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 1240
    iget v0, p0, Lcom/google/android/gms/icing/az;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 1242
    mul-int/lit8 v1, v0, 0x1f

    iget-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/2addr v0, v1

    .line 1244
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/az;->c:I

    add-int/2addr v0, v1

    .line 1245
    return v0

    .line 1242
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/ba;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 448
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/az;->a:I

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/icing/ba;

    invoke-direct {v0}, Lcom/google/android/gms/icing/ba;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/az;->c:I

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 1251
    iget v0, p0, Lcom/google/android/gms/icing/az;->a:I

    if-eqz v0, :cond_0

    .line 1252
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/az;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 1254
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    if-eqz v0, :cond_1

    .line 1255
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 1257
    :cond_1
    iget v0, p0, Lcom/google/android/gms/icing/az;->c:I

    if-eqz v0, :cond_2

    .line 1258
    const/4 v0, 0x3

    iget v1, p0, Lcom/google/android/gms/icing/az;->c:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 1260
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 1261
    return-void
.end method
