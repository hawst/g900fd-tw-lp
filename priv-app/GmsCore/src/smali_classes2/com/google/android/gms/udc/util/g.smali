.class final Lcom/google/android/gms/udc/util/g;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/android/volley/toolbox/t;


# instance fields
.field private final a:Landroid/widget/ImageView;

.field private final b:Landroid/content/res/Resources;


# direct methods
.method constructor <init>(Landroid/widget/ImageView;Landroid/content/res/Resources;)V
    .locals 0

    .prologue
    .line 323
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 324
    iput-object p1, p0, Lcom/google/android/gms/udc/util/g;->a:Landroid/widget/ImageView;

    .line 325
    iput-object p2, p0, Lcom/google/android/gms/udc/util/g;->b:Landroid/content/res/Resources;

    .line 326
    return-void
.end method


# virtual methods
.method public final a(Lcom/android/volley/ac;)V
    .locals 3

    .prologue
    .line 331
    const-string v0, "UdcUiUtil"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error loading background image: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/android/volley/ac;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 332
    return-void
.end method

.method public final a(Lcom/android/volley/toolbox/s;Z)V
    .locals 4
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 337
    const/16 v0, 0x10

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 338
    iget-object v0, p0, Lcom/google/android/gms/udc/util/g;->a:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/gms/udc/util/g;->b:Landroid/content/res/Resources;

    iget-object v3, p1, Lcom/android/volley/toolbox/s;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 343
    :goto_0
    return-void

    .line 340
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/udc/util/g;->a:Landroid/widget/ImageView;

    new-instance v1, Landroid/graphics/drawable/BitmapDrawable;

    iget-object v2, p0, Lcom/google/android/gms/udc/util/g;->b:Landroid/content/res/Resources;

    iget-object v3, p1, Lcom/android/volley/toolbox/s;->a:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2, v3}, Landroid/graphics/drawable/BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method
