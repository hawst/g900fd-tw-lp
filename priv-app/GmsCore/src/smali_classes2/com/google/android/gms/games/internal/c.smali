.class public final Lcom/google/android/gms/games/internal/c;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field a:Lcom/google/android/gms/games/internal/d/d;

.field private final g:Ljava/lang/String;

.field private final h:Ljava/lang/String;

.field private final i:Ljava/util/Map;

.field private j:Lcom/google/android/gms/games/PlayerEntity;

.field private k:Lcom/google/android/gms/games/GameEntity;

.field private final l:Lcom/google/android/gms/games/internal/ek;

.field private m:Z

.field private final n:Landroid/os/Binder;

.field private final o:J

.field private final p:Lcom/google/android/gms/games/h;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;ILandroid/view/View;Lcom/google/android/gms/games/h;)V
    .locals 8

    .prologue
    .line 377
    move-object v2, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-direct/range {v2 .. v7}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 183
    new-instance v2, Lcom/google/android/gms/games/internal/d;

    invoke-direct {v2, p0}, Lcom/google/android/gms/games/internal/d;-><init>(Lcom/google/android/gms/games/internal/c;)V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    .line 323
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    .line 378
    iput-object p3, p0, Lcom/google/android/gms/games/internal/c;->g:Ljava/lang/String;

    .line 379
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->h:Ljava/lang/String;

    .line 380
    new-instance v2, Landroid/os/Binder;

    invoke-direct {v2}, Landroid/os/Binder;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->n:Landroid/os/Binder;

    .line 381
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->i:Ljava/util/Map;

    .line 382
    move/from16 v0, p8

    invoke-static {p0, v0}, Lcom/google/android/gms/games/internal/ek;->a(Lcom/google/android/gms/games/internal/c;I)Lcom/google/android/gms/games/internal/ek;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/internal/ek;

    .line 383
    iget-object v2, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/internal/ek;

    move-object/from16 v0, p9

    invoke-virtual {v2, v0}, Lcom/google/android/gms/games/internal/ek;->a(Landroid/view/View;)V

    .line 384
    invoke-virtual {p0}, Ljava/lang/Object;->hashCode()I

    move-result v2

    int-to-long v2, v2

    iput-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    .line 385
    move-object/from16 v0, p10

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    .line 387
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/x;)V

    .line 388
    invoke-virtual {p0, p0}, Lcom/google/android/gms/games/internal/c;->a(Lcom/google/android/gms/common/api/y;)V

    .line 389
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 1

    .prologue
    .line 154
    invoke-static {p0}, Lcom/google/android/gms/games/internal/c;->b(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;

    move-result-object v0

    return-object v0
.end method

.method private static b(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 3

    .prologue
    .line 6775
    new-instance v1, Lcom/google/android/gms/games/multiplayer/realtime/c;

    invoke-direct {v1, p0}, Lcom/google/android/gms/games/multiplayer/realtime/c;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 6777
    const/4 v0, 0x0

    .line 6779
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->c()I

    move-result v2

    if-lez v2, :cond_0

    .line 6780
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/multiplayer/realtime/c;->a(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;

    invoke-interface {v0}, Lcom/google/android/gms/games/multiplayer/realtime/Room;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/multiplayer/realtime/Room;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 6783
    :cond_0
    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    .line 6785
    return-object v0

    .line 6783
    :catchall_0
    move-exception v0

    invoke-virtual {v1}, Lcom/google/android/gms/games/multiplayer/realtime/c;->w_()V

    throw v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 5981
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/achievement/AchievementEntity;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5986
    :goto_0
    return-object v0

    .line 5983
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5984
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 5926
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/multiplayer/ZInvitationCluster;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5932
    :goto_0
    return-object v0

    .line 5929
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5930
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 4746
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/request/GameRequestCluster;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4752
    :goto_0
    return-object v0

    .line 4749
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4750
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 3459
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    const/4 v1, -0x1

    invoke-interface {v0, p1, v1}, Lcom/google/android/gms/games/internal/du;->f(Ljava/lang/String;I)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3464
    :goto_0
    return-object v0

    .line 3461
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3462
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;)Landroid/content/Intent;
    .locals 7

    .prologue
    .line 5958
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move-object v6, p2

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a([Lcom/google/android/gms/games/multiplayer/ParticipantEntity;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 5964
    :goto_0
    return-object v0

    .line 5961
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 5962
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 154
    invoke-static {p1}, Lcom/google/android/gms/games/internal/dv;->a(Landroid/os/IBinder;)Lcom/google/android/gms/games/internal/du;

    move-result-object v0

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 480
    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/GameEntity;

    .line 481
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->a()V

    .line 482
    return-void
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 6790
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    .line 6791
    const-class v0, Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 6792
    const-string v0, "show_welcome_popup"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    .line 6793
    const-string v0, "com.google.android.gms.games.current_player"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;

    .line 6794
    const-string v0, "com.google.android.gms.games.current_game"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/GameEntity;

    .line 6796
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 6797
    return-void
.end method

.method public final a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 6750
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6752
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->a(Landroid/os/IBinder;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6757
    :cond_0
    :goto_0
    return-void

    .line 6754
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/aj;)V
    .locals 4

    .prologue
    .line 3842
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/y;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/y;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 3843
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3847
    :goto_0
    return-void

    .line 3845
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6163
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/y;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/y;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 6164
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6168
    :goto_0
    return-void

    .line 6166
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 4063
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/u;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/u;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;)V

    .line 4064
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;I)V
    .locals 2

    .prologue
    .line 4118
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ab;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ab;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;I)V

    .line 4120
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;III)V
    .locals 2

    .prologue
    .line 4877
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bs;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bs;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;III)V

    .line 4879
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;IIZZ)V
    .locals 6

    .prologue
    .line 5422
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/p;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/p;-><init>(Lcom/google/android/gms/common/api/m;)V

    move v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;IIZZ)V

    .line 5425
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;IZZ)V
    .locals 2

    .prologue
    .line 3322
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;IZZ)V

    .line 3324
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;I[I)V
    .locals 2

    .prologue
    .line 4414
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cf;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cf;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;I[I)V

    .line 4417
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Lcom/google/android/gms/games/e/f;II)V
    .locals 3

    .prologue
    .line 3587
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ae;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ae;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-virtual {p2}, Lcom/google/android/gms/games/e/f;->a()Lcom/google/android/gms/games/e/g;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/gms/games/e/g;->a:Landroid/os/Bundle;

    invoke-interface {v0, v1, v2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Landroid/os/Bundle;II)V

    .line 3589
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4226
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cd;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cd;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->l(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 4228
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 6093
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ab;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ab;-><init>(Lcom/google/android/gms/common/api/m;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, p2, p3, v2}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZ)V

    .line 6096
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IIIZ)V
    .locals 7

    .prologue
    .line 3544
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ae;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ae;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIIZ)V

    .line 3546
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;IZZ)V
    .locals 6

    .prologue
    .line 5769
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IZZ)V

    .line 5771
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;I[I)V
    .locals 2

    .prologue
    .line 6136
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cf;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cf;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;I[I)V

    .line 6139
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6002
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cd;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cd;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V

    .line 6004
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;III)V
    .locals 7

    .prologue
    .line 6625
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bs;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bs;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;III)V

    .line 6627
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;IIIZ)V
    .locals 8

    .prologue
    .line 5606
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ae;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ae;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IIIZ)V

    .line 5609
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;IZ)V
    .locals 7

    .prologue
    const/4 v5, 0x0

    .line 3394
    const/4 v0, -0x1

    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    :cond_0
    :goto_0
    packed-switch v0, :pswitch_data_0

    .line 3400
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Invalid player collection: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 3394
    :sswitch_0
    const-string v1, "circled"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    move v0, v5

    goto :goto_0

    :sswitch_1
    const-string v1, "played_with"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    goto :goto_0

    :sswitch_2
    const-string v1, "nearby"

    invoke-virtual {p2, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x2

    goto :goto_0

    .line 3402
    :pswitch_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v6, p5

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;IZZ)V

    .line 3404
    return-void

    .line 3394
    :sswitch_data_0
    .sparse-switch
        -0x3e8dd581 -> :sswitch_2
        0x9529ab2 -> :sswitch_1
        0x2eaadd94 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 5904
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/i;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/i;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;Z)V

    .line 5907
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Z[Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 5104
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 5105
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bn;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bn;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p5

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V

    .line 5107
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;[IIZ)V
    .locals 7

    .prologue
    .line 5082
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 5083
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bn;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bn;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[IIZ)V

    .line 5085
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6597
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bt;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bt;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 6600
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 5559
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/af;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/af;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 5561
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;[Ljava/lang/String;I[BI)V
    .locals 7

    .prologue
    .line 6580
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/br;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/br;-><init>(Lcom/google/android/gms/common/api/m;)V

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;[Ljava/lang/String;I[BI)V

    .line 6582
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 3418
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->c(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 3420
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;ZLandroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 6508
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/m;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/m;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;ZLandroid/os/Bundle;)V

    .line 6511
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Z[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5042
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 5043
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bn;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bn;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p3, p2}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;Z)V

    .line 5045
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;[IIZ)V
    .locals 2

    .prologue
    .line 5025
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 5026
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bn;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bn;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3, p4}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;[IIZ)V

    .line 5028
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/m;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 3298
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bh;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->c(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 3299
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 430
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    .line 431
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 10

    .prologue
    .line 543
    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget-object v0, v0, Landroid/content/res/Configuration;->locale:Ljava/util/Locale;

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    .line 545
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 546
    const-string v0, "com.google.android.gms.games.key.isHeadless"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget-boolean v1, v1, Lcom/google/android/gms/games/h;->a:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 547
    const-string v0, "com.google.android.gms.games.key.showConnectingPopup"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget-boolean v1, v1, Lcom/google/android/gms/games/h;->b:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 548
    const-string v0, "com.google.android.gms.games.key.connectingPopupGravity"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget v1, v1, Lcom/google/android/gms/games/h;->c:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 549
    const-string v0, "com.google.android.gms.games.key.retryingSignIn"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget-boolean v1, v1, Lcom/google/android/gms/games/h;->d:Z

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 550
    const-string v0, "com.google.android.gms.games.key.sdkVariant"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget v1, v1, Lcom/google/android/gms/games/h;->e:I

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 551
    const-string v0, "com.google.android.gms.games.key.forceResolveAccountKey"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget-object v1, v1, Lcom/google/android/gms/games/h;->f:Ljava/lang/String;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    const-string v0, "com.google.android.gms.games.key.proxyApis"

    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->p:Lcom/google/android/gms/games/h;

    iget-object v1, v1, Lcom/google/android/gms/games/h;->g:Ljava/util/ArrayList;

    invoke-virtual {v9, v0, v1}, Landroid/os/Bundle;->putStringArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 554
    const v2, 0x6768a8

    iget-object v0, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/internal/c;->h:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/common/internal/aj;->d:[Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/gms/games/internal/c;->g:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/ek;->b()Landroid/os/IBinder;

    move-result-object v7

    move-object v0, p1

    move-object v1, p2

    invoke-interface/range {v0 .. v9}, Lcom/google/android/gms/common/internal/bj;->a(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Landroid/os/IBinder;Ljava/lang/String;Landroid/os/Bundle;)V

    .line 558
    return-void
.end method

.method public final a(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 4133
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->b(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4137
    :goto_0
    return-void

    .line 4135
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6070
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->b(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6074
    :goto_0
    return-void

    .line 6072
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6034
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/internal/du;->a(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6038
    :goto_0
    return-void

    .line 6036
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 3366
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->c(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3370
    :goto_0
    return-void

    .line 3368
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final varargs a([Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v4, 0x1

    const/4 v1, 0x0

    .line 456
    move v0, v1

    move v2, v1

    move v3, v1

    .line 458
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    .line 459
    aget-object v5, p1, v0

    .line 460
    const-string v6, "https://www.googleapis.com/auth/games"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    move v3, v4

    .line 458
    :cond_0
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 462
    :cond_1
    const-string v6, "https://www.googleapis.com/auth/games.firstparty"

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v2, v4

    .line 463
    goto :goto_1

    .line 468
    :cond_2
    if-eqz v2, :cond_4

    .line 469
    if-nez v3, :cond_3

    move v0, v4

    :goto_2
    const-string v2, "Cannot have both %s and %s!"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const-string v5, "https://www.googleapis.com/auth/games"

    aput-object v5, v3, v1

    const-string v1, "https://www.googleapis.com/auth/games.firstparty"

    aput-object v1, v3, v4

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    .line 474
    :goto_3
    return-void

    :cond_3
    move v0, v1

    .line 469
    goto :goto_2

    .line 472
    :cond_4
    const-string v0, "Games APIs requires %s to function."

    new-array v2, v4, [Ljava/lang/Object;

    const-string v4, "https://www.googleapis.com/auth/games"

    aput-object v4, v2, v1

    invoke-static {v3, v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/String;[Ljava/lang/Object;)V

    goto :goto_3
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 508
    const-string v0, "com.google.android.gms.games.service.START"

    return-object v0
.end method

.method public final b()V
    .locals 4

    .prologue
    .line 487
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    .line 490
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 492
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    .line 493
    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->c()V

    .line 494
    iget-object v1, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v1}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 495
    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/du;->a(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 502
    :cond_0
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_1
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    goto :goto_1

    .line 497
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "Failed to notify client disconnect."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 502
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->i:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->clear()V

    .line 503
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->b()V

    .line 504
    return-void
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 4701
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4705
    :goto_0
    return-void

    .line 4703
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/aj;)V
    .locals 4

    .prologue
    .line 3876
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/au;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/au;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 3877
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3881
    :goto_0
    return-void

    .line 3879
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6208
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/au;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/au;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 6209
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6213
    :goto_0
    return-void

    .line 6211
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 4077
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 4078
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ca;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ca;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;)V

    .line 4079
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4239
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cd;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cd;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->m(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 4241
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6017
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cd;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cd;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->e(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Ljava/lang/String;)V

    .line 6019
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Z)V
    .locals 2

    .prologue
    .line 6446
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/s;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/s;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;Z)V

    .line 6448
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 3479
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/af;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/af;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 3481
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4855
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bt;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bt;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->b(Lcom/google/android/gms/games/internal/dr;[Ljava/lang/String;)V

    .line 4857
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4387
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->f(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4391
    :goto_0
    return-void

    .line 4389
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 4149
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->a(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 4153
    :goto_0
    return-void

    .line 4151
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6556
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6560
    :goto_0
    return-void

    .line 6558
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6054
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/games/internal/du;->b(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6058
    :goto_0
    return-void

    .line 6056
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final b(Z)V
    .locals 2

    .prologue
    .line 6399
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/du;->a(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6403
    :goto_0
    return-void

    .line 6401
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 513
    const-string v0, "com.google.android.gms.games.internal.IGamesService"

    return-object v0
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 417
    iget-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    if-eqz v0, :cond_0

    .line 418
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->l:Lcom/google/android/gms/games/internal/ek;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/ek;->a()V

    .line 419
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/games/internal/c;->m:Z

    .line 421
    :cond_0
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/aj;)V
    .locals 4

    .prologue
    .line 3906
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/bm;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bm;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 3907
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3911
    :goto_0
    return-void

    .line 3909
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6253
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/bm;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bm;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 6254
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6258
    :goto_0
    return-void

    .line 6256
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Lcom/google/android/gms/common/api/m;)V
    .locals 2

    .prologue
    .line 6328
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ax;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ax;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/internal/du;->h(Lcom/google/android/gms/games/internal/dr;)V

    .line 6329
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 4991
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->a:Lcom/google/android/gms/games/internal/d/d;

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/d/d;->b()V

    .line 4992
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bk;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bk;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->u(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 4993
    return-void
.end method

.method public final c(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 3659
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/i;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/i;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->a(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 3661
    return-void
.end method

.method public final c(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6180
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/du;->a(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6184
    :goto_0
    return-void

    .line 6182
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Ljava/lang/String;I)V
    .locals 2

    .prologue
    .line 6355
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1, p2}, Lcom/google/android/gms/games/internal/du;->c(Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6359
    :goto_0
    return-void

    .line 6357
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final c(Z)V
    .locals 2

    .prologue
    .line 6429
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->b(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6433
    :goto_0
    return-void

    .line 6431
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/aj;)V
    .locals 4

    .prologue
    .line 3938
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/bo;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bo;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 3939
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3}, Lcom/google/android/gms/games/internal/du;->c(Lcom/google/android/gms/games/internal/dr;J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3943
    :goto_0
    return-void

    .line 3941
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6297
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/bo;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bo;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 6298
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v1, v2, v3, p2}, Lcom/google/android/gms/games/internal/du;->c(Lcom/google/android/gms/games/internal/dr;JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6302
    :goto_0
    return-void

    .line 6300
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d(Lcom/google/android/gms/common/api/m;)V
    .locals 3

    .prologue
    .line 6522
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/w;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/w;-><init>(Lcom/google/android/gms/common/api/m;)V

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/du;->t(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 6524
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 5275
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cb;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cb;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->r(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 5277
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 5190
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/cc;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/cc;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->d(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 5192
    return-void
.end method

.method public final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6225
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/du;->b(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6229
    :goto_0
    return-void

    .line 6227
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final d_()Landroid/os/Bundle;
    .locals 2

    .prologue
    .line 524
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->b()Landroid/os/Bundle;

    move-result-object v0

    .line 525
    if-eqz v0, :cond_0

    .line 526
    const-class v1, Lcom/google/android/gms/games/internal/c;

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 531
    :cond_0
    :goto_0
    return-object v0

    .line 530
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 531
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/common/api/aj;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/Room;
    .locals 2

    .prologue
    .line 6678
    :try_start_0
    new-instance v1, Lcom/google/android/gms/games/internal/bv;

    invoke-direct {v1, p1, p1}, Lcom/google/android/gms/games/internal/bv;-><init>(Lcom/google/android/gms/common/api/aj;Lcom/google/android/gms/common/api/aj;)V

    .line 6682
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->h(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)Lcom/google/android/gms/games/multiplayer/realtime/RoomEntity;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6685
    :goto_0
    return-object v0

    .line 6684
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6685
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 6340
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/ay;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/ay;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->j(Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V

    .line 6342
    return-void
.end method

.method public final e(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 5872
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bi;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bi;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->g(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 5874
    return-void
.end method

.method public final e(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6269
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/du;->d(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6273
    :goto_0
    return-void

    .line 6271
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3166
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->d()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3169
    :goto_0
    return-object v0

    .line 3168
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3169
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final f(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 5885
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/bj;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/bj;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->h(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 5887
    return-void
.end method

.method public final f(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 6314
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3, p1}, Lcom/google/android/gms/games/internal/du;->c(JLjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6318
    :goto_0
    return-void

    .line 6316
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 426
    return-void
.end method

.method public final g()Ljava/lang/String;
    .locals 2

    .prologue
    .line 3178
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;

    if-eqz v0, :cond_0

    .line 3179
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->a()Ljava/lang/String;

    move-result-object v0

    .line 3187
    :goto_0
    return-object v0

    .line 3184
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->e()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 3186
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3187
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 6537
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 6540
    :goto_0
    return-object v0

    .line 6539
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6540
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g(Lcom/google/android/gms/common/api/m;Z)V
    .locals 2

    .prologue
    .line 6491
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    new-instance v1, Lcom/google/android/gms/games/internal/l;

    invoke-direct {v1, p1}, Lcom/google/android/gms/games/internal/l;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/games/internal/du;->e(Lcom/google/android/gms/games/internal/dr;Z)V

    .line 6493
    return-void
.end method

.method public final h(Ljava/lang/String;)I
    .locals 2

    .prologue
    .line 6700
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/du;->d(Ljava/lang/String;)I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6703
    :goto_0
    return v0

    .line 6702
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 6703
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public final l()Lcom/google/android/gms/games/Player;
    .locals 2

    .prologue
    .line 3195
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->j()V

    .line 3196
    monitor-enter p0

    .line 3197
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 3200
    :try_start_1
    new-instance v1, Lcom/google/android/gms/games/t;

    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->f()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3202
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 3203
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/t;->b(I)Lcom/google/android/gms/games/Player;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3206
    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3212
    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3215
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->j:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0

    .line 3206
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcom/google/android/gms/games/t;->w_()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3209
    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 3212
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final m()Lcom/google/android/gms/games/Game;
    .locals 2

    .prologue
    .line 3223
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->j()V

    .line 3224
    monitor-enter p0

    .line 3225
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/GameEntity;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    if-nez v0, :cond_1

    .line 3228
    :try_start_1
    new-instance v1, Lcom/google/android/gms/games/a;

    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->h()Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 3230
    :try_start_2
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->c()I

    move-result v0

    if-lez v0, :cond_0

    .line 3231
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/a;->b(I)Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/GameEntity;

    iput-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/GameEntity;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 3234
    :cond_0
    :try_start_3
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V
    :try_end_3
    .catch Landroid/os/RemoteException; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 3240
    :cond_1
    :goto_0
    :try_start_4
    monitor-exit p0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    .line 3243
    iget-object v0, p0, Lcom/google/android/gms/games/internal/c;->k:Lcom/google/android/gms/games/GameEntity;

    return-object v0

    .line 3234
    :catchall_0
    move-exception v0

    :try_start_5
    invoke-virtual {v1}, Lcom/google/android/gms/games/a;->w_()V

    throw v0
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 3237
    :catch_0
    move-exception v0

    :try_start_6
    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_1

    goto :goto_0

    .line 3240
    :catchall_1
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final n()V
    .locals 4

    .prologue
    .line 3855
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/du;->b(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3859
    :goto_0
    return-void

    .line 3857
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final o()V
    .locals 4

    .prologue
    .line 3889
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/du;->c(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3893
    :goto_0
    return-void

    .line 3891
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final p()V
    .locals 4

    .prologue
    .line 3918
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/du;->e(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3922
    :goto_0
    return-void

    .line 3920
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final q()V
    .locals 4

    .prologue
    .line 3951
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    iget-wide v2, p0, Lcom/google/android/gms/games/internal/c;->o:J

    invoke-interface {v0, v2, v3}, Lcom/google/android/gms/games/internal/du;->d(J)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 3955
    :goto_0
    return-void

    .line 3953
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final r()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 3978
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->o()Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 3983
    :goto_0
    return-object v0

    .line 3980
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 3981
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final s()Landroid/content/Intent;
    .locals 2

    .prologue
    .line 4048
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->p()Landroid/content/Intent;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4053
    :goto_0
    return-object v0

    .line 4050
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4051
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final t()Ljava/lang/String;
    .locals 2

    .prologue
    .line 4101
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->a()Ljava/lang/String;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 4104
    :goto_0
    return-object v0

    .line 4103
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 4104
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final u()I
    .locals 3

    .prologue
    .line 4802
    const/4 v1, -0x1

    .line 4804
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->s()I
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 4808
    :goto_0
    return v0

    .line 4806
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final v()V
    .locals 2

    .prologue
    .line 6367
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->j()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6371
    :goto_0
    return-void

    .line 6369
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final w()Z
    .locals 3

    .prologue
    .line 6381
    const/4 v1, 0x1

    .line 6383
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->g()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6387
    :goto_0
    return v0

    .line 6385
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final x()Z
    .locals 3

    .prologue
    .line 6412
    const/4 v1, 0x1

    .line 6414
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->y()Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v0

    .line 6418
    :goto_0
    return v0

    .line 6416
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v2, "service died"

    invoke-static {v0, v2}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    goto :goto_0
.end method

.method public final y()V
    .locals 2

    .prologue
    .line 6472
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->v()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6476
    :goto_0
    return-void

    .line 6474
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final z()V
    .locals 2

    .prologue
    .line 6765
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 6767
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/internal/c;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/du;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/du;->c()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 6772
    :cond_0
    :goto_0
    return-void

    .line 6769
    :catch_0
    move-exception v0

    const-string v0, "GamesClientImpl"

    const-string v1, "service died"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
