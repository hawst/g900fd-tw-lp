.class public final Lcom/google/android/gms/people/internal/p;
.super Lcom/google/android/gms/common/internal/aj;
.source "SourceFile"


# static fields
.field private static volatile i:Landroid/os/Bundle;

.field private static volatile j:Landroid/os/Bundle;


# instance fields
.field public final a:Ljava/lang/String;

.field public final g:Ljava/lang/String;

.field private final h:Ljava/util/HashMap;

.field private k:Ljava/lang/Long;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const/4 v0, 0x0

    new-array v5, v0, [Ljava/lang/String;

    move-object v0, p0

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/common/internal/aj;-><init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;[Ljava/lang/String;)V

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    .line 1928
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/people/internal/p;->k:Ljava/lang/Long;

    .line 141
    iput-object p5, p0, Lcom/google/android/gms/people/internal/p;->a:Ljava/lang/String;

    .line 142
    iput-object p6, p0, Lcom/google/android/gms/people/internal/p;->g:Ljava/lang/String;

    .line 143
    return-void
.end method

.method static synthetic a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/api/Status;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 94
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    if-nez p1, :cond_0

    move-object v0, v1

    :goto_0
    invoke-direct {v2, p0, v1, v0}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    return-object v2

    :cond_0
    const-string v0, "pendingIntent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/app/PendingIntent;

    goto :goto_0
.end method

.method private a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;)Lcom/google/android/gms/people/internal/ac;
    .locals 3

    .prologue
    .line 263
    iget-object v1, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    monitor-enter v1

    .line 264
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/ac;

    monitor-exit v1

    .line 272
    :goto_0
    return-object v0

    .line 269
    :cond_0
    new-instance v0, Lcom/google/android/gms/people/internal/ac;

    invoke-interface {p1, p2}, Lcom/google/android/gms/common/api/v;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/people/internal/ac;-><init>(Lcom/google/android/gms/common/api/aj;)V

    .line 271
    iget-object v2, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v2, p2, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 272
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 274
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/data/DataHolder;)Lcom/google/android/gms/people/model/k;
    .locals 4

    .prologue
    .line 94
    if-nez p0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Lcom/google/android/gms/people/model/k;

    new-instance v1, Lcom/google/android/gms/people/internal/a/d;

    sget-object v2, Lcom/google/android/gms/people/internal/p;->j:Landroid/os/Bundle;

    invoke-direct {v1, v2}, Lcom/google/android/gms/people/internal/a/d;-><init>(Landroid/os/Bundle;)V

    new-instance v2, Lcom/google/android/gms/people/internal/a/c;

    sget-object v3, Lcom/google/android/gms/people/internal/p;->i:Landroid/os/Bundle;

    invoke-direct {v2, v3}, Lcom/google/android/gms/people/internal/a/c;-><init>(Landroid/os/Bundle;)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/people/model/k;-><init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/people/internal/a/d;Lcom/google/android/gms/people/internal/a/c;)V

    goto :goto_0
.end method

.method private declared-synchronized a(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 232
    monitor-enter p0

    if-nez p1, :cond_0

    .line 240
    :goto_0
    monitor-exit p0

    return-void

    .line 235
    :cond_0
    :try_start_0
    const-string v0, "use_contactables_api"

    const/4 v1, 0x1

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/a/a;->a(Z)V

    .line 237
    sget-object v0, Lcom/google/android/gms/people/internal/o;->a:Lcom/google/android/gms/people/internal/o;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/people/internal/o;->a(Landroid/os/Bundle;)V

    .line 238
    const-string v0, "config.email_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/p;->i:Landroid/os/Bundle;

    .line 239
    const-string v0, "config.phone_type_map"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/people/internal/p;->j:Landroid/os/Bundle;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 232
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method protected final bridge synthetic a(Landroid/os/IBinder;)Landroid/os/IInterface;
    .locals 1

    .prologue
    .line 93
    invoke-static {p1}, Lcom/google/android/gms/people/internal/j;->a(Landroid/os/IBinder;)Lcom/google/android/gms/people/internal/i;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;)Lcom/google/android/gms/common/internal/bd;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 835
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 838
    new-instance v2, Lcom/google/android/gms/people/internal/aj;

    invoke-direct {v2, p1}, Lcom/google/android/gms/people/internal/aj;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 841
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    invoke-interface {v0, v2, p2}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;)Lcom/google/android/gms/common/internal/bd;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 847
    :goto_0
    return-object v0

    .line 845
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v1, v1, v1}, Lcom/google/android/gms/people/internal/aj;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    move-object v0, v1

    .line 847
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 779
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 782
    new-instance v2, Lcom/google/android/gms/people/internal/aj;

    invoke-direct {v2, p1}, Lcom/google/android/gms/people/internal/aj;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 785
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    invoke-interface {v0, v2, p2, p3, p4}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 791
    :goto_0
    return-object v0

    .line 789
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v2, v0, v1, v1, v1}, Lcom/google/android/gms/people/internal/aj;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    move-object v0, v1

    .line 791
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 765
    new-instance v1, Lcom/google/android/gms/people/internal/aj;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/aj;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 768
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/gms/common/internal/bd;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 773
    :goto_0
    return-object v0

    .line 771
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6, v6}, Lcom/google/android/gms/people/internal/aj;->a(ILandroid/os/Bundle;Landroid/os/ParcelFileDescriptor;Landroid/os/Bundle;)V

    move-object v0, v6

    .line 773
    goto :goto_0
.end method

.method protected final a(ILandroid/os/IBinder;Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 192
    if-nez p1, :cond_0

    if-eqz p3, :cond_0

    .line 193
    const-string v0, "post_init_configuration"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/internal/p;->a(Landroid/os/Bundle;)V

    .line 196
    :cond_0
    if-nez p3, :cond_1

    const/4 v0, 0x0

    :goto_0
    invoke-super {p0, p1, p2, v0}, Lcom/google/android/gms/common/internal/aj;->a(ILandroid/os/IBinder;Landroid/os/Bundle;)V

    .line 198
    return-void

    .line 196
    :cond_1
    const-string v0, "post_init_resolution"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Landroid/os/Bundle;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1651
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1653
    new-instance v1, Lcom/google/android/gms/people/internal/ae;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ae;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1655
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Landroid/os/Bundle;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1659
    :goto_0
    return-void

    .line 1657
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/people/internal/ae;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1337
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1338
    new-instance v1, Lcom/google/android/gms/people/internal/x;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/x;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1341
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1345
    :goto_0
    return-void

    .line 1343
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/people/internal/x;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1366
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1368
    new-instance v1, Lcom/google/android/gms/people/internal/z;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/z;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1370
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Landroid/net/Uri;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1374
    :goto_0
    return-void

    .line 1372
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lcom/google/android/gms/people/internal/z;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/h;)V
    .locals 14

    .prologue
    .line 514
    if-nez p4, :cond_0

    .line 515
    sget-object p4, Lcom/google/android/gms/people/h;->a:Lcom/google/android/gms/people/h;

    .line 517
    :cond_0
    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->b()Ljava/util/Collection;

    move-result-object v2

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->c()I

    move-result v6

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->d()Z

    move-result v7

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->e()J

    move-result-wide v8

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->f()Ljava/lang/String;

    move-result-object v10

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->g()I

    move-result v11

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->h()I

    move-result v12

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/gms/people/h;->i()I

    move-result v13

    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    new-instance v1, Lcom/google/android/gms/people/internal/ak;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ak;-><init>(Lcom/google/android/gms/common/api/m;)V

    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    if-nez v2, :cond_1

    const/4 v5, 0x0

    :goto_0
    move-object/from16 v2, p2

    move-object/from16 v3, p3

    invoke-interface/range {v0 .. v13}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;IZJLjava/lang/String;III)V

    .line 521
    :goto_1
    return-void

    .line 517
    :cond_1
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5, v2}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/ak;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/people/t;)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 680
    if-nez p4, :cond_0

    .line 681
    sget-object p4, Lcom/google/android/gms/people/t;->a:Lcom/google/android/gms/people/t;

    .line 683
    :cond_0
    invoke-virtual {p4}, Lcom/google/android/gms/people/t;->a()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p4}, Lcom/google/android/gms/people/t;->b()I

    move-result v5

    invoke-virtual {p4}, Lcom/google/android/gms/people/t;->c()Ljava/lang/String;

    move-result-object v6

    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    new-instance v1, Lcom/google/android/gms/people/internal/al;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/al;-><init>(Lcom/google/android/gms/common/api/m;)V

    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 685
    :goto_0
    return-void

    .line 683
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lcom/google/android/gms/people/internal/al;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    .locals 8

    .prologue
    .line 434
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 437
    new-instance v1, Lcom/google/android/gms/people/internal/ab;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ab;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 439
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    move-object v6, p6

    move v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 446
    :goto_0
    return-void

    .line 444
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/ab;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1241
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1243
    new-instance v1, Lcom/google/android/gms/people/internal/t;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/t;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1246
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1251
    :goto_0
    return-void

    .line 1249
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lcom/google/android/gms/people/internal/t;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 1149
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1151
    new-instance v1, Lcom/google/android/gms/people/internal/v;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/v;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1154
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1159
    :goto_0
    return-void

    .line 1157
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lcom/google/android/gms/people/internal/v;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    .locals 8

    .prologue
    .line 1071
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1074
    new-instance v1, Lcom/google/android/gms/people/internal/am;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/am;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1077
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    move-object v7, p7

    invoke-interface/range {v0 .. v7}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/util/List;Lcom/google/android/gms/common/server/FavaDiagnosticsEntity;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1082
    :goto_0
    return-void

    .line 1080
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/am;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 7

    .prologue
    const/4 v6, 0x0

    .line 999
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1002
    new-instance v1, Lcom/google/android/gms/people/internal/ag;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ag;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1005
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    const/4 v3, 0x0

    move-object v2, p2

    move-object v4, p3

    move v5, p4

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->b(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1010
    :goto_0
    return-void

    .line 1008
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v6, v6}, Lcom/google/android/gms/people/internal/ag;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/m;ZZLjava/lang/String;Ljava/lang/String;I)V
    .locals 8

    .prologue
    const/4 v7, 0x0

    .line 367
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 370
    new-instance v1, Lcom/google/android/gms/people/internal/ai;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ai;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 372
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    move v6, p6

    invoke-interface/range {v0 .. v6}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;ZZLjava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 379
    :goto_0
    return-void

    .line 377
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v7, v7}, Lcom/google/android/gms/people/internal/ai;->a(ILandroid/os/Bundle;Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7

    .prologue
    .line 280
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 282
    iget-object v6, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    monitor-enter v6

    .line 283
    :try_start_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/people/internal/p;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/w;)Lcom/google/android/gms/people/internal/ac;

    move-result-object v1

    .line 285
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    const/4 v2, 0x1

    move-object v3, p3

    move-object v4, p4

    move v5, p5

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;

    .line 287
    monitor-exit v6
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method protected final a(Lcom/google/android/gms/common/internal/bj;Lcom/google/android/gms/common/internal/an;)V
    .locals 3

    .prologue
    .line 180
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 181
    const-string v1, "social_client_application_id"

    iget-object v2, p0, Lcom/google/android/gms/people/internal/p;->a:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 182
    const-string v1, "real_client_package_name"

    iget-object v2, p0, Lcom/google/android/gms/people/internal/p;->g:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const-string v1, "support_new_image_callback"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 184
    const v1, 0x6768a8

    iget-object v2, p0, Lcom/google/android/gms/common/internal/aj;->b:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2, v1, v2, v0}, Lcom/google/android/gms/common/internal/bj;->f(Lcom/google/android/gms/common/internal/bg;ILjava/lang/String;Landroid/os/Bundle;)V

    .line 187
    return-void
.end method

.method public final varargs a(Lcom/google/android/gms/people/internal/s;Lcom/google/android/gms/people/identity/e;[Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 1824
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 1826
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1829
    new-instance v1, Lcom/google/android/gms/people/internal/ad;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ad;-><init>(Lcom/google/android/gms/people/internal/s;)V

    .line 1831
    new-instance v2, Lcom/google/android/gms/people/identity/internal/AccountToken;

    iget-object v0, p2, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/c;->a:Ljava/lang/String;

    iget-object v3, p2, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v3, v3, Lcom/google/android/gms/people/identity/c;->b:Ljava/lang/String;

    invoke-direct {v2, v0, v3}, Lcom/google/android/gms/people/identity/internal/AccountToken;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 1833
    invoke-static {p3}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v3

    .line 1836
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    new-instance v4, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;

    invoke-direct {v4, p2}, Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;-><init>(Lcom/google/android/gms/people/identity/e;)V

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;Lcom/google/android/gms/people/identity/internal/AccountToken;Ljava/util/List;Lcom/google/android/gms/people/identity/internal/ParcelableGetOptions;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1842
    :goto_0
    return-void

    .line 1840
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    const/4 v2, 0x0

    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/people/internal/ad;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/people/w;)V
    .locals 7

    .prologue
    .line 292
    iget-object v6, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    monitor-enter v6

    .line 294
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 296
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result v0

    if-nez v0, :cond_0

    .line 307
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v6
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 309
    :goto_0
    return-void

    .line 300
    :cond_0
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/internal/ac;

    .line 302
    invoke-virtual {v1}, Lcom/google/android/gms/people/internal/ac;->a()V

    .line 303
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 307
    :try_start_3
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 309
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 307
    :catchall_1
    move-exception v0

    :try_start_4
    iget-object v1, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    throw v0
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;JZZ)V
    .locals 9

    .prologue
    .line 1453
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1455
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/internal/i;

    move-object v2, p1

    move-object v3, p2

    move-wide v4, p3

    move v6, p5

    move v7, p6

    invoke-interface/range {v1 .. v7}, Lcom/google/android/gms/people/internal/i;->a(Ljava/lang/String;Ljava/lang/String;JZZ)Landroid/os/Bundle;

    .line 1457
    return-void
.end method

.method protected final a_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const-string v0, "com.google.android.gms.people.service.START"

    return-object v0
.end method

.method public final b()V
    .locals 8

    .prologue
    .line 202
    iget-object v6, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    monitor-enter v6

    .line 207
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/people/internal/p;->c_()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/people/internal/ac;

    .line 213
    invoke-virtual {v1}, Lcom/google/android/gms/people/internal/ac;->a()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    :try_start_1
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    const/4 v2, 0x0

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/people/internal/i;->a(Lcom/google/android/gms/people/internal/f;ZLjava/lang/String;Ljava/lang/String;I)Landroid/os/Bundle;
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 217
    :catch_0
    move-exception v0

    .line 218
    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "Failed to unregister listener"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 225
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0

    .line 219
    :catch_1
    move-exception v0

    .line 220
    :try_start_3
    const-string v1, "PeopleClient"

    const-string v2, "PeopleService is in unexpected state"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 224
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/internal/p;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 225
    monitor-exit v6
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 226
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->b()V

    .line 227
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/m;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 1352
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->j()V

    .line 1353
    new-instance v1, Lcom/google/android/gms/people/internal/ag;

    invoke-direct {v1, p1}, Lcom/google/android/gms/people/internal/ag;-><init>(Lcom/google/android/gms/common/api/m;)V

    .line 1356
    :try_start_0
    invoke-super {p0}, Lcom/google/android/gms/common/internal/aj;->k()Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/people/internal/i;

    invoke-interface {v0, v1, p2, p3}, Lcom/google/android/gms/people/internal/i;->c(Lcom/google/android/gms/people/internal/f;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 1360
    :goto_0
    return-void

    .line 1358
    :catch_0
    move-exception v0

    const/16 v0, 0x8

    invoke-virtual {v1, v0, v2, v2}, Lcom/google/android/gms/people/internal/ag;->a(ILandroid/os/Bundle;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method protected final b_()Ljava/lang/String;
    .locals 1

    .prologue
    .line 169
    const-string v0, "com.google.android.gms.people.internal.IPeopleService"

    return-object v0
.end method
