.class final Lcom/google/android/gms/car/by;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field final synthetic a:I

.field final synthetic b:Z

.field final synthetic c:Lcom/google/android/gms/car/br;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/br;IZ)V
    .locals 0

    .prologue
    .line 215
    iput-object p1, p0, Lcom/google/android/gms/car/by;->c:Lcom/google/android/gms/car/br;

    iput p2, p0, Lcom/google/android/gms/car/by;->a:I

    iput-boolean p3, p0, Lcom/google/android/gms/car/by;->b:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 3

    .prologue
    .line 218
    iget v0, p0, Lcom/google/android/gms/car/by;->a:I

    sparse-switch v0, :sswitch_data_0

    .line 236
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BluetoothPairingResponse with wrong status: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/by;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 239
    :goto_0
    return-void

    .line 220
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/car/by;->c:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/by;->b:Z

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(Z)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/car/by;->c:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0xca

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    goto :goto_0

    .line 224
    :sswitch_1
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 225
    const-string v0, "CAR.BT"

    const-string v1, "Bluetooth pairing delayed by the car"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 227
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/by;->c:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0xc9

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 228
    iget-object v0, p0, Lcom/google/android/gms/car/by;->c:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/bz;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/bz;-><init>(Lcom/google/android/gms/car/by;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    goto :goto_0

    .line 218
    nop

    :sswitch_data_0
    .sparse-switch
        -0xa -> :sswitch_1
        0x0 -> :sswitch_0
    .end sparse-switch
.end method
