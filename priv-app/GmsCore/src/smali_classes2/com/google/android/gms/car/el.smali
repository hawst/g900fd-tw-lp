.class final Lcom/google/android/gms/car/el;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/CarHomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/CarHomeActivity;)V
    .locals 0

    .prologue
    .line 81
    iput-object p1, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final b_(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 84
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 85
    const-string v0, "CAR.HOME"

    const-string v1, "Car client connected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->c(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->e()Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarHomeActivity;->finish()V

    .line 112
    :goto_0
    return-void

    .line 104
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->c(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v0

    .line 105
    iget-object v1, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v1}, Lcom/google/android/gms/car/CarHomeActivity;->d(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/hs;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/hs;)V

    .line 107
    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->y()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->b(Lcom/google/android/gms/car/CarHomeActivity;)V

    goto :goto_0

    .line 110
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->a(Lcom/google/android/gms/car/CarHomeActivity;)V

    goto :goto_0
.end method

.method public final f_(I)V
    .locals 2

    .prologue
    .line 116
    const-string v0, "CAR.HOME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    const-string v0, "CAR.HOME"

    const-string v1, "Car client suspended"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-static {v0}, Lcom/google/android/gms/car/CarHomeActivity;->c(Lcom/google/android/gms/car/CarHomeActivity;)Lcom/google/android/gms/car/ea;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/ea;->g()Lcom/google/android/gms/car/gx;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/gx;->a(Lcom/google/android/gms/car/hs;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/gms/car/el;->a:Lcom/google/android/gms/car/CarHomeActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/car/CarHomeActivity;->finish()V

    .line 121
    return-void
.end method
