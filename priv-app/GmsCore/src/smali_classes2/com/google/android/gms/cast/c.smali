.class public final Lcom/google/android/gms/cast/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/cast/CastDevice;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/net/Inet4Address;)V
    .locals 1

    .prologue
    .line 425
    iput-object p1, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 426
    invoke-static {p1, p3}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/net/Inet4Address;)Ljava/net/Inet4Address;

    .line 427
    invoke-static {p1}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/net/Inet4Address;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-static {p1}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;)Ljava/net/Inet4Address;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/Inet4Address;->getHostAddress()Ljava/lang/String;

    move-result-object v0

    :goto_0
    iput-object v0, p1, Lcom/google/android/gms/cast/CastDevice;->a:Ljava/lang/String;

    .line 429
    invoke-static {p1, p2}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    .line 430
    return-void

    .line 427
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 460
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;I)I

    .line 461
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->b(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    .line 437
    return-object p0
.end method

.method public final a(Ljava/util/List;)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 468
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->a(Lcom/google/android/gms/cast/CastDevice;Ljava/util/List;)Ljava/util/List;

    .line 469
    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 479
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->b(Lcom/google/android/gms/cast/CastDevice;I)I

    .line 480
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->c(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    .line 445
    return-object p0
.end method

.method public final c(I)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 492
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->c(Lcom/google/android/gms/cast/CastDevice;I)I

    .line 493
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/cast/c;
    .locals 1

    .prologue
    .line 452
    iget-object v0, p0, Lcom/google/android/gms/cast/c;->a:Lcom/google/android/gms/cast/CastDevice;

    invoke-static {v0, p1}, Lcom/google/android/gms/cast/CastDevice;->d(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;)Ljava/lang/String;

    .line 453
    return-object p0
.end method
