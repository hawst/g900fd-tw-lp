.class public final Lcom/google/android/gms/games/ui/c/j;
.super Lcom/google/android/gms/games/ui/c/b;
.source "SourceFile"


# instance fields
.field private final p:Landroid/os/Bundle;

.field private q:I

.field private r:Landroid/widget/TextView;

.field private s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V
    .locals 7

    .prologue
    .line 63
    const-wide/16 v4, 0xbb8

    const-string v0, "com.google.android.gms.games.extra.state"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const/4 v6, 0x1

    :goto_0
    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    invoke-direct/range {v1 .. v6}, Lcom/google/android/gms/games/ui/c/b;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;JZ)V

    .line 65
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/Bundle;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->p:Landroid/os/Bundle;

    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->p:Landroid/os/Bundle;

    const-string v1, "com.google.android.gms.games.extra.state"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/ui/c/j;->q:I

    .line 67
    return-void

    .line 63
    :cond_0
    const/4 v6, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 56
    new-instance v0, Lcom/google/android/gms/games/ui/c/j;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/ui/c/j;-><init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/el;Landroid/os/Bundle;)V

    .line 58
    sget-object v1, Lcom/google/android/gms/games/ui/c/j;->b:Lcom/google/android/gms/games/ui/c/e;

    sget-object v2, Lcom/google/android/gms/games/ui/c/j;->b:Lcom/google/android/gms/games/ui/c/e;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v0}, Lcom/google/android/gms/games/ui/c/e;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/games/ui/c/e;->sendMessage(Landroid/os/Message;)Z

    .line 59
    return-void
.end method


# virtual methods
.method protected final a()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 71
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/j;->j()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 74
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->l:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->py:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 75
    const/16 v2, 0x8

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->l:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pz:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/f;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/g;->aA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    int-to-float v2, v2

    invoke-virtual {v0, v3, v2}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 82
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    sget-object v2, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    invoke-virtual {v0, v3, v3, v3, v3}, Landroid/widget/TextView;->setPadding(IIII)V

    .line 87
    iget v0, p0, Lcom/google/android/gms/games/ui/c/j;->q:I

    packed-switch v0, :pswitch_data_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/c/j;->j()Landroid/content/Context;

    move-result-object v0

    const-string v1, "QuestPopup"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Unrecognized state provided: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/games/ui/c/j;->q:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/internal/dq;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 119
    :goto_0
    return-void

    .line 89
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->iz:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->l:Landroid/view/View;

    sget v1, Lcom/google/android/gms/j;->bD:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    .line 117
    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/j;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->p:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.games.extra.imageUri"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/net/Uri;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/c/j;->a(Landroid/net/Uri;)Landroid/graphics/Bitmap;

    move-result-object v0

    sget v2, Lcom/google/android/gms/h;->aA:I

    invoke-virtual {p0, v2}, Lcom/google/android/gms/games/ui/c/j;->a(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->a(Landroid/graphics/Bitmap;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 95
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->m:Landroid/view/View;

    invoke-virtual {v0, v3}, Landroid/view/View;->setVisibility(I)V

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->m:Landroid/view/View;

    sget v2, Lcom/google/android/gms/j;->pw:I

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 99
    sget v2, Lcom/google/android/gms/p;->iA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 101
    sget v2, Lcom/google/android/gms/f;->u:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 103
    sget v2, Lcom/google/android/gms/g;->aA:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    int-to-float v1, v1

    invoke-virtual {v0, v3, v1}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 105
    sget-object v1, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setEllipsize(Landroid/text/TextUtils$TruncateAt;)V

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/j;->p:Landroid/os/Bundle;

    const-string v2, "com.google.android.gms.games.extra.name"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_1

    .line 87
    :pswitch_data_0
    .packed-switch 0x3
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected final b()V
    .locals 2

    .prologue
    .line 133
    invoke-super {p0}, Lcom/google/android/gms/games/ui/c/b;->b()V

    .line 136
    iget v0, p0, Lcom/google/android/gms/games/ui/c/j;->q:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->r:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/j;->i:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/games/ui/c/j;->s:Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;

    iget-object v1, p0, Lcom/google/android/gms/games/ui/c/j;->j:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/widget/MetagameAvatarView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 140
    :cond_0
    return-void
.end method

.method protected final e()I
    .locals 1

    .prologue
    .line 123
    const/16 v0, 0x1e

    return v0
.end method

.method protected final f()I
    .locals 1

    .prologue
    .line 128
    const/16 v0, 0x1f

    return v0
.end method
