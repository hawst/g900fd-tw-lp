.class public Lcom/google/android/gms/drive/internal/bg;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/drive/y;


# instance fields
.field protected final a:Lcom/google/android/gms/drive/DriveId;


# direct methods
.method protected constructor <init>(Lcom/google/android/gms/drive/DriveId;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/bg;->a:Lcom/google/android/gms/drive/DriveId;

    .line 39
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/gms/drive/internal/bh;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/bh;-><init>(Lcom/google/android/gms/drive/internal/bg;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a()Lcom/google/android/gms/drive/DriveId;
    .locals 1

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/gms/drive/internal/bg;->a:Lcom/google/android/gms/drive/DriveId;

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Lcom/google/android/gms/drive/internal/bi;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/bi;-><init>(Lcom/google/android/gms/drive/internal/bg;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/gms/drive/internal/bj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/drive/internal/bj;-><init>(Lcom/google/android/gms/drive/internal/bg;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
