.class public final Lcom/google/android/gms/games/h/a/ci;
.super Lcom/google/android/gms/common/server/j;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/server/n;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/server/n;)V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/google/android/gms/common/server/j;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/games/h/a/ci;->a:Lcom/google/android/gms/common/server/n;

    .line 34
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/String;)Lcom/google/android/gms/games/h/a/bd;
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 223
    const-string v0, "players/%1$s/experiences/%2$s"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    invoke-static {p2}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v3, 0x1

    invoke-static {p3}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    if-eqz p4, :cond_0

    const-string v0, "language"

    invoke-static {p4}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_0
    if-eqz p5, :cond_1

    const-string v0, "maxResults"

    invoke-static {p5}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    :cond_1
    if-eqz p6, :cond_2

    const-string v0, "pageToken"

    invoke-static {p6}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v0, v1}, Lcom/google/android/gms/games/h/a/ci;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 224
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/games/h/a/ci;->a:Lcom/google/android/gms/common/server/n;

    const/4 v4, 0x0

    const-class v5, Lcom/google/android/gms/games/h/a/bd;

    move-object v1, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/common/server/n;->a(Lcom/google/android/gms/common/server/ClientContext;ILjava/lang/String;Ljava/lang/Object;Ljava/lang/Class;)Lcom/google/android/gms/common/server/response/FastJsonResponse;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/h/a/bd;

    return-object v0
.end method
