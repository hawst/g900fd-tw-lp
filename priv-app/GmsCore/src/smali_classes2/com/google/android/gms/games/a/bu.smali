.class final Lcom/google/android/gms/games/a/bu;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Ljava/lang/String;

.field b:Ljava/util/HashMap;

.field private c:Lcom/google/android/gms/common/data/ai;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/util/HashMap;)V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 108
    iput-object p1, p0, Lcom/google/android/gms/games/a/bu;->a:Ljava/lang/String;

    .line 109
    invoke-virtual {p0, p2}, Lcom/google/android/gms/games/a/bu;->a(Ljava/util/HashMap;)V

    .line 110
    return-void
.end method


# virtual methods
.method final a()Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/a/bu;->c:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v0}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v0

    if-nez v0, :cond_0

    .line 125
    const/4 v0, 0x1

    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 127
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/bu;->c:Lcom/google/android/gms/common/data/ai;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0
.end method

.method final a(Ljava/util/HashMap;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 113
    iput-object p1, p0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    .line 114
    new-instance v0, Lcom/google/android/gms/common/data/ai;

    invoke-static {}, Lcom/google/android/gms/games/a/bt;->a()[Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/bu;->c:Lcom/google/android/gms/common/data/ai;

    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/a/bu;->b:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/ContentValues;

    .line 118
    iget-object v2, p0, Lcom/google/android/gms/games/a/bu;->c:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    goto :goto_0

    .line 121
    :cond_0
    return-void
.end method

.method final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/gms/games/a/bu;->a:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
