.class final Lcom/google/android/gms/common/widget/a/c;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/widget/a/h;


# static fields
.field private static a:Lcom/google/android/gms/common/widget/a/c;


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    return-void
.end method

.method public static a()Lcom/google/android/gms/common/widget/a/c;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/google/android/gms/common/widget/a/c;->a:Lcom/google/android/gms/common/widget/a/c;

    if-nez v0, :cond_0

    .line 25
    new-instance v0, Lcom/google/android/gms/common/widget/a/c;

    invoke-direct {v0}, Lcom/google/android/gms/common/widget/a/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/widget/a/c;->a:Lcom/google/android/gms/common/widget/a/c;

    .line 27
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/widget/a/c;->a:Lcom/google/android/gms/common/widget/a/c;

    return-object v0
.end method


# virtual methods
.method public final a(Landroid/view/ViewGroup;I)Lcom/google/android/gms/common/widget/a/g;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 42
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    .line 45
    sget v0, Lcom/google/android/gms/common/widget/e;->a:I

    if-ne v0, p2, :cond_1

    .line 46
    invoke-virtual {v2, p2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    .line 51
    :goto_0
    sget v0, Lcom/google/android/gms/common/widget/e;->d:I

    if-ne v0, p2, :cond_0

    .line 52
    const v0, 0x1020018

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 54
    invoke-virtual {v2, p2, v0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 57
    :cond_0
    sget v0, Lcom/google/android/gms/common/widget/e;->b:I

    if-ne v0, p2, :cond_2

    .line 58
    new-instance v0, Lcom/google/android/gms/common/widget/a/q;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/widget/a/q;-><init>(Landroid/view/View;)V

    .line 62
    :goto_1
    return-object v0

    .line 48
    :cond_1
    sget v0, Lcom/google/android/gms/common/widget/e;->b:I

    invoke-virtual {v2, v0, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    move-object v1, v0

    goto :goto_0

    .line 59
    :cond_2
    sget v0, Lcom/google/android/gms/common/widget/e;->a:I

    if-ne v0, p2, :cond_3

    .line 60
    new-instance v0, Lcom/google/android/gms/common/widget/a/k;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/widget/a/k;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 61
    :cond_3
    sget v0, Lcom/google/android/gms/common/widget/e;->d:I

    if-ne v0, p2, :cond_4

    .line 62
    new-instance v0, Lcom/google/android/gms/common/widget/a/s;

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/widget/a/s;-><init>(Landroid/view/View;)V

    goto :goto_1

    .line 65
    :cond_4
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown view type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method
