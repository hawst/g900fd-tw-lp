.class final Lcom/google/android/gms/car/rb;
.super Lcom/google/android/gms/car/qp;
.source "SourceFile"


# instance fields
.field private final l:I

.field private final m:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ok;)V
    .locals 4

    .prologue
    .line 746
    const-wide/16 v0, 0x96

    const-wide/16 v2, 0x4b

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    const/4 v2, 0x0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/car/qp;-><init>(JZ)V

    .line 747
    iget v0, p1, Lcom/google/android/gms/car/ok;->d:I

    iput v0, p0, Lcom/google/android/gms/car/rb;->l:I

    .line 748
    iget v0, p1, Lcom/google/android/gms/car/ok;->a:I

    iget v1, p1, Lcom/google/android/gms/car/ok;->e:I

    add-int/2addr v0, v1

    div-int/lit8 v0, v0, 0x2

    iput v0, p0, Lcom/google/android/gms/car/rb;->m:I

    .line 749
    return-void
.end method


# virtual methods
.method protected final a(ZJ)Lcom/google/android/gms/car/qq;
    .locals 10

    .prologue
    const/4 v1, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    const/4 v8, 0x0

    .line 754
    sget-object v0, Lcom/google/android/gms/car/rb;->a:Lcom/google/android/gms/car/pd;

    long-to-float v2, p2

    const/high16 v3, 0x42960000    # 75.0f

    invoke-virtual {v0, v2, v8, v3}, Lcom/google/android/gms/car/pd;->a(FFF)F

    move-result v0

    .line 756
    sget-object v2, Lcom/google/android/gms/car/rb;->a:Lcom/google/android/gms/car/pd;

    long-to-float v3, p2

    const/high16 v4, 0x43160000    # 150.0f

    invoke-virtual {v2, v3, v8, v4}, Lcom/google/android/gms/car/pd;->a(FFF)F

    move-result v7

    .line 758
    const v2, 0x3f666666    # 0.9f

    const v3, 0x3dccccd0    # 0.100000024f

    sub-float v0, v5, v0

    mul-float/2addr v0, v3

    add-float v4, v2, v0

    .line 760
    iget-object v0, p0, Lcom/google/android/gms/car/rb;->k:Lcom/google/android/gms/car/qq;

    iget-object v0, v0, Lcom/google/android/gms/car/qq;->a:[F

    sget-object v2, Lcom/google/android/gms/car/rb;->f:[F

    move v3, v1

    move v6, v5

    invoke-static/range {v0 .. v6}, Landroid/opengl/Matrix;->scaleM([FI[FIFFF)V

    .line 761
    sub-float v0, v5, v4

    iget v2, p0, Lcom/google/android/gms/car/rb;->m:I

    int-to-float v2, v2

    mul-float/2addr v0, v2

    .line 762
    iget-object v2, p0, Lcom/google/android/gms/car/rb;->k:Lcom/google/android/gms/car/qq;

    iget-object v2, v2, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v2, v1, v0, v8, v8}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 764
    iget v0, p0, Lcom/google/android/gms/car/rb;->l:I

    int-to-float v0, v0

    mul-float/2addr v0, v7

    .line 765
    iget-object v2, p0, Lcom/google/android/gms/car/rb;->k:Lcom/google/android/gms/car/qq;

    iget-object v2, v2, Lcom/google/android/gms/car/qq;->a:[F

    invoke-static {v2, v1, v8, v0, v8}, Landroid/opengl/Matrix;->translateM([FIFFF)V

    .line 770
    iget-object v0, p0, Lcom/google/android/gms/car/rb;->k:Lcom/google/android/gms/car/qq;

    return-object v0
.end method
