.class final Lcom/google/android/gms/common/download/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/service/b;


# instance fields
.field private final a:Lcom/google/android/gms/common/download/b/j;

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/download/b/j;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 83
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 84
    iput-object p1, p0, Lcom/google/android/gms/common/download/i;->a:Lcom/google/android/gms/common/download/b/j;

    .line 85
    iput-object p2, p0, Lcom/google/android/gms/common/download/i;->b:Ljava/lang/String;

    .line 86
    iput-object p3, p0, Lcom/google/android/gms/common/download/i;->c:Ljava/lang/String;

    .line 87
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/Status;)V
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/common/download/i;->a:Lcom/google/android/gms/common/download/b/j;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/download/b/j;->a(Lcom/google/android/gms/common/api/Status;)V

    .line 121
    return-void
.end method

.method public final synthetic a(Lcom/google/android/gms/common/service/c;)V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 75
    check-cast p1, Lcom/google/android/gms/common/download/DownloadOperationService;

    iget-object v0, p0, Lcom/google/android/gms/common/download/i;->c:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/google/android/gms/common/download/DownloadService;->e(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v1

    const/16 v2, 0x1b58

    if-ne v1, v2, :cond_0

    invoke-virtual {p1}, Lcom/google/android/gms/common/download/DownloadOperationService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/common/download/i;->b:Ljava/lang/String;

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getApplicationInfo(Ljava/lang/String;I)Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->getApplicationLabel(Landroid/content/pm/ApplicationInfo;)Ljava/lang/CharSequence;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/common/download/i;->c:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/google/android/gms/common/download/r;->g(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/download/f;

    move-result-object v2

    if-eqz v2, :cond_0

    sget v0, Lcom/google/android/gms/p;->fJ:I

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/download/DownloadOperationService;->getString(I)Ljava/lang/String;

    move-result-object v0

    sget v3, Lcom/google/android/gms/p;->fI:I

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    aput-object v1, v4, v5

    invoke-virtual {p1, v3, v4}, Lcom/google/android/gms/common/download/DownloadOperationService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v2, v0, v1}, Lcom/google/android/gms/common/download/DownloadService;->a(Landroid/content/Context;Lcom/google/android/gms/common/download/f;Ljava/lang/String;Ljava/lang/String;)Z

    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/common/download/DownloadService;

    invoke-direct {v0, p1, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/download/DownloadOperationService;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    iget-object v0, p0, Lcom/google/android/gms/common/download/i;->a:Lcom/google/android/gms/common/download/b/j;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x1b59

    sget v3, Lcom/google/android/gms/p;->fF:I

    invoke-virtual {p1, v3}, Lcom/google/android/gms/common/download/DownloadOperationService;->getString(I)Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/download/b/j;->a(Lcom/google/android/gms/common/api/Status;)V

    :goto_0
    return-void

    :catch_0
    move-exception v0

    iget-object v0, p0, Lcom/google/android/gms/common/download/i;->a:Lcom/google/android/gms/common/download/b/j;

    new-instance v1, Lcom/google/android/gms/common/api/Status;

    const/16 v2, 0x8

    invoke-direct {v1, v2}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/download/b/j;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0

    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/common/download/i;->a:Lcom/google/android/gms/common/download/b/j;

    invoke-interface {v1, v0}, Lcom/google/android/gms/common/download/b/j;->a(Lcom/google/android/gms/common/api/Status;)V

    goto :goto_0
.end method
