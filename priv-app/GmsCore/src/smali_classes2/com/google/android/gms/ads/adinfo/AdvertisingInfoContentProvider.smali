.class public Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;
.super Landroid/content/ContentProvider;
.source "SourceFile"


# static fields
.field private static final b:Ljava/nio/charset/Charset;


# instance fields
.field a:Lcom/google/android/gms/ads/adinfo/a;

.field private c:Z

.field private final d:Ljava/lang/Object;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const-string v0, "UTF-8"

    invoke-static {v0}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->b:Ljava/nio/charset/Charset;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 29
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->d:Ljava/lang/Object;

    return-void
.end method

.method private static a([B)Landroid/os/ParcelFileDescriptor;
    .locals 3

    .prologue
    .line 54
    invoke-static {}, Landroid/os/ParcelFileDescriptor;->createPipe()[Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    .line 55
    new-instance v1, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;

    const/4 v2, 0x1

    aget-object v2, v0, v2

    invoke-direct {v1, v2}, Landroid/os/ParcelFileDescriptor$AutoCloseOutputStream;-><init>(Landroid/os/ParcelFileDescriptor;)V

    .line 56
    invoke-virtual {v1, p0}, Ljava/io/OutputStream;->write([B)V

    .line 57
    invoke-virtual {v1}, Ljava/io/OutputStream;->close()V

    .line 58
    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method private a()Lcom/google/android/gms/ads/identifier/c;
    .locals 3

    .prologue
    .line 125
    const/4 v0, 0x0

    .line 128
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/ads/identifier/a;->a(Landroid/content/Context;)Lcom/google/android/gms/ads/identifier/c;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 133
    :goto_0
    return-object v0

    .line 130
    :catch_0
    move-exception v1

    .line 131
    const-string v2, "Cannot get advertising info."

    invoke-static {v2, v1}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 111
    if-nez p0, :cond_0

    .line 112
    const-string p0, "na"

    .line 114
    :cond_0
    const-string v0, "<html>\n    <head>\n        <title>Ad Info Fetcher</title>\n        <script src=\"%s\"></script>\n        <script type=\"text/javascript\">setAdInfo(\"%s\");</script>\n    </head>\n</html>\n"

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    sget-object v3, Lcom/google/android/gms/ads/a/b;->c:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v3}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    aput-object p0, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 159
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 146
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v1, "gif"

    invoke-virtual {v0, v1}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 147
    const-string v0, "image/gif"

    .line 149
    :goto_0
    return-object v0

    :cond_0
    const-string v0, "text/html"

    goto :goto_0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 154
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x1

    return v0
.end method

.method public openFile(Landroid/net/Uri;Ljava/lang/String;)Landroid/os/ParcelFileDescriptor;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 63
    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->d:Ljava/lang/Object;

    monitor-enter v2

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->c:Z

    if-eqz v0, :cond_0

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 64
    :goto_0
    sget-object v0, Lcom/google/android/gms/ads/a/b;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-nez v0, :cond_1

    move-object v0, v1

    .line 88
    :goto_1
    return-object v0

    .line 63
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->c:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    :try_start_2
    new-instance v0, Lcom/google/android/gms/ads/adinfo/a;

    invoke-direct {v0}, Lcom/google/android/gms/ads/adinfo/a;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a:Lcom/google/android/gms/ads/adinfo/a;
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    :goto_2
    :try_start_3
    monitor-exit v2
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    :catch_0
    move-exception v0

    :try_start_4
    const-string v3, "Could not create encrypter."

    invoke-static {v3, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    goto :goto_2

    .line 72
    :cond_1
    if-eqz p1, :cond_2

    :try_start_5
    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v0

    const-string v2, "gif"

    invoke-virtual {v0, v2}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 73
    const-string v0, "R0lGODlhAQABAIAAAAUEBAAAACwAAAAAAQABAAACAkQBADs="

    const/4 v2, 0x0

    invoke-static {v0, v2}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a([B)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 78
    :cond_2
    sget-object v0, Lcom/google/android/gms/ads/a/b;->b:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_6

    .line 79
    invoke-direct {p0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a()Lcom/google/android/gms/ads/identifier/c;

    move-result-object v0

    .line 82
    :goto_3
    if-eqz v0, :cond_5

    if-eqz v0, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a:Lcom/google/android/gms/ads/adinfo/a;

    if-nez v2, :cond_4

    :cond_3
    move-object v0, v1

    :goto_4
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_5

    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 83
    :goto_5
    sget-object v2, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->b:Ljava/nio/charset/Charset;

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 84
    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a([B)Landroid/os/ParcelFileDescriptor;

    move-result-object v0

    goto :goto_1

    .line 82
    :cond_4
    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a:Lcom/google/android/gms/ads/adinfo/a;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/ads/adinfo/a;->a(Lcom/google/android/gms/ads/identifier/c;)Ljava/lang/String;

    move-result-object v0

    goto :goto_4

    :cond_5
    const-string v0, "na"

    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/AdvertisingInfoContentProvider;->a(Ljava/lang/String;)Ljava/lang/String;
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_1

    move-result-object v0

    goto :goto_5

    .line 88
    :catch_1
    move-exception v0

    move-object v0, v1

    goto :goto_1

    :cond_6
    move-object v0, v1

    goto :goto_3
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 141
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 164
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
