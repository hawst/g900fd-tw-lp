.class public final Lcom/google/android/gms/car/bg;
.super Lcom/google/android/gms/car/ke;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;


# instance fields
.field final a:Ljava/util/List;

.field final b:Landroid/util/SparseArray;

.field final c:[Lcom/google/android/gms/car/g;

.field final d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

.field e:Lcom/google/android/gms/car/nt;

.field volatile f:Z

.field private volatile g:Lcom/google/android/gms/car/bl;

.field private final h:[Lcom/google/android/gms/car/bh;

.field private final i:Lcom/google/android/gms/car/aw;

.field private final j:Lcom/google/android/gms/car/gx;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;)V
    .locals 2

    .prologue
    const/4 v1, 0x3

    .line 60
    invoke-direct {p0}, Lcom/google/android/gms/car/ke;-><init>()V

    .line 34
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    .line 38
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    .line 46
    new-array v0, v1, [Lcom/google/android/gms/car/g;

    iput-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    .line 48
    new-array v0, v1, [Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iput-object v0, p0, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    .line 50
    new-array v0, v1, [Lcom/google/android/gms/car/bh;

    iput-object v0, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    .line 55
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/bg;->f:Z

    .line 61
    iput-object p1, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    .line 62
    iput-object p2, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    .line 63
    return-void
.end method

.method public static a(I)I
    .locals 3

    .prologue
    .line 288
    packed-switch p0, :pswitch_data_0

    .line 296
    :pswitch_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 290
    :pswitch_1
    const/4 v0, 0x0

    .line 294
    :goto_0
    return v0

    .line 292
    :pswitch_2
    const/4 v0, 0x1

    goto :goto_0

    .line 294
    :pswitch_3
    const/4 v0, 0x2

    goto :goto_0

    .line 288
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_3
        :pswitch_0
        :pswitch_1
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public static a(I[Lcom/google/android/gms/car/CarAudioConfiguration;)I
    .locals 5

    .prologue
    .line 208
    const/16 v1, 0x3e80

    .line 209
    const/4 v0, 0x4

    .line 210
    const/4 v2, 0x3

    if-ne p0, v2, :cond_0

    .line 212
    const v1, 0xbb80

    .line 213
    const/16 v0, 0xc

    .line 215
    :cond_0
    const/4 v2, 0x0

    :goto_0
    array-length v3, p1

    if-ge v2, v3, :cond_2

    .line 216
    aget-object v3, p1, v2

    iget v3, v3, Lcom/google/android/gms/car/CarAudioConfiguration;->b:I

    if-ne v3, v1, :cond_1

    aget-object v3, p1, v2

    iget v3, v3, Lcom/google/android/gms/car/CarAudioConfiguration;->c:I

    if-ne v3, v0, :cond_1

    aget-object v3, p1, v2

    iget v3, v3, Lcom/google/android/gms/car/CarAudioConfiguration;->d:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_1

    .line 221
    :goto_1
    return v2

    .line 215
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 221
    :cond_2
    const/4 v2, -0x1

    goto :goto_1
.end method

.method static a(Lcom/google/android/gms/car/CarAudioConfiguration;)I
    .locals 4

    .prologue
    const/16 v3, 0xc

    .line 440
    const/16 v0, 0x400

    .line 441
    iget v1, p0, Lcom/google/android/gms/car/CarAudioConfiguration;->b:I

    const v2, 0xac44

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/gms/car/CarAudioConfiguration;->b:I

    const v2, 0xbb80

    if-ne v1, v2, :cond_1

    .line 442
    :cond_0
    const/16 v0, 0x800

    .line 444
    :cond_1
    mul-int/lit8 v0, v0, 0x2

    .line 445
    iget v1, p0, Lcom/google/android/gms/car/CarAudioConfiguration;->c:I

    if-eq v1, v3, :cond_2

    iget v1, p0, Lcom/google/android/gms/car/CarAudioConfiguration;->c:I

    if-ne v1, v3, :cond_3

    .line 447
    :cond_2
    mul-int/lit8 v0, v0, 0x2

    .line 449
    :cond_3
    return v0
.end method

.method private a(Lcom/google/android/gms/car/kg;)Lcom/google/android/gms/car/bj;
    .locals 3

    .prologue
    .line 669
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    monitor-enter v1

    .line 670
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/kg;)Lcom/google/android/gms/car/bj;

    move-result-object v0

    .line 671
    if-eqz v0, :cond_0

    .line 672
    monitor-exit v1

    .line 680
    :goto_0
    return-object v0

    .line 674
    :cond_0
    new-instance v0, Lcom/google/android/gms/car/bj;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/bj;-><init>(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/kg;)V

    .line 675
    invoke-virtual {v0}, Lcom/google/android/gms/car/bj;->a()Z

    move-result v2

    if-nez v2, :cond_1

    .line 677
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v2, "invalid callback"

    invoke-direct {v0, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 681
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 679
    :cond_1
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 680
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/car/bg;)Lcom/google/android/gms/car/gx;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/car/bf;)V
    .locals 1

    .prologue
    .line 533
    invoke-virtual {p0}, Lcom/google/android/gms/car/bf;->b()Lcom/google/android/gms/car/bj;

    move-result-object v0

    .line 534
    if-eqz v0, :cond_0

    .line 535
    invoke-virtual {v0, p0}, Lcom/google/android/gms/car/bj;->a(Lcom/google/android/gms/car/bf;)V

    .line 537
    :cond_0
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/bj;)V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bj;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/bg;Lcom/google/android/gms/car/bo;)V
    .locals 3

    .prologue
    .line 30
    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/bg;->c(Lcom/google/android/gms/car/bo;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0, p1}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bo;)V

    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/car/bo;->a()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    iget-object v1, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    aget-object v0, v1, v0

    iget-object v1, v0, Lcom/google/android/gms/car/bh;->b:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, v0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    if-ne v2, p1, :cond_1

    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public static a(Lcom/google/android/gms/car/bo;)V
    .locals 2

    .prologue
    .line 540
    invoke-virtual {p0}, Lcom/google/android/gms/car/bo;->c()Lcom/google/android/gms/car/bj;

    move-result-object v0

    .line 541
    if-eqz v0, :cond_0

    .line 542
    invoke-virtual {p0}, Lcom/google/android/gms/car/bo;->a()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/bj;->a(I)V

    .line 544
    :cond_0
    return-void
.end method

.method private static a([Lcom/google/android/c/b/f;Z)[Lcom/google/android/gms/car/CarAudioConfiguration;
    .locals 10

    .prologue
    const/16 v3, 0x10

    const/16 v2, 0xc

    const/4 v9, 0x2

    .line 242
    array-length v0, p0

    new-array v4, v0, [Lcom/google/android/gms/car/CarAudioConfiguration;

    .line 243
    const/4 v0, 0x0

    :goto_0
    array-length v1, v4

    if-ge v0, v1, :cond_4

    .line 244
    aget-object v5, p0, v0

    if-eqz p1, :cond_2

    iget v1, v5, Lcom/google/android/c/b/f;->c:I

    if-ne v1, v9, :cond_1

    move v1, v2

    :goto_1
    iget v6, v5, Lcom/google/android/c/b/f;->b:I

    if-eq v6, v3, :cond_0

    const-string v6, "CAR.AUDIO"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Audio config received has wrong number of bits "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v8, v5, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v6, Lcom/google/android/gms/car/CarAudioConfiguration;

    iget v5, v5, Lcom/google/android/c/b/f;->a:I

    invoke-direct {v6, v5, v1}, Lcom/google/android/gms/car/CarAudioConfiguration;-><init>(II)V

    aput-object v6, v4, v0

    .line 243
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 244
    :cond_1
    const/4 v1, 0x4

    goto :goto_1

    :cond_2
    iget v1, v5, Lcom/google/android/c/b/f;->c:I

    if-ne v1, v9, :cond_3

    move v1, v2

    goto :goto_1

    :cond_3
    move v1, v3

    goto :goto_1

    .line 246
    :cond_4
    return-object v4
.end method

.method private b(Lcom/google/android/gms/car/kg;)Lcom/google/android/gms/car/bj;
    .locals 5

    .prologue
    .line 685
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    monitor-enter v1

    .line 686
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bj;

    .line 687
    invoke-static {v0}, Lcom/google/android/gms/car/bj;->a(Lcom/google/android/gms/car/bj;)Lcom/google/android/gms/car/kg;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/gms/car/kg;->asBinder()Landroid/os/IBinder;

    move-result-object v3

    invoke-interface {p1}, Lcom/google/android/gms/car/kg;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v3, v4, :cond_0

    .line 688
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 691
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    monitor-exit v1

    goto :goto_0

    .line 692
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private static b(Lcom/google/android/c/b/f;)V
    .locals 3

    .prologue
    .line 192
    iget v0, p0, Lcom/google/android/c/b/f;->a:I

    const v1, 0xbb80

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/c/b/f;->a:I

    const/16 v1, 0x3e80

    if-eq v0, v1, :cond_0

    .line 193
    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong sampling rate "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/c/b/f;->a:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    .line 196
    :cond_0
    iget v0, p0, Lcom/google/android/c/b/f;->b:I

    const/16 v1, 0x10

    if-eq v0, v1, :cond_1

    .line 197
    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong number of bits "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/c/b/f;->b:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    .line 200
    :cond_1
    iget v0, p0, Lcom/google/android/c/b/f;->c:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_2

    iget v0, p0, Lcom/google/android/c/b/f;->c:I

    const/4 v1, 0x1

    if-eq v0, v1, :cond_2

    .line 201
    new-instance v0, Lcom/google/android/gms/car/ed;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong number of channles "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/c/b/f;->c:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ed;-><init>(Ljava/lang/String;)V

    throw v0

    .line 204
    :cond_2
    return-void
.end method

.method private b(Lcom/google/android/gms/car/bj;)V
    .locals 3

    .prologue
    .line 708
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    monitor-enter v1

    .line 709
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->a:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .line 710
    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bj;

    .line 712
    if-ne v0, p1, :cond_0

    .line 713
    invoke-virtual {v0}, Lcom/google/android/gms/car/bj;->b()V

    .line 714
    invoke-interface {v2}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 717
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_1
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private static d(I)V
    .locals 2

    .prologue
    .line 513
    const/4 v0, 0x3

    if-ne p0, v0, :cond_0

    .line 514
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Unsuported stream"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 516
    :cond_0
    return-void
.end method

.method private e(I)Z
    .locals 4

    .prologue
    .line 519
    const/4 v1, 0x0

    .line 520
    iget-object v3, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v3

    .line 521
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v2, v0

    :goto_0
    if-ltz v2, :cond_1

    .line 522
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bk;

    .line 523
    iget v0, v0, Lcom/google/android/gms/car/bk;->a:I

    if-ne p1, v0, :cond_0

    .line 524
    const/4 v0, 0x1

    .line 528
    :goto_1
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    return v0

    .line 521
    :cond_0
    add-int/lit8 v0, v2, -0x1

    move v2, v0

    goto :goto_0

    .line 528
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    :cond_1
    move v0, v1

    goto :goto_1
.end method


# virtual methods
.method public final a(II)Lcom/google/android/gms/car/CarAudioConfiguration;
    .locals 3

    .prologue
    .line 376
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 377
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 378
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bk;

    .line 379
    if-nez v0, :cond_0

    .line 380
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "CarNotSupported"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 383
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 382
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/car/bk;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    aget-object v0, v0, p2

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method final a(Lcom/google/android/gms/car/bo;Z)Lcom/google/android/gms/car/g;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 625
    invoke-virtual {p1}, Lcom/google/android/gms/car/bo;->a()I

    move-result v0

    .line 626
    invoke-static {v0}, Lcom/google/android/gms/car/bg;->d(I)V

    .line 627
    invoke-static {v0}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v0

    .line 628
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    aget-object v2, v2, v0

    .line 630
    iget-object v3, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v0, v3, v0

    .line 631
    invoke-virtual {v2}, Lcom/google/android/gms/car/bh;->b()Lcom/google/android/gms/car/bo;

    move-result-object v3

    .line 632
    if-eqz p2, :cond_2

    .line 633
    if-ne v3, p1, :cond_1

    .line 634
    invoke-virtual {v2, v1}, Lcom/google/android/gms/car/bh;->a(Lcom/google/android/gms/car/bo;)V

    .line 644
    :cond_0
    :goto_0
    return-object v0

    :cond_1
    move-object v0, v1

    .line 637
    goto :goto_0

    .line 639
    :cond_2
    if-nez v3, :cond_3

    .line 640
    invoke-virtual {v2, p1}, Lcom/google/android/gms/car/bh;->a(Lcom/google/android/gms/car/bo;)V

    goto :goto_0

    .line 641
    :cond_3
    if-eq v3, p1, :cond_0

    move-object v0, v1

    .line 642
    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/kg;III)Lcom/google/android/gms/car/kp;
    .locals 7

    .prologue
    .line 455
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 456
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/bg;->e(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 457
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 459
    :cond_0
    invoke-static {p2}, Lcom/google/android/gms/car/bg;->d(I)V

    .line 460
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/kg;)Lcom/google/android/gms/car/bj;

    move-result-object v0

    .line 461
    invoke-virtual {p0, p2, p3}, Lcom/google/android/gms/car/bg;->c(II)I

    move-result v5

    .line 462
    rem-int v1, p4, v5

    if-eqz v1, :cond_1

    .line 463
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buffer size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not multiple of min buffer size "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 466
    :cond_1
    if-ge p4, v5, :cond_2

    .line 467
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "buffer size "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " smaller than required minimum "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 470
    :cond_2
    const/high16 v1, 0x80000

    if-le p4, v1, :cond_3

    .line 471
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "buffer size bigger than current limit 524288"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 474
    :cond_3
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    move-object v1, p0

    move v2, p2

    move v3, p3

    move v4, p4

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/car/bj;->a(Lcom/google/android/gms/car/bg;IIIII)Lcom/google/android/gms/car/bo;

    move-result-object v0

    return-object v0
.end method

.method public final a(II[Lcom/google/android/c/b/f;)V
    .locals 9

    .prologue
    .line 109
    :try_start_0
    array-length v1, p3

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    aget-object v2, p3, v0

    .line 110
    invoke-static {v2}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/c/b/f;)V
    :try_end_0
    .catch Lcom/google/android/gms/car/ed; {:try_start_0 .. :try_end_0} :catch_0

    .line 109
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    const/4 v2, 0x2

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 163
    :goto_1
    return-void

    .line 117
    :cond_0
    packed-switch p2, :pswitch_data_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unsupported stream type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :pswitch_0
    const/4 v3, 0x3

    .line 118
    :goto_2
    const/4 v0, 0x1

    invoke-static {p3, v0}, Lcom/google/android/gms/car/bg;->a([Lcom/google/android/c/b/f;Z)[Lcom/google/android/gms/car/CarAudioConfiguration;

    move-result-object v6

    .line 120
    invoke-static {v3, v6}, Lcom/google/android/gms/car/bg;->a(I[Lcom/google/android/gms/car/CarAudioConfiguration;)I

    move-result v4

    .line 121
    const/4 v0, -0x1

    if-ne v4, v0, :cond_2

    .line 122
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    const/4 v2, 0x2

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v0, "no default config"

    invoke-direct {v4, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x3

    if-ne v3, v0, :cond_1

    const-string v0, "48000 stereo missing"

    :goto_3
    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " for stream "

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_1

    .line 117
    :pswitch_1
    const/4 v3, 0x5

    goto :goto_2

    :pswitch_2
    const/4 v3, 0x1

    goto :goto_2

    :pswitch_3
    const/4 v3, 0x0

    goto :goto_2

    .line 122
    :cond_1
    const-string v0, "16000 mono missing"

    goto :goto_3

    .line 128
    :cond_2
    new-instance v0, Lcom/google/android/gms/car/bk;

    invoke-direct {v0, p0, v3, v6}, Lcom/google/android/gms/car/bk;-><init>(Lcom/google/android/gms/car/bg;I[Lcom/google/android/gms/car/CarAudioConfiguration;)V

    .line 129
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 130
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v3}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    if-eqz v2, :cond_3

    .line 131
    const-string v2, "CAR.AUDIO"

    const/4 v5, 0x3

    invoke-static {v2, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 132
    const-string v2, "CAR.AUDIO"

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v7, "stream type "

    invoke-direct {v5, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v7, " already discovered."

    invoke-virtual {v5, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v2, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 135
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 136
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 137
    invoke-static {v3}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v7

    .line 138
    iget-object v8, p0, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    new-instance v0, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    iget-object v1, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    iget-object v2, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    move v5, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;-><init>(Lcom/google/android/gms/car/aw;Lcom/google/android/gms/car/gx;III)V

    aput-object v0, v8, v7

    .line 142
    const/4 v0, 0x3

    if-ne v3, v0, :cond_5

    .line 143
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    const/4 v1, 0x0

    new-instance v2, Lcom/google/android/gms/car/g;

    iget-object v4, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    iget-object v5, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    const/4 v8, 0x1

    invoke-direct {v2, v4, v5, v3, v8}, Lcom/google/android/gms/car/g;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;IZ)V

    aput-object v2, v0, v1

    .line 145
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    invoke-virtual {v0, p3, v6}, Lcom/google/android/gms/car/g;->a([Lcom/google/android/c/b/f;[Lcom/google/android/gms/car/CarAudioConfiguration;)V

    .line 161
    :cond_4
    :goto_4
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    iget-object v1, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v1, v1, v7

    iget-object v2, p0, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    aget-object v2, v2, v7

    invoke-virtual {v0, v7, v1, v2}, Lcom/google/android/gms/car/aw;->a(ILcom/google/android/gms/car/g;Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;)V

    goto/16 :goto_1

    .line 136
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 147
    :cond_5
    const/4 v0, 0x5

    if-eq v3, v0, :cond_6

    const/4 v0, 0x1

    if-ne v3, v0, :cond_4

    .line 149
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    new-instance v1, Lcom/google/android/gms/car/g;

    iget-object v2, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    iget-object v4, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    const/4 v5, 0x0

    invoke-direct {v1, v2, v4, v3, v5}, Lcom/google/android/gms/car/g;-><init>(Lcom/google/android/gms/car/gx;Lcom/google/android/gms/car/aw;IZ)V

    aput-object v1, v0, v7

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v0, v0, v7

    invoke-virtual {v0, p3, v6}, Lcom/google/android/gms/car/g;->a([Lcom/google/android/c/b/f;[Lcom/google/android/gms/car/CarAudioConfiguration;)V

    .line 153
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    new-instance v1, Lcom/google/android/gms/car/bh;

    iget-object v2, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v2, v2, v7

    invoke-direct {v1, p0, v3, v2}, Lcom/google/android/gms/car/bh;-><init>(Lcom/google/android/gms/car/bg;ILcom/google/android/gms/car/g;)V

    aput-object v1, v0, v7

    .line 156
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v0, v0, v7

    iget-object v1, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    aget-object v1, v1, v7

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/g;->a(Lcom/google/android/gms/car/i;)V

    goto :goto_4

    .line 117
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/c/b/f;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    if-eqz v0, :cond_0

    .line 171
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 172
    const-string v0, "CAR.AUDIO"

    const-string v1, "car microphone already discovered."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 176
    :cond_0
    :try_start_0
    invoke-static {p1}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/c/b/f;)V
    :try_end_0
    .catch Lcom/google/android/gms/car/ed; {:try_start_0 .. :try_end_0} :catch_0

    .line 182
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/google/android/c/b/f;

    aput-object p1, v0, v2

    .line 184
    invoke-static {v0, v2}, Lcom/google/android/gms/car/bg;->a([Lcom/google/android/c/b/f;Z)[Lcom/google/android/gms/car/CarAudioConfiguration;

    move-result-object v0

    .line 186
    new-instance v1, Lcom/google/android/gms/car/bl;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/bl;-><init>(Lcom/google/android/gms/car/bg;[Lcom/google/android/gms/car/CarAudioConfiguration;)V

    iput-object v1, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    .line 187
    new-instance v1, Lcom/google/android/gms/car/nt;

    invoke-direct {v1}, Lcom/google/android/gms/car/nt;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    .line 188
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    iput v2, v1, Lcom/google/android/gms/car/nt;->b:I

    iput-object v0, v1, Lcom/google/android/gms/car/nt;->a:[Lcom/google/android/gms/car/CarAudioConfiguration;

    .line 189
    :goto_0
    return-void

    .line 177
    :catch_0
    move-exception v0

    .line 178
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->j:Lcom/google/android/gms/car/gx;

    const/4 v2, 0x2

    invoke-virtual {v0}, Lcom/google/android/gms/car/ed;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/bj;)V
    .locals 3

    .prologue
    .line 701
    const-string v0, "CAR.AUDIO"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 702
    const-string v0, "CAR.AUDIO"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "error while accessing client "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 704
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bg;->b(Lcom/google/android/gms/car/bj;)V

    .line 705
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/bg;->f:Z

    .line 73
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 931
    const-string v1, "**Audio Sources**"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 932
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "connected="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v2, p0, Lcom/google/android/gms/car/bg;->f:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 933
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    array-length v3, v2

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v4, v2, v1

    .line 934
    if-eqz v4, :cond_0

    .line 935
    invoke-virtual {v4, p1}, Lcom/google/android/gms/car/g;->a(Ljava/io/PrintWriter;)V

    .line 933
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 937
    :cond_0
    const-string v4, "null source service"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 940
    :cond_1
    const-string v1, "**Audio Source BHs**"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 941
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->d:[Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;

    array-length v3, v2

    move v1, v0

    :goto_2
    if-ge v1, v3, :cond_3

    aget-object v4, v2, v1

    .line 942
    if-eqz v4, :cond_2

    .line 943
    invoke-virtual {v4, p1}, Lcom/google/android/gms/car/AudioSourceServiceBottomHalf;->a(Ljava/io/PrintWriter;)V

    .line 941
    :goto_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 945
    :cond_2
    const-string v4, "null source service BH"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_3

    .line 948
    :cond_3
    const-string v1, "**Audio Source Clients**"

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 949
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    array-length v2, v1

    :goto_4
    if-ge v0, v2, :cond_6

    aget-object v3, v1, v0

    .line 950
    if-eqz v3, :cond_5

    .line 951
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "stream type:"

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v3, Lcom/google/android/gms/car/bh;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    iget-object v3, v3, Lcom/google/android/gms/car/bh;->c:Lcom/google/android/gms/car/bo;

    if-eqz v3, :cond_4

    const-string v4, "***Active Track***"

    invoke-virtual {p1, v4}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Lcom/google/android/gms/car/bo;->a(Ljava/io/PrintWriter;)V

    .line 949
    :cond_4
    :goto_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 953
    :cond_5
    const-string v3, "null client"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_5

    .line 956
    :cond_6
    const-string v0, "**Microphone Input**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 957
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    .line 958
    if-eqz v0, :cond_7

    .line 959
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/nt;->a(Ljava/io/PrintWriter;)V

    .line 961
    :cond_7
    const-string v0, "**Audio Focus**"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 962
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    .line 963
    if-eqz v0, :cond_8

    .line 964
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/aw;->a(Ljava/io/PrintWriter;)V

    .line 966
    :cond_8
    return-void
.end method

.method public final a(J)Z
    .locals 9

    .prologue
    const-wide/16 v6, 0x0

    .line 491
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 492
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    invoke-virtual {v0}, Lcom/google/android/gms/car/aw;->b()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    cmp-long v1, p1, v6

    if-gtz v1, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const/4 v1, 0x3

    invoke-virtual {v0, v1, p1, p2}, Lcom/google/android/gms/car/aw;->a(IJ)V

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    sub-long v2, v4, v2

    sub-long v2, p1, v2

    cmp-long v1, v2, v6

    if-lez v1, :cond_2

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/car/aw;->a(IJ)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/car/aw;->b()Z

    move-result v0

    goto :goto_0
.end method

.method public final a()[I
    .locals 4

    .prologue
    .line 340
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 341
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v2

    .line 342
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    new-array v3, v0, [I

    .line 343
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0}, Landroid/util/SparseArray;->size()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    move v1, v0

    :goto_0
    if-ltz v1, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bk;

    .line 345
    iget v0, v0, Lcom/google/android/gms/car/bk;->a:I

    aput v0, v3, v1

    .line 343
    add-int/lit8 v0, v1, -0x1

    move v1, v0

    goto :goto_0

    .line 347
    :cond_0
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-object v3

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(II)Lcom/google/android/gms/car/CarAudioConfiguration;
    .locals 2

    .prologue
    .line 398
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 399
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    .line 400
    if-nez p1, :cond_0

    if-nez v0, :cond_1

    .line 401
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 403
    :cond_1
    if-eqz p2, :cond_2

    .line 404
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 406
    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/car/bl;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    const/4 v1, 0x0

    aget-object v0, v0, v1

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/car/kg;III)Lcom/google/android/gms/car/kj;
    .locals 2

    .prologue
    .line 481
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 482
    if-nez p2, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    if-nez v0, :cond_1

    .line 483
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 485
    :cond_1
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/kg;)Lcom/google/android/gms/car/bj;

    move-result-object v0

    .line 486
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->e:Lcom/google/android/gms/car/nt;

    invoke-virtual {v0, p0, p2, v1}, Lcom/google/android/gms/car/bj;->a(Lcom/google/android/gms/car/bg;ILcom/google/android/gms/car/nt;)Lcom/google/android/gms/car/bf;

    move-result-object v0

    return-object v0
.end method

.method final b(Lcom/google/android/gms/car/bo;)V
    .locals 4

    .prologue
    .line 586
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/bo;Z)Lcom/google/android/gms/car/g;

    move-result-object v0

    .line 587
    if-nez v0, :cond_0

    .line 595
    :goto_0
    return-void

    .line 591
    :cond_0
    const-string v1, "CAR.AUDIO"

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 592
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "stopAudioTrack for track "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 594
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/g;->d()V

    goto :goto_0
.end method

.method public final b(J)Z
    .locals 1

    .prologue
    .line 497
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 498
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->i:Lcom/google/android/gms/car/aw;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/car/aw;->a(J)Z

    move-result v0

    return v0
.end method

.method public final b()[I
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 353
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 354
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    .line 355
    if-nez v1, :cond_0

    .line 356
    new-array v0, v2, [I

    .line 359
    :goto_0
    return-object v0

    .line 358
    :cond_0
    const/4 v0, 0x1

    new-array v0, v0, [I

    iget v1, v1, Lcom/google/android/gms/car/bl;->a:I

    aput v1, v0, v2

    goto :goto_0
.end method

.method public final b(I)[Lcom/google/android/gms/car/CarAudioConfiguration;
    .locals 3

    .prologue
    .line 364
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 365
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 366
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bk;

    .line 367
    if-nez v0, :cond_0

    .line 368
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "CarNotSupported"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 371
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 370
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/car/bk;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-object v0
.end method

.method public final c(II)I
    .locals 3

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 412
    iget-object v1, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    monitor-enter v1

    .line 413
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->b:Landroid/util/SparseArray;

    invoke-virtual {v0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bk;

    .line 414
    if-nez v0, :cond_0

    .line 415
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "CarNotSupported"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 418
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 417
    :cond_0
    :try_start_1
    iget-object v0, v0, Lcom/google/android/gms/car/bk;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    aget-object v0, v0, p2

    invoke-static {v0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/CarAudioConfiguration;)I

    move-result v0

    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return v0
.end method

.method final c()V
    .locals 2

    .prologue
    .line 507
    iget-boolean v0, p0, Lcom/google/android/gms/car/bg;->f:Z

    if-nez v0, :cond_0

    .line 508
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 510
    :cond_0
    return-void
.end method

.method final c(Lcom/google/android/gms/car/bo;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 599
    invoke-virtual {p1}, Lcom/google/android/gms/car/bo;->a()I

    move-result v1

    .line 600
    invoke-static {v1}, Lcom/google/android/gms/car/bg;->d(I)V

    .line 601
    invoke-static {v1}, Lcom/google/android/gms/car/bg;->a(I)I

    move-result v1

    .line 602
    iget-object v2, p0, Lcom/google/android/gms/car/bg;->h:[Lcom/google/android/gms/car/bh;

    aget-object v2, v2, v1

    .line 604
    iget-object v3, p0, Lcom/google/android/gms/car/bg;->c:[Lcom/google/android/gms/car/g;

    aget-object v1, v3, v1

    .line 605
    if-nez v1, :cond_1

    .line 612
    :cond_0
    :goto_0
    return v0

    .line 609
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/car/bh;->b()Lcom/google/android/gms/car/bo;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 612
    invoke-virtual {v1}, Lcom/google/android/gms/car/g;->e()Z

    move-result v0

    goto :goto_0
.end method

.method public final c(I)[Lcom/google/android/gms/car/CarAudioConfiguration;
    .locals 2

    .prologue
    .line 388
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 389
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    .line 390
    if-nez p1, :cond_0

    if-nez v0, :cond_1

    .line 391
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 393
    :cond_1
    iget-object v0, v0, Lcom/google/android/gms/car/bl;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    return-object v0
.end method

.method public final d(II)I
    .locals 2

    .prologue
    .line 423
    invoke-virtual {p0}, Lcom/google/android/gms/car/bg;->c()V

    .line 424
    iget-object v0, p0, Lcom/google/android/gms/car/bg;->g:Lcom/google/android/gms/car/bl;

    .line 425
    if-nez p1, :cond_0

    if-nez v0, :cond_1

    .line 426
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotSupported"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :cond_1
    if-eqz p2, :cond_2

    .line 429
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "index out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 431
    :cond_2
    iget-object v0, v0, Lcom/google/android/gms/car/bl;->b:[Lcom/google/android/gms/car/CarAudioConfiguration;

    aget-object v0, v0, p2

    invoke-static {v0}, Lcom/google/android/gms/car/bg;->a(Lcom/google/android/gms/car/CarAudioConfiguration;)I

    move-result v0

    return v0
.end method
