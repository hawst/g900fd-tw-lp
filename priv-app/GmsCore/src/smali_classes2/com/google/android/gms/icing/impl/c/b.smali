.class public final Lcom/google/android/gms/icing/impl/c/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:[Lcom/google/android/gms/icing/impl/c/a;

.field public final b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

.field public final c:Z

.field public final d:Lcom/google/android/gms/icing/impl/r;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/n;Ljava/util/List;Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;Lcom/google/android/gms/icing/impl/r;)V
    .locals 4

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    new-instance v1, Ljava/util/ArrayList;

    invoke-interface {p2}, Ljava/util/List;->size()I

    move-result v0

    invoke-direct {v1, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 41
    invoke-interface {p2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/g;

    .line 43
    iget-object v3, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {p4, v3}, Lcom/google/android/gms/icing/impl/r;->a(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 44
    const-string v3, "Blacklisted %s"

    iget-object v0, v0, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-static {v3, v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;Ljava/lang/Object;)I

    goto :goto_0

    .line 47
    :cond_0
    new-instance v3, Lcom/google/android/gms/icing/impl/c/a;

    invoke-direct {v3, p1, v0}, Lcom/google/android/gms/icing/impl/c/a;-><init>(Lcom/google/android/gms/icing/impl/n;Lcom/google/android/gms/icing/g;)V

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 49
    :cond_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v0

    new-array v0, v0, [Lcom/google/android/gms/icing/impl/c/a;

    invoke-interface {v1, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/impl/c/a;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    .line 51
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/c/b;->b:Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;

    .line 52
    invoke-interface {p1}, Lcom/google/android/gms/icing/impl/n;->a()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/impl/c/b;->c:Z

    .line 53
    iput-object p4, p0, Lcom/google/android/gms/icing/impl/c/b;->d:Lcom/google/android/gms/icing/impl/r;

    .line 54
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/icing/ap;)Lcom/google/android/gms/appdatasearch/SearchResults;
    .locals 11

    .prologue
    const/4 v6, 0x0

    const/4 v4, 0x0

    .line 98
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v0, v0

    new-array v3, v0, [Ljava/lang/String;

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v0, v0

    new-array v7, v0, [Landroid/os/Bundle;

    .line 100
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v0, v0

    new-array v8, v0, [Landroid/os/Bundle;

    .line 102
    new-instance v5, Landroid/util/SparseIntArray;

    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v0, v0

    invoke-direct {v5, v0}, Landroid/util/SparseIntArray;-><init>(I)V

    move v1, v4

    .line 103
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    array-length v0, v0

    if-ge v1, v0, :cond_0

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/b;->a:[Lcom/google/android/gms/icing/impl/c/a;

    aget-object v2, v0, v1

    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v9, v2, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v9, v9, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v9, "-"

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v9, v2, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget-object v9, v9, Lcom/google/android/gms/icing/g;->b:Ljava/lang/String;

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v3, v1

    .line 107
    iget-object v0, p1, Lcom/google/android/gms/icing/ap;->d:[Lcom/google/android/gms/icing/aq;

    aget-object v0, v0, v1

    iget v9, p1, Lcom/google/android/gms/icing/ap;->b:I

    invoke-virtual {v2, v0}, Lcom/google/android/gms/icing/impl/c/a;->a(Lcom/google/android/gms/icing/aq;)Landroid/util/Pair;

    move-result-object v9

    .line 109
    iget-object v0, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    aput-object v0, v7, v1

    .line 110
    iget-object v0, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Landroid/os/Bundle;

    aput-object v0, v8, v1

    .line 112
    iget-object v0, v2, Lcom/google/android/gms/icing/impl/c/a;->b:Lcom/google/android/gms/icing/g;

    iget v0, v0, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v5, v0, v1}, Landroid/util/SparseIntArray;->put(II)V

    .line 103
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 114
    :cond_0
    iget-object v2, p1, Lcom/google/android/gms/icing/ap;->c:[I

    .line 115
    :goto_1
    iget v0, p1, Lcom/google/android/gms/icing/ap;->b:I

    if-ge v4, v0, :cond_1

    .line 116
    aget v0, v2, v4

    invoke-virtual {v5, v0}, Landroid/util/SparseIntArray;->get(I)I

    move-result v0

    aput v0, v2, v4

    .line 115
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 119
    :cond_1
    iget-object v4, p1, Lcom/google/android/gms/icing/ap;->f:[I

    .line 120
    iget-object v5, p1, Lcom/google/android/gms/icing/ap;->g:[B

    .line 121
    iget v1, p1, Lcom/google/android/gms/icing/ap;->b:I

    iget-object v0, p1, Lcom/google/android/gms/icing/ap;->h:Lcom/google/android/gms/icing/at;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/e/d;->a(Lcom/google/protobuf/nano/j;)[B

    move-result-object v9

    iget v0, p1, Lcom/google/android/gms/icing/ap;->b:I

    if-lez v0, :cond_2

    iget-object v0, p1, Lcom/google/android/gms/icing/ap;->i:[D

    array-length v0, v0

    if-nez v0, :cond_2

    move-object v10, v6

    :goto_2
    new-instance v0, Lcom/google/android/gms/appdatasearch/SearchResults;

    invoke-direct/range {v0 .. v10}, Lcom/google/android/gms/appdatasearch/SearchResults;-><init>(I[I[Ljava/lang/String;[I[B[Landroid/os/Bundle;[Landroid/os/Bundle;[Landroid/os/Bundle;[B[D)V

    return-object v0

    :cond_2
    iget-object v10, p1, Lcom/google/android/gms/icing/ap;->i:[D

    goto :goto_2
.end method
