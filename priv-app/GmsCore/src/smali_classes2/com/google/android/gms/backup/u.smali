.class public final Lcom/google/android/gms/backup/u;
.super Lcom/google/protobuf/a/f;
.source "SourceFile"


# instance fields
.field a:Z

.field b:I

.field c:Ljava/util/List;

.field d:Ljava/util/List;

.field private e:Z

.field private f:I

.field private g:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 8
    invoke-direct {p0}, Lcom/google/protobuf/a/f;-><init>()V

    .line 670
    iput v0, p0, Lcom/google/android/gms/backup/u;->f:I

    .line 687
    iput v0, p0, Lcom/google/android/gms/backup/u;->b:I

    .line 703
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    .line 736
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    .line 804
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/backup/u;->g:I

    .line 8
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 807
    iget v0, p0, Lcom/google/android/gms/backup/u;->g:I

    if-gez v0, :cond_0

    .line 809
    invoke-virtual {p0}, Lcom/google/android/gms/backup/u;->b()I

    .line 811
    :cond_0
    iget v0, p0, Lcom/google/android/gms/backup/u;->g:I

    return v0
.end method

.method public final synthetic a(Lcom/google/protobuf/a/b;)Lcom/google/protobuf/a/f;
    .locals 1

    .prologue
    .line 5
    invoke-virtual {p0, p1}, Lcom/google/android/gms/backup/u;->b(Lcom/google/protobuf/a/b;)Lcom/google/android/gms/backup/u;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/a/c;)V
    .locals 3

    .prologue
    .line 790
    iget-boolean v0, p0, Lcom/google/android/gms/backup/u;->e:Z

    if-eqz v0, :cond_0

    .line 791
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/backup/u;->f:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->b(II)V

    .line 793
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/v;

    .line 794
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_0

    .line 796
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/w;

    .line 797
    const/4 v2, 0x7

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/a/c;->a(ILcom/google/protobuf/a/f;)V

    goto :goto_1

    .line 799
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/backup/u;->a:Z

    if-eqz v0, :cond_3

    .line 800
    const/16 v0, 0xb

    iget v1, p0, Lcom/google/android/gms/backup/u;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/c;->a(II)V

    .line 802
    :cond_3
    return-void
.end method

.method public final b()I
    .locals 4

    .prologue
    .line 816
    const/4 v0, 0x0

    .line 817
    iget-boolean v1, p0, Lcom/google/android/gms/backup/u;->e:Z

    if-eqz v1, :cond_0

    .line 818
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/backup/u;->f:I

    invoke-static {v0, v1}, Lcom/google/protobuf/a/c;->e(II)I

    move-result v0

    add-int/lit8 v0, v0, 0x0

    .line 821
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/v;

    .line 822
    const/4 v3, 0x2

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v0, v1

    move v1, v0

    .line 824
    goto :goto_0

    .line 825
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/backup/w;

    .line 826
    const/4 v3, 0x7

    invoke-static {v3, v0}, Lcom/google/protobuf/a/c;->c(ILcom/google/protobuf/a/f;)I

    move-result v0

    add-int/2addr v1, v0

    .line 828
    goto :goto_1

    .line 829
    :cond_2
    iget-boolean v0, p0, Lcom/google/android/gms/backup/u;->a:Z

    if-eqz v0, :cond_3

    .line 830
    const/16 v0, 0xb

    iget v2, p0, Lcom/google/android/gms/backup/u;->b:I

    invoke-static {v0, v2}, Lcom/google/protobuf/a/c;->d(II)I

    move-result v0

    add-int/2addr v1, v0

    .line 833
    :cond_3
    iput v1, p0, Lcom/google/android/gms/backup/u;->g:I

    .line 834
    return v1
.end method

.method public final b(Lcom/google/protobuf/a/b;)Lcom/google/android/gms/backup/u;
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 842
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->a()I

    move-result v0

    .line 843
    sparse-switch v0, :sswitch_data_0

    .line 847
    invoke-virtual {p1, v0}, Lcom/google/protobuf/a/b;->b(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 848
    :sswitch_0
    return-object p0

    .line 853
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->h()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/u;->e:Z

    iput v0, p0, Lcom/google/android/gms/backup/u;->f:I

    goto :goto_0

    .line 857
    :sswitch_2
    new-instance v0, Lcom/google/android/gms/backup/v;

    invoke-direct {v0}, Lcom/google/android/gms/backup/v;-><init>()V

    .line 858
    const/4 v1, 0x2

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    .line 859
    iget-object v1, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_1

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/backup/u;->c:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 863
    :sswitch_3
    new-instance v0, Lcom/google/android/gms/backup/w;

    invoke-direct {v0}, Lcom/google/android/gms/backup/w;-><init>()V

    .line 864
    const/4 v1, 0x7

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/a/b;->a(Lcom/google/protobuf/a/f;I)V

    .line 865
    iget-object v1, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 869
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/a/b;->c()I

    move-result v0

    iput-boolean v2, p0, Lcom/google/android/gms/backup/u;->a:Z

    iput v0, p0, Lcom/google/android/gms/backup/u;->b:I

    goto :goto_0

    .line 843
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x13 -> :sswitch_2
        0x3b -> :sswitch_3
        0x58 -> :sswitch_4
    .end sparse-switch
.end method

.method public final c()I
    .locals 1

    .prologue
    .line 741
    iget-object v0, p0, Lcom/google/android/gms/backup/u;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    return v0
.end method
