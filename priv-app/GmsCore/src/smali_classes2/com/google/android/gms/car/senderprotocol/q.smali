.class public final Lcom/google/android/gms/car/senderprotocol/q;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# static fields
.field public static a:Ljava/lang/String;

.field public static b:[B

.field public static c:[B

.field public static d:[B


# instance fields
.field public e:Lcom/google/android/c/b/cq;

.field f:I

.field g:I

.field private final k:Lcom/google/android/gms/car/senderprotocol/w;

.field private final l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

.field private final m:Lcom/google/android/gms/car/senderprotocol/v;

.field private final n:Lcom/google/android/gms/car/senderprotocol/t;

.field private final o:Lcom/google/android/gms/car/eb;

.field private final p:Lcom/google/android/gms/car/senderprotocol/u;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 160
    invoke-direct {p0, v0, v0}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 161
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/q;->k:Lcom/google/android/gms/car/senderprotocol/w;

    .line 162
    iput-object p2, p0, Lcom/google/android/gms/car/senderprotocol/q;->m:Lcom/google/android/gms/car/senderprotocol/v;

    .line 163
    iput-object p3, p0, Lcom/google/android/gms/car/senderprotocol/q;->p:Lcom/google/android/gms/car/senderprotocol/u;

    .line 164
    iput-object p4, p0, Lcom/google/android/gms/car/senderprotocol/q;->n:Lcom/google/android/gms/car/senderprotocol/t;

    .line 165
    iput-object p5, p0, Lcom/google/android/gms/car/senderprotocol/q;->o:Lcom/google/android/gms/car/eb;

    .line 167
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    invoke-direct {v0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    .line 168
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->a()Z

    move-result v0

    .line 169
    if-nez v0, :cond_0

    .line 170
    const-string v0, "CAR.GAL"

    const-string v1, "Failed to initialize ssl library!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 178
    :goto_0
    return-void

    .line 174
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->k:Lcom/google/android/gms/car/senderprotocol/w;

    invoke-interface {v0, p0}, Lcom/google/android/gms/car/senderprotocol/w;->a(Lcom/google/android/gms/car/senderprotocol/q;)V

    .line 175
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->m:Lcom/google/android/gms/car/senderprotocol/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/car/senderprotocol/v;->a(Lcom/google/android/gms/car/senderprotocol/q;)V

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->p:Lcom/google/android/gms/car/senderprotocol/u;

    invoke-interface {v0, p0}, Lcom/google/android/gms/car/senderprotocol/u;->a(Lcom/google/android/gms/car/senderprotocol/q;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->n:Lcom/google/android/gms/car/senderprotocol/t;

    invoke-interface {v0, p0}, Lcom/google/android/gms/car/senderprotocol/t;->a(Lcom/google/android/gms/car/senderprotocol/q;)V

    goto :goto_0
.end method

.method private a(ZI)V
    .locals 1

    .prologue
    .line 283
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->o:Lcom/google/android/gms/car/eb;

    invoke-interface {v0, p2}, Lcom/google/android/gms/car/eb;->a(I)V

    .line 284
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->g()Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v0

    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/senderprotocol/k;->a(Z)V

    .line 285
    return-void
.end method


# virtual methods
.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v4, 0x6

    const/4 v2, 0x1

    const/4 v3, 0x0

    const/4 v1, 0x3

    .line 299
    sparse-switch p1, :sswitch_data_0

    .line 377
    const-string v0, "CAR.GAL"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received unexpected message of type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 380
    :cond_0
    :goto_0
    return-void

    .line 301
    :sswitch_0
    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v0

    invoke-virtual {p2}, Ljava/nio/ByteBuffer;->getShort()S

    move-result v4

    const-string v5, "CAR.GAL"

    invoke-static {v5, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "CAR.GAL"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Car requests protocol version "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "."

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_1
    if-ne v0, v2, :cond_2

    :goto_1
    const/16 v5, 0x8

    invoke-static {v5}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v5

    invoke-virtual {v5, v8}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    invoke-virtual {v5, v2}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    int-to-short v6, v3

    invoke-virtual {v5, v6}, Ljava/nio/ByteBuffer;->putShort(S)Ljava/nio/ByteBuffer;

    invoke-virtual {p0, v5}, Lcom/google/android/gms/car/senderprotocol/q;->c(Ljava/nio/ByteBuffer;)V

    if-eqz v3, :cond_3

    invoke-direct {p0, v2, v2}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto :goto_0

    :cond_2
    const/4 v3, -0x1

    goto :goto_1

    :cond_3
    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->f:I

    iput v4, p0, Lcom/google/android/gms/car/senderprotocol/q;->g:I

    const-string v0, "CAR.GAL"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.GAL"

    const-string v1, "Waiting for authentication..."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 304
    :sswitch_1
    new-instance v0, Lcom/google/android/c/b/cq;

    invoke-direct {v0}, Lcom/google/android/c/b/cq;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/cq;

    .line 306
    if-nez v0, :cond_4

    const-string v0, "CAR.GAL"

    const-string v1, "Bad service discovery response! Cannot proceed!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto :goto_0

    :cond_4
    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->e:Lcom/google/android/c/b/cq;

    new-instance v1, Lcom/google/android/gms/car/senderprotocol/r;

    invoke-direct {v1, p0, v0}, Lcom/google/android/gms/car/senderprotocol/r;-><init>(Lcom/google/android/gms/car/senderprotocol/q;Lcom/google/android/c/b/cq;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    goto/16 :goto_0

    .line 309
    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    invoke-virtual {v0, p2}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    move-result-object v6

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v2

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->arrayOffset()I

    move-result v4

    invoke-virtual {v6}, Ljava/nio/ByteBuffer;->limit()I

    move-result v5

    move-object v0, p0

    invoke-super/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/az;->a(I[BZII)V

    invoke-static {v6}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b()I

    move-result v0

    if-ne v0, v1, :cond_6

    const-string v0, "CAR.GAL"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.GAL"

    const-string v1, "Authentication succeeded! Awaiting auth complete message."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_6
    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    const-string v0, "CAR.GAL"

    const-string v1, "Authentication failed. Terminating connection."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x7

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto/16 :goto_0

    .line 312
    :sswitch_3
    new-instance v0, Lcom/google/android/c/b/i;

    invoke-direct {v0}, Lcom/google/android/c/b/i;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/i;

    .line 314
    iget v0, v0, Lcom/google/android/c/b/i;->a:I

    if-eqz v0, :cond_7

    .line 315
    const-string v0, "CAR.GAL"

    const-string v1, "Remote end reported an auth failure."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 316
    const/4 v0, 0x7

    invoke-direct {p0, v3, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto/16 :goto_0

    .line 318
    :cond_7
    const-string v0, "CAR.GAL"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_8

    .line 319
    const-string v0, "CAR.GAL"

    const-string v1, "Remote end reported auth success."

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 322
    :cond_8
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->g()Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->l:Lcom/google/android/gms/car/senderprotocol/SslWrapper;

    iget-object v0, v0, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/senderprotocol/y;->a(Lcom/google/android/gms/car/senderprotocol/SslWrapper;)V

    .line 323
    new-instance v0, Lcom/google/android/c/b/cp;

    invoke-direct {v0}, Lcom/google/android/c/b/cp;-><init>()V

    .line 324
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/q;->b:[B

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/cp;->a([B)Lcom/google/android/c/b/cp;

    .line 325
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/q;->c:[B

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/cp;->b([B)Lcom/google/android/c/b/cp;

    .line 326
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/q;->d:[B

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/cp;->c([B)Lcom/google/android/c/b/cp;

    .line 327
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/q;->a:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/cp;->a(Ljava/lang/String;)Lcom/google/android/c/b/cp;

    .line 328
    const/4 v1, 0x5

    invoke-static {v0}, Lcom/google/android/c/b/cp;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    goto/16 :goto_0

    .line 333
    :sswitch_4
    new-instance v0, Lcom/google/android/c/b/cc;

    invoke-direct {v0}, Lcom/google/android/c/b/cc;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/cc;

    .line 334
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->k:Lcom/google/android/gms/car/senderprotocol/w;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/w;->a(Lcom/google/android/c/b/cc;)V

    goto/16 :goto_0

    .line 337
    :sswitch_5
    new-instance v0, Lcom/google/android/c/b/cd;

    invoke-direct {v0}, Lcom/google/android/c/b/cd;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    .line 339
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->k:Lcom/google/android/gms/car/senderprotocol/w;

    goto/16 :goto_0

    .line 342
    :sswitch_6
    const-string v0, "CAR.GAL"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_9

    .line 343
    const-string v0, "CAR.GAL"

    const-string v1, "received ByeByeRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    :cond_9
    new-instance v0, Lcom/google/android/c/b/n;

    invoke-direct {v0}, Lcom/google/android/c/b/n;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/n;

    .line 347
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->m:Lcom/google/android/gms/car/senderprotocol/v;

    iget v0, v0, Lcom/google/android/c/b/n;->a:I

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/v;->b(I)V

    goto/16 :goto_0

    .line 350
    :sswitch_7
    const-string v0, "CAR.GAL"

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 351
    const-string v0, "CAR.GAL"

    const-string v1, "received ByeByeResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 354
    :cond_a
    new-instance v0, Lcom/google/android/c/b/o;

    invoke-direct {v0}, Lcom/google/android/c/b/o;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    .line 356
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->m:Lcom/google/android/gms/car/senderprotocol/v;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/v;->c()V

    goto/16 :goto_0

    .line 359
    :sswitch_8
    new-instance v0, Lcom/google/android/c/b/bl;

    invoke-direct {v0}, Lcom/google/android/c/b/bl;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/bl;

    .line 361
    const-string v4, "CAR.GAL"

    invoke-static {v4, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_b

    const-string v1, "CAR.GAL"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Navigation focus is now: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v5, v0, Lcom/google/android/c/b/bl;->a:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_b
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->n:Lcom/google/android/gms/car/senderprotocol/t;

    iget v0, v0, Lcom/google/android/c/b/bl;->a:I

    if-ne v0, v8, :cond_c

    move v3, v2

    :cond_c
    invoke-interface {v1, v3}, Lcom/google/android/gms/car/senderprotocol/t;->a(Z)V

    goto/16 :goto_0

    .line 364
    :sswitch_9
    new-instance v0, Lcom/google/android/c/b/g;

    invoke-direct {v0}, Lcom/google/android/c/b/g;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/q;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/g;

    .line 366
    iget v1, v0, Lcom/google/android/c/b/g;->a:I

    const-string v0, "CAR.GAL"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_d

    const-string v2, "CAR.AUDIO"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "Got audio focus state from car:"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch v1, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_2
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_d
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->p:Lcom/google/android/gms/car/senderprotocol/u;

    invoke-interface {v0, v1}, Lcom/google/android/gms/car/senderprotocol/u;->a(I)V

    goto/16 :goto_0

    :pswitch_0
    const-string v0, "AUDIO_FOCUS_STATE_GAIN"

    goto :goto_2

    :pswitch_1
    const-string v0, "AUDIO_FOCUS_STATE_GAIN_TRANSIENT"

    goto :goto_2

    :pswitch_2
    const-string v0, "AUDIO_FOCUS_STATE_LOSS"

    goto :goto_2

    :pswitch_3
    const-string v0, "AUDIO_FOCUS_STATE_LOSS_TRANSIENT_CAN_DUCK"

    goto :goto_2

    :pswitch_4
    const-string v0, "AUDIO_FOCUS_STATE_LOSS_TRANSIENT"

    goto :goto_2

    :pswitch_5
    const-string v0, "AUDIO_FOCUS_STATE_GAIN_MEDIA_ONLY"

    goto :goto_2

    :pswitch_6
    const-string v0, "AUDIO_FOCUS_STATE_GAIN_TRANSIENT_GUIDANCE_ONLY"

    goto :goto_2

    .line 369
    :sswitch_a
    const-string v0, "CAR.GAL"

    const-string v1, "Terminating connection due to framing error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto/16 :goto_0

    .line 373
    :sswitch_b
    const-string v0, "CAR.GAL"

    const-string v1, "Terminating connection due to unexpected message error."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 374
    invoke-direct {p0, v3, v4}, Lcom/google/android/gms/car/senderprotocol/q;->a(ZI)V

    goto/16 :goto_0

    .line 299
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x6 -> :sswitch_1
        0xb -> :sswitch_4
        0xc -> :sswitch_5
        0xe -> :sswitch_8
        0xf -> :sswitch_6
        0x10 -> :sswitch_7
        0x13 -> :sswitch_9
        0xff -> :sswitch_b
        0xffff -> :sswitch_a
    .end sparse-switch

    .line 366
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
    .end packed-switch
.end method

.method public final a(J)V
    .locals 3

    .prologue
    .line 390
    new-instance v0, Lcom/google/android/c/b/cd;

    invoke-direct {v0}, Lcom/google/android/c/b/cd;-><init>()V

    .line 391
    iput-wide p1, v0, Lcom/google/android/c/b/cd;->a:J

    .line 392
    const/16 v1, 0xc

    invoke-static {v0}, Lcom/google/android/c/b/cc;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 393
    return-void
.end method

.method public final b()V
    .locals 4

    .prologue
    const/4 v0, 0x1

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->e:Lcom/google/android/c/b/cq;

    iget-object v1, v1, Lcom/google/android/c/b/cq;->a:[Lcom/google/android/c/b/co;

    .line 266
    new-instance v2, Lcom/google/android/gms/car/senderprotocol/s;

    invoke-direct {v2, p0, v1}, Lcom/google/android/gms/car/senderprotocol/s;-><init>(Lcom/google/android/gms/car/senderprotocol/q;[Lcom/google/android/c/b/co;)V

    invoke-static {v2}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;)V

    .line 277
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/q;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v1}, Lcom/google/android/gms/car/senderprotocol/e;->g()Lcom/google/android/gms/car/senderprotocol/k;

    move-result-object v1

    iget-object v2, v1, Lcom/google/android/gms/car/senderprotocol/k;->d:Ljava/lang/Object;

    monitor-enter v2

    const/4 v3, 0x1

    :try_start_0
    iput-boolean v3, v1, Lcom/google/android/gms/car/senderprotocol/k;->e:Z

    :goto_0
    const/16 v3, 0xff

    if-ge v0, v3, :cond_1

    iget-object v3, v1, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    iget-object v3, v1, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    aget-object v3, v3, v0

    invoke-virtual {v3}, Lcom/google/android/gms/car/senderprotocol/e;->e()V

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 279
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->e:Lcom/google/android/c/b/cq;

    .line 280
    return-void

    .line 277
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(I)V
    .locals 2

    .prologue
    .line 416
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 417
    const-string v0, "CAR.GAL"

    const-string v1, "send ByeByeRequest"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :cond_0
    new-instance v0, Lcom/google/android/c/b/n;

    invoke-direct {v0}, Lcom/google/android/c/b/n;-><init>()V

    .line 420
    iput p1, v0, Lcom/google/android/c/b/n;->a:I

    .line 421
    const/16 v1, 0xf

    invoke-static {v0}, Lcom/google/android/c/b/n;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 422
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->k:Lcom/google/android/gms/car/senderprotocol/w;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/w;->a()V

    .line 291
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->m:Lcom/google/android/gms/car/senderprotocol/v;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/v;->b()V

    .line 292
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->p:Lcom/google/android/gms/car/senderprotocol/u;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/u;->a()V

    .line 293
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/q;->n:Lcom/google/android/gms/car/senderprotocol/t;

    invoke-interface {v0}, Lcom/google/android/gms/car/senderprotocol/t;->a()V

    .line 294
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->c()V

    .line 295
    return-void
.end method

.method public final c(I)V
    .locals 2

    .prologue
    .line 459
    new-instance v0, Lcom/google/android/c/b/dc;

    invoke-direct {v0}, Lcom/google/android/c/b/dc;-><init>()V

    .line 460
    invoke-virtual {v0, p1}, Lcom/google/android/c/b/dc;->a(I)Lcom/google/android/c/b/dc;

    .line 461
    const/16 v1, 0x11

    invoke-static {v0}, Lcom/google/android/c/b/dc;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 463
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 428
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 429
    const-string v0, "CAR.GAL"

    const-string v1, "send ByeByeResponse"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 431
    :cond_0
    new-instance v0, Lcom/google/android/c/b/o;

    invoke-direct {v0}, Lcom/google/android/c/b/o;-><init>()V

    .line 432
    const/16 v1, 0x10

    invoke-static {v0}, Lcom/google/android/c/b/o;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 433
    return-void
.end method

.method public final d(I)V
    .locals 3

    .prologue
    .line 473
    const-string v0, "CAR.GAL"

    const/4 v1, 0x4

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 474
    const-string v1, "CAR.AUDIO"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v0, "sent audio focus request: "

    invoke-direct {v2, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    packed-switch p1, :pswitch_data_0

    const-string v0, "UNKNOWN"

    :goto_0
    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 477
    :cond_0
    new-instance v0, Lcom/google/android/c/b/h;

    invoke-direct {v0}, Lcom/google/android/c/b/h;-><init>()V

    .line 478
    iput p1, v0, Lcom/google/android/c/b/h;->a:I

    .line 479
    const/16 v1, 0x12

    invoke-static {v0}, Lcom/google/android/c/b/h;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 481
    return-void

    .line 474
    :pswitch_0
    const-string v0, "AUDIO_FOCUS_GAIN"

    goto :goto_0

    :pswitch_1
    const-string v0, "AUDIO_FOCUS_GAIN_TRANSIENT"

    goto :goto_0

    :pswitch_2
    const-string v0, "AUDIO_FOCUS_GAIN_TRANSIENT_MAY_DUCK"

    goto :goto_0

    :pswitch_3
    const-string v0, "AUDIO_FOCUS_RELEASE"

    goto :goto_0

    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 436
    const-string v0, "CAR.GAL"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 437
    const-string v0, "CAR.GAL"

    const-string v1, "requesting navigation focus"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 439
    :cond_0
    new-instance v0, Lcom/google/android/c/b/bm;

    invoke-direct {v0}, Lcom/google/android/c/b/bm;-><init>()V

    .line 440
    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/bm;->a(I)Lcom/google/android/c/b/bm;

    .line 441
    const/16 v1, 0xd

    invoke-static {v0}, Lcom/google/android/c/b/bm;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 442
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 445
    new-instance v0, Lcom/google/android/c/b/bm;

    invoke-direct {v0}, Lcom/google/android/c/b/bm;-><init>()V

    .line 446
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/c/b/bm;->a(I)Lcom/google/android/c/b/bm;

    .line 447
    const/16 v1, 0xd

    invoke-static {v0}, Lcom/google/android/c/b/bm;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/q;->a(I[B)V

    .line 448
    return-void
.end method
