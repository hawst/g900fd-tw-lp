.class public final Lcom/google/android/gms/wallet/shared/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V
    .locals 0

    .prologue
    .line 109
    iput-object p1, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;B)V
    .locals 0

    .prologue
    .line 106
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/shared/a;-><init>(Lcom/google/android/gms/wallet/shared/ApplicationParameters;)V

    return-void
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->b:I

    .line 120
    return-object p0
.end method

.method public final a(Landroid/accounts/Account;)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    .prologue
    .line 133
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->c:Landroid/accounts/Account;

    .line 134
    return-object p0
.end method

.method public final a(Landroid/os/Bundle;)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    .prologue
    .line 167
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-object p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->d:Landroid/os/Bundle;

    .line 168
    return-object p0
.end method

.method public final a(Z)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput-boolean p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->e:Z

    .line 152
    return-object p0
.end method

.method public final b(I)Lcom/google/android/gms/wallet/shared/a;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/wallet/shared/a;->a:Lcom/google/android/gms/wallet/shared/ApplicationParameters;

    iput p1, v0, Lcom/google/android/gms/wallet/shared/ApplicationParameters;->f:I

    .line 163
    return-object p0
.end method
