.class final Lcom/google/android/gms/games/service/statemachine/n;
.super Lcom/google/android/gms/common/util/a/b;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/service/statemachine/m;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/service/statemachine/m;)V
    .locals 0

    .prologue
    .line 32
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    invoke-direct {p0}, Lcom/google/android/gms/common/util/a/b;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    invoke-virtual {v0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    .line 50
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    if-eqz v0, :cond_1

    .line 51
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    iget v1, p1, Landroid/os/Message;->what:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/k;

    .line 52
    if-eqz v0, :cond_1

    .line 53
    iget-object v1, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/games/service/statemachine/l;

    iget-object v3, v1, Lcom/google/android/gms/games/service/statemachine/l;->b:Lcom/google/android/gms/games/service/statemachine/k;

    invoke-virtual {v3, v0}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, v1, Lcom/google/android/gms/games/service/statemachine/l;->a:Lcom/google/android/gms/games/service/statemachine/g;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/g;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v2

    :goto_0
    if-nez v0, :cond_1

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/m;->h:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v1, "Old timer -- ignoring"

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    move v0, v2

    .line 61
    :goto_1
    return v0

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 61
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/service/statemachine/m;->a(Landroid/os/Message;)Z

    move-result v0

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    if-eqz v0, :cond_0

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/m;->i:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/k;

    .line 37
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/k;->c:Lcom/google/android/gms/games/service/statemachine/f;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/f;->a()Lcom/google/android/gms/games/service/statemachine/g;

    goto :goto_0

    .line 40
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/m;->d()V

    .line 41
    return-void
.end method

.method public final c()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/n;->a:Lcom/google/android/gms/games/service/statemachine/m;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/m;->a()V

    .line 46
    return-void
.end method
