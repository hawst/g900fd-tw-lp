.class public final Lcom/google/android/gms/games/ui/headless/upsell/a;
.super Landroid/support/v4/app/m;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 114
    invoke-direct {p0}, Landroid/support/v4/app/m;-><init>()V

    return-void
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 4

    .prologue
    .line 121
    new-instance v0, Landroid/app/Dialog;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/app/Dialog;-><init>(Landroid/content/Context;I)V

    .line 122
    invoke-virtual {v0}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v1

    .line 123
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/view/Window;->requestFeature(I)Z

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/q;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    .line 125
    sget v2, Lcom/google/android/gms/l;->aQ:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 127
    sget v2, Lcom/google/android/gms/j;->mB:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 128
    sget v2, Lcom/google/android/gms/j;->hd:I

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 129
    invoke-virtual {v0, v1}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 131
    return-object v0
.end method

.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1

    .prologue
    .line 136
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onCancel(Landroid/content/DialogInterface;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 139
    if-eqz v0, :cond_0

    .line 140
    invoke-virtual {v0}, Landroid/support/v4/app/q;->finish()V

    .line 142
    :cond_0
    return-void
.end method

.method public final onClick(Landroid/view/View;)V
    .locals 3

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;

    .line 152
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    .line 153
    sget v2, Lcom/google/android/gms/j;->mB:I

    if-ne v1, v2, :cond_1

    .line 154
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->dismiss()V

    .line 155
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->finish()V

    .line 170
    :cond_0
    :goto_0
    return-void

    .line 158
    :cond_1
    sget v2, Lcom/google/android/gms/j;->hd:I

    if-ne v1, v2, :cond_0

    .line 159
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/upsell/a;->dismiss()V

    .line 160
    if-eqz v0, :cond_2

    .line 161
    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->y()Lcom/google/android/gms/games/ui/bh;

    move-result-object v1

    .line 162
    invoke-virtual {v1}, Lcom/google/android/gms/games/ui/bh;->g()Ljava/lang/String;

    move-result-object v1

    const-string v2, "com.google.android.play.games"

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;->a(Lcom/google/android/gms/games/ui/headless/upsell/HeadlessLevelUpUpsellActivity;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 167
    :cond_2
    const-string v0, "HeadlessLvlUpUpsellAct"

    const-string v1, "get_the_app button: no parent; ignoring..."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public final onDismiss(Landroid/content/DialogInterface;)V
    .locals 0

    .prologue
    .line 146
    invoke-super {p0, p1}, Landroid/support/v4/app/m;->onDismiss(Landroid/content/DialogInterface;)V

    .line 147
    return-void
.end method
