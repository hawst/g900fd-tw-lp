.class public final Lcom/google/android/gms/games/service/a/k/a;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/service/g;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I

.field private final f:Landroid/os/Bundle;

.field private final g:[Ljava/lang/String;

.field private final h:Lcom/google/android/gms/games/internal/ConnectionInfo;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/g;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)V
    .locals 2

    .prologue
    .line 27
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;Z)V

    .line 28
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/k/a;->c:Lcom/google/android/gms/games/service/g;

    .line 29
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/k/a;->d:Lcom/google/android/gms/games/a/au;

    .line 30
    iput p3, p0, Lcom/google/android/gms/games/service/a/k/a;->e:I

    .line 31
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/k/a;->g:[Ljava/lang/String;

    .line 32
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/k/a;->f:Landroid/os/Bundle;

    .line 33
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/k/a;->h:Lcom/google/android/gms/games/internal/ConnectionInfo;

    .line 34
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/k/a;->c:Lcom/google/android/gms/games/service/g;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/g;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 46
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 39
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/k/a;->d:Lcom/google/android/gms/games/a/au;

    iget v2, p0, Lcom/google/android/gms/games/service/a/k/a;->e:I

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/k/a;->g:[Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/k/a;->f:Landroid/os/Bundle;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/k/a;->h:Lcom/google/android/gms/games/internal/ConnectionInfo;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;I[Ljava/lang/String;Landroid/os/Bundle;Lcom/google/android/gms/games/internal/ConnectionInfo;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
