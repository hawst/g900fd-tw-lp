.class public final Lcom/google/android/gms/udc/util/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/gms/udc/util/e;->a:Landroid/view/LayoutInflater;

    .line 20
    iput-object p2, p0, Lcom/google/android/gms/udc/util/e;->b:Landroid/view/ViewGroup;

    .line 21
    return-void
.end method


# virtual methods
.method public final a(I)Landroid/view/View;
    .locals 3

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/gms/udc/util/e;->a:Landroid/view/LayoutInflater;

    iget-object v1, p0, Lcom/google/android/gms/udc/util/e;->b:Landroid/view/ViewGroup;

    const/4 v2, 0x0

    invoke-virtual {v0, p1, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 25
    if-eqz v0, :cond_0

    .line 26
    iget-object v1, p0, Lcom/google/android/gms/udc/util/e;->b:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 30
    :goto_0
    return-object v0

    .line 28
    :cond_0
    const-string v1, "SectionBuilder"

    const-string v2, "Layout was not inflated"

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method
