.class final Lcom/google/android/gms/games/service/a/l;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field final a:Landroid/os/HandlerThread;

.field b:Lcom/google/android/gms/common/api/v;

.field final synthetic c:Lcom/google/android/gms/games/service/a/k;

.field private final d:Landroid/content/Context;

.field private final e:Lcom/google/android/gms/common/server/ClientContext;

.field private final f:Lcom/google/android/gms/games/service/l;

.field private final g:Lcom/google/android/gms/games/a/t;

.field private final h:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/a/k;Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/games/a/t;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 780
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/l;->c:Lcom/google/android/gms/games/service/a/k;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 781
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/l;->d:Landroid/content/Context;

    .line 782
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/l;->e:Lcom/google/android/gms/common/server/ClientContext;

    .line 783
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/l;->f:Lcom/google/android/gms/games/service/l;

    .line 784
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/l;->g:Lcom/google/android/gms/games/a/t;

    .line 785
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/l;->h:Ljava/lang/String;

    .line 790
    new-instance v0, Landroid/os/HandlerThread;

    const-string v1, "ApiClientConnector"

    invoke-direct {v0, v1}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/l;->a:Landroid/os/HandlerThread;

    .line 791
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->start()V

    .line 792
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 815
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 816
    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 817
    const-string v0, "pendingIntent"

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->d()Landroid/app/PendingIntent;

    move-result-object v1

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 819
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->c:Lcom/google/android/gms/games/service/a/k;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l;->d:Landroid/content/Context;

    invoke-virtual {p1}, Lcom/google/android/gms/common/c;->c()I

    move-result v2

    move-object v4, v3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/service/a/k;->a(Landroid/content/Context;ILandroid/os/IBinder;Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 821
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 822
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 4

    .prologue
    .line 806
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->f:Lcom/google/android/gms/games/service/l;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l;->b:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/l;->a(Lcom/google/android/gms/common/api/v;)V

    .line 807
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->g:Lcom/google/android/gms/games/a/t;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l;->d:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/l;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/l;->h:Ljava/lang/String;

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/gms/games/a/t;->h(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;

    move-result-object v0

    .line 809
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l;->c:Lcom/google/android/gms/games/service/a/k;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/l;->f:Lcom/google/android/gms/games/service/l;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/l;->g:Lcom/google/android/gms/games/a/t;

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/games/service/a/k;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/service/l;Lcom/google/android/gms/games/a/t;)V

    .line 810
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l;->a:Landroid/os/HandlerThread;

    invoke-virtual {v0}, Landroid/os/HandlerThread;->quit()Z

    .line 811
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 826
    return-void
.end method
