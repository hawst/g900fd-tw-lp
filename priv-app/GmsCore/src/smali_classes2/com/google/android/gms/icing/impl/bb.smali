.class public final Lcom/google/android/gms/icing/impl/bb;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Lcom/google/android/gms/icing/impl/be;

.field private static final b:Lcom/google/android/gms/icing/impl/be;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/gms/icing/impl/bc;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/bc;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/bb;->a:Lcom/google/android/gms/icing/impl/be;

    .line 84
    new-instance v0, Lcom/google/android/gms/icing/impl/bd;

    invoke-direct {v0}, Lcom/google/android/gms/icing/impl/bd;-><init>()V

    sput-object v0, Lcom/google/android/gms/icing/impl/bb;->b:Lcom/google/android/gms/icing/impl/be;

    return-void
.end method

.method public static a()Ljava/lang/String;
    .locals 1

    .prologue
    .line 467
    const/4 v0, 0x0

    return-object v0
.end method

.method public static a(I)Ljava/lang/String;
    .locals 2

    .prologue
    .line 507
    if-lez p0, :cond_0

    const/16 v0, 0x14

    if-le p0, v0, :cond_1

    .line 508
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad max num updates: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 510
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Landroid/content/Context;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/lang/String;
    .locals 14

    .prologue
    const-wide/16 v12, 0x0

    const-wide/high16 v10, 0x3ff0000000000000L    # 1.0

    const/4 v1, 0x0

    .line 230
    :try_start_0
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;)V

    .line 231
    const-string v0, "Version tag"

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->c:Ljava/lang/String;

    const/16 v3, 0x64

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/impl/bb;->d(Ljava/lang/String;Ljava/lang/String;I)V

    .line 234
    const-string v0, "Content provider uri"

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-static {v0, v2}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 235
    const-string v0, "Content provider uri"

    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->d:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x800

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 239
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    if-nez v0, :cond_0

    .line 240
    const-string v0, "Section information is missing"

    .line 347
    :goto_0
    return-object v0

    .line 242
    :cond_0
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    const/16 v2, 0x10

    if-le v0, v2, :cond_1

    .line 243
    const-string v0, "Too many sections (max: 16)"

    goto :goto_0

    .line 246
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 247
    iget-object v2, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v4, v2

    move v0, v1

    :goto_1
    if-ge v0, v4, :cond_d

    aget-object v5, v2, v0

    .line 248
    if-nez v5, :cond_2

    .line 249
    const-string v0, "Null section info"

    goto :goto_0

    .line 251
    :cond_2
    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/bb;->d(Ljava/lang/String;)V

    .line 252
    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    if-eqz v6, :cond_3

    sget-object v7, Lcom/google/android/gms/appdatasearch/al;->a:Ljava/util/Set;

    invoke-interface {v7, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_3

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad section format: ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 344
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 253
    :cond_3
    :try_start_1
    const-string v6, "blob"

    iget-object v7, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_4

    iget-boolean v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->d:Z

    if-nez v6, :cond_4

    .line 255
    const-string v0, "Format cannot be indexed"

    goto :goto_0

    .line 257
    :cond_4
    iget v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->e:I

    invoke-static {v6}, Lcom/google/android/gms/icing/impl/bb;->c(I)V

    .line 258
    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->g:Ljava/lang/String;

    if-eqz v6, :cond_5

    invoke-virtual {v6}, Ljava/lang/String;->length()I

    move-result v7

    const/16 v8, 0x18

    if-le v7, v8, :cond_5

    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Subsection separator string too long, max is 24: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 259
    :cond_5
    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-interface {v3, v6}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_6

    .line 260
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Duplicate section name "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 262
    :cond_6
    iget-object v6, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-interface {v3, v6, v5}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 264
    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/aw;->b(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v6

    .line 265
    cmpg-double v8, v6, v12

    if-lez v8, :cond_7

    cmpl-double v8, v6, v10

    if-lez v8, :cond_8

    .line 266
    :cond_7
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad demote common words factor "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 269
    :cond_8
    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/aw;->c(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)D

    move-result-wide v6

    .line 270
    cmpl-double v8, v6, v10

    if-eqz v8, :cond_9

    const-string v8, "rfc822"

    iget-object v9, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-nez v8, :cond_9

    .line 271
    const-string v0, "DemoteRfc822Hostname feature only allowed on rfc822 sections."

    goto/16 :goto_0

    .line 273
    :cond_9
    cmpg-double v8, v6, v12

    if-lez v8, :cond_a

    cmpl-double v8, v6, v10

    if-lez v8, :cond_b

    .line 274
    :cond_a
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad demote rfc822 hostname factor "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v6, v7}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 276
    :cond_b
    invoke-static {v5}, Lcom/google/android/gms/appdatasearch/aw;->a(Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;)Z

    move-result v6

    .line 277
    if-eqz v6, :cond_c

    const-string v6, "url"

    iget-object v5, v5, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v6, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-nez v5, :cond_c

    .line 278
    const-string v0, "omniboxUrlSection only allowed on url section"

    goto/16 :goto_0

    .line 247
    :cond_c
    add-int/lit8 v0, v0, 0x1

    goto/16 :goto_1

    .line 282
    :cond_d
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    if-eqz v0, :cond_f

    .line 283
    invoke-static {p0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    .line 285
    invoke-virtual {v0}, Landroid/accounts/AccountManager;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    array-length v4, v2

    move v0, v1

    :goto_2
    if-ge v0, v4, :cond_1b

    aget-object v5, v2, v0

    .line 286
    iget-object v6, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    invoke-virtual {v6, v5}, Landroid/accounts/Account;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_e

    .line 287
    const/4 v0, 0x1

    .line 291
    :goto_3
    if-nez v0, :cond_f

    .line 292
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Account not found: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->h:Landroid/accounts/Account;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 285
    :cond_e
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 297
    :cond_f
    iget-object v0, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    if-eqz v0, :cond_1a

    .line 298
    iget-object v4, p1, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->i:Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;

    .line 300
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->c:[Ljava/lang/String;

    if-eqz v0, :cond_10

    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->c:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_11

    .line 301
    :cond_10
    const-string v0, "IME section names not defined"

    goto/16 :goto_0

    .line 305
    :cond_11
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 306
    iget-object v6, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->c:[Ljava/lang/String;

    array-length v7, v6

    move v2, v1

    :goto_4
    if-ge v2, v7, :cond_15

    aget-object v8, v6, v2

    .line 307
    invoke-interface {v3, v8}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    .line 308
    if-nez v0, :cond_12

    .line 309
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown section for IME: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 311
    :cond_12
    const-string v9, "blob"

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->c:Ljava/lang/String;

    invoke-virtual {v9, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_13

    .line 312
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Cannot have blob section for IME: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 314
    :cond_13
    invoke-interface {v5, v8}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_14

    .line 315
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Duplicate section for IME: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 317
    :cond_14
    invoke-interface {v5, v8}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 306
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_4

    .line 320
    :cond_15
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->d:Ljava/lang/String;

    if-eqz v0, :cond_16

    .line 321
    const-string v0, "User input tag"

    iget-object v2, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->d:Ljava/lang/String;

    const/16 v5, 0x3e8

    invoke-static {v0, v2, v5}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 325
    :cond_16
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    if-eqz v0, :cond_17

    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    invoke-interface {v3, v0}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_17

    .line 327
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Unknown user input section: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto/16 :goto_0

    .line 330
    :cond_17
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    if-eqz v0, :cond_1a

    .line 331
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    array-length v0, v0

    if-nez v0, :cond_18

    .line 332
    const-string v0, "User input section values empty"

    goto/16 :goto_0

    .line 334
    :cond_18
    iget-object v0, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->e:Ljava/lang/String;

    if-nez v0, :cond_19

    .line 335
    const-string v0, "User input section values set but no name"

    goto/16 :goto_0

    .line 337
    :cond_19
    iget-object v2, v4, Lcom/google/android/gms/appdatasearch/RegisterCorpusIMEInfo;->f:[Ljava/lang/String;

    array-length v3, v2

    move v0, v1

    :goto_5
    if-ge v0, v3, :cond_1a

    aget-object v1, v2, v0

    .line 338
    const-string v4, "User input section value"

    const/16 v5, 0x3e8

    invoke-static {v4, v1, v5}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 337
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 347
    :cond_1a
    const/4 v0, 0x0

    goto/16 :goto_0

    :cond_1b
    move v0, v1

    goto/16 :goto_3
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x10

    .line 485
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    if-eqz v0, :cond_2

    .line 486
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_1

    .line 487
    const-string v0, "Too many tags requested."

    .line 502
    :cond_0
    :goto_0
    return-object v0

    .line 489
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->c:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 490
    const-string v2, "Tag"

    const/16 v3, 0x3e8

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    goto :goto_1

    .line 494
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v0

    if-le v0, v4, :cond_3

    .line 496
    const-string v0, "Too many sections requested."

    goto :goto_0

    .line 498
    :cond_3
    iget v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->i:I

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 499
    if-nez v0, :cond_0

    .line 502
    iget v0, p0, Lcom/google/android/gms/appdatasearch/QuerySpecification;->g:I

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->d(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/ResultClickInfo;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 472
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->b()[Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    .line 473
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->a()Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    if-nez v0, :cond_1

    .line 474
    :cond_0
    const-string v0, "reportResultClick: Invalid input: null paramater"

    .line 481
    :goto_0
    return-object v0

    .line 477
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/ResultClickInfo;->c()I

    move-result v1

    .line 478
    if-ltz v1, :cond_2

    array-length v0, v0

    if-lt v1, v0, :cond_3

    .line 479
    :cond_2
    const-string v0, "reportResultClick: Invalid input: clickPosition out of range"

    goto :goto_0

    .line 481
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/UsageInfo;)Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v1, 0x0

    .line 610
    if-nez p0, :cond_1

    .line 611
    const-string v0, "No usageInfo"

    .line 675
    :cond_0
    :goto_0
    return-object v0

    .line 613
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->a()Lcom/google/android/gms/appdatasearch/DocumentId;

    move-result-object v0

    .line 614
    if-nez v0, :cond_2

    .line 615
    const-string v0, "No document ID"

    goto :goto_0

    .line 617
    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->a()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_3

    const-string v0, "No package name specified"

    .line 618
    :goto_1
    if-nez v0, :cond_0

    .line 621
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->b()J

    move-result-wide v2

    const-wide/16 v4, 0x0

    cmp-long v0, v2, v4

    if-gez v0, :cond_5

    .line 622
    const-string v0, "Negative timestamp specified"

    goto :goto_0

    .line 617
    :cond_3
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->b()Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    const-string v0, "Null corpus name specified"

    goto :goto_1

    :cond_4
    const-string v2, "Uri"

    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/DocumentId;->c()Ljava/lang/String;

    move-result-object v0

    const/16 v3, 0x100

    invoke-static {v2, v0, v3}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v0

    goto :goto_1

    .line 624
    :cond_5
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->c()I

    move-result v0

    .line 625
    if-ltz v0, :cond_6

    const/4 v2, 0x4

    if-le v0, v2, :cond_7

    .line 626
    :cond_6
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad usage type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 628
    :cond_7
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/UsageInfo;->d()Lcom/google/android/gms/appdatasearch/DocumentContents;

    move-result-object v3

    .line 629
    if-eqz v3, :cond_e

    .line 630
    invoke-virtual {v3}, Lcom/google/android/gms/appdatasearch/DocumentContents;->a()[Lcom/google/android/gms/appdatasearch/DocumentSection;

    move-result-object v4

    .line 631
    array-length v0, v4

    const/16 v2, 0x10

    if-le v0, v2, :cond_8

    .line 632
    const-string v0, "Too many sections"

    goto :goto_0

    .line 635
    :cond_8
    array-length v5, v4

    move v2, v1

    :goto_2
    if-ge v2, v5, :cond_d

    aget-object v6, v4, v2

    .line 636
    invoke-virtual {v6}, Lcom/google/android/gms/appdatasearch/DocumentSection;->b()Ljava/lang/String;

    move-result-object v0

    .line 637
    if-nez v0, :cond_0

    .line 640
    sget-object v0, Lcom/google/android/gms/icing/impl/bb;->b:Lcom/google/android/gms/icing/impl/be;

    .line 642
    invoke-virtual {v6}, Lcom/google/android/gms/appdatasearch/DocumentSection;->a()Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    move-result-object v7

    iget-object v7, v7, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->j:Ljava/lang/String;

    .line 643
    const-string v8, "name"

    invoke-virtual {v8, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    .line 644
    sget-object v0, Lcom/google/android/gms/icing/impl/bb;->a:Lcom/google/android/gms/icing/impl/be;

    .line 646
    :cond_9
    iget v7, v6, Lcom/google/android/gms/appdatasearch/DocumentSection;->e:I

    sget v8, Lcom/google/android/gms/appdatasearch/DocumentSection;->a:I

    if-eq v7, v8, :cond_b

    .line 648
    iget v7, v6, Lcom/google/android/gms/appdatasearch/DocumentSection;->e:I

    invoke-static {v7}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v7

    .line 650
    iget-boolean v8, v3, Lcom/google/android/gms/appdatasearch/DocumentContents;->d:Z

    if-nez v8, :cond_a

    .line 651
    const-string v0, "Has global search fields but global search not enabled"

    goto/16 :goto_0

    .line 653
    :cond_a
    const-string v8, "text1"

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_b

    .line 654
    const/4 v1, 0x1

    .line 655
    sget-object v0, Lcom/google/android/gms/icing/impl/bb;->a:Lcom/google/android/gms/icing/impl/be;

    .line 660
    :cond_b
    iget-object v7, v6, Lcom/google/android/gms/appdatasearch/DocumentSection;->c:Ljava/lang/String;

    if-eqz v7, :cond_c

    .line 661
    const-string v7, "Contents"

    iget-object v6, v6, Lcom/google/android/gms/appdatasearch/DocumentSection;->c:Ljava/lang/String;

    invoke-interface {v0, v7, v6}, Lcom/google/android/gms/icing/impl/be;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 667
    :goto_3
    if-nez v0, :cond_0

    .line 635
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_2

    .line 664
    :cond_c
    const-string v7, "Contents"

    iget-object v6, v6, Lcom/google/android/gms/appdatasearch/DocumentSection;->f:[B

    invoke-interface {v0, v7, v6}, Lcom/google/android/gms/icing/impl/be;->a(Ljava/lang/String;[B)Ljava/lang/String;

    move-result-object v0

    goto :goto_3

    .line 671
    :cond_d
    iget-boolean v0, v3, Lcom/google/android/gms/appdatasearch/DocumentContents;->d:Z

    if-eqz v0, :cond_e

    if-nez v1, :cond_e

    .line 672
    const-string v0, "Global search enabled but no title or no data"

    goto/16 :goto_0

    .line 675
    :cond_e
    const/4 v0, 0x0

    goto/16 :goto_0
.end method

.method public static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 352
    if-eqz p0, :cond_0

    .line 353
    :try_start_0
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 358
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 355
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;IILcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 550
    :try_start_0
    const-string v0, "Query"

    const/16 v1, 0x3e8

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 551
    if-ltz p1, :cond_0

    if-gtz p2, :cond_1

    .line 552
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad start and num results: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 558
    :goto_0
    return-object v0

    .line 554
    :cond_1
    invoke-static {p3}, Lcom/google/android/gms/icing/impl/bb;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 558
    const/4 v0, 0x0

    goto :goto_0

    .line 555
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;J)Ljava/lang/String;
    .locals 3

    .prologue
    .line 444
    :try_start_0
    const-string v0, "Corpus name"

    const/16 v1, 0x64

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 445
    const-wide/16 v0, 0x0

    cmp-long v0, p1, v0

    if-gez v0, :cond_0

    .line 446
    const-string v0, "Negative sequence number"
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 451
    :goto_0
    return-object v0

    .line 448
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 451
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 191
    :try_start_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 192
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;I)Ljava/lang/String;
    .locals 6

    .prologue
    const/16 v5, 0x64

    .line 405
    :try_start_0
    const-string v0, "Query"

    const/16 v1, 0x3e8

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 406
    if-nez p1, :cond_0

    if-eqz p2, :cond_0

    .line 407
    const-string v0, "Corpora specified but not package name"

    .line 421
    :goto_0
    return-object v0

    .line 409
    :cond_0
    if-eqz p2, :cond_1

    .line 410
    array-length v1, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, p2, v0

    .line 411
    const-string v3, "Corpus name"

    const/16 v4, 0x64

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 410
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 415
    :cond_1
    if-lez p3, :cond_2

    if-le p3, v5, :cond_3

    .line 416
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad num suggestions: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    goto :goto_0

    .line 418
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 421
    :cond_3
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;IILcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 376
    if-nez p5, :cond_0

    .line 377
    const-string v0, "No query spec defined"

    .line 399
    :goto_0
    return-object v0

    .line 379
    :cond_0
    :try_start_0
    const-string v0, "Query"

    const/16 v1, 0x3e8

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 380
    if-nez p1, :cond_1

    if-eqz p2, :cond_1

    .line 381
    const-string v0, "Corpora specified but not package name"

    goto :goto_0

    .line 383
    :cond_1
    if-eqz p2, :cond_3

    .line 384
    iget-boolean v0, p5, Lcom/google/android/gms/appdatasearch/QuerySpecification;->h:Z

    if-eqz v0, :cond_2

    .line 385
    const-string v0, "Cannot restrict to corpora if requesting semantic section names"

    goto :goto_0

    .line 387
    :cond_2
    array-length v1, p2

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_3

    aget-object v2, p2, v0

    .line 388
    const-string v3, "Corpus name"

    const/16 v4, 0x64

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 387
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 392
    :cond_3
    if-ltz p3, :cond_4

    if-gtz p4, :cond_5

    .line 393
    :cond_4
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad start and num results: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 395
    :cond_5
    invoke-static {p5}, Lcom/google/android/gms/icing/impl/bb;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 399
    const/4 v0, 0x0

    goto :goto_0

    .line 396
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final a(Ljava/lang/String;[BI)Ljava/lang/String;
    .locals 3

    .prologue
    .line 200
    if-eqz p1, :cond_0

    :try_start_0
    array-length v0, p1

    if-nez v0, :cond_1

    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    .line 204
    :goto_0
    return-object v0

    .line 200
    :cond_1
    const v0, 0xfa00

    :try_start_1
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;[BI)V
    :try_end_1
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_0

    .line 204
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 427
    if-nez p2, :cond_0

    .line 428
    const-string v0, "No query spec defined"

    .line 438
    :goto_0
    return-object v0

    .line 430
    :cond_0
    :try_start_0
    const-string v0, "Corpus name"

    const/16 v1, 0x64

    invoke-static {v0, p1, v1}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 431
    array-length v1, p0

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v1, :cond_1

    aget-object v2, p0, v0

    .line 432
    const-string v3, "Uri"

    const/16 v4, 0x100

    invoke-static {v3, v2, v4}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 431
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 434
    :cond_1
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/bb;->a(Lcom/google/android/gms/appdatasearch/QuerySpecification;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 438
    const/4 v0, 0x0

    goto :goto_0

    .line 435
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a([Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 522
    if-eqz p0, :cond_0

    if-nez p1, :cond_1

    .line 523
    :cond_0
    const-string v0, "Null phrases or specs"

    .line 544
    :goto_0
    return-object v0

    .line 525
    :cond_1
    :try_start_0
    array-length v0, p0

    const/16 v1, 0x64

    if-le v0, v1, :cond_2

    .line 526
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Bad num phrases: "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    array-length v1, p0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 528
    :cond_2
    array-length v0, p1

    if-nez v0, :cond_3

    .line 529
    const-string v0, "No corpora names"

    goto :goto_0

    .line 531
    :cond_3
    array-length v3, p1

    const/4 v0, 0x0

    move v2, v0

    :goto_1
    if-ge v2, v3, :cond_6

    aget-object v0, p1, v2

    .line 532
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a()Ljava/util/Map;

    move-result-object v0

    .line 533
    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 534
    const-string v0, "Empty section weights"

    goto :goto_0

    .line 536
    :cond_4
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 537
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bb;->d(Ljava/lang/String;)V

    .line 538
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->c(I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 541
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 531
    :cond_5
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    .line 544
    :cond_6
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;)V
    .locals 4

    .prologue
    .line 563
    if-nez p0, :cond_1

    .line 581
    :cond_0
    return-void

    .line 565
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->a()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;

    .line 566
    iget-object v2, v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 567
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Null package name"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 569
    :cond_3
    iget v2, v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    if-lez v2, :cond_4

    iget v2, v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    const/high16 v3, 0x10000

    if-le v2, v3, :cond_2

    .line 570
    :cond_4
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad weight "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v0, v0, Lcom/google/android/gms/appdatasearch/CorpusScoringInfo;->c:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 573
    :cond_5
    iget v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->f:I

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->d(I)Ljava/lang/String;

    move-result-object v0

    .line 574
    if-eqz v0, :cond_6

    .line 575
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 577
    :cond_6
    iget v0, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchQuerySpecification;->g:I

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->e(I)Ljava/lang/String;

    move-result-object v0

    .line 578
    if-eqz v0, :cond_0

    .line 579
    new-instance v1, Ljava/lang/IllegalArgumentException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private static final a(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3

    .prologue
    .line 114
    if-nez p1, :cond_0

    .line 115
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " null"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    return-void
.end method

.method public static b(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 516
    invoke-static {p0}, Lcom/google/android/gms/icing/impl/bb;->a(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static b(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 363
    if-eqz p0, :cond_0

    .line 364
    :try_start_0
    const-string v0, "Corpus name"

    const/16 v1, 0x64

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 370
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 367
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Ljava/lang/String;Ljava/lang/String;I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 210
    const v0, 0xfa00

    :try_start_0
    invoke-static {p0, p1, v0}, Lcom/google/android/gms/icing/impl/bb;->d(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 214
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 211
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static final b(Ljava/lang/String;[BI)Ljava/lang/String;
    .locals 1

    .prologue
    .line 220
    :try_start_0
    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/Object;)V

    const v0, 0xfa00

    invoke-static {p0, p1, v0}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;[BI)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 224
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 221
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 456
    :try_start_0
    const-string v0, "Corpus name"

    const/16 v1, 0x64

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 460
    const/4 v0, 0x0

    :goto_0
    return-object v0

    .line 457
    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->getMessage()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static final c(I)V
    .locals 3

    .prologue
    .line 167
    if-lez p0, :cond_0

    const/16 v0, 0x3f

    if-le p0, v0, :cond_1

    .line 168
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Bad section weight: ["

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 170
    :cond_1
    return-void
.end method

.method private static final c(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 99
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " empty"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 102
    :cond_0
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 103
    return-void
.end method

.method private static final c(Ljava/lang/String;[BI)V
    .locals 3

    .prologue
    .line 140
    array-length v0, p1

    if-le v0, p2, :cond_0

    .line 141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too long (max "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 143
    :cond_0
    return-void
.end method

.method private static d(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 584
    if-ltz p0, :cond_0

    const/4 v0, 0x3

    if-le p0, v0, :cond_1

    .line 586
    :cond_0
    const-string v0, "Invalid ranking strategy."

    .line 588
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final d(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 156
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/a;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/c/a/a;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/c/a/d;->b(C)Z

    move-result v1

    if-nez v1, :cond_1

    :cond_0
    const-string v1, "Expected a letter or _"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;)V

    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C

    :goto_0
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->a()Z

    move-result v1

    if-nez v1, :cond_3

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->e()C

    move-result v1

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/c/a/d;->c(C)Z

    move-result v1

    if-nez v1, :cond_2

    const-string v1, "Invalid character; only letters, digits and _ are allowed"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/icing/impl/c/a/a;->a(Ljava/lang/String;)V

    :cond_2
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/a;->d()C
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/c/a/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 157
    :catch_0
    move-exception v0

    .line 158
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Invalid section name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 160
    :cond_3
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    const/16 v1, 0x40

    if-le v0, v1, :cond_4

    .line 161
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Section name too long, max is 64: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 164
    :cond_4
    return-void
.end method

.method private static final d(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 121
    invoke-static {p0, p1}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;Ljava/lang/Object;)V

    .line 122
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/icing/impl/bb;->e(Ljava/lang/String;Ljava/lang/String;I)V

    .line 123
    return-void
.end method

.method private static e(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 592
    if-ltz p0, :cond_0

    const/4 v0, 0x2

    if-le p0, v0, :cond_1

    .line 594
    :cond_0
    const-string v0, "Invalid query tokenizer"

    .line 596
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static e(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 181
    const-string v0, "Corpus name"

    const/16 v1, 0x64

    invoke-static {v0, p0, v1}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;Ljava/lang/String;I)V

    .line 182
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Ljava/lang/String;->charAt(I)C

    move-result v0

    const/16 v1, 0x2e

    if-ne v0, v1, :cond_0

    .line 183
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Corpus name cannot start with ."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    return-void
.end method

.method private static final e(Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3

    .prologue
    .line 133
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v0

    if-le v0, p2, :cond_0

    .line 134
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " too long (max "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 136
    :cond_0
    return-void
.end method
