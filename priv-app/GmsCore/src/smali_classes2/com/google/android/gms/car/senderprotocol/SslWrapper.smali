.class public Lcom/google/android/gms/car/senderprotocol/SslWrapper;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[B


# instance fields
.field private b:[Ljava/nio/ByteBuffer;

.field private mNativeAuthState:I

.field private mNativeSslInfo:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const/16 v0, 0x6b0

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->a:[B

    .line 506
    const-string v0, "sslwrapper_jni"

    invoke-static {v0}, Ljava/lang/System;->loadLibrary(Ljava/lang/String;)V

    .line 507
    return-void

    .line 65
    nop

    :array_0
    .array-data 1
        -0x2et
        -0x5et
        -0x2ft
        0x1ft
        0x36t
        0xct
        -0x4at
        -0x60t
        -0x4t
        -0xbt
        -0x38t
        -0x47t
        -0x24t
        0x43t
        0x7dt
        -0x46t
        0x5at
        -0x3et
        0x34t
        -0x9t
        0x50t
        0x4et
        0x52t
        -0x74t
        0x2dt
        0x4et
        -0x1dt
        -0x42t
        -0x10t
        0x50t
        0x30t
        0x17t
        0x3dt
        0x16t
        0x7t
        -0x48t
        0x44t
        -0x71t
        0x79t
        -0x58t
        0x16t
        -0x31t
        0x6ft
        -0x51t
        0x3ft
        -0x75t
        -0x80t
        -0x30t
        0x44t
        0x7ft
        -0x26t
        0x4ft
        -0x57t
        -0x15t
        -0x30t
        0x48t
        0x44t
        0x50t
        -0x46t
        -0x3dt
        -0x1et
        0x7t
        -0x6dt
        -0x16t
        0x3t
        -0x3ft
        0x10t
        -0x2ct
        -0x28t
        0x5dt
        0x1bt
        -0x43t
        0x24t
        -0x11t
        -0x72t
        -0x3bt
        0x6ct
        -0x36t
        -0x74t
        0x1bt
        -0x65t
        0x1bt
        -0x5dt
        -0x4t
        0x4at
        0x39t
        0x43t
        0x7dt
        0x6t
        -0x18t
        0x1bt
        0x66t
        0x4ct
        0x40t
        -0x48t
        -0x76t
        0x2bt
        0xbt
        0x3dt
        0x22t
        -0x46t
        0x4et
        -0x67t
        -0x66t
        -0x77t
        -0x50t
        -0x32t
        -0x16t
        0x3ft
        0x79t
        0x56t
        -0x1at
        0x60t
        0x32t
        0x15t
        -0x27t
        0x32t
        -0x21t
        -0x62t
        -0x42t
        -0x4et
        -0x6t
        0x26t
        0x57t
        0x76t
        -0x48t
        -0x42t
        0x6dt
        0x4at
        0x7t
        -0x1dt
        -0x67t
        0x5bt
        -0x61t
        0x8t
        -0x50t
        -0x76t
        -0x1et
        -0x3ct
        -0x66t
        0x44t
        -0xat
        0x7bt
        -0x42t
        0x2t
        -0x65t
        -0x75t
        -0x2t
        -0x58t
        -0x1ct
        -0x1t
        0x1at
        0x4bt
        0x54t
        -0x77t
        -0x7t
        0x2bt
        0x1bt
        0x61t
        -0xct
        0x9t
        -0x40t
        0x36t
        0x65t
        -0x77t
        0x40t
        0x4dt
        0x37t
        0x6bt
        -0x3at
        -0x12t
        -0x3ft
        -0x70t
        0x32t
        -0x27t
        -0x49t
        0x4at
        -0x41t
        0x10t
        -0x18t
        -0x38t
        -0x75t
        0x62t
        0x5et
        0x23t
        -0x1ft
        -0x80t
        0xct
        -0x80t
        -0x7ct
        -0x6t
        0x6et
        0x15t
        -0x42t
        -0x53t
        0x8t
        0x29t
        -0x8t
        0x20t
        -0x4at
        -0x13t
        -0x62t
        -0x7bt
        0x50t
        -0x7bt
        -0x35t
        0x27t
        0x47t
        0x78t
        -0x38t
        0x6ct
        0x19t
        0x57t
        0x12t
        0x2bt
        -0x6ft
        0x37t
        0x5et
        -0x69t
        0x12t
        -0x37t
        0x36t
        0x55t
        0x1dt
        -0x6at
        0x47t
        0x25t
        0x1ft
        0x1et
        0x6bt
        -0x19t
        0x6dt
        0x56t
        -0x1ct
        0x3ct
        0x4et
        -0x57t
        -0x14t
        -0xct
        0x24t
        0x63t
        0x1ct
        0x77t
        -0x63t
        0x4at
        0x2dt
        -0x5dt
        0xbt
        0x7bt
        -0x3t
        0x1bt
        0x11t
        0x10t
        0x38t
        0x3et
        -0x4et
        0x26t
        0xat
        -0x7dt
        0x62t
        -0x17t
        0x20t
        0x64t
        0x38t
        0x2ft
        0x17t
        0x1ft
        0x79t
        0x32t
        -0x38t
        -0x1ft
        -0x80t
        0x3ct
        0x42t
        -0x7et
        0x4dt
        -0x1et
        -0x37t
        -0x19t
        -0x6bt
        0x1ct
        0x7t
        0x30t
        0xdt
        0x38t
        -0x44t
        0x17t
        0x44t
        0x31t
        0x3t
        -0x4bt
        -0x47t
        -0x70t
        -0x59t
        -0x4at
        -0x69t
        0x6t
        0x1bt
        -0x21t
        -0x49t
        0x66t
        -0x7dt
        0x7bt
        -0xdt
        0x17t
        0x2bt
        0x28t
        0x46t
        0x38t
        -0x2at
        0x1t
        -0x58t
        -0xdt
        -0x44t
        -0x1et
        -0x22t
        0x65t
        -0x7ft
        -0x52t
        0x4t
        -0x2ft
        0x74t
        0x66t
        -0x4bt
        0x55t
        0x52t
        0x56t
        -0x19t
        0x34t
        -0xdt
        -0x7ct
        0x7ct
        0x13t
        0x20t
        -0x2t
        0x52t
        0x10t
        -0x60t
        -0x5ct
        -0x80t
        -0x7et
        -0x7et
        -0xft
        -0x2ct
        -0x80t
        -0x21t
        -0x3ft
        0x7dt
        -0x4t
        0x41t
        0x70t
        0x5ct
        -0x7et
        0x56t
        0x19t
        0x68t
        -0x65t
        0x5at
        -0x4et
        -0x25t
        0x18t
        -0xet
        -0x32t
        -0x10t
        0x41t
        -0x2ct
        -0x3t
        0x7at
        0x2et
        0x6at
        0x72t
        0x22t
        0x75t
        -0x17t
        -0x1ct
        -0x42t
        0x63t
        0x1at
        0x2bt
        -0x14t
        -0x40t
        -0x39t
        -0x79t
        0x6at
        0x34t
        0x45t
        0x5ft
        0x37t
        0x31t
        -0x56t
        0x4bt
        0xat
        0x34t
        0x38t
        -0x25t
        0x4at
        -0xet
        0x53t
        0x1at
        -0x7t
        -0x47t
        -0x1et
        0x6dt
        0x59t
        0x41t
        0x51t
        0x55t
        0x66t
        -0x63t
        -0x19t
        0x6et
        -0x38t
        0x59t
        -0x36t
        0x6t
        0x69t
        0x21t
        -0xat
        -0x65t
        -0x36t
        -0x80t
        0x5t
        0x46t
        -0x9t
        0x6t
        0x12t
        -0xct
        -0xet
        -0x9t
        0x1t
        -0x70t
        -0x24t
        -0x12t
        -0x61t
        0x31t
        -0x5et
        0x3dt
        -0x63t
        -0x24t
        -0x60t
        -0x2ft
        0x2ft
        0x2bt
        -0x64t
        0x62t
        0x14t
        0x68t
        -0x18t
        -0x22t
        0x36t
        -0x57t
        0x76t
        0x67t
        0x21t
        -0x31t
        -0x64t
        -0x77t
        -0x2bt
        0x1ct
        -0x6bt
        0xbt
        0x5t
        -0x75t
        0x66t
        0x32t
        -0x67t
        0x7at
        -0x17t
        0x7bt
        0x73t
        -0x51t
        -0x4t
        -0xct
        0x34t
        0xbt
        -0x44t
        0x30t
        -0x5ct
        -0x27t
        -0x15t
        -0x60t
        0x54t
        0xat
        0x56t
        -0x3bt
        0x14t
        -0x7t
        -0x28t
        -0x55t
        -0x6dt
        0x6et
        0x77t
        -0x4t
        -0x5at
        -0x66t
        -0x19t
        -0x16t
        0x7dt
        -0x33t
        0x60t
        -0x2ft
        0x8t
        -0x55t
        0x10t
        -0x21t
        -0x49t
        -0x18t
        -0x27t
        0x1dt
        -0x30t
        0x52t
        -0x61t
        0x2t
        -0x28t
        0x7et
        -0xft
        0x41t
        -0x38t
        -0x4t
        -0x5bt
        0x24t
        -0x50t
        0x7bt
        -0x20t
        0x35t
        -0x24t
        0x15t
        -0x17t
        -0x2at
        -0x6ft
        -0x21t
        -0xbt
        -0x80t
        -0x4t
        0x62t
        -0x3ft
        -0x30t
        -0x33t
        -0x4ft
        0x67t
        0x70t
        0x34t
        -0x6ft
        0x2dt
        0x4et
        -0x4bt
        0x2et
        0x11t
        -0x80t
        0x55t
        -0x10t
        -0x2at
        0x42t
        0x42t
        -0x20t
        0x3bt
        -0x47t
        -0x37t
        -0x72t
        0x28t
        0x15t
        -0x6ct
        0xct
        0x21t
        0x3t
        0x62t
        -0x5bt
        0x4t
        0x26t
        0x71t
        -0x2bt
        -0x79t
        -0x77t
        -0x4ft
        -0xat
        0x5bt
        0x7t
        -0xet
        0x5bt
        0x37t
        -0x1t
        -0xct
        0x7ct
        0x52t
        -0x52t
        -0x60t
        0xbt
        0x19t
        -0x16t
        -0x78t
        -0x73t
        -0x6dt
        -0x4et
        0x33t
        0x53t
        -0x33t
        -0x41t
        0x5dt
        0x39t
        -0x75t
        0x43t
        0x1ct
        0x2ft
        -0x5dt
        -0x40t
        0x43t
        0xdt
        -0x4et
        0x41t
        0x71t
        -0x66t
        -0x67t
        -0x21t
        0xft
        -0x55t
        -0x68t
        -0x3ct
        -0x6et
        -0x55t
        0x4ct
        -0x4ft
        -0x6ft
        0x5ft
        0x3dt
        0x1bt
        0x1et
        -0x69t
        -0x21t
        0x2t
        -0x3ct
        -0x5t
        -0x18t
        -0x33t
        0x54t
        0x5bt
        0x63t
        -0x63t
        0x3at
        -0x37t
        -0x74t
        0x53t
        0x38t
        0x5ft
        -0x80t
        -0x79t
        -0x26t
        -0x78t
        -0x15t
        0x7et
        -0x3t
        0x62t
        0x50t
        -0x47t
        -0x6dt
        -0x1ft
        -0x1ct
        0x5bt
        -0x24t
        -0x47t
        0x4ft
        0x8t
        -0x9t
        -0x41t
        -0x47t
        -0x5bt
        -0x8t
        0x20t
        -0x4t
        0x30t
        -0x21t
        0x36t
        0x0t
        0xat
        0x21t
        0x3dt
        -0xet
        -0x75t
        -0x32t
        0x15t
        0x2ft
        0x48t
        0x22t
        0x39t
        0x45t
        -0x71t
        0x1ft
        0x7at
        0x7dt
        -0xat
        -0x5at
        -0x72t
        0x7t
        0x64t
        0x75t
        0x38t
        0x0t
        -0x56t
        0x63t
        -0x42t
        0x49t
        0x42t
        0x41t
        0x64t
        0x1t
        -0x48t
        -0x59t
        0x19t
        -0x5ft
        -0x4bt
        0x6bt
        0x4ft
        0x5et
        -0x3bt
        -0x70t
        -0x2t
        0x4ct
        -0x62t
        -0x40t
        0xat
        -0x73t
        -0x7ft
        0x29t
        0x1t
        -0x5dt
        -0x2bt
        -0x25t
        0x32t
        -0x61t
        -0x6at
        -0x5ct
        0x14t
        -0x1at
        -0x2at
        -0x62t
        -0x2dt
        -0x72t
        0x7ft
        -0x4ft
        0x17t
        -0x6dt
        -0x56t
        -0x1dt
        0x4ct
        -0x57t
        -0x6bt
        -0x1et
        0x26t
        0x45t
        0x24t
        -0x7dt
        0x6ft
        -0x6ct
        0x26t
        0x16t
        0x42t
        0x7t
        -0x12t
        0x27t
        -0x45t
        -0x22t
        0x78t
        -0x11t
        -0x36t
        0x12t
        0x6ft
        -0x4ct
        -0x1t
        -0x63t
        -0x64t
        -0x40t
        0x71t
        0x79t
        -0x61t
        0x58t
        -0x15t
        -0x8t
        0x7ct
        0x65t
        0x65t
        -0x57t
        -0x20t
        0x73t
        0x2dt
        0xet
        -0x56t
        0x54t
        -0x5ct
        -0x5ct
        0x29t
        -0x4ft
        0x2at
        -0x42t
        0x3t
        0x9t
        -0x4bt
        -0x50t
        0x4ft
        0x7ct
        -0x7ft
        -0x70t
        -0x6ct
        0x22t
        -0x71t
        -0x7bt
        -0x1et
        -0x3t
        -0x6at
        0x42t
        -0x7et
        -0x40t
        -0x48t
        -0x49t
        0x2at
        0x6ct
        -0x1bt
        -0x4ft
        -0x31t
        -0x33t
        -0x6et
        -0xct
        0x37t
        0x2ct
        0x3ct
        0x56t
        0x4ct
        -0x13t
        0x2et
        0x7ft
        0x2t
        -0x51t
        -0x3ft
        -0x53t
        -0x6at
        -0x34t
        0xat
        0x52t
        -0x76t
        0x63t
        -0x63t
        0x61t
        -0x2at
        -0x27t
        0x36t
        0x3ct
        -0x80t
        0x1ft
        0x17t
        -0x2dt
        -0x9t
        -0x50t
        0x79t
        0x32t
        0x58t
        -0xbt
        0x64t
        -0x5t
        0x38t
        -0x3bt
        -0x8t
        -0xct
        0x65t
        -0x4ct
        0x43t
        -0x35t
        0x3dt
        -0x61t
        0x25t
        -0x47t
        0x7at
        0x23t
        -0x55t
        0xdt
        0x26t
        0x55t
        0x25t
        0x2bt
        0x54t
        0x32t
        -0x2bt
        -0x78t
        0x2dt
        0x3et
        0x43t
        -0x3bt
        -0x7ct
        0x5at
        -0x3et
        0x2bt
        0x2ct
        0x2ct
        -0x6at
        0x8t
        -0xet
        -0xet
        0x19t
        0x46t
        -0x32t
        -0x2dt
        0x9t
        0x49t
        0x1at
        -0x6bt
        -0x28t
        0xbt
        -0x6et
        0x2et
        0x7ct
        0x1at
        0x6at
        -0xft
        -0x36t
        -0x80t
        -0xdt
        0x1ft
        -0x8t
        -0x70t
        -0x4t
        0x2t
        0x1dt
        0x3t
        0x1ft
        -0x20t
        0x1et
        0x44t
        0x69t
        -0x66t
        0x8t
        -0x39t
        -0x22t
        0x7ct
        -0x7dt
        0x69t
        -0xat
        0x3t
        -0x71t
        0x31t
        0x2et
        -0x3dt
        0x26t
        0x76t
        -0x2et
        -0x6bt
        0x2ft
        -0x24t
        0x68t
        -0x15t
        -0xat
        0x7at
        0x5at
        0x61t
        0x0t
        -0x40t
        -0xdt
        0x78t
        0x21t
        -0x43t
        0x20t
        0x62t
        -0x52t
        -0x32t
        0x2t
        -0x1et
        -0xat
        0x7bt
        0x65t
        -0x11t
        -0x79t
        -0x44t
        -0x70t
        0x37t
        -0x7bt
        0x4et
        -0x2t
        0x75t
        0x1ft
        0x35t
        -0x4t
        -0x5dt
        -0x53t
        -0x16t
        -0x62t
        -0x49t
        -0x21t
        0x26t
        0x3dt
        -0x54t
        -0x7et
        -0x6ct
        -0x77t
        -0x79t
        0x2at
        -0xct
        -0x4bt
        0x14t
        0x56t
        0x6ft
        0x44t
        0x6ft
        -0xat
        0x32t
        -0x40t
        0x1at
        -0x7ft
        -0x51t
        0x30t
        -0x78t
        0x3t
        -0x6at
        -0x2ft
        -0x70t
        0x57t
        0x42t
        0x7et
        -0x46t
        0x15t
        -0x7dt
        -0x15t
        0x6at
        -0x24t
        0x42t
        -0x56t
        -0x66t
        -0x55t
        0x6t
        -0x10t
        0x5at
        0x77t
        -0x8t
        0x68t
        0x38t
        -0x6bt
        -0x58t
        0x29t
        -0x8t
        -0x3bt
        -0x3dt
        -0x1bt
        -0x63t
        -0x44t
        -0x80t
        -0x7ft
        -0x5bt
        -0x40t
        0x2dt
        -0x37t
        0x4bt
        -0x74t
        -0x75t
        -0x60t
        -0xbt
        0x50t
        0x5ct
        -0x80t
        0x7ct
        -0x60t
        -0x5at
        0x4t
        0x21t
        -0x25t
        -0x78t
        0x1t
        -0x54t
        0x50t
        -0x4ft
        -0x46t
        -0x41t
        0x2dt
        0x12t
        -0x1ft
        -0x1ct
        -0x54t
        0xet
        -0xft
        -0x18t
        0x60t
        -0x5t
        -0x5bt
        0x1ct
        -0x17t
        -0x1at
        0x62t
        -0x1t
        -0x5ft
        0x70t
        0x11t
        -0x11t
        0x1t
        -0x50t
        0x6at
        -0x2ft
        -0x7t
        0x71t
        0x5at
        -0x57t
        -0x2et
        -0x4dt
        -0x6ft
        -0x33t
        -0x6et
        0x6dt
        -0x8t
        0x73t
        0x1ft
        0x0t
        -0x57t
        0x19t
        0x25t
        0x14t
        -0x41t
        0x7bt
        0x39t
        -0x3bt
        -0xet
        0x1ct
        0x57t
        -0x68t
        0x1ft
        -0x6at
        0x45t
        -0xct
        -0x55t
        -0x1et
        -0x17t
        0x6t
        0x79t
        0xft
        0x54t
        -0x41t
        -0x8t
        -0x65t
        0x72t
        0x78t
        -0x7t
        -0x79t
        -0x5t
        0x7ft
        0xft
        -0x16t
        0x35t
        -0x6bt
        -0x17t
        -0x78t
        -0x47t
        0x33t
        0x22t
        0x22t
        0x57t
        0x43t
        0x61t
        -0x35t
        -0x70t
        -0x7ft
        0x36t
        -0x67t
        0x65t
        0x42t
        0x7bt
        0x52t
        -0x76t
        0x49t
        0x1bt
        0x39t
        0x0t
        -0xbt
        0x77t
        -0x3at
        -0x1t
        0x33t
        -0x12t
        -0x7ft
        0x48t
        0x0t
        0x2et
        0x63t
        -0x58t
        -0x1dt
        0x6ft
        -0x41t
        -0x45t
        -0x49t
        0x63t
        0x5t
        0x7ct
        -0x4ct
        0x53t
        -0x68t
        -0x34t
        -0x24t
        0xdt
        0x55t
        -0x55t
        -0x4dt
        -0x24t
        0x1ct
        0x68t
        -0x62t
        0x71t
        0x47t
        0x20t
        -0x32t
        0x78t
        -0x77t
        -0x2dt
        -0x3et
        -0x66t
        -0x11t
        0x73t
        -0x62t
        -0x51t
        0x14t
        0x42t
        -0x6dt
        0x75t
        0x23t
        0x29t
        0x36t
        0x37t
        -0x6et
        -0x6ft
        -0x3at
        0x2dt
        -0x13t
        -0x8t
        -0x4ft
        0x23t
        0x5bt
        -0x6dt
        0x53t
        0x6bt
        0x2t
        -0x40t
        0x2at
        0x1ft
        0x4ct
        0x66t
        0x22t
        -0x7bt
        0x62t
        0x60t
        0x61t
        0x69t
        0x4at
        0x78t
        -0x4t
        -0x71t
        0x6ct
        -0x58t
        -0x28t
        -0x6bt
        0x47t
        -0x6ct
        -0x7ft
        0x18t
        -0x6bt
        -0x69t
        -0x1at
        -0x4bt
        -0x56t
        0x25t
        0x7dt
        -0x24t
        -0x37t
        -0x4bt
        0x1bt
        -0x14t
        -0x1at
        0x5ct
        0x57t
        0x46t
        0x33t
        -0x33t
        -0x40t
        -0x54t
        -0x14t
        0x44t
        0x38t
        -0x39t
        -0x7bt
        -0x36t
        0x78t
        -0x7at
        -0x68t
        -0x72t
        -0x32t
        0x27t
        0x37t
        -0x39t
        -0x4dt
        -0x35t
        -0x55t
        -0x7bt
        0x68t
        -0x11t
        0x1dt
        -0x1ft
        0x66t
        0x15t
        -0x1at
        0x7ct
        -0x62t
        -0x65t
        -0x3et
        -0x26t
        0x57t
        0x7t
        -0x25t
        0x4ft
        -0x6ct
        -0x37t
        -0x5ft
        0x6et
        0x1ct
        -0x4dt
        -0x4et
        -0x64t
        0x4et
        -0x26t
        -0x5t
        -0x7ft
        0x39t
        0x28t
        -0x3t
        0x23t
        0x3t
        -0x2bt
        -0xft
        -0x6t
        0x2et
        -0x1at
        -0x3ft
        -0x16t
        0x4t
        -0x14t
        0x3et
        0x63t
        -0x4ft
        0x6t
        -0x2dt
        0x7ft
        0x23t
        0x4at
        0x14t
        -0x17t
        0x65t
        0x37t
        0x47t
        0x50t
        0x3at
        0x25t
        -0x25t
        0xat
        0x51t
        0x4bt
        -0x54t
        -0x2dt
        0x37t
        -0x43t
        -0x49t
        0x7ct
        0x7bt
        -0x5et
        -0x66t
        0x75t
        0x3ct
        0x43t
        -0x9t
        0x6ct
        -0x1t
        -0x24t
        -0x6t
        0x2bt
        0x69t
        0xct
        0x5ft
        0x15t
        -0x3t
        0x13t
        0x21t
        0x5ct
        0x3ft
        -0x65t
        -0x26t
        0x1bt
        0x1t
        -0x37t
        -0x10t
        0x6ct
        -0x3at
        -0x3t
        -0x73t
        -0x5bt
        0x41t
        0x40t
        0x4at
        0x6bt
        -0x27t
        -0xat
        0x18t
        -0x31t
        -0x4ct
        0x16t
        0x2ct
        0x16t
        -0x71t
        -0xet
        -0x6dt
        -0x2et
        -0x69t
        -0x6et
        0x2et
        0x76t
        -0x60t
        -0x7ft
        0x46t
        -0x13t
        0x58t
        -0x5et
        0x34t
        -0x1at
        0x49t
        -0x2dt
        -0x1ct
        0x63t
        0x9t
        -0x35t
        0x7ft
        0x15t
        -0x61t
        -0x1t
        -0x1et
        -0x35t
        -0x18t
        -0x54t
        0x57t
        0x39t
        -0x42t
        -0x25t
        -0x64t
        0x5ct
        0x39t
        -0x74t
        -0x48t
        0x70t
        0x9t
        0x17t
        0x54t
        -0x2at
        -0x5at
        0x28t
        -0x2at
        -0xft
        0x51t
        0x58t
        -0x39t
        0x3at
        0x49t
        0x6et
        0x1bt
        -0x1at
        -0x23t
        0x45t
        0x4ft
        0x64t
        0x3bt
        -0x80t
        0x4at
        -0x62t
        -0x33t
        -0x38t
        0x58t
        0xct
        -0x36t
        -0x1et
        0x42t
        -0x50t
        0x7bt
        0x1ct
        -0x77t
        0x77t
        -0x5ft
        0x7dt
        -0x57t
        -0x4dt
        0x2at
        0x33t
        0x1at
        0x2dt
        -0x3et
        0x34t
        0x47t
        0x71t
        0x65t
        -0x28t
        -0x40t
        -0x47t
        0x1ct
        -0x80t
        -0x20t
        0x7et
        0x3et
        -0x5dt
        0x28t
        -0x34t
        0x41t
        0x5t
        -0x7at
        -0x7at
        0x22t
        -0x4et
        0x6at
        0x1t
        0x64t
        -0x5et
        0x7ft
        -0x5t
        -0x2bt
        0x74t
        0xdt
        0x4bt
        0x47t
        0x2bt
        -0xdt
        -0x6ct
        0xdt
        -0x35t
        -0x13t
        -0x9t
        0x73t
        0x3ft
        0x26t
        0x38t
        0x63t
        -0x34t
        0x62t
        -0x20t
        -0x71t
        -0x3t
        0x27t
        -0x2et
        0x1t
        0x42t
        0x71t
        0x6ct
        0x77t
        -0x20t
        -0x19t
        -0x20t
        0x31t
        0x5dt
        -0x6ct
        -0x22t
        -0x45t
        -0x73t
        0x2at
        -0x43t
        -0x7et
        -0x60t
        -0x4dt
        -0x73t
        0x30t
        0x31t
        0x1ft
        0x46t
        0x4ft
        -0x2at
        0x70t
        0x6dt
        0x6at
        0x30t
        0x38t
        -0x3dt
        -0x2ft
        0x68t
        -0x3ft
        -0x45t
        -0x16t
        0x17t
        -0x60t
        0x35t
        -0x61t
        0x0t
        0x1et
        0x6ct
        -0x14t
        -0x45t
        0x51t
        0x3at
        0x2ft
        -0x2dt
        -0x5bt
        -0x10t
        0x18t
        -0x2et
        0x6et
        -0x2t
        0x40t
        -0x7t
        -0x43t
        -0x61t
        -0x58t
        0x6et
        -0x66t
        0x1dt
        0x41t
        0x14t
        0x4t
        0x77t
        0x33t
        -0x76t
        -0x60t
        0x63t
        -0x52t
        0x53t
        -0x3ct
        -0x32t
        0x36t
        -0xdt
        0x38t
        0x42t
        0x1bt
        0x43t
        0x5bt
        0x62t
        -0x24t
        0x65t
        0x4ct
        -0x74t
        -0x20t
        -0x74t
        -0x2at
        0x53t
        0x2et
        -0x7dt
        -0x46t
        0x7ct
        0x2bt
        0x9t
        -0x6ct
        -0x21t
        -0x3at
        0x1ft
        -0x3et
        -0x34t
        -0x27t
        -0x6at
        0x6at
        -0x5ct
        -0x65t
        0x17t
        -0x2at
        -0x41t
        -0x60t
        -0x13t
        0x29t
        0x2at
        0x9t
        0x22t
        0x4dt
        0x1t
        -0x5dt
        0x18t
        0x47t
        -0xdt
        -0x40t
        0x51t
        -0x21t
        -0x38t
        -0x1dt
        -0x46t
        -0x6dt
        -0x51t
        0x71t
        0x60t
        -0x38t
        -0x14t
        -0x7bt
        0x66t
        -0x30t
        0x31t
        0x5et
        0x61t
        0x45t
        -0x60t
        0x4ct
        0x3ct
        0x2ct
        0x4et
        0x5et
        0x60t
        -0x72t
        0xdt
        -0x7dt
        0x55t
        -0x32t
        -0x32t
        -0x66t
        -0x3et
        0x64t
        -0x39t
        0x76t
        0x43t
        -0x13t
        -0x6t
        -0x7ct
        -0x26t
        0x1ft
        0x3ft
        0x41t
        -0x27t
        0x77t
        0x5t
        -0x78t
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 510
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->mNativeAuthState:I

    .line 524
    const/16 v0, 0x40

    new-array v0, v0, [Ljava/nio/ByteBuffer;

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b:[Ljava/nio/ByteBuffer;

    return-void
.end method

.method private native nativeDecryptionPipelineDequeue(Ljava/nio/ByteBuffer;I)I
.end method

.method private native nativeDecryptionPipelineEnqueue(Ljava/nio/ByteBuffer;I)I
.end method

.method private native nativeDecryptionPipelinePending()I
.end method

.method private native nativeEncryptionPipelineDequeue(Ljava/nio/ByteBuffer;I)I
.end method

.method private native nativeEncryptionPipelineEnqueue(Ljava/nio/ByteBuffer;II)I
.end method

.method private native nativeHandshake()I
.end method

.method private native nativeHandshakeDataDequeue(Ljava/nio/ByteBuffer;I)V
.end method

.method private native nativeHandshakeDataEnqueue(Ljava/nio/ByteBuffer;II)V
.end method

.method private native nativeInit(Ljava/lang/String;Ljava/lang/String;[B)Z
.end method


# virtual methods
.method public final a(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 2

    .prologue
    .line 531
    if-eqz p1, :cond_0

    .line 532
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v1

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeHandshakeDataEnqueue(Ljava/nio/ByteBuffer;II)V

    .line 534
    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeHandshake()I

    move-result v1

    .line 535
    if-gtz v1, :cond_1

    .line 536
    const/4 v0, 0x0

    .line 541
    :goto_0
    return-object v0

    .line 538
    :cond_1
    invoke-static {v1}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 539
    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeHandshakeDataDequeue(Ljava/nio/ByteBuffer;I)V

    .line 540
    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    goto :goto_0
.end method

.method public final a(Ljava/nio/ByteBuffer;II)Ljava/nio/ByteBuffer;
    .locals 3

    .prologue
    .line 549
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeEncryptionPipelineEnqueue(Ljava/nio/ByteBuffer;II)I

    move-result v0

    .line 550
    invoke-static {v0}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v1

    .line 551
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeEncryptionPipelineDequeue(Ljava/nio/ByteBuffer;I)I

    move-result v0

    .line 552
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 553
    invoke-virtual {v1, v0}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 554
    return-object v1
.end method

.method public final a()Z
    .locals 3

    .prologue
    .line 527
    const-string v0, "-----BEGIN CERTIFICATE-----\nMIIDiTCCAnGgAwIBAgIJAMFO56WkVE1CMA0GCSqGSIb3DQEBBQUAMFsxCzAJBgNV\nBAYTAlVTMRMwEQYDVQQIDApDYWxpZm9ybmlhMRYwFAYDVQQHDA1Nb3VudGFpbiBW\naWV3MR8wHQYDVQQKDBZHb29nbGUgQXV0b21vdGl2ZSBMaW5rMB4XDTE0MDYwNjE4\nMjgxOVoXDTQ0MDYwNTE4MjgxOVowWzELMAkGA1UEBhMCVVMxEzARBgNVBAgMCkNh\nbGlmb3JuaWExFjAUBgNVBAcMDU1vdW50YWluIFZpZXcxHzAdBgNVBAoMFkdvb2ds\nZSBBdXRvbW90aXZlIExpbmswggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIB\nAQDUH+iIbwwVb74NdI5eBv/ACFmh4ml/NOW7gUVWdYX50n8uQQsHHLCNIhk5VV2H\nhanvAZ/XXHPuVAPadE2HpnNqePKF/RDo4eJo/+rOief8gBYq/Z+OQTZeLdNm+GoI\nHBrEjU4Ms8IdLuFW0jF8LlIRgekjLHpc7duUl3QpwBlmAWQK40T/SZjprlmhyqfJ\ng1rxFdnGbrSibmCsTmb3m6WZyZUyrcwmd7t6q3pHbMABO+o02asPG/YPj/SJo4+i\nfb5/Nk56f3hH9pBiPKQXJnVUdVLKMXSRgydDBsGSBol4C0JL77MNDrMR5jdafJ4j\nmWmsa2+mnzoAv9AxEL9T0LiNAgMBAAGjUDBOMB0GA1UdDgQWBBS5dqvv8DPQiwrM\nfgn8xKR91k7wgjAfBgNVHSMEGDAWgBS5dqvv8DPQiwrMfgn8xKR91k7wgjAMBgNV\nHRMEBTADAQH/MA0GCSqGSIb3DQEBBQUAA4IBAQDKcnBsrbB0Jbz2VGJKP2lwYB6P\ndCTCCpQu7dVp61UQOX+zWfd2hnNMnLs/r1xPO+eyN0vmw7sD05phaIhbXVauKWZi\n9WqWHTaR+9s6CTyBOc1Mye0DMj+4vHt+WLmf0lYjkYUVYvR1EImX8ktXzkVmOqn+\ne30siqlZ8pQpsOgegIKfJ+pNQM8c3eXVv3KFMUgjZW33SziZL8IMsLvSO+1LtH37\nKqbTEMP6XUwVuZopgGvaHU74eT/WSRGlL7vX4OL5/UXXP4qsGH2Zp7uQlErv4H9j\nkMs37UL1vGb4M8RM7Eyu9/RulepSmqZUF+3i+3eby8iGq/3OWk9wgJf7AXnx\n-----END CERTIFICATE-----\n"

    const-string v1, "-----BEGIN CERTIFICATE-----\nMIIDKzCCAhMCAQcwDQYJKoZIhvcNAQELBQAwWzELMAkGA1UEBhMCVVMxEzARBgNV\nBAgMCkNhbGlmb3JuaWExFjAUBgNVBAcMDU1vdW50YWluIFZpZXcxHzAdBgNVBAoM\nFkdvb2dsZSBBdXRvbW90aXZlIExpbmswJhcRMTQwNzA0MDAwMDAwLTA3MDAXETE1\nMDYwNDE3MzQ1NC0wNzAwMFQxCzAJBgNVBAYTAlVTMQswCQYDVQQIDAJDQTEWMBQG\nA1UEBwwNTW91bnRhaW4gVmlldzETMBEGA1UECgwKQ2FyU2VydmljZTELMAkGA1UE\nCwwCMDIwggEiMA0GCSqGSIb3DQEBAQUAA4IBDwAwggEKAoIBAQDHp1DVA/QikpFl\neXOmvHJWoMSF8+x4CX8KeM9ukM5fR08ZMu3Z1yluxrq5dqBYGslOcUkah4rh7i/q\nxw/NF7uNEaGqCJfcmxl78U1BJvjqtqaIzOsSQC91WHwGK6bPv+EIDeExy7uFeBnN\nXuU6nWfhdbCk1J0hY5lnuoCM7u+cG2iX4ymGcUbNpJC57svw48WH3eZL5P73B0Dc\n+yL9MxNjbIwK1XbjQQ+bZ7R9N/ZzITcF2zNpzzT1csBD43II+QfZ7gON08ECTsCt\nfxQ3hpjSTNIXXjOyOTHsiYG8+d0SFGZD2Argga5UrPcsfXks6CNn3xqM9fNDGfpd\n+IIjn1u3AgMBAAEwDQYJKoZIhvcNAQELBQADggEBAIN5sC4QAGkaHEFhhKcadggl\nXxo7+aeNafghCeifMumgfnxLWMeqn+PWTYiuusDpNOWXw+M8cyt8shy6LmDAbcrY\ns9s88UDVWbUuQkTkoxgPbaIuExUCFBFqealF7z7FoLImMQxG2aZjqkLVKu0C0JLD\n3iFl3Yiogr33krGoEjw+Ryr3E1rQZLlpWTkhjhYvWG/GKl480uSYOKzqd2fFtqR6\njok/QmZ1qtKnRT0w7UQ9QJnqWLc+mIMKDPEPQI5nxEbZcj8975PxVOHTw1fv/eng\n7xu1a0U6yL1PMrEdfEYE5b40BglimOIzFV+3D49OEhhNOKN+uNIkyysIf2T2Fw4=\n-----END CERTIFICATE-----\n"

    sget-object v2, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->a:[B

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeInit(Ljava/lang/String;Ljava/lang/String;[B)Z

    move-result v0

    return v0
.end method

.method public final b()I
    .locals 1

    .prologue
    .line 545
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->mNativeAuthState:I

    return v0
.end method

.method public final b(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 558
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->remaining()I

    move-result v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeDecryptionPipelineEnqueue(Ljava/nio/ByteBuffer;I)I

    move-result v0

    .line 559
    if-gtz v0, :cond_0

    .line 560
    const-string v0, "CAR.SECURITY"

    const-string v1, "Decryption failed!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 561
    const/4 v0, 0x0

    .line 587
    :goto_0
    return-object v0

    :cond_0
    move v0, v1

    move v2, v1

    .line 566
    :goto_1
    invoke-direct {p0}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeDecryptionPipelinePending()I

    move-result v3

    if-lez v3, :cond_1

    .line 567
    invoke-static {v3}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v4

    .line 568
    invoke-direct {p0, v4, v3}, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->nativeDecryptionPipelineDequeue(Ljava/nio/ByteBuffer;I)I

    move-result v3

    .line 569
    invoke-virtual {v4, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 570
    invoke-virtual {v4, v3}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    .line 571
    add-int/2addr v3, v2

    .line 572
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b:[Ljava/nio/ByteBuffer;

    add-int/lit8 v2, v0, 0x1

    aput-object v4, v5, v0

    move v0, v2

    move v2, v3

    .line 573
    goto :goto_1

    .line 574
    :cond_1
    const/4 v3, 0x1

    if-ne v0, v3, :cond_2

    .line 577
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b:[Ljava/nio/ByteBuffer;

    aget-object v0, v0, v1

    goto :goto_0

    .line 579
    :cond_2
    invoke-static {v2}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v3

    move v4, v1

    .line 580
    :goto_2
    if-ge v4, v0, :cond_3

    .line 581
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b:[Ljava/nio/ByteBuffer;

    aget-object v5, v5, v4

    invoke-virtual {v3, v5}, Ljava/nio/ByteBuffer;->put(Ljava/nio/ByteBuffer;)Ljava/nio/ByteBuffer;

    .line 582
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/SslWrapper;->b:[Ljava/nio/ByteBuffer;

    aget-object v5, v5, v4

    invoke-static {v5}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 580
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 584
    :cond_3
    invoke-virtual {v3, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 585
    invoke-virtual {v3, v2}, Ljava/nio/ByteBuffer;->limit(I)Ljava/nio/Buffer;

    move-object v0, v3

    goto :goto_0
.end method
