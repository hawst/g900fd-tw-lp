.class public final Lcom/google/android/gms/car/oc;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/l;


# instance fields
.field a:F

.field final b:Lcom/google/android/gms/car/k;

.field c:Lcom/google/android/gms/car/ny;

.field volatile d:Z

.field volatile e:I

.field private f:Landroid/os/PowerManager;

.field private g:Ljava/lang/reflect/Method;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/high16 v0, 0x42380000    # 46.0f

    iput v0, p0, Lcom/google/android/gms/car/oc;->a:F

    .line 36
    iput-boolean v1, p0, Lcom/google/android/gms/car/oc;->d:Z

    .line 45
    iput v1, p0, Lcom/google/android/gms/car/oc;->e:I

    .line 48
    new-instance v0, Lcom/google/android/gms/car/k;

    invoke-direct {v0, p1, p0}, Lcom/google/android/gms/car/k;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/l;)V

    iput-object v0, p0, Lcom/google/android/gms/car/oc;->b:Lcom/google/android/gms/car/k;

    .line 49
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    iput-object v0, p0, Lcom/google/android/gms/car/oc;->f:Landroid/os/PowerManager;

    .line 51
    invoke-static {}, Lcom/google/android/gms/car/ie;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 53
    :try_start_0
    const-class v0, Landroid/os/PowerManager;

    const-string v1, "userActivity"

    const/4 v2, 0x3

    new-array v2, v2, [Ljava/lang/Class;

    const/4 v3, 0x0

    sget-object v4, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x1

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    sget-object v4, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v4, v2, v3

    invoke-virtual {v0, v1, v2}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/oc;->g:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :cond_0
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    const-string v0, "CAR.POWER"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    const-string v0, "CAR.POWER"

    const-string v1, "PowerManager.userActivity not found"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 59
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/oc;->e:I

    goto :goto_0
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 73
    const-string v0, "CAR.POWER"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const-string v0, "CAR.POWER"

    const-string v1, "stopping power management"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/oc;->b:Lcom/google/android/gms/car/k;

    invoke-virtual {v0}, Lcom/google/android/gms/car/k;->b()V

    .line 78
    return-void
.end method

.method public final a(F)V
    .locals 8

    .prologue
    const/high16 v4, 0x42340000    # 45.0f

    const/4 v7, 0x3

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 86
    .line 89
    cmpl-float v0, p1, v4

    if-ltz v0, :cond_8

    move v0, v1

    .line 93
    :goto_0
    iget v3, p0, Lcom/google/android/gms/car/oc;->a:F

    cmpl-float v3, v3, v4

    if-ltz v3, :cond_7

    move v3, v1

    .line 97
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/gms/car/oc;->d:Z

    if-nez v4, :cond_6

    if-eqz v0, :cond_6

    move v4, v1

    move v5, v1

    .line 102
    :goto_2
    iget-boolean v6, p0, Lcom/google/android/gms/car/oc;->d:Z

    if-eqz v6, :cond_0

    if-nez v0, :cond_0

    if-nez v3, :cond_0

    move v4, v2

    move v5, v1

    .line 106
    :cond_0
    const-string v0, "CAR.POWER"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 107
    const-string v0, "CAR.POWER"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "mode changed:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " powerSaving:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/google/android/gms/car/oc;->d:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " current:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " previous:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/oc;->a:F

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(F)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_1
    iput p1, p0, Lcom/google/android/gms/car/oc;->a:F

    .line 112
    if-eqz v5, :cond_3

    .line 114
    iput-boolean v4, p0, Lcom/google/android/gms/car/oc;->d:Z

    .line 115
    iget-object v1, p0, Lcom/google/android/gms/car/oc;->c:Lcom/google/android/gms/car/ny;

    .line 116
    if-eqz v1, :cond_5

    .line 117
    const-string v0, "CAR.POWER"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 118
    const-string v2, "CAR.POWER"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v0, "mode change,"

    invoke-direct {v3, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    if-eqz v4, :cond_4

    const-string v0, "enter"

    :goto_3
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, " power saving"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v2, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 121
    :cond_2
    invoke-interface {v1, v4}, Lcom/google/android/gms/car/ny;->a(Z)V

    .line 126
    :cond_3
    :goto_4
    return-void

    .line 118
    :cond_4
    const-string v0, "exit"

    goto :goto_3

    .line 122
    :cond_5
    const-string v0, "CAR.POWER"

    invoke-static {v0, v7}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 123
    const-string v0, "CAR.POWER"

    const-string v1, "power controller not available"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_4

    :cond_6
    move v4, v2

    move v5, v2

    goto/16 :goto_2

    :cond_7
    move v3, v2

    goto/16 :goto_1

    :cond_8
    move v0, v2

    goto/16 :goto_0
.end method

.method final a(I)V
    .locals 6

    .prologue
    .line 142
    iget-object v0, p0, Lcom/google/android/gms/car/oc;->g:Ljava/lang/reflect/Method;

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/gms/car/oc;->e:I

    if-eqz v0, :cond_1

    .line 154
    :cond_0
    :goto_0
    return-void

    .line 145
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    .line 147
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/oc;->g:Ljava/lang/reflect/Method;

    iget-object v3, p0, Lcom/google/android/gms/car/oc;->f:Landroid/os/PowerManager;

    const/4 v4, 0x3

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    aput-object v0, v4, v5

    const/4 v0, 0x1

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    const/4 v0, 0x2

    const/4 v1, 0x2

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v4, v0

    invoke-virtual {v2, v3, v4}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_2
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 150
    :catch_0
    move-exception v0

    :goto_1
    const/4 v0, -0x2

    iput v0, p0, Lcom/google/android/gms/car/oc;->e:I

    goto :goto_0

    .line 152
    :catch_1
    move-exception v0

    const/4 v0, -0x3

    iput v0, p0, Lcom/google/android/gms/car/oc;->e:I

    goto :goto_0

    .line 150
    :catch_2
    move-exception v0

    goto :goto_1

    :catch_3
    move-exception v0

    goto :goto_1
.end method
