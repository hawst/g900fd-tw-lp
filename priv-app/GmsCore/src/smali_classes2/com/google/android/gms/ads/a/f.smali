.class public final Lcom/google/android/gms/ads/a/f;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 79
    const-string v0, "gms:ads:social:doritos:doritos_url"

    const-string v1, "https://googleads.g.doubleclick.net/pagead/drt/m"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->a:Lcom/google/android/gms/common/a/d;

    .line 87
    const-string v0, "gms:ads:social:doritos:min_time_between_requests_ms"

    const/16 v1, 0x7d0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->b:Lcom/google/android/gms/common/a/d;

    .line 93
    const-string v0, "gms:ads:social:doritos:doritos_refresh_period_s"

    const v1, 0xd2f0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->c:Lcom/google/android/gms/common/a/d;

    .line 100
    const-string v0, "gms:ads:social:doritos:doritos_refresh_flex_s"

    const/16 v1, 0x2a30

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->d:Lcom/google/android/gms/common/a/d;

    .line 107
    const-string v0, "gms:ads:social:doritos:doritos_fast_refresh_flex_s"

    const/16 v1, 0x1e

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->e:Lcom/google/android/gms/common/a/d;

    .line 113
    const-string v0, "gms:ads:social:doritos:doritos_oauth_scope"

    const-string v1, "oauth2:https://www.googleapis.com/auth/emeraldsea.mobileapps.doritos.cookie"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/ads/a/f;->f:Lcom/google/android/gms/common/a/d;

    return-void
.end method
