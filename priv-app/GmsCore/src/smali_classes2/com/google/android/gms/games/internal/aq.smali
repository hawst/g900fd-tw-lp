.class final Lcom/google/android/gms/games/internal/aq;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/request/e;


# instance fields
.field private final a:Lcom/google/android/gms/common/api/Status;

.field private final b:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/Status;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 2872
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 2873
    iput-object p1, p0, Lcom/google/android/gms/games/internal/aq;->a:Lcom/google/android/gms/common/api/Status;

    .line 2874
    iput-object p2, p0, Lcom/google/android/gms/games/internal/aq;->b:Landroid/os/Bundle;

    .line 2875
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 2879
    iget-object v0, p0, Lcom/google/android/gms/games/internal/aq;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a(I)Lcom/google/android/gms/games/request/a;
    .locals 2

    .prologue
    .line 2884
    invoke-static {p1}, Lcom/google/android/gms/games/internal/b/g;->a(I)Ljava/lang/String;

    move-result-object v0

    .line 2885
    iget-object v1, p0, Lcom/google/android/gms/games/internal/aq;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 2886
    const/4 v0, 0x0

    .line 2889
    :goto_0
    return-object v0

    .line 2888
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/internal/aq;->b:Landroid/os/Bundle;

    invoke-virtual {v1, v0}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 2889
    new-instance v1, Lcom/google/android/gms/games/request/a;

    invoke-direct {v1, v0}, Lcom/google/android/gms/games/request/a;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    move-object v0, v1

    goto :goto_0
.end method

.method public final w_()V
    .locals 3

    .prologue
    .line 2894
    iget-object v0, p0, Lcom/google/android/gms/games/internal/aq;->b:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 2895
    iget-object v2, p0, Lcom/google/android/gms/games/internal/aq;->b:Landroid/os/Bundle;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 2896
    if-eqz v0, :cond_0

    .line 2897
    invoke-virtual {v0}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    .line 2900
    :cond_1
    return-void
.end method
