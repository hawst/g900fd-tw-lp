.class public final Lcom/google/android/gms/people/x;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field static final b:Lcom/google/android/gms/common/api/i;

.field public static final c:Lcom/google/android/gms/common/api/c;

.field public static final d:Lcom/google/android/gms/people/identity/a;

.field public static final e:Lcom/google/android/gms/people/c;

.field public static final f:Lcom/google/android/gms/people/j;

.field public static final g:Lcom/google/android/gms/people/o;

.field public static final h:Lcom/google/android/gms/people/ai;

.field public static final i:Lcom/google/android/gms/people/a;

.field public static final j:Lcom/google/android/gms/people/r;

.field public static final k:Lcom/google/android/gms/people/s;

.field public static final l:Lcom/google/android/gms/people/b;

.field public static final m:Lcom/google/android/gms/people/v;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 110
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->a:Lcom/google/android/gms/common/api/j;

    .line 116
    new-instance v0, Lcom/google/android/gms/people/y;

    invoke-direct {v0}, Lcom/google/android/gms/people/y;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->b:Lcom/google/android/gms/common/api/i;

    .line 141
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/people/x;->b:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/people/x;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/people/x;->c:Lcom/google/android/gms/common/api/c;

    .line 149
    new-instance v0, Lcom/google/android/gms/people/identity/internal/v;

    invoke-direct {v0}, Lcom/google/android/gms/people/identity/internal/v;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->d:Lcom/google/android/gms/people/identity/a;

    .line 157
    new-instance v0, Lcom/google/android/gms/people/internal/b/c;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/c;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->e:Lcom/google/android/gms/people/c;

    .line 165
    new-instance v0, Lcom/google/android/gms/people/internal/b/l;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/l;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->f:Lcom/google/android/gms/people/j;

    .line 171
    new-instance v0, Lcom/google/android/gms/people/internal/b/w;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/w;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    .line 177
    new-instance v0, Lcom/google/android/gms/people/internal/b/an;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/an;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->h:Lcom/google/android/gms/people/ai;

    .line 183
    new-instance v0, Lcom/google/android/gms/people/internal/b/a;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->i:Lcom/google/android/gms/people/a;

    .line 189
    new-instance v0, Lcom/google/android/gms/people/internal/b/ae;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/ae;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->j:Lcom/google/android/gms/people/r;

    .line 194
    new-instance v0, Lcom/google/android/gms/people/internal/b/af;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/af;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->k:Lcom/google/android/gms/people/s;

    .line 202
    new-instance v0, Lcom/google/android/gms/people/internal/b/b;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->l:Lcom/google/android/gms/people/b;

    .line 208
    new-instance v0, Lcom/google/android/gms/people/internal/b/ak;

    invoke-direct {v0}, Lcom/google/android/gms/people/internal/b/ak;-><init>()V

    sput-object v0, Lcom/google/android/gms/people/x;->m:Lcom/google/android/gms/people/v;

    return-void
.end method
