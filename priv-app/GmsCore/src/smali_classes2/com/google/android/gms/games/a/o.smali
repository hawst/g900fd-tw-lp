.class public final Lcom/google/android/gms/games/a/o;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Lcom/google/android/gms/common/e/b;

.field b:[Ljava/lang/String;

.field c:Ljava/lang/String;

.field d:I

.field private final e:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 1277
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1278
    iput-object p1, p0, Lcom/google/android/gms/games/a/o;->e:Landroid/content/Context;

    .line 1279
    iput-object v0, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    .line 1280
    iput-object v0, p0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    .line 1281
    iput-object v0, p0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    .line 1282
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/games/a/o;->d:I

    .line 1283
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/a/au;)V
    .locals 1

    .prologue
    .line 1286
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/a/o;-><init>(Landroid/content/Context;)V

    .line 1287
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/data/DataHolder;
    .locals 4

    .prologue
    .line 1327
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/o;->b()Landroid/database/AbstractWindowedCursor;

    move-result-object v0

    .line 1328
    new-instance v1, Lcom/google/android/gms/common/data/DataHolder;

    iget v2, p0, Lcom/google/android/gms/games/a/o;->d:I

    invoke-static {v0, v2}, Lcom/google/android/gms/games/a/l;->a(Landroid/database/Cursor;I)I

    move-result v2

    const/4 v3, 0x0

    invoke-direct {v1, v0, v2, v3}, Lcom/google/android/gms/common/data/DataHolder;-><init>(Landroid/database/AbstractWindowedCursor;ILandroid/os/Bundle;)V

    return-object v1
.end method

.method public final a(Landroid/net/Uri;)Lcom/google/android/gms/games/a/o;
    .locals 1

    .prologue
    .line 1295
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    .line 1296
    return-object p0
.end method

.method public final a(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/games/a/o;
    .locals 1

    .prologue
    .line 1300
    new-instance v0, Lcom/google/android/gms/common/e/b;

    invoke-direct {v0, p1, p2, p3}, Lcom/google/android/gms/common/e/b;-><init>(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    .line 1301
    return-object p0
.end method

.method public final b()Landroid/database/AbstractWindowedCursor;
    .locals 6

    .prologue
    .line 1338
    iget-object v0, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    if-nez v0, :cond_0

    .line 1339
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "You must call setQuerySpec before querying."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1342
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/o;->e:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.background"

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->acquireContentProviderClient(Ljava/lang/String;)Landroid/content/ContentProviderClient;

    move-result-object v0

    .line 1345
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    iget-object v1, v1, Lcom/google/android/gms/common/e/b;->a:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/gms/games/a/o;->b:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    invoke-virtual {v3}, Lcom/google/android/gms/common/e/b;->a()Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/games/a/o;->a:Lcom/google/android/gms/common/e/b;

    iget-object v4, v4, Lcom/google/android/gms/common/e/b;->c:[Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/a/o;->c:Ljava/lang/String;

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentProviderClient;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    check-cast v1, Landroid/database/AbstractWindowedCursor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1351
    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    return-object v1

    .line 1347
    :catch_0
    move-exception v1

    .line 1349
    :try_start_1
    new-instance v2, Ljava/lang/RuntimeException;

    invoke-direct {v2, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/Throwable;)V

    throw v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 1351
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/ContentProviderClient;->release()Z

    throw v1
.end method
