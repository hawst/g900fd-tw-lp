.class public final Lcom/google/android/gms/car/fo;
.super Lcom/google/android/gms/car/lx;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/t;


# static fields
.field private static final a:[[I


# instance fields
.field private final b:Lcom/google/android/gms/car/gx;

.field private c:Lcom/google/android/gms/car/senderprotocol/q;

.field private final d:Ljava/lang/Object;

.field private final e:[Lcom/google/android/gms/car/fq;

.field private final f:Ljava/util/LinkedList;

.field private final g:Ljava/util/HashMap;

.field private h:Z

.field private i:Z

.field private final j:Landroid/media/AudioManager;

.field private final k:Landroid/media/AudioManager$OnAudioFocusChangeListener;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    const/4 v0, 0x2

    new-array v0, v0, [[I

    new-array v1, v4, [I

    const v2, 0x20001

    aput v2, v1, v3

    aput-object v1, v0, v3

    new-array v1, v4, [I

    const v2, 0x50001

    aput v2, v1, v3

    aput-object v1, v0, v4

    sput-object v0, Lcom/google/android/gms/car/fo;->a:[[I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 78
    invoke-direct {p0}, Lcom/google/android/gms/car/lx;-><init>()V

    .line 44
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    .line 45
    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/gms/car/fq;

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    .line 48
    new-instance v0, Ljava/util/LinkedList;

    invoke-direct {v0}, Ljava/util/LinkedList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    .line 54
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->g:Ljava/util/HashMap;

    .line 58
    iput-boolean v1, p0, Lcom/google/android/gms/car/fo;->h:Z

    .line 61
    iput-boolean v1, p0, Lcom/google/android/gms/car/fo;->i:Z

    .line 70
    new-instance v0, Lcom/google/android/gms/car/fp;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/fp;-><init>(Lcom/google/android/gms/car/fo;)V

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    .line 80
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    iput-object v0, p0, Lcom/google/android/gms/car/fo;->j:Landroid/media/AudioManager;

    .line 82
    return-void
.end method

.method private static a(II)J
    .locals 4

    .prologue
    .line 188
    int-to-long v0, p0

    const/16 v2, 0x20

    shl-long/2addr v0, v2

    int-to-long v2, p1

    or-long/2addr v0, v2

    return-wide v0
.end method

.method private static a(I)V
    .locals 3

    .prologue
    .line 320
    if-ltz p0, :cond_0

    sget-object v0, Lcom/google/android/gms/car/fo;->a:[[I

    array-length v0, v0

    if-lt p0, v0, :cond_1

    .line 321
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong category "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 323
    :cond_1
    return-void
.end method

.method private a(III)V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 267
    const-string v0, "CAR.MSG"

    invoke-static {v0, v5}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    const-string v0, "CAR.MSG"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send message to car, category:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_0
    packed-switch p1, :pswitch_data_0

    .line 317
    :cond_1
    :goto_0
    return-void

    .line 273
    :pswitch_0
    if-nez p2, :cond_1

    .line 274
    if-ne p3, v3, :cond_2

    .line 275
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->c:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/q;->e()V

    .line 276
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 277
    const/4 v0, 0x1

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/car/fo;->h:Z

    .line 278
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 279
    :cond_2
    if-nez p3, :cond_3

    .line 280
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->c:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/q;->f()V

    .line 281
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 282
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/car/fo;->h:Z

    .line 283
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_0

    :catchall_1
    move-exception v0

    monitor-exit v1

    throw v0

    .line 285
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown value, category:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :pswitch_1
    if-nez p2, :cond_1

    .line 292
    if-eq p3, v3, :cond_4

    if-eq p3, v4, :cond_4

    .line 294
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown value, category:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " key:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " value:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 297
    :cond_4
    if-ne p3, v3, :cond_5

    .line 298
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 299
    const/4 v0, 0x1

    :try_start_2
    iput-boolean v0, p0, Lcom/google/android/gms/car/fo;->i:Z

    .line 300
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_2

    .line 303
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->j:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/fo;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1, v5, v4}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 310
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->c:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0, p3}, Lcom/google/android/gms/car/senderprotocol/q;->c(I)V

    .line 311
    if-ne p3, v4, :cond_1

    .line 312
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->j:Landroid/media/AudioManager;

    iget-object v1, p0, Lcom/google/android/gms/car/fo;->k:Landroid/media/AudioManager$OnAudioFocusChangeListener;

    invoke-virtual {v0, v1}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    goto/16 :goto_0

    .line 300
    :catchall_2
    move-exception v0

    monitor-exit v1

    throw v0

    .line 306
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 307
    const/4 v0, 0x0

    :try_start_3
    iput-boolean v0, p0, Lcom/google/android/gms/car/fo;->i:Z

    .line 308
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_3

    goto :goto_1

    :catchall_3
    move-exception v0

    monitor-exit v1

    throw v0

    .line 271
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method static synthetic a(Lcom/google/android/gms/car/fo;Lcom/google/android/gms/car/fq;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->a(Lcom/google/android/gms/car/fq;)V

    return-void
.end method

.method private a(Lcom/google/android/gms/car/fq;)V
    .locals 4

    .prologue
    .line 405
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 406
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    invoke-virtual {v0, p1}, Ljava/util/LinkedList;->remove(Ljava/lang/Object;)Z

    .line 407
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 408
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v2, v2, v0

    if-ne v2, p1, :cond_0

    .line 409
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 410
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/fo;->b(I)V

    .line 407
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 413
    :cond_1
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 414
    invoke-virtual {p1}, Lcom/google/android/gms/car/fq;->b()V

    .line 415
    return-void

    .line 413
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private b(II)I
    .locals 3

    .prologue
    .line 331
    invoke-static {p1}, Lcom/google/android/gms/car/fo;->a(I)V

    sget-object v0, Lcom/google/android/gms/car/fo;->a:[[I

    aget-object v0, v0, p1

    .line 332
    if-ltz p2, :cond_0

    array-length v1, v0

    if-lt p2, v1, :cond_1

    .line 333
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "wrong key "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " for given category "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 336
    :cond_1
    aget v0, v0, p2

    return v0
.end method

.method private b(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 427
    packed-switch p1, :pswitch_data_0

    .line 443
    :cond_0
    :goto_0
    return-void

    .line 429
    :pswitch_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/fo;->h:Z

    if-eqz v0, :cond_0

    .line 430
    iput-boolean v1, p0, Lcom/google/android/gms/car/fo;->h:Z

    .line 431
    invoke-direct {p0, p1, v1, v1}, Lcom/google/android/gms/car/fo;->a(III)V

    goto :goto_0

    .line 436
    :pswitch_1
    iget-boolean v0, p0, Lcom/google/android/gms/car/fo;->i:Z

    if-eqz v0, :cond_0

    .line 437
    iput-boolean v1, p0, Lcom/google/android/gms/car/fo;->i:Z

    .line 438
    const/4 v0, 0x2

    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/gms/car/fo;->a(III)V

    goto :goto_0

    .line 427
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method private c(Lcom/google/android/gms/car/lz;I)V
    .locals 3

    .prologue
    .line 340
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 341
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->d(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v0

    .line 342
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v2, v2, p2

    .line 343
    if-nez v2, :cond_0

    .line 344
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "not owned"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 346
    :cond_0
    if-eq v2, v0, :cond_1

    .line 347
    :try_start_1
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "not owned"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 349
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method private d(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;
    .locals 2

    .prologue
    .line 353
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->e(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v0

    .line 354
    if-nez v0, :cond_0

    .line 355
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "not registered"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 357
    :cond_0
    return-object v0
.end method

.method private e(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;
    .locals 5

    .prologue
    .line 361
    invoke-interface {p1}, Lcom/google/android/gms/car/lz;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    .line 362
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 363
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fq;

    .line 364
    invoke-virtual {v0}, Lcom/google/android/gms/car/fq;->a()Lcom/google/android/gms/car/lz;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/gms/car/lz;->asBinder()Landroid/os/IBinder;

    move-result-object v4

    if-ne v4, v1, :cond_0

    .line 365
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 369
    :goto_0
    return-object v0

    .line 368
    :cond_1
    monitor-exit v2

    .line 369
    const/4 v0, 0x0

    goto :goto_0

    .line 368
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 245
    iput-object v0, p0, Lcom/google/android/gms/car/fo;->c:Lcom/google/android/gms/car/senderprotocol/q;

    .line 246
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 247
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/fq;

    .line 248
    invoke-virtual {v0}, Lcom/google/android/gms/car/fq;->b()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 254
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 250
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    invoke-virtual {v0}, Ljava/util/LinkedList;->clear()V

    .line 251
    const/4 v0, 0x0

    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    array-length v2, v2

    if-ge v0, v2, :cond_1

    .line 252
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    const/4 v3, 0x0

    aput-object v3, v2, v0

    .line 251
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 254
    :cond_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    return-void
.end method

.method public final a(Lcom/google/android/gms/car/lz;)V
    .locals 4

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->u()V

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    iget-object v0, v0, Lcom/google/android/gms/car/gx;->a:Lcom/google/android/gms/car/CarService;

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/car/of;->a(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 89
    new-instance v0, Ljava/lang/SecurityException;

    const-string v1, "wrong signature"

    invoke-direct {v0, v1}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 91
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 92
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->e(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v0

    .line 93
    if-eqz v0, :cond_1

    .line 94
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v2, "already registered"

    invoke-direct {v0, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 105
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 96
    :cond_1
    :try_start_1
    new-instance v0, Lcom/google/android/gms/car/fq;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/car/fq;-><init>(Lcom/google/android/gms/car/fo;Lcom/google/android/gms/car/lz;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 98
    :try_start_2
    invoke-interface {p1}, Lcom/google/android/gms/car/lz;->asBinder()Landroid/os/IBinder;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 103
    :try_start_3
    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/fq;->a(I)V

    .line 104
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->f:Ljava/util/LinkedList;

    invoke-virtual {v2, v0}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    .line 105
    monitor-exit v1

    :goto_0
    return-void

    .line 100
    :catch_0
    move-exception v0

    const-string v0, "CAR.MSG"

    const-string v2, "client already dead?"

    invoke-static {v0, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 101
    monitor-exit v1
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/lz;III)V
    .locals 4

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->u()V

    .line 148
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/car/fo;->b(II)I

    move-result v0

    .line 149
    const/high16 v1, 0x60000

    and-int/2addr v1, v0

    if-nez v1, :cond_0

    .line 151
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not writable, category:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keytype:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 154
    :cond_0
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_1

    .line 155
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not int type, category:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keytype:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 158
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/fo;->c(Lcom/google/android/gms/car/lz;I)V

    .line 159
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/car/fo;->a(III)V

    .line 160
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 219
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/q;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/google/android/gms/car/fo;->c:Lcom/google/android/gms/car/senderprotocol/q;

    .line 241
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 228
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 229
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v1, v1, v0

    if-eqz v1, :cond_0

    .line 230
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "category "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " owner "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/google/android/gms/car/fq;->a()Lcom/google/android/gms/car/lz;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 228
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 234
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "mNavigationFocusSet "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/fo;->h:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mVrFocusSet "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/fo;->i:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 236
    return-void
.end method

.method public final a(Z)V
    .locals 6

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 259
    if-eqz p1, :cond_1

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    iget-object v3, p0, Lcom/google/android/gms/car/fo;->g:Ljava/util/HashMap;

    const/4 v4, 0x1

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/google/android/gms/car/fo;->a(II)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v2, :cond_2

    const-string v1, "CAR.MSG"

    const/4 v2, 0x4

    invoke-static {v1, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "CAR.MSG"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "No receiver for message from car, category:1"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v3, " key:0 value:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 263
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v0, v1

    .line 259
    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_2
    :try_start_1
    const-string v1, "CAR.MSG"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    const-string v1, "CAR.MSG"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Notify message from car, category:1"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v4, " key:0 value:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    invoke-virtual {v2}, Lcom/google/android/gms/car/fq;->a()Lcom/google/android/gms/car/lz;

    move-result-object v1

    const/4 v3, 0x1

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4, v0}, Lcom/google/android/gms/car/lz;->a(III)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    :catch_0
    move-exception v0

    invoke-direct {p0, v2}, Lcom/google/android/gms/car/fo;->a(Lcom/google/android/gms/car/fq;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/lz;I)Z
    .locals 7

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 115
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v2}, Lcom/google/android/gms/car/gx;->u()V

    .line 116
    invoke-static {p2}, Lcom/google/android/gms/car/fo;->a(I)V

    .line 117
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->d(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v2

    .line 121
    iget-object v3, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v3

    .line 122
    :try_start_0
    iget-object v4, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v4, v4, p2

    .line 123
    if-nez v4, :cond_2

    .line 124
    iget-object v5, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aput-object v2, v5, p2

    .line 137
    :cond_0
    :goto_0
    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 138
    if-eqz v0, :cond_1

    .line 139
    :try_start_1
    invoke-virtual {v4}, Lcom/google/android/gms/car/fq;->a()Lcom/google/android/gms/car/lz;

    move-result-object v0

    invoke-interface {v0, p2}, Lcom/google/android/gms/car/lz;->a(I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0

    .line 140
    :goto_1
    invoke-direct {p0, p2}, Lcom/google/android/gms/car/fo;->b(I)V

    .line 142
    :cond_1
    return v1

    .line 126
    :cond_2
    if-eq v4, v2, :cond_0

    .line 127
    :try_start_2
    iget-object v5, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v6

    invoke-virtual {v5}, Lcom/google/android/gms/car/gx;->L()Lcom/google/android/gms/car/ae;

    move-result-object v5

    if-eqz v5, :cond_3

    invoke-virtual {v5, v6}, Lcom/google/android/gms/car/ae;->b(I)Z

    move-result v0

    :cond_3
    if-nez v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-static {}, Landroid/os/Binder;->getCallingPid()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/car/gx;->b(I)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v4}, Lcom/google/android/gms/car/fq;->c()I

    move-result v5

    invoke-virtual {v0, v5}, Lcom/google/android/gms/car/gx;->b(I)Z

    move-result v0

    if-nez v0, :cond_5

    .line 131
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aput-object v2, v0, p2

    move v0, v1

    .line 132
    goto :goto_0

    .line 135
    :cond_5
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "already taken"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 137
    :catchall_0
    move-exception v0

    monitor-exit v3

    throw v0

    .line 139
    :catch_0
    move-exception v0

    invoke-direct {p0, v4}, Lcom/google/android/gms/car/fo;->a(Lcom/google/android/gms/car/fq;)V

    goto :goto_1
.end method

.method public final a(Lcom/google/android/gms/car/lz;II)[I
    .locals 4

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->u()V

    .line 165
    invoke-direct {p0, p2, p3}, Lcom/google/android/gms/car/fo;->b(II)I

    move-result v0

    .line 166
    const/high16 v1, 0x10000

    and-int/2addr v1, v0

    if-nez v1, :cond_0

    .line 167
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not readable, category:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keytype:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 170
    :cond_0
    and-int/lit8 v1, v0, 0x1

    if-nez v1, :cond_1

    .line 171
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "not int type, category:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " key:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " keytype:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 174
    :cond_1
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/fo;->c(Lcom/google/android/gms/car/lz;I)V

    .line 175
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 177
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->g:Ljava/util/HashMap;

    invoke-static {p2, p3}, Lcom/google/android/gms/car/fo;->a(II)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 178
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 179
    if-eqz v0, :cond_2

    .line 180
    const/4 v1, 0x1

    new-array v1, v1, [I

    .line 181
    const/4 v2, 0x0

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v1, v2

    move-object v0, v1

    .line 184
    :goto_0
    return-object v0

    .line 178
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 184
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/car/lz;)V
    .locals 2

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->e(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-direct {p0, v0}, Lcom/google/android/gms/car/fo;->a(Lcom/google/android/gms/car/fq;)V

    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/lz;I)V
    .locals 3

    .prologue
    .line 193
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->u()V

    .line 194
    invoke-static {p2}, Lcom/google/android/gms/car/fo;->a(I)V

    .line 195
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->d(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v0

    .line 196
    iget-object v1, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 197
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v2, v2, p2

    if-ne v2, v0, :cond_0

    .line 198
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    const/4 v2, 0x0

    aput-object v2, v0, p2

    .line 200
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final c(Lcom/google/android/gms/car/lz;)V
    .locals 5

    .prologue
    .line 205
    iget-object v0, p0, Lcom/google/android/gms/car/fo;->b:Lcom/google/android/gms/car/gx;

    invoke-virtual {v0}, Lcom/google/android/gms/car/gx;->u()V

    .line 206
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fo;->d(Lcom/google/android/gms/car/lz;)Lcom/google/android/gms/car/fq;

    move-result-object v1

    .line 207
    iget-object v2, p0, Lcom/google/android/gms/car/fo;->d:Ljava/lang/Object;

    monitor-enter v2

    .line 208
    const/4 v0, 0x0

    :goto_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    array-length v3, v3

    if-ge v0, v3, :cond_1

    .line 209
    iget-object v3, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    aget-object v3, v3, v0

    if-ne v3, v1, :cond_0

    .line 210
    iget-object v3, p0, Lcom/google/android/gms/car/fo;->e:[Lcom/google/android/gms/car/fq;

    const/4 v4, 0x0

    aput-object v4, v3, v0

    .line 208
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 213
    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method
