.class final Lcom/google/android/gms/car/a/s;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/a/k;)V
    .locals 0

    .prologue
    .line 635
    iput-object p1, p0, Lcom/google/android/gms/car/a/s;->a:Lcom/google/android/gms/car/a/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/a/k;B)V
    .locals 0

    .prologue
    .line 635
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/s;-><init>(Lcom/google/android/gms/car/a/k;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5

    .prologue
    const/4 v4, 0x3

    const/high16 v3, -0x80000000

    .line 638
    const-string v0, "CAR.BT"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 639
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "PairingRequestReceiver#onReceive: intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 641
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/s;->a:Lcom/google/android/gms/car/a/k;

    iget v0, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 642
    const-string v0, "CAR.BT"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 643
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothUtil not initialized or being cleaned up"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 685
    :cond_1
    :goto_0
    return-void

    .line 647
    :cond_2
    const-string v0, "android.bluetooth.device.action.PAIRING_REQUEST"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 648
    const-string v0, "CAR.BT"

    const-string v1, "PairingRequestReceiver: Wrong intent. This shouldn\'t happen"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 652
    :cond_3
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 653
    iget-object v1, p0, Lcom/google/android/gms/car/a/s;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 654
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Wrong device is being paired: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 658
    :cond_4
    const-string v0, "android.bluetooth.device.extra.PAIRING_VARIANT"

    invoke-virtual {p2, v0, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    .line 660
    if-ne v0, v3, :cond_5

    .line 662
    const-string v0, "CAR.BT"

    const-string v1, "Wrong Bluetooth pairing method"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 666
    :cond_5
    const-string v1, "android.bluetooth.device.extra.PAIRING_KEY"

    invoke-virtual {p2, v1, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    .line 669
    const-string v2, "CAR.BT"

    invoke-static {v2, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 670
    const-string v2, "CAR.BT"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "PairingRequestReceiver#onReceive: pairing method="

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", pairing key="

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 675
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/car/a/s;->a:Lcom/google/android/gms/car/a/k;

    iput v0, v2, Lcom/google/android/gms/car/a/k;->m:I

    .line 676
    iget-object v0, p0, Lcom/google/android/gms/car/a/s;->a:Lcom/google/android/gms/car/a/k;

    iput v1, v0, Lcom/google/android/gms/car/a/k;->n:I

    .line 677
    new-instance v0, Lcom/google/android/gms/car/a/t;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/a/t;-><init>(Lcom/google/android/gms/car/a/s;)V

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-static {v1, v0}, Lcom/google/android/gms/car/of;->a(Landroid/os/Looper;Ljava/lang/Runnable;)V

    .line 684
    invoke-virtual {p0}, Lcom/google/android/gms/car/a/s;->abortBroadcast()V

    goto/16 :goto_0
.end method
