.class final Lcom/google/android/gms/car/a/q;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/k;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/a/k;)V
    .locals 0

    .prologue
    .line 703
    iput-object p1, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/a/k;B)V
    .locals 0

    .prologue
    .line 703
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/a/q;-><init>(Lcom/google/android/gms/car/a/k;)V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8

    .prologue
    const/16 v7, 0xc

    const/16 v6, 0xa

    const/4 v3, 0x3

    .line 706
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 707
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "BondStateChangeReceiver#onReceive: intent="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 709
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    iget v0, v0, Lcom/google/android/gms/car/a/k;->k:I

    if-eqz v0, :cond_2

    .line 710
    const-string v0, "CAR.BT"

    invoke-static {v0, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 711
    const-string v0, "CAR.BT"

    const-string v1, "BluetoothUtil not initialized or being cleaned up"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 749
    :cond_1
    :goto_0
    return-void

    .line 715
    :cond_2
    const-string v0, "android.bluetooth.device.action.BOND_STATE_CHANGED"

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 716
    const-string v0, "CAR.BT"

    const-string v1, "BondStateChangeReceiver: Wrong intent. This shouldn\'t happen"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 720
    :cond_3
    const-string v0, "android.bluetooth.device.extra.DEVICE"

    invoke-virtual {p2, v0}, Landroid/content/Intent;->getParcelableExtra(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Landroid/bluetooth/BluetoothDevice;

    .line 721
    iget-object v1, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->d:Landroid/bluetooth/BluetoothDevice;

    invoke-virtual {v0, v1}, Landroid/bluetooth/BluetoothDevice;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 722
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 723
    const-string v1, "CAR.BT"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bond state changed, but not our device: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0}, Landroid/bluetooth/BluetoothDevice;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 728
    :cond_4
    const-string v1, "android.bluetooth.device.extra.BOND_STATE"

    const/high16 v2, -0x80000000

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    .line 730
    const-string v1, "CAR.BT"

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 731
    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    .line 732
    if-ne v2, v6, :cond_7

    .line 733
    const-string v1, "NONE"

    .line 739
    :cond_5
    :goto_1
    const-string v3, "CAR.BT"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "BondStateChangeReceiver#onReceive: new state="

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 741
    :cond_6
    if-ne v2, v7, :cond_9

    .line 742
    iget-object v1, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    iget-object v1, v1, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v1}, Lcom/google/android/gms/car/a/p;->c()V

    .line 743
    iget-object v1, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    :try_start_0
    const-class v1, Landroid/bluetooth/BluetoothDevice;

    const-string v2, "setPhonebookAccessPermission"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Class;

    const/4 v4, 0x0

    sget-object v5, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/reflect/InvocationTargetException; {:try_start_0 .. :try_end_0} :catch_2

    goto/16 :goto_0

    :catch_0
    move-exception v0

    const-string v1, "CAR.BT"

    const-string v2, "Failed to set phone book access permission."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 734
    :cond_7
    const/16 v3, 0xb

    if-ne v2, v3, :cond_8

    .line 735
    const-string v1, "BONDING"

    goto :goto_1

    .line 736
    :cond_8
    if-ne v2, v7, :cond_5

    .line 737
    const-string v1, "BONDED"

    goto :goto_1

    .line 743
    :catch_1
    move-exception v0

    const-string v1, "CAR.BT"

    const-string v2, "Failed to set phone book access permission."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    :catch_2
    move-exception v0

    const-string v1, "CAR.BT"

    const-string v2, "Failed to set phone book access permission."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto/16 :goto_0

    .line 744
    :cond_9
    if-ne v2, v6, :cond_1

    .line 747
    iget-object v0, p0, Lcom/google/android/gms/car/a/q;->a:Lcom/google/android/gms/car/a/k;

    iget-object v0, v0, Lcom/google/android/gms/car/a/k;->e:Lcom/google/android/gms/car/a/p;

    invoke-interface {v0}, Lcom/google/android/gms/car/a/p;->d()V

    goto/16 :goto_0
.end method
