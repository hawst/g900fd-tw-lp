.class public final Lcom/google/android/gms/auth/trustagent/trustlet/bk;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/auth/trustagent/trustlet/bi;


# instance fields
.field public final a:Landroid/content/Context;

.field public b:Z

.field public c:Z

.field public d:Lcom/google/android/gms/auth/trustagent/trustlet/u;

.field e:Lcom/google/android/gms/auth/trustagent/trustlet/w;

.field f:Z

.field public final g:Landroid/content/BroadcastReceiver;

.field public final h:Ljava/util/Map;

.field final i:Ljava/util/Set;

.field public j:Ljava/util/Map;

.field public k:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

.field public l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

.field private final m:Ljava/util/List;

.field private n:Z

.field private final o:Ljava/util/List;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    .line 78
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->m:Ljava/util/List;

    .line 79
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->j:Ljava/util/Map;

    .line 81
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->o:Ljava/util/List;

    .line 82
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    .line 84
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->i:Ljava/util/Set;

    .line 86
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bl;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bl;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->e:Lcom/google/android/gms/auth/trustagent/trustlet/w;

    .line 103
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/u;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->e:Lcom/google/android/gms/auth/trustagent/trustlet/w;

    invoke-direct {v0, p1, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/u;-><init>(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/w;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->d:Lcom/google/android/gms/auth/trustagent/trustlet/u;

    .line 104
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->d:Lcom/google/android/gms/auth/trustagent/trustlet/u;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/u;->a()V

    .line 106
    invoke-static {}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->a()Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->k:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    .line 107
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->k:Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b(Z)V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V

    .line 109
    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bm;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bm;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->g:Landroid/content/BroadcastReceiver;

    .line 128
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.gservices.intent.action.GSERVICES_CHANGED"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->g:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2, v0}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 129
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/auth/trustagent/trustlet/bk;Ljava/util/List;)V
    .locals 1

    .prologue
    .line 27
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/util/List;)V

    return-void
.end method

.method private a(ZZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 312
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bo;

    .line 313
    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/gms/auth/trustagent/trustlet/bo;->a(ZZLjava/lang/String;)V

    goto :goto_0

    .line 315
    :cond_0
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 357
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c()V

    .line 358
    return-void
.end method

.method public final a(Lcom/google/android/gms/auth/trustagent/trustlet/bh;ZLjava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 347
    if-eqz p2, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1, p4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 352
    :goto_0
    invoke-virtual {p0, p3}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/lang/String;)V

    .line 353
    return-void

    .line 350
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->j:Ljava/util/Map;

    invoke-virtual {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V
    .locals 1

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 140
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 270
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->f:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    if-eqz v0, :cond_2

    .line 271
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    if-eqz v0, :cond_1

    .line 272
    iput-boolean v2, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    .line 273
    invoke-direct {p0, v2, v2, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(ZZLjava/lang/String;)V

    .line 292
    :cond_1
    :goto_0
    return-void

    .line 279
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_3
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    .line 280
    iget-boolean v4, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g:Z

    if-eqz v4, :cond_3

    .line 282
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->g()Z

    move-result v0

    if-eqz v0, :cond_4

    move v0, v1

    move v2, v1

    .line 288
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    if-eq v0, v1, :cond_1

    .line 289
    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    .line 290
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    invoke-direct {p0, v0, v2, p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(ZZLjava/lang/String;)V

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_1

    :cond_5
    move v0, v2

    goto :goto_1
.end method

.method public final a(Ljava/util/List;)V
    .locals 9

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x1

    .line 242
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->o:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    move v2, v3

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bj;

    .line 245
    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bj;->d()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bj;->b(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 247
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    if-eqz v1, :cond_1

    const-string v6, "Coffee - TrustletManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Stopping trustlet "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v6, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->k()V

    move v0, v4

    :goto_1
    if-eqz v0, :cond_6

    move v2, v4

    .line 248
    goto :goto_0

    :cond_1
    move v0, v3

    .line 247
    goto :goto_1

    .line 251
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v1, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bj;->a()Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    move-result-object v1

    if-eqz v1, :cond_3

    const-string v6, "Coffee - TrustletManager"

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "Starting trustlet "

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->d()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v6, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {v1, v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->a(Landroid/content/Context;Lcom/google/android/gms/auth/trustagent/trustlet/bi;)V

    move v0, v4

    :goto_2
    if-eqz v0, :cond_6

    move v0, v4

    :goto_3
    move v2, v0

    .line 255
    goto/16 :goto_0

    :cond_3
    move v0, v3

    .line 251
    goto :goto_2

    .line 256
    :cond_4
    if-eqz v2, :cond_5

    .line 257
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a(Ljava/lang/String;)V

    .line 259
    :cond_5
    return-void

    :cond_6
    move v0, v2

    goto :goto_3
.end method

.method public final a(Lcom/google/android/gms/auth/trustagent/trustlet/bj;)Z
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-virtual {p1, v0}, Lcom/google/android/gms/auth/trustagent/trustlet/bj;->a(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->o:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 164
    invoke-virtual {p1}, Lcom/google/android/gms/auth/trustagent/trustlet/bj;->c()Ljava/lang/String;

    move-result-object v0

    .line 165
    if-eqz v0, :cond_0

    .line 166
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->i:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 167
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    if-nez v0, :cond_0

    new-instance v0, Lcom/google/android/gms/auth/trustagent/trustlet/bn;

    invoke-direct {v0, p0}, Lcom/google/android/gms/auth/trustagent/trustlet/bn;-><init>(Lcom/google/android/gms/auth/trustagent/trustlet/bk;)V

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->a:Landroid/content/Context;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ar;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->l:Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;

    invoke-interface {v0, v1}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 169
    :cond_0
    const/4 v0, 0x1

    .line 171
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/auth/trustagent/trustlet/bo;)V
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->m:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->remove(Ljava/lang/Object;)Z

    .line 144
    return-void
.end method

.method public final b()Z
    .locals 1

    .prologue
    .line 362
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->b:Z

    return v0
.end method

.method public final c()V
    .locals 3

    .prologue
    .line 295
    const/4 v1, 0x0

    .line 296
    iget-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->c:Z

    if-nez v0, :cond_2

    .line 297
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->h:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;

    .line 298
    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x1

    .line 304
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->n:Z

    if-eq v0, v1, :cond_1

    .line 305
    iput-boolean v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->n:Z

    .line 306
    iget-boolean v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->n:Z

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/bk;->m:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/trustlet/bo;

    invoke-interface {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bo;->a(Z)V

    goto :goto_1

    .line 308
    :cond_1
    return-void

    :cond_2
    move v0, v1

    goto :goto_0
.end method
