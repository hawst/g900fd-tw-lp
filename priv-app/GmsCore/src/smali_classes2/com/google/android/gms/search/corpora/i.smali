.class public Lcom/google/android/gms/search/corpora/i;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 31
    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move v2, v1

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 33
    return-void
.end method

.method private c()Lcom/google/android/gms/appdatasearch/CorpusStatus;
    .locals 11

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x0

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/i;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->b:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bb;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    if-eqz v0, :cond_0

    .line 49
    const-string v1, "Bad get corpus status args: %s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    move-object v0, v2

    .line 77
    :goto_0
    return-object v0

    .line 53
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v4

    .line 54
    invoke-interface {v4}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v10

    monitor-enter v10

    .line 55
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/i;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v0, Lcom/google/android/gms/icing/impl/a/aa;

    .line 56
    const/4 v1, 0x1

    new-array v5, v1, [Ljava/lang/String;

    const/4 v6, 0x0

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/i;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;

    iget-object v1, v1, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Request;->b:Ljava/lang/String;

    aput-object v1, v5, v6

    const/4 v1, 0x0

    invoke-interface {v4, v0, v5, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    .line 61
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 62
    invoke-interface {v4, v0}, Lcom/google/android/gms/icing/impl/a/x;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v1

    .line 64
    new-instance v8, Landroid/os/Bundle;

    invoke-direct {v8}, Landroid/os/Bundle;-><init>()V

    .line 66
    iget-object v0, v1, Lcom/google/android/gms/icing/i;->b:Lcom/google/android/gms/icing/j;

    iget-object v2, v0, Lcom/google/android/gms/icing/j;->c:[Lcom/google/android/gms/icing/k;

    array-length v4, v2

    move v0, v3

    :goto_1
    if-ge v0, v4, :cond_1

    aget-object v3, v2, v0

    .line 67
    iget-object v5, v3, Lcom/google/android/gms/icing/k;->a:Ljava/lang/String;

    iget v3, v3, Lcom/google/android/gms/icing/k;->b:I

    invoke-virtual {v8, v5, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 66
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 70
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/i;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v0

    .line 72
    iget-object v2, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v2, v2, Lcom/google/android/gms/icing/g;->a:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IZ)J

    move-result-wide v2

    .line 73
    iget-object v4, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v4, v4, Lcom/google/android/gms/icing/g;->a:I

    const/4 v5, 0x1

    invoke-virtual {v0, v4, v5}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IZ)J

    move-result-wide v4

    .line 74
    iget-object v0, v1, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget-object v9, v0, Lcom/google/android/gms/icing/g;->c:Ljava/lang/String;

    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    const-wide/16 v6, 0x0

    invoke-direct/range {v1 .. v9}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>(JJJLandroid/os/Bundle;Ljava/lang/String;)V

    move-object v0, v1

    .line 77
    :goto_2
    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/appdatasearch/CorpusStatus;

    invoke-direct {v0}, Lcom/google/android/gms/appdatasearch/CorpusStatus;-><init>()V

    :cond_2
    monitor-exit v10
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 80
    :catchall_0
    move-exception v0

    monitor-exit v10

    throw v0

    :cond_3
    move-object v0, v2

    goto :goto_2
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 28
    new-instance v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;

    invoke-direct {v0}, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;-><init>()V

    invoke-direct {p0}, Lcom/google/android/gms/search/corpora/i;->c()Lcom/google/android/gms/appdatasearch/CorpusStatus;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->b:Lcom/google/android/gms/appdatasearch/CorpusStatus;

    sget-object v1, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v1, v0, Lcom/google/android/gms/search/corpora/GetCorpusStatusCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method
