.class final Lcom/google/android/gms/car/ag;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/content/ServiceConnection;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/ae;

.field private b:J

.field private c:I


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/ae;)V
    .locals 2

    .prologue
    .line 322
    iput-object p1, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 327
    const-wide/high16 v0, -0x8000000000000000L

    iput-wide v0, p0, Lcom/google/android/gms/car/ag;->b:J

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/ag;J)J
    .locals 1

    .prologue
    .line 322
    iput-wide p1, p0, Lcom/google/android/gms/car/ag;->b:J

    return-wide p1
.end method


# virtual methods
.method public final onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3

    .prologue
    .line 333
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 334
    const-string v0, "CAR.IME"

    const-string v1, "onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 336
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    monitor-enter v1

    .line 337
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    invoke-static {p2}, Lcom/google/android/gms/car/b/e;->a(Landroid/os/IBinder;)Lcom/google/android/gms/car/b/d;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/b/d;)Lcom/google/android/gms/car/b/d;

    .line 338
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 6

    .prologue
    .line 343
    const-string v0, "CAR.IME"

    const/4 v1, 0x2

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 344
    const-string v0, "CAR.IME"

    const-string v1, "onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 346
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    monitor-enter v1

    .line 347
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    const/4 v2, 0x0

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/b/d;)Lcom/google/android/gms/car/b/d;

    .line 348
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 349
    const-string v0, "CAR.IME"

    const-string v1, "Car IME died."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 351
    iget-object v0, p0, Lcom/google/android/gms/car/ag;->a:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->c(Lcom/google/android/gms/car/ae;)V

    .line 354
    iget-wide v0, p0, Lcom/google/android/gms/car/ag;->b:J

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    const-wide/16 v4, 0xbb8

    sub-long/2addr v2, v4

    cmp-long v0, v0, v2

    if-lez v0, :cond_1

    .line 356
    iget v0, p0, Lcom/google/android/gms/car/ag;->c:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/ag;->c:I

    .line 361
    :goto_0
    iget v0, p0, Lcom/google/android/gms/car/ag;->c:I

    const/4 v1, 0x5

    if-le v0, v1, :cond_2

    .line 365
    const-string v0, "CAR.IME"

    const-string v1, "Input service crashed too many times, giving up"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 390
    :goto_1
    return-void

    .line 348
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 358
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/ag;->c:I

    goto :goto_0

    .line 369
    :cond_2
    const-string v0, "CAR.IME"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 370
    const-string v0, "CAR.IME"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Scheduling rebind for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/ag;->c:I

    mul-int/lit16 v2, v2, 0xbb8

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ms"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 375
    :cond_3
    new-instance v0, Lcom/google/android/gms/car/ah;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/ah;-><init>(Lcom/google/android/gms/car/ag;)V

    iget v1, p0, Lcom/google/android/gms/car/ag;->c:I

    mul-int/lit16 v1, v1, 0xbb8

    int-to-long v2, v1

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/car/of;->a(Ljava/lang/Runnable;J)V

    goto :goto_1
.end method
