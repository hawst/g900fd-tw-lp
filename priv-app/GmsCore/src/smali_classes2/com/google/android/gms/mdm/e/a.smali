.class public final Lcom/google/android/gms/mdm/e/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/r;

.field public static final b:Lcom/google/android/gms/common/a/r;

.field public static final c:Lcom/google/android/gms/common/a/r;

.field public static final d:Lcom/google/android/gms/common/a/r;

.field public static final e:Lcom/google/android/gms/common/a/r;

.field public static final f:Lcom/google/android/gms/common/a/r;

.field public static final g:Lcom/google/android/gms/common/a/r;

.field public static final h:Lcom/google/android/gms/common/a/r;

.field public static final i:Lcom/google/android/gms/common/a/r;

.field private static final j:Lcom/google/android/gms/common/a/m;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v3, 0x0

    const/4 v4, 0x0

    .line 14
    new-instance v0, Lcom/google/android/gms/common/a/m;

    const-string v1, "mdm"

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/a/m;-><init>(Ljava/lang/String;)V

    .line 18
    sput-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "sitrepGmsCoreVersion"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->a:Lcom/google/android/gms/common/a/r;

    .line 23
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "sitrepGcmRegistrationId"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->b:Lcom/google/android/gms/common/a/r;

    .line 28
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "sitrepIsDeviceAdmin"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->c:Lcom/google/android/gms/common/a/r;

    .line 41
    sget-object v1, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v2, "locationEnabled"

    sget-object v0, Lcom/google/android/gms/mdm/b/a;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Boolean;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->d:Lcom/google/android/gms/common/a/r;

    .line 44
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "lockMessage"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->e:Lcom/google/android/gms/common/a/r;

    .line 47
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "lockPhoneNumber"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->f:Lcom/google/android/gms/common/a/r;

    .line 55
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "lastSitrepReason"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->g:Lcom/google/android/gms/common/a/r;

    .line 62
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "sitrepRetryEpochTimeMs"

    const-wide/16 v2, 0x0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->h:Lcom/google/android/gms/common/a/r;

    .line 69
    sget-object v0, Lcom/google/android/gms/mdm/e/a;->j:Lcom/google/android/gms/common/a/m;

    const-string v1, "sitrepFailureCount"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/a/m;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/r;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/e/a;->i:Lcom/google/android/gms/common/a/r;

    return-void
.end method
