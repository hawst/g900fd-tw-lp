.class public final Lcom/google/android/gms/common/widget/a/r;
.super Lcom/google/android/gms/common/widget/a/o;
.source "SourceFile"

# interfaces
.implements Landroid/widget/Checkable;


# instance fields
.field private a:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/widget/a/o;-><init>(Landroid/content/Context;)V

    .line 23
    return-void
.end method


# virtual methods
.method public final e()I
    .locals 1

    .prologue
    .line 28
    sget v0, Lcom/google/android/gms/common/widget/e;->d:I

    return v0
.end method

.method public final f()Lcom/google/android/gms/common/widget/a/h;
    .locals 1

    .prologue
    .line 34
    invoke-static {}, Lcom/google/android/gms/common/widget/a/c;->a()Lcom/google/android/gms/common/widget/a/c;

    move-result-object v0

    return-object v0
.end method

.method public final isChecked()Z
    .locals 1

    .prologue
    .line 40
    iget-boolean v0, p0, Lcom/google/android/gms/common/widget/a/r;->a:Z

    return v0
.end method

.method public final setChecked(Z)V
    .locals 0

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/google/android/gms/common/widget/a/r;->a:Z

    .line 47
    invoke-virtual {p0}, Lcom/google/android/gms/common/widget/a/r;->d()V

    .line 48
    return-void
.end method

.method public final toggle()V
    .locals 1

    .prologue
    .line 53
    iget-boolean v0, p0, Lcom/google/android/gms/common/widget/a/r;->a:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/widget/a/r;->setChecked(Z)V

    .line 54
    return-void

    .line 53
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
