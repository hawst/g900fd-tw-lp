.class public final Lcom/google/android/gms/games/service/a/g/d;
.super Lcom/google/android/gms/games/service/a/b;
.source "SourceFile"


# instance fields
.field private final b:Lcom/google/android/gms/games/internal/dr;

.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Ljava/lang/String;

.field private final e:I

.field private final f:I

.field private final g:I

.field private final h:I


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;IIII)V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/service/a/b;-><init>(Lcom/google/android/gms/common/server/ClientContext;I)V

    .line 32
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/g/d;->b:Lcom/google/android/gms/games/internal/dr;

    .line 33
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/g/d;->c:Lcom/google/android/gms/games/a/au;

    .line 34
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/g/d;->d:Ljava/lang/String;

    .line 35
    iput p4, p0, Lcom/google/android/gms/games/service/a/g/d;->e:I

    .line 36
    iput p5, p0, Lcom/google/android/gms/games/service/a/g/d;->f:I

    .line 37
    iput p6, p0, Lcom/google/android/gms/games/service/a/g/d;->g:I

    .line 38
    iput p7, p0, Lcom/google/android/gms/games/service/a/g/d;->h:I

    .line 39
    return-void
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/games/a/t;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    const/4 v2, 0x1

    .line 44
    if-nez p2, :cond_2

    .line 46
    iget v0, p0, Lcom/google/android/gms/games/service/a/g/d;->h:I

    if-ne v0, v2, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/d;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/g/d;->d:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/games/service/a/g/d;->e:I

    iget v4, p0, Lcom/google/android/gms/games/service/a/g/d;->f:I

    iget v5, p0, Lcom/google/android/gms/games/service/a/g/d;->g:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->b(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 49
    :goto_0
    return-object v0

    .line 46
    :cond_0
    iget v0, p0, Lcom/google/android/gms/games/service/a/g/d;->h:I

    if-nez v0, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/d;->c:Lcom/google/android/gms/games/a/au;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/g/d;->d:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/games/service/a/g/d;->e:I

    iget v4, p0, Lcom/google/android/gms/games/service/a/g/d;->f:I

    iget v5, p0, Lcom/google/android/gms/games/service/a/g/d;->g:I

    move-object v0, p1

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;III)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    :cond_1
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown page type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/games/service/a/g/d;->h:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 47
    :cond_2
    if-ne p2, v2, :cond_3

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/d;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/g/d;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1, v2}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Ljava/lang/String;Z)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    goto :goto_0

    .line 51
    :cond_3
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Incorrect number of data holders requested!"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected final a([Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 78
    array-length v0, p1

    const/4 v3, 0x2

    if-ne v0, v3, :cond_0

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/g/d;->b:Lcom/google/android/gms/games/internal/dr;

    aget-object v1, p1, v1

    aget-object v2, p1, v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/common/data/DataHolder;)V

    .line 80
    return-void

    :cond_0
    move v0, v2

    .line 78
    goto :goto_0
.end method
