.class public final Lcom/google/android/gms/games/c/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static A:Lcom/google/android/gms/common/a/d;

.field public static B:Lcom/google/android/gms/common/a/d;

.field public static C:Lcom/google/android/gms/common/a/d;

.field public static D:Lcom/google/android/gms/common/a/d;

.field public static E:Lcom/google/android/gms/common/a/d;

.field public static F:Lcom/google/android/gms/common/a/d;

.field public static G:Lcom/google/android/gms/common/a/d;

.field public static H:Lcom/google/android/gms/common/a/d;

.field public static I:Lcom/google/android/gms/common/a/d;

.field public static J:Lcom/google/android/gms/common/a/d;

.field public static K:Lcom/google/android/gms/common/a/d;

.field public static L:Lcom/google/android/gms/common/a/d;

.field public static M:Lcom/google/android/gms/common/a/d;

.field public static N:Lcom/google/android/gms/common/a/d;

.field public static O:Lcom/google/android/gms/common/a/d;

.field public static P:Lcom/google/android/gms/common/a/d;

.field public static Q:Lcom/google/android/gms/common/a/d;

.field public static R:Lcom/google/android/gms/common/a/d;

.field public static S:Lcom/google/android/gms/common/a/d;

.field public static T:Lcom/google/android/gms/common/a/d;

.field public static U:Lcom/google/android/gms/common/a/d;

.field public static V:Lcom/google/android/gms/common/a/d;

.field public static W:Lcom/google/android/gms/common/a/d;

.field public static a:Lcom/google/android/gms/common/a/d;

.field public static b:Lcom/google/android/gms/common/a/d;

.field public static c:Lcom/google/android/gms/common/a/d;

.field public static d:Lcom/google/android/gms/common/a/d;

.field public static e:Lcom/google/android/gms/common/a/d;

.field public static f:Lcom/google/android/gms/common/a/d;

.field public static g:Lcom/google/android/gms/common/a/d;

.field public static h:Lcom/google/android/gms/common/a/d;

.field public static i:Lcom/google/android/gms/common/a/d;

.field public static j:Lcom/google/android/gms/common/a/d;

.field public static k:Lcom/google/android/gms/common/a/d;

.field public static l:Lcom/google/android/gms/common/a/d;

.field public static m:Lcom/google/android/gms/common/a/d;

.field public static n:Lcom/google/android/gms/common/a/d;

.field public static o:Lcom/google/android/gms/common/a/d;

.field public static p:Lcom/google/android/gms/common/a/d;

.field public static q:Lcom/google/android/gms/common/a/d;

.field public static r:Lcom/google/android/gms/common/a/d;

.field public static s:Lcom/google/android/gms/common/a/d;

.field public static t:Lcom/google/android/gms/common/a/d;

.field public static u:Lcom/google/android/gms/common/a/d;

.field public static v:Lcom/google/android/gms/common/a/d;

.field public static w:Lcom/google/android/gms/common/a/d;

.field public static x:Lcom/google/android/gms/common/a/d;

.field public static y:Lcom/google/android/gms/common/a/d;

.field public static z:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 10

    .prologue
    const-wide/32 v8, 0x493e0

    const/16 v7, 0x1e

    const/16 v6, 0xa

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 14
    const-string v0, "games.base_service_url"

    const-string v1, "https://www.googleapis.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->a:Lcom/google/android/gms/common/a/d;

    .line 16
    const-string v0, "games.server_version"

    const-string v1, "v1"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->b:Lcom/google/android/gms/common/a/d;

    .line 18
    const-string v0, "games.internal_server_version"

    const-string v1, "v1whitelisted"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->c:Lcom/google/android/gms/common/a/d;

    .line 20
    const-string v0, "games.force_server_version"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->d:Lcom/google/android/gms/common/a/d;

    .line 24
    const-string v0, "games.finsky_service_url"

    const-string v1, "https://market.android.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->e:Lcom/google/android/gms/common/a/d;

    .line 27
    const-string v0, "games.cache_enabled"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->f:Lcom/google/android/gms/common/a/d;

    .line 30
    const-string v0, "games.verbose_volley_logging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->g:Lcom/google/android/gms/common/a/d;

    .line 33
    const-string v0, "games.verbose_cache_logging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->h:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "games.revision_check_interval_ms"

    const-wide/32 v2, 0x5265c00

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->i:Lcom/google/android/gms/common/a/d;

    .line 41
    const-string v0, "games.leaderboard_cache_stale_threshold_millis"

    const-wide/32 v2, 0x1b7740

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->j:Lcom/google/android/gms/common/a/d;

    .line 45
    const-string v0, "games.achievement_cache_stale_threshold_millis"

    const-wide/32 v2, 0x1b7740

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->k:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "games.experience_event_cache_stale_threshold_millis"

    const-wide/32 v2, 0x36ee80

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->l:Lcom/google/android/gms/common/a/d;

    .line 54
    const-string v0, "games.max_experience_events_cached"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->m:Lcom/google/android/gms/common/a/d;

    .line 59
    const-string v0, "games.max_scores_per_page"

    const/16 v1, 0x19

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->n:Lcom/google/android/gms/common/a/d;

    .line 63
    const-string v0, "games.enable_buzzbot_subscription"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->o:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "games.enable_verbose_realtime_multiplayer_logging"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->p:Lcom/google/android/gms/common/a/d;

    .line 71
    const-string v0, "games.always_show_achievements"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->q:Lcom/google/android/gms/common/a/d;

    .line 75
    const-string v0, "games.max_completed_matches"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->r:Lcom/google/android/gms/common/a/d;

    .line 79
    const-string v0, "games.max_accepted_outbound_requests"

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->s:Lcom/google/android/gms/common/a/d;

    .line 83
    const-string v0, "games.sync_buffer_millis"

    const-wide/16 v2, 0x7530

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->t:Lcom/google/android/gms/common/a/d;

    .line 88
    const-string v0, "games.sync_buffer_max_millis"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->u:Lcom/google/android/gms/common/a/d;

    .line 92
    const-string v0, "games.tickle_sync_threshold_millis"

    const-wide/32 v2, 0xea60

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->v:Lcom/google/android/gms/common/a/d;

    .line 96
    const-string v0, "games.max_turn_based_match_data_bytes"

    const/high16 v1, 0x20000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->w:Lcom/google/android/gms/common/a/d;

    .line 100
    const-string v0, "games.max_request_payload_bytes"

    const/16 v1, 0x800

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->x:Lcom/google/android/gms/common/a/d;

    .line 104
    const-string v0, "games.max_request_lifetime_days"

    const/16 v1, 0xe

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->y:Lcom/google/android/gms/common/a/d;

    .line 108
    const-string v0, "games.allow_periodic_syncs"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->z:Lcom/google/android/gms/common/a/d;

    .line 112
    const-string v0, "games.periodic_fine_sync_period_seconds"

    const-wide/16 v2, 0xe10

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->A:Lcom/google/android/gms/common/a/d;

    .line 116
    const-string v0, "games.periodic_sync_period_seconds"

    const-wide/32 v2, 0x15f90

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->B:Lcom/google/android/gms/common/a/d;

    .line 121
    const-string v0, "games.most_recent_connection_threshold_millis"

    const-wide/32 v2, 0x240c8400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->C:Lcom/google/android/gms/common/a/d;

    .line 125
    const-string v0, "games.event_window_size_millis"

    const-wide/32 v2, 0xdbba0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->D:Lcom/google/android/gms/common/a/d;

    .line 129
    const-string v0, "games.rtmp_buzz_email_address"

    const-string v1, "games@ob.talk.google.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->E:Lcom/google/android/gms/common/a/d;

    .line 134
    const-string v0, "games.recent_quest_threshold_millis"

    const-wide/32 v2, 0x240c8400

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->F:Lcom/google/android/gms/common/a/d;

    .line 139
    const-string v0, "games.snapshot_folder"

    const-string v1, "play_games"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->G:Lcom/google/android/gms/common/a/d;

    .line 144
    const-string v0, "games.snapshot_initial_mime_type"

    const-string v1, "application/vnd.google-play-games.snapshot-initial"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->H:Lcom/google/android/gms/common/a/d;

    .line 150
    const-string v0, "games.snapshot_committed_mime_type"

    const-string v1, "application/vnd.google-play-games.snapshot"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->I:Lcom/google/android/gms/common/a/d;

    .line 155
    const-string v0, "games.snapshot_conflict_mime_type"

    const-string v1, "application/vnd.google-play-games.snapshot-conflict"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->J:Lcom/google/android/gms/common/a/d;

    .line 160
    const-string v0, "games.max_snapshot_bytes"

    const/high16 v1, 0x300000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->K:Lcom/google/android/gms/common/a/d;

    .line 164
    const-string v0, "games.max_snapshot_cover_image_bytes"

    const v1, 0xc8000

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->L:Lcom/google/android/gms/common/a/d;

    .line 168
    const-string v0, "games.rtmp_max_reconnect_attempts"

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->M:Lcom/google/android/gms/common/a/d;

    .line 172
    const-string v0, "games.rtmp_libjingle_connect_timeout_seconds"

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->N:Lcom/google/android/gms/common/a/d;

    .line 176
    const-string v0, "games.rtmp_reuse_xmpp_connections"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->O:Lcom/google/android/gms/common/a/d;

    .line 181
    const-string v0, "games.nearby_player_lifetime_millis"

    const-wide/32 v2, 0x927c0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->P:Lcom/google/android/gms/common/a/d;

    .line 187
    const-string v0, "games.sign_leaderboard_scores"

    const-wide/high16 v2, 0x3ff0000000000000L    # 1.0

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Double;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->Q:Lcom/google/android/gms/common/a/d;

    .line 192
    const-string v0, "games.use_separate_id_for_play_games_and_gms_core"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->R:Lcom/google/android/gms/common/a/d;

    .line 197
    const-string v0, "games.forced_device_type"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->S:Lcom/google/android/gms/common/a/d;

    .line 202
    const-string v0, "games.max_snapshot_property_length"

    const/16 v1, 0x71

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->T:Lcom/google/android/gms/common/a/d;

    .line 207
    const-string v0, "games.capturing"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->U:Lcom/google/android/gms/common/a/d;

    .line 211
    const-string v0, "games.enable_clickable_toasts"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->V:Lcom/google/android/gms/common/a/d;

    .line 216
    const-string v0, "games.app_content_in_memory_cache_timeout"

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/games/c/a;->W:Lcom/google/android/gms/common/a/d;

    return-void
.end method
