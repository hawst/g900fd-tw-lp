.class public Lcom/google/android/gms/car/senderprotocol/aq;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# static fields
.field private static final f:Lcom/google/android/gms/car/ii;

.field private static final g:Lcom/google/android/gms/car/ii;


# instance fields
.field a:I

.field protected volatile b:Z

.field protected volatile c:I

.field protected volatile d:Z

.field protected e:J

.field private final k:Ljava/util/Vector;

.field private l:Z

.field private volatile m:I

.field private final n:Ljava/util/concurrent/Semaphore;

.field private volatile o:I

.field private final p:Ljava/lang/Object;

.field private q:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 39
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.enable_media_frames"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->f:Lcom/google/android/gms/car/ii;

    .line 41
    new-instance v0, Lcom/google/android/gms/car/ii;

    const-string v1, "debug.car.media_ack_latency"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/ii;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->g:Lcom/google/android/gms/car/ii;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 83
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 62
    iput-boolean v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->l:Z

    .line 63
    iput-boolean v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    .line 64
    iput v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->c:I

    .line 65
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->m:I

    .line 67
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v2}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    .line 68
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    .line 70
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->e:J

    .line 72
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->p:Ljava/lang/Object;

    .line 80
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0, v2}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 84
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->f:Lcom/google/android/gms/car/ii;

    .line 85
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->k:Ljava/util/Vector;

    .line 89
    return-void
.end method

.method private declared-synchronized a(Lcom/google/android/c/b/da;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 380
    monitor-enter p0

    :try_start_0
    invoke-virtual {p1}, Lcom/google/android/c/b/da;->d()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p1}, Lcom/google/android/c/b/da;->c()Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v0

    .line 381
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/c/b/da;->b()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-virtual {p1}, Lcom/google/android/c/b/da;->a()I

    move-result v3

    if-ne v3, v0, :cond_2

    .line 384
    :goto_1
    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    if-nez v1, :cond_3

    if-eqz v0, :cond_3

    .line 385
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    .line 386
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/senderprotocol/aq;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 380
    goto :goto_0

    :cond_2
    move v0, v1

    .line 381
    goto :goto_1

    .line 387
    :cond_3
    :try_start_1
    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    if-eqz v1, :cond_0

    if-nez v0, :cond_0

    .line 388
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    .line 389
    invoke-virtual {p0, v2}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Z)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 380
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method


# virtual methods
.method public final a()V
    .locals 3

    .prologue
    .line 93
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->a()V

    .line 94
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "send setup, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v0, Lcom/google/android/c/b/cr;

    invoke-direct {v0}, Lcom/google/android/c/b/cr;-><init>()V

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->a:I

    iput v1, v0, Lcom/google/android/c/b/cr;->a:I

    const v1, 0x8000

    invoke-static {v0}, Lcom/google/android/c/b/cr;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/aq;->a(I[B)V

    .line 95
    return-void
.end method

.method public a(I)V
    .locals 3

    .prologue
    .line 99
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Closing channel with reason: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ch:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 103
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    if-eqz v0, :cond_1

    .line 105
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    .line 107
    :cond_1
    return-void
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 4

    .prologue
    .line 152
    packed-switch p1, :pswitch_data_0

    .line 174
    :pswitch_0
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Received message with invalid type header: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " ch:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    const/4 v0, 0x1

    if-eq p1, v0, :cond_1

    .line 181
    invoke-static {p2}, Lcom/google/android/c/a/a;->a(Ljava/nio/ByteBuffer;)V

    .line 183
    :cond_1
    return-void

    .line 154
    :pswitch_1
    new-instance v0, Lcom/google/android/c/b/t;

    invoke-direct {v0}, Lcom/google/android/c/b/t;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/t;

    .line 155
    if-eqz v0, :cond_0

    .line 156
    invoke-virtual {p0, v0}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/t;)V

    goto :goto_0

    .line 161
    :pswitch_2
    new-instance v0, Lcom/google/android/c/b/e;

    invoke-direct {v0}, Lcom/google/android/c/b/e;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/e;

    .line 162
    if-eqz v0, :cond_0

    .line 163
    sget-object v1, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    if-nez v1, :cond_2

    iget v1, v0, Lcom/google/android/c/b/e;->a:I

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->m:I

    if-eq v1, v2, :cond_3

    const-string v1, "CAR.MEDIA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got ack while not started. Previous session id was "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->m:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/c/b/e;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ch:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    :cond_2
    iget v1, v0, Lcom/google/android/c/b/e;->a:I

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->c:I

    if-eq v1, v2, :cond_3

    iget v1, v0, Lcom/google/android/c/b/e;->a:I

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->m:I

    if-eq v1, v2, :cond_0

    const-string v1, "CAR.MEDIA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Got mismatch session id in ack. Expected: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->c:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " got: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v0, v0, Lcom/google/android/c/b/e;->a:I

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " ch:"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    :cond_3
    sget-object v1, Lcom/google/android/gms/car/senderprotocol/aq;->f:Lcom/google/android/gms/car/ii;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Lcom/google/android/c/b/e;->a()I

    move-result v0

    invoke-virtual {v1, v0}, Ljava/util/concurrent/Semaphore;->release(I)V

    invoke-virtual {p0}, Lcom/google/android/gms/car/senderprotocol/aq;->b()Z

    move-result v0

    if-nez v0, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->p:Ljava/lang/Object;

    monitor-enter v1

    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->p:Ljava/lang/Object;

    invoke-virtual {v0}, Ljava/lang/Object;->notifyAll()V

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :cond_4
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto/16 :goto_0

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 168
    :pswitch_3
    new-instance v0, Lcom/google/android/c/b/da;

    invoke-direct {v0}, Lcom/google/android/c/b/da;-><init>()V

    invoke-static {v0, p2}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/protobuf/nano/j;Ljava/nio/ByteBuffer;)Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/c/b/da;

    .line 170
    invoke-direct {p0, v0}, Lcom/google/android/gms/car/senderprotocol/aq;->a(Lcom/google/android/c/b/da;)V

    goto/16 :goto_0

    .line 152
    :pswitch_data_0
    .packed-switch 0x8003
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public final a(J)V
    .locals 5

    .prologue
    .line 121
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->p:Ljava/lang/Object;

    monitor-enter v1

    .line 122
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/car/senderprotocol/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 123
    const-string v0, "CAR.MEDIA"

    const/4 v2, 0x3

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 124
    const-string v0, "CAR.MEDIA"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "waiting for ACK, max:"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " available:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " ch:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v3}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 129
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->p:Ljava/lang/Object;

    invoke-virtual {v0, p1, p2}, Ljava/lang/Object;->wait(J)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 134
    :cond_1
    :goto_0
    :try_start_2
    monitor-exit v1
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 135
    invoke-virtual {p0}, Lcom/google/android/gms/car/senderprotocol/aq;->b()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 136
    const-string v0, "CAR.MEDIA"

    const-string v1, "Car did not give all ACKs. Just reset counter"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 138
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/Semaphore;->release(I)V

    .line 140
    :cond_2
    return-void

    .line 134
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(JLjava/nio/ByteBuffer;)V
    .locals 5
    .annotation build Landroid/annotation/TargetApi;
        value = 0x13
    .end annotation

    .prologue
    .line 299
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 300
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/az;->h:Z

    if-eqz v0, :cond_0

    .line 342
    :goto_0
    return-void

    .line 308
    :cond_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->g:Lcom/google/android/gms/car/ii;

    .line 310
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 313
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    const-wide/16 v2, 0x12c

    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v0, v2, v3, v1}, Ljava/util/concurrent/Semaphore;->tryAcquire(JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 316
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v0

    .line 317
    const/16 v1, 0x190

    if-le v0, v1, :cond_1

    .line 318
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Car not sending ACK, channel:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", timeouts:"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", media type:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->a:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", sesisonId:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->c:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", MAX_UNACK:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/az;->j:Lcom/google/android/gms/car/gx;

    const/16 v2, 0xc

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/car/gx;->a(ILjava/lang/String;)V

    .line 324
    :cond_1
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    .line 325
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->g:Lcom/google/android/gms/car/ii;

    .line 328
    sget-object v0, Lcom/google/android/gms/car/senderprotocol/aq;->f:Lcom/google/android/gms/car/ii;

    .line 335
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    if-nez v0, :cond_2

    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to send media data before starting, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 341
    :goto_1
    sget-object v0, Lcom/google/android/gms/car/ib;->b:Lcom/google/android/gms/car/ii;

    goto/16 :goto_0

    .line 335
    :cond_2
    :try_start_1
    invoke-virtual {p3}, Ljava/nio/ByteBuffer;->position()I

    move-result v0

    const/4 v1, 0x2

    invoke-virtual {p3, v1}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    invoke-virtual {p3, p1, p2}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    invoke-virtual {p3, v0}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v1, 0x1

    invoke-virtual {v0, p3, v1}, Lcom/google/android/gms/car/senderprotocol/e;->a(Ljava/nio/ByteBuffer;Z)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 339
    :catch_0
    move-exception v0

    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Dropping frame, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method protected a(Lcom/google/android/c/b/ct;)V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 246
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->l:Z

    if-nez v0, :cond_0

    .line 247
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to start Media before receiving Config message, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 260
    :goto_0
    return-void

    .line 251
    :cond_0
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 252
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "start with MAX_UNACK:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 255
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/Semaphore;->release(I)V

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->q:Ljava/util/concurrent/atomic/AtomicInteger;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/atomic/AtomicInteger;->set(I)V

    .line 257
    const v0, 0x8001

    invoke-static {p1}, Lcom/google/android/c/b/ct;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(I[B)V

    .line 258
    iput-boolean v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    .line 259
    iput-boolean v3, p0, Lcom/google/android/gms/car/senderprotocol/aq;->b:Z

    goto :goto_0
.end method

.method protected a(Lcom/google/android/c/b/t;)V
    .locals 3

    .prologue
    .line 189
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->l:Z

    if-nez v0, :cond_0

    .line 190
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->l:Z

    .line 192
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/c/b/t;->a()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    .line 193
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->drainPermits()I

    .line 194
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v0, v1}, Ljava/util/concurrent/Semaphore;->release(I)V

    .line 195
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 196
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "configMessage, MAX_UNACK:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 198
    :cond_1
    return-void
.end method

.method protected a(Z)V
    .locals 3

    .prologue
    .line 395
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 396
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignored loss of focus, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 398
    :cond_0
    return-void
.end method

.method public final a([B)V
    .locals 3

    .prologue
    .line 289
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    if-nez v0, :cond_0

    .line 290
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to send codec config without starting media, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 295
    :goto_0
    return-void

    .line 294
    :cond_0
    const/4 v0, 0x1

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/car/senderprotocol/aq;->a(I[B)V

    goto :goto_0
.end method

.method protected b(Z)V
    .locals 3

    .prologue
    .line 402
    const-string v0, "CAR.MEDIA"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 403
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Ignored focus gain, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 405
    :cond_0
    return-void
.end method

.method public final b()Z
    .locals 2

    .prologue
    .line 111
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 278
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->c()V

    .line 279
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    const v1, 0x186a0

    invoke-virtual {v0, v1}, Ljava/util/concurrent/Semaphore;->release(I)V

    .line 283
    return-void
.end method

.method public final d()I
    .locals 2

    .prologue
    .line 115
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->n:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->availablePermits()I

    move-result v0

    .line 116
    iget v1, p0, Lcom/google/android/gms/car/senderprotocol/aq;->o:I

    sub-int v0, v1, v0

    .line 117
    if-gez v0, :cond_0

    const/4 v0, 0x0

    :cond_0
    return v0
.end method

.method public l_()V
    .locals 3

    .prologue
    .line 266
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    if-nez v0, :cond_0

    .line 267
    const-string v0, "CAR.MEDIA"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Trying to stop Media before starting, ch:"

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/aq;->i:Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v2}, Lcom/google/android/gms/car/senderprotocol/e;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 274
    :goto_0
    return-void

    .line 270
    :cond_0
    iget v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->c:I

    iput v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->m:I

    .line 271
    new-instance v0, Lcom/google/android/c/b/cu;

    invoke-direct {v0}, Lcom/google/android/c/b/cu;-><init>()V

    .line 272
    const v1, 0x8002

    invoke-static {v0}, Lcom/google/android/c/b/cu;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/car/senderprotocol/aq;->a(I[B)V

    .line 273
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/aq;->d:Z

    goto :goto_0
.end method
