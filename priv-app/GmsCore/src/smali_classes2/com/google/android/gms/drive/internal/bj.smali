.class final Lcom/google/android/gms/drive/internal/bj;
.super Lcom/google/android/gms/drive/internal/ai;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/drive/internal/bg;


# direct methods
.method constructor <init>(Lcom/google/android/gms/drive/internal/bg;Lcom/google/android/gms/common/api/v;)V
    .locals 0

    .prologue
    .line 108
    iput-object p1, p0, Lcom/google/android/gms/drive/internal/bj;->a:Lcom/google/android/gms/drive/internal/bg;

    invoke-direct {p0, p2}, Lcom/google/android/gms/drive/internal/ai;-><init>(Lcom/google/android/gms/common/api/v;)V

    return-void
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 3

    .prologue
    .line 108
    check-cast p1, Lcom/google/android/gms/drive/internal/aj;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/internal/aj;->f()Lcom/google/android/gms/drive/internal/bx;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/drive/internal/DeleteResourceRequest;

    iget-object v2, p0, Lcom/google/android/gms/drive/internal/bj;->a:Lcom/google/android/gms/drive/internal/bg;

    iget-object v2, v2, Lcom/google/android/gms/drive/internal/bg;->a:Lcom/google/android/gms/drive/DriveId;

    invoke-direct {v1, v2}, Lcom/google/android/gms/drive/internal/DeleteResourceRequest;-><init>(Lcom/google/android/gms/drive/DriveId;)V

    new-instance v2, Lcom/google/android/gms/drive/internal/dh;

    invoke-direct {v2, p0}, Lcom/google/android/gms/drive/internal/dh;-><init>(Lcom/google/android/gms/common/api/m;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/drive/internal/bx;->a(Lcom/google/android/gms/drive/internal/DeleteResourceRequest;Lcom/google/android/gms/drive/internal/ca;)V

    return-void
.end method
