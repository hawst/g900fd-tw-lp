.class public Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/firstparty/dataservice/bk;


# instance fields
.field final a:I

.field public final b:I

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    new-instance v0, Lcom/google/android/gms/auth/firstparty/dataservice/bk;

    invoke-direct {v0}, Lcom/google/android/gms/auth/firstparty/dataservice/bk;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->CREATOR:Lcom/google/android/gms/auth/firstparty/dataservice/bk;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 2

    .prologue
    .line 60
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;-><init>(IILjava/lang/String;)V

    .line 61
    return-void
.end method

.method constructor <init>(IILjava/lang/String;)V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    iput p1, p0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->a:I

    .line 51
    iput p2, p0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->b:I

    .line 52
    iput-object p3, p0, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;->c:Ljava/lang/String;

    .line 53
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 56
    const/4 v0, 0x1

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;-><init>(IILjava/lang/String;)V

    .line 57
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 78
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/firstparty/dataservice/bk;->a(Lcom/google/android/gms/auth/firstparty/dataservice/ValidateAccountCredentialsResponse;Landroid/os/Parcel;)V

    .line 79
    return-void
.end method
