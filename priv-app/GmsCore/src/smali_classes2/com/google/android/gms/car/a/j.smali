.class final Lcom/google/android/gms/car/a/j;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/bluetooth/BluetoothProfile$ServiceListener;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/a/h;


# direct methods
.method constructor <init>(Lcom/google/android/gms/car/a/h;)V
    .locals 0

    .prologue
    .line 92
    iput-object p1, p0, Lcom/google/android/gms/car/a/j;->a:Lcom/google/android/gms/car/a/h;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final onServiceConnected(ILandroid/bluetooth/BluetoothProfile;)V
    .locals 3

    .prologue
    .line 95
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 96
    const-string v0, "CAR.BT"

    const-string v1, "a2dp onServiceConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    :cond_0
    const/4 v0, 0x2

    if-eq p1, v0, :cond_1

    .line 99
    const-string v0, "CAR.BT"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "a2dp onServiceConnected: wrong profile="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    :goto_0
    return-void

    .line 102
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/car/a/j;->a:Lcom/google/android/gms/car/a/h;

    check-cast p2, Landroid/bluetooth/BluetoothA2dp;

    iput-object p2, v0, Lcom/google/android/gms/car/a/h;->c:Landroid/bluetooth/BluetoothA2dp;

    .line 103
    iget-object v0, p0, Lcom/google/android/gms/car/a/j;->a:Lcom/google/android/gms/car/a/h;

    invoke-static {v0}, Lcom/google/android/gms/car/a/h;->a(Lcom/google/android/gms/car/a/h;)V

    goto :goto_0
.end method

.method public final onServiceDisconnected(I)V
    .locals 2

    .prologue
    .line 108
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 109
    const-string v0, "CAR.BT"

    const-string v1, "a2dp onServiceDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 111
    :cond_0
    return-void
.end method
