.class final Lcom/google/android/gms/car/ch;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/a/p;


# instance fields
.field final synthetic a:Lcom/google/android/gms/car/br;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/br;)V
    .locals 0

    .prologue
    .line 560
    iput-object p1, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/br;B)V
    .locals 0

    .prologue
    .line 560
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/ch;-><init>(Lcom/google/android/gms/car/br;)V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 568
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 569
    const-string v0, "CAR.BT"

    const-string v1, "onEnabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 571
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x68

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/ci;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ci;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 578
    return-void
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 582
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 583
    const-string v0, "CAR.BT"

    const-string v1, "onDisabled"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 585
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/a/g;)Lcom/google/android/gms/car/a/g;

    .line 586
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x190

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 587
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/cj;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/cj;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 593
    return-void
.end method

.method public final c()V
    .locals 2

    .prologue
    .line 597
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 598
    const-string v0, "CAR.BT"

    const-string v1, "onPaired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 600
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x12c

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 601
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/ck;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/ck;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 607
    return-void
.end method

.method public final d()V
    .locals 2

    .prologue
    .line 611
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 612
    const-string v0, "CAR.BT"

    const-string v1, "onUnpaired"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 614
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x191

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 615
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/cl;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/cl;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 621
    return-void
.end method

.method public final e()V
    .locals 2

    .prologue
    .line 625
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 626
    const-string v0, "CAR.BT"

    const-string v1, "onHfpConnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 628
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x12d

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 629
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/cm;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/cm;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 635
    return-void
.end method

.method public final f()V
    .locals 2

    .prologue
    .line 639
    const-string v0, "CAR.BT"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 640
    const-string v0, "CAR.BT"

    const-string v1, "onHfpDisconnected"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    invoke-static {v0}, Lcom/google/android/gms/car/br;->c(Lcom/google/android/gms/car/br;)Lcom/google/android/gms/car/a/a;

    move-result-object v0

    const/16 v1, 0x192

    invoke-virtual {v0, v1}, Lcom/google/android/gms/car/a/a;->a(I)V

    .line 643
    iget-object v0, p0, Lcom/google/android/gms/car/ch;->a:Lcom/google/android/gms/car/br;

    new-instance v1, Lcom/google/android/gms/car/cn;

    invoke-direct {v1, p0}, Lcom/google/android/gms/car/cn;-><init>(Lcom/google/android/gms/car/ch;)V

    invoke-static {v0, v1}, Lcom/google/android/gms/car/br;->a(Lcom/google/android/gms/car/br;Lcom/google/android/gms/car/co;)V

    .line 649
    return-void
.end method
