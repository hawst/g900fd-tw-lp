.class public final Lcom/google/android/gms/icing/impl/bj;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:Lcom/google/android/gms/icing/impl/bk;

.field final b:Landroid/content/SharedPreferences;

.field final c:I

.field d:Ljava/util/Map;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/icing/impl/bk;Landroid/content/SharedPreferences;ILjava/lang/String;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/bj;->e:Landroid/content/Context;

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    .line 64
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    .line 65
    iput p4, p0, Lcom/google/android/gms/icing/impl/bj;->c:I

    .line 66
    iput-object p5, p0, Lcom/google/android/gms/icing/impl/bj;->f:Ljava/lang/String;

    .line 67
    return-void
.end method

.method private a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;
    .locals 2

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bj;->d:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    .line 217
    if-nez v0, :cond_0

    .line 218
    new-instance v0, Lcom/google/android/gms/icing/aw;

    invoke-direct {v0}, Lcom/google/android/gms/icing/aw;-><init>()V

    .line 219
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->d:Ljava/util/Map;

    invoke-interface {v1, p1, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    :cond_0
    return-object v0
.end method

.method private static a(Landroid/content/SharedPreferences;Ljava/util/Map;Ljava/util/Map;)V
    .locals 4

    .prologue
    .line 201
    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 202
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 203
    const-string v2, "gsai-src-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 204
    const/16 v2, 0x9

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 205
    const/4 v3, 0x0

    invoke-interface {p0, v0, v3}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 206
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-interface {p1, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 207
    :cond_1
    const-string v2, "fp-"

    invoke-virtual {v0, v2}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 208
    const/4 v2, 0x3

    invoke-virtual {v0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    .line 209
    const/4 v3, 0x0

    invoke-interface {p0, v0, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 210
    invoke-interface {p2, v2, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 213
    :cond_2
    return-void
.end method

.method private static a(Landroid/content/SharedPreferences;)Z
    .locals 1

    .prologue
    .line 102
    invoke-interface {p0}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->isEmpty()Z

    move-result v0

    return v0
.end method

.method private static b(Landroid/content/SharedPreferences;)Ljava/util/Set;
    .locals 4

    .prologue
    .line 175
    const-string v0, "blockedpackages"

    const/4 v1, 0x0

    invoke-interface {p0, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 176
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 177
    if-eqz v0, :cond_0

    .line 179
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2, v0}, Lorg/json/JSONArray;-><init>(Ljava/lang/String;)V

    .line 180
    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v2}, Lorg/json/JSONArray;->length()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 181
    invoke-virtual {v2, v0}, Lorg/json/JSONArray;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-interface {v1, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 184
    :catch_0
    move-exception v0

    const-string v0, "Upgrade: blocked packages list corrputed"

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;)I

    .line 187
    :cond_0
    return-object v1
.end method

.method private b(Ljava/lang/String;)V
    .locals 5

    .prologue
    .line 225
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 226
    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bj;->e:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v2

    iget-object v2, v2, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    const-string v3, "shared_prefs"

    invoke-direct {v1, v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 227
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".xml"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v1, v3}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 228
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 229
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "deleting old settings: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    .line 230
    invoke-virtual {v2}, Ljava/io/File;->delete()Z

    .line 234
    :goto_0
    return-void

    .line 232
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Does not exist: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/String;)I

    goto :goto_0
.end method

.method private c(Ljava/lang/String;)I
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 275
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->e:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 277
    const/4 v2, 0x0

    :try_start_0
    invoke-virtual {v1, p1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 279
    :goto_0
    return v0

    :catch_0
    move-exception v1

    goto :goto_0
.end method


# virtual methods
.method final a()V
    .locals 8

    .prologue
    .line 107
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    const-string v1, "-version"

    invoke-interface {v0, v1}, Lcom/google/android/gms/icing/impl/bk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    const-string v2, "-appauth"

    invoke-interface {v1, v2}, Lcom/google/android/gms/icing/impl/bk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 109
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    const-string v3, "-blockedpackages"

    invoke-interface {v2, v3}, Lcom/google/android/gms/icing/impl/bk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 111
    iget-object v3, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    const-string v4, "-universalsearch"

    invoke-interface {v3, v4}, Lcom/google/android/gms/icing/impl/bk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 113
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/bj;->a:Lcom/google/android/gms/icing/impl/bk;

    const-string v5, "-resourceparse"

    invoke-interface {v4, v5}, Lcom/google/android/gms/icing/impl/bk;->a(Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v4

    .line 116
    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v3}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-static {v4}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 164
    :goto_0
    return-void

    .line 125
    :cond_0
    const-string v5, "Upgrading settings"

    invoke-static {v5}, Lcom/google/android/gms/icing/c;->c(Ljava/lang/String;)I

    .line 127
    const-string v5, "version"

    const/4 v6, -0x1

    invoke-interface {v0, v5, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 128
    invoke-interface {v1}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v1

    .line 129
    invoke-static {v2}, Lcom/google/android/gms/icing/impl/bj;->b(Landroid/content/SharedPreferences;)Ljava/util/Set;

    move-result-object v2

    .line 130
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    invoke-interface {v3}, Landroid/content/SharedPreferences;->getAll()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    move-result-object v0

    invoke-interface {v6, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 132
    :cond_1
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    .line 133
    new-instance v7, Ljava/util/HashMap;

    invoke-direct {v7}, Ljava/util/HashMap;-><init>()V

    .line 134
    invoke-static {v4, v3, v7}, Lcom/google/android/gms/icing/impl/bj;->a(Landroid/content/SharedPreferences;Ljava/util/Map;Ljava/util/Map;)V

    .line 137
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/bj;->d:Ljava/util/Map;

    .line 138
    invoke-interface {v1}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 139
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/bj;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/icing/aw;->a:Ljava/lang/String;

    goto :goto_2

    .line 141
    :cond_2
    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 142
    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v0

    const/4 v2, 0x1

    iput-boolean v2, v0, Lcom/google/android/gms/icing/aw;->b:Z

    goto :goto_3

    .line 144
    :cond_3
    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_4
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    .line 145
    invoke-virtual {v0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v2

    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/bj;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v2

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Lcom/google/android/gms/icing/t;

    move-result-object v0

    iput-object v0, v2, Lcom/google/android/gms/icing/aw;->c:Lcom/google/android/gms/icing/t;

    goto :goto_4

    .line 147
    :cond_4
    invoke-interface {v3}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 148
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/bj;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v1, Lcom/google/android/gms/icing/aw;->d:I

    goto :goto_5

    .line 150
    :cond_5
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_6
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 151
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-direct {p0, v1}, Lcom/google/android/gms/icing/impl/bj;->a(Ljava/lang/String;)Lcom/google/android/gms/icing/aw;

    move-result-object v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, v1, Lcom/google/android/gms/icing/aw;->e:Ljava/lang/String;

    goto :goto_6

    .line 155
    :cond_6
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 156
    const-string v0, "index-version"

    invoke-interface {v2, v0, v5}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 157
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/bj;->d:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_7
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 158
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    goto :goto_7

    .line 160
    :cond_7
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 163
    const-string v0, "-version"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->b(Ljava/lang/String;)V

    const-string v0, "-appauth"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->b(Ljava/lang/String;)V

    const-string v0, "-blockedpackages"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->b(Ljava/lang/String;)V

    const-string v0, "-universalsearch"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->b(Ljava/lang/String;)V

    const-string v0, "-resourceparse"

    invoke-direct {p0, v0}, Lcom/google/android/gms/icing/impl/bj;->b(Ljava/lang/String;)V

    goto/16 :goto_0
.end method

.method final b()V
    .locals 5

    .prologue
    .line 237
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 238
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    .line 239
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    .line 240
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 242
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    const/4 v4, 0x1

    iput-boolean v4, v1, Lcom/google/android/gms/icing/aw;->f:Z

    .line 243
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v2, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    goto :goto_0

    .line 245
    :cond_0
    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 246
    return-void
.end method

.method final c()V
    .locals 10

    .prologue
    .line 249
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 250
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 251
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    .line 252
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    .line 253
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v5

    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 254
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    iget-wide v6, v1, Lcom/google/android/gms/icing/aw;->g:J

    const-wide/16 v8, 0x0

    cmp-long v1, v6, v8

    if-nez v1, :cond_0

    .line 255
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    iput-wide v2, v1, Lcom/google/android/gms/icing/aw;->g:J

    .line 257
    :cond_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v4, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    goto :goto_0

    .line 259
    :cond_1
    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 260
    return-void
.end method

.method final d()V
    .locals 5

    .prologue
    .line 263
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 264
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    .line 265
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 266
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 268
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-direct {p0, v2}, Lcom/google/android/gms/icing/impl/bj;->c(Ljava/lang/String;)I

    move-result v2

    iput v2, v1, Lcom/google/android/gms/icing/aw;->i:I

    .line 269
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    goto :goto_0

    .line 271
    :cond_0
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 272
    return-void
.end method

.method final e()V
    .locals 6

    .prologue
    .line 298
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 299
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences;Ljava/util/Map;)V

    .line 300
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/bj;->b:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v3

    .line 301
    invoke-interface {v0}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 304
    :try_start_0
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/bj;->e:Landroid/content/Context;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-static {v5, v2}, Lcom/google/android/gms/common/util/e;->d(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 309
    :goto_1
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/icing/aw;

    invoke-static {v3, v1, v0}, Lcom/google/android/gms/icing/impl/q;->a(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;Lcom/google/android/gms/icing/aw;)Z

    goto :goto_0

    .line 307
    :catch_0
    move-exception v1

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/icing/aw;

    const-string v2, ""

    iput-object v2, v1, Lcom/google/android/gms/icing/aw;->j:Ljava/lang/String;

    goto :goto_1

    .line 311
    :cond_0
    invoke-interface {v3}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 312
    return-void
.end method
