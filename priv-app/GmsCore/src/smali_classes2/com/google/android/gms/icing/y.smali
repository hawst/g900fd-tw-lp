.class public final Lcom/google/android/gms/icing/y;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/y;


# instance fields
.field public a:I

.field public b:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 7893
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 7894
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/y;->a:I

    sget-object v0, Lcom/google/protobuf/nano/m;->f:[Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/y;->cachedSize:I

    .line 7895
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/y;
    .locals 2

    .prologue
    .line 7876
    sget-object v0, Lcom/google/android/gms/icing/y;->c:[Lcom/google/android/gms/icing/y;

    if-nez v0, :cond_1

    .line 7877
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 7879
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/y;->c:[Lcom/google/android/gms/icing/y;

    if-nez v0, :cond_0

    .line 7880
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/y;

    sput-object v0, Lcom/google/android/gms/icing/y;->c:[Lcom/google/android/gms/icing/y;

    .line 7882
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 7884
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/y;->c:[Lcom/google/android/gms/icing/y;

    return-object v0

    .line 7882
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 7951
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 7952
    iget v2, p0, Lcom/google/android/gms/icing/y;->a:I

    if-eqz v2, :cond_0

    .line 7953
    const/4 v2, 0x1

    iget v3, p0, Lcom/google/android/gms/icing/y;->a:I

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v2

    add-int/2addr v0, v2

    .line 7956
    :cond_0
    iget-object v2, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    array-length v2, v2

    if-lez v2, :cond_3

    move v2, v1

    move v3, v1

    .line 7959
    :goto_0
    iget-object v4, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    array-length v4, v4

    if-ge v1, v4, :cond_2

    .line 7960
    iget-object v4, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    aget-object v4, v4, v1

    .line 7961
    if-eqz v4, :cond_1

    .line 7962
    add-int/lit8 v3, v3, 0x1

    .line 7963
    invoke-static {v4}, Lcom/google/protobuf/nano/b;->a(Ljava/lang/String;)I

    move-result v4

    add-int/2addr v2, v4

    .line 7959
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 7967
    :cond_2
    add-int/2addr v0, v2

    .line 7968
    mul-int/lit8 v1, v3, 0x1

    add-int/2addr v0, v1

    .line 7970
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 7906
    if-ne p1, p0, :cond_1

    .line 7920
    :cond_0
    :goto_0
    return v0

    .line 7909
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/y;

    if-nez v2, :cond_2

    move v0, v1

    .line 7910
    goto :goto_0

    .line 7912
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/y;

    .line 7913
    iget v2, p0, Lcom/google/android/gms/icing/y;->a:I

    iget v3, p1, Lcom/google/android/gms/icing/y;->a:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 7914
    goto :goto_0

    .line 7916
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    invoke-static {v2, v3}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    move v0, v1

    .line 7918
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 7925
    iget v0, p0, Lcom/google/android/gms/icing/y;->a:I

    add-int/lit16 v0, v0, 0x20f

    .line 7927
    mul-int/lit8 v0, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    invoke-static {v1}, Lcom/google/protobuf/nano/h;->a([Ljava/lang/Object;)I

    move-result v1

    add-int/2addr v0, v1

    .line 7929
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 7870
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/y;->a:I

    goto :goto_0

    :sswitch_2
    const/16 v0, 0x12

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->b(Lcom/google/protobuf/nano/a;I)I

    move-result v2

    iget-object v0, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    if-nez v0, :cond_2

    move v0, v1

    :goto_1
    add-int/2addr v2, v0

    new-array v2, v2, [Ljava/lang/String;

    if-eqz v0, :cond_1

    iget-object v3, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    invoke-static {v3, v1, v2, v1, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    :cond_1
    :goto_2
    array-length v3, v2

    add-int/lit8 v3, v3, -0x1

    if-ge v0, v3, :cond_3

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    array-length v0, v0

    goto :goto_1

    :cond_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v0

    iput-object v2, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    goto :goto_0

    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x8 -> :sswitch_1
        0x12 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 3

    .prologue
    .line 7935
    iget v0, p0, Lcom/google/android/gms/icing/y;->a:I

    if-eqz v0, :cond_0

    .line 7936
    const/4 v0, 0x1

    iget v1, p0, Lcom/google/android/gms/icing/y;->a:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 7938
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    array-length v0, v0

    if-lez v0, :cond_2

    .line 7939
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    array-length v1, v1

    if-ge v0, v1, :cond_2

    .line 7940
    iget-object v1, p0, Lcom/google/android/gms/icing/y;->b:[Ljava/lang/String;

    aget-object v1, v1, v0

    .line 7941
    if-eqz v1, :cond_1

    .line 7942
    const/4 v2, 0x2

    invoke-virtual {p1, v2, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 7939
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 7946
    :cond_2
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 7947
    return-void
.end method
