.class final Lcom/google/android/gms/common/api/ae;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field private a:Ljava/lang/ref/WeakReference;


# direct methods
.method constructor <init>(Lcom/google/android/gms/common/api/z;)V
    .locals 1

    .prologue
    .line 352
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 353
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p1}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/gms/common/api/ae;->a:Ljava/lang/ref/WeakReference;

    .line 354
    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 358
    invoke-virtual {p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 359
    const/4 v0, 0x0

    .line 360
    if-eqz v1, :cond_0

    .line 361
    invoke-virtual {v1}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v0

    .line 363
    :cond_0
    if-eqz v0, :cond_1

    const-string v1, "com.google.android.gms"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 374
    :cond_1
    :goto_0
    return-void

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/ae;->a:Ljava/lang/ref/WeakReference;

    invoke-virtual {v0}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/z;

    .line 368
    if-eqz v0, :cond_1

    .line 371
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->g()Z

    move-result v1

    if-nez v1, :cond_1

    iget-boolean v1, v0, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v1, :cond_1

    .line 372
    invoke-virtual {v0}, Lcom/google/android/gms/common/api/z;->b()V

    goto :goto_0
.end method
