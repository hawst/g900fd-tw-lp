.class public final Lcom/google/android/gms/cast/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:J

.field public b:Lcom/google/android/gms/cast/g;

.field public c:D

.field public d:I

.field public e:I

.field public f:J

.field public g:Lorg/json/JSONObject;

.field private h:J

.field private i:D

.field private j:Z

.field private k:[J


# direct methods
.method public constructor <init>(Lorg/json/JSONObject;)V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/cast/k;->a(Lorg/json/JSONObject;I)I

    .line 119
    return-void
.end method


# virtual methods
.method public final a(Lorg/json/JSONObject;I)I
    .locals 12

    .prologue
    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 296
    .line 298
    const-string v0, "mediaSessionId"

    invoke-virtual {p1, v0}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    .line 299
    iget-wide v10, p0, Lcom/google/android/gms/cast/k;->a:J

    cmp-long v0, v8, v10

    if-eqz v0, :cond_19

    .line 300
    iput-wide v8, p0, Lcom/google/android/gms/cast/k;->a:J

    move v0, v1

    .line 304
    :goto_0
    const-string v3, "playerState"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 307
    const-string v3, "playerState"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 308
    const-string v7, "IDLE"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_7

    move v3, v1

    .line 318
    :goto_1
    iget v7, p0, Lcom/google/android/gms/cast/k;->d:I

    if-eq v3, v7, :cond_0

    .line 319
    iput v3, p0, Lcom/google/android/gms/cast/k;->d:I

    .line 320
    or-int/lit8 v0, v0, 0x2

    .line 323
    :cond_0
    if-ne v3, v1, :cond_1

    const-string v3, "idleReason"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 326
    const-string v3, "idleReason"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 327
    const-string v7, "CANCELLED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 337
    :goto_2
    iget v3, p0, Lcom/google/android/gms/cast/k;->e:I

    if-eq v4, v3, :cond_1

    .line 338
    iput v4, p0, Lcom/google/android/gms/cast/k;->e:I

    .line 339
    or-int/lit8 v0, v0, 0x2

    .line 344
    :cond_1
    const-string v3, "playbackRate"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 345
    const-string v3, "playbackRate"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 346
    iget-wide v6, p0, Lcom/google/android/gms/cast/k;->c:D

    cmpl-double v3, v6, v4

    if-eqz v3, :cond_2

    .line 347
    iput-wide v4, p0, Lcom/google/android/gms/cast/k;->c:D

    .line 348
    or-int/lit8 v0, v0, 0x2

    .line 352
    :cond_2
    const-string v3, "currentTime"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    and-int/lit8 v3, p2, 0x2

    if-nez v3, :cond_3

    .line 353
    const-string v3, "currentTime"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    const-wide v6, 0x408f400000000000L    # 1000.0

    mul-double/2addr v4, v6

    double-to-long v4, v4

    .line 354
    iget-wide v6, p0, Lcom/google/android/gms/cast/k;->f:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_3

    .line 355
    iput-wide v4, p0, Lcom/google/android/gms/cast/k;->f:J

    .line 356
    or-int/lit8 v0, v0, 0x2

    .line 360
    :cond_3
    const-string v3, "supportedMediaCommands"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 361
    const-string v3, "supportedMediaCommands"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 362
    iget-wide v6, p0, Lcom/google/android/gms/cast/k;->h:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_4

    .line 363
    iput-wide v4, p0, Lcom/google/android/gms/cast/k;->h:J

    .line 364
    or-int/lit8 v0, v0, 0x2

    .line 368
    :cond_4
    const-string v3, "volume"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_6

    and-int/lit8 v3, p2, 0x1

    if-nez v3, :cond_6

    .line 369
    const-string v3, "volume"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v3

    .line 370
    const-string v4, "level"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getDouble(Ljava/lang/String;)D

    move-result-wide v4

    .line 371
    iget-wide v6, p0, Lcom/google/android/gms/cast/k;->i:D

    cmpl-double v6, v4, v6

    if-eqz v6, :cond_5

    .line 372
    iput-wide v4, p0, Lcom/google/android/gms/cast/k;->i:D

    .line 373
    or-int/lit8 v0, v0, 0x2

    .line 375
    :cond_5
    const-string v4, "muted"

    invoke-virtual {v3, v4}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 376
    iget-boolean v4, p0, Lcom/google/android/gms/cast/k;->j:Z

    if-eq v3, v4, :cond_6

    .line 377
    iput-boolean v3, p0, Lcom/google/android/gms/cast/k;->j:Z

    .line 378
    or-int/lit8 v0, v0, 0x2

    .line 383
    :cond_6
    const/4 v3, 0x0

    .line 384
    const-string v4, "activeTrackIds"

    invoke-virtual {p1, v4}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_14

    .line 385
    const-string v3, "activeTrackIds"

    invoke-virtual {p1, v3}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v5

    .line 386
    invoke-virtual {v5}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 387
    new-array v3, v6, [J

    move v4, v2

    .line 388
    :goto_3
    if-ge v4, v6, :cond_d

    .line 389
    invoke-virtual {v5, v4}, Lorg/json/JSONArray;->getLong(I)J

    move-result-wide v8

    aput-wide v8, v3, v4

    .line 388
    add-int/lit8 v4, v4, 0x1

    goto :goto_3

    .line 310
    :cond_7
    const-string v7, "PLAYING"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    move v3, v4

    .line 311
    goto/16 :goto_1

    .line 312
    :cond_8
    const-string v7, "PAUSED"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_9

    move v3, v5

    .line 313
    goto/16 :goto_1

    .line 314
    :cond_9
    const-string v7, "BUFFERING"

    invoke-virtual {v3, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_18

    move v3, v6

    .line 315
    goto/16 :goto_1

    .line 329
    :cond_a
    const-string v4, "INTERRUPTED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_b

    move v4, v5

    .line 330
    goto/16 :goto_2

    .line 331
    :cond_b
    const-string v4, "FINISHED"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    move v4, v1

    .line 332
    goto/16 :goto_2

    .line 333
    :cond_c
    const-string v4, "ERROR"

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_17

    move v4, v6

    .line 334
    goto/16 :goto_2

    .line 391
    :cond_d
    iget-object v4, p0, Lcom/google/android/gms/cast/k;->k:[J

    if-nez v4, :cond_13

    .line 404
    :cond_e
    :goto_4
    if-eqz v1, :cond_f

    .line 405
    iput-object v3, p0, Lcom/google/android/gms/cast/k;->k:[J

    :cond_f
    move v2, v1

    move-object v1, v3

    .line 413
    :goto_5
    if-eqz v2, :cond_10

    .line 414
    iput-object v1, p0, Lcom/google/android/gms/cast/k;->k:[J

    .line 415
    or-int/lit8 v0, v0, 0x2

    .line 418
    :cond_10
    const-string v1, "customData"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_11

    .line 421
    const-string v1, "customData"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/cast/k;->g:Lorg/json/JSONObject;

    .line 422
    or-int/lit8 v0, v0, 0x2

    .line 425
    :cond_11
    const-string v1, "media"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 426
    const-string v1, "media"

    invoke-virtual {p1, v1}, Lorg/json/JSONObject;->getJSONObject(Ljava/lang/String;)Lorg/json/JSONObject;

    move-result-object v1

    .line 428
    new-instance v2, Lcom/google/android/gms/cast/g;

    invoke-direct {v2, v1}, Lcom/google/android/gms/cast/g;-><init>(Lorg/json/JSONObject;)V

    iput-object v2, p0, Lcom/google/android/gms/cast/k;->b:Lcom/google/android/gms/cast/g;

    .line 429
    or-int/lit8 v0, v0, 0x2

    .line 431
    const-string v2, "metadata"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->has(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_12

    .line 432
    or-int/lit8 v0, v0, 0x4

    .line 436
    :cond_12
    return v0

    .line 393
    :cond_13
    iget-object v4, p0, Lcom/google/android/gms/cast/k;->k:[J

    array-length v4, v4

    if-ne v4, v6, :cond_e

    move v4, v2

    .line 396
    :goto_6
    if-ge v4, v6, :cond_16

    .line 397
    iget-object v5, p0, Lcom/google/android/gms/cast/k;->k:[J

    aget-wide v8, v5, v4

    aget-wide v10, v3, v4

    cmp-long v5, v8, v10

    if-nez v5, :cond_e

    .line 396
    add-int/lit8 v4, v4, 0x1

    goto :goto_6

    .line 408
    :cond_14
    iget-object v4, p0, Lcom/google/android/gms/cast/k;->k:[J

    if-eqz v4, :cond_15

    move v2, v1

    move-object v1, v3

    .line 409
    goto :goto_5

    :cond_15
    move-object v1, v3

    goto :goto_5

    :cond_16
    move v1, v2

    goto :goto_4

    :cond_17
    move v4, v2

    goto/16 :goto_2

    :cond_18
    move v3, v2

    goto/16 :goto_1

    :cond_19
    move v0, v2

    goto/16 :goto_0
.end method
