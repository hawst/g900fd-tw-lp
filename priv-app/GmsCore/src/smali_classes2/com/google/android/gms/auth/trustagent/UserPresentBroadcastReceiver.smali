.class public Lcom/google/android/gms/auth/trustagent/UserPresentBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-class v0, Lcom/google/android/gms/auth/trustagent/UserPresentBroadcastReceiver;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/auth/trustagent/UserPresentBroadcastReceiver;->a:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4

    .prologue
    .line 30
    const/16 v0, 0x15

    invoke-static {v0}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    sget-object v0, Lcom/google/android/gms/auth/b/a;->r:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    invoke-virtual {v0}, Ljava/util/Random;->nextFloat()F

    move-result v1

    sget-object v0, Lcom/google/android/gms/auth/b/a;->y:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    cmpg-float v0, v1, v0

    if-gez v0, :cond_0

    .line 33
    sget-object v0, Lcom/google/android/gms/auth/trustagent/UserPresentBroadcastReceiver;->a:Ljava/lang/String;

    const-string v1, "Received %s."

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p2, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 35
    invoke-static {}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->a()Lcom/google/android/gms/auth/trustagent/trustlet/bf;

    move-result-object v1

    .line 36
    new-instance v2, Lcom/google/android/gms/auth/e/f;

    invoke-direct {v2, p1}, Lcom/google/android/gms/auth/e/f;-><init>(Landroid/content/Context;)V

    .line 37
    const/16 v0, 0x63

    iput v0, v2, Lcom/google/android/gms/auth/e/f;->d:I

    .line 38
    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->b()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/auth/e/f;->j:Z

    .line 39
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/auth/e/f;->k:Z

    .line 41
    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/bf;->c()Z

    move-result v0

    iput-boolean v0, v2, Lcom/google/android/gms/auth/e/f;->l:Z

    .line 42
    invoke-virtual {v2}, Lcom/google/android/gms/auth/e/f;->a()V

    .line 44
    :cond_0
    return-void
.end method
