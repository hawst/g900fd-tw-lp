.class public final Lcom/google/android/gms/games/ui/bg;
.super Lcom/google/android/gms/games/ui/b/b;
.source "SourceFile"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1086
    invoke-direct {p0}, Lcom/google/android/gms/games/ui/b/b;-><init>()V

    return-void
.end method

.method public static a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)Lcom/google/android/gms/games/ui/bg;
    .locals 3

    .prologue
    .line 1090
    new-instance v0, Lcom/google/android/gms/games/ui/bg;

    invoke-direct {v0}, Lcom/google/android/gms/games/ui/bg;-><init>()V

    .line 1091
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 1092
    const-string v2, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v1, v2, p0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 1093
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/bg;->setArguments(Landroid/os/Bundle;)V

    .line 1094
    return-object v0
.end method


# virtual methods
.method public final a_(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 1099
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 1100
    sget v1, Lcom/google/android/gms/p;->ly:I

    new-array v2, v5, [Ljava/lang/Object;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/game/ExtendedGame;->a()Lcom/google/android/gms/games/Game;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/games/Game;->s_()Ljava/lang/String;

    move-result-object v0

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/gms/games/ui/bg;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1103
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->lA:I

    invoke-static {v1, v2}, Lcom/google/android/gms/games/ui/d/al;->a(Landroid/content/Context;I)Landroid/view/View;

    move-result-object v1

    .line 1106
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v2, v4}, Landroid/app/AlertDialog$Builder;->setIcon(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setCustomTitle(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    sget v2, Lcom/google/android/gms/p;->lz:I

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x1040000

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0, v5}, Landroid/app/AlertDialog$Builder;->setInverseBackgroundForced(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    return-object v0
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 3

    .prologue
    .line 1118
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 1120
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    .line 1121
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "com.google.android.gms.games.EXTENDED_GAME"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/internal/game/ExtendedGame;

    .line 1122
    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->a(Lcom/google/android/gms/games/internal/game/ExtendedGame;)V

    .line 1124
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/bg;->dismiss()V

    .line 1125
    return-void
.end method
