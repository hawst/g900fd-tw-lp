.class public Lcom/google/android/gms/drive/DrivePreferences;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field final a:I

.field final b:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    new-instance v0, Lcom/google/android/gms/drive/x;

    invoke-direct {v0}, Lcom/google/android/gms/drive/x;-><init>()V

    sput-object v0, Lcom/google/android/gms/drive/DrivePreferences;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(IZ)V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iput p1, p0, Lcom/google/android/gms/drive/DrivePreferences;->a:I

    .line 38
    iput-boolean p2, p0, Lcom/google/android/gms/drive/DrivePreferences;->b:Z

    .line 39
    return-void
.end method

.method public constructor <init>(Z)V
    .locals 1

    .prologue
    .line 45
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/drive/DrivePreferences;-><init>(IZ)V

    .line 46
    return-void
.end method


# virtual methods
.method public final a()Z
    .locals 1

    .prologue
    .line 52
    iget-boolean v0, p0, Lcom/google/android/gms/drive/DrivePreferences;->b:Z

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 62
    invoke-static {p0, p1}, Lcom/google/android/gms/drive/x;->a(Lcom/google/android/gms/drive/DrivePreferences;Landroid/os/Parcel;)V

    .line 63
    return-void
.end method
