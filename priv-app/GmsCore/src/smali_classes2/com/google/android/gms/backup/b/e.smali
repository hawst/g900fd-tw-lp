.class public final Lcom/google/android/gms/backup/b/e;
.super Ljava/io/FilterInputStream;
.source "SourceFile"


# instance fields
.field private final a:Lcom/google/android/gms/drive/b/a/j;

.field private b:Z

.field private c:Ljava/security/MessageDigest;


# direct methods
.method public constructor <init>(Ljava/io/InputStream;Lcom/google/android/gms/drive/b/a/j;Ljava/security/MessageDigest;)V
    .locals 1

    .prologue
    .line 25
    invoke-direct {p0, p1}, Ljava/io/FilterInputStream;-><init>(Ljava/io/InputStream;)V

    .line 20
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/backup/b/e;->a:Lcom/google/android/gms/drive/b/a/j;

    .line 28
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 29
    iput-object p3, p0, Lcom/google/android/gms/backup/b/e;->c:Ljava/security/MessageDigest;

    .line 30
    iget-object v0, p0, Lcom/google/android/gms/backup/b/e;->c:Ljava/security/MessageDigest;

    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 31
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/b/a/i;
    .locals 1

    .prologue
    .line 91
    iget-boolean v0, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    if-eqz v0, :cond_0

    .line 92
    iget-object v0, p0, Lcom/google/android/gms/backup/b/e;->a:Lcom/google/android/gms/drive/b/a/j;

    invoke-virtual {v0}, Lcom/google/android/gms/drive/b/a/j;->a()Lcom/google/android/gms/drive/b/a/i;

    move-result-object v0

    .line 94
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final close()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Ljava/io/FilterInputStream;->close()V

    .line 86
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    .line 87
    return-void
.end method

.method public final markSupported()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x0

    return v0
.end method

.method public final read()I
    .locals 3

    .prologue
    .line 35
    invoke-super {p0}, Ljava/io/FilterInputStream;->read()I

    move-result v0

    .line 36
    if-ltz v0, :cond_0

    .line 37
    iget-object v1, p0, Lcom/google/android/gms/backup/b/e;->a:Lcom/google/android/gms/drive/b/a/j;

    int-to-byte v2, v0

    invoke-virtual {v1, v2}, Lcom/google/android/gms/drive/b/a/j;->a(B)Lcom/google/android/gms/drive/b/a/j;

    .line 38
    iget-object v1, p0, Lcom/google/android/gms/backup/b/e;->c:Ljava/security/MessageDigest;

    int-to-byte v2, v0

    invoke-virtual {v1, v2}, Ljava/security/MessageDigest;->update(B)V

    .line 42
    :goto_0
    return v0

    .line 40
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    goto :goto_0
.end method

.method public final read([B)I
    .locals 2

    .prologue
    .line 47
    const/4 v0, 0x0

    array-length v1, p1

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/backup/b/e;->read([BII)I

    move-result v0

    return v0
.end method

.method public final read([BII)I
    .locals 2

    .prologue
    .line 54
    invoke-super {p0, p1, p2, p3}, Ljava/io/FilterInputStream;->read([BII)I

    move-result v0

    .line 55
    if-lez v0, :cond_0

    .line 56
    iget-object v1, p0, Lcom/google/android/gms/backup/b/e;->a:Lcom/google/android/gms/drive/b/a/j;

    invoke-virtual {v1, p1, p2, v0}, Lcom/google/android/gms/drive/b/a/j;->a([BII)Lcom/google/android/gms/drive/b/a/j;

    .line 57
    iget-object v1, p0, Lcom/google/android/gms/backup/b/e;->c:Ljava/security/MessageDigest;

    invoke-virtual {v1, p1, p2, v0}, Ljava/security/MessageDigest;->update([BII)V

    .line 61
    :goto_0
    return v0

    .line 59
    :cond_0
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    goto :goto_0
.end method

.method public final skip(J)J
    .locals 5

    .prologue
    .line 71
    const-wide/16 v0, 0x0

    .line 72
    :goto_0
    cmp-long v2, p1, v0

    if-lez v2, :cond_0

    .line 73
    invoke-virtual {p0}, Lcom/google/android/gms/backup/b/e;->read()I

    move-result v2

    .line 74
    if-gez v2, :cond_1

    .line 75
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/backup/b/e;->b:Z

    .line 80
    :cond_0
    return-wide v0

    .line 78
    :cond_1
    const-wide/16 v2, 0x1

    add-long/2addr v0, v2

    .line 79
    goto :goto_0
.end method
