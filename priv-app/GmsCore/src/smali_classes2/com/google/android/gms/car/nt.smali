.class public final Lcom/google/android/gms/car/nt;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/as;


# instance fields
.field a:[Lcom/google/android/gms/car/CarAudioConfiguration;

.field b:I

.field volatile c:Lcom/google/android/gms/car/senderprotocol/ar;

.field final d:Ljava/util/List;

.field volatile e:I

.field volatile f:Z

.field private volatile g:Z

.field private final h:Landroid/os/Handler;

.field private final i:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    iput-boolean v2, p0, Lcom/google/android/gms/car/nt;->g:Z

    .line 28
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    .line 30
    iput v2, p0, Lcom/google/android/gms/car/nt;->e:I

    .line 32
    new-instance v0, Lcom/google/android/gms/car/nv;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/nv;-><init>(Lcom/google/android/gms/car/nt;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/nt;->h:Landroid/os/Handler;

    .line 34
    new-instance v0, Lcom/google/android/gms/car/nu;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/nu;-><init>(Lcom/google/android/gms/car/nt;)V

    iput-object v0, p0, Lcom/google/android/gms/car/nt;->i:Ljava/lang/Runnable;

    .line 47
    iput-boolean v2, p0, Lcom/google/android/gms/car/nt;->f:Z

    .line 176
    return-void
.end method


# virtual methods
.method final a()V
    .locals 4

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/car/nt;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/nt;->i:Ljava/lang/Runnable;

    const-wide/16 v2, 0x7d0

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 136
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 1

    .prologue
    .line 116
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/ar;

    iput-object p1, p0, Lcom/google/android/gms/car/nt;->c:Lcom/google/android/gms/car/senderprotocol/ar;

    .line 117
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/nt;->g:Z

    .line 118
    return-void
.end method

.method public final a(Ljava/io/PrintWriter;)V
    .locals 4

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "discovered:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p0, Lcom/google/android/gms/car/nt;->g:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " mic opened:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/gms/car/nt;->f:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 156
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "stream type:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/nt;->b:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 157
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "num clients:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v1, p0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 159
    const-string v0, "*** Audio Configs***"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/car/nt;->a:[Lcom/google/android/gms/car/CarAudioConfiguration;

    .line 161
    if-eqz v0, :cond_1

    .line 162
    iget-object v1, p0, Lcom/google/android/gms/car/nt;->a:[Lcom/google/android/gms/car/CarAudioConfiguration;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_2

    aget-object v3, v1, v0

    .line 163
    if-eqz v3, :cond_0

    .line 164
    invoke-virtual {v3}, Lcom/google/android/gms/car/CarAudioConfiguration;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 162
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 166
    :cond_0
    const-string v3, "null config"

    invoke-virtual {p1, v3}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    goto :goto_1

    .line 170
    :cond_1
    const-string v0, "null configs"

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 172
    :cond_2
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "frames received "

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget v1, p0, Lcom/google/android/gms/car/nt;->e:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p1, v0}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 174
    return-void
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/car/nt;->c()V

    .line 104
    iget v0, p0, Lcom/google/android/gms/car/nt;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/nt;->e:I

    .line 105
    invoke-virtual {p0}, Lcom/google/android/gms/car/nt;->b()V

    .line 106
    iget-object v1, p0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    monitor-enter v1

    .line 107
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/nt;->d:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/bf;

    .line 108
    invoke-virtual {v0, p1}, Lcom/google/android/gms/car/bf;->a(Ljava/nio/ByteBuffer;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    :cond_0
    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 111
    invoke-virtual {p0}, Lcom/google/android/gms/car/nt;->a()V

    .line 112
    return-void
.end method

.method final b()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/gms/car/nt;->h:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/gms/car/nt;->i:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 140
    return-void
.end method

.method final c()V
    .locals 2

    .prologue
    .line 147
    iget-boolean v0, p0, Lcom/google/android/gms/car/nt;->g:Z

    if-nez v0, :cond_0

    .line 148
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attempted to use MicrophoneInputService before service discovery."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 151
    :cond_0
    return-void
.end method
