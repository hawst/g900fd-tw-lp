.class public abstract Lcom/google/android/gms/games/ui/headless/a;
.super Lcom/google/android/gms/games/ui/q;
.source "SourceFile"


# instance fields
.field private i:Ljava/lang/String;

.field private final j:Z


# direct methods
.method public constructor <init>(II)V
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZ)V

    .line 61
    return-void
.end method

.method public constructor <init>(IIZ)V
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/games/ui/headless/a;-><init>(IIZZ)V

    .line 80
    return-void
.end method

.method public constructor <init>(IIZZ)V
    .locals 6

    .prologue
    .line 98
    const/4 v1, 0x2

    const/4 v2, 0x0

    move-object v0, p0

    move v3, p1

    move v4, p2

    move v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/ui/q;-><init>(IIIIZ)V

    .line 101
    iput-boolean p3, p0, Lcom/google/android/gms/games/ui/headless/a;->j:Z

    .line 102
    return-void
.end method


# virtual methods
.method public K()V
    .locals 2

    .prologue
    .line 106
    invoke-super {p0}, Lcom/google/android/gms/games/ui/q;->K()V

    .line 108
    iget-boolean v0, p0, Lcom/google/android/gms/games/ui/headless/a;->j:Z

    if-eqz v0, :cond_0

    .line 112
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/a;->f()V

    .line 118
    invoke-super {p0}, Landroid/support/v7/app/d;->d()Landroid/support/v7/app/e;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/app/e;->b()Landroid/support/v7/app/a;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v7/app/a;->a(Z)V

    .line 120
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 200
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 2

    .prologue
    const/16 v1, 0x4e21

    .line 153
    const/16 v0, 0x384

    if-ne p1, v0, :cond_0

    if-ne p2, v1, :cond_0

    .line 157
    invoke-virtual {p0, v1}, Lcom/google/android/gms/games/ui/headless/a;->setResult(I)V

    .line 158
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->finish()V

    .line 160
    :cond_0
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/gms/games/ui/q;->onActivityResult(IILandroid/content/Intent;)V

    .line 161
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 168
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v1

    .line 169
    const v2, 0x102002c

    if-ne v1, v2, :cond_0

    .line 176
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->onBackPressed()V

    .line 182
    :goto_0
    return v0

    .line 178
    :cond_0
    sget v2, Lcom/google/android/gms/j;->lX:I

    if-ne v1, v2, :cond_1

    .line 179
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->t()V

    goto :goto_0

    .line 182
    :cond_1
    invoke-super {p0, p1}, Lcom/google/android/gms/games/ui/q;->onOptionsItemSelected(Landroid/view/MenuItem;)Z

    move-result v0

    goto :goto_0
.end method

.method protected final r()Lcom/google/android/gms/common/api/v;
    .locals 3

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/a;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/headless/a;->i:Ljava/lang/String;

    .line 127
    iget-object v0, p0, Lcom/google/android/gms/games/ui/headless/a;->i:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 128
    const-string v0, "HeadlessFragment"

    const-string v1, "Account name not found in the Intent! Bailing!"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 131
    const/4 v0, 0x0

    .line 144
    :goto_0
    return-object v0

    .line 135
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->x()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 136
    invoke-virtual {p0}, Lcom/google/android/gms/games/ui/headless/a;->w()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 141
    :cond_1
    invoke-static {}, Lcom/google/android/gms/games/h;->a()Lcom/google/android/gms/games/i;

    move-result-object v0

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/google/android/gms/games/i;->a:Z

    invoke-virtual {v0}, Lcom/google/android/gms/games/i;->a()Lcom/google/android/gms/games/h;

    move-result-object v0

    .line 144
    new-instance v1, Lcom/google/android/gms/common/api/w;

    invoke-direct {v1, p0, p0, p0}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)V

    sget-object v2, Lcom/google/android/gms/games/d;->e:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;Lcom/google/android/gms/common/api/e;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/games/ui/headless/a;->i:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->a:Ljava/lang/String;

    const-string v1, "com.google.android.gms"

    iput-object v1, v0, Lcom/google/android/gms/common/api/w;->b:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    goto :goto_0
.end method

.method public final t()V
    .locals 3

    .prologue
    .line 190
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.gms.games.SHOW_GOOGLE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 191
    const-string v1, "com.google.android.gms.games.ACCOUNT_NAME"

    iget-object v2, p0, Lcom/google/android/gms/games/ui/headless/a;->i:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 192
    const-string v1, "com.google.android.play.games"

    invoke-static {p0, v1}, Lcom/google/android/gms/games/ui/d/aa;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 194
    const-string v2, "com.google.android.gms.games.DEST_APP_VERSION"

    invoke-virtual {v0, v2, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 195
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/headless/a;->startActivity(Landroid/content/Intent;)V

    .line 196
    return-void
.end method
