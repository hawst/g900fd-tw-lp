.class public final Lcom/google/android/gms/ads/social/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/accounts/AccountManager;

.field private c:Landroid/content/SharedPreferences;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->b()Lcom/google/android/gms/common/app/GmsApplication;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/a;->a:Landroid/content/Context;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/ads/social/a;->a:Landroid/content/Context;

    invoke-static {v0}, Landroid/accounts/AccountManager;->get(Landroid/content/Context;)Landroid/accounts/AccountManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/social/a;->b:Landroid/accounts/AccountManager;

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/ads/social/a;->a:Landroid/content/Context;

    const/16 v1, 0xb

    invoke-static {v1}, Lcom/google/android/gms/common/util/ao;->a(I)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "social.account_doritos"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/ads/social/a;->c:Landroid/content/SharedPreferences;

    .line 55
    return-void

    .line 54
    :cond_0
    const-string v1, "social.account_doritos"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(Ljava/util/Map;)Ljava/lang/String;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 83
    .line 84
    invoke-interface {p0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 85
    const-string v3, "Set-Cookie"

    invoke-virtual {v3, v0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 86
    invoke-interface {p0, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 91
    :goto_0
    if-eqz v0, :cond_2

    .line 93
    invoke-interface {v0}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 94
    if-eqz v0, :cond_1

    const-string v3, "_drt_"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 101
    :goto_1
    return-object v0

    :cond_2
    move-object v0, v1

    goto :goto_1

    :cond_3
    move-object v0, v1

    goto :goto_0
.end method

.method private a(Ljava/lang/String;Lcom/google/android/gms/ads/social/b;Ljava/net/URL;)Z
    .locals 9

    .prologue
    const/16 v8, 0x191

    const/4 v1, 0x0

    .line 139
    new-instance v3, Lcom/google/android/gms/http/e;

    iget-object v0, p0, Lcom/google/android/gms/ads/social/a;->a:Landroid/content/Context;

    invoke-direct {v3, v0}, Lcom/google/android/gms/http/e;-><init>(Landroid/content/Context;)V

    move v2, v1

    .line 145
    :goto_0
    :try_start_0
    sget-object v0, Lcom/google/android/gms/ads/a/f;->f:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iget-object v4, p2, Lcom/google/android/gms/ads/social/b;->a:Landroid/content/Context;

    invoke-static {v4, p1, v0}, Lcom/google/android/gms/auth/r;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 155
    if-nez v0, :cond_0

    move v0, v1

    .line 198
    :goto_1
    return v0

    .line 151
    :catch_0
    move-exception v0

    .line 152
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to get auth token for DRT: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    move v0, v1

    .line 153
    goto :goto_1

    .line 161
    :cond_0
    :try_start_1
    invoke-virtual {v3, p3}, Lcom/google/android/gms/http/e;->a(Ljava/net/URL;)Ljava/net/HttpURLConnection;

    move-result-object v4

    .line 162
    if-nez v4, :cond_1

    move v0, v1

    .line 163
    goto :goto_1

    .line 165
    :cond_1
    const-string v5, "Authorization"

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "Bearer "

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Ljava/net/HttpURLConnection;->setRequestProperty(Ljava/lang/String;Ljava/lang/String;)V

    .line 166
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getResponseCode()I

    move-result v5

    .line 167
    invoke-virtual {v3, v4, v5}, Lcom/google/android/gms/http/e;->a(Ljava/net/HttpURLConnection;I)V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 176
    if-ne v5, v8, :cond_2

    .line 178
    :try_start_2
    iget-object v6, p2, Lcom/google/android/gms/ads/social/b;->a:Landroid/content/Context;

    invoke-static {v6, v0}, Lcom/google/android/gms/auth/r;->b(Landroid/content/Context;Ljava/lang/String;)V
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_2

    .line 184
    :cond_2
    if-ne v5, v8, :cond_3

    add-int/lit8 v0, v2, 0x1

    if-eqz v2, :cond_5

    .line 189
    :cond_3
    const/16 v0, 0xc8

    if-ne v5, v0, :cond_4

    .line 190
    invoke-virtual {v4}, Ljava/net/HttpURLConnection;->getHeaderFields()Ljava/util/Map;

    move-result-object v0

    .line 191
    if-eqz v0, :cond_4

    .line 192
    invoke-static {v0}, Lcom/google/android/gms/ads/social/a;->a(Ljava/util/Map;)Ljava/lang/String;

    move-result-object v0

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/ads/social/a;->c:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1, p1, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    const-string v0, "Saved DRT."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 194
    const/4 v0, 0x1

    goto :goto_1

    .line 170
    :catch_1
    move-exception v0

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Failed to connect to "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ". No DRT retrieved."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->b(Ljava/lang/String;)V

    move v0, v1

    .line 171
    goto/16 :goto_1

    .line 179
    :catch_2
    move-exception v0

    .line 180
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Failed to clear auth token for DRT: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    move v0, v1

    .line 181
    goto/16 :goto_1

    :cond_4
    move v0, v1

    .line 198
    goto/16 :goto_1

    :cond_5
    move v2, v0

    goto/16 :goto_0
.end method


# virtual methods
.method final a(Ljava/lang/String;Lcom/google/android/gms/ads/social/b;)Z
    .locals 2

    .prologue
    .line 121
    :try_start_0
    new-instance v1, Ljava/net/URL;

    sget-object v0, Lcom/google/android/gms/ads/a/f;->a:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, v0}, Ljava/net/URL;-><init>(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    .line 129
    invoke-direct {p0, p1, p2, v1}, Lcom/google/android/gms/ads/social/a;->a(Ljava/lang/String;Lcom/google/android/gms/ads/social/b;Ljava/net/URL;)Z

    move-result v0

    :goto_0
    return v0

    .line 126
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
