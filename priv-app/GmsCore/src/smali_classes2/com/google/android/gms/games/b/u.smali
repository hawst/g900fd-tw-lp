.class public abstract Lcom/google/android/gms/games/b/u;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/support/v4/g/h;

.field protected final b:Ljava/lang/String;

.field public c:Z

.field private final d:[Ljava/lang/String;

.field private final e:J

.field private final f:Ljava/lang/String;

.field private g:Z

.field private h:Ljava/lang/Object;


# direct methods
.method protected constructor <init>([Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 8

    .prologue
    .line 139
    const/4 v5, 0x1

    move-object v0, p0

    move-object v1, p1

    move-wide v2, p2

    move-object v4, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/b/u;-><init>([Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;)V

    .line 140
    return-void
.end method

.method protected constructor <init>([Ljava/lang/String;JLjava/lang/String;ZLjava/lang/String;)V
    .locals 2

    .prologue
    .line 159
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 160
    iput-object p1, p0, Lcom/google/android/gms/games/b/u;->d:[Ljava/lang/String;

    .line 161
    new-instance v0, Landroid/support/v4/g/h;

    invoke-virtual {p0}, Lcom/google/android/gms/games/b/u;->b()I

    move-result v1

    invoke-direct {v0, v1}, Landroid/support/v4/g/h;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    .line 162
    iput-wide p2, p0, Lcom/google/android/gms/games/b/u;->e:J

    .line 163
    iput-object p4, p0, Lcom/google/android/gms/games/b/u;->f:Ljava/lang/String;

    .line 164
    iput-boolean p5, p0, Lcom/google/android/gms/games/b/u;->g:Z

    .line 165
    iput-object p6, p0, Lcom/google/android/gms/games/b/u;->b:Ljava/lang/String;

    .line 166
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/b/u;->h:Ljava/lang/Object;

    .line 167
    sget-object v0, Lcom/google/android/gms/games/c/a;->h:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->b()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/games/b/u;->c:Z

    .line 168
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/Object;Landroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;
    .locals 5

    .prologue
    .line 309
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 310
    if-nez v0, :cond_0

    .line 311
    const/4 v0, 0x4

    invoke-static {v0, p2}, Lcom/google/android/gms/common/data/DataHolder;->a(ILandroid/os/Bundle;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    .line 351
    :goto_0
    return-object v0

    .line 314
    :cond_0
    iget-object v1, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    .line 315
    if-nez p2, :cond_1

    .line 316
    new-instance p2, Landroid/os/Bundle;

    invoke-direct {p2}, Landroid/os/Bundle;-><init>()V

    .line 320
    :cond_1
    iget-object v2, v0, Lcom/google/android/gms/games/b/v;->c:Ljava/lang/String;

    if-eqz v2, :cond_2

    .line 321
    iget-object v2, v0, Lcom/google/android/gms/games/b/v;->c:Ljava/lang/String;

    iget v3, v0, Lcom/google/android/gms/games/b/v;->d:I

    invoke-virtual {p2, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 324
    :cond_2
    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->f:Ljava/lang/String;

    if-eqz v2, :cond_3

    .line 325
    iget-boolean v2, p0, Lcom/google/android/gms/games/b/u;->g:Z

    if-eqz v2, :cond_6

    .line 326
    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/data/ai;->a(Ljava/lang/String;)V

    .line 333
    :cond_3
    :goto_1
    iget v2, v0, Lcom/google/android/gms/games/b/v;->b:I

    invoke-virtual {v1, v2, p2, p3}, Lcom/google/android/gms/common/data/ai;->a(ILandroid/os/Bundle;I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    .line 334
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->g()Landroid/os/Bundle;

    move-result-object v2

    .line 341
    iget-object v3, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v3

    .line 342
    invoke-virtual {v1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v4

    .line 343
    if-le v3, v4, :cond_5

    .line 344
    iget-boolean v3, v0, Lcom/google/android/gms/games/b/v;->g:Z

    if-eqz v3, :cond_4

    const-string v3, "next_page_token"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    if-nez v3, :cond_4

    .line 345
    const-string v3, "next_page_token"

    const-string v4, "has_local_data"

    invoke-virtual {v2, v3, v4}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 347
    :cond_4
    iget-boolean v0, v0, Lcom/google/android/gms/games/b/v;->h:Z

    if-eqz v0, :cond_5

    const-string v0, "prev_page_token"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    if-nez v0, :cond_5

    .line 348
    const-string v0, "prev_page_token"

    const-string v3, "has_local_data"

    invoke-virtual {v2, v0, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    :cond_5
    move-object v0, v1

    .line 351
    goto :goto_0

    .line 328
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->f:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/common/data/ai;->b(Ljava/lang/String;)V

    goto :goto_1
.end method

.method protected abstract a()Ljava/lang/String;
.end method

.method public final a(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 177
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/b/u;->b(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->b()V

    .line 180
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/b/u;->h:Ljava/lang/Object;

    .line 181
    return-void
.end method

.method public final a(Ljava/lang/Object;I)V
    .locals 1

    .prologue
    .line 547
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 548
    if-eqz v0, :cond_0

    .line 549
    iput p2, v0, Lcom/google/android/gms/games/b/v;->b:I

    .line 551
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 576
    if-eqz v0, :cond_0

    .line 577
    iput-object p2, v0, Lcom/google/android/gms/games/b/v;->c:Ljava/lang/String;

    .line 578
    iput p3, v0, Lcom/google/android/gms/games/b/v;->d:I

    .line 580
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJ)V
    .locals 13

    .prologue
    .line 461
    iget-wide v10, p0, Lcom/google/android/gms/games/b/u;->e:J

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move-wide/from16 v8, p7

    invoke-virtual/range {v1 .. v11}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V

    .line 463
    return-void
.end method

.method public final a(Ljava/lang/Object;Ljava/util/ArrayList;ILjava/lang/String;Ljava/lang/String;IJJ)V
    .locals 11

    .prologue
    .line 484
    move-wide/from16 v0, p7

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;J)Z

    move-result v10

    .line 485
    if-nez v10, :cond_1

    .line 486
    iget-boolean v2, p0, Lcom/google/android/gms/games/b/u;->c:Z

    if-eqz v2, :cond_0

    .line 487
    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v2, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/games/b/v;

    .line 488
    if-eqz v2, :cond_0

    iget-object v3, v2, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    if-eqz v3, :cond_0

    iget-object v2, v2, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v2

    if-lez v2, :cond_0

    .line 489
    const-string v2, "TransientDataCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Expired; clearing data for key "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 492
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/b/u;->c(Ljava/lang/Object;)V

    .line 496
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/b/u;->d(Ljava/lang/Object;)Lcom/google/android/gms/games/b/v;

    move-result-object v3

    if-nez v3, :cond_2

    new-instance v4, Lcom/google/android/gms/common/data/ai;

    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->d:[Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/b/u;->b:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-direct {v4, v2, v3, p4, v0}, Lcom/google/android/gms/common/data/ai;-><init>([Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    new-instance v3, Lcom/google/android/gms/games/b/v;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    move-wide/from16 v6, p7

    move-wide/from16 v8, p9

    invoke-direct/range {v3 .. v9}, Lcom/google/android/gms/games/b/v;-><init>(Lcom/google/android/gms/common/data/ai;Ljava/lang/Integer;JJ)V

    .line 498
    :goto_0
    const/4 v2, 0x0

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v5

    move v4, v2

    :goto_1
    if-ge v4, v5, :cond_3

    .line 499
    iget-object v6, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {p2, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/content/ContentValues;

    invoke-virtual {v6, v2}, Lcom/google/android/gms/common/data/ai;->a(Landroid/content/ContentValues;)V

    .line 498
    add-int/lit8 v2, v4, 0x1

    move v4, v2

    goto :goto_1

    .line 496
    :cond_2
    iput p3, v3, Lcom/google/android/gms/games/b/v;->b:I

    move-wide/from16 v0, p7

    iput-wide v0, v3, Lcom/google/android/gms/games/b/v;->e:J

    goto :goto_0

    .line 504
    :cond_3
    packed-switch p6, :pswitch_data_0

    .line 531
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid page direction "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move/from16 v0, p6

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 506
    :pswitch_0
    if-eqz v10, :cond_4

    if-eqz p5, :cond_4

    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v2, v2, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 509
    const-string v2, "TransientDataCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got a new response with same next page token - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p5

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    const/16 p5, 0x0

    .line 512
    :cond_4
    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    move-object/from16 v0, p5

    iput-object v0, v2, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    .line 513
    iget-boolean v4, v3, Lcom/google/android/gms/games/b/v;->g:Z

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_6

    const/4 v2, 0x1

    :goto_2
    or-int/2addr v2, v4

    iput-boolean v2, v3, Lcom/google/android/gms/games/b/v;->g:Z

    .line 533
    :goto_3
    iget-object v2, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v2, p1, v3}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 534
    iget-boolean v2, p0, Lcom/google/android/gms/games/b/u;->c:Z

    if-eqz v2, :cond_5

    .line 535
    invoke-virtual {p1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, v2, v3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/String;Lcom/google/android/gms/games/b/v;)V

    .line 537
    :cond_5
    return-void

    .line 513
    :cond_6
    const/4 v2, 0x0

    goto :goto_2

    .line 516
    :pswitch_1
    if-eqz v10, :cond_7

    if-eqz p4, :cond_7

    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v2, v2, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    invoke-virtual {p4, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 519
    const-string v2, "TransientDataCache"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Got a new response with same prev page token - "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, p4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 520
    const/4 p4, 0x0

    .line 522
    :cond_7
    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iput-object p4, v2, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    .line 523
    iget-boolean v4, v3, Lcom/google/android/gms/games/b/v;->h:Z

    invoke-virtual {p2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-lez v2, :cond_8

    const/4 v2, 0x1

    :goto_4
    or-int/2addr v2, v4

    iput-boolean v2, v3, Lcom/google/android/gms/games/b/v;->h:Z

    goto :goto_3

    :cond_8
    const/4 v2, 0x0

    goto :goto_4

    .line 527
    :pswitch_2
    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    move-object/from16 v0, p5

    iput-object v0, v2, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    .line 528
    iget-object v2, v3, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iput-object p4, v2, Lcom/google/android/gms/common/data/ai;->a:Ljava/lang/String;

    goto :goto_3

    .line 504
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final a(Ljava/lang/String;Lcom/google/android/gms/games/b/v;)V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 651
    invoke-virtual {p0}, Lcom/google/android/gms/games/b/u;->a()Ljava/lang/String;

    move-result-object v1

    .line 652
    if-nez v1, :cond_0

    .line 664
    :goto_0
    return-void

    .line 655
    :cond_0
    const-string v2, "TransientDataCache"

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "*** Emitting cache entry for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " ***"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 656
    iget-object v2, p2, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/ai;->a(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v2

    .line 658
    :try_start_0
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v3

    :goto_1
    if-ge v0, v3, :cond_1

    .line 659
    const-string v4, "TransientDataCache"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    add-int/lit8 v6, v0, 0x1

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ". "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v2, v0}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v6

    invoke-virtual {v2, v1, v0, v6}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 658
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 663
    :cond_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/DataHolder;->j()V

    throw v0
.end method

.method public final a(Ljava/lang/Object;J)Z
    .locals 6

    .prologue
    const/4 v1, 0x0

    .line 203
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 204
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v2}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v2

    if-nez v2, :cond_1

    :cond_0
    move v0, v1

    .line 210
    :goto_0
    return v0

    .line 209
    :cond_1
    iget-wide v2, v0, Lcom/google/android/gms/games/b/v;->e:J

    sub-long v2, p2, v2

    .line 210
    iget-wide v4, v0, Lcom/google/android/gms/games/b/v;->f:J

    cmp-long v0, v2, v4

    if-gtz v0, :cond_2

    const/4 v0, 0x1

    goto :goto_0

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;JIZ)Z
    .locals 4

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 232
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;J)Z

    move-result v0

    if-nez v0, :cond_0

    move v0, v1

    .line 245
    :goto_0
    return v0

    .line 236
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 237
    if-eqz p5, :cond_2

    .line 240
    iget-object v3, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v3

    if-lez v3, :cond_1

    iget-object v0, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v0, v0, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    if-nez v0, :cond_1

    move v0, v2

    goto :goto_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 245
    :cond_2
    iget-object v3, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    invoke-virtual {v3}, Lcom/google/android/gms/common/data/ai;->a()I

    move-result v3

    if-ge v3, p4, :cond_3

    iget-object v0, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v0, v0, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    if-nez v0, :cond_4

    :cond_3
    move v0, v2

    goto :goto_0

    :cond_4
    move v0, v1

    goto :goto_0
.end method

.method public final a(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 420
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 421
    if-nez v0, :cond_0

    move v0, v1

    .line 425
    :goto_0
    return v0

    :cond_0
    iget-object v0, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v3, v0, Lcom/google/android/gms/common/data/ai;->c:Lcom/google/android/gms/common/data/m;

    iget-object v0, v3, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v4

    move v2, v1

    :goto_1
    if-ge v2, v4, :cond_2

    iget-object v0, v3, Lcom/google/android/gms/common/data/m;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/HashMap;

    invoke-virtual {v0, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    invoke-static {v0, p3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    goto :goto_0

    :cond_1
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_1

    :cond_2
    move v0, v1

    goto :goto_0
.end method

.method protected b()I
    .locals 1

    .prologue
    .line 633
    const/16 v0, 0x64

    return v0
.end method

.method public final b(Ljava/lang/Object;J)Ljava/lang/String;
    .locals 2

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    .line 377
    if-eqz v0, :cond_0

    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/gms/games/b/u;->a(Ljava/lang/Object;J)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    iget-object v0, v0, Lcom/google/android/gms/games/b/v;->a:Lcom/google/android/gms/common/data/ai;

    iget-object v0, v0, Lcom/google/android/gms/common/data/ai;->b:Ljava/lang/String;

    .line 380
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 190
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->h:Ljava/lang/Object;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 586
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0}, Landroid/support/v4/g/h;->c()Ljava/util/Map;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v0

    return-object v0
.end method

.method public final c(Ljava/lang/Object;)V
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->b(Ljava/lang/Object;)Ljava/lang/Object;

    .line 256
    return-void
.end method

.method protected final d(Ljava/lang/Object;)Lcom/google/android/gms/games/b/v;
    .locals 1

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/gms/games/b/u;->a:Landroid/support/v4/g/h;

    invoke-virtual {v0, p1}, Landroid/support/v4/g/h;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/b/v;

    return-object v0
.end method
