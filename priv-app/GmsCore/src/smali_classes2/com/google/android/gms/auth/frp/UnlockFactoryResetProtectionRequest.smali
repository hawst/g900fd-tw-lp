.class public Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/auth/frp/j;


# instance fields
.field final a:I

.field public final b:Ljava/lang/String;

.field public final c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 18
    new-instance v0, Lcom/google/android/gms/auth/frp/j;

    invoke-direct {v0}, Lcom/google/android/gms/auth/frp/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->CREATOR:Lcom/google/android/gms/auth/frp/j;

    return-void
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->a:I

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->b:Ljava/lang/String;

    .line 40
    iput-object p3, p0, Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;->c:Ljava/lang/String;

    .line 41
    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 56
    invoke-static {p0, p1}, Lcom/google/android/gms/auth/frp/j;->a(Lcom/google/android/gms/auth/frp/UnlockFactoryResetProtectionRequest;Landroid/os/Parcel;)V

    .line 57
    return-void
.end method
