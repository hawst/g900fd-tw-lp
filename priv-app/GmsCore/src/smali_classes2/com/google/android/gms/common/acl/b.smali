.class public final Lcom/google/android/gms/common/acl/b;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:Ljava/lang/String;

.field private c:Ljava/lang/String;

.field private d:Ljava/lang/String;

.field private e:Z

.field private f:Ljava/lang/String;

.field private g:Ljava/lang/String;

.field private h:Z

.field private i:Z

.field private j:Z

.field private k:Z

.field private l:Z

.field private m:Z

.field private n:Ljava/util/List;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 75
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 64
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->e:Z

    .line 67
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->h:Z

    .line 68
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->i:Z

    .line 69
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->j:Z

    .line 70
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->k:Z

    .line 71
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->l:Z

    .line 72
    iput-boolean v0, p0, Lcom/google/android/gms/common/acl/b;->m:Z

    .line 73
    invoke-static {}, Lcom/google/android/gms/common/acl/ScopeData;->u()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/acl/b;->n:Ljava/util/List;

    .line 76
    iput-object p1, p0, Lcom/google/android/gms/common/acl/b;->a:Ljava/lang/String;

    .line 77
    iput-object p2, p0, Lcom/google/android/gms/common/acl/b;->b:Ljava/lang/String;

    .line 78
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/common/acl/ScopeData;
    .locals 15

    .prologue
    .line 137
    new-instance v0, Lcom/google/android/gms/common/acl/ScopeData;

    iget-object v1, p0, Lcom/google/android/gms/common/acl/b;->a:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/gms/common/acl/b;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/common/acl/b;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/common/acl/b;->d:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/gms/common/acl/b;->e:Z

    iget-object v6, p0, Lcom/google/android/gms/common/acl/b;->f:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/gms/common/acl/b;->g:Ljava/lang/String;

    iget-boolean v8, p0, Lcom/google/android/gms/common/acl/b;->h:Z

    iget-boolean v9, p0, Lcom/google/android/gms/common/acl/b;->i:Z

    iget-boolean v10, p0, Lcom/google/android/gms/common/acl/b;->j:Z

    iget-boolean v11, p0, Lcom/google/android/gms/common/acl/b;->k:Z

    iget-boolean v12, p0, Lcom/google/android/gms/common/acl/b;->l:Z

    iget-boolean v13, p0, Lcom/google/android/gms/common/acl/b;->m:Z

    iget-object v14, p0, Lcom/google/android/gms/common/acl/b;->n:Ljava/util/List;

    invoke-direct/range {v0 .. v14}, Lcom/google/android/gms/common/acl/ScopeData;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Ljava/lang/String;ZZZZZZLjava/util/List;)V

    return-object v0
.end method
