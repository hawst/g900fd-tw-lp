.class public final Lcom/google/android/gms/car/diagnostics/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/senderprotocol/w;


# instance fields
.field private final a:Ljava/lang/Object;

.field private b:Lcom/google/android/gms/car/senderprotocol/q;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/i;->a:Ljava/lang/Object;

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 34
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 35
    const/4 v0, 0x0

    :try_start_0
    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/i;->b:Lcom/google/android/gms/car/senderprotocol/q;

    .line 36
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/c/b/cc;)V
    .locals 4

    .prologue
    .line 41
    const-string v0, "CAR.DIAGNOSTICS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 42
    const-string v0, "CAR.DIAGNOSTICS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "onPingRequest: timestamp="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-wide v2, p1, Lcom/google/android/c/b/cc;->a:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 44
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 45
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/i;->b:Lcom/google/android/gms/car/senderprotocol/q;

    if-eqz v0, :cond_2

    .line 46
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/i;->b:Lcom/google/android/gms/car/senderprotocol/q;

    iget-wide v2, p1, Lcom/google/android/c/b/cc;->a:J

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/car/senderprotocol/q;->a(J)V

    .line 52
    :cond_1
    :goto_0
    monitor-exit v1

    return-void

    .line 48
    :cond_2
    const-string v0, "CAR.DIAGNOSTICS"

    const/4 v2, 0x4

    invoke-static {v0, v2}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 49
    const-string v0, "CAR.DIAGNOSTICS"

    const-string v2, "onPingRequest: ControlEndPoint has not been registered"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/q;)V
    .locals 2

    .prologue
    .line 24
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/i;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 25
    :try_start_0
    iput-object p1, p0, Lcom/google/android/gms/car/diagnostics/i;->b:Lcom/google/android/gms/car/senderprotocol/q;

    .line 26
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
