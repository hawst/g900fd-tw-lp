.class public final Lcom/google/android/gms/icing/impl/bp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:[C


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    const/16 v0, 0x10

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/icing/impl/bp;->a:[C

    return-void

    :array_0
    .array-data 2
        0x30s
        0x31s
        0x32s
        0x33s
        0x34s
        0x35s
        0x36s
        0x37s
        0x38s
        0x39s
        0x41s
        0x42s
        0x43s
        0x44s
        0x45s
        0x46s
    .end array-data
.end method

.method public static a(I)I
    .locals 2

    .prologue
    .line 249
    packed-switch p0, :pswitch_data_0

    .line 259
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown rankingStrategy"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 251
    :pswitch_0
    const/4 v0, 0x0

    .line 257
    :goto_0
    return v0

    .line 253
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 255
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 257
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 249
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static a(II)I
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 265
    packed-switch p0, :pswitch_data_0

    .line 274
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "unknown queryTokenizer"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 267
    :pswitch_0
    const/4 v2, 0x3

    if-ne p1, v2, :cond_0

    .line 272
    :goto_0
    :pswitch_1
    return v0

    :cond_0
    move v0, v1

    .line 267
    goto :goto_0

    :pswitch_2
    move v0, v1

    .line 270
    goto :goto_0

    .line 265
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method

.method public static final a([I)I
    .locals 2

    .prologue
    .line 240
    const/4 v0, 0x0

    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 241
    aget v1, p0, v0

    if-nez v1, :cond_0

    .line 245
    :goto_1
    return v0

    .line 240
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 245
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/icing/t;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 121
    if-nez p0, :cond_0

    .line 124
    :goto_0
    return-object v7

    :cond_0
    new-instance v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;

    iget-object v1, p0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    iget v2, p0, Lcom/google/android/gms/icing/t;->b:I

    iget v3, p0, Lcom/google/android/gms/icing/t;->c:I

    iget v4, p0, Lcom/google/android/gms/icing/t;->d:I

    iget-object v5, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_2

    iget-object v5, p0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    :goto_1
    iget-object v6, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v6

    if-nez v6, :cond_3

    iget-object v6, p0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    :goto_2
    iget-object v8, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v8

    if-nez v8, :cond_1

    iget-object v7, p0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    :cond_1
    invoke-direct/range {v0 .. v7}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;-><init>(Ljava/lang/String;IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    move-object v7, v0

    goto :goto_0

    :cond_2
    move-object v5, v7

    goto :goto_1

    :cond_3
    move-object v6, v7

    goto :goto_2
.end method

.method public static a(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    .locals 5

    .prologue
    const/4 v0, 0x0

    .line 137
    if-nez p0, :cond_1

    .line 149
    :cond_0
    :goto_0
    return-object v0

    .line 142
    :cond_1
    :try_start_0
    invoke-static {}, Landroid/os/Parcel;->obtain()Landroid/os/Parcel;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v1

    .line 143
    const/4 v0, 0x0

    :try_start_1
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    .line 144
    const/4 v2, 0x0

    array-length v3, v0

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Parcel;->unmarshall([BII)V

    .line 145
    const/4 v0, 0x0

    invoke-virtual {v1, v0}, Landroid/os/Parcel;->setDataPosition(I)V

    .line 146
    sget-object v0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->CREATOR:Lcom/google/android/gms/appdatasearch/t;

    invoke-static {v1}, Lcom/google/android/gms/appdatasearch/t;->a(Landroid/os/Parcel;)Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v0

    .line 148
    if-eqz v1, :cond_0

    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    goto :goto_0

    .line 148
    :catchall_0
    move-exception v1

    move-object v4, v1

    move-object v1, v0

    move-object v0, v4

    :goto_1
    if-eqz v1, :cond_2

    .line 149
    invoke-virtual {v1}, Landroid/os/Parcel;->recycle()V

    :cond_2
    throw v0

    .line 148
    :catchall_1
    move-exception v0

    goto :goto_1
.end method

.method public static a(Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;)Lcom/google/android/gms/icing/t;
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/google/android/gms/icing/t;

    invoke-direct {v0}, Lcom/google/android/gms/icing/t;-><init>()V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->a()Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/icing/t;->a:Ljava/lang/String;

    .line 104
    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->c:I

    iput v1, v0, Lcom/google/android/gms/icing/t;->b:I

    .line 105
    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->d:I

    iput v1, v0, Lcom/google/android/gms/icing/t;->c:I

    .line 106
    iget v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->e:I

    iput v1, v0, Lcom/google/android/gms/icing/t;->d:I

    .line 107
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    if-eqz v1, :cond_0

    .line 108
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/icing/t;->e:Ljava/lang/String;

    .line 110
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 111
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/icing/t;->f:Ljava/lang/String;

    .line 113
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    if-eqz v1, :cond_2

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/appdatasearch/GlobalSearchApplicationInfo;->h:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/icing/t;->g:Ljava/lang/String;

    .line 116
    :cond_2
    return-object v0
.end method

.method public static a(Ljava/lang/String;Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 72
    if-nez p0, :cond_0

    .line 79
    :goto_0
    return-object p1

    .line 76
    :cond_0
    const/4 v0, 0x0

    :try_start_0
    invoke-static {p0, v0}, Landroid/util/Base64;->decode(Ljava/lang/String;I)[B

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Lcom/google/protobuf/nano/i; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object p1

    goto :goto_0

    .line 77
    :catch_0
    move-exception v0

    const-string v1, "Corrupted message"

    new-array v2, v2, [Ljava/lang/Object;

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    goto :goto_0
.end method

.method private static a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;
    .locals 3

    .prologue
    .line 208
    const/4 v0, 0x0

    :goto_0
    const/4 v1, 0x2

    if-ge v0, v1, :cond_1

    .line 210
    :try_start_0
    invoke-interface {p0}, Ljava/util/concurrent/Callable;->call()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    move-result-object v0

    return-object v0

    .line 211
    :catch_0
    move-exception v1

    .line 212
    const/4 v2, 0x1

    if-ne v0, v2, :cond_0

    .line 213
    throw v1

    .line 216
    :catch_1
    move-exception v0

    throw v0

    .line 208
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 221
    :cond_1
    new-instance v0, Ljava/lang/AssertionError;

    const-string v1, "should not happen"

    invoke-direct {v0, v1}, Ljava/lang/AssertionError;-><init>(Ljava/lang/Object;)V

    throw v0
.end method

.method public static a(J)Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v2, 0x1

    const/4 v6, 0x0

    .line 155
    const-wide/32 v0, 0x40000000

    cmp-long v0, p0, v0

    if-lez v0, :cond_0

    .line 156
    const-string v0, "%.2fGB"

    new-array v1, v2, [Ljava/lang/Object;

    long-to-double v2, p0

    const-wide v4, 0x41d2d00000000000L    # 1.262485504E9

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 162
    :goto_0
    return-object v0

    .line 157
    :cond_0
    const-wide/32 v0, 0x12d000

    cmp-long v0, p0, v0

    if-lez v0, :cond_1

    .line 158
    const-string v0, "%.2fMB"

    new-array v1, v2, [Ljava/lang/Object;

    long-to-double v2, p0

    const-wide v4, 0x4132d00000000000L    # 1232896.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 159
    :cond_1
    const-wide/16 v0, 0x400

    cmp-long v0, p0, v0

    if-lez v0, :cond_2

    .line 160
    const-string v0, "%.2fKB"

    new-array v1, v2, [Ljava/lang/Object;

    long-to-double v2, p0

    const-wide/high16 v4, 0x4090000000000000L    # 1024.0

    div-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 162
    :cond_2
    const-string v0, "%d Bytes"

    new-array v1, v2, [Ljava/lang/Object;

    invoke-static {p0, p1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v1, v6

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public static a(Lcom/google/protobuf/nano/j;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 68
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a([B)Ljava/lang/String;
    .locals 6

    .prologue
    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    array-length v0, p0

    mul-int/lit8 v0, v0, 0x2

    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 45
    array-length v2, p0

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-byte v3, p0, v0

    .line 46
    sget-object v4, Lcom/google/android/gms/icing/impl/bp;->a:[C

    shr-int/lit8 v5, v3, 0x4

    and-int/lit8 v5, v5, 0xf

    aget-char v4, v4, v5

    invoke-virtual {v1, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 47
    sget-object v4, Lcom/google/android/gms/icing/impl/bp;->a:[C

    and-int/lit8 v3, v3, 0xf

    aget-char v3, v4, v3

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 45
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 49
    :cond_0
    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/io/File;)V
    .locals 4

    .prologue
    .line 53
    if-nez p0, :cond_0

    .line 65
    :goto_0
    return-void

    .line 56
    :cond_0
    invoke-virtual {p0}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 57
    invoke-virtual {p0}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v1

    .line 58
    if-eqz v1, :cond_1

    .line 59
    array-length v2, v1

    const/4 v0, 0x0

    :goto_1
    if-ge v0, v2, :cond_1

    aget-object v3, v1, v0

    .line 60
    invoke-static {v3}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/io/File;)V

    .line 59
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 64
    :cond_1
    invoke-virtual {p0}, Ljava/io/File;->delete()Z

    goto :goto_0
.end method

.method public static b(I)I
    .locals 1

    .prologue
    .line 279
    packed-switch p0, :pswitch_data_0

    .line 289
    const/4 v0, 0x3

    :goto_0
    return v0

    .line 281
    :pswitch_0
    const/4 v0, 0x0

    goto :goto_0

    .line 283
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 285
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 287
    :pswitch_3
    const/4 v0, 0x4

    goto :goto_0

    .line 279
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public static b(Lcom/google/protobuf/nano/j;)Lcom/google/protobuf/nano/j;
    .locals 2

    .prologue
    .line 87
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/protobuf/nano/j;
    :try_end_0
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1

    .line 94
    :try_start_1
    invoke-static {p0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/protobuf/nano/j;->mergeFrom(Lcom/google/protobuf/nano/j;[B)Lcom/google/protobuf/nano/j;
    :try_end_1
    .catch Lcom/google/protobuf/nano/i; {:try_start_1 .. :try_end_1} :catch_2

    move-result-object v0

    return-object v0

    .line 88
    :catch_0
    move-exception v0

    .line 89
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 90
    :catch_1
    move-exception v0

    .line 91
    new-instance v1, Ljava/lang/IllegalStateException;

    invoke-direct {v1, v0}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/Throwable;)V

    throw v1

    .line 96
    :catch_2
    move-exception v0

    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Bad types"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public static b(Ljava/lang/String;)Ljava/security/MessageDigest;
    .locals 4

    .prologue
    .line 170
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/bq;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/bq;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 178
    :goto_0
    return-object v0

    .line 176
    :catch_0
    move-exception v0

    const-string v1, "MessageDigest algorithm %s not available"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 178
    const/4 v0, 0x0

    goto :goto_0

    .line 179
    :catch_1
    move-exception v0

    .line 180
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception from MessageDigest.getInstance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method public static c(Ljava/lang/String;)Ljavax/crypto/Mac;
    .locals 4

    .prologue
    .line 189
    :try_start_0
    new-instance v0, Lcom/google/android/gms/icing/impl/br;

    invoke-direct {v0, p0}, Lcom/google/android/gms/icing/impl/br;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/icing/impl/bp;->a(Ljava/util/concurrent/Callable;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljavax/crypto/Mac;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_1

    .line 197
    :goto_0
    return-object v0

    .line 195
    :catch_0
    move-exception v0

    const-string v1, "Mac algorithm %s not available"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p0, v2, v3

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/icing/c;->b(Ljava/lang/Throwable;Ljava/lang/String;[Ljava/lang/Object;)I

    .line 197
    const/4 v0, 0x0

    goto :goto_0

    .line 198
    :catch_1
    move-exception v0

    .line 199
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "Unexpected exception from Mac.getInstance"

    invoke-direct {v1, v2, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method
