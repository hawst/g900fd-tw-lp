.class public final Lcom/google/android/gms/auth/trustagent/bh;
.super Lcom/google/android/gms/auth/trustagent/a;
.source "SourceFile"

# interfaces
.implements Landroid/preference/Preference$OnPreferenceClickListener;


# instance fields
.field private c:Landroid/preference/PreferenceCategory;

.field private d:Landroid/preference/PreferenceCategory;

.field private e:Landroid/preference/SwitchPreference;

.field private f:Landroid/preference/SwitchPreference;

.field private g:Lcom/google/android/gms/auth/trustagent/am;

.field private h:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;-><init>()V

    .line 521
    return-void
.end method

.method private b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 5

    .prologue
    .line 405
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    .line 406
    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 407
    const-string v1, "auth_trust_agent_pref_trusted_place_account_"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v3, ""

    invoke-virtual {v1, v0, v3}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    if-eqz v1, :cond_0

    .line 408
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->h(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 409
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "auth_trust_agent_pref_trusted_place_name_"

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 414
    :goto_1
    return-object v0

    .line 407
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 414
    :cond_2
    const-string v0, ""

    goto :goto_1
.end method

.method private c(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 385
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_trust_agent_pref_trusted_place_enabled_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Home"

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private d(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 390
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_trust_agent_pref_trusted_place_enabled_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Work"

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private e(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 395
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_trust_agent_pref_trusted_place_address_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Home"

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private f(Ljava/lang/String;)Ljava/lang/String;
    .locals 2

    .prologue
    .line 400
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "auth_trust_agent_pref_trusted_place_address_"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "Work"

    invoke-direct {p0, p1, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private j()V
    .locals 9

    .prologue
    const/4 v2, 0x0

    .line 320
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/trustagent/bh;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/trustagent/bh;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Landroid/content/Context;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/trustagent/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3}, Lcom/google/android/gms/auth/trustagent/bh;->f(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    const-string v4, ""

    invoke-virtual {v1, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    .line 321
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0}, Landroid/preference/PreferenceCategory;->removeAll()V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    iget-object v0, v0, Lcom/google/android/gms/auth/trustagent/am;->e:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_7

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_2
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_7

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->i(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "auth_trust_agent_pref_trusted_place_name_"

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    new-instance v6, Ljava/lang/StringBuilder;

    const-string v7, "auth_trust_agent_pref_trusted_place_address_"

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    new-instance v7, Ljava/lang/StringBuilder;

    const-string v8, "auth_trust_agent_pref_trusted_place_account_"

    invoke-direct {v7, v8}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    const-string v7, ""

    invoke-virtual {v6, v1, v7}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_6

    const/4 v1, 0x1

    :goto_3
    const-string v6, "Home"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_1

    if-nez v1, :cond_0

    :cond_1
    const-string v6, "Work"

    invoke-virtual {v6, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    if-nez v1, :cond_0

    :cond_2
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_3

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    :cond_3
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    invoke-virtual {v1, v0}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setPersistent(Z)V

    invoke-virtual {v1, v4}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    invoke-virtual {v1, v5}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    goto/16 :goto_2

    .line 320
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    sget v1, Lcom/google/android/gms/p;->cN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v2}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    sget v1, Lcom/google/android/gms/p;->cN:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setSummary(Ljava/lang/CharSequence;)V

    goto/16 :goto_1

    :cond_6
    move v1, v2

    .line 321
    goto :goto_3

    .line 322
    :cond_7
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/location/places/f;)V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 286
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->a()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 287
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v0

    sget v1, Lcom/google/android/gms/p;->ct:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v4}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 317
    :goto_0
    return-void

    .line 293
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->a()Ljava/lang/String;

    move-result-object v0

    .line 294
    new-instance v1, Landroid/preference/Preference;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/preference/Preference;-><init>(Landroid/content/Context;)V

    .line 295
    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setKey(Ljava/lang/String;)V

    .line 298
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 299
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 300
    const-string v2, ""

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 305
    :goto_1
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v1}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z

    .line 306
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 307
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v4

    invoke-interface {v4}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 309
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v3

    invoke-interface {v3}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v0, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 313
    invoke-virtual {v1}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/auth/trustagent/bk;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bk;

    move-result-object v0

    .line 316
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Coffee - TrustedPlacesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bk;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 302
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 303
    invoke-virtual {p1}, Lcom/google/android/gms/location/places/f;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    goto :goto_1
.end method

.method public final a(Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 179
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_place_home_work_account"

    invoke-virtual {v0, v1, p1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 181
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3

    .prologue
    .line 169
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 170
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 171
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 173
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 174
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/ak;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 176
    :cond_0
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 268
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 269
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 270
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    .line 271
    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 272
    invoke-virtual {v2, p2}, Landroid/preference/Preference;->setTitle(Ljava/lang/CharSequence;)V

    .line 273
    invoke-virtual {v2, p3}, Landroid/preference/Preference;->setSummary(Ljava/lang/CharSequence;)V

    .line 277
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, p3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bh;->j()V

    .line 281
    return-void

    .line 269
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final a([Ljava/lang/String;)V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 189
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/am;->a()Z

    move-result v0

    if-nez v0, :cond_1

    .line 191
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/bh;->h:[Ljava/lang/String;

    .line 249
    :goto_0
    return-void

    .line 195
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Home"

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 197
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Work"

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bh;->b(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 199
    aget-object v2, p1, v9

    .line 200
    const/4 v3, 0x1

    aget-object v3, p1, v3

    .line 201
    const/4 v4, 0x2

    aget-object v4, p1, v4

    .line 202
    const/4 v5, 0x3

    aget-object v5, p1, v5

    .line 204
    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_3

    .line 205
    iget-object v6, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v6, v9}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 206
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 207
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    const-string v6, "Home"

    invoke-virtual {p0, v0, v6}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 210
    :cond_2
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 213
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v2}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    iget-object v7, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v8

    invoke-static {v8}, Lcom/google/android/gms/auth/trustagent/ak;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v7

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 217
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v2}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    const-string v7, "Home"

    invoke-virtual {v0, v6, v7}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 219
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v2}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v2}, Lcom/google/android/gms/auth/trustagent/ak;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    :cond_3
    invoke-virtual {v1, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 227
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v2, "auth_trust_agent_pref_trusted_places_work_key"

    invoke-virtual {v0, v2, v9}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 228
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 229
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Work"

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/auth/trustagent/bh;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    :cond_4
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_5

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/gms/auth/trustagent/ak;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const-string v2, "Work"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1, v5}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {v4}, Lcom/google/android/gms/auth/trustagent/ak;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 248
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bh;->j()V

    goto/16 :goto_0
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    .line 85
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->h:[Ljava/lang/String;

    if-eqz v0, :cond_0

    .line 86
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->h:[Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->a([Ljava/lang/String;)V

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->h:[Ljava/lang/String;

    .line 89
    :cond_0
    const-string v0, "auth_trust_agent_pref_trusted_places_add_trusted_place_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    .line 90
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setEnabled(Z)V

    .line 91
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bh;->j()V

    .line 92
    return-void
.end method

.method public final b(Ljava/lang/String;)V
    .locals 4

    .prologue
    .line 252
    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 253
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2}, Landroid/preference/PreferenceCategory;->getPreferenceCount()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 254
    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v2, v0}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v2

    .line 255
    invoke-virtual {v2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 256
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    invoke-virtual {v0, v2}, Landroid/preference/PreferenceCategory;->removePreference(Landroid/preference/Preference;)Z

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-static {p1}, Lcom/google/android/gms/auth/trustagent/ak;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->c(Ljava/lang/String;)V

    .line 264
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/bh;->j()V

    .line 265
    return-void

    .line 253
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method

.method public final d()Z
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final e()V
    .locals 3

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ak;->e(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 150
    return-void
.end method

.method public final f()V
    .locals 3

    .prologue
    .line 153
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/ak;->g(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 156
    return-void
.end method

.method public final g()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 159
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 160
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 161
    return-void
.end method

.method public final h()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 164
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setChecked(Z)V

    .line 165
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    invoke-virtual {v0, v1}, Landroid/preference/SwitchPreference;->setEnabled(Z)V

    .line 166
    return-void
.end method

.method public final i()Ljava/lang/String;
    .locals 3

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_place_home_work_account"

    const-string v2, ""

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 72
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/a;->onCreate(Landroid/os/Bundle;)V

    .line 73
    sget v0, Lcom/google/android/gms/s;->h:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->addPreferencesFromResource(I)V

    .line 74
    const-string v0, "auth_trust_agent_pref_trusted_places_from_google_maps_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->c:Landroid/preference/PreferenceCategory;

    .line 76
    const-string v0, "auth_trust_agent_pref_trusted_places_custom_places_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceCategory;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->d:Landroid/preference/PreferenceCategory;

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->c:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->e:Landroid/preference/SwitchPreference;

    .line 79
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->c:Landroid/preference/PreferenceCategory;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceCategory;->getPreference(I)Landroid/preference/Preference;

    move-result-object v0

    check-cast v0, Landroid/preference/SwitchPreference;

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->f:Landroid/preference/SwitchPreference;

    .line 80
    return-void
.end method

.method public final onPreferenceClick(Landroid/preference/Preference;)Z
    .locals 1

    .prologue
    .line 139
    const/4 v0, 0x0

    return v0
.end method

.method public final onPreferenceTreeClick(Landroid/preference/PreferenceScreen;Landroid/preference/Preference;)Z
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 96
    const-string v0, "auth_trust_agent_pref_trusted_places_home_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->c(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p2

    check-cast v0, Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 103
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->e()V

    .line 104
    check-cast p2, Landroid/preference/SwitchPreference;

    invoke-virtual {p2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_places_enable_home_work_first_time_key"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_places_enable_home_work_first_time_key"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 107
    invoke-static {}, Lcom/google/android/gms/auth/trustagent/bi;->a()Lcom/google/android/gms/auth/trustagent/bi;

    move-result-object v0

    .line 109
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Coffee - TrustedPlacesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    .line 134
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 111
    :cond_1
    const-string v0, "auth_trust_agent_pref_trusted_places_work_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 114
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->i()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/gms/auth/trustagent/bh;->d(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object v0, p2

    check-cast v0, Landroid/preference/SwitchPreference;

    invoke-virtual {v0}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->f()V

    .line 119
    check-cast p2, Landroid/preference/SwitchPreference;

    invoke-virtual {p2}, Landroid/preference/SwitchPreference;->isChecked()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_places_enable_home_work_first_time_key"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/am;->b(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/bh;->g:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trusted_places_enable_home_work_first_time_key"

    invoke-virtual {v0, v1, v3}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 122
    invoke-static {}, Lcom/google/android/gms/auth/trustagent/bi;->a()Lcom/google/android/gms/auth/trustagent/bi;

    move-result-object v0

    .line 124
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Coffee - TrustedPlacesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bi;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto :goto_0

    .line 126
    :cond_2
    const-string v0, "auth_trust_agent_pref_trusted_places_add_trusted_place_key"

    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 127
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/GoogleTrustAgentTrustedPlacesSettings;->g()V

    goto :goto_0

    .line 129
    :cond_3
    invoke-virtual {p2}, Landroid/preference/Preference;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2}, Landroid/preference/Preference;->getTitle()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2}, Landroid/preference/Preference;->getSummary()Ljava/lang/CharSequence;

    move-result-object v2

    invoke-interface {v2}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bo;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/auth/trustagent/bo;

    move-result-object v0

    .line 132
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/bh;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    const-string v2, "Coffee - TrustedPlacesFragment"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/bo;->show(Landroid/app/FragmentManager;Ljava/lang/String;)V

    goto/16 :goto_0
.end method
