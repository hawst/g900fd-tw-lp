.class final Lcom/google/android/gms/auth/trustagent/trustlet/aq;
.super Landroid/content/BroadcastReceiver;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;


# direct methods
.method constructor <init>(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V
    .locals 0

    .prologue
    .line 162
    iput-object p1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public final onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2

    .prologue
    .line 165
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 166
    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 169
    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-static {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V

    .line 171
    :cond_0
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_1

    const-string v1, "android.intent.action.USER_PRESENT"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 173
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->b(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V

    .line 174
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->e:Z

    if-eqz v0, :cond_2

    .line 176
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->e:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    const-string v1, "User present. Revoking one time auth."

    invoke-virtual {v0, v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->e(Ljava/lang/String;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->c(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)V

    .line 186
    :cond_2
    :goto_0
    return-void

    .line 182
    :cond_3
    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-virtual {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->j()Z

    move-result v0

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    iget-boolean v0, v0, Lcom/google/android/gms/auth/trustagent/trustlet/bh;->h:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-static {v0}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d(Lcom/google/android/gms/auth/trustagent/trustlet/ap;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    iget-object v1, p0, Lcom/google/android/gms/auth/trustagent/trustlet/aq;->a:Lcom/google/android/gms/auth/trustagent/trustlet/ap;

    invoke-virtual {v1}, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->a()Z

    move-result v1

    iput-boolean v1, v0, Lcom/google/android/gms/auth/trustagent/trustlet/ap;->d:Z

    goto :goto_0
.end method
