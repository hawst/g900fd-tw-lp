.class public final Lcom/google/android/gms/wallet/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final synthetic a:Lcom/google/android/gms/wallet/FullWallet;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/wallet/FullWallet;)V
    .locals 0

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/wallet/FullWallet;B)V
    .locals 0

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/gms/wallet/f;-><init>(Lcom/google/android/gms/wallet/FullWallet;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 243
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->h:Lcom/google/android/gms/identity/intents/model/UserAddress;

    .line 244
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->e:Lcom/google/android/gms/wallet/Address;

    .line 229
    return-object p0
.end method

.method public final a(Lcom/google/android/gms/wallet/ProxyCard;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->c:Lcom/google/android/gms/wallet/ProxyCard;

    .line 219
    return-object p0
.end method

.method public final a(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->a:Ljava/lang/String;

    .line 209
    return-object p0
.end method

.method public final a([Ljava/lang/String;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 238
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->g:[Ljava/lang/String;

    .line 239
    return-object p0
.end method

.method public final b(Lcom/google/android/gms/identity/intents/model/UserAddress;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->i:Lcom/google/android/gms/identity/intents/model/UserAddress;

    .line 249
    return-object p0
.end method

.method public final b(Lcom/google/android/gms/wallet/Address;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->f:Lcom/google/android/gms/wallet/Address;

    .line 234
    return-object p0
.end method

.method public final b(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 213
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->b:Ljava/lang/String;

    .line 214
    return-object p0
.end method

.method public final c(Ljava/lang/String;)Lcom/google/android/gms/wallet/f;
    .locals 1

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/gms/wallet/f;->a:Lcom/google/android/gms/wallet/FullWallet;

    iput-object p1, v0, Lcom/google/android/gms/wallet/FullWallet;->d:Ljava/lang/String;

    .line 224
    return-object p0
.end method
