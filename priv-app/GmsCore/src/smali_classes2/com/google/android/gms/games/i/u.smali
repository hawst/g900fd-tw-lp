.class final Lcom/google/android/gms/games/i/u;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/i/f;
.implements Lcom/google/android/gms/games/z;


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/i/b;

.field private final b:Lcom/google/android/gms/common/api/aq;

.field private final c:Lcom/google/android/gms/common/api/Status;

.field private final d:Lcom/google/android/gms/games/t;


# direct methods
.method constructor <init>(Lcom/google/android/gms/games/i/b;Lcom/google/android/gms/common/api/aq;Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 325
    iput-object p1, p0, Lcom/google/android/gms/games/i/u;->a:Lcom/google/android/gms/games/i/b;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 326
    iput-object p2, p0, Lcom/google/android/gms/games/i/u;->b:Lcom/google/android/gms/common/api/aq;

    .line 327
    invoke-virtual {p3}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/gms/games/o;->a(I)Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/i/u;->c:Lcom/google/android/gms/common/api/Status;

    .line 328
    new-instance v0, Lcom/google/android/gms/games/t;

    invoke-direct {v0, p3}, Lcom/google/android/gms/games/t;-><init>(Lcom/google/android/gms/common/data/DataHolder;)V

    iput-object v0, p0, Lcom/google/android/gms/games/i/u;->d:Lcom/google/android/gms/games/t;

    .line 329
    return-void
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/gms/games/i/u;->c:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 333
    iget-object v0, p0, Lcom/google/android/gms/games/i/u;->b:Lcom/google/android/gms/common/api/aq;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/aq;->a(Lcom/google/android/gms/common/api/ap;)V

    .line 334
    return-void
.end method

.method public final c()Lcom/google/android/gms/games/t;
    .locals 1

    .prologue
    .line 350
    iget-object v0, p0, Lcom/google/android/gms/games/i/u;->d:Lcom/google/android/gms/games/t;

    return-object v0
.end method

.method public final w_()V
    .locals 1

    .prologue
    .line 343
    iget-object v0, p0, Lcom/google/android/gms/games/i/u;->d:Lcom/google/android/gms/games/t;

    if-eqz v0, :cond_0

    .line 344
    iget-object v0, p0, Lcom/google/android/gms/games/i/u;->d:Lcom/google/android/gms/games/t;

    invoke-virtual {v0}, Lcom/google/android/gms/games/t;->w_()V

    .line 346
    :cond_0
    return-void
.end method
