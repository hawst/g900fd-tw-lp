.class public final Lcom/google/android/gms/games/a/au;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/Context;

.field public final b:Lcom/google/android/gms/common/server/ClientContext;

.field public final c:Ljava/lang/String;

.field final d:Ljava/lang/String;

.field public final e:Ljava/lang/String;

.field public final f:Ljava/lang/String;

.field final g:Z

.field public final h:Z

.field final i:Z

.field j:J

.field private k:J

.field private l:J

.field private m:J


# direct methods
.method private constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    .line 36
    iput-boolean p7, p0, Lcom/google/android/gms/games/a/au;->g:Z

    .line 37
    iput-wide v4, p0, Lcom/google/android/gms/games/a/au;->k:J

    .line 38
    iput-wide v4, p0, Lcom/google/android/gms/games/a/au;->l:J

    .line 39
    iput-wide v4, p0, Lcom/google/android/gms/games/a/au;->m:J

    .line 40
    iput-wide v4, p0, Lcom/google/android/gms/games/a/au;->j:J

    .line 41
    iput-object p5, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    .line 42
    iput-object p3, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    .line 43
    iput-object p4, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    .line 44
    iput-object p6, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    .line 46
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v4

    .line 47
    if-eqz p3, :cond_8

    const-string v0, "593950602418"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_8

    move v3, v2

    .line 49
    :goto_0
    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_9

    :cond_0
    move v0, v2

    :goto_1
    const-string v5, "Must provide a valid owning Game ID, or null for \'GmsCore\'"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 52
    if-eqz p4, :cond_1

    invoke-virtual {p4}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_a

    :cond_1
    move v0, v2

    :goto_2
    const-string v5, "Must provide a valid target Game ID, or null for \'all games\'"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 55
    if-eqz v4, :cond_c

    .line 56
    if-nez p3, :cond_b

    move v0, v2

    :goto_3
    const-string v5, "GmsCore context should have a null owning game ID"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 66
    :cond_2
    :goto_4
    if-eqz p8, :cond_3

    .line 67
    if-eqz v4, :cond_e

    if-nez v3, :cond_e

    move v0, v2

    :goto_5
    const-string v5, "Only GmsCore context can create background operations"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 71
    :cond_3
    if-nez p4, :cond_5

    .line 72
    if-nez v4, :cond_4

    if-eqz v3, :cond_f

    :cond_4
    move v0, v2

    :goto_6
    const-string v5, "Only Gms or destination app context can request a null target games ID"

    invoke-static {v0, v5}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 75
    :cond_5
    if-nez v4, :cond_6

    if-eqz v3, :cond_7

    :cond_6
    move v1, v2

    :cond_7
    iput-boolean v1, p0, Lcom/google/android/gms/games/a/au;->h:Z

    .line 76
    iput-boolean p8, p0, Lcom/google/android/gms/games/a/au;->i:Z

    .line 77
    return-void

    :cond_8
    move v3, v1

    .line 47
    goto :goto_0

    :cond_9
    move v0, v1

    .line 49
    goto :goto_1

    :cond_a
    move v0, v1

    .line 52
    goto :goto_2

    :cond_b
    move v0, v1

    .line 56
    goto :goto_3

    .line 58
    :cond_c
    if-nez p3, :cond_d

    .line 59
    const-string v0, "If the owning game is not provided, then we must be in GmsCore context"

    invoke-static {v4, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    goto :goto_4

    .line 61
    :cond_d
    invoke-virtual {p3, p4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 62
    const-string v0, "Only destination app can make request for other games"

    invoke-static {v3, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    goto :goto_4

    :cond_e
    move v0, v1

    .line 67
    goto :goto_5

    :cond_f
    move v0, v1

    .line 72
    goto :goto_6
.end method

.method synthetic constructor <init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZB)V
    .locals 0

    .prologue
    .line 14
    invoke-direct/range {p0 .. p8}, Lcom/google/android/gms/games/a/au;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    return-void
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Lcom/google/android/gms/games/a/au;
    .locals 2

    .prologue
    .line 506
    invoke-virtual {p1}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    .line 507
    :goto_0
    new-instance v1, Lcom/google/android/gms/games/a/av;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object v0, v1, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    iput-object p2, v1, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v1}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0

    :cond_0
    move-object v0, p2

    .line 506
    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/android/gms/games/a/au;
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 484
    if-eqz p2, :cond_0

    move v0, v1

    :goto_0
    const-string v3, "External game ID cannot be null"

    invoke-static {v0, v3}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 485
    if-eqz p3, :cond_1

    :goto_1
    const-string v0, "External player ID cannot be null"

    invoke-static {v1, v0}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 487
    new-instance v0, Lcom/google/android/gms/games/a/av;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iput-object p3, v0, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iput-boolean p4, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iput-object p2, v0, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    iput-object p2, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    invoke-virtual {v0}, Lcom/google/android/gms/games/a/av;->a()Lcom/google/android/gms/games/a/au;

    move-result-object v0

    return-object v0

    :cond_0
    move v0, v2

    .line 484
    goto :goto_0

    :cond_1
    move v1, v2

    .line 485
    goto :goto_1
.end method


# virtual methods
.method public final a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)Lcom/google/android/gms/games/a/av;
    .locals 2

    .prologue
    .line 184
    new-instance v0, Lcom/google/android/gms/games/a/av;

    invoke-direct {v0, p1, p2}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-boolean v1, p0, Lcom/google/android/gms/games/a/au;->i:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->a:Z

    iget-boolean v1, p0, Lcom/google/android/gms/games/a/au;->g:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    .line 192
    invoke-virtual {p2}, Lcom/google/android/gms/common/server/ClientContext;->f()Z

    move-result v1

    if-nez v1, :cond_0

    .line 193
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    .line 196
    :cond_0
    return-object v0
.end method

.method public final a()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 102
    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->m:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 103
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 111
    :cond_0
    :goto_0
    return v0

    .line 107
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/a/au;->m:J

    .line 109
    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->m:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 111
    goto :goto_0
.end method

.method public final b()Z
    .locals 8

    .prologue
    const-wide/16 v6, -0x1

    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 153
    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->j:J

    cmp-long v2, v2, v6

    if-nez v2, :cond_2

    .line 154
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    if-nez v2, :cond_1

    .line 162
    :cond_0
    :goto_0
    return v0

    .line 158
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v4, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    invoke-static {v2, v3, v4}, Lcom/google/android/gms/games/a/l;->c(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/a/au;->j:J

    .line 160
    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->j:J

    cmp-long v2, v2, v6

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0

    :cond_2
    move v0, v1

    .line 162
    goto :goto_0
.end method

.method public final c()Lcom/google/android/gms/games/a/av;
    .locals 3

    .prologue
    .line 169
    new-instance v0, Lcom/google/android/gms/games/a/av;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/a/av;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;)V

    iget-boolean v1, p0, Lcom/google/android/gms/games/a/au;->i:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->a:Z

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->c:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/gms/games/a/au;->g:Z

    iput-boolean v1, v0, Lcom/google/android/gms/games/a/av;->g:Z

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->d:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->e:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/games/a/av;->f:Ljava/lang/String;

    return-object v0
.end method

.method public final d()Lcom/google/android/gms/games/b/m;
    .locals 3

    .prologue
    .line 203
    new-instance v0, Lcom/google/android/gms/games/b/m;

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->e()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->f()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/games/b/m;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 251
    iget-boolean v0, p0, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->d:Ljava/lang/String;

    goto :goto_0
.end method

.method public final f()Ljava/lang/String;
    .locals 1

    .prologue
    .line 263
    iget-boolean v0, p0, Lcom/google/android/gms/games/a/au;->h:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/games/a/au;->i:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    goto :goto_0
.end method

.method public final g()Z
    .locals 1

    .prologue
    .line 290
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final h()Z
    .locals 2

    .prologue
    .line 313
    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->f:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final i()J
    .locals 3

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/gms/games/a/au;->a()Z

    move-result v0

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing local player ID for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->c:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 334
    iget-wide v0, p0, Lcom/google/android/gms/games/a/au;->m:J

    return-wide v0
.end method

.method public final j()J
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    const/4 v0, 0x0

    .line 354
    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->l:J

    cmp-long v1, v2, v4

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    if-nez v1, :cond_1

    :cond_0
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Missing local game ID for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 356
    iget-wide v0, p0, Lcom/google/android/gms/games/a/au;->l:J

    return-wide v0

    .line 354
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/games/a/au;->a:Landroid/content/Context;

    iget-object v2, p0, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v3, p0, Lcom/google/android/gms/games/a/au;->e:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/games/a/l;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/gms/games/a/au;->l:J

    iget-wide v2, p0, Lcom/google/android/gms/games/a/au;->l:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method
