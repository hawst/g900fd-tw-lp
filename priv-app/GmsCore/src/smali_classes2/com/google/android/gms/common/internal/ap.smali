.class public final Lcom/google/android/gms/common/internal/ap;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/y;


# instance fields
.field private final a:Lcom/google/android/gms/common/et;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/common/et;)V
    .locals 0

    .prologue
    .line 294
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 295
    iput-object p1, p0, Lcom/google/android/gms/common/internal/ap;->a:Lcom/google/android/gms/common/et;

    .line 296
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ap;->a:Lcom/google/android/gms/common/et;

    invoke-interface {v0, p1}, Lcom/google/android/gms/common/et;->a(Lcom/google/android/gms/common/c;)V

    .line 301
    return-void
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 2

    .prologue
    .line 305
    instance-of v0, p1, Lcom/google/android/gms/common/internal/ap;

    if-eqz v0, :cond_0

    .line 306
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ap;->a:Lcom/google/android/gms/common/et;

    check-cast p1, Lcom/google/android/gms/common/internal/ap;

    iget-object v1, p1, Lcom/google/android/gms/common/internal/ap;->a:Lcom/google/android/gms/common/et;

    invoke-virtual {v0, v1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    .line 308
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ap;->a:Lcom/google/android/gms/common/et;

    invoke-virtual {v0, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v0

    goto :goto_0
.end method
