.class public final Lcom/google/android/gms/games/service/ad;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Landroid/content/SyncResult;

.field private final b:Ljava/util/ArrayList;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 633
    new-instance v0, Landroid/content/SyncResult;

    invoke-direct {v0}, Landroid/content/SyncResult;-><init>()V

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/ad;-><init>(Landroid/content/SyncResult;)V

    .line 634
    return-void
.end method

.method public constructor <init>(Landroid/content/SyncResult;)V
    .locals 1

    .prologue
    .line 636
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 637
    iput-object p1, p0, Lcom/google/android/gms/games/service/ad;->a:Landroid/content/SyncResult;

    .line 638
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/service/ad;->b:Ljava/util/ArrayList;

    .line 639
    return-void
.end method


# virtual methods
.method public final a(I)V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/games/service/ad;->b:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 643
    return-void
.end method

.method public final a()[I
    .locals 4

    .prologue
    .line 648
    iget-object v0, p0, Lcom/google/android/gms/games/service/ad;->b:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    new-array v2, v0, [I

    .line 649
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/service/ad;->b:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    .line 650
    iget-object v0, p0, Lcom/google/android/gms/games/service/ad;->b:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    aput v0, v2, v1

    .line 649
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 652
    :cond_0
    return-object v2
.end method
