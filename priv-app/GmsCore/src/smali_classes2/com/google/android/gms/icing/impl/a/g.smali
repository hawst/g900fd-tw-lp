.class public final Lcom/google/android/gms/icing/impl/a/g;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final f:Ljava/util/Set;


# instance fields
.field public final a:Lcom/google/android/gms/icing/impl/q;

.field public final b:Landroid/content/Context;

.field public final c:Landroid/content/pm/PackageManager;

.field public final d:Lcom/google/android/gms/icing/impl/a/z;

.field public final e:Lcom/google/android/gms/icing/impl/k;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 56
    new-instance v0, Ljava/util/HashSet;

    const/16 v1, 0x9

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "com.android.inputmethod.latin"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "com.google.android.inputmethod.latin"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "com.google.android.inputmethod.latin.dogfood"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "com.google.android.inputmethod.pinyin"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "com.google.android.inputmethod.korean"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "com.google.android.inputmethod.japanese"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "com.google.android.apps.inputmethod.hindi"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "com.google.android.apps.inputmethod.cantonese"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "com.google.android.apps.inputmethod.zhuyin"

    aput-object v3, v1, v2

    invoke-static {v1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/HashSet;-><init>(Ljava/util/Collection;)V

    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/icing/impl/a/g;->f:Ljava/util/Set;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/icing/impl/q;Landroid/content/Context;Landroid/content/pm/PackageManager;Lcom/google/android/gms/icing/impl/a/z;Lcom/google/android/gms/icing/impl/k;)V
    .locals 0

    .prologue
    .line 78
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 79
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    .line 80
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/a/g;->b:Landroid/content/Context;

    .line 81
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    .line 82
    iput-object p4, p0, Lcom/google/android/gms/icing/impl/a/g;->d:Lcom/google/android/gms/icing/impl/a/z;

    .line 83
    iput-object p5, p0, Lcom/google/android/gms/icing/impl/a/g;->e:Lcom/google/android/gms/icing/impl/k;

    .line 84
    return-void
.end method

.method static c()Ljava/util/Set;
    .locals 1

    .prologue
    .line 101
    sget-object v0, Lcom/google/android/gms/icing/impl/a/g;->f:Ljava/util/Set;

    return-object v0
.end method


# virtual methods
.method public final a()Z
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/g;->e:Lcom/google/android/gms/icing/impl/k;

    invoke-interface {v0}, Lcom/google/android/gms/icing/impl/k;->a()Ljava/lang/String;

    move-result-object v0

    .line 88
    iget-object v1, p0, Lcom/google/android/gms/icing/impl/a/g;->a:Lcom/google/android/gms/icing/impl/q;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/q;->z()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final a(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/ew;->b(Landroid/content/pm/PackageManager;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method final b(Ljava/lang/String;)Landroid/support/v4/g/o;
    .locals 3

    .prologue
    .line 106
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/a/g;->c:Landroid/content/pm/PackageManager;

    const/16 v1, 0x40

    invoke-virtual {v0, p1, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    .line 107
    if-eqz v1, :cond_0

    .line 108
    new-instance v0, Landroid/support/v4/g/o;

    iget v2, v1, Landroid/content/pm/PackageInfo;->versionCode:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1}, Lcom/google/android/gms/common/util/e;->a(Landroid/content/pm/PackageInfo;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v2, v1}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 114
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    :cond_0
    new-instance v0, Landroid/support/v4/g/o;

    const/4 v1, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    const-string v2, ""

    invoke-direct {v0, v1, v2}, Landroid/support/v4/g/o;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_0
.end method

.method final b()Z
    .locals 1

    .prologue
    .line 97
    const-string v0, "com.google.android.googlequicksearchbox"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/icing/impl/a/g;->a(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method
