.class public final Lcom/google/android/gms/games/service/a/m/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/common/data/DataHolder;

.field public final b:Ljava/lang/String;

.field public final c:Lcom/google/android/gms/drive/Contents;

.field public final d:Lcom/google/android/gms/drive/Contents;

.field public final e:Lcom/google/android/gms/drive/Contents;


# direct methods
.method public constructor <init>(I)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 80
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 81
    invoke-static {p1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 82
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->b:Ljava/lang/String;

    .line 83
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    .line 84
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->d:Lcom/google/android/gms/drive/Contents;

    .line 85
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->e:Lcom/google/android/gms/drive/Contents;

    .line 86
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/m;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 90
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->b:Ljava/lang/String;

    .line 91
    invoke-static {p2}, Lcom/google/android/gms/games/service/a/m/e;->a(Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    .line 92
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->d:Lcom/google/android/gms/drive/Contents;

    .line 93
    iput-object v1, p0, Lcom/google/android/gms/games/service/a/m/e;->e:Lcom/google/android/gms/drive/Contents;

    .line 94
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/m;Lcom/google/android/gms/drive/m;)V
    .locals 1

    .prologue
    .line 97
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    .line 99
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/e;->b:Ljava/lang/String;

    .line 100
    invoke-static {p3}, Lcom/google/android/gms/games/service/a/m/e;->a(Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    .line 101
    invoke-static {p4}, Lcom/google/android/gms/games/service/a/m/e;->a(Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/e;->d:Lcom/google/android/gms/drive/Contents;

    .line 102
    invoke-static {p5}, Lcom/google/android/gms/games/service/a/m/e;->a(Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/e;->e:Lcom/google/android/gms/drive/Contents;

    .line 103
    return-void
.end method

.method private static a(Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/drive/Contents;
    .locals 1

    .prologue
    .line 106
    if-nez p0, :cond_0

    .line 107
    const/4 v0, 0x0

    .line 109
    :goto_0
    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/drive/m;->e()Lcom/google/android/gms/drive/Contents;

    move-result-object v0

    goto :goto_0
.end method
