.class public Lcom/google/android/gms/common/download/DownloadOperationService;
.super Lcom/google/android/gms/common/service/c;
.source "SourceFile"


# static fields
.field private static final b:Lcom/google/android/gms/common/service/d;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    new-instance v0, Lcom/google/android/gms/common/service/d;

    invoke-direct {v0}, Lcom/google/android/gms/common/service/d;-><init>()V

    sput-object v0, Lcom/google/android/gms/common/download/DownloadOperationService;->b:Lcom/google/android/gms/common/service/d;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 28
    const-string v0, "DownloadOperationService"

    sget-object v1, Lcom/google/android/gms/common/download/DownloadOperationService;->b:Lcom/google/android/gms/common/service/d;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/common/service/c;-><init>(Ljava/lang/String;Lcom/google/android/gms/common/service/d;)V

    .line 29
    return-void
.end method

.method private static a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V
    .locals 1

    .prologue
    .line 33
    invoke-static {}, Lcom/google/android/gms/common/app/GmsApplication;->a()V

    .line 34
    sget-object v0, Lcom/google/android/gms/common/download/DownloadOperationService;->b:Lcom/google/android/gms/common/service/d;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/service/d;->add(Ljava/lang/Object;)Z

    .line 35
    const-string v0, "com.google.android.gms.common.download.INTENT"

    invoke-static {v0}, Lcom/google/android/gms/common/util/e;->g(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 36
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/download/b/j;Landroid/content/Context;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/gms/common/download/h;

    invoke-direct {v0, p0, p2}, Lcom/google/android/gms/common/download/h;-><init>(Lcom/google/android/gms/common/download/b/j;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/common/download/DownloadOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 41
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/download/b/j;Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 45
    new-instance v0, Lcom/google/android/gms/common/download/i;

    invoke-direct {v0, p0, p2, p3}, Lcom/google/android/gms/common/download/i;-><init>(Lcom/google/android/gms/common/download/b/j;Ljava/lang/String;Ljava/lang/String;)V

    invoke-static {p1, v0}, Lcom/google/android/gms/common/download/DownloadOperationService;->a(Landroid/content/Context;Lcom/google/android/gms/common/service/b;)V

    .line 46
    return-void
.end method
