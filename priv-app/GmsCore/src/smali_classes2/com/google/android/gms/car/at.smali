.class public final Lcom/google/android/gms/car/at;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static final a:Ljava/lang/String;

.field private static final b:Ljava/lang/String;


# instance fields
.field private final c:Landroid/content/Context;

.field private final d:Lcom/google/android/gms/car/gx;

.field private final e:Lcom/google/android/gms/common/api/v;

.field private f:Lcom/google/android/gms/car/CarFacet;

.field private g:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/google/android/gms/car/ae;->c:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/at;->a:Ljava/lang/String;

    .line 31
    sget-object v0, Lcom/google/android/gms/car/ae;->d:Landroid/content/ComponentName;

    invoke-virtual {v0}, Landroid/content/ComponentName;->getPackageName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/car/at;->b:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/google/android/gms/car/gx;)V
    .locals 2

    .prologue
    .line 43
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 44
    if-nez p2, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "binder must not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/at;->c:Landroid/content/Context;

    .line 49
    iput-object p2, p0, Lcom/google/android/gms/car/at;->d:Lcom/google/android/gms/car/gx;

    .line 52
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v1, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/at;->e:Lcom/google/android/gms/common/api/v;

    .line 54
    iget-object v0, p0, Lcom/google/android/gms/car/at;->e:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 55
    return-void
.end method

.method private a(Lcom/google/k/f/b;I)Lcom/google/android/gms/car/at;
    .locals 3

    .prologue
    .line 157
    new-instance v0, Lcom/google/android/gms/clearcut/a;

    iget-object v1, p0, Lcom/google/android/gms/car/at;->c:Landroid/content/Context;

    const-string v2, "CAR"

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    .line 159
    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/a;->a([B)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/clearcut/c;->a(I)Lcom/google/android/gms/clearcut/c;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/car/at;->e:Lcom/google/android/gms/common/api/v;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/clearcut/c;->b(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;

    .line 162
    return-object p0
.end method

.method private a()Lcom/google/k/f/b;
    .locals 9

    .prologue
    const/16 v4, 0xb

    const/4 v2, 0x2

    const/4 v1, 0x1

    const/4 v0, -0x1

    const/4 v3, 0x0

    .line 238
    new-instance v6, Lcom/google/k/f/b;

    invoke-direct {v6}, Lcom/google/k/f/b;-><init>()V

    .line 240
    iget-object v5, p0, Lcom/google/android/gms/car/at;->d:Lcom/google/android/gms/car/gx;

    invoke-virtual {v5}, Lcom/google/android/gms/car/gx;->H()Lcom/google/android/gms/car/gn;

    move-result-object v7

    .line 241
    if-eqz v7, :cond_1

    .line 245
    const/4 v5, 0x2

    :try_start_0
    invoke-virtual {v7, v5}, Lcom/google/android/gms/car/gn;->a(I)Lcom/google/android/gms/car/CarSensorEvent;

    move-result-object v5

    if-nez v5, :cond_3

    move v5, v3

    :goto_0
    iput v5, v6, Lcom/google/k/f/b;->e:I

    .line 247
    const/16 v5, 0x9

    invoke-virtual {v7, v5}, Lcom/google/android/gms/car/gn;->a(I)Lcom/google/android/gms/car/CarSensorEvent;

    move-result-object v5

    if-eqz v5, :cond_4

    iget-object v5, v5, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v8, 0x0

    aget-byte v5, v5, v8

    if-eqz v5, :cond_4

    move v5, v1

    :goto_1
    iput-boolean v5, v6, Lcom/google/k/f/b;->f:Z

    .line 249
    const/16 v5, 0xb

    invoke-virtual {v7, v5}, Lcom/google/android/gms/car/gn;->a(I)Lcom/google/android/gms/car/CarSensorEvent;

    move-result-object v5

    if-nez v5, :cond_5

    move v5, v3

    :goto_2
    iput v5, v6, Lcom/google/k/f/b;->g:I

    .line 251
    const/4 v5, 0x7

    invoke-virtual {v7, v5}, Lcom/google/android/gms/car/gn;->a(I)Lcom/google/android/gms/car/CarSensorEvent;

    move-result-object v5

    if-eqz v5, :cond_0

    iget-object v5, v5, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v7, 0x0

    aget-byte v5, v5, v7

    sparse-switch v5, :sswitch_data_0

    :cond_0
    move v1, v0

    :goto_3
    :sswitch_0
    iput v1, v6, Lcom/google/k/f/b;->h:I
    :try_end_0
    .catch Ljava/lang/IllegalStateException; {:try_start_0 .. :try_end_0} :catch_0

    .line 258
    :cond_1
    :goto_4
    new-instance v1, Landroid/content/IntentFilter;

    const-string v2, "android.intent.action.BATTERY_CHANGED"

    invoke-direct {v1, v2}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/at;->c:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    move-result-object v1

    if-nez v1, :cond_6

    :cond_2
    :goto_5
    iput v0, v6, Lcom/google/k/f/b;->j:I

    .line 259
    iget v0, p0, Lcom/google/android/gms/car/at;->g:I

    iput v0, v6, Lcom/google/k/f/b;->i:I

    .line 260
    return-object v6

    .line 245
    :cond_3
    :try_start_1
    iget-object v5, v5, Lcom/google/android/gms/car/CarSensorEvent;->d:[F

    const/4 v8, 0x0

    aget v5, v5, v8

    const/high16 v8, 0x447a0000    # 1000.0f

    mul-float/2addr v5, v8

    float-to-int v5, v5

    goto :goto_0

    :cond_4
    move v5, v3

    .line 247
    goto :goto_1

    .line 249
    :cond_5
    iget-object v5, v5, Lcom/google/android/gms/car/CarSensorEvent;->e:[B

    const/4 v8, 0x0

    aget-byte v5, v5, v8
    :try_end_1
    .catch Ljava/lang/IllegalStateException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    :sswitch_1
    move v1, v2

    .line 251
    goto :goto_3

    :sswitch_2
    const/4 v1, 0x3

    goto :goto_3

    :sswitch_3
    const/4 v1, 0x4

    goto :goto_3

    :sswitch_4
    const/4 v1, 0x5

    goto :goto_3

    :sswitch_5
    const/4 v1, 0x6

    goto :goto_3

    :sswitch_6
    move v1, v3

    goto :goto_3

    :sswitch_7
    const/16 v1, 0xc

    goto :goto_3

    :sswitch_8
    const/16 v1, 0xa

    goto :goto_3

    :sswitch_9
    move v1, v4

    goto :goto_3

    .line 258
    :cond_6
    const-string v2, "level"

    invoke-virtual {v1, v2, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    const-string v3, "scale"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    if-ltz v2, :cond_2

    if-ltz v1, :cond_2

    const v0, 0x186a0

    div-int/2addr v0, v1

    mul-int/2addr v0, v2

    div-int/lit16 v0, v0, 0x3e8

    goto :goto_5

    :catch_0
    move-exception v1

    goto :goto_4

    .line 251
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_6
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x3 -> :sswitch_2
        0x4 -> :sswitch_3
        0x5 -> :sswitch_4
        0x6 -> :sswitch_5
        0x64 -> :sswitch_8
        0x65 -> :sswitch_9
        0x66 -> :sswitch_7
    .end sparse-switch
.end method

.method private static a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 265
    const/4 v0, 0x0

    :try_start_0
    invoke-virtual {p0, p1, v0}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 266
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->versionName:Ljava/lang/String;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 269
    :goto_0
    return-object v0

    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a(Ljava/lang/String;)Ljava/lang/String;
    .locals 0

    .prologue
    .line 153
    if-nez p0, :cond_0

    const-string p0, ""

    :cond_0
    return-object p0
.end method


# virtual methods
.method public final a(I)Lcom/google/android/gms/car/at;
    .locals 2

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/gms/car/at;->a()Lcom/google/k/f/b;

    move-result-object v0

    .line 140
    if-nez v0, :cond_0

    .line 148
    :goto_0
    return-object p0

    .line 144
    :cond_0
    new-instance v1, Lcom/google/k/f/i;

    invoke-direct {v1}, Lcom/google/k/f/i;-><init>()V

    .line 145
    iput p1, v1, Lcom/google/k/f/i;->a:I

    .line 146
    iput-object v1, v0, Lcom/google/k/f/b;->d:Lcom/google/k/f/i;

    .line 148
    const/4 v1, 0x4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/at;->a(Lcom/google/k/f/b;I)Lcom/google/android/gms/car/at;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(J)Lcom/google/android/gms/car/at;
    .locals 3

    .prologue
    .line 95
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    .line 97
    invoke-direct {p0}, Lcom/google/android/gms/car/at;->a()Lcom/google/k/f/b;

    move-result-object v0

    .line 98
    if-nez v0, :cond_0

    .line 106
    :goto_0
    return-object p0

    .line 102
    :cond_0
    new-instance v1, Lcom/google/k/f/f;

    invoke-direct {v1}, Lcom/google/k/f/f;-><init>()V

    .line 103
    iput-wide p1, v1, Lcom/google/k/f/f;->a:J

    .line 104
    iput-object v1, v0, Lcom/google/k/f/b;->b:Lcom/google/k/f/f;

    .line 106
    const/4 v1, 0x2

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/at;->a(Lcom/google/k/f/b;I)Lcom/google/android/gms/car/at;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/CarFacet;)Lcom/google/android/gms/car/at;
    .locals 4

    .prologue
    .line 110
    invoke-direct {p0}, Lcom/google/android/gms/car/at;->a()Lcom/google/k/f/b;

    move-result-object v0

    .line 111
    if-nez v0, :cond_1

    .line 135
    :cond_0
    :goto_0
    return-object p0

    .line 114
    :cond_1
    if-eqz p1, :cond_0

    .line 118
    new-instance v1, Lcom/google/k/f/h;

    invoke-direct {v1}, Lcom/google/k/f/h;-><init>()V

    .line 119
    iget-object v2, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    if-eqz v2, :cond_2

    .line 120
    new-instance v2, Lcom/google/k/f/g;

    invoke-direct {v2}, Lcom/google/k/f/g;-><init>()V

    .line 121
    iget-object v3, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    iget v3, v3, Lcom/google/android/gms/car/CarFacet;->b:I

    iput v3, v2, Lcom/google/k/f/g;->a:I

    .line 122
    iget-object v3, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    iget-object v3, v3, Lcom/google/android/gms/car/CarFacet;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/g;->b:Ljava/lang/String;

    .line 123
    iput-object v2, v1, Lcom/google/k/f/h;->a:Lcom/google/k/f/g;

    .line 126
    :cond_2
    if-eqz p1, :cond_3

    .line 127
    new-instance v2, Lcom/google/k/f/g;

    invoke-direct {v2}, Lcom/google/k/f/g;-><init>()V

    .line 128
    iput-object v2, v1, Lcom/google/k/f/h;->b:Lcom/google/k/f/g;

    .line 129
    iget v3, p1, Lcom/google/android/gms/car/CarFacet;->b:I

    iput v3, v2, Lcom/google/k/f/g;->a:I

    .line 130
    iget-object v3, p1, Lcom/google/android/gms/car/CarFacet;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/g;->b:Ljava/lang/String;

    .line 131
    iput-object v1, v0, Lcom/google/k/f/b;->c:Lcom/google/k/f/h;

    .line 134
    :cond_3
    iput-object p1, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    .line 135
    const/4 v1, 0x3

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/at;->a(Lcom/google/k/f/b;I)Lcom/google/android/gms/car/at;

    move-result-object p0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/car/CarInfoInternal;)Lcom/google/android/gms/car/at;
    .locals 6

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/gms/car/at;->c:Landroid/content/Context;

    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 60
    const-string v1, "car_session"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/gms/car/at;->g:I

    .line 61
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "car_session"

    iget v2, p0, Lcom/google/android/gms/car/at;->g:I

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 63
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/at;->f:Lcom/google/android/gms/car/CarFacet;

    .line 65
    invoke-direct {p0}, Lcom/google/android/gms/car/at;->a()Lcom/google/k/f/b;

    move-result-object v0

    .line 66
    if-nez v0, :cond_0

    .line 91
    :goto_0
    return-object p0

    .line 70
    :cond_0
    new-instance v1, Lcom/google/k/f/c;

    invoke-direct {v1}, Lcom/google/k/f/c;-><init>()V

    .line 71
    iput-object v1, v0, Lcom/google/k/f/b;->a:Lcom/google/k/f/c;

    .line 73
    new-instance v2, Lcom/google/k/f/d;

    invoke-direct {v2}, Lcom/google/k/f/d;-><init>()V

    .line 74
    iget-object v3, p1, Lcom/google/android/gms/car/CarInfoInternal;->b:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/d;->a:Ljava/lang/String;

    .line 75
    iget-object v3, p1, Lcom/google/android/gms/car/CarInfoInternal;->c:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/d;->b:Ljava/lang/String;

    .line 76
    iget-object v3, p1, Lcom/google/android/gms/car/CarInfoInternal;->d:Ljava/lang/String;

    invoke-static {v3}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/google/k/f/d;->c:Ljava/lang/String;

    .line 77
    iput-object v2, v1, Lcom/google/k/f/c;->b:Lcom/google/k/f/d;

    .line 79
    iget-object v2, p0, Lcom/google/android/gms/car/at;->c:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    .line 80
    new-instance v3, Lcom/google/k/f/e;

    invoke-direct {v3}, Lcom/google/k/f/e;-><init>()V

    .line 81
    sget-object v4, Lcom/google/android/gms/car/at;->b:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/car/at;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/k/f/e;->a:Ljava/lang/String;

    .line 83
    iget-object v4, p0, Lcom/google/android/gms/car/at;->d:Lcom/google/android/gms/car/gx;

    const-string v5, "gmm_package_name"

    invoke-virtual {v4, v5}, Lcom/google/android/gms/car/gx;->b(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Lcom/google/android/gms/car/at;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/k/f/e;->b:Ljava/lang/String;

    .line 85
    const-string v4, "com.google.android.music"

    invoke-static {v2, v4}, Lcom/google/android/gms/car/at;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Lcom/google/k/f/e;->c:Ljava/lang/String;

    .line 87
    sget-object v4, Lcom/google/android/gms/car/at;->a:Ljava/lang/String;

    invoke-static {v2, v4}, Lcom/google/android/gms/car/at;->a(Landroid/content/pm/PackageManager;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/car/at;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v3, Lcom/google/k/f/e;->d:Ljava/lang/String;

    .line 89
    iput-object v3, v1, Lcom/google/k/f/c;->a:Lcom/google/k/f/e;

    .line 91
    const/4 v1, 0x1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/car/at;->a(Lcom/google/k/f/b;I)Lcom/google/android/gms/car/at;

    move-result-object p0

    goto :goto_0
.end method
