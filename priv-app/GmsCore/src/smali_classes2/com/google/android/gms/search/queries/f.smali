.class public Lcom/google/android/gms/search/queries/f;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;)V
    .locals 6

    .prologue
    .line 34
    const/4 v1, 0x1

    const/4 v2, 0x2

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    return-void
.end method

.method private c()Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;
    .locals 15

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;

    iget-object v1, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->a:[Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/impl/bb;->a([Ljava/lang/String;[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;)Ljava/lang/String;

    move-result-object v0

    .line 51
    if-eqz v0, :cond_0

    .line 52
    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v0

    .line 116
    :goto_0
    return-object v0

    .line 55
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;

    iget-object v3, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->b:[Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;

    .line 58
    new-instance v4, Lcom/google/android/gms/icing/ah;

    invoke-direct {v4}, Lcom/google/android/gms/icing/ah;-><init>()V

    .line 59
    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Request;->a:[Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/icing/ah;->b:[Ljava/lang/String;

    .line 61
    new-instance v5, Lcom/google/android/gms/icing/aj;

    invoke-direct {v5}, Lcom/google/android/gms/icing/aj;-><init>()V

    .line 62
    iput-object v5, v4, Lcom/google/android/gms/icing/ah;->a:Lcom/google/android/gms/icing/aj;

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v6

    .line 65
    invoke-interface {v6}, Lcom/google/android/gms/icing/impl/a/x;->b()Ljava/lang/Object;

    move-result-object v7

    monitor-enter v7

    .line 67
    :try_start_0
    array-length v0, v3

    new-array v0, v0, [Lcom/google/android/gms/icing/ai;

    iput-object v0, v4, Lcom/google/android/gms/icing/ah;->c:[Lcom/google/android/gms/icing/ai;

    .line 68
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 69
    const/4 v0, 0x0

    move v2, v0

    :goto_1
    array-length v0, v3

    if-ge v2, v0, :cond_4

    .line 70
    aget-object v0, v3, v2

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 71
    aget-object v1, v3, v2

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->a()Ljava/util/Map;

    move-result-object v9

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/search/queries/f;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v1

    iget-object v10, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-virtual {v1, v10}, Lcom/google/android/gms/icing/impl/a/f;->e(Ljava/lang/String;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 76
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Package name non-existent or not globally searchable "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/aq;->d(Ljava/lang/String;)Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v0

    monitor-exit v7
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 117
    :catchall_0
    move-exception v0

    monitor-exit v7

    throw v0

    .line 81
    :cond_1
    :try_start_1
    new-instance v10, Lcom/google/android/gms/icing/ai;

    invoke-direct {v10}, Lcom/google/android/gms/icing/ai;-><init>()V

    .line 82
    new-instance v1, Lcom/google/android/gms/appdatasearch/CorpusId;

    iget-object v11, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->b:Ljava/lang/String;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/CorpusId;->c:Ljava/lang/String;

    invoke-direct {v1, v11, v0}, Lcom/google/android/gms/appdatasearch/CorpusId;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v6, v1}, Lcom/google/android/gms/icing/impl/a/x;->a(Lcom/google/android/gms/appdatasearch/CorpusId;)Ljava/lang/String;

    move-result-object v0

    .line 84
    invoke-interface {v6, v0}, Lcom/google/android/gms/icing/impl/a/x;->d(Ljava/lang/String;)Lcom/google/android/gms/icing/g;

    move-result-object v11

    .line 86
    if-eqz v11, :cond_3

    .line 87
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    iget v12, v11, Lcom/google/android/gms/icing/g;->a:I

    aput v12, v0, v1

    iput-object v0, v10, Lcom/google/android/gms/icing/ai;->a:[I

    .line 92
    const/4 v1, 0x0

    :goto_2
    iget-object v0, v11, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v0, v0

    if-ge v1, v0, :cond_2

    .line 93
    iget-object v0, v11, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v12, v0, v1

    .line 94
    iget-object v0, v12, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-interface {v9, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 95
    if-eqz v0, :cond_6

    .line 97
    new-instance v13, Lcom/google/android/gms/icing/ao;

    invoke-direct {v13}, Lcom/google/android/gms/icing/ao;-><init>()V

    .line 98
    iget v14, v11, Lcom/google/android/gms/icing/g;->a:I

    iput v14, v13, Lcom/google/android/gms/icing/ao;->a:I

    .line 99
    iput v1, v13, Lcom/google/android/gms/icing/ao;->b:I

    .line 100
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, v13, Lcom/google/android/gms/icing/ao;->c:I

    .line 101
    iget v0, v12, Lcom/google/android/gms/icing/av;->l:I

    iput v0, v13, Lcom/google/android/gms/icing/ao;->d:I

    .line 102
    invoke-interface {v8, v13}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 103
    add-int/lit8 v0, v1, 0x1

    .line 92
    :goto_3
    add-int/lit8 v1, v0, 0x1

    goto :goto_2

    .line 105
    :cond_2
    iget-object v0, v4, Lcom/google/android/gms/icing/ah;->c:[Lcom/google/android/gms/icing/ai;

    aput-object v10, v0, v2

    .line 69
    :cond_3
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto/16 :goto_1

    .line 107
    :cond_4
    iget-object v0, v5, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    invoke-interface {v8, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/ao;

    iput-object v0, v5, Lcom/google/android/gms/icing/aj;->e:[Lcom/google/android/gms/icing/ao;

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/search/queries/f;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Lcom/google/android/gms/icing/ah;)[I

    move-result-object v1

    .line 112
    array-length v0, v3

    new-array v2, v0, [Lcom/google/android/gms/appdatasearch/CorpusId;

    .line 113
    const/4 v0, 0x0

    :goto_4
    array-length v4, v3

    if-ge v0, v4, :cond_5

    .line 114
    aget-object v4, v3, v0

    iget-object v4, v4, Lcom/google/android/gms/appdatasearch/PhraseAffinityCorpusSpec;->b:Lcom/google/android/gms/appdatasearch/CorpusId;

    aput-object v4, v2, v0

    .line 113
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 116
    :cond_5
    new-instance v0, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    invoke-direct {v0, v2, v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;-><init>([Lcom/google/android/gms/appdatasearch/CorpusId;[I)V

    monitor-exit v7
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto/16 :goto_0

    :cond_6
    move v0, v1

    goto :goto_3
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 5

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/gms/search/queries/f;->c()Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    move-result-object v1

    new-instance v2, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;

    invoke-direct {v2}, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;-><init>()V

    iput-object v1, v2, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->b:Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v3, 0x8

    invoke-virtual {v1}, Lcom/google/android/gms/appdatasearch/PhraseAffinityResponse;->b()Ljava/lang/String;

    move-result-object v1

    const/4 v4, 0x0

    invoke-direct {v0, v3, v1, v4}, Lcom/google/android/gms/common/api/Status;-><init>(ILjava/lang/String;Landroid/app/PendingIntent;)V

    :goto_0
    iput-object v0, v2, Lcom/google/android/gms/search/queries/GetPhraseAffinityCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v2

    :cond_0
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto :goto_0
.end method
