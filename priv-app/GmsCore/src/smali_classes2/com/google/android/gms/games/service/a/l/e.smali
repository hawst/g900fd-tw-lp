.class public final Lcom/google/android/gms/games/service/a/l/e;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/a/au;

.field private final e:I

.field private final f:I

.field private final g:[B

.field private final h:[Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;II[B[Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 25
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 26
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/l/e;->c:Lcom/google/android/gms/games/internal/dr;

    .line 27
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/l/e;->d:Lcom/google/android/gms/games/a/au;

    .line 28
    iput p3, p0, Lcom/google/android/gms/games/service/a/l/e;->e:I

    .line 29
    iput p4, p0, Lcom/google/android/gms/games/service/a/l/e;->f:I

    .line 30
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/l/e;->h:[Ljava/lang/String;

    .line 32
    if-nez p5, :cond_0

    .line 33
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/l/e;->g:[B

    .line 38
    :goto_0
    return-void

    .line 35
    :cond_0
    array-length v0, p5

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/l/e;->g:[B

    .line 36
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/e;->g:[B

    array-length v1, p5

    invoke-static {p5, v2, v0, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    goto :goto_0
.end method


# virtual methods
.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/l/e;->c:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->E(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 50
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 6

    .prologue
    .line 43
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/l/e;->d:Lcom/google/android/gms/games/a/au;

    iget v2, p0, Lcom/google/android/gms/games/service/a/l/e;->e:I

    iget v3, p0, Lcom/google/android/gms/games/service/a/l/e;->f:I

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/l/e;->g:[B

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/l/e;->h:[Ljava/lang/String;

    move-object v0, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;II[B[Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
