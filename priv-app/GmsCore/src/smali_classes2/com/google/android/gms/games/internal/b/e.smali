.class public final Lcom/google/android/gms/games/internal/b/e;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Ljava/lang/String;)I
    .locals 5

    .prologue
    const/4 v1, 0x2

    const/4 v2, 0x1

    const/4 v0, 0x0

    const/4 v3, -0x1

    .line 50
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    :cond_0
    move v4, v3

    :goto_0
    packed-switch v4, :pswitch_data_0

    .line 58
    const-string v0, "NotificationChannel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v3

    .line 59
    :goto_1
    :pswitch_0
    return v0

    .line 50
    :sswitch_0
    const-string v4, "MATCHES"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v0

    goto :goto_0

    :sswitch_1
    const-string v4, "QUESTS"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v2

    goto :goto_0

    :sswitch_2
    const-string v4, "REQUESTS"

    invoke-virtual {p0, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    move v4, v1

    goto :goto_0

    :pswitch_1
    move v0, v1

    .line 54
    goto :goto_1

    :pswitch_2
    move v0, v2

    .line 56
    goto :goto_1

    .line 50
    nop

    :sswitch_data_0
    .sparse-switch
        -0x70f9082f -> :sswitch_1
        0x17354fc4 -> :sswitch_2
        0x5cea1513 -> :sswitch_0
    .end sparse-switch

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static a(I)Ljava/lang/String;
    .locals 3

    .prologue
    .line 32
    packed-switch p0, :pswitch_data_0

    .line 40
    const-string v0, "NotificationChannel"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown channel type "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    const-string v0, "UNKNOWN_TYPE"

    :goto_0
    return-object v0

    .line 34
    :pswitch_0
    const-string v0, "MATCHES"

    goto :goto_0

    .line 36
    :pswitch_1
    const-string v0, "QUESTS"

    goto :goto_0

    .line 38
    :pswitch_2
    const-string v0, "REQUESTS"

    goto :goto_0

    .line 32
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
    .end packed-switch
.end method
