.class public final Lcom/google/android/gms/icing/impl/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final a:Ljava/lang/String;

.field private final b:[I

.field private final c:Ljava/util/Map;

.field private final d:Landroid/content/res/Resources;

.field private e:Z

.field private f:Ljava/util/List;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/g;Landroid/content/res/Resources;)V
    .locals 6

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    iget-object v0, p1, Lcom/google/android/gms/icing/g;->d:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->a:Ljava/lang/String;

    .line 50
    invoke-static {}, Lcom/google/android/gms/appdatasearch/y;->a()I

    move-result v0

    new-array v0, v0, [I

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    .line 51
    iget-object v1, p1, Lcom/google/android/gms/icing/g;->k:[Lcom/google/android/gms/icing/az;

    array-length v2, v1

    const/4 v0, 0x0

    :goto_0
    if-ge v0, v2, :cond_0

    aget-object v3, v1, v0

    .line 52
    iget-object v4, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    iget v5, v3, Lcom/google/android/gms/icing/az;->a:I

    iget v3, v3, Lcom/google/android/gms/icing/az;->c:I

    aput v3, v4, v5

    .line 51
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 54
    :cond_0
    invoke-static {p1}, Lcom/google/android/gms/icing/impl/c/c;->a(Lcom/google/android/gms/icing/g;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->c:Ljava/util/Map;

    .line 55
    iput-object p2, p0, Lcom/google/android/gms/icing/impl/c/c;->d:Landroid/content/res/Resources;

    .line 56
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;Landroid/content/res/Resources;)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput-object p1, p0, Lcom/google/android/gms/icing/impl/c/c;->a:Ljava/lang/String;

    .line 39
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    if-nez v0, :cond_0

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    .line 44
    :goto_0
    invoke-static {p2}, Lcom/google/android/gms/icing/impl/c/c;->a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->c:Ljava/util/Map;

    .line 45
    iput-object p3, p0, Lcom/google/android/gms/icing/impl/c/c;->d:Landroid/content/res/Resources;

    .line 46
    return-void

    .line 42
    :cond_0
    iget-object v0, p2, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->f:Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;

    iget-object v0, v0, Lcom/google/android/gms/appdatasearch/GlobalSearchCorpusConfig;->b:[I

    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    goto :goto_0
.end method

.method private static a(Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;)Ljava/util/Map;
    .locals 4

    .prologue
    .line 79
    new-instance v1, Ljava/util/HashMap;

    iget-object v0, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v0, v0

    invoke-direct {v1, v0}, Ljava/util/HashMap;-><init>(I)V

    .line 80
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 81
    iget-object v2, p0, Lcom/google/android/gms/appdatasearch/RegisterCorpusInfo;->e:[Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;

    aget-object v2, v2, v0

    iget-object v2, v2, Lcom/google/android/gms/appdatasearch/RegisterSectionInfo;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 83
    :cond_0
    return-object v1
.end method

.method private static a(Lcom/google/android/gms/icing/g;)Ljava/util/Map;
    .locals 5

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    array-length v1, v0

    .line 71
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2, v1}, Ljava/util/HashMap;-><init>(I)V

    .line 72
    const/4 v0, 0x0

    :goto_0
    if-ge v0, v1, :cond_0

    .line 73
    iget-object v3, p0, Lcom/google/android/gms/icing/g;->j:[Lcom/google/android/gms/icing/av;

    aget-object v3, v3, v0

    iget-object v3, v3, Lcom/google/android/gms/icing/av;->a:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-interface {v2, v3, v4}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 72
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 75
    :cond_0
    return-object v2
.end method

.method private a([Lcom/google/android/gms/icing/impl/c/d;Ljava/util/List;)V
    .locals 6

    .prologue
    .line 120
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    array-length v0, p1

    if-ge v1, v0, :cond_3

    .line 122
    :try_start_0
    aget-object v2, p1, v1

    if-nez v2, :cond_1

    const/4 v0, 0x0

    .line 123
    :goto_1
    if-eqz v0, :cond_0

    .line 124
    invoke-interface {p2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 120
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 122
    :cond_1
    new-instance v0, Lcom/google/android/gms/icing/impl/c/a/d;

    iget-object v3, v2, Lcom/google/android/gms/icing/impl/c/d;->b:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/c/c;->c:Ljava/util/Map;

    invoke-direct {v0, v3, v4}, Lcom/google/android/gms/icing/impl/c/a/d;-><init>(Ljava/lang/String;Ljava/util/Map;)V

    new-instance v3, Lcom/google/android/gms/icing/ba;

    invoke-direct {v3}, Lcom/google/android/gms/icing/ba;-><init>()V

    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->a()Z

    move-result v5

    if-nez v5, :cond_2

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/d;->h()Lcom/google/android/gms/icing/bb;

    move-result-object v5

    invoke-interface {v4, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/c/a/b; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_2

    .line 126
    :catch_0
    move-exception v0

    .line 127
    new-instance v2, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "In global search section spec for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v1}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v3, ":\n"

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v0}, Lcom/google/android/gms/icing/impl/c/a/b;->a()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;)V

    throw v2

    .line 122
    :cond_2
    :try_start_1
    iget-object v0, v3, Lcom/google/android/gms/icing/ba;->a:[Lcom/google/android/gms/icing/bb;

    invoke-interface {v4, v0}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/gms/icing/bb;

    iput-object v0, v3, Lcom/google/android/gms/icing/ba;->a:[Lcom/google/android/gms/icing/bb;

    new-instance v0, Lcom/google/android/gms/icing/az;

    invoke-direct {v0}, Lcom/google/android/gms/icing/az;-><init>()V

    iget v2, v2, Lcom/google/android/gms/icing/impl/c/d;->a:I

    iput v2, v0, Lcom/google/android/gms/icing/az;->c:I

    iput v1, v0, Lcom/google/android/gms/icing/az;->a:I

    iput-object v3, v0, Lcom/google/android/gms/icing/az;->b:Lcom/google/android/gms/icing/ba;
    :try_end_1
    .catch Lcom/google/android/gms/icing/impl/c/a/b; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1

    .line 131
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 87
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    if-eqz v0, :cond_3

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->d:Landroid/content/res/Resources;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/google/android/gms/icing/impl/b/d;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed to get resources for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/icing/impl/c/c;->a:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/gms/icing/impl/b/d;-><init>(Ljava/lang/String;)V

    throw v0

    .line 92
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    array-length v0, v0

    new-array v1, v0, [Lcom/google/android/gms/icing/impl/c/d;

    .line 94
    const/4 v0, 0x0

    :goto_0
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    array-length v2, v2

    if-ge v0, v2, :cond_2

    .line 95
    iget-object v2, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    aget v2, v2, v0

    if-eqz v2, :cond_1

    .line 97
    :try_start_0
    new-instance v2, Lcom/google/android/gms/icing/impl/c/d;

    iget-object v3, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    aget v3, v3, v0

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/c/c;->d:Landroid/content/res/Resources;

    iget-object v5, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    aget v5, v5, v0

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v3, v4}, Lcom/google/android/gms/icing/impl/c/d;-><init>(ILjava/lang/String;)V

    aput-object v2, v1, v0
    :try_end_0
    .catch Landroid/content/res/Resources$NotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 94
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 100
    :catch_0
    move-exception v1

    .line 101
    new-instance v2, Lcom/google/android/gms/icing/impl/b/a;

    new-instance v3, Ljava/lang/StringBuilder;

    const-string v4, "Invalid resource ID for "

    invoke-direct {v3, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/android/gms/appdatasearch/y;->a(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ": 0x"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/gms/icing/impl/c/c;->b:[I

    aget v0, v4, v0

    invoke-static {v0}, Ljava/lang/Integer;->toHexString(I)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {v2, v0, v1}, Lcom/google/android/gms/icing/impl/b/a;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 107
    :cond_2
    new-instance v0, Ljava/util/ArrayList;

    array-length v2, v1

    invoke-direct {v0, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 110
    :try_start_1
    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/icing/impl/c/c;->a([Lcom/google/android/gms/icing/impl/c/d;Ljava/util/List;)V

    .line 111
    iput-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->f:Ljava/util/List;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 113
    iput-boolean v6, p0, Lcom/google/android/gms/icing/impl/c/c;->e:Z

    .line 116
    :cond_3
    return-void

    .line 113
    :catchall_0
    move-exception v0

    iput-boolean v6, p0, Lcom/google/android/gms/icing/impl/c/c;->e:Z

    throw v0
.end method

.method public final b()Ljava/util/List;
    .locals 1

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->f:Ljava/util/List;

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/icing/impl/c/c;->e:Z

    if-nez v0, :cond_0

    .line 159
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/icing/impl/c/c;->a()V
    :try_end_0
    .catch Lcom/google/android/gms/icing/impl/b/b; {:try_start_0 .. :try_end_0} :catch_0

    .line 164
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/impl/c/c;->f:Ljava/util/List;

    :goto_0
    return-object v0

    .line 161
    :catch_0
    move-exception v0

    const/4 v0, 0x0

    goto :goto_0
.end method
