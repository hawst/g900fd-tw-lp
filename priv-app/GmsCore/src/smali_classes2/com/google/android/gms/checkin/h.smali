.class public final Lcom/google/android/gms/checkin/h;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Landroid/content/Context;

.field b:Landroid/os/DropBoxManager;

.field c:Landroid/content/SharedPreferences;

.field d:Lcom/android/a/a;

.field e:Ljava/lang/String;

.field f:I

.field g:I

.field h:I

.field i:J

.field j:J

.field k:Lcom/google/android/gms/checkin/b/k;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 93
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 98
    iput-object v2, p0, Lcom/google/android/gms/checkin/h;->b:Landroid/os/DropBoxManager;

    .line 101
    iput-object v2, p0, Lcom/google/android/gms/checkin/h;->c:Landroid/content/SharedPreferences;

    .line 104
    iput-object v2, p0, Lcom/google/android/gms/checkin/h;->d:Lcom/android/a/a;

    .line 107
    const-string v0, "https://android.clients.google.com/checkin"

    iput-object v0, p0, Lcom/google/android/gms/checkin/h;->e:Ljava/lang/String;

    .line 110
    const/high16 v0, 0x30000

    iput v0, p0, Lcom/google/android/gms/checkin/h;->f:I

    .line 113
    const/high16 v0, 0x80000

    iput v0, p0, Lcom/google/android/gms/checkin/h;->g:I

    .line 116
    const/16 v0, 0xa

    iput v0, p0, Lcom/google/android/gms/checkin/h;->h:I

    .line 119
    const-wide/32 v0, 0x240c8400

    iput-wide v0, p0, Lcom/google/android/gms/checkin/h;->i:J

    .line 122
    const-wide/32 v0, -0x75817280

    iput-wide v0, p0, Lcom/google/android/gms/checkin/h;->j:J

    .line 124
    iput-object v2, p0, Lcom/google/android/gms/checkin/h;->k:Lcom/google/android/gms/checkin/b/k;

    return-void
.end method
