.class final Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 36
    return-void
.end method

.method private a(Lcom/google/android/gms/games/service/statemachine/roomclient/v;Z)Z
    .locals 4

    .prologue
    .line 96
    const/4 v1, 0x0

    .line 98
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->d:Lcom/google/android/gms/games/service/c;

    iget-object v2, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/v;->a:Landroid/content/Context;

    iget-object v3, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/v;->d:Lcom/google/android/gms/common/server/ClientContext;

    invoke-interface {v0, v2, v3, p2}, Lcom/google/android/gms/games/service/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Z)Ljava/lang/String;
    :try_end_0
    .catch Lcom/google/android/gms/auth/q; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 103
    :goto_0
    if-nez v0, :cond_0

    .line 104
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Error getting auth token"

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 105
    const/4 v0, 0x0

    .line 112
    :goto_1
    return v0

    .line 100
    :catch_0
    move-exception v0

    .line 101
    const-string v2, "RoomServiceClientStateMachine"

    const-string v3, "Google authentication error"

    invoke-static {v2, v3, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v0, v1

    goto :goto_0

    .line 108
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    iget-object v2, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/v;->c:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/games/service/statemachine/roomclient/v;->e:Ljava/lang/String;

    invoke-interface {v1, v2, v0, v3}, Lcom/google/android/gms/games/internal/ed;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_1

    .line 112
    :goto_2
    const/4 v0, 0x1

    goto :goto_1

    .line 109
    :catch_1
    move-exception v0

    .line 110
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a(Landroid/os/RemoteException;)V

    goto :goto_2
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v8, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 53
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 80
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->g:Z

    :goto_0
    return v0

    .line 57
    :sswitch_0
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->b:Lcom/google/android/gms/games/service/statemachine/roomclient/cc;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cc;->b:Lcom/google/android/gms/games/internal/ed;

    invoke-interface {v0}, Lcom/google/android/gms/games/internal/ed;->b()V

    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->i:Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ServiceSetupState;->b()V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 62
    :goto_1
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->f:Z

    goto :goto_0

    .line 59
    :catch_0
    move-exception v0

    .line 60
    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a(Landroid/os/RemoteException;)V

    goto :goto_1

    .line 66
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 68
    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v3, v8}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 69
    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget-object v4, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget-wide v4, v4, Lcom/google/android/gms/games/g/ag;->j:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-nez v4, :cond_0

    iget-object v4, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    invoke-virtual {v3}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b()J

    move-result-wide v6

    iput-wide v6, v4, Lcom/google/android/gms/games/g/ag;->j:J

    .line 70
    :goto_2
    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->b()Lcom/google/android/gms/games/service/statemachine/roomclient/ad;

    move-result-object v4

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    iget v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/x;->a:I

    if-lt v3, v8, :cond_1

    move v3, v1

    :goto_3
    if-nez v3, :cond_3

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v5, Ljava/lang/StringBuilder;

    const-string v6, "Connect with retry.getRetryAuthToken(): "

    invoke-direct {v5, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v6, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    invoke-virtual {v6}, Lcom/google/android/gms/games/service/statemachine/roomclient/x;->a()Z

    move-result v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v3, v5}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->j:Lcom/google/android/gms/games/service/statemachine/roomclient/v;

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    invoke-virtual {v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/x;->a()Z

    move-result v5

    invoke-direct {p0, v3, v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/v;Z)Z

    move-result v3

    if-nez v3, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v3, "Unable to get auth token to try connect."

    invoke-interface {v1, v3}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    const/4 v1, 0x6

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    :goto_4
    move v1, v2

    :goto_5
    if-eqz v1, :cond_4

    .line 71
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->k:Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;

    iget-wide v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->k:J

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->e:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1, v2, v3, v4, v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitConnectNetworkState;->a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V

    .line 78
    :goto_6
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->f:Z

    goto/16 :goto_0

    .line 69
    :cond_0
    iget-object v3, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a:Lcom/google/android/gms/games/g/ag;

    iget v4, v3, Lcom/google/android/gms/games/g/ag;->l:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/android/gms/games/g/ag;->l:I

    goto :goto_2

    :cond_1
    move v3, v2

    .line 70
    goto :goto_3

    :cond_2
    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v4, "Trying to connect with returned auth token."

    invoke-interface {v3, v4}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    iget-object v3, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->l:Lcom/google/android/gms/games/service/statemachine/roomclient/x;

    iget v4, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/x;->a:I

    add-int/lit8 v4, v4, 0x1

    iput v4, v3, Lcom/google/android/gms/games/service/statemachine/roomclient/x;->a:I

    iget-object v3, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v3, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    goto :goto_5

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    const-string v3, "Unable to connect to network -- failing"

    invoke-interface {v1, v3}, Lcom/google/android/gms/games/service/statemachine/d;->a(Ljava/lang/String;)V

    const/4 v1, 0x4

    invoke-static {v1}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v1

    invoke-interface {v4, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ad;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_4

    .line 74
    :cond_4
    iget-object v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(Z)V

    .line 75
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a()V

    goto :goto_6

    .line 53
    nop

    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_1
        0x28 -> :sswitch_0
        0x2a -> :sswitch_0
    .end sparse-switch
.end method

.method public final b()V
    .locals 0

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->c()V

    .line 43
    return-void
.end method

.method public final d()V
    .locals 3

    .prologue
    .line 47
    const/16 v0, 0x28

    const/16 v1, 0x3c

    sget-object v2, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/gms/games/service/statemachine/roomclient/DisconnectedNetworkState;->a(IILjava/util/concurrent/TimeUnit;)V

    .line 49
    return-void
.end method
