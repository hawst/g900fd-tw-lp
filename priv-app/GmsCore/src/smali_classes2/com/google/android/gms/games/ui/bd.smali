.class final Lcom/google/android/gms/games/ui/bd;
.super Lcom/google/android/gms/games/ui/ay;
.source "SourceFile"


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/ui/aj;

.field private g:Landroid/widget/TextView;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/games/ui/aj;)V
    .locals 3

    .prologue
    .line 713
    iput-object p1, p0, Lcom/google/android/gms/games/ui/bd;->a:Lcom/google/android/gms/games/ui/aj;

    .line 714
    const/16 v0, 0xa

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Lcom/google/android/gms/games/ui/ay;-><init>(Lcom/google/android/gms/games/ui/aj;IIZ)V

    .line 715
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/games/ui/aj;B)V
    .locals 0

    .prologue
    .line 709
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/ui/bd;-><init>(Lcom/google/android/gms/games/ui/aj;)V

    return-void
.end method


# virtual methods
.method public final a(Landroid/view/View;)V
    .locals 2

    .prologue
    .line 719
    sget v0, Lcom/google/android/gms/j;->sQ:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 720
    sget v1, Lcom/google/android/gms/j;->sb:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/gms/games/ui/bd;->g:Landroid/widget/TextView;

    .line 722
    sget v1, Lcom/google/android/gms/p;->iu:I

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 723
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bd;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/games/ui/aj;)Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/games/ui/bd;->a(Z)V

    .line 724
    return-void
.end method

.method public final a(Z)V
    .locals 3

    .prologue
    .line 727
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bd;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v0}, Lcom/google/android/gms/games/ui/aj;->b(Lcom/google/android/gms/games/ui/aj;)Lcom/google/android/gms/games/ui/GamesSettingsActivity;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/ui/GamesSettingsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    .line 728
    if-eqz p1, :cond_0

    .line 729
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bd;->g:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->iw:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 730
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bd;->g:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/f;->W:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 736
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/games/ui/bd;->a:Lcom/google/android/gms/games/ui/aj;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/ui/aj;->a(Lcom/google/android/gms/games/ui/aj;Z)Z

    .line 737
    return-void

    .line 732
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bd;->g:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/p;->iv:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(I)V

    .line 733
    iget-object v1, p0, Lcom/google/android/gms/games/ui/bd;->g:Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/f;->w:I

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method
