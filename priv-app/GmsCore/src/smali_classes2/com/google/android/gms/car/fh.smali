.class public final Lcom/google/android/gms/car/fh;
.super Lcom/google/android/gms/car/lr;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/car/gw;
.implements Lcom/google/android/gms/car/senderprotocol/ao;


# instance fields
.field final a:Ljava/lang/Object;

.field b:Lcom/google/android/gms/car/fi;

.field volatile c:Z

.field private d:Lcom/google/android/gms/car/senderprotocol/an;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/gms/car/lr;-><init>()V

    .line 23
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    .line 127
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/car/fh;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic b(Lcom/google/android/gms/car/fh;)Lcom/google/android/gms/car/fi;
    .locals 1

    .prologue
    .line 20
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    return-object v0
.end method

.method private c(Lcom/google/android/gms/car/lt;)V
    .locals 2

    .prologue
    .line 115
    iget-boolean v0, p0, Lcom/google/android/gms/car/fh;->c:Z

    if-nez v0, :cond_0

    .line 116
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "CarNotConnected"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 117
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->d:Lcom/google/android/gms/car/senderprotocol/an;

    if-nez v0, :cond_1

    .line 118
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "MediaBrowserEndpoint not set."

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 119
    :cond_1
    if-nez p1, :cond_2

    .line 120
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "CarMediaPlaybackStatusEventListener is null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    if-eqz v0, :cond_3

    iget-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v0, v0, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-interface {v0}, Lcom/google/android/gms/car/lt;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/car/lt;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    if-eq v0, v1, :cond_3

    .line 123
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Media playback service already in use."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 125
    :cond_3
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 31
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/fh;->c:Z

    .line 32
    return-void
.end method

.method public final a(I)V
    .locals 4

    .prologue
    .line 70
    iget-object v1, p0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 72
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v0, v0, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-interface {v0, p1}, Lcom/google/android/gms/car/lt;->a(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 76
    :goto_0
    :try_start_1
    monitor-exit v1

    return-void

    .line 73
    :catch_0
    move-exception v0

    .line 74
    const-string v2, "CAR.INST"

    const-string v3, "Error calling onInput."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 76
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(Lcom/google/android/gms/car/lt;ILjava/lang/String;IZZZ)V
    .locals 7

    .prologue
    .line 54
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fh;->c(Lcom/google/android/gms/car/lt;)V

    .line 55
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->d:Lcom/google/android/gms/car/senderprotocol/an;

    move v1, p2

    move-object v2, p3

    move v3, p4

    move v4, p5

    move v5, p6

    move v6, p7

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/car/senderprotocol/an;->a(ILjava/lang/String;IZZZ)V

    .line 57
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/lt;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;II)V
    .locals 8

    .prologue
    .line 63
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fh;->c(Lcom/google/android/gms/car/lt;)V

    .line 64
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->d:Lcom/google/android/gms/car/senderprotocol/an;

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    move v6, p7

    move/from16 v7, p8

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/gms/car/senderprotocol/an;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;II)V

    .line 66
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/senderprotocol/az;)V
    .locals 0

    .prologue
    .line 36
    check-cast p1, Lcom/google/android/gms/car/senderprotocol/an;

    iput-object p1, p0, Lcom/google/android/gms/car/fh;->d:Lcom/google/android/gms/car/senderprotocol/an;

    .line 37
    return-void
.end method

.method public final a(Lcom/google/android/gms/car/lt;)Z
    .locals 6

    .prologue
    const/4 v0, 0x0

    .line 81
    iget-object v2, p0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    monitor-enter v2

    .line 82
    :try_start_0
    iget-object v1, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    if-eqz v1, :cond_1

    .line 83
    const-string v1, "CAR.INST"

    const/4 v3, 0x3

    invoke-static {v1, v3}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 84
    const-string v1, "CAR.INST"

    const-string v3, "Unregistering existing listener."

    invoke-static {v1, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 86
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v1, v1, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-virtual {p0, v1}, Lcom/google/android/gms/car/fh;->b(Lcom/google/android/gms/car/lt;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 89
    :cond_1
    :try_start_1
    new-instance v1, Lcom/google/android/gms/car/fi;

    invoke-direct {v1, p0, p1}, Lcom/google/android/gms/car/fi;-><init>(Lcom/google/android/gms/car/fh;Lcom/google/android/gms/car/lt;)V

    iput-object v1, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    .line 90
    iget-object v1, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v1, v1, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-interface {v1}, Lcom/google/android/gms/car/lt;->asBinder()Landroid/os/IBinder;

    move-result-object v1

    iget-object v3, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/os/IBinder;->linkToDeath(Landroid/os/IBinder$DeathRecipient;I)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 95
    monitor-exit v2

    .line 96
    const/4 v0, 0x1

    :goto_0
    return v0

    .line 91
    :catch_0
    move-exception v1

    .line 92
    :try_start_2
    const-string v3, "CAR.INST"

    new-instance v4, Ljava/lang/StringBuilder;

    const-string v5, "Adding listener failed: "

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v3, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    monitor-exit v2
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0

    .line 95
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0
.end method

.method public final b(Lcom/google/android/gms/car/lt;)Z
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 101
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/fh;->c(Lcom/google/android/gms/car/lt;)V

    .line 102
    iget-object v1, p0, Lcom/google/android/gms/car/fh;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 103
    :try_start_0
    iget-object v2, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    if-nez v2, :cond_0

    .line 104
    monitor-exit v1

    .line 108
    :goto_0
    return v0

    .line 106
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    iget-object v0, v0, Lcom/google/android/gms/car/fi;->a:Lcom/google/android/gms/car/lt;

    invoke-interface {v0}, Lcom/google/android/gms/car/lt;->asBinder()Landroid/os/IBinder;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    const/4 v3, 0x0

    invoke-interface {v0, v2, v3}, Landroid/os/IBinder;->unlinkToDeath(Landroid/os/IBinder$DeathRecipient;I)Z

    .line 107
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/fh;->b:Lcom/google/android/gms/car/fi;

    .line 108
    const/4 v0, 0x1

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 110
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method
