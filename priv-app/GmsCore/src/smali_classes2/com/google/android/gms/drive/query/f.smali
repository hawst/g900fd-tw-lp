.class public final Lcom/google/android/gms/drive/query/f;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Lcom/google/android/gms/drive/query/SortOrder;

.field public b:Ljava/util/List;

.field private final c:Ljava/util/List;

.field private d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/query/f;->c:Ljava/util/List;

    .line 63
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/drive/query/Query;)V
    .locals 2

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 57
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/drive/query/f;->c:Ljava/util/List;

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/drive/query/f;->c:Ljava/util/List;

    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->a()Lcom/google/android/gms/drive/query/Filter;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 70
    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->b()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/query/f;->d:Ljava/lang/String;

    .line 71
    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->c()Lcom/google/android/gms/drive/query/SortOrder;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/query/f;->a:Lcom/google/android/gms/drive/query/SortOrder;

    .line 72
    invoke-virtual {p1}, Lcom/google/android/gms/drive/query/Query;->d()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/drive/query/f;->b:Ljava/util/List;

    .line 73
    return-void
.end method


# virtual methods
.method public final a()Lcom/google/android/gms/drive/query/Query;
    .locals 5

    .prologue
    .line 123
    new-instance v0, Lcom/google/android/gms/drive/query/Query;

    new-instance v1, Lcom/google/android/gms/drive/query/internal/LogicalFilter;

    sget-object v2, Lcom/google/android/gms/drive/query/internal/Operator;->f:Lcom/google/android/gms/drive/query/internal/Operator;

    iget-object v3, p0, Lcom/google/android/gms/drive/query/f;->c:Ljava/util/List;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/drive/query/internal/LogicalFilter;-><init>(Lcom/google/android/gms/drive/query/internal/Operator;Ljava/lang/Iterable;)V

    iget-object v2, p0, Lcom/google/android/gms/drive/query/f;->d:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/drive/query/f;->a:Lcom/google/android/gms/drive/query/SortOrder;

    iget-object v4, p0, Lcom/google/android/gms/drive/query/f;->b:Ljava/util/List;

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/gms/drive/query/Query;-><init>(Lcom/google/android/gms/drive/query/internal/LogicalFilter;Ljava/lang/String;Lcom/google/android/gms/drive/query/SortOrder;Ljava/util/List;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/drive/query/Filter;)Lcom/google/android/gms/drive/query/f;
    .locals 1

    .prologue
    .line 82
    instance-of v0, p1, Lcom/google/android/gms/drive/query/internal/MatchAllFilter;

    if-nez v0, :cond_0

    .line 83
    iget-object v0, p0, Lcom/google/android/gms/drive/query/f;->c:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 85
    :cond_0
    return-object p0
.end method
