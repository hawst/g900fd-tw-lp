.class public final Lcom/google/android/gms/icing/am;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile c:[Lcom/google/android/gms/icing/am;


# instance fields
.field public a:Ljava/lang/String;

.field public b:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 4862
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 4863
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/icing/am;->b:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/am;->cachedSize:I

    .line 4864
    return-void
.end method

.method public static a()[Lcom/google/android/gms/icing/am;
    .locals 2

    .prologue
    .line 4845
    sget-object v0, Lcom/google/android/gms/icing/am;->c:[Lcom/google/android/gms/icing/am;

    if-nez v0, :cond_1

    .line 4846
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 4848
    :try_start_0
    sget-object v0, Lcom/google/android/gms/icing/am;->c:[Lcom/google/android/gms/icing/am;

    if-nez v0, :cond_0

    .line 4849
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/icing/am;

    sput-object v0, Lcom/google/android/gms/icing/am;->c:[Lcom/google/android/gms/icing/am;

    .line 4851
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 4853
    :cond_1
    sget-object v0, Lcom/google/android/gms/icing/am;->c:[Lcom/google/android/gms/icing/am;

    return-object v0

    .line 4851
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 4918
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 4919
    iget-object v1, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 4920
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 4923
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/am;->b:I

    if-eqz v1, :cond_1

    .line 4924
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/am;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 4927
    :cond_1
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 4875
    if-ne p1, p0, :cond_1

    .line 4892
    :cond_0
    :goto_0
    return v0

    .line 4878
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/am;

    if-nez v2, :cond_2

    move v0, v1

    .line 4879
    goto :goto_0

    .line 4881
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/am;

    .line 4882
    iget-object v2, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 4883
    iget-object v2, p1, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 4884
    goto :goto_0

    .line 4886
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 4887
    goto :goto_0

    .line 4889
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/am;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/am;->b:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 4890
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 4897
    iget-object v0, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 4900
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/am;->b:I

    add-int/2addr v0, v1

    .line 4901
    return v0

    .line 4897
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 4839
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/am;->b:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 4907
    iget-object v0, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 4908
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/am;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 4910
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/am;->b:I

    if-eqz v0, :cond_1

    .line 4911
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/am;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 4913
    :cond_1
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 4914
    return-void
.end method
