.class public final Lcom/google/android/gms/icing/bc;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# instance fields
.field public a:Ljava/lang/String;

.field public b:I

.field public c:Z

.field public d:I


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 654
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 655
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    iput v1, p0, Lcom/google/android/gms/icing/bc;->b:I

    iput-boolean v1, p0, Lcom/google/android/gms/icing/bc;->c:Z

    iput v1, p0, Lcom/google/android/gms/icing/bc;->d:I

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/icing/bc;->cachedSize:I

    .line 656
    return-void
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 726
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 727
    iget-object v1, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 728
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 731
    :cond_0
    iget v1, p0, Lcom/google/android/gms/icing/bc;->b:I

    if-eqz v1, :cond_1

    .line 732
    const/4 v1, 0x2

    iget v2, p0, Lcom/google/android/gms/icing/bc;->b:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 735
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/icing/bc;->c:Z

    if-eqz v1, :cond_2

    .line 736
    const/4 v1, 0x3

    iget-boolean v2, p0, Lcom/google/android/gms/icing/bc;->c:Z

    invoke-static {v1}, Lcom/google/protobuf/nano/b;->c(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    add-int/2addr v0, v1

    .line 739
    :cond_2
    iget v1, p0, Lcom/google/android/gms/icing/bc;->d:I

    if-eqz v1, :cond_3

    .line 740
    const/4 v1, 0x4

    iget v2, p0, Lcom/google/android/gms/icing/bc;->d:I

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->g(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 743
    :cond_3
    return v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 669
    if-ne p1, p0, :cond_1

    .line 692
    :cond_0
    :goto_0
    return v0

    .line 672
    :cond_1
    instance-of v2, p1, Lcom/google/android/gms/icing/bc;

    if-nez v2, :cond_2

    move v0, v1

    .line 673
    goto :goto_0

    .line 675
    :cond_2
    check-cast p1, Lcom/google/android/gms/icing/bc;

    .line 676
    iget-object v2, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    if-nez v2, :cond_3

    .line 677
    iget-object v2, p1, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    if-eqz v2, :cond_4

    move v0, v1

    .line 678
    goto :goto_0

    .line 680
    :cond_3
    iget-object v2, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    iget-object v3, p1, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_4

    move v0, v1

    .line 681
    goto :goto_0

    .line 683
    :cond_4
    iget v2, p0, Lcom/google/android/gms/icing/bc;->b:I

    iget v3, p1, Lcom/google/android/gms/icing/bc;->b:I

    if-eq v2, v3, :cond_5

    move v0, v1

    .line 684
    goto :goto_0

    .line 686
    :cond_5
    iget-boolean v2, p0, Lcom/google/android/gms/icing/bc;->c:Z

    iget-boolean v3, p1, Lcom/google/android/gms/icing/bc;->c:Z

    if-eq v2, v3, :cond_6

    move v0, v1

    .line 687
    goto :goto_0

    .line 689
    :cond_6
    iget v2, p0, Lcom/google/android/gms/icing/bc;->d:I

    iget v3, p1, Lcom/google/android/gms/icing/bc;->d:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 690
    goto :goto_0
.end method

.method public final hashCode()I
    .locals 2

    .prologue
    .line 697
    iget-object v0, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    add-int/lit16 v0, v0, 0x20f

    .line 700
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bc;->b:I

    add-int/2addr v0, v1

    .line 701
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v0, p0, Lcom/google/android/gms/icing/bc;->c:Z

    if-eqz v0, :cond_1

    const/16 v0, 0x4cf

    :goto_1
    add-int/2addr v0, v1

    .line 702
    mul-int/lit8 v0, v0, 0x1f

    iget v1, p0, Lcom/google/android/gms/icing/bc;->d:I

    add-int/2addr v0, v1

    .line 703
    return v0

    .line 697
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 701
    :cond_1
    const/16 v0, 0x4d5

    goto :goto_1
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 625
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    goto :goto_0

    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bc;->b:I

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->d()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/icing/bc;->c:Z

    goto :goto_0

    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/icing/bc;->d:I

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x10 -> :sswitch_2
        0x18 -> :sswitch_3
        0x20 -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 710
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/icing/bc;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 712
    :cond_0
    iget v0, p0, Lcom/google/android/gms/icing/bc;->b:I

    if-eqz v0, :cond_1

    .line 713
    const/4 v0, 0x2

    iget v1, p0, Lcom/google/android/gms/icing/bc;->b:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 715
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/gms/icing/bc;->c:Z

    if-eqz v0, :cond_2

    .line 716
    const/4 v0, 0x3

    iget-boolean v1, p0, Lcom/google/android/gms/icing/bc;->c:Z

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(IZ)V

    .line 718
    :cond_2
    iget v0, p0, Lcom/google/android/gms/icing/bc;->d:I

    if-eqz v0, :cond_3

    .line 719
    const/4 v0, 0x4

    iget v1, p0, Lcom/google/android/gms/icing/bc;->d:I

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->c(II)V

    .line 721
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 722
    return-void
.end method
