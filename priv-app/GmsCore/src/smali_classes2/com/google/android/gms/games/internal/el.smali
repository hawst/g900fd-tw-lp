.class public final Lcom/google/android/gms/games/internal/el;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public a:Landroid/os/IBinder;

.field public b:I

.field public c:I

.field public d:I

.field public e:I

.field public f:I

.field public g:I


# direct methods
.method private constructor <init>(ILandroid/os/IBinder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 245
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->c:I

    .line 240
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->d:I

    .line 241
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->e:I

    .line 242
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->f:I

    .line 243
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->g:I

    .line 246
    iput p1, p0, Lcom/google/android/gms/games/internal/el;->b:I

    .line 247
    iput-object p2, p0, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    .line 248
    return-void
.end method

.method synthetic constructor <init>(ILandroid/os/IBinder;B)V
    .locals 0

    .prologue
    .line 228
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/internal/el;-><init>(ILandroid/os/IBinder;)V

    return-void
.end method

.method public constructor <init>(Landroid/os/Bundle;Landroid/os/IBinder;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 250
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 239
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->c:I

    .line 240
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->d:I

    .line 241
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->e:I

    .line 242
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->f:I

    .line 243
    iput v1, p0, Lcom/google/android/gms/games/internal/el;->g:I

    .line 251
    iput-object p2, p0, Lcom/google/android/gms/games/internal/el;->a:Landroid/os/IBinder;

    .line 252
    const-string v0, "popupLocationInfo.gravity"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->b:I

    .line 253
    const-string v0, "popupLocationInfo.displayId"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->c:I

    .line 254
    const-string v0, "popupLocationInfo.left"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->d:I

    .line 255
    const-string v0, "popupLocationInfo.top"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->e:I

    .line 256
    const-string v0, "popupLocationInfo.right"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->f:I

    .line 257
    const-string v0, "popupLocationInfo.bottom"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/internal/el;->g:I

    .line 258
    return-void
.end method
