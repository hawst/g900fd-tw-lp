.class public Lcom/google/android/gms/common/people/views/AudienceView;
.super Landroid/widget/FrameLayout;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/x;
.implements Lcom/google/android/gms/common/api/y;


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# instance fields
.field private final a:Landroid/view/LayoutInflater;

.field private final b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

.field private final c:Landroid/view/View;

.field private final d:Ljava/util/LinkedHashMap;

.field private final e:Lcom/google/android/gms/plus/internal/ad;

.field private final f:Lcom/google/android/gms/common/api/v;

.field private g:Z

.field private final h:Ljava/util/HashMap;

.field private final i:Lcom/google/android/gms/common/internal/a/a;

.field private j:Z

.field private k:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2

    .prologue
    .line 102
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 103
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1

    .prologue
    .line 106
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 107
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 1

    .prologue
    .line 110
    sget-object v0, Lcom/google/android/gms/plus/internal/ab;->a:Lcom/google/android/gms/plus/internal/ad;

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/google/android/gms/common/people/views/AudienceView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/google/android/gms/plus/internal/ad;)V

    .line 111
    return-void
.end method

.method constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;ILcom/google/android/gms/plus/internal/ad;)V
    .locals 3

    .prologue
    .line 116
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 78
    new-instance v0, Ljava/util/LinkedHashMap;

    invoke-direct {v0}, Ljava/util/LinkedHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    .line 86
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    .line 89
    new-instance v0, Lcom/google/android/gms/common/internal/a/a;

    const/high16 v1, 0x200000

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/internal/a/a;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lcom/google/android/gms/common/internal/a/a;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    const-string v1, "layout_inflater"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    .line 121
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    sget v1, Lcom/google/android/gms/l;->ad:I

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v1

    .line 123
    sget v0, Lcom/google/android/gms/j;->bl:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    .line 124
    sget v0, Lcom/google/android/gms/j;->bk:I

    invoke-virtual {v1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->c:Landroid/view/View;

    .line 125
    iput-object p4, p0, Lcom/google/android/gms/common/people/views/AudienceView;->e:Lcom/google/android/gms/plus/internal/ad;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->e:Lcom/google/android/gms/plus/internal/ad;

    const/16 v1, 0x50

    const/4 v2, 0x0

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/plus/internal/ad;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    .line 132
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/x;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, p0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/y;)V

    .line 134
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->setSaveEnabled(Z)V

    .line 135
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/common/people/views/AudienceView;)Lcom/google/android/gms/common/internal/a/a;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lcom/google/android/gms/common/internal/a/a;

    return-object v0
.end method

.method static synthetic a(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 0

    .prologue
    .line 51
    invoke-static {p0, p1}, Lcom/google/android/gms/common/people/views/AudienceView;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    return-void
.end method

.method private static a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/google/android/gms/common/people/views/d;)V
    .locals 1

    .prologue
    .line 310
    iget v0, p3, Lcom/google/android/gms/common/people/views/d;->a:I

    invoke-virtual {p0, v0}, Landroid/view/View;->setBackgroundResource(I)V

    .line 311
    iget-object v0, p3, Lcom/google/android/gms/common/people/views/d;->b:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 312
    const/16 v0, 0x7f

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 313
    iget v0, p3, Lcom/google/android/gms/common/people/views/d;->c:I

    invoke-virtual {p2, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 314
    return-void
.end method

.method private a(Ljava/lang/String;Landroid/widget/ImageView;)V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 317
    if-nez p1, :cond_0

    .line 334
    :goto_0
    return-void

    .line 321
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->i:Lcom/google/android/gms/common/internal/a/a;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/a/a;->b(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 322
    if-eqz v0, :cond_1

    .line 323
    invoke-static {v0, p2}, Lcom/google/android/gms/common/people/views/AudienceView;->b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V

    .line 326
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-nez v0, :cond_2

    .line 327
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 331
    :cond_2
    sget-object v0, Lcom/google/android/gms/people/x;->g:Lcom/google/android/gms/people/o;

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v1, p1, v2, v2}, Lcom/google/android/gms/people/o;->a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;II)Lcom/google/android/gms/common/api/am;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/common/people/views/b;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/gms/common/people/views/b;-><init>(Lcom/google/android/gms/common/people/views/AudienceView;Ljava/lang/String;Landroid/widget/ImageView;)V

    invoke-interface {v0, v1}, Lcom/google/android/gms/common/api/am;->a(Lcom/google/android/gms/common/api/aq;)V

    goto :goto_0
.end method

.method private static b(Landroid/graphics/Bitmap;Landroid/widget/ImageView;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 337
    const/16 v0, 0xff

    invoke-virtual {p1, v0}, Landroid/widget/ImageView;->setAlpha(I)V

    .line 338
    invoke-virtual {p1, p0}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 339
    invoke-virtual {p1, v1, v1, v1, v1}, Landroid/widget/ImageView;->setPadding(IIII)V

    .line 340
    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/common/people/views/AudienceView;)Z
    .locals 1

    .prologue
    .line 51
    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    return v0
.end method

.method private d()V
    .locals 1

    .prologue
    .line 147
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->clear()V

    .line 148
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    .line 149
    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v0, :cond_0

    .line 150
    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->e()V

    .line 152
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    .line 153
    return-void
.end method

.method private e()V
    .locals 4

    .prologue
    .line 303
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 304
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    .line 305
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    sget v2, Lcom/google/android/gms/l;->af:I

    const/4 v3, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    .line 306
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/ArrayList;
    .locals 2

    .prologue
    .line 220
    new-instance v0, Ljava/util/ArrayList;

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->keySet()Ljava/util/Set;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/c;)V
    .locals 0

    .prologue
    .line 389
    return-void
.end method

.method public final a(Ljava/util/Collection;)V
    .locals 7

    .prologue
    .line 206
    if-eqz p1, :cond_5

    invoke-interface {p1}, Ljava/util/Collection;->size()I

    move-result v0

    if-lez v0, :cond_5

    .line 207
    invoke-interface {p1}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/people/data/AudienceMember;

    .line 208
    if-nez v0, :cond_1

    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "AudienceMember can not be null."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0}, Ljava/util/LinkedHashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    if-eqz v1, :cond_2

    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    :cond_2
    iget-boolean v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1}, Ljava/util/LinkedHashMap;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v1}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->removeAllViews()V

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->a:Landroid/view/LayoutInflater;

    sget v2, Lcom/google/android/gms/l;->ae:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v4

    sget v1, Lcom/google/android/gms/j;->cG:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    sget v1, Lcom/google/android/gms/j;->cJ:I

    invoke-virtual {v4, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    sget v2, Lcom/google/android/gms/j;->cI:I

    invoke-virtual {v4, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageView;

    sget v6, Lcom/google/android/gms/j;->cH:I

    invoke-virtual {v4, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v6

    invoke-static {v6, v0}, Lcom/google/android/gms/common/people/views/c;->a(Landroid/content/Context;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/views/d;

    move-result-object v6

    invoke-static {v5, v1, v2, v6}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/google/android/gms/common/people/views/d;)V

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->k()Z

    move-result v1

    if-eqz v1, :cond_4

    invoke-virtual {v0}, Lcom/google/android/gms/common/people/data/AudienceMember;->g()Ljava/lang/String;

    move-result-object v1

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->d:Ljava/util/LinkedHashMap;

    invoke-virtual {v1, v0, v4}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v4}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    goto :goto_0

    .line 210
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    if-eqz v0, :cond_6

    .line 211
    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->e()V

    .line 212
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    .line 214
    :cond_6
    return-void
.end method

.method public final a(Z)V
    .locals 2

    .prologue
    .line 248
    iget-object v1, p0, Lcom/google/android/gms/common/people/views/AudienceView;->c:Landroid/view/View;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 249
    return-void

    .line 248
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public final b()V
    .locals 1

    .prologue
    .line 252
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    .line 253
    return-void
.end method

.method public final b_(Landroid/os/Bundle;)V
    .locals 3

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 394
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/lang/String;Landroid/widget/ImageView;)V

    goto :goto_0

    .line 396
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->h:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->clear()V

    .line 397
    return-void
.end method

.method public final c()V
    .locals 9

    .prologue
    .line 285
    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->d()V

    .line 286
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    .line 288
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 289
    sget v1, Lcom/google/android/gms/l;->ae:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 290
    sget v0, Lcom/google/android/gms/j;->cG:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 291
    sget v0, Lcom/google/android/gms/j;->cJ:I

    invoke-virtual {v2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 292
    sget v1, Lcom/google/android/gms/j;->cI:I

    invoke-virtual {v2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 293
    sget v4, Lcom/google/android/gms/j;->cH:I

    invoke-virtual {v2, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    .line 295
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->getContext()Landroid/content/Context;

    move-result-object v4

    new-instance v5, Lcom/google/android/gms/common/people/views/d;

    sget v6, Lcom/google/android/gms/h;->v:I

    sget v7, Lcom/google/android/gms/p;->el:I

    invoke-virtual {v4, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    sget v7, Lcom/google/android/gms/h;->D:I

    const/4 v8, 0x0

    invoke-direct {v5, v6, v4, v7, v8}, Lcom/google/android/gms/common/people/views/d;-><init>(ILjava/lang/String;IB)V

    invoke-static {v3, v0, v1, v5}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Landroid/view/View;Landroid/widget/TextView;Landroid/widget/ImageView;Lcom/google/android/gms/common/people/views/d;)V

    .line 298
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->b:Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/plus/audience/widgets/MultiLineLayout;->addView(Landroid/view/View;)V

    .line 299
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->invalidate()V

    .line 300
    return-void
.end method

.method public final f_(I)V
    .locals 0

    .prologue
    .line 402
    return-void
.end method

.method protected onAttachedToWindow()V
    .locals 1

    .prologue
    .line 374
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    .line 375
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 376
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 380
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 381
    iget-object v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->f:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 382
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->g:Z

    .line 383
    return-void
.end method

.method protected onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 351
    check-cast p1, Landroid/os/Bundle;

    .line 352
    const-string v0, "parent"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    invoke-super {p0, v0}, Landroid/widget/FrameLayout;->onRestoreInstanceState(Landroid/os/Parcelable;)V

    .line 353
    const-string v0, "showOnlyYou"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    .line 354
    const-string v0, "showEmptyText"

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    .line 355
    iget-boolean v0, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    if-eqz v0, :cond_0

    .line 356
    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->c()V

    .line 360
    :goto_0
    return-void

    .line 358
    :cond_0
    const-string v0, "audience"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-direct {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->d()V

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Ljava/util/Collection;)V

    goto :goto_0
.end method

.method protected onSaveInstanceState()Landroid/os/Parcelable;
    .locals 3

    .prologue
    .line 364
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 365
    const-string v1, "parent"

    invoke-super {p0}, Landroid/widget/FrameLayout;->onSaveInstanceState()Landroid/os/Parcelable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 366
    const-string v1, "showOnlyYou"

    iget-boolean v2, p0, Lcom/google/android/gms/common/people/views/AudienceView;->j:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 367
    const-string v1, "showEmptyText"

    iget-boolean v2, p0, Lcom/google/android/gms/common/people/views/AudienceView;->k:Z

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 368
    const-string v1, "audience"

    invoke-virtual {p0}, Lcom/google/android/gms/common/people/views/AudienceView;->a()Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putParcelableArrayList(Ljava/lang/String;Ljava/util/ArrayList;)V

    .line 369
    return-object v0
.end method

.method public setClickable(Z)V
    .locals 0

    .prologue
    .line 243
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setClickable(Z)V

    .line 244
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/people/views/AudienceView;->a(Z)V

    .line 245
    return-void
.end method
