.class final Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;
.super Lcom/google/android/gms/games/service/statemachine/roomclient/ca;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field private e:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ca;-><init>(Lcom/google/android/gms/games/service/statemachine/roomclient/cb;)V

    .line 30
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/service/statemachine/roomclient/ce;)V
    .locals 0

    .prologue
    .line 37
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    .line 38
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->c()V

    .line 39
    return-void
.end method

.method public final a(Landroid/os/Message;)Z
    .locals 6

    .prologue
    .line 43
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 73
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->g:Z

    :goto_0
    return v0

    .line 46
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 47
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->f:Z

    goto :goto_0

    .line 49
    :sswitch_1
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;

    .line 50
    invoke-static {p0, p1}, Lcom/google/android/gms/games/service/statemachine/roomclient/cb;->a(Lcom/google/android/gms/games/service/statemachine/roomclient/ca;Landroid/os/Message;)V

    .line 51
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->a:Lcom/google/android/gms/games/service/statemachine/roomclient/cd;

    iget-object v1, v1, Lcom/google/android/gms/games/service/statemachine/roomclient/cd;->f:Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;

    iget-object v2, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-wide v2, v2, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->b:J

    iget-object v4, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v4, v4, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->c:Lcom/google/android/gms/common/server/ClientContext;

    iget-object v5, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/al;

    iget-object v5, v5, Lcom/google/android/gms/games/service/statemachine/roomclient/al;->d:Lcom/google/android/gms/games/internal/ConnectionInfo;

    invoke-virtual {v1, v2, v3, v4, v5}, Lcom/google/android/gms/games/service/statemachine/roomclient/NetworkConnectedState;->a(JLcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/ConnectionInfo;)V

    .line 54
    iget-object v2, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    iget v1, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->a:I

    if-nez v1, :cond_1

    const/4 v1, 0x1

    :goto_1
    invoke-virtual {v2, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->b(Z)V

    .line 55
    iget-object v1, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->e:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    invoke-virtual {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a()V

    .line 56
    iget v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/bk;->a:I

    if-eqz v0, :cond_0

    .line 59
    const-string v0, "RoomServiceClientStateMachine"

    const-string v1, "Disconnecting network because room leave failed!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 61
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    new-instance v1, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;

    invoke-direct {v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ae;-><init>()V

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/service/statemachine/d;->b(Lcom/google/android/gms/games/service/statemachine/j;)V

    .line 64
    :cond_0
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->f:Z

    goto :goto_0

    .line 54
    :cond_1
    const/4 v1, 0x0

    goto :goto_1

    .line 67
    :sswitch_2
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;

    .line 68
    iget-object v0, v0, Lcom/google/android/gms/games/service/statemachine/roomclient/ag;->n:Lcom/google/android/gms/games/service/statemachine/roomclient/ce;

    const/16 v1, 0xc

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ce;->a(I)V

    .line 69
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->c:Lcom/google/android/gms/games/service/statemachine/d;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/service/statemachine/d;->a(Landroid/os/Message;)V

    .line 70
    sget-boolean v0, Lcom/google/android/gms/games/service/statemachine/roomclient/WaitForHttpLeaveState;->f:Z

    goto :goto_0

    .line 43
    :sswitch_data_0
    .sparse-switch
        0x1d -> :sswitch_2
        0x27 -> :sswitch_0
        0x29 -> :sswitch_0
        0x2e -> :sswitch_1
    .end sparse-switch
.end method
