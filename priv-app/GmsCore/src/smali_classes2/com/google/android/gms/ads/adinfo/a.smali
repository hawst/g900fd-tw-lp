.class public final Lcom/google/android/gms/ads/adinfo/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field private static a:I


# instance fields
.field private final b:[B

.field private final c:[B

.field private final d:[B

.field private final e:Ljava/security/interfaces/RSAPublicKey;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const/16 v0, 0xb

    sput v0, Lcom/google/android/gms/ads/adinfo/a;->a:I

    return-void
.end method

.method public constructor <init>()V
    .locals 8

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 41
    sget-object v0, Lcom/google/android/gms/ads/a/b;->d:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v0}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 42
    sget-object v1, Lcom/google/android/gms/ads/a/b;->e:Lcom/google/android/gms/common/a/d;

    invoke-virtual {v1}, Lcom/google/android/gms/common/a/d;->c()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 43
    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/adinfo/a;->b:[B

    .line 45
    invoke-static {v1}, Lcom/google/android/gms/ads/adinfo/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/ads/adinfo/a;->c:[B

    .line 46
    new-instance v0, Ljava/security/spec/RSAPublicKeySpec;

    new-instance v1, Ljava/math/BigInteger;

    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/a;->b:[B

    invoke-direct {v1, v4, v2}, Ljava/math/BigInteger;-><init>(I[B)V

    new-instance v2, Ljava/math/BigInteger;

    iget-object v3, p0, Lcom/google/android/gms/ads/adinfo/a;->c:[B

    invoke-direct {v2, v4, v3}, Ljava/math/BigInteger;-><init>(I[B)V

    invoke-direct {v0, v1, v2}, Ljava/security/spec/RSAPublicKeySpec;-><init>(Ljava/math/BigInteger;Ljava/math/BigInteger;)V

    .line 48
    const-string v1, "RSA"

    const-string v2, "BC"

    invoke-static {v1, v2}, Ljava/security/KeyFactory;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljava/security/KeyFactory;

    move-result-object v1

    .line 49
    invoke-virtual {v1, v0}, Ljava/security/KeyFactory;->generatePublic(Ljava/security/spec/KeySpec;)Ljava/security/PublicKey;

    move-result-object v0

    check-cast v0, Ljava/security/interfaces/RSAPublicKey;

    iput-object v0, p0, Lcom/google/android/gms/ads/adinfo/a;->e:Ljava/security/interfaces/RSAPublicKey;

    .line 50
    invoke-direct {p0}, Lcom/google/android/gms/ads/adinfo/a;->a()[B

    move-result-object v0

    if-nez v0, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    :cond_0
    const/4 v1, 0x5

    new-array v1, v1, [B

    aput-byte v5, v1, v5

    aget-byte v2, v0, v5

    aput-byte v2, v1, v4

    aget-byte v2, v0, v4

    aput-byte v2, v1, v6

    aget-byte v2, v0, v6

    aput-byte v2, v1, v7

    const/4 v2, 0x4

    aget-byte v0, v0, v7

    aput-byte v0, v1, v2

    iput-object v1, p0, Lcom/google/android/gms/ads/adinfo/a;->d:[B

    .line 51
    return-void
.end method

.method private static a(Ljava/security/MessageDigest;[B)V
    .locals 4

    .prologue
    .line 142
    array-length v0, p1

    const/4 v1, 0x4

    new-array v1, v1, [B

    const/4 v2, 0x0

    shr-int/lit8 v3, v0, 0x18

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    const/4 v2, 0x1

    shr-int/lit8 v3, v0, 0x10

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    const/4 v2, 0x2

    shr-int/lit8 v3, v0, 0x8

    and-int/lit16 v3, v3, 0xff

    int-to-byte v3, v3

    aput-byte v3, v1, v2

    const/4 v2, 0x3

    and-int/lit16 v0, v0, 0xff

    int-to-byte v0, v0

    aput-byte v0, v1, v2

    invoke-virtual {p0, v1}, Ljava/security/MessageDigest;->update([B)V

    .line 143
    invoke-virtual {p0, p1}, Ljava/security/MessageDigest;->update([B)V

    .line 144
    return-void
.end method

.method private a()[B
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 120
    .line 124
    const/4 v0, 0x0

    move v2, v0

    move-object v0, v1

    :goto_0
    const/4 v3, 0x2

    if-ge v2, v3, :cond_0

    .line 126
    :try_start_0
    const-string v3, "SHA-1"

    invoke-static {v3}, Ljava/security/MessageDigest;->getInstance(Ljava/lang/String;)Ljava/security/MessageDigest;
    :try_end_0
    .catch Ljava/security/NoSuchAlgorithmException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 124
    :goto_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 131
    :cond_0
    if-nez v0, :cond_1

    .line 132
    const-string v0, "Hashing algorithm cannot be found"

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->d(Ljava/lang/String;)V

    move-object v0, v1

    .line 138
    :goto_2
    return-object v0

    .line 135
    :cond_1
    invoke-virtual {v0}, Ljava/security/MessageDigest;->reset()V

    .line 136
    iget-object v1, p0, Lcom/google/android/gms/ads/adinfo/a;->b:[B

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/adinfo/a;->a(Ljava/security/MessageDigest;[B)V

    .line 137
    iget-object v1, p0, Lcom/google/android/gms/ads/adinfo/a;->c:[B

    invoke-static {v0, v1}, Lcom/google/android/gms/ads/adinfo/a;->a(Ljava/security/MessageDigest;[B)V

    .line 138
    invoke-virtual {v0}, Ljava/security/MessageDigest;->digest()[B

    move-result-object v0

    goto :goto_2

    :catch_0
    move-exception v3

    goto :goto_1
.end method

.method private static a(Ljava/lang/String;)[B
    .locals 7

    .prologue
    const/16 v6, 0x10

    .line 80
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    rem-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 81
    :goto_0
    div-int/lit8 v1, v0, 0x2

    new-array v2, v1, [B

    .line 83
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v0, :cond_1

    .line 84
    div-int/lit8 v3, v1, 0x2

    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v4

    invoke-static {v4, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v4

    shl-int/lit8 v4, v4, 0x4

    add-int/lit8 v5, v1, 0x1

    invoke-virtual {p0, v5}, Ljava/lang/String;->charAt(I)C

    move-result v5

    invoke-static {v5, v6}, Ljava/lang/Character;->digit(CI)I

    move-result v5

    add-int/2addr v4, v5

    int-to-byte v4, v4

    aput-byte v4, v2, v3

    .line 83
    add-int/lit8 v1, v1, 0x2

    goto :goto_1

    .line 80
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 87
    :cond_1
    return-object v2
.end method

.method private a([B)[B
    .locals 6

    .prologue
    .line 92
    :try_start_0
    const-string v0, "RSA/None/OAEPPadding"

    const-string v1, "BC"

    invoke-static {v0, v1}, Ljavax/crypto/Cipher;->getInstance(Ljava/lang/String;Ljava/lang/String;)Ljavax/crypto/Cipher;

    move-result-object v0

    .line 94
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/a;->e:Ljava/security/interfaces/RSAPublicKey;

    invoke-virtual {v0, v1, v2}, Ljavax/crypto/Cipher;->init(ILjava/security/Key;)V

    .line 95
    invoke-virtual {v0, p1}, Ljavax/crypto/Cipher;->doFinal([B)[B

    move-result-object v1

    .line 99
    iget-object v0, p0, Lcom/google/android/gms/ads/adinfo/a;->d:[B

    array-length v0, v0

    array-length v2, v1

    add-int/2addr v0, v2

    new-array v0, v0, [B

    .line 100
    iget-object v2, p0, Lcom/google/android/gms/ads/adinfo/a;->d:[B

    const/4 v3, 0x0

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/gms/ads/adinfo/a;->d:[B

    array-length v5, v5

    invoke-static {v2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 101
    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/ads/adinfo/a;->d:[B

    array-length v3, v3

    array-length v4, v1

    invoke-static {v1, v2, v0, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    return-object v0

    .line 105
    :catch_0
    move-exception v0

    .line 106
    const-string v1, "Encryption failed"

    invoke-static {v1, v0}, Lcom/google/android/gms/ads/internal/util/client/b;->c(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 107
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/ads/identifier/c;)Ljava/lang/String;
    .locals 7

    .prologue
    const/4 v1, 0x0

    .line 54
    if-nez p1, :cond_0

    move-object v0, v1

    .line 64
    :goto_0
    return-object v0

    .line 57
    :cond_0
    const-string v2, "i1=%s&i2=%b"

    const/4 v0, 0x2

    new-array v3, v0, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v0, p1, Lcom/google/android/gms/ads/identifier/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_1

    const-string v5, "^[a-fA-F0-9]{8}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{4}-[a-fA-F0-9]{12}$"

    invoke-virtual {v0, v5}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    const-string v5, "-"

    const-string v6, ""

    invoke-virtual {v0, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/ads/adinfo/a;->a(Ljava/lang/String;)[B

    move-result-object v0

    sget v5, Lcom/google/android/gms/ads/adinfo/a;->a:I

    invoke-static {v0, v5}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    :cond_1
    aput-object v0, v3, v4

    const/4 v0, 0x1

    iget-boolean v4, p1, Lcom/google/android/gms/ads/identifier/c;->b:Z

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    aput-object v4, v3, v0

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 59
    const-string v2, "UTF-8"

    invoke-static {v2}, Ljava/nio/charset/Charset;->forName(Ljava/lang/String;)Ljava/nio/charset/Charset;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->getBytes(Ljava/nio/charset/Charset;)[B

    move-result-object v0

    .line 60
    invoke-direct {p0, v0}, Lcom/google/android/gms/ads/adinfo/a;->a([B)[B

    move-result-object v0

    .line 61
    if-nez v0, :cond_2

    move-object v0, v1

    .line 62
    goto :goto_0

    .line 64
    :cond_2
    sget v1, Lcom/google/android/gms/ads/adinfo/a;->a:I

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
