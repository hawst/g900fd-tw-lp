.class public final Lcom/google/android/gms/common/util/ae;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method public static a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;
    .locals 1

    .prologue
    .line 21
    const-string v0, "input_method"

    invoke-virtual {p0, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    return-object v0
.end method

.method public static a(Landroid/content/Context;Landroid/view/View;)Z
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 30
    invoke-static {p0}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    .line 31
    if-eqz v1, :cond_0

    .line 32
    invoke-virtual {v1, p1, v0}, Landroid/view/inputmethod/InputMethodManager;->showSoftInput(Landroid/view/View;I)Z

    .line 33
    const/4 v0, 0x1

    .line 36
    :cond_0
    return v0
.end method

.method public static b(Landroid/content/Context;Landroid/view/View;)Z
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 45
    invoke-static {p0}, Lcom/google/android/gms/common/util/ae;->a(Landroid/content/Context;)Landroid/view/inputmethod/InputMethodManager;

    move-result-object v1

    .line 46
    if-eqz v1, :cond_0

    .line 47
    invoke-virtual {p1}, Landroid/view/View;->getWindowToken()Landroid/os/IBinder;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Landroid/view/inputmethod/InputMethodManager;->hideSoftInputFromWindow(Landroid/os/IBinder;I)Z

    .line 48
    const/4 v0, 0x1

    .line 51
    :cond_0
    return v0
.end method
