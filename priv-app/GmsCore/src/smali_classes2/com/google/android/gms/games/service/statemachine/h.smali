.class public abstract Lcom/google/android/gms/games/service/statemachine/h;
.super Lcom/google/android/gms/common/util/a/c;
.source "SourceFile"


# instance fields
.field protected final i:Lcom/google/android/gms/games/service/statemachine/d;

.field protected j:Ljava/lang/Object;

.field protected k:Ljava/lang/Object;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 90
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/util/a/c;-><init>(Ljava/lang/String;)V

    .line 20
    new-instance v0, Lcom/google/android/gms/games/service/statemachine/i;

    invoke-direct {v0, p0}, Lcom/google/android/gms/games/service/statemachine/i;-><init>(Lcom/google/android/gms/games/service/statemachine/h;)V

    iput-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->i:Lcom/google/android/gms/games/service/statemachine/d;

    .line 91
    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;)Lcom/google/android/gms/common/util/a/a;
    .locals 1

    .prologue
    .line 16
    invoke-virtual {p0}, Lcom/google/android/gms/games/service/statemachine/h;->a()Lcom/google/android/gms/common/util/a/a;

    move-result-object v0

    return-object v0
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;Landroid/os/Message;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Landroid/os/Message;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/a;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/common/util/a/a;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/b;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Lcom/google/android/gms/common/util/a/b;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/games/service/statemachine/j;)V
    .locals 1

    .prologue
    .line 16
    invoke-interface {p1}, Lcom/google/android/gms/games/service/statemachine/j;->a()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->c(ILjava/lang/Object;)V

    return-void
.end method

.method static synthetic a(Lcom/google/android/gms/games/service/statemachine/h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/service/statemachine/h;Lcom/google/android/gms/common/util/a/b;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(Lcom/google/android/gms/common/util/a/b;)V

    return-void
.end method

.method static synthetic b(Lcom/google/android/gms/games/service/statemachine/h;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 16
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->a(Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/games/service/statemachine/j;)V
    .locals 1

    .prologue
    .line 132
    invoke-interface {p1}, Lcom/google/android/gms/games/service/statemachine/j;->a()I

    move-result v0

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/service/statemachine/h;->b(ILjava/lang/Object;)V

    .line 133
    return-void
.end method

.method protected final a(Ljava/lang/Object;)V
    .locals 0

    .prologue
    .line 99
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/h;->k:Ljava/lang/Object;

    .line 100
    return-void
.end method

.method protected final b(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->k:Ljava/lang/Object;

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 107
    iput-object p1, p0, Lcom/google/android/gms/games/service/statemachine/h;->j:Ljava/lang/Object;

    .line 108
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->j:Ljava/lang/Object;

    return-object v0
.end method

.method public final e()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 112
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->k:Ljava/lang/Object;

    return-object v0
.end method

.method public final f()Lcom/google/android/gms/games/service/statemachine/d;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->i:Lcom/google/android/gms/games/service/statemachine/d;

    return-object v0
.end method

.method public final g()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 120
    iget-object v0, p0, Lcom/google/android/gms/games/service/statemachine/h;->j:Ljava/lang/Object;

    return-object v0
.end method
