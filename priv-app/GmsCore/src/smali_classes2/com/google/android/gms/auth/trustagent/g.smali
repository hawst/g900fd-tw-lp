.class public final Lcom/google/android/gms/auth/trustagent/g;
.super Lcom/google/android/gms/auth/trustagent/a;
.source "SourceFile"


# instance fields
.field private c:Landroid/preference/Preference;

.field private d:Landroid/preference/Preference;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Lcom/google/android/gms/auth/trustagent/a;-><init>()V

    .line 86
    return-void
.end method


# virtual methods
.method final d()V
    .locals 3

    .prologue
    .line 71
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 72
    const-string v1, "com.android.facelock"

    const-string v2, "com.android.facelock.FaceLockReceiver"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 74
    const-string v1, "deleteGallery"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 75
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/g;->getActivity()Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/app/Activity;->sendBroadcast(Landroid/content/Intent;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/a;->a:Lcom/google/android/gms/auth/trustagent/am;

    const-string v1, "auth_trust_agent_pref_trustlet_enabled_FaceUnlockTrustlet"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/auth/trustagent/am;->a(Ljava/lang/String;Z)V

    .line 78
    invoke-virtual {p0}, Lcom/google/android/gms/auth/trustagent/g;->getActivity()Landroid/app/Activity;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/Activity;->finish()V

    .line 79
    return-void
.end method

.method public final onCreate(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 37
    invoke-super {p0, p1}, Lcom/google/android/gms/auth/trustagent/a;->onCreate(Landroid/os/Bundle;)V

    .line 38
    sget v0, Lcom/google/android/gms/s;->c:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/g;->addPreferencesFromResource(I)V

    .line 40
    const-string v0, "auth_trust_agent_pref_face_unlock_improve_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/g;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/g;->c:Landroid/preference/Preference;

    .line 41
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/g;->c:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/h;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/h;-><init>(Lcom/google/android/gms/auth/trustagent/g;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 52
    const-string v0, "auth_trust_agent_pref_face_unlock_reset_key"

    invoke-virtual {p0, v0}, Lcom/google/android/gms/auth/trustagent/g;->findPreference(Ljava/lang/CharSequence;)Landroid/preference/Preference;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/auth/trustagent/g;->d:Landroid/preference/Preference;

    .line 53
    iget-object v0, p0, Lcom/google/android/gms/auth/trustagent/g;->d:Landroid/preference/Preference;

    new-instance v1, Lcom/google/android/gms/auth/trustagent/i;

    invoke-direct {v1, p0}, Lcom/google/android/gms/auth/trustagent/i;-><init>(Lcom/google/android/gms/auth/trustagent/g;)V

    invoke-virtual {v0, v1}, Landroid/preference/Preference;->setOnPreferenceClickListener(Landroid/preference/Preference$OnPreferenceClickListener;)V

    .line 61
    return-void
.end method
