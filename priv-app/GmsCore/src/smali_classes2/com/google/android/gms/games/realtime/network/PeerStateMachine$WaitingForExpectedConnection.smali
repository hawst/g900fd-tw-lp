.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field a:I

.field final synthetic f:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;IILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 6

    .prologue
    .line 398
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->f:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    .line 399
    const/4 v3, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v4, p4

    move-object v5, p5

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 400
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->a:I

    .line 401
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 403
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 8

    .prologue
    .line 407
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 421
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 409
    :pswitch_0
    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    move-object v4, v0

    check-cast v4, Lcom/google/android/gms/games/jingle/d;

    .line 411
    iget v0, v4, Lcom/google/android/gms/games/jingle/d;->d:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 412
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->f:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a:Lcom/google/android/gms/games/jingle/Libjingle;

    iget-object v1, v4, Lcom/google/android/gms/games/jingle/d;->c:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/jingle/Libjingle;->a(Ljava/lang/String;)V

    .line 413
    iget-object v7, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->f:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->f:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->b:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->a:I

    iget-object v4, v4, Lcom/google/android/gms/games/jingle/d;->c:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->c:I

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForExpectedConnection;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$WaitingForConnectionData;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;ILjava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v7, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    .line 417
    :cond_0
    const/4 v0, 0x1

    goto :goto_0

    .line 407
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
    .end packed-switch
.end method
