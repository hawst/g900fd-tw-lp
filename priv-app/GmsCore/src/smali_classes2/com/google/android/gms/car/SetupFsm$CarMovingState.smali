.class public Lcom/google/android/gms/car/SetupFsm$CarMovingState;
.super Lcom/google/android/car/fsm/j;
.source "SourceFile"


# annotations
.annotation runtime Lcom/google/android/car/fsm/l;
    a = {
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_ERROR"
            b = Lcom/google/android/gms/car/SetupFsm$ErrorState;
            c = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_DISCONNECTED"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_USER_EXIT"
            b = Lcom/google/android/gms/car/SetupFsm$SetupFailedState;
            c = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation,
        .subannotation Lcom/google/android/car/fsm/k;
            a = "EVENT_CAR_PARKED"
            c = Lcom/google/android/gms/car/SetupFsm$CarMovingState;
        .end subannotation
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 612
    invoke-direct {p0}, Lcom/google/android/car/fsm/j;-><init>()V

    return-void
.end method


# virtual methods
.method public final a()V
    .locals 2

    .prologue
    .line 615
    iget-object v0, p0, Lcom/google/android/car/fsm/j;->a:Lcom/google/android/car/fsm/FsmController;

    const-class v1, Lcom/google/android/gms/car/ps;

    invoke-virtual {v0, v1}, Lcom/google/android/car/fsm/FsmController;->a(Ljava/lang/Class;)V

    .line 616
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 620
    const-string v0, "EVENT_ERROR"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_DISCONNECTED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_USER_EXIT"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "EVENT_CAR_PARKED"

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 625
    :cond_0
    const/4 v0, 0x0

    .line 629
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method
