.class public final Lcom/google/android/gms/car/senderprotocol/k;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field private final A:Lcom/google/android/gms/car/no;

.field private final B:Lcom/google/android/gms/car/senderprotocol/n;

.field private final C:Lcom/google/android/gms/car/senderprotocol/n;

.field private final D:Lcom/google/android/gms/car/senderprotocol/n;

.field private final E:Lcom/google/android/gms/car/senderprotocol/n;

.field private final F:Lcom/google/android/gms/car/senderprotocol/af;

.field public final a:Lcom/google/android/gms/car/senderprotocol/o;

.field public b:Lcom/google/android/gms/car/senderprotocol/y;

.field public c:[Lcom/google/android/gms/car/senderprotocol/e;

.field final d:Ljava/lang/Object;

.field e:Z

.field volatile f:Z

.field public g:Z

.field public final h:Lcom/google/android/gms/car/senderprotocol/p;

.field final i:[Lcom/google/android/gms/car/senderprotocol/p;

.field public final j:Lcom/google/android/gms/car/senderprotocol/p;

.field public final k:Lcom/google/android/gms/car/senderprotocol/p;

.field public final l:Lcom/google/android/gms/car/senderprotocol/p;

.field public final m:Lcom/google/android/gms/car/senderprotocol/p;

.field public final n:Lcom/google/android/gms/car/senderprotocol/p;

.field public final o:Lcom/google/android/gms/car/senderprotocol/p;

.field public final p:Lcom/google/android/gms/car/senderprotocol/p;

.field public final q:Lcom/google/android/gms/car/senderprotocol/p;

.field public final r:Lcom/google/android/gms/car/senderprotocol/p;

.field public final s:Ljava/util/HashMap;

.field public final t:Lcom/google/android/gms/car/senderprotocol/q;

.field public final u:Lcom/google/android/gms/car/senderprotocol/n;

.field final v:Lcom/google/android/gms/car/eb;

.field private final w:Lcom/google/android/gms/car/no;

.field private final x:Lcom/google/android/gms/car/no;

.field private final y:Lcom/google/android/gms/car/no;

.field private final z:Lcom/google/android/gms/car/no;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/senderprotocol/o;Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V
    .locals 6

    .prologue
    .line 221
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->d:Ljava/lang/Object;

    .line 161
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->f:Z

    .line 169
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->g:Z

    .line 171
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->h:Lcom/google/android/gms/car/senderprotocol/p;

    .line 173
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->j:Lcom/google/android/gms/car/senderprotocol/p;

    .line 174
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->k:Lcom/google/android/gms/car/senderprotocol/p;

    .line 175
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->l:Lcom/google/android/gms/car/senderprotocol/p;

    .line 176
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->m:Lcom/google/android/gms/car/senderprotocol/p;

    .line 177
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->n:Lcom/google/android/gms/car/senderprotocol/p;

    .line 178
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->o:Lcom/google/android/gms/car/senderprotocol/p;

    .line 179
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->p:Lcom/google/android/gms/car/senderprotocol/p;

    .line 180
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->q:Lcom/google/android/gms/car/senderprotocol/p;

    .line 181
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v0, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->r:Lcom/google/android/gms/car/senderprotocol/p;

    .line 183
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->s:Ljava/util/HashMap;

    .line 222
    iput-object p8, p0, Lcom/google/android/gms/car/senderprotocol/k;->v:Lcom/google/android/gms/car/eb;

    .line 223
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/q;

    move-object v1, p4

    move-object v2, p5

    move-object v3, p6

    move-object v4, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/q;-><init>(Lcom/google/android/gms/car/senderprotocol/w;Lcom/google/android/gms/car/senderprotocol/v;Lcom/google/android/gms/car/senderprotocol/u;Lcom/google/android/gms/car/senderprotocol/t;Lcom/google/android/gms/car/eb;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    .line 225
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/gms/car/senderprotocol/p;

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->i:[Lcom/google/android/gms/car/senderprotocol/p;

    .line 226
    const/4 v0, 0x0

    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->i:[Lcom/google/android/gms/car/senderprotocol/p;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 227
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->i:[Lcom/google/android/gms/car/senderprotocol/p;

    new-instance v2, Lcom/google/android/gms/car/senderprotocol/p;

    invoke-direct {v2, p0}, Lcom/google/android/gms/car/senderprotocol/p;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    aput-object v2, v1, v0

    .line 226
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 229
    :cond_0
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/k;->a:Lcom/google/android/gms/car/senderprotocol/o;

    .line 230
    const/16 v0, 0x100

    new-array v0, v0, [Lcom/google/android/gms/car/senderprotocol/e;

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    .line 231
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->e:Z

    .line 234
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "RxDef"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->w:Lcom/google/android/gms/car/no;

    .line 235
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->w:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 236
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "RxAud"

    const/16 v2, -0x13

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->x:Lcom/google/android/gms/car/no;

    .line 237
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->x:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 238
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "RxMic"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->y:Lcom/google/android/gms/car/no;

    .line 239
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->y:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 240
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "RxVid"

    const/4 v2, -0x8

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;I)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->z:Lcom/google/android/gms/car/no;

    .line 241
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->z:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 242
    new-instance v0, Lcom/google/android/gms/car/no;

    const-string v1, "RxSen"

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/no;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->A:Lcom/google/android/gms/car/no;

    .line 243
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->A:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->start()V

    .line 244
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/n;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->w:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/n;-><init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->u:Lcom/google/android/gms/car/senderprotocol/n;

    .line 245
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/n;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->x:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/n;-><init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->B:Lcom/google/android/gms/car/senderprotocol/n;

    .line 246
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/n;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->y:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/n;-><init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->C:Lcom/google/android/gms/car/senderprotocol/n;

    .line 247
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/n;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->z:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/n;-><init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->D:Lcom/google/android/gms/car/senderprotocol/n;

    .line 248
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/n;

    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->A:Lcom/google/android/gms/car/no;

    invoke-virtual {v1}, Lcom/google/android/gms/car/no;->b()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/n;-><init>(Lcom/google/android/gms/car/senderprotocol/k;Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->E:Lcom/google/android/gms/car/senderprotocol/n;

    .line 250
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/m;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/senderprotocol/m;-><init>(Lcom/google/android/gms/car/senderprotocol/k;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->F:Lcom/google/android/gms/car/senderprotocol/af;

    .line 252
    new-instance v4, Lcom/google/android/gms/car/senderprotocol/l;

    invoke-direct {v4, p0}, Lcom/google/android/gms/car/senderprotocol/l;-><init>(Lcom/google/android/gms/car/senderprotocol/k;)V

    .line 282
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/y;

    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/k;->F:Lcom/google/android/gms/car/senderprotocol/af;

    const/4 v5, 0x0

    move-object v1, p2

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/y;-><init>(Ljava/io/InputStream;Ljava/io/OutputStream;Lcom/google/android/gms/car/senderprotocol/af;Lcom/google/android/gms/car/senderprotocol/z;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    .line 283
    return-void
.end method


# virtual methods
.method public final a(ILcom/google/android/gms/car/senderprotocol/az;I)Lcom/google/android/gms/car/senderprotocol/e;
    .locals 8

    .prologue
    const/4 v7, 0x3

    .line 466
    iget-object v6, p0, Lcom/google/android/gms/car/senderprotocol/k;->d:Ljava/lang/Object;

    monitor-enter v6

    .line 471
    :try_start_0
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/k;->u:Lcom/google/android/gms/car/senderprotocol/n;

    .line 472
    const/4 v0, 0x2

    if-ne p3, v0, :cond_3

    .line 473
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/k;->B:Lcom/google/android/gms/car/senderprotocol/n;

    .line 481
    :cond_0
    :goto_0
    new-instance v0, Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v2, 0x0

    move v1, p1

    move-object v3, p0

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/car/senderprotocol/e;-><init>(IILcom/google/android/gms/car/senderprotocol/k;Lcom/google/android/gms/car/senderprotocol/f;Landroid/os/Handler;)V

    .line 483
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    aput-object v0, v1, p1

    .line 484
    invoke-virtual {p2, v0}, Lcom/google/android/gms/car/senderprotocol/az;->a(Lcom/google/android/gms/car/senderprotocol/e;)V

    .line 485
    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->d()V

    .line 487
    iget-boolean v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->e:Z

    if-nez v1, :cond_1

    if-ne p3, v7, :cond_2

    .line 490
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->e()V

    .line 492
    :cond_2
    monitor-exit v6

    return-object v0

    .line 474
    :cond_3
    const/4 v0, 0x1

    if-ne p3, v0, :cond_4

    .line 475
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/k;->D:Lcom/google/android/gms/car/senderprotocol/n;

    goto :goto_0

    .line 476
    :cond_4
    if-ne p3, v7, :cond_5

    .line 477
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/k;->E:Lcom/google/android/gms/car/senderprotocol/n;

    goto :goto_0

    .line 478
    :cond_5
    const/4 v0, 0x4

    if-ne p3, v0, :cond_0

    .line 479
    iget-object v5, p0, Lcom/google/android/gms/car/senderprotocol/k;->C:Lcom/google/android/gms/car/senderprotocol/n;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 493
    :catchall_0
    move-exception v0

    monitor-exit v6

    throw v0
.end method

.method public final a()V
    .locals 1

    .prologue
    .line 405
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->f:Z

    .line 406
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->t:Lcom/google/android/gms/car/senderprotocol/q;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/q;->c()V

    .line 407
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->w:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 408
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->x:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 409
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->y:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 410
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->z:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 411
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->A:Lcom/google/android/gms/car/no;

    invoke-virtual {v0}, Lcom/google/android/gms/car/no;->a()V

    .line 412
    return-void
.end method

.method public final a(I)V
    .locals 3

    .prologue
    .line 297
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/k;->d:Ljava/lang/Object;

    monitor-enter v1

    .line 298
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v2, 0x0

    aput-object v2, v0, p1

    .line 299
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final a(ILjava/nio/ByteBuffer;ZZ)V
    .locals 1

    .prologue
    .line 506
    iget-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->f:Z

    if-eqz v0, :cond_0

    .line 510
    :goto_0
    return-void

    .line 509
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/gms/car/senderprotocol/y;->a(ILjava/nio/ByteBuffer;ZZ)V

    goto :goto_0
.end method

.method public final a(Z)V
    .locals 5

    .prologue
    .line 355
    invoke-virtual {p0}, Lcom/google/android/gms/car/senderprotocol/k;->a()V

    .line 356
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->e:Z

    .line 357
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v2, p0, Lcom/google/android/gms/car/senderprotocol/k;->d:Ljava/lang/Object;

    monitor-enter v2

    const/4 v0, 0x1

    :goto_0
    const/16 v3, 0xff

    if-gt v0, v3, :cond_1

    :try_start_0
    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    aget-object v3, v3, v0

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    aget-object v3, v3, v0

    invoke-interface {v1, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/google/android/gms/car/senderprotocol/k;->c:[Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v4, 0x0

    aput-object v4, v3, v0

    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    invoke-interface {v1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/senderprotocol/e;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/e;->h()V

    goto :goto_1

    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 358
    :cond_2
    if-eqz p1, :cond_3

    .line 360
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/y;->c()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    .line 366
    :cond_3
    :goto_2
    :try_start_2
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->b:Lcom/google/android/gms/car/senderprotocol/y;

    invoke-virtual {v0}, Lcom/google/android/gms/car/senderprotocol/y;->b()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_0

    .line 370
    :goto_3
    return-void

    :catch_0
    move-exception v0

    goto :goto_3

    :catch_1
    move-exception v0

    goto :goto_2
.end method

.method public final b(I)Lcom/google/android/gms/car/senderprotocol/p;
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/k;->i:[Lcom/google/android/gms/car/senderprotocol/p;

    add-int/lit8 v1, p1, -0x1

    aget-object v0, v0, v1

    return-object v0
.end method
