.class public final Lcom/google/android/gms/games/service/a/j/a;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/a/au;

.field private final d:Lcom/google/android/gms/games/internal/dr;

.field private final e:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 21
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 22
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/j/a;->c:Lcom/google/android/gms/games/a/au;

    .line 23
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/j/a;->d:Lcom/google/android/gms/games/internal/dr;

    .line 24
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/j/a;->e:Ljava/lang/String;

    .line 25
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/a;->d:Lcom/google/android/gms/games/internal/dr;

    invoke-interface {v0, p1}, Lcom/google/android/gms/games/internal/dr;->J(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 39
    return-void
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 2

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/j/a;->c:Lcom/google/android/gms/games/a/au;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/j/a;->e:Ljava/lang/String;

    invoke-virtual {p2, v0, v1}, Lcom/google/android/gms/games/a/t;->i(Lcom/google/android/gms/games/a/au;Ljava/lang/String;)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    return-object v0
.end method
