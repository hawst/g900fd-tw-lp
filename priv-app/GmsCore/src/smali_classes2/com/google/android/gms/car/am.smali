.class final Lcom/google/android/gms/car/am;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/lang/Runnable;


# instance fields
.field public a:Landroid/content/ComponentName;

.field public b:I

.field public c:Landroid/content/Intent;

.field public d:Landroid/content/Intent;

.field final synthetic e:Lcom/google/android/gms/car/ae;


# direct methods
.method private constructor <init>(Lcom/google/android/gms/car/ae;)V
    .locals 0

    .prologue
    .line 2030
    iput-object p1, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/gms/car/ae;B)V
    .locals 0

    .prologue
    .line 2030
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/am;-><init>(Lcom/google/android/gms/car/ae;)V

    return-void
.end method


# virtual methods
.method public final run()V
    .locals 5

    .prologue
    const/4 v4, 0x3

    .line 2039
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->d(Lcom/google/android/gms/car/ae;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 2040
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    .line 2140
    :goto_0
    return-void

    .line 2043
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->i(Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/on;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/car/on;->d()Z

    move-result v0

    if-nez v0, :cond_2

    .line 2045
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->j(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/List;

    .line 2046
    if-nez v0, :cond_1

    .line 2047
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 2048
    iget-object v1, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v1}, Lcom/google/android/gms/car/ae;->j(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;

    move-result-object v1

    iget v2, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2, v0}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2050
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/car/am;->c:Landroid/content/Intent;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 2051
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    goto :goto_0

    .line 2056
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->k(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-virtual {v0, v1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/car/y;

    .line 2057
    iget-object v1, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v1}, Lcom/google/android/gms/car/ae;->l(Lcom/google/android/gms/car/ae;)Ljava/util/Map;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    invoke-interface {v1, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/car/y;

    .line 2058
    const/4 v2, 0x0

    .line 2059
    if-eqz v0, :cond_d

    if-eq v0, v1, :cond_d

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->o()Z

    move-result v3

    if-nez v3, :cond_d

    invoke-virtual {v0}, Lcom/google/android/gms/car/y;->m()I

    move-result v3

    if-eqz v3, :cond_d

    .line 2066
    :goto_1
    if-nez v1, :cond_7

    .line 2067
    const-string v1, "CAR.CAM"

    invoke-static {v1, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 2068
    const-string v1, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client not found in active client list, starting new client for "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v3, p0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    invoke-virtual {v3}, Landroid/content/ComponentName;->toShortString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2071
    :cond_3
    new-instance v1, Lcom/google/android/gms/car/y;

    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    iget-object v3, p0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    invoke-direct {v1, v2, v3}, Lcom/google/android/gms/car/y;-><init>(Lcom/google/android/gms/car/ae;Landroid/content/ComponentName;)V

    .line 2072
    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->i(Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/on;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/car/on;->a(ILcom/google/android/gms/car/om;)Lcom/google/android/gms/car/oi;

    move-result-object v2

    .line 2074
    if-nez v2, :cond_4

    .line 2075
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    goto/16 :goto_0

    .line 2078
    :cond_4
    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/oi;)V

    .line 2079
    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->l(Lcom/google/android/gms/car/ae;)Ljava/util/Map;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/car/am;->a:Landroid/content/ComponentName;

    invoke-interface {v2, v3, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 2102
    :cond_5
    iget-object v2, p0, Lcom/google/android/gms/car/am;->c:Landroid/content/Intent;

    iget-object v3, p0, Lcom/google/android/gms/car/am;->d:Landroid/content/Intent;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/gms/car/y;->a(Landroid/content/Intent;Landroid/content/Intent;)V

    .line 2103
    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->k(Lcom/google/android/gms/car/ae;)Landroid/util/SparseArray;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-virtual {v2, v3, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 2105
    if-eqz v0, :cond_6

    .line 2106
    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->m(Lcom/google/android/gms/car/ae;)V

    .line 2107
    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Lcom/google/android/gms/car/y;->a(I)V

    .line 2109
    :cond_6
    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->f()Z

    move-result v0

    if-nez v0, :cond_c

    .line 2110
    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->d()Z

    move-result v0

    .line 2111
    if-nez v0, :cond_b

    .line 2112
    const-string v0, "CAR.CAM"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Error binding to requested service for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/am;->c:Landroid/content/Intent;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 2113
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    goto/16 :goto_0

    .line 2082
    :cond_7
    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->g()Lcom/google/android/gms/car/oi;

    move-result-object v2

    if-nez v2, :cond_9

    .line 2083
    iget-object v2, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v2}, Lcom/google/android/gms/car/ae;->i(Lcom/google/android/gms/car/ae;)Lcom/google/android/gms/car/on;

    move-result-object v2

    iget v3, p0, Lcom/google/android/gms/car/am;->b:I

    invoke-virtual {v2, v3, v1}, Lcom/google/android/gms/car/on;->a(ILcom/google/android/gms/car/om;)Lcom/google/android/gms/car/oi;

    move-result-object v2

    .line 2085
    if-nez v2, :cond_8

    .line 2086
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    goto/16 :goto_0

    .line 2089
    :cond_8
    invoke-virtual {v1, v2}, Lcom/google/android/gms/car/y;->a(Lcom/google/android/gms/car/oi;)V

    .line 2092
    :cond_9
    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->p()Z

    move-result v2

    if-eqz v2, :cond_5

    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->n()Z

    move-result v2

    if-eqz v2, :cond_5

    .line 2093
    const-string v0, "CAR.CAM"

    invoke-static {v0, v4}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 2094
    const-string v0, "CAR.CAM"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Client is already started, sending new intent"

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/gms/car/y;->k()Landroid/content/ComponentName;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/ComponentName;->getShortClassName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 2097
    :cond_a
    iget-object v0, p0, Lcom/google/android/gms/car/am;->c:Landroid/content/Intent;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/car/y;->a(Landroid/content/Intent;)V

    .line 2098
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0}, Lcom/google/android/gms/car/ae;->h(Lcom/google/android/gms/car/ae;)V

    goto/16 :goto_0

    .line 2116
    :cond_b
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/y;

    goto/16 :goto_0

    .line 2120
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ae;->a(Lcom/google/android/gms/car/ae;Lcom/google/android/gms/car/y;)Lcom/google/android/gms/car/y;

    .line 2121
    new-instance v0, Lcom/google/android/gms/car/an;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/an;-><init>(Lcom/google/android/gms/car/am;Lcom/google/android/gms/car/y;)V

    .line 2138
    iget-object v1, p0, Lcom/google/android/gms/car/am;->e:Lcom/google/android/gms/car/ae;

    invoke-static {v1}, Lcom/google/android/gms/car/ae;->o(Lcom/google/android/gms/car/ae;)Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto/16 :goto_0

    :cond_d
    move-object v0, v2

    goto/16 :goto_1
.end method
