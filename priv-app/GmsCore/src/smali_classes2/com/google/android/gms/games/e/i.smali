.class public final Lcom/google/android/gms/games/e/i;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/e/e;


# instance fields
.field private final a:J

.field private final b:Ljava/lang/String;

.field private final c:Ljava/lang/String;

.field private final d:J

.field private final e:J

.field private final f:Ljava/lang/String;

.field private final g:Landroid/net/Uri;

.field private final h:Landroid/net/Uri;

.field private final i:Lcom/google/android/gms/games/PlayerEntity;

.field private final j:Ljava/lang/String;

.field private final k:Ljava/lang/String;

.field private final l:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/e/e;)V
    .locals 2

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/e/i;->a:J

    .line 37
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->b:Ljava/lang/String;

    .line 38
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->c:Ljava/lang/String;

    .line 39
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->f()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/e/i;->d:J

    .line 40
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->g()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/games/e/i;->e:J

    .line 41
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->f:Ljava/lang/String;

    .line 42
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->i()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->g:Landroid/net/Uri;

    .line 43
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->k()Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->h:Landroid/net/Uri;

    .line 44
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    .line 45
    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    .line 46
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->n()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->j:Ljava/lang/String;

    .line 47
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->j()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->k:Ljava/lang/String;

    .line 48
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->l()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/e/i;->l:Ljava/lang/String;

    .line 49
    return-void

    .line 45
    :cond_0
    invoke-interface {v0}, Lcom/google/android/gms/games/Player;->c()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/PlayerEntity;

    goto :goto_0
.end method

.method static a(Lcom/google/android/gms/games/e/e;)I
    .locals 4

    .prologue
    .line 168
    const/16 v0, 0x9

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x3

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x4

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x5

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x6

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->i()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x7

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->k()Landroid/net/Uri;

    move-result-object v2

    aput-object v2, v0, v1

    const/16 v1, 0x8

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method static a(Lcom/google/android/gms/games/e/e;Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 180
    instance-of v2, p1, Lcom/google/android/gms/games/e/e;

    if-nez v2, :cond_1

    .line 188
    :cond_0
    :goto_0
    return v0

    .line 183
    :cond_1
    if-ne p0, p1, :cond_2

    move v0, v1

    .line 184
    goto :goto_0

    .line 187
    :cond_2
    check-cast p1, Lcom/google/android/gms/games/e/e;

    .line 188
    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->f()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->g()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->i()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->i()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->k()Landroid/net/Uri;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->k()Landroid/net/Uri;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {p1}, Lcom/google/android/gms/games/e/e;->n()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->n()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto/16 :goto_0
.end method

.method static b(Lcom/google/android/gms/games/e/e;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 209
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "Rank"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->a()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "DisplayRank"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->d()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "Score"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->f()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "DisplayScore"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->e()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "Timestamp"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->g()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "DisplayName"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->h()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "IconImageUri"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->i()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "IconImageUrl"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->j()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "HiResImageUri"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->k()Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "HiResImageUrl"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->l()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v1

    const-string v2, "Player"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v2, v0}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "ScoreTag"

    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->n()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    invoke-interface {p0}, Lcom/google/android/gms/games/e/e;->m()Lcom/google/android/gms/games/Player;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public final a()J
    .locals 2

    .prologue
    .line 53
    iget-wide v0, p0, Lcom/google/android/gms/games/e/i;->a:J

    return-wide v0
.end method

.method public final a(Landroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->c:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/q;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    .line 74
    return-void
.end method

.method public final b(Landroid/database/CharArrayBuffer;)V
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->f:Ljava/lang/String;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/q;->a(Ljava/lang/String;Landroid/database/CharArrayBuffer;)V

    .line 101
    :goto_0
    return-void

    .line 100
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/PlayerEntity;->a(Landroid/database/CharArrayBuffer;)V

    goto :goto_0
.end method

.method public final bridge synthetic c()Ljava/lang/Object;
    .locals 0

    .prologue
    .line 20
    return-object p0
.end method

.method public final d()Ljava/lang/String;
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->b:Ljava/lang/String;

    return-object v0
.end method

.method public final e()Ljava/lang/String;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->c:Ljava/lang/String;

    return-object v0
.end method

.method public final equals(Ljava/lang/Object;)Z
    .locals 1

    .prologue
    .line 176
    invoke-static {p0, p1}, Lcom/google/android/gms/games/e/i;->a(Lcom/google/android/gms/games/e/e;Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final f()J
    .locals 2

    .prologue
    .line 78
    iget-wide v0, p0, Lcom/google/android/gms/games/e/i;->d:J

    return-wide v0
.end method

.method public final g()J
    .locals 2

    .prologue
    .line 83
    iget-wide v0, p0, Lcom/google/android/gms/games/e/i;->e:J

    return-wide v0
.end method

.method public final h()Ljava/lang/String;
    .locals 1

    .prologue
    .line 88
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 89
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->f:Ljava/lang/String;

    .line 91
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->v_()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final hashCode()I
    .locals 1

    .prologue
    .line 164
    invoke-static {p0}, Lcom/google/android/gms/games/e/i;->a(Lcom/google/android/gms/games/e/e;)I

    move-result v0

    return v0
.end method

.method public final i()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 106
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->g:Landroid/net/Uri;

    .line 108
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->e()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final j()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->k:Ljava/lang/String;

    .line 119
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->f()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final k()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->h:Landroid/net/Uri;

    .line 127
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->g()Landroid/net/Uri;

    move-result-object v0

    goto :goto_0
.end method

.method public final l()Ljava/lang/String;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    if-nez v0, :cond_0

    .line 136
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->l:Ljava/lang/String;

    .line 138
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    invoke-virtual {v0}, Lcom/google/android/gms/games/PlayerEntity;->h()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final m()Lcom/google/android/gms/games/Player;
    .locals 1

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->i:Lcom/google/android/gms/games/PlayerEntity;

    return-object v0
.end method

.method public final n()Ljava/lang/String;
    .locals 1

    .prologue
    .line 148
    iget-object v0, p0, Lcom/google/android/gms/games/e/i;->j:Ljava/lang/String;

    return-object v0
.end method

.method public final toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 205
    invoke-static {p0}, Lcom/google/android/gms/games/e/i;->b(Lcom/google/android/gms/games/e/e;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final z_()Z
    .locals 1

    .prologue
    .line 159
    const/4 v0, 0x1

    return v0
.end method
