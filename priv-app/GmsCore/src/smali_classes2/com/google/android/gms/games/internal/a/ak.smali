.class public final Lcom/google/android/gms/games/internal/a/ak;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/games/p;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 58
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 145
    new-instance v0, Lcom/google/android/gms/games/internal/a/al;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/al;-><init>(Lcom/google/android/gms/games/internal/a/ak;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;ZLandroid/os/Bundle;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 253
    new-instance v0, Lcom/google/android/gms/games/internal/a/aq;

    invoke-direct {v0, p0, p1, p2, p3}, Lcom/google/android/gms/games/internal/a/aq;-><init>(Lcom/google/android/gms/games/internal/a/ak;Lcom/google/android/gms/common/api/v;ZLandroid/os/Bundle;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;)V
    .locals 1

    .prologue
    .line 115
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->v()V

    .line 116
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;I)V
    .locals 1

    .prologue
    .line 100
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->b(I)V

    .line 101
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 109
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2, p3}, Lcom/google/android/gms/games/internal/c;->c(Ljava/lang/String;I)V

    .line 111
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/v;Z)V
    .locals 1

    .prologue
    .line 138
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0, p2}, Lcom/google/android/gms/games/internal/c;->c(Z)V

    .line 140
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/v;Ljava/lang/String;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 177
    new-instance v0, Lcom/google/android/gms/games/internal/a/an;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/gms/games/internal/a/an;-><init>(Lcom/google/android/gms/games/internal/a/ak;Lcom/google/android/gms/common/api/v;Ljava/lang/String;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/common/api/v;)Z
    .locals 1

    .prologue
    .line 120
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->w()Z

    move-result v0

    return v0
.end method

.method public final c(Lcom/google/android/gms/common/api/v;)V
    .locals 2

    .prologue
    .line 126
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/internal/c;->b(Z)V

    .line 128
    return-void
.end method

.method public final d(Lcom/google/android/gms/common/api/v;)Z
    .locals 1

    .prologue
    .line 132
    invoke-static {p1}, Lcom/google/android/gms/games/d;->a(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/games/internal/c;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/internal/c;->x()Z

    move-result v0

    return v0
.end method

.method public final e(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 2

    .prologue
    .line 242
    new-instance v0, Lcom/google/android/gms/games/internal/a/ap;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/games/internal/a/ap;-><init>(Lcom/google/android/gms/games/internal/a/ak;Lcom/google/android/gms/common/api/v;Z)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method

.method public final f(Lcom/google/android/gms/common/api/v;)Lcom/google/android/gms/common/api/am;
    .locals 1

    .prologue
    .line 264
    new-instance v0, Lcom/google/android/gms/games/internal/a/ar;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/games/internal/a/ar;-><init>(Lcom/google/android/gms/games/internal/a/ak;Lcom/google/android/gms/common/api/v;)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/v;->a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    move-result-object v0

    return-object v0
.end method
