.class public Lcom/google/android/gms/mdm/LockscreenActivity;
.super Landroid/support/v4/app/q;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;


# static fields
.field private static final a:Landroid/content/IntentFilter;


# instance fields
.field private b:Landroid/widget/ImageButton;

.field private c:Landroid/widget/Button;

.field private d:Ljava/lang/String;

.field private e:Landroid/support/v4/a/m;

.field private final f:Landroid/content/BroadcastReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 34
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "com.google.android.gms.mdm.DISMISS_MESSAGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/mdm/LockscreenActivity;->a:Landroid/content/IntentFilter;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Landroid/support/v4/app/q;-><init>()V

    .line 50
    new-instance v0, Lcom/google/android/gms/mdm/b;

    invoke-direct {v0, p0}, Lcom/google/android/gms/mdm/b;-><init>(Lcom/google/android/gms/mdm/LockscreenActivity;)V

    iput-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->f:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 90
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 91
    invoke-static {p0, v1, v0, v1}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 2

    .prologue
    .line 63
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/mdm/LockscreenActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 64
    const-string v1, "lock_message"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 65
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 66
    const-string v1, "phone_number"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 68
    :cond_0
    const/high16 v1, 0x50800000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 75
    if-eqz p3, :cond_1

    .line 76
    const/high16 v1, 0x20000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 81
    :goto_0
    return-object v0

    .line 78
    :cond_1
    const/high16 v1, 0x4000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    goto :goto_0
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/high16 v5, 0x800000

    .line 163
    iget-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->b:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 164
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.CALL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 165
    const-string v1, "tel:%1$s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->d:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 166
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 167
    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->startActivity(Landroid/content/Intent;)V

    .line 173
    :cond_0
    :goto_0
    return-void

    .line 168
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->c:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 169
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.android.phone.EmergencyDialer.DIAL"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 170
    invoke-virtual {v0, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 171
    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/16 v6, 0x8

    const/4 v3, 0x0

    .line 96
    invoke-super {p0, p1}, Landroid/support/v4/app/q;->onCreate(Landroid/os/Bundle;)V

    .line 98
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "lock_message"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 99
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 100
    const-string v0, "LockscreenActivity started without a lock message, closing."

    new-array v1, v3, [Ljava/lang/Object;

    const-string v2, "MDM"

    invoke-static {v2, v0, v1}, Lcom/google/android/gms/mdm/f/b;->d(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    invoke-virtual {p0, v3}, Lcom/google/android/gms/mdm/LockscreenActivity;->setResult(I)V

    .line 102
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity;->finish()V

    .line 153
    :goto_0
    return-void

    .line 109
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    const/high16 v4, 0x280000

    invoke-virtual {v0, v4}, Landroid/view/Window;->addFlags(I)V

    .line 110
    sget v0, Lcom/google/android/gms/l;->cL:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->setContentView(I)V

    .line 113
    sget v0, Lcom/google/android/gms/j;->lt:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 116
    sget v0, Lcom/google/android/gms/j;->nR:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->b:Landroid/widget/ImageButton;

    .line 117
    sget v0, Lcom/google/android/gms/j;->nS:I

    invoke-virtual {p0, v0}, Lcom/google/android/gms/mdm/LockscreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 118
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "phone_number"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 119
    invoke-virtual {p0}, Lcom/google/android/gms/mdm/LockscreenActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    const-string v4, "phone_number"

    invoke-virtual {v1, v4}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->d:Ljava/lang/String;

    .line 121
    iget-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    :goto_1
    sget v1, Lcom/google/android/gms/j;->fo:I

    invoke-virtual {p0, v1}, Lcom/google/android/gms/mdm/LockscreenActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    iput-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->c:Landroid/widget/Button;

    .line 128
    iget-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->c:Landroid/widget/Button;

    invoke-virtual {v1, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 130
    invoke-static {p0}, Lcom/google/android/gms/common/util/e;->c(Landroid/content/Context;)Z

    move-result v4

    .line 131
    sget v1, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v5, 0xe

    if-ge v1, v5, :cond_6

    move v1, v2

    .line 134
    :goto_2
    if-eqz v4, :cond_1

    if-eqz v1, :cond_2

    .line 135
    :cond_1
    iget-object v3, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->c:Landroid/widget/Button;

    invoke-virtual {v3, v6}, Landroid/widget/Button;->setVisibility(I)V

    .line 139
    :cond_2
    if-eqz v4, :cond_3

    .line 140
    invoke-virtual {p0, v2}, Lcom/google/android/gms/mdm/LockscreenActivity;->setRequestedOrientation(I)V

    .line 144
    :cond_3
    if-eqz v1, :cond_4

    .line 146
    invoke-virtual {v0}, Landroid/widget/TextView;->getText()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    .line 147
    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 150
    :cond_4
    invoke-static {p0}, Landroid/support/v4/a/m;->a(Landroid/content/Context;)Landroid/support/v4/a/m;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->e:Landroid/support/v4/a/m;

    .line 151
    iget-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->e:Landroid/support/v4/a/m;

    iget-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->f:Landroid/content/BroadcastReceiver;

    sget-object v2, Lcom/google/android/gms/mdm/LockscreenActivity;->a:Landroid/content/IntentFilter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)V

    goto/16 :goto_0

    .line 123
    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->b:Landroid/widget/ImageButton;

    invoke-virtual {v1, v6}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 124
    invoke-virtual {v0, v6}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    :cond_6
    move v1, v3

    .line 131
    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 157
    iget-object v0, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->e:Landroid/support/v4/a/m;

    iget-object v1, p0, Lcom/google/android/gms/mdm/LockscreenActivity;->f:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/support/v4/a/m;->a(Landroid/content/BroadcastReceiver;)V

    .line 158
    invoke-super {p0}, Landroid/support/v4/app/q;->onDestroy()V

    .line 159
    return-void
.end method
