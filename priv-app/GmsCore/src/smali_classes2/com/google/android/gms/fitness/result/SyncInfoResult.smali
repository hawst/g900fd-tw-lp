.class public Lcom/google/android/gms/fitness/result/SyncInfoResult;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/ap;
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;


# instance fields
.field private final a:I

.field private final b:Lcom/google/android/gms/common/api/Status;

.field private final c:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 103
    new-instance v0, Lcom/google/android/gms/fitness/result/h;

    invoke-direct {v0}, Lcom/google/android/gms/fitness/result/h;-><init>()V

    sput-object v0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/common/api/Status;J)V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    iput p1, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->a:I

    .line 39
    iput-object p2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    .line 40
    iput-wide p3, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    .line 41
    return-void
.end method

.method private constructor <init>(Lcom/google/android/gms/common/api/Status;)V
    .locals 4

    .prologue
    .line 47
    const/4 v0, 0x1

    const-wide/16 v2, -0x1

    invoke-direct {p0, v0, p1, v2, v3}, Lcom/google/android/gms/fitness/result/SyncInfoResult;-><init>(ILcom/google/android/gms/common/api/Status;J)V

    .line 48
    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/common/api/Status;J)V
    .locals 2

    .prologue
    .line 54
    const/4 v0, 0x1

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/gms/fitness/result/SyncInfoResult;-><init>(ILcom/google/android/gms/common/api/Status;J)V

    .line 55
    return-void
.end method

.method public static a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/fitness/result/SyncInfoResult;
    .locals 1

    .prologue
    .line 61
    new-instance v0, Lcom/google/android/gms/fitness/result/SyncInfoResult;

    invoke-direct {v0, p0}, Lcom/google/android/gms/fitness/result/SyncInfoResult;-><init>(Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method


# virtual methods
.method public final C_()Lcom/google/android/gms/common/api/Status;
    .locals 1

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    return-object v0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 77
    iget-wide v0, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    return-wide v0
.end method

.method final c()I
    .locals 1

    .prologue
    .line 107
    iget v0, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->a:I

    return v0
.end method

.method public describeContents()I
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 6

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 81
    if-eq p0, p1, :cond_0

    instance-of v2, p1, Lcom/google/android/gms/fitness/result/SyncInfoResult;

    if-eqz v2, :cond_1

    check-cast p1, Lcom/google/android/gms/fitness/result/SyncInfoResult;

    iget-object v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    iget-object v3, p1, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v2, v3}, Lcom/google/android/gms/common/api/Status;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    iget-wide v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    iget-wide v4, p1, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    :goto_0
    if-eqz v2, :cond_1

    :cond_0
    move v0, v1

    :cond_1
    return v0

    :cond_2
    move v2, v0

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    .line 92
    const/4 v0, 0x2

    new-array v0, v0, [Ljava/lang/Object;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-wide v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->hashCode([Ljava/lang/Object;)I

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 97
    invoke-static {p0}, Lcom/google/android/gms/common/internal/bu;->a(Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "status"

    iget-object v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->b:Lcom/google/android/gms/common/api/Status;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    const-string v1, "timestamp"

    iget-wide v2, p0, Lcom/google/android/gms/fitness/result/SyncInfoResult;->c:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/common/internal/bv;->a(Ljava/lang/String;Ljava/lang/Object;)Lcom/google/android/gms/common/internal/bv;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/bv;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 115
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/fitness/result/h;->a(Lcom/google/android/gms/fitness/result/SyncInfoResult;Landroid/os/Parcel;I)V

    .line 116
    return-void
.end method
