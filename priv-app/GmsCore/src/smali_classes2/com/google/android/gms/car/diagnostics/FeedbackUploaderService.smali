.class public Lcom/google/android/gms/car/diagnostics/FeedbackUploaderService;
.super Landroid/app/IntentService;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 17
    const-class v0, Lcom/google/android/gms/car/diagnostics/FeedbackUploaderService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 18
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 3

    .prologue
    .line 22
    invoke-static {p1}, Lcom/google/android/gms/car/diagnostics/l;->a(Landroid/content/Intent;)Lcom/google/android/gms/car/diagnostics/a;

    move-result-object v0

    .line 23
    if-nez v0, :cond_0

    .line 24
    const-string v0, "CAR.FEEDBACK"

    const-string v1, "Received empty feedback bundle"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 28
    :goto_0
    return-void

    .line 27
    :cond_0
    new-instance v1, Lcom/google/android/gms/car/diagnostics/d;

    invoke-virtual {p0}, Lcom/google/android/gms/car/diagnostics/FeedbackUploaderService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Lcom/google/android/gms/car/diagnostics/d;-><init>(Landroid/content/Context;Lcom/google/android/gms/car/diagnostics/a;)V

    invoke-virtual {v1}, Lcom/google/android/gms/car/diagnostics/d;->run()V

    goto :goto_0
.end method
