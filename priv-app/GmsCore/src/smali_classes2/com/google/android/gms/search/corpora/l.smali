.class public Lcom/google/android/gms/search/corpora/l;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;)V
    .locals 6

    .prologue
    .line 25
    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-static {p2}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    move-object v0, p0

    move-object v3, p1

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 27
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 14

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 22
    new-instance v5, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;

    invoke-direct {v5}, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;-><init>()V

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    iget-object v0, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->a:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    iget-object v1, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->b:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/gms/search/corpora/l;->b:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    iget-wide v6, v0, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->c:J

    invoke-static {v1, v6, v7}, Lcom/google/android/gms/icing/impl/bb;->a(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_0

    const-string v1, "Bad request indexing args: %s"

    invoke-static {v1, v0}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;)I

    :goto_0
    iput-boolean v3, v5, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->b:Z

    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v0, v5, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    return-object v5

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/search/corpora/l;->c:Lcom/google/android/gms/icing/impl/a/h;

    check-cast v0, Lcom/google/android/gms/icing/impl/a/aa;

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/l;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->j()Lcom/google/android/gms/icing/impl/a/j;

    move-result-object v6

    new-array v2, v4, [Ljava/lang/String;

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    iget-object v1, v1, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->b:Ljava/lang/String;

    aput-object v1, v2, v3

    invoke-virtual {v6, v0, v2, v3}, Lcom/google/android/gms/icing/impl/a/j;->a(Lcom/google/android/gms/icing/impl/a/aa;[Ljava/lang/String;Z)Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v7

    move v2, v3

    :cond_1
    :goto_1
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    invoke-virtual {v6, v0}, Lcom/google/android/gms/icing/impl/a/j;->b(Ljava/lang/String;)Lcom/google/android/gms/icing/i;

    move-result-object v8

    iget-object v1, v8, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-static {v1}, Lcom/google/android/gms/icing/impl/a/j;->b(Lcom/google/android/gms/icing/g;)Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/l;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v1

    iget-object v9, v8, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    iget v9, v9, Lcom/google/android/gms/icing/g;->a:I

    invoke-virtual {v1, v9, v3}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(IZ)J

    move-result-wide v10

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/l;->b:Ljava/lang/Object;

    check-cast v1, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;

    iget-wide v12, v1, Lcom/google/android/gms/search/corpora/RequestIndexingCall$Request;->c:J

    cmp-long v1, v12, v10

    if-lez v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/search/corpora/l;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v1}, Lcom/google/android/gms/icing/impl/u;->E()Lcom/google/android/gms/icing/impl/e;

    move-result-object v1

    iget-object v2, v8, Lcom/google/android/gms/icing/i;->a:Lcom/google/android/gms/icing/g;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/icing/impl/e;->a(Ljava/lang/String;Lcom/google/android/gms/icing/g;)V

    move v0, v4

    :goto_2
    move v2, v0

    goto :goto_1

    :cond_2
    move v3, v2

    goto :goto_0

    :cond_3
    move v0, v2

    goto :goto_2
.end method
