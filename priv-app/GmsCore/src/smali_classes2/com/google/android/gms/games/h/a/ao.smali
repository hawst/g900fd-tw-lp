.class public final Lcom/google/android/gms/games/h/a/ao;
.super Lcom/google/android/gms/common/server/response/c;
.source "SourceFile"


# static fields
.field private static final b:Ljava/util/HashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 42
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    .line 44
    sput-object v0, Lcom/google/android/gms/games/h/a/ao;->b:Ljava/util/HashMap;

    const-string v1, "channelType"

    const-string v2, "channelType"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->g(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 45
    sget-object v0, Lcom/google/android/gms/games/h/a/ao;->b:Ljava/util/HashMap;

    const-string v1, "isEnabled"

    const-string v2, "isEnabled"

    invoke-static {v2}, Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;->f(Ljava/lang/String;)Lcom/google/android/gms/common/server/response/FastJsonResponse$Field;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 46
    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Lcom/google/android/gms/common/server/response/c;-><init>()V

    .line 59
    if-eqz p1, :cond_0

    .line 60
    const-string v0, "channelType"

    invoke-virtual {p0, v0, p1}, Lcom/google/android/gms/games/h/a/ao;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 62
    :cond_0
    if-eqz p2, :cond_1

    .line 63
    const-string v0, "isEnabled"

    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/games/h/a/ao;->a(Ljava/lang/String;Z)V

    .line 65
    :cond_1
    return-void
.end method


# virtual methods
.method public final a()Ljava/util/HashMap;
    .locals 1

    .prologue
    .line 50
    sget-object v0, Lcom/google/android/gms/games/h/a/ao;->b:Ljava/util/HashMap;

    return-object v0
.end method

.method public final b()Ljava/lang/String;
    .locals 2

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "channelType"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    return-object v0
.end method

.method public final c()Ljava/lang/Boolean;
    .locals 2

    .prologue
    .line 84
    iget-object v0, p0, Lcom/google/android/gms/common/server/response/c;->a:Ljava/util/HashMap;

    const-string v1, "isEnabled"

    invoke-virtual {v0, v1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Boolean;

    return-object v0
.end method
