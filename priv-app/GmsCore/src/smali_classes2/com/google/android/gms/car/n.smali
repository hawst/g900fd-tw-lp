.class final Lcom/google/android/gms/car/n;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field final a:I

.field final b:I

.field final c:I

.field final d:Z

.field final e:Ljava/util/concurrent/Semaphore;

.field final f:Ljava/nio/ByteBuffer;

.field final g:Ljava/util/concurrent/Semaphore;

.field final h:Lcom/google/android/gms/car/nz;

.field final i:Lcom/google/android/gms/car/o;

.field j:Lcom/google/android/gms/car/a;

.field k:Z


# direct methods
.method public constructor <init>(IILjava/nio/ByteBuffer;Lcom/google/android/gms/car/nz;Z)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/n;->e:Ljava/util/concurrent/Semaphore;

    .line 90
    new-instance v0, Ljava/util/concurrent/Semaphore;

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/car/n;->g:Ljava/util/concurrent/Semaphore;

    .line 93
    new-instance v0, Lcom/google/android/gms/car/o;

    invoke-direct {v0, p0, v1}, Lcom/google/android/gms/car/o;-><init>(Lcom/google/android/gms/car/n;B)V

    iput-object v0, p0, Lcom/google/android/gms/car/n;->i:Lcom/google/android/gms/car/o;

    .line 97
    iput-boolean v1, p0, Lcom/google/android/gms/car/n;->k:Z

    .line 112
    iput p1, p0, Lcom/google/android/gms/car/n;->a:I

    .line 113
    const/16 v0, 0x10

    iput v0, p0, Lcom/google/android/gms/car/n;->b:I

    .line 114
    iput p2, p0, Lcom/google/android/gms/car/n;->c:I

    .line 115
    iput-boolean p5, p0, Lcom/google/android/gms/car/n;->d:Z

    .line 117
    iput-object p3, p0, Lcom/google/android/gms/car/n;->f:Ljava/nio/ByteBuffer;

    .line 118
    iput-object p4, p0, Lcom/google/android/gms/car/n;->h:Lcom/google/android/gms/car/nz;

    .line 119
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 173
    iget-boolean v0, p0, Lcom/google/android/gms/car/n;->k:Z

    if-nez v0, :cond_0

    .line 179
    :goto_0
    return-void

    .line 176
    :cond_0
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/n;->k:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/gms/car/n;->j:Lcom/google/android/gms/car/a;

    invoke-virtual {v0}, Lcom/google/android/gms/car/a;->b()V

    .line 178
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/n;->j:Lcom/google/android/gms/car/a;

    goto :goto_0
.end method
