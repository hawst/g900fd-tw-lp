.class final Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;
.super Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;
.source "SourceFile"


# annotations
.annotation build Lcom/google/android/gms/common/util/RetainForClient;
.end annotation


# instance fields
.field final synthetic a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

.field private final f:Lcom/google/android/gms/games/realtime/a;

.field private g:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/realtime/a;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V
    .locals 6

    .prologue
    .line 574
    iput-object p1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move v4, p5

    move-object v5, p6

    .line 575
    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    .line 576
    invoke-static {p3}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->g:Ljava/lang/String;

    .line 577
    invoke-static {p4}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/realtime/a;

    iput-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->f:Lcom/google/android/gms/games/realtime/a;

    .line 578
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    const/4 v1, 0x6

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/service/statemachine/roomclient/ch;->a(I)V

    .line 579
    return-void
.end method


# virtual methods
.method public final a(Landroid/os/Message;)Z
    .locals 9

    .prologue
    const/4 v7, 0x1

    .line 583
    iget v0, p1, Landroid/os/Message;->what:I

    sparse-switch v0, :sswitch_data_0

    .line 595
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 585
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v0, v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->b:Lcom/google/android/gms/games/realtime/network/a;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->f:Lcom/google/android/gms/games/realtime/a;

    invoke-interface {v0, v1}, Lcom/google/android/gms/games/realtime/network/a;->b(Lcom/google/android/gms/games/realtime/a;)V

    .line 586
    iget-object v8, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    new-instance v0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;

    iget-object v1, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    iget-object v2, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->g:Ljava/lang/String;

    iget v4, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->c:I

    iget-object v5, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->f:Lcom/google/android/gms/games/realtime/a;

    iget-object v6, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->d:Lcom/google/android/gms/games/service/statemachine/roomclient/ch;

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedState;-><init>(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/gms/games/realtime/a;Lcom/google/android/gms/games/service/statemachine/roomclient/ch;)V

    iput-object v0, v8, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->d:Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ContextState;

    move v0, v7

    .line 588
    goto :goto_0

    .line 592
    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/games/realtime/network/PeerStateMachine$ConnectedToUnsolicitedRemote;->a:Lcom/google/android/gms/games/realtime/network/PeerStateMachine;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/realtime/network/PeerStateMachine;->a(Lcom/google/android/gms/games/realtime/network/PeerStateMachine;Landroid/os/Message;)V

    move v0, v7

    .line 593
    goto :goto_0

    .line 583
    nop

    :sswitch_data_0
    .sparse-switch
        0x65 -> :sswitch_1
        0x12e -> :sswitch_1
        0x1779 -> :sswitch_0
    .end sparse-switch
.end method
