.class public final Lcom/google/android/gms/mdm/b/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/a/d;

.field public static final b:Lcom/google/android/gms/common/a/d;

.field public static final c:Lcom/google/android/gms/common/a/d;

.field public static final d:Lcom/google/android/gms/common/a/d;

.field public static final e:Lcom/google/android/gms/common/a/d;

.field public static final f:Lcom/google/android/gms/common/a/d;

.field public static final g:Lcom/google/android/gms/common/a/d;

.field public static final h:Lcom/google/android/gms/common/a/d;

.field public static final i:Lcom/google/android/gms/common/a/d;

.field public static final j:Lcom/google/android/gms/common/a/d;

.field public static final k:Lcom/google/android/gms/common/a/d;

.field public static final l:Lcom/google/android/gms/common/a/d;

.field public static final m:Lcom/google/android/gms/common/a/d;

.field public static final n:Lcom/google/android/gms/common/a/d;

.field public static final o:Lcom/google/android/gms/common/a/d;

.field public static final p:Lcom/google/android/gms/common/a/d;

.field public static final q:Lcom/google/android/gms/common/a/d;

.field public static final r:Lcom/google/android/gms/common/a/d;

.field public static final s:Lcom/google/android/gms/common/a/d;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const-wide/32 v2, 0xea60

    const/4 v4, 0x1

    .line 10
    const-string v0, "mdm.response_url"

    const-string v1, "https://android.googleapis.com/nova/remote_payload"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->a:Lcom/google/android/gms/common/a/d;

    .line 13
    const-string v0, "mdm.sitrep_url"

    const-string v1, "https://android.googleapis.com/nova/sitrep"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->b:Lcom/google/android/gms/common/a/d;

    .line 16
    const-string v0, "mdm.initial_sitrep_delay_ms"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->c:Lcom/google/android/gms/common/a/d;

    .line 19
    const-string v0, "mdm.location_enabled_default"

    sget-object v1, Ljava/lang/Boolean;->TRUE:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->d:Lcom/google/android/gms/common/a/d;

    .line 22
    const-string v0, "mdm.location_collection_duration_ms"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->e:Lcom/google/android/gms/common/a/d;

    .line 25
    const-string v0, "mdm.pre_wipe_location_timeout_ms"

    const-wide/16 v2, 0x2710

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->f:Lcom/google/android/gms/common/a/d;

    .line 28
    const-string v0, "mdm.location_collection_max_updates"

    const/16 v1, 0xa

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->g:Lcom/google/android/gms/common/a/d;

    .line 31
    const-string v0, "mdm.location_accuracy_m"

    const/high16 v1, 0x41c80000    # 25.0f

    invoke-static {v1}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Float;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->h:Lcom/google/android/gms/common/a/d;

    .line 34
    const-string v0, "mdm.noise_timeout_ms"

    const-wide/32 v2, 0x493e0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->i:Lcom/google/android/gms/common/a/d;

    .line 37
    const-string v0, "mdm.tone_loop_interval_ms"

    const-wide/16 v2, 0x7d0

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Long;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->j:Lcom/google/android/gms/common/a/d;

    .line 40
    const-string v0, "mdm.target_ringtone"

    const-string v1, "Orion"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->k:Lcom/google/android/gms/common/a/d;

    .line 43
    const-string v0, "mdm.restrict_to_primary_user"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->l:Lcom/google/android/gms/common/a/d;

    .line 46
    const-string v0, "mdm.oauth_scope"

    const-string v1, "https://www.googleapis.com/auth/android_device_manager"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->m:Lcom/google/android/gms/common/a/d;

    .line 49
    const-string v0, "mdm.ostensible_gmail_domains"

    const-string v1, "@googlemail.com"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->n:Lcom/google/android/gms/common/a/d;

    .line 55
    const-string v0, "mdm.maximum_sitrep_failures"

    const/16 v1, 0xc8

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/Integer;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->o:Lcom/google/android/gms/common/a/d;

    .line 58
    const-string v0, "mdm.get_devices_url"

    const-string v1, "https://android.googleapis.com/nova/get_devices"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->p:Lcom/google/android/gms/common/a/d;

    .line 61
    const-string v0, "mdm.rename_device_url"

    const-string v1, "https://android.googleapis.com/nova/rename_device"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->q:Lcom/google/android/gms/common/a/d;

    .line 64
    const-string v0, "mdm.log_sensitive_info"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->r:Lcom/google/android/gms/common/a/d;

    .line 67
    const-string v0, "mdm.use_new_account_intent"

    invoke-static {v0, v4}, Lcom/google/android/gms/common/a/d;->a(Ljava/lang/String;Z)Lcom/google/android/gms/common/a/d;

    move-result-object v0

    sput-object v0, Lcom/google/android/gms/mdm/b/a;->s:Lcom/google/android/gms/common/a/d;

    return-void
.end method
