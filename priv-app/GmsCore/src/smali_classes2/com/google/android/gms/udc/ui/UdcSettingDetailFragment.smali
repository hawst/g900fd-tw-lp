.class public Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;
.super Landroid/support/v4/app/Fragment;
.source "SourceFile"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;
.implements Lcom/google/android/gms/common/widget/h;


# instance fields
.field private a:Ljava/lang/String;

.field private b:Lcom/google/android/gms/udc/e/m;

.field private c:Lcom/google/android/gms/udc/ui/x;

.field private d:Z

.field private e:Lcom/google/android/gms/common/widget/SwitchBar;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 479
    return-void
.end method

.method private a(II)Landroid/content/Intent;
    .locals 4

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v0

    .line 468
    new-instance v1, Lcom/google/android/gms/udc/e/d;

    invoke-direct {v1}, Lcom/google/android/gms/udc/e/d;-><init>()V

    .line 469
    const/4 v2, 0x1

    new-array v2, v2, [I

    const/4 v3, 0x0

    aput p2, v2, v3

    iput-object v2, v1, Lcom/google/android/gms/udc/e/d;->a:[I

    .line 470
    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v0, v2, v3}, Lcom/google/android/gms/udc/c/w;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/udc/e/u;

    move-result-object v2

    iput-object v2, v1, Lcom/google/android/gms/udc/e/d;->d:Lcom/google/android/gms/udc/e/u;

    .line 472
    const/4 v2, 0x2

    iput v2, v1, Lcom/google/android/gms/udc/e/d;->c:I

    .line 473
    iput p1, v1, Lcom/google/android/gms/udc/e/d;->b:I

    .line 475
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    invoke-static {v0, v2, v1}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/gms/udc/e/d;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x24000000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 2

    .prologue
    .line 301
    sget v0, Lcom/google/android/gms/l;->fG:I

    const/4 v1, 0x0

    invoke-virtual {p0, v0, p1, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 302
    return-void
.end method

.method private a(Landroid/view/View;Lcom/google/android/gms/udc/e/m;)V
    .locals 15

    .prologue
    .line 190
    if-eqz p1, :cond_0

    if-nez p2, :cond_1

    .line 292
    :cond_0
    :goto_0
    return-void

    .line 194
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 195
    new-instance v1, Lcom/google/android/gms/udc/util/f;

    invoke-direct {v1}, Lcom/google/android/gms/udc/util/f;-><init>()V

    .line 197
    sget v2, Lcom/google/android/gms/j;->sn:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/widget/SwitchBar;

    iput-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->e:Lcom/google/android/gms/common/widget/SwitchBar;

    .line 198
    iget-object v5, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->e:Lcom/google/android/gms/common/widget/SwitchBar;

    const/4 v3, 0x0

    const/4 v2, 0x0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v4, v4, Lcom/google/android/gms/udc/e/p;->c:I

    packed-switch v4, :pswitch_data_0

    :goto_1
    :pswitch_0
    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/gms/udc/e/m;->b:[Lcom/google/android/gms/udc/e/r;

    array-length v7, v6

    const/4 v4, 0x0

    :goto_2
    if-ge v4, v7, :cond_3

    aget-object v9, v6, v4

    iget v10, v9, Lcom/google/android/gms/udc/e/r;->a:I

    packed-switch v10, :pswitch_data_1

    :cond_2
    :goto_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    :pswitch_1
    const/4 v3, 0x1

    const/4 v2, 0x1

    goto :goto_1

    :pswitch_2
    const/4 v3, 0x0

    const/4 v2, 0x1

    goto :goto_1

    :pswitch_3
    const/4 v3, 0x1

    const/4 v2, 0x1

    goto :goto_1

    :pswitch_4
    const/4 v3, 0x0

    const/4 v2, 0x0

    goto :goto_1

    :pswitch_5
    iget-object v9, v9, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v9}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v5, v9}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Ljava/lang/CharSequence;)V

    goto :goto_3

    :pswitch_6
    iget-object v9, v9, Lcom/google/android/gms/udc/e/r;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v9}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v9

    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_2

    invoke-virtual {v5, v9}, Lcom/google/android/gms/common/widget/SwitchBar;->b(Ljava/lang/CharSequence;)V

    goto :goto_3

    :cond_3
    if-eqz v3, :cond_4

    move-object v4, p0

    :goto_4
    invoke-virtual {v5, v4}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Lcom/google/android/gms/common/widget/h;)V

    invoke-virtual {v5, v3}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    if-eqz v2, :cond_5

    const/4 v2, 0x0

    :goto_5
    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->setVisibility(I)V

    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v2, v2, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(I)Z

    move-result v2

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Z)V

    sget v2, Lcom/google/android/gms/j;->sZ:I

    invoke-virtual {v5, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->findViewById(I)Landroid/view/View;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/View;->setSaveEnabled(Z)V

    .line 200
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/udc/e/m;->l:Lcom/google/android/gms/udc/e/l;

    if-eqz v2, :cond_6

    .line 201
    sget v2, Lcom/google/android/gms/j;->tx:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 203
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 204
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/udc/e/m;->l:Lcom/google/android/gms/udc/e/l;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/l;->b:[Lcom/google/android/gms/udc/e/s;

    array-length v3, v3

    if-lez v3, :cond_6

    .line 205
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 206
    new-instance v3, Lcom/google/android/gms/udc/util/e;

    invoke-direct {v3, v8, v2}, Lcom/google/android/gms/udc/util/e;-><init>(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 207
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/udc/e/m;->l:Lcom/google/android/gms/udc/e/l;

    iget-object v4, v2, Lcom/google/android/gms/udc/e/l;->b:[Lcom/google/android/gms/udc/e/s;

    array-length v5, v4

    const/4 v2, 0x0

    :goto_6
    if-ge v2, v5, :cond_6

    aget-object v6, v4, v2

    .line 208
    sget v7, Lcom/google/android/gms/l;->fM:I

    invoke-virtual {v3, v7}, Lcom/google/android/gms/udc/util/e;->a(I)Landroid/view/View;

    move-result-object v7

    .line 209
    sget v9, Lcom/google/android/gms/j;->sy:I

    invoke-virtual {v1, v7, v9, v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 207
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 198
    :cond_4
    const/4 v4, 0x0

    goto :goto_4

    :cond_5
    const/16 v2, 0x8

    goto :goto_5

    .line 214
    :cond_6
    sget v3, Lcom/google/android/gms/j;->eg:I

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->g:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    move-object/from16 v2, p1

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;Landroid/support/v4/app/q;Ljava/lang/String;)Landroid/widget/TextView;

    move-result-object v2

    .line 216
    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->c:I

    const/4 v4, 0x3

    if-ne v3, v4, :cond_7

    .line 217
    invoke-direct {p0, v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/widget/TextView;)V

    .line 220
    :cond_7
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/gms/udc/e/m;->i:Lcom/google/android/gms/udc/e/c;

    if-eqz v2, :cond_8

    .line 221
    sget v2, Lcom/google/android/gms/j;->lI:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 222
    sget v2, Lcom/google/android/gms/j;->lI:I

    move-object/from16 v0, p2

    iget-object v3, v0, Lcom/google/android/gms/udc/e/m;->i:Lcom/google/android/gms/udc/e/c;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0, v2, v3}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 225
    :cond_8
    sget v2, Lcom/google/android/gms/j;->ac:I

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/view/ViewGroup;

    .line 226
    invoke-virtual {v2}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 227
    const/4 v3, 0x0

    .line 229
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v4, v4, Lcom/google/android/gms/udc/e/p;->c:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_9

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->h:Lcom/google/android/gms/udc/e/c;

    if-eqz v4, :cond_9

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->h:Lcom/google/android/gms/udc/e/c;

    iget-object v4, v4, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    if-eqz v4, :cond_9

    .line 231
    const/4 v3, 0x1

    .line 232
    invoke-static {v8, v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 233
    sget v4, Lcom/google/android/gms/l;->fN:I

    const/4 v5, 0x0

    invoke-virtual {v8, v4, v2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v4

    .line 235
    invoke-virtual {v4, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 236
    move-object/from16 v0, p2

    invoke-virtual {v4, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 237
    sget v5, Lcom/google/android/gms/j;->sQ:I

    move-object/from16 v0, p2

    iget-object v6, v0, Lcom/google/android/gms/udc/e/m;->h:Lcom/google/android/gms/udc/e/c;

    iget-object v6, v6, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v4, v5, v6}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 238
    const/4 v5, -0x1

    const/4 v6, -0x2

    invoke-virtual {v2, v4, v5, v6}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 242
    :cond_9
    move-object/from16 v0, p2

    iget-object v9, v0, Lcom/google/android/gms/udc/e/m;->n:[Lcom/google/android/gms/udc/e/o;

    array-length v10, v9

    const/4 v4, 0x0

    move v6, v4

    :goto_7
    if-ge v6, v10, :cond_c

    aget-object v11, v9, v6

    .line 243
    const/4 v7, 0x1

    .line 244
    invoke-static {v8, v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 246
    sget v3, Lcom/google/android/gms/l;->fR:I

    const/4 v4, 0x0

    invoke-virtual {v8, v3, v2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v12

    .line 248
    sget v3, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {v12, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/CompoundButton;

    .line 249
    iget-object v4, v11, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v4, v4, Lcom/google/android/gms/udc/e/p;->b:I

    invoke-static {v4}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(I)Z

    move-result v4

    invoke-virtual {v3, v4}, Landroid/widget/CompoundButton;->setChecked(Z)V

    .line 250
    sget v4, Lcom/google/android/gms/j;->sQ:I

    iget-object v5, v11, Lcom/google/android/gms/udc/e/o;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v12, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    move-result-object v13

    .line 252
    const/4 v4, 0x0

    .line 253
    iget-object v5, v11, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    if-eqz v5, :cond_a

    .line 254
    sget v4, Lcom/google/android/gms/j;->sl:I

    iget-object v5, v11, Lcom/google/android/gms/udc/e/o;->d:Lcom/google/android/gms/udc/e/l;

    iget-object v5, v5, Lcom/google/android/gms/udc/e/l;->b:[Lcom/google/android/gms/udc/e/s;

    const/4 v14, 0x0

    aget-object v5, v5, v14

    invoke-virtual {v1, v12, v4, v5}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    move-result-object v4

    .line 258
    :cond_a
    iget-object v5, v11, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v5, v5, Lcom/google/android/gms/udc/e/p;->c:I

    packed-switch v5, :pswitch_data_2

    const/4 v5, 0x0

    :goto_8
    if-eqz v5, :cond_b

    .line 259
    invoke-virtual {v12, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 260
    invoke-virtual {v3, p0}, Landroid/widget/CompoundButton;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 261
    sget v4, Lcom/google/android/gms/j;->tA:I

    invoke-virtual {v3, v4, v11}, Landroid/widget/CompoundButton;->setTag(ILjava/lang/Object;)V

    .line 269
    :goto_9
    const/4 v3, -0x1

    const/4 v4, -0x2

    invoke-virtual {v2, v12, v3, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    .line 242
    add-int/lit8 v3, v6, 0x1

    move v6, v3

    move v3, v7

    goto :goto_7

    .line 258
    :pswitch_7
    const/4 v5, 0x1

    goto :goto_8

    :pswitch_8
    const/4 v5, 0x0

    goto :goto_8

    :pswitch_9
    const/4 v5, 0x0

    goto :goto_8

    :pswitch_a
    const/4 v5, 0x0

    goto :goto_8

    .line 263
    :cond_b
    const/4 v5, 0x0

    invoke-virtual {v3, v5}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 264
    const/4 v3, 0x0

    invoke-virtual {v12, v3}, Landroid/view/View;->setEnabled(Z)V

    .line 265
    invoke-direct {p0, v13}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/widget/TextView;)V

    .line 266
    invoke-direct {p0, v4}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/widget/TextView;)V

    goto :goto_9

    .line 273
    :cond_c
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    if-eqz v4, :cond_d

    .line 274
    const/4 v3, 0x1

    .line 275
    invoke-static {v8, v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    .line 277
    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    .line 279
    sget v5, Lcom/google/android/gms/l;->fP:I

    const/4 v6, 0x0

    invoke-virtual {v8, v5, v2, v6}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v5

    .line 281
    sget v6, Lcom/google/android/gms/j;->sQ:I

    iget-object v7, v4, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v5, v6, v7}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 282
    sget v6, Lcom/google/android/gms/j;->eg:I

    iget-object v4, v4, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-virtual {v1, v5, v6, v4}, Lcom/google/android/gms/udc/util/f;->a(Landroid/view/View;ILcom/google/android/gms/udc/e/s;)Landroid/widget/TextView;

    .line 283
    invoke-virtual {v5, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 285
    const/4 v1, -0x1

    const/4 v4, -0x2

    invoke-virtual {v2, v5, v1, v4}, Landroid/view/ViewGroup;->addView(Landroid/view/View;II)V

    :cond_d
    move v1, v3

    .line 289
    if-eqz v1, :cond_0

    .line 290
    invoke-static {v8, v2}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V

    goto/16 :goto_0

    .line 198
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_3
        :pswitch_2
        :pswitch_0
        :pswitch_4
    .end packed-switch

    :pswitch_data_1
    .packed-switch 0x2
        :pswitch_5
        :pswitch_6
    .end packed-switch

    .line 258
    :pswitch_data_2
    .packed-switch 0x1
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_a
        :pswitch_9
    .end packed-switch
.end method

.method private a(Landroid/widget/TextView;)V
    .locals 2

    .prologue
    .line 295
    if-eqz p1, :cond_0

    .line 296
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Lcom/google/android/gms/f;->aE:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 298
    :cond_0
    return-void
.end method

.method private a([II)V
    .locals 9

    .prologue
    const/4 v0, 0x0

    .line 146
    const/4 v1, 0x3

    if-ne p2, v1, :cond_2

    array-length v1, p1

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 147
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    aget v3, p1, v0

    iget-object v4, v2, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    if-eqz v4, :cond_1

    iget v1, v4, Lcom/google/android/gms/udc/e/n;->c:I

    if-eq v1, v3, :cond_0

    iget-object v1, v2, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    if-eqz v1, :cond_5

    iget-object v1, v2, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    iget v1, v1, Lcom/google/android/gms/udc/e/n;->c:I

    if-lez v1, :cond_5

    iget-object v1, v2, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v1, v1, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v5, v2, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    iget v5, v5, Lcom/google/android/gms/udc/e/n;->c:I

    if-ne v1, v5, :cond_3

    iget-object v1, v2, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v1, v1, Lcom/google/android/gms/udc/e/p;->a:I

    :goto_0
    if-ne v1, v3, :cond_1

    :cond_0
    iget v0, v4, Lcom/google/android/gms/udc/e/n;->c:I

    .line 148
    :cond_1
    if-eqz v0, :cond_2

    .line 149
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    invoke-interface {v1, v0}, Lcom/google/android/gms/udc/ui/x;->a(I)V

    .line 152
    :cond_2
    return-void

    .line 147
    :cond_3
    iget-object v5, v2, Lcom/google/android/gms/udc/e/m;->n:[Lcom/google/android/gms/udc/e/o;

    array-length v6, v5

    move v1, v0

    :goto_1
    if-ge v1, v6, :cond_5

    aget-object v7, v5, v1

    iget-object v7, v7, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v7, v7, Lcom/google/android/gms/udc/e/p;->a:I

    iget-object v8, v2, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    iget v8, v8, Lcom/google/android/gms/udc/e/n;->c:I

    if-ne v7, v8, :cond_4

    iget-object v1, v2, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v1, v1, Lcom/google/android/gms/udc/e/p;->a:I

    goto :goto_0

    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    :cond_5
    move v1, v0

    goto :goto_0
.end method

.method private static a(I)Z
    .locals 1

    .prologue
    .line 451
    const/4 v0, 0x2

    if-ne p0, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(Lcom/google/android/gms/udc/e/m;)V
    .locals 2

    .prologue
    .line 455
    iget-object v0, p1, Lcom/google/android/gms/udc/e/m;->h:Lcom/google/android/gms/udc/e/c;

    iget v0, v0, Lcom/google/android/gms/udc/e/c;->e:I

    iget-object v1, p1, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v1, v1, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(II)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    .line 458
    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/common/widget/SwitchBar;Z)V
    .locals 2

    .prologue
    .line 442
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 443
    iget-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->d:Z

    if-nez v0, :cond_0

    .line 444
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->d:Z

    .line 445
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-interface {v0, v1}, Lcom/google/android/gms/udc/ui/x;->a(Lcom/google/android/gms/udc/e/m;)V

    .line 446
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-direct {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b(Lcom/google/android/gms/udc/e/m;)V

    .line 448
    :cond_0
    return-void
.end method

.method public final a(Lcom/google/android/gms/udc/e/m;)V
    .locals 1

    .prologue
    .line 59
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    .line 60
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0, p1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/View;Lcom/google/android/gms/udc/e/m;)V

    .line 61
    return-void
.end method

.method public final a(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    .line 65
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v0, -0x1

    .line 105
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/Fragment;->onActivityResult(IILandroid/content/Intent;)V

    .line 107
    packed-switch p1, :pswitch_data_0

    .line 136
    :goto_0
    :pswitch_0
    return-void

    .line 109
    :pswitch_1
    if-ne p2, v0, :cond_0

    .line 110
    invoke-static {p3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b(Landroid/content/Intent;)I

    move-result v0

    .line 111
    invoke-static {p3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Landroid/content/Intent;)[I

    move-result-object v1

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a([II)V

    .line 113
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->e:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(I)Z

    move-result v0

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/widget/SwitchBar;->a(Z)V

    .line 114
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->e:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 115
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/udc/ui/x;->e()V

    goto :goto_0

    .line 117
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/View;Lcom/google/android/gms/udc/e/m;)V

    goto :goto_0

    .line 122
    :pswitch_2
    if-ne p2, v0, :cond_1

    .line 123
    invoke-static {p3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->a(Landroid/content/Intent;)[I

    move-result-object v0

    invoke-static {p3}, Lcom/google/android/gms/udc/ui/UdcConsentActivity;->b(Landroid/content/Intent;)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a([II)V

    .line 125
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/udc/ui/x;->e()V

    goto :goto_0

    .line 127
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/View;Lcom/google/android/gms/udc/e/m;)V

    goto :goto_0

    .line 132
    :pswitch_3
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->e:Lcom/google/android/gms/common/widget/SwitchBar;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/widget/SwitchBar;->setEnabled(Z)V

    .line 133
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-interface {v0}, Lcom/google/android/gms/udc/ui/x;->e()V

    goto :goto_0

    .line 107
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_1
        :pswitch_3
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public onAttach(Landroid/app/Activity;)V
    .locals 2

    .prologue
    .line 75
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onAttach(Landroid/app/Activity;)V

    .line 76
    instance-of v0, p1, Lcom/google/android/gms/udc/ui/x;

    if-eqz v0, :cond_0

    .line 77
    check-cast p1, Lcom/google/android/gms/udc/ui/x;

    iput-object p1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    return-void

    .line 79
    :cond_0
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Attaching activity must implement SettingActionListener"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2

    .prologue
    .line 426
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->setEnabled(Z)V

    .line 427
    sget v0, Lcom/google/android/gms/j;->tA:I

    invoke-virtual {p1, v0}, Landroid/widget/CompoundButton;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/o;

    .line 429
    if-nez v0, :cond_1

    .line 430
    const-string v0, "UdcSettingDetailFragment"

    const-string v1, "SubSetting config missing"

    invoke-static {v0, v1}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    .line 438
    :cond_0
    :goto_0
    return-void

    .line 433
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->d:Z

    if-nez v1, :cond_0

    .line 434
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->d:Z

    .line 435
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    invoke-interface {v1, v0}, Lcom/google/android/gms/udc/ui/x;->a(Lcom/google/android/gms/udc/e/o;)V

    .line 436
    iget-object v1, v0, Lcom/google/android/gms/udc/e/o;->c:Lcom/google/android/gms/udc/e/c;

    iget v1, v1, Lcom/google/android/gms/udc/e/c;->e:I

    iget-object v0, v0, Lcom/google/android/gms/udc/e/o;->a:Lcom/google/android/gms/udc/e/p;

    iget v0, v0, Lcom/google/android/gms/udc/e/p;->a:I

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(II)Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6

    .prologue
    const/4 v1, 0x1

    .line 376
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    .line 378
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v2

    .line 379
    sget v3, Lcom/google/android/gms/j;->tA:I

    if-ne v2, v3, :cond_1

    .line 380
    sget v0, Lcom/google/android/gms/j;->cE:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CompoundButton;

    .line 381
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->isEnabled()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 382
    invoke-virtual {v0}, Landroid/widget/CompoundButton;->toggle()V

    .line 422
    :cond_0
    :goto_0
    return-void

    .line 384
    :cond_1
    sget v3, Lcom/google/android/gms/j;->lI:I

    if-ne v2, v3, :cond_3

    .line 385
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    iget-object v0, v0, Lcom/google/android/gms/udc/e/m;->i:Lcom/google/android/gms/udc/e/c;

    if-eqz v0, :cond_0

    .line 388
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-interface {v0, v2}, Lcom/google/android/gms/udc/ui/x;->b(Lcom/google/android/gms/udc/e/m;)V

    .line 390
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    iget-object v2, v0, Lcom/google/android/gms/udc/e/m;->i:Lcom/google/android/gms/udc/e/c;

    .line 392
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    move v0, v1

    .line 393
    :goto_1
    iget-object v3, v2, Lcom/google/android/gms/udc/e/c;->a:Lcom/google/android/gms/udc/e/s;

    invoke-static {v3}, Lcom/google/android/gms/udc/util/f;->a(Lcom/google/android/gms/udc/e/s;)Landroid/text/Spanned;

    move-result-object v3

    .line 394
    iget-object v2, v2, Lcom/google/android/gms/udc/e/c;->c:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    invoke-static {v2, v0, v3, v4}, Lcom/google/android/gms/common/ui/AuthenticatingWebViewActivity;->a(Ljava/lang/String;ZLjava/lang/CharSequence;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 400
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 392
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 401
    :cond_3
    sget v1, Lcom/google/android/gms/j;->tw:I

    if-ne v2, v1, :cond_5

    .line 402
    iget-object v0, v0, Lcom/google/android/gms/udc/e/m;->k:Lcom/google/android/gms/udc/e/n;

    .line 403
    iget-object v1, v0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1}, Lcom/google/android/gms/udc/util/f;->b(Lcom/google/android/gms/udc/e/s;)Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, v0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    invoke-static {v1}, Lcom/google/android/gms/udc/util/f;->b(Lcom/google/android/gms/udc/e/s;)Z

    move-result v1

    if-nez v1, :cond_4

    .line 405
    invoke-virtual {p0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->getActivity()Landroid/support/v4/app/q;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/gms/udc/e/n;->a:Lcom/google/android/gms/udc/e/s;

    iget-object v3, v3, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/gms/udc/e/n;->b:Lcom/google/android/gms/udc/e/s;

    iget-object v4, v4, Lcom/google/android/gms/udc/e/s;->b:Ljava/lang/String;

    iget v5, v0, Lcom/google/android/gms/udc/e/n;->c:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/gms/udc/ui/DeviceUsageActivity;->a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 411
    iget-object v2, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->c:Lcom/google/android/gms/udc/ui/x;

    invoke-interface {v2, v0}, Lcom/google/android/gms/udc/ui/x;->a(Lcom/google/android/gms/udc/e/n;)V

    .line 412
    const/4 v0, 0x2

    invoke-virtual {p0, v1, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->startActivityForResult(Landroid/content/Intent;I)V

    goto :goto_0

    .line 414
    :cond_4
    const-string v0, "UdcSettingDetailFragment"

    const-string v1, "The title and description of a device setting are required."

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 416
    :cond_5
    sget v0, Lcom/google/android/gms/j;->tz:I

    if-ne v2, v0, :cond_0

    .line 417
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 418
    instance-of v1, v0, Lcom/google/android/gms/udc/e/m;

    if-eqz v1, :cond_0

    .line 419
    check-cast v0, Lcom/google/android/gms/udc/e/m;

    invoke-direct {p0, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b(Lcom/google/android/gms/udc/e/m;)V

    goto/16 :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 1

    .prologue
    .line 86
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 87
    if-eqz p1, :cond_0

    .line 88
    const-string v0, "udc.AccountName"

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    .line 90
    :cond_0
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2

    .prologue
    .line 96
    sget v0, Lcom/google/android/gms/l;->fQ:I

    const/4 v1, 0x0

    invoke-virtual {p1, v0, p2, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 99
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->b:Lcom/google/android/gms/udc/e/m;

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Landroid/view/View;Lcom/google/android/gms/udc/e/m;)V

    .line 100
    return-object v0
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 140
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 141
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->d:Z

    .line 142
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2

    .prologue
    .line 69
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onSaveInstanceState(Landroid/os/Bundle;)V

    .line 70
    const-string v0, "udc.AccountName"

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    return-void
.end method
