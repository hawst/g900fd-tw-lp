.class final Lcom/google/android/gms/games/a/e;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field public final a:Lcom/google/android/gms/games/a/au;

.field public final b:Ljava/util/ArrayList;

.field public final c:Ljava/util/ArrayList;

.field public final d:Lcom/google/android/gms/games/h/a/i;

.field final synthetic e:Lcom/google/android/gms/games/a/c;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/c;Lcom/google/android/gms/games/a/au;)V
    .locals 2

    .prologue
    .line 1720
    iput-object p1, p0, Lcom/google/android/gms/games/a/e;->e:Lcom/google/android/gms/games/a/c;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 1721
    iget-boolean v0, p2, Lcom/google/android/gms/games/a/au;->h:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This should always be a 3P games context"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 1723
    iput-object p2, p0, Lcom/google/android/gms/games/a/e;->a:Lcom/google/android/gms/games/a/au;

    .line 1725
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/e;->b:Ljava/util/ArrayList;

    .line 1726
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/a/e;->c:Ljava/util/ArrayList;

    .line 1727
    new-instance v0, Lcom/google/android/gms/games/h/a/i;

    iget-object v1, p0, Lcom/google/android/gms/games/a/e;->b:Ljava/util/ArrayList;

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/h/a/i;-><init>(Ljava/util/ArrayList;)V

    iput-object v0, p0, Lcom/google/android/gms/games/a/e;->d:Lcom/google/android/gms/games/h/a/i;

    .line 1728
    return-void

    .line 1721
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
