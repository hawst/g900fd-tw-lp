.class public final Lcom/google/android/gms/icing/c/c;
.super Ljava/lang/Object;
.source "SourceFile"


# direct methods
.method static a(Landroid/location/Location;II)Ljava/lang/String;
    .locals 6

    .prologue
    const-wide v4, 0x416312d000000000L    # 1.0E7

    .line 65
    if-nez p0, :cond_0

    const/4 v0, 0x0

    .line 83
    :goto_0
    return-object v0

    .line 67
    :cond_0
    new-instance v0, Lcom/google/t/c/g;

    invoke-direct {v0}, Lcom/google/t/c/g;-><init>()V

    .line 68
    new-instance v1, Lcom/google/t/c/d;

    invoke-direct {v1}, Lcom/google/t/c/d;-><init>()V

    .line 70
    iput p1, v0, Lcom/google/t/c/g;->a:I

    .line 71
    iput p2, v0, Lcom/google/t/c/g;->b:I

    .line 73
    invoke-virtual {p0}, Landroid/location/Location;->getLatitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, v1, Lcom/google/t/c/d;->a:I

    .line 74
    invoke-virtual {p0}, Landroid/location/Location;->getLongitude()D

    move-result-wide v2

    mul-double/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Math;->round(D)J

    move-result-wide v2

    long-to-int v2, v2

    iput v2, v1, Lcom/google/t/c/d;->b:I

    .line 75
    iput-object v1, v0, Lcom/google/t/c/g;->e:Lcom/google/t/c/d;

    .line 76
    invoke-virtual {p0}, Landroid/location/Location;->getTime()J

    move-result-wide v2

    .line 77
    sget-object v1, Ljava/util/concurrent/TimeUnit;->MILLISECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3}, Ljava/util/concurrent/TimeUnit;->toMicros(J)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/t/c/g;->c:J

    .line 79
    invoke-virtual {p0}, Landroid/location/Location;->hasAccuracy()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 80
    invoke-virtual {p0}, Landroid/location/Location;->getAccuracy()F

    move-result v1

    float-to-double v2, v1

    const-wide v4, 0x408f400000000000L    # 1000.0

    mul-double/2addr v2, v4

    double-to-float v1, v2

    iput v1, v0, Lcom/google/t/c/g;->h:F

    .line 83
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "w "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    const/16 v2, 0xa

    invoke-static {v0, v2}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
