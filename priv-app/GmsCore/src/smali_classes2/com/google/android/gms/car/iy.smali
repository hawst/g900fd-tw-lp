.class public final Lcom/google/android/gms/car/iy;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field volatile a:Z

.field volatile b:J

.field private final c:Lcom/google/android/gms/car/ov;

.field private d:J

.field private e:I

.field private f:I

.field private final g:Z

.field private final h:I

.field private i:J

.field private volatile j:J


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ov;ZI)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-boolean v1, p0, Lcom/google/android/gms/car/iy;->a:Z

    .line 32
    iput v1, p0, Lcom/google/android/gms/car/iy;->e:I

    .line 33
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/car/iy;->f:I

    .line 42
    iput-object p1, p0, Lcom/google/android/gms/car/iy;->c:Lcom/google/android/gms/car/ov;

    .line 43
    iput-boolean p2, p0, Lcom/google/android/gms/car/iy;->g:Z

    .line 44
    iput p3, p0, Lcom/google/android/gms/car/iy;->h:I

    .line 45
    invoke-direct {p0, v1}, Lcom/google/android/gms/car/iy;->b(Z)V

    .line 46
    return-void
.end method

.method private b(J)V
    .locals 3

    .prologue
    .line 139
    iget v0, p0, Lcom/google/android/gms/car/iy;->e:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/gms/car/iy;->e:I

    .line 140
    iget v0, p0, Lcom/google/android/gms/car/iy;->e:I

    if-gez v0, :cond_0

    .line 141
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/gms/car/iy;->e:I

    .line 143
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/car/iy;->c:Lcom/google/android/gms/car/ov;

    iget v1, p0, Lcom/google/android/gms/car/iy;->e:I

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/gms/car/ov;->a(JI)V

    .line 144
    return-void
.end method

.method private b(Z)V
    .locals 4

    .prologue
    const-wide/16 v2, 0x32

    .line 49
    if-eqz p1, :cond_0

    .line 50
    const-wide/16 v0, 0x12c

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->i:J

    .line 51
    const-wide/16 v0, 0x64

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->b:J

    .line 52
    const-wide/16 v0, 0x96

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->j:J

    .line 64
    :goto_0
    return-void

    .line 54
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/iy;->g:Z

    if-nez v0, :cond_1

    .line 55
    iput-wide v2, p0, Lcom/google/android/gms/car/iy;->i:J

    .line 56
    const-wide/16 v0, 0x11

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->b:J

    .line 57
    const-wide/16 v0, 0x16

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->j:J

    goto :goto_0

    .line 59
    :cond_1
    const-wide/16 v0, 0x50

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->i:J

    .line 60
    const-wide/16 v0, 0x23

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->b:J

    .line 61
    iput-wide v2, p0, Lcom/google/android/gms/car/iy;->j:J

    goto :goto_0
.end method


# virtual methods
.method public final declared-synchronized a()V
    .locals 4

    .prologue
    .line 71
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/car/iy;->a:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 79
    :goto_0
    monitor-exit p0

    return-void

    .line 74
    :cond_0
    :try_start_1
    iget v0, p0, Lcom/google/android/gms/car/iy;->h:I

    if-lez v0, :cond_1

    .line 75
    iget-boolean v0, p0, Lcom/google/android/gms/car/iy;->a:Z

    if-eqz v0, :cond_1

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/gms/car/iy;->d:J

    sub-long/2addr v0, v2

    iget-wide v2, p0, Lcom/google/android/gms/car/iy;->i:J

    cmp-long v0, v0, v2

    if-lez v0, :cond_2

    iget-wide v0, p0, Lcom/google/android/gms/car/iy;->b:J

    :goto_1
    invoke-virtual {p0, v0, v1}, Lcom/google/android/gms/car/iy;->a(J)V

    .line 77
    :cond_1
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/iy;->d:J

    .line 78
    iget-object v0, p0, Lcom/google/android/gms/car/iy;->c:Lcom/google/android/gms/car/ov;

    invoke-virtual {v0}, Lcom/google/android/gms/car/ov;->h()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 71
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 75
    :cond_2
    :try_start_2
    iget-wide v0, p0, Lcom/google/android/gms/car/iy;->j:J
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1
.end method

.method public final declared-synchronized a(J)V
    .locals 5

    .prologue
    .line 109
    monitor-enter p0

    .line 110
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/iy;->e:I

    iput v0, p0, Lcom/google/android/gms/car/iy;->f:I

    .line 114
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/iy;->b(J)V

    .line 115
    const/4 v0, 0x1

    :goto_0
    iget v1, p0, Lcom/google/android/gms/car/iy;->h:I

    if-ge v0, v1, :cond_0

    .line 116
    iget-wide v2, p0, Lcom/google/android/gms/car/iy;->b:J

    add-long/2addr p1, v2

    .line 117
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/iy;->b(J)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 115
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    :cond_0
    monitor-exit p0

    return-void

    .line 109
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(Z)V
    .locals 1

    .prologue
    .line 135
    monitor-enter p0

    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/car/iy;->b(Z)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    monitor-exit p0

    return-void

    .line 135
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final declared-synchronized a(I)Z
    .locals 1

    .prologue
    .line 123
    monitor-enter p0

    :try_start_0
    iget v0, p0, Lcom/google/android/gms/car/iy;->f:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-gt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    monitor-exit p0

    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
