.class public final Lcom/google/android/gms/f/a/c;
.super Lcom/google/protobuf/nano/j;
.source "SourceFile"


# static fields
.field private static volatile e:[Lcom/google/android/gms/f/a/c;


# instance fields
.field public a:Lcom/google/android/gms/f/a/d;

.field public b:Lcom/google/android/gms/f/a/d;

.field public c:Ljava/lang/Integer;

.field public d:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 626
    invoke-direct {p0}, Lcom/google/protobuf/nano/j;-><init>()V

    .line 627
    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/f/a/c;->cachedSize:I

    .line 628
    return-void
.end method

.method public static a()[Lcom/google/android/gms/f/a/c;
    .locals 2

    .prologue
    .line 603
    sget-object v0, Lcom/google/android/gms/f/a/c;->e:[Lcom/google/android/gms/f/a/c;

    if-nez v0, :cond_1

    .line 604
    sget-object v1, Lcom/google/protobuf/nano/h;->a:Ljava/lang/Object;

    monitor-enter v1

    .line 606
    :try_start_0
    sget-object v0, Lcom/google/android/gms/f/a/c;->e:[Lcom/google/android/gms/f/a/c;

    if-nez v0, :cond_0

    .line 607
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/gms/f/a/c;

    sput-object v0, Lcom/google/android/gms/f/a/c;->e:[Lcom/google/android/gms/f/a/c;

    .line 609
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 611
    :cond_1
    sget-object v0, Lcom/google/android/gms/f/a/c;->e:[Lcom/google/android/gms/f/a/c;

    return-object v0

    .line 609
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method protected final computeSerializedSize()I
    .locals 3

    .prologue
    .line 659
    invoke-super {p0}, Lcom/google/protobuf/nano/j;->computeSerializedSize()I

    move-result v0

    .line 660
    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    if-eqz v1, :cond_0

    .line 661
    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 664
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    if-eqz v1, :cond_1

    .line 665
    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILcom/google/protobuf/nano/j;)I

    move-result v1

    add-int/2addr v0, v1

    .line 668
    :cond_1
    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 669
    const/4 v1, 0x3

    iget-object v2, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->f(II)I

    move-result v1

    add-int/2addr v0, v1

    .line 672
    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    if-eqz v1, :cond_3

    .line 673
    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/protobuf/nano/b;->b(ILjava/lang/String;)I

    move-result v1

    add-int/2addr v0, v1

    .line 676
    :cond_3
    return v0
.end method

.method public final synthetic mergeFrom(Lcom/google/protobuf/nano/a;)Lcom/google/protobuf/nano/j;
    .locals 1

    .prologue
    .line 453
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->a()I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    invoke-static {p1, v0}, Lcom/google/protobuf/nano/m;->a(Lcom/google/protobuf/nano/a;I)Z

    move-result v0

    if-nez v0, :cond_0

    :sswitch_0
    return-object p0

    :sswitch_1
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    if-nez v0, :cond_1

    new-instance v0, Lcom/google/android/gms/f/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_2
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    if-nez v0, :cond_2

    new-instance v0, Lcom/google/android/gms/f/a/d;

    invoke-direct {v0}, Lcom/google/android/gms/f/a/d;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/a;->a(Lcom/google/protobuf/nano/j;)V

    goto :goto_0

    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->i()I

    move-result v0

    sparse-switch v0, :sswitch_data_1

    goto :goto_0

    :sswitch_4
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    goto :goto_0

    :sswitch_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/a;->e()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    goto :goto_0

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x18 -> :sswitch_3
        0x22 -> :sswitch_5
    .end sparse-switch

    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_4
        0x1 -> :sswitch_4
        0xf -> :sswitch_4
    .end sparse-switch
.end method

.method public final writeTo(Lcom/google/protobuf/nano/b;)V
    .locals 2

    .prologue
    .line 642
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    if-eqz v0, :cond_0

    .line 643
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->a:Lcom/google/android/gms/f/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 645
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    if-eqz v0, :cond_1

    .line 646
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->b:Lcom/google/android/gms/f/a/d;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILcom/google/protobuf/nano/j;)V

    .line 648
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    if-eqz v0, :cond_2

    .line 649
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->c:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(II)V

    .line 651
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    if-eqz v0, :cond_3

    .line 652
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/gms/f/a/c;->d:Ljava/lang/String;

    invoke-virtual {p1, v0, v1}, Lcom/google/protobuf/nano/b;->a(ILjava/lang/String;)V

    .line 654
    :cond_3
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/j;->writeTo(Lcom/google/protobuf/nano/b;)V

    .line 655
    return-void
.end method
