.class public final Lcom/google/android/gms/udc/ui/w;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/support/v4/app/av;


# instance fields
.field final synthetic a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)V
    .locals 0

    .prologue
    .line 228
    iput-object p1, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(ILandroid/os/Bundle;)Landroid/support/v4/a/j;
    .locals 4

    .prologue
    .line 234
    new-instance v0, Lcom/google/android/gms/udc/e/f;

    invoke-direct {v0}, Lcom/google/android/gms/udc/e/f;-><init>()V

    .line 235
    iget-object v1, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    const/4 v2, 0x4

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/gms/udc/c/w;->a(Landroid/content/Context;ILjava/lang/String;)Lcom/google/android/gms/udc/e/u;

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/udc/e/f;->c:Lcom/google/android/gms/udc/e/u;

    .line 237
    const/4 v1, 0x1

    new-array v1, v1, [I

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->c(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/udc/e/m;

    move-result-object v3

    iget-object v3, v3, Lcom/google/android/gms/udc/e/m;->a:Lcom/google/android/gms/udc/e/p;

    iget v3, v3, Lcom/google/android/gms/udc/e/p;->a:I

    aput v3, v1, v2

    iput-object v1, v0, Lcom/google/android/gms/udc/e/f;->b:[I

    .line 239
    new-instance v1, Lcom/google/android/gms/udc/c/i;

    iget-object v2, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    iget-object v3, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v3}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/common/api/v;

    move-result-object v3

    invoke-direct {v1, v2, v3, v0}, Lcom/google/android/gms/udc/c/i;-><init>(Landroid/content/Context;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/udc/e/f;)V

    .line 241
    const-wide/16 v2, 0xa

    sget-object v0, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v0}, Lcom/google/android/gms/udc/c/i;->a(JLjava/util/concurrent/TimeUnit;)V

    .line 243
    return-object v1
.end method

.method public final a(Landroid/support/v4/a/j;)V
    .locals 0

    .prologue
    .line 275
    return-void
.end method

.method public final synthetic a(Landroid/support/v4/a/j;Ljava/lang/Object;)V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 228
    check-cast p2, Lcom/google/android/gms/udc/f;

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    new-instance v1, Lcom/google/android/gms/analytics/bi;

    invoke-direct {v1}, Lcom/google/android/gms/analytics/bi;-><init>()V

    invoke-virtual {v1}, Lcom/google/android/gms/analytics/bi;->a()Ljava/util/Map;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/analytics/bv;->a(Ljava/util/Map;)V

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->b()Lcom/google/protobuf/nano/j;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/udc/e/g;

    iget-object v0, v0, Lcom/google/android/gms/udc/e/g;->b:[Lcom/google/android/gms/udc/e/m;

    array-length v1, v0

    if-lez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    aget-object v0, v0, v4

    invoke-static {v1, v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->a(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;Lcom/google/android/gms/udc/e/m;)Lcom/google/android/gms/udc/e/m;

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->e(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->c(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/udc/e/m;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/gms/udc/ui/UdcSettingDetailFragment;->a(Lcom/google/android/gms/udc/e/m;)V

    :cond_0
    :goto_0
    return-void

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-static {v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->d(Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;)Lcom/google/android/gms/analytics/bv;

    move-result-object v0

    const-string v1, "ReloadOverviewConfig"

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v2

    invoke-static {v0, v1, v2}, Lcom/google/android/gms/udc/util/a;->a(Lcom/google/android/gms/analytics/bv;Ljava/lang/String;I)V

    const-string v0, "UdcSettingDetailActivity"

    const-string v1, "Error reading setting:%s"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    invoke-interface {p2}, Lcom/google/android/gms/udc/f;->C_()Lcom/google/android/gms/common/api/Status;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/gms/common/api/Status;->c()Ljava/lang/String;

    move-result-object v3

    aput-object v3, v2, v4

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/google/android/gms/udc/ui/w;->a:Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;

    invoke-virtual {v0}, Lcom/google/android/gms/udc/ui/UdcSettingDetailActivity;->finish()V

    goto :goto_0
.end method
