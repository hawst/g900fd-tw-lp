.class public Lcom/google/android/gms/search/administration/d;
.super Lcom/google/android/gms/search/a;
.source "SourceFile"


# direct methods
.method public constructor <init>(Lcom/google/android/gms/icing/impl/u;Ljava/lang/String;Lcom/google/android/gms/search/administration/GetStorageStatsCall$Request;)V
    .locals 6

    .prologue
    .line 34
    const/4 v1, 0x1

    const/4 v2, 0x4

    move-object v0, p0

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/search/a;-><init>(IILcom/google/android/gms/icing/impl/u;Ljava/lang/String;Ljava/lang/Object;)V

    .line 35
    return-void
.end method


# virtual methods
.method protected final synthetic a()Ljava/lang/Object;
    .locals 26

    .prologue
    .line 31
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/search/administration/d;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->C()Lcom/google/android/gms/icing/impl/NativeIndex;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->p()Lcom/google/android/gms/icing/bi;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/search/administration/d;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->i()Lcom/google/android/gms/icing/impl/a/x;

    move-result-object v2

    invoke-interface {v2}, Lcom/google/android/gms/icing/impl/a/x;->j()Landroid/util/SparseArray;

    move-result-object v16

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/gms/search/administration/d;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/u;->D()Lcom/google/android/gms/icing/impl/bn;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/gms/icing/impl/bn;->b()Ljava/io/File;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;)J

    move-result-wide v18

    const-wide/16 v6, 0x0

    new-instance v2, Ljava/io/File;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/administration/d;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->z()Landroid/content/Context;

    move-result-object v3

    invoke-virtual {v3}, Landroid/content/Context;->getApplicationInfo()Landroid/content/pm/ApplicationInfo;

    move-result-object v3

    iget-object v3, v3, Landroid/content/pm/ApplicationInfo;->dataDir:Ljava/lang/String;

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->listFiles()[Ljava/io/File;

    move-result-object v5

    array-length v8, v5

    const/4 v2, 0x0

    move v4, v2

    :goto_0
    if-ge v4, v8, :cond_0

    aget-object v2, v5, v4

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v9, "cache"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-virtual {v2}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v3

    const-string v9, "lib"

    invoke-virtual {v3, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    invoke-static {v2}, Lcom/google/android/gms/icing/impl/NativeIndex;->a(Ljava/io/File;)J

    move-result-wide v2

    add-long/2addr v2, v6

    :goto_1
    add-int/lit8 v4, v4, 0x1

    move-wide v6, v2

    goto :goto_0

    :cond_0
    const-wide/16 v10, 0x0

    const-wide/16 v8, 0x0

    const-wide/16 v4, 0x0

    new-instance v17, Ljava/util/HashMap;

    invoke-direct/range {v17 .. v17}, Ljava/util/HashMap;-><init>()V

    iget-object v0, v12, Lcom/google/android/gms/icing/bi;->a:[Lcom/google/android/gms/icing/bj;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    array-length v0, v0

    move/from16 v21, v0

    const/4 v2, 0x0

    move-wide v12, v4

    move-wide v14, v8

    move v5, v2

    move-wide v8, v10

    :goto_2
    move/from16 v0, v21

    if-ge v5, v0, :cond_3

    aget-object v4, v20, v5

    iget-wide v0, v4, Lcom/google/android/gms/icing/bj;->d:J

    move-wide/from16 v22, v0

    iget-wide v0, v4, Lcom/google/android/gms/icing/bj;->e:J

    move-wide/from16 v24, v0

    add-long v14, v14, v22

    add-long v10, v12, v24

    iget v2, v4, Lcom/google/android/gms/icing/bj;->a:I

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    if-nez v2, :cond_2

    add-long v2, v8, v24

    const-wide/16 v8, 0x0

    cmp-long v8, v22, v8

    if-eqz v8, :cond_1

    const-string v8, "Corpus %d from an unknown package using %d bytes."

    iget v4, v4, Lcom/google/android/gms/icing/bj;->a:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v9

    invoke-static {v8, v4, v9}, Lcom/google/android/gms/icing/c;->d(Ljava/lang/String;Ljava/lang/Object;Ljava/lang/Object;)I

    :cond_1
    :goto_3
    add-int/lit8 v4, v5, 0x1

    move v5, v4

    move-wide v12, v10

    move-wide v8, v2

    goto :goto_2

    :cond_2
    move-object/from16 v0, v17

    invoke-interface {v0, v2}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/util/Pair;

    if-nez v3, :cond_7

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    const-wide/16 v12, 0x0

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    move-object v4, v3

    :goto_4
    iget-object v3, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v12

    add-long v12, v12, v22

    invoke-static {v12, v13}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    iget-object v3, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v22

    add-long v22, v22, v24

    invoke-static/range {v22 .. v23}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-static {v12, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-interface {v0, v2, v3}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-wide v2, v8

    goto :goto_3

    :cond_3
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    invoke-interface/range {v17 .. v17}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v16

    :goto_5
    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_6

    invoke-interface/range {v16 .. v16}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v4, v2

    check-cast v4, Ljava/util/Map$Entry;

    invoke-interface {v4}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/util/Pair;

    const-wide/16 v10, 0x0

    const-wide/16 v20, 0x0

    cmp-long v3, v14, v20

    if-eqz v3, :cond_4

    sub-long v10, v18, v12

    iget-object v3, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v3, Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v20

    mul-long v10, v10, v20

    div-long/2addr v10, v14

    :cond_4
    new-instance v17, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    invoke-direct/range {v17 .. v17}, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;-><init>()V

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, v17

    iput-object v3, v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->a:Ljava/lang/String;

    move-object/from16 v0, v17

    iput-wide v10, v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->b:J

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/gms/search/administration/d;->a:Lcom/google/android/gms/icing/impl/u;

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/u;->A()Lcom/google/android/gms/icing/impl/a/f;

    move-result-object v10

    invoke-interface {v4}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    invoke-virtual {v10, v3}, Lcom/google/android/gms/icing/impl/a/f;->c(Ljava/lang/String;)Lcom/google/android/gms/icing/impl/a/e;

    move-result-object v3

    if-eqz v3, :cond_5

    invoke-virtual {v3}, Lcom/google/android/gms/icing/impl/a/e;->d()Z

    move-result v3

    if-eqz v3, :cond_5

    const/4 v3, 0x1

    :goto_6
    move-object/from16 v0, v17

    iput-boolean v3, v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->c:Z

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/lang/Long;

    invoke-virtual {v2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    move-object/from16 v0, v17

    iput-wide v2, v0, Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;->d:J

    move-object/from16 v0, v17

    invoke-interface {v5, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_5

    :cond_5
    const/4 v3, 0x0

    goto :goto_6

    :cond_6
    new-instance v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;

    invoke-direct {v3}, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;-><init>()V

    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v2, v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->a:Lcom/google/android/gms/common/api/Status;

    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    invoke-interface {v5, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v2

    check-cast v2, [Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    iput-object v2, v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->b:[Lcom/google/android/gms/search/administration/GetStorageStatsCall$PackageStats;

    iput-wide v8, v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->c:J

    move-wide/from16 v0, v18

    iput-wide v0, v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->d:J

    iput-wide v6, v3, Lcom/google/android/gms/search/administration/GetStorageStatsCall$Response;->e:J

    return-object v3

    :cond_7
    move-object v4, v3

    goto/16 :goto_4

    :cond_8
    move-wide v2, v6

    goto/16 :goto_1
.end method
