.class final Lcom/google/android/gms/games/service/ab;
.super Lcom/google/android/gms/games/internal/eb;
.source "SourceFile"


# instance fields
.field private final a:Landroid/content/Context;


# direct methods
.method constructor <init>(Landroid/content/Context;)V
    .locals 1

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/gms/games/internal/eb;-><init>()V

    .line 44
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/common/a/d;->a(Landroid/content/Context;)V

    .line 46
    iput-object p1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    .line 47
    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;
    .locals 4

    .prologue
    .line 224
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v0

    .line 225
    iget-object v0, v0, Landroid/content/pm/PackageInfo;->applicationInfo:Landroid/content/pm/ApplicationInfo;

    iget v1, v0, Landroid/content/pm/ApplicationInfo;->uid:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 232
    new-instance v0, Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {v0, v1, p1, p1, p2}, Lcom/google/android/gms/common/server/ClientContext;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 234
    invoke-virtual {v0, p3}, Lcom/google/android/gms/common/server/ClientContext;->a([Ljava/lang/String;)V

    .line 235
    :goto_0
    return-object v0

    .line 226
    :catch_0
    move-exception v0

    .line 227
    const-string v1, "GamesSignInService"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Bad package name: "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 228
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static a()V
    .locals 2

    .prologue
    .line 55
    invoke-static {}, Landroid/os/Process;->myUid()I

    move-result v0

    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 56
    new-instance v0, Ljava/lang/SecurityException;

    invoke-direct {v0}, Ljava/lang/SecurityException;-><init>()V

    throw v0

    .line 58
    :cond_0
    return-void
.end method


# virtual methods
.method public final a(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/i/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final a(Ljava/lang/String;Z)Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    if-eqz p2, :cond_0

    .line 84
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/i/a;->b(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 86
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/common/util/a;->a(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 114
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 115
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 116
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p1, p2}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V

    .line 119
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 139
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 140
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 141
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 142
    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 144
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 146
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V

    .line 147
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 124
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 125
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 127
    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    const-string v0, "game package name is missing"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 132
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    return-void
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 178
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 179
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 180
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 181
    const-string v0, "package name is missing"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 182
    const-string v0, "scopes are missing"

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 185
    invoke-direct {p0, p2, p4, p5}, Lcom/google/android/gms/games/service/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 186
    if-nez v0, :cond_0

    .line 188
    const/4 v0, 0x1

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/common/data/DataHolder;->b(I)Lcom/google/android/gms/common/data/DataHolder;

    move-result-object v0

    invoke-interface {p1, v0}, Lcom/google/android/gms/games/internal/dx;->c(Lcom/google/android/gms/common/data/DataHolder;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 198
    :goto_0
    return-void

    .line 196
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p3, p1}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;Lcom/google/android/gms/games/internal/dx;)V

    goto :goto_0

    .line 192
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 94
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 95
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 96
    const-string v0, "package name is missing"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 97
    const-string v0, "scopes are missing"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 100
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/games/service/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 101
    if-nez v0, :cond_0

    .line 103
    const/4 v0, 0x1

    const/4 v1, 0x0

    :try_start_0
    invoke-interface {p1, v0, v1}, Lcom/google/android/gms/games/internal/dx;->a(ILandroid/content/Intent;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 110
    :goto_0
    return-void

    .line 109
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;)V

    goto :goto_0

    .line 107
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 203
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 204
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 206
    const-string v0, "player ID is missing"

    invoke-static {p5, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 208
    invoke-direct {p0, p2, p3, p4}, Lcom/google/android/gms/games/service/ab;->a(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 209
    if-nez v0, :cond_0

    .line 211
    const/4 v0, 0x1

    :try_start_0
    invoke-interface {p1, v0}, Lcom/google/android/gms/games/internal/dx;->b(I)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 218
    :goto_0
    return-void

    .line 217
    :cond_0
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p5}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;)V

    goto :goto_0

    .line 215
    :catch_0
    move-exception v0

    goto :goto_0
.end method

.method public final a(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 166
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 167
    const-string v0, "Account name is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 168
    const-string v0, "Must provide a non-empty game ID"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/String;Ljava/lang/Object;)Ljava/lang/String;

    .line 170
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p1}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 172
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p2}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->a(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)V

    .line 173
    return-void
.end method

.method public final b(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 67
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/gms/games/service/ab;->a(Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final b(Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 152
    invoke-static {}, Lcom/google/android/gms/games/service/ab;->a()V

    .line 153
    const-string v0, "callbacks is missing"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 154
    const-string v0, "account name is missing"

    invoke-static {p2, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 155
    const-string v0, "game ID is missing"

    invoke-static {p3, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 156
    const-string v0, "ACL data is missing"

    invoke-static {p4, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 158
    iget-object v0, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v0, p2}, Lcom/google/android/gms/games/a/l;->a(Landroid/content/Context;Ljava/lang/String;)Lcom/google/android/gms/common/server/ClientContext;

    move-result-object v0

    .line 160
    iget-object v1, p0, Lcom/google/android/gms/games/service/ab;->a:Landroid/content/Context;

    invoke-static {v1, v0, p1, p3, p4}, Lcom/google/android/gms/games/service/GamesSignInIntentService;->b(Landroid/content/Context;Lcom/google/android/gms/common/server/ClientContext;Lcom/google/android/gms/games/internal/dx;Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    return-void
.end method
