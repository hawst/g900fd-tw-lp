.class public final Lcom/google/android/gms/car/diagnostics/a;
.super Ljava/lang/Object;
.source "SourceFile"


# instance fields
.field a:Ljava/lang/String;

.field b:Ljava/lang/Throwable;

.field c:Ljava/lang/String;

.field d:Ljava/lang/String;

.field e:Z

.field transient f:Ljava/io/File;

.field private g:Ljava/lang/String;

.field private h:Ljava/util/Date;

.field private i:Z

.field private j:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/util/Date;)V
    .locals 0

    .prologue
    .line 129
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    iput-object p1, p0, Lcom/google/android/gms/car/diagnostics/a;->h:Ljava/util/Date;

    .line 131
    return-void
.end method

.method public static a(Ljava/io/File;)Lcom/google/android/gms/car/diagnostics/a;
    .locals 6

    .prologue
    .line 72
    new-instance v0, Ljava/io/FileInputStream;

    invoke-direct {v0, p0}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    .line 73
    invoke-static {v0}, Lcom/google/android/gms/common/util/ab;->a(Ljava/io/InputStream;)[B

    move-result-object v0

    .line 74
    invoke-static {v0}, Lcom/google/android/gms/car/diagnostics/k;->a([B)Lcom/google/android/gms/car/diagnostics/k;

    move-result-object v0

    .line 75
    new-instance v1, Lcom/google/android/gms/car/diagnostics/a;

    new-instance v2, Ljava/util/Date;

    iget-wide v4, v0, Lcom/google/android/gms/car/diagnostics/k;->e:J

    invoke-direct {v2, v4, v5}, Ljava/util/Date;-><init>(J)V

    invoke-direct {v1, v2}, Lcom/google/android/gms/car/diagnostics/a;-><init>(Ljava/util/Date;)V

    .line 76
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->a:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 77
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->a:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/car/diagnostics/a;->a:Ljava/lang/String;

    .line 79
    :cond_0
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->c:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 80
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->c:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/car/diagnostics/a;->c:Ljava/lang/String;

    .line 82
    :cond_1
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->d:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 83
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->d:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/car/diagnostics/a;->d:Ljava/lang/String;

    .line 85
    :cond_2
    iget-boolean v2, v0, Lcom/google/android/gms/car/diagnostics/k;->g:Z

    iput-boolean v2, v1, Lcom/google/android/gms/car/diagnostics/a;->i:Z

    .line 86
    iget-boolean v2, v0, Lcom/google/android/gms/car/diagnostics/k;->h:Z

    iput-boolean v2, v1, Lcom/google/android/gms/car/diagnostics/a;->e:Z

    .line 87
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->i:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_3

    .line 88
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->i:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/android/gms/car/diagnostics/a;->j:Ljava/lang/String;

    .line 91
    :cond_3
    iget-object v2, v0, Lcom/google/android/gms/car/diagnostics/k;->f:[B

    array-length v2, v2

    if-eqz v2, :cond_4

    .line 93
    :try_start_0
    iget-object v0, v0, Lcom/google/android/gms/car/diagnostics/k;->f:[B

    new-instance v2, Ljava/io/ByteArrayInputStream;

    invoke-direct {v2, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    new-instance v3, Ljava/io/ObjectInputStream;

    invoke-direct {v3, v2}, Ljava/io/ObjectInputStream;-><init>(Ljava/io/InputStream;)V

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->readObject()Ljava/lang/Object;

    move-result-object v0

    invoke-virtual {v3}, Ljava/io/ObjectInputStream;->close()V

    check-cast v0, Ljava/lang/Throwable;

    iput-object v0, v1, Lcom/google/android/gms/car/diagnostics/a;->b:Ljava/lang/Throwable;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 98
    :cond_4
    :goto_0
    iput-object p0, v1, Lcom/google/android/gms/car/diagnostics/a;->f:Ljava/io/File;

    .line 99
    return-object v1

    .line 94
    :catch_0
    move-exception v0

    .line 95
    const-string v2, "CAR.FEEDBACK"

    const-string v3, "Couldn\'t find the subclass of the exception."

    invoke-static {v2, v3, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method public static a(Landroid/content/Context;Landroid/graphics/Bitmap;)Ljava/lang/String;
    .locals 4

    .prologue
    .line 41
    invoke-static {p0}, Lcom/google/android/gms/car/diagnostics/l;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 42
    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    .line 45
    :try_start_0
    const-string v1, "feedback_data"

    const-string v2, ""

    invoke-static {v1, v2, v0}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    .line 46
    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v0}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    .line 47
    sget-object v2, Landroid/graphics/Bitmap$CompressFormat;->PNG:Landroid/graphics/Bitmap$CompressFormat;

    const/16 v3, 0x64

    invoke-virtual {p1, v2, v3, v1}, Landroid/graphics/Bitmap;->compress(Landroid/graphics/Bitmap$CompressFormat;ILjava/io/OutputStream;)Z

    .line 48
    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 53
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    .line 55
    invoke-virtual {v0}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v0

    :goto_0
    return-object v0

    .line 49
    :catch_0
    move-exception v0

    .line 50
    :try_start_1
    const-string v1, "CAR.FEEDBACK"

    const-string v2, "Couldn\'t write."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 51
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->recycle()V

    throw v0
.end method


# virtual methods
.method public final a(Landroid/content/Context;)V
    .locals 4

    .prologue
    .line 138
    invoke-static {p1}, Lcom/google/android/gms/car/diagnostics/l;->b(Landroid/content/Context;)Landroid/content/Context;

    move-result-object v0

    .line 139
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->f:Ljava/io/File;

    if-nez v1, :cond_1

    .line 140
    new-instance v1, Ljava/io/File;

    invoke-virtual {v0}, Landroid/content/Context;->getCacheDir()Ljava/io/File;

    move-result-object v0

    const-string v2, "car_feedback_reports"

    invoke-direct {v1, v0, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 141
    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    .line 142
    invoke-virtual {v1}, Ljava/io/File;->isDirectory()Z

    move-result v0

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Ljava/io/IOException;

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "Couldn\'t create directory "

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "report."

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/diagnostics/a;->h:Ljava/util/Date;

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    const-string v2, ".feedbackbundle"

    invoke-static {v0, v2, v1}, Ljava/io/File;->createTempFile(Ljava/lang/String;Ljava/lang/String;Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/a;->f:Ljava/io/File;

    .line 149
    :cond_1
    new-instance v0, Lcom/google/android/gms/car/diagnostics/k;

    invoke-direct {v0}, Lcom/google/android/gms/car/diagnostics/k;-><init>()V

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->a:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->a:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->a:Ljava/lang/String;

    :cond_2
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->g:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->g:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->b:Ljava/lang/String;

    :cond_3
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->c:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->c:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->c:Ljava/lang/String;

    :cond_4
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->d:Ljava/lang/String;

    if-eqz v1, :cond_5

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->d:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->d:Ljava/lang/String;

    :cond_5
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->b:Ljava/lang/Throwable;

    if-eqz v1, :cond_6

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->b:Ljava/lang/Throwable;

    new-instance v2, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v2}, Ljava/io/ByteArrayOutputStream;-><init>()V

    new-instance v3, Ljava/io/ObjectOutputStream;

    invoke-direct {v3, v2}, Ljava/io/ObjectOutputStream;-><init>(Ljava/io/OutputStream;)V

    invoke-virtual {v3, v1}, Ljava/io/ObjectOutputStream;->writeObject(Ljava/lang/Object;)V

    invoke-virtual {v3}, Ljava/io/ObjectOutputStream;->flush()V

    invoke-virtual {v2}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v1

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->f:[B

    :cond_6
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->h:Ljava/util/Date;

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    iput-wide v2, v0, Lcom/google/android/gms/car/diagnostics/k;->e:J

    iget-boolean v1, p0, Lcom/google/android/gms/car/diagnostics/a;->i:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/diagnostics/k;->g:Z

    iget-boolean v1, p0, Lcom/google/android/gms/car/diagnostics/a;->e:Z

    iput-boolean v1, v0, Lcom/google/android/gms/car/diagnostics/k;->h:Z

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->j:Ljava/lang/String;

    if-eqz v1, :cond_7

    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/a;->j:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/android/gms/car/diagnostics/k;->i:Ljava/lang/String;

    :cond_7
    invoke-static {v0}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v0

    new-instance v1, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/google/android/gms/car/diagnostics/a;->f:Ljava/io/File;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-virtual {v1, v0}, Ljava/io/FileOutputStream;->write([B)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 150
    return-void
.end method
