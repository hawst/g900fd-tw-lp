.class public final Lcom/google/android/gms/games/provider/ab;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Landroid/provider/BaseColumns;
.implements Lcom/google/android/gms/games/internal/cw;


# direct methods
.method private static a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 350
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/e;->a(Z)V

    .line 351
    const-string v0, "ext_game"

    invoke-virtual {p0, v0}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0

    .line 350
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static a(Landroid/net/Uri;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 303
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 323
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 345
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/ab;->a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 295
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;J)Landroid/net/Uri;
    .locals 3

    .prologue
    .line 312
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {p1, p2}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1

    .prologue
    .line 335
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Lcom/google/android/gms/common/server/ClientContext;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-static {v0, p1}, Lcom/google/android/gms/games/provider/ab;->a(Landroid/net/Uri$Builder;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public static a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2

    .prologue
    .line 389
    const-string v0, "games"

    invoke-static {p0, v0}, Lcom/google/android/gms/games/provider/ac;->a(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "ext_game"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "icon_image"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
