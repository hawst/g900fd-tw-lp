.class public final Lcom/google/android/gms/common/people/views/c;
.super Ljava/lang/Object;
.source "SourceFile"


# annotations
.annotation runtime Ljava/lang/Deprecated;
.end annotation


# direct methods
.method public static a(Landroid/content/Context;Lcom/google/android/gms/common/people/data/AudienceMember;)Lcom/google/android/gms/common/people/views/d;
    .locals 5

    .prologue
    .line 37
    invoke-static {p1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;)Ljava/lang/Object;

    .line 42
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v0

    packed-switch v0, :pswitch_data_0

    .line 87
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown audience member type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :pswitch_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v0

    packed-switch v0, :pswitch_data_1

    .line 71
    :pswitch_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Unknown circle type: "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->c()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 46
    :pswitch_2
    sget v2, Lcom/google/android/gms/h;->w:I

    .line 47
    sget v0, Lcom/google/android/gms/p;->em:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 48
    sget v1, Lcom/google/android/gms/h;->F:I

    .line 91
    :goto_0
    new-instance v3, Lcom/google/android/gms/common/people/views/d;

    const/4 v4, 0x0

    invoke-direct {v3, v2, v0, v1, v4}, Lcom/google/android/gms/common/people/views/d;-><init>(ILjava/lang/String;IB)V

    return-object v3

    .line 51
    :pswitch_3
    sget v2, Lcom/google/android/gms/h;->w:I

    .line 52
    sget v0, Lcom/google/android/gms/p;->ek:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 53
    sget v1, Lcom/google/android/gms/h;->C:I

    goto :goto_0

    .line 56
    :pswitch_4
    sget v2, Lcom/google/android/gms/h;->v:I

    .line 57
    sget v0, Lcom/google/android/gms/p;->en:I

    invoke-virtual {p0, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 58
    sget v1, Lcom/google/android/gms/h;->A:I

    goto :goto_0

    .line 61
    :pswitch_5
    sget v2, Lcom/google/android/gms/h;->w:I

    .line 62
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 63
    sget v1, Lcom/google/android/gms/h;->B:I

    goto :goto_0

    .line 66
    :pswitch_6
    sget v2, Lcom/google/android/gms/h;->v:I

    .line 67
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 68
    sget v1, Lcom/google/android/gms/h;->A:I

    goto :goto_0

    .line 76
    :pswitch_7
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->e()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/gms/people/internal/at;->i(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 77
    sget v2, Lcom/google/android/gms/h;->x:I

    .line 78
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 79
    sget v1, Lcom/google/android/gms/h;->G:I

    goto :goto_0

    .line 81
    :cond_0
    sget v2, Lcom/google/android/gms/h;->v:I

    .line 82
    invoke-virtual {p1}, Lcom/google/android/gms/common/people/data/AudienceMember;->f()Ljava/lang/String;

    move-result-object v0

    .line 83
    sget v1, Lcom/google/android/gms/h;->E:I

    goto :goto_0

    .line 42
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_7
    .end packed-switch

    .line 44
    :pswitch_data_1
    .packed-switch -0x1
        :pswitch_6
        :pswitch_1
        :pswitch_2
        :pswitch_5
        :pswitch_4
        :pswitch_3
    .end packed-switch
.end method
