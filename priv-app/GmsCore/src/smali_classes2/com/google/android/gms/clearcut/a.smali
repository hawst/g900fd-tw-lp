.class public final Lcom/google/android/gms/clearcut/a;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/common/api/j;

.field public static final b:Lcom/google/android/gms/common/api/i;

.field public static final c:Lcom/google/android/gms/common/api/c;

.field public static final d:Lcom/google/android/gms/clearcut/e;


# instance fields
.field private final e:Ljava/lang/String;

.field private final f:I

.field private g:Ljava/lang/String;

.field private h:I

.field private i:Ljava/lang/String;

.field private j:Ljava/lang/String;

.field private final k:Lcom/google/android/gms/clearcut/e;

.field private final l:Lcom/google/android/gms/common/util/p;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 46
    new-instance v0, Lcom/google/android/gms/common/api/j;

    invoke-direct {v0}, Lcom/google/android/gms/common/api/j;-><init>()V

    sput-object v0, Lcom/google/android/gms/clearcut/a;->a:Lcom/google/android/gms/common/api/j;

    .line 49
    new-instance v0, Lcom/google/android/gms/clearcut/b;

    invoke-direct {v0}, Lcom/google/android/gms/clearcut/b;-><init>()V

    sput-object v0, Lcom/google/android/gms/clearcut/a;->b:Lcom/google/android/gms/common/api/i;

    .line 71
    new-instance v0, Lcom/google/android/gms/common/api/c;

    sget-object v1, Lcom/google/android/gms/clearcut/a;->b:Lcom/google/android/gms/common/api/i;

    sget-object v2, Lcom/google/android/gms/clearcut/a;->a:Lcom/google/android/gms/common/api/j;

    const/4 v3, 0x0

    new-array v3, v3, [Lcom/google/android/gms/common/api/Scope;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/gms/common/api/c;-><init>(Lcom/google/android/gms/common/api/i;Lcom/google/android/gms/common/api/j;[Lcom/google/android/gms/common/api/Scope;)V

    sput-object v0, Lcom/google/android/gms/clearcut/a;->c:Lcom/google/android/gms/common/api/c;

    .line 73
    new-instance v0, Lcom/google/android/gms/clearcut/a/a;

    invoke-direct {v0}, Lcom/google/android/gms/clearcut/a/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/clearcut/a;->d:Lcom/google/android/gms/clearcut/e;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;)V
    .locals 7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 141
    sget-object v5, Lcom/google/android/gms/clearcut/a;->d:Lcom/google/android/gms/clearcut/e;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/clearcut/e;Lcom/google/android/gms/common/util/p;)V

    .line 144
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;ILjava/lang/String;Ljava/lang/String;Lcom/google/android/gms/clearcut/e;Lcom/google/android/gms/common/util/p;)V
    .locals 1
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation

    .prologue
    .line 151
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p5, p0, Lcom/google/android/gms/clearcut/a;->k:Lcom/google/android/gms/clearcut/e;

    .line 154
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/a;->e:Ljava/lang/String;

    .line 156
    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/clearcut/a;->f:I

    .line 158
    iput p2, p0, Lcom/google/android/gms/clearcut/a;->h:I

    .line 159
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/gms/clearcut/a;->g:Ljava/lang/String;

    .line 160
    iput-object p3, p0, Lcom/google/android/gms/clearcut/a;->i:Ljava/lang/String;

    .line 161
    iput-object p4, p0, Lcom/google/android/gms/clearcut/a;->j:Ljava/lang/String;

    .line 162
    iput-object p6, p0, Lcom/google/android/gms/clearcut/a;->l:Lcom/google/android/gms/common/util/p;

    .line 163
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 105
    sget-object v5, Lcom/google/android/gms/clearcut/a;->d:Lcom/google/android/gms/clearcut/e;

    invoke-static {}, Lcom/google/android/gms/common/util/r;->c()Lcom/google/android/gms/common/util/p;

    move-result-object v6

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-direct/range {v0 .. v6}, Lcom/google/android/gms/clearcut/a;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/clearcut/e;Lcom/google/android/gms/common/util/p;)V

    .line 108
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/clearcut/e;Lcom/google/android/gms/common/util/p;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 121
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 123
    iput-object p5, p0, Lcom/google/android/gms/clearcut/a;->k:Lcom/google/android/gms/clearcut/e;

    .line 124
    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/clearcut/a;->e:Ljava/lang/String;

    .line 126
    invoke-static {p1}, Lcom/google/android/gms/clearcut/a;->a(Landroid/content/Context;)I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/clearcut/a;->f:I

    .line 128
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Ljava/lang/String;->length()I

    move-result v0

    if-nez v0, :cond_1

    .line 129
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "mLogSourceName may not be empty"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 131
    :cond_1
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/gms/clearcut/a;->h:I

    .line 132
    iput-object p2, p0, Lcom/google/android/gms/clearcut/a;->g:Ljava/lang/String;

    .line 133
    iput-object v1, p0, Lcom/google/android/gms/clearcut/a;->i:Ljava/lang/String;

    .line 134
    iput-object v1, p0, Lcom/google/android/gms/clearcut/a;->j:Ljava/lang/String;

    .line 135
    iput-object p6, p0, Lcom/google/android/gms/clearcut/a;->l:Lcom/google/android/gms/common/util/p;

    .line 136
    return-void
.end method

.method private static a(Landroid/content/Context;)I
    .locals 4

    .prologue
    const/4 v0, 0x0

    .line 166
    .line 168
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v1

    iget v0, v1, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 173
    :goto_0
    return v0

    .line 171
    :catch_0
    move-exception v1

    const-string v1, "ClearcutLogger"

    const-string v2, "This can\'t happen."

    invoke-static {v1, v2}, Landroid/util/Log;->wtf(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method static synthetic a(Lcom/google/android/gms/clearcut/a;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/gms/clearcut/a;->h:I

    return v0
.end method

.method static synthetic b(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->g:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic c(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->i:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic d(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->j:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic e(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/common/util/p;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->l:Lcom/google/android/gms/common/util/p;

    return-object v0
.end method

.method static synthetic f(Lcom/google/android/gms/clearcut/a;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->e:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic g(Lcom/google/android/gms/clearcut/a;)I
    .locals 1

    .prologue
    .line 45
    iget v0, p0, Lcom/google/android/gms/clearcut/a;->f:I

    return v0
.end method

.method static synthetic h(Lcom/google/android/gms/clearcut/a;)Lcom/google/android/gms/clearcut/e;
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->k:Lcom/google/android/gms/clearcut/e;

    return-object v0
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/clearcut/d;)Lcom/google/android/gms/clearcut/c;
    .locals 2

    .prologue
    .line 202
    new-instance v0, Lcom/google/android/gms/clearcut/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;Lcom/google/android/gms/clearcut/d;B)V

    return-object v0
.end method

.method public final a(Lcom/google/protobuf/nano/j;)Lcom/google/android/gms/clearcut/c;
    .locals 3

    .prologue
    .line 183
    new-instance v0, Lcom/google/android/gms/clearcut/c;

    invoke-static {p1}, Lcom/google/protobuf/nano/j;->toByteArray(Lcom/google/protobuf/nano/j;)[B

    move-result-object v1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[BB)V

    return-object v0
.end method

.method public final a([B)Lcom/google/android/gms/clearcut/c;
    .locals 2

    .prologue
    .line 191
    new-instance v0, Lcom/google/android/gms/clearcut/c;

    const/4 v1, 0x0

    invoke-direct {v0, p0, p1, v1}, Lcom/google/android/gms/clearcut/c;-><init>(Lcom/google/android/gms/clearcut/a;[BB)V

    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z
    .locals 2

    .prologue
    .line 217
    iget-object v0, p0, Lcom/google/android/gms/clearcut/a;->k:Lcom/google/android/gms/clearcut/e;

    invoke-interface {v0, p1, p2, p3, p4}, Lcom/google/android/gms/clearcut/e;->a(Lcom/google/android/gms/common/api/v;JLjava/util/concurrent/TimeUnit;)Z

    move-result v0

    return v0
.end method
