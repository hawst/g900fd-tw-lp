.class public final Lcom/google/android/gms/car/senderprotocol/bc;
.super Lcom/google/android/gms/car/senderprotocol/az;
.source "SourceFile"


# instance fields
.field public volatile a:Z

.field private final b:Lcom/google/android/gms/car/senderprotocol/bd;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/car/ig;Lcom/google/android/gms/car/gx;)V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/car/senderprotocol/az;-><init>(Lcom/google/android/gms/car/gw;Lcom/google/android/gms/car/gx;)V

    .line 28
    iput-object p1, p0, Lcom/google/android/gms/car/senderprotocol/bc;->b:Lcom/google/android/gms/car/senderprotocol/bd;

    .line 29
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 67
    invoke-super {p0}, Lcom/google/android/gms/car/senderprotocol/az;->a()V

    .line 68
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/bc;->a:Z

    .line 69
    monitor-enter p0

    .line 70
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 71
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public final a(I)V
    .locals 1

    .prologue
    .line 57
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/car/senderprotocol/bc;->a:Z

    .line 58
    invoke-super {p0, p1}, Lcom/google/android/gms/car/senderprotocol/az;->a(I)V

    .line 60
    monitor-enter p0

    .line 61
    :try_start_0
    invoke-virtual {p0}, Ljava/lang/Object;->notify()V

    .line 62
    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final a(ILjava/nio/ByteBuffer;)V
    .locals 2

    .prologue
    .line 42
    new-instance v0, Ljava/lang/RuntimeException;

    const-string v1, "Method should not be called."

    invoke-direct {v0, v1}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public final a(Ljava/nio/ByteBuffer;)V
    .locals 3

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/gms/car/senderprotocol/bc;->b:Lcom/google/android/gms/car/senderprotocol/bd;

    if-eqz v0, :cond_0

    .line 34
    invoke-virtual {p1}, Ljava/nio/ByteBuffer;->limit()I

    move-result v0

    new-array v0, v0, [B

    .line 35
    const/4 v1, 0x0

    array-length v2, v0

    invoke-virtual {p1, v0, v1, v2}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 36
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/bc;->b:Lcom/google/android/gms/car/senderprotocol/bd;

    invoke-interface {v1, v0}, Lcom/google/android/gms/car/senderprotocol/bd;->a([B)V

    .line 38
    :cond_0
    return-void
.end method

.method public final a([BI)V
    .locals 3

    .prologue
    .line 46
    invoke-static {p2}, Lcom/google/android/c/a/a;->a(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 47
    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1, p2}, Ljava/nio/ByteBuffer;->put([BII)Ljava/nio/ByteBuffer;

    .line 48
    iget-object v1, p0, Lcom/google/android/gms/car/senderprotocol/bc;->i:Lcom/google/android/gms/car/senderprotocol/e;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2}, Lcom/google/android/gms/car/senderprotocol/e;->a(Ljava/nio/ByteBuffer;Z)V

    .line 49
    return-void
.end method
