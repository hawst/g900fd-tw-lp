.class abstract Lcom/google/android/gms/common/internal/ae;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Ljava/util/Iterator;


# instance fields
.field b:Lcom/google/android/gms/common/internal/af;

.field c:Ljava/lang/Object;


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 536
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 537
    sget-object v0, Lcom/google/android/gms/common/internal/af;->b:Lcom/google/android/gms/common/internal/af;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    .line 539
    return-void
.end method

.method synthetic constructor <init>(B)V
    .locals 0

    .prologue
    .line 536
    invoke-direct {p0}, Lcom/google/android/gms/common/internal/ae;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract a()Ljava/lang/Object;
.end method

.method public final hasNext()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 554
    iget-object v2, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    sget-object v3, Lcom/google/android/gms/common/internal/af;->d:Lcom/google/android/gms/common/internal/af;

    if-ne v2, v3, :cond_0

    .line 555
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0

    .line 558
    :cond_0
    sget-object v2, Lcom/google/android/gms/common/internal/z;->a:[I

    iget-object v3, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    invoke-virtual {v3}, Lcom/google/android/gms/common/internal/af;->ordinal()I

    move-result v3

    aget v2, v2, v3

    packed-switch v2, :pswitch_data_0

    .line 565
    sget-object v2, Lcom/google/android/gms/common/internal/af;->d:Lcom/google/android/gms/common/internal/af;

    iput-object v2, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/ae;->a()Ljava/lang/Object;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/internal/ae;->c:Ljava/lang/Object;

    iget-object v2, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    sget-object v3, Lcom/google/android/gms/common/internal/af;->c:Lcom/google/android/gms/common/internal/af;

    if-eq v2, v3, :cond_1

    sget-object v0, Lcom/google/android/gms/common/internal/af;->a:Lcom/google/android/gms/common/internal/af;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    move v0, v1

    :cond_1
    :goto_0
    :pswitch_0
    return v0

    :pswitch_1
    move v0, v1

    .line 562
    goto :goto_0

    .line 558
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public final next()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 580
    invoke-virtual {p0}, Lcom/google/android/gms/common/internal/ae;->hasNext()Z

    move-result v0

    if-nez v0, :cond_0

    .line 581
    new-instance v0, Ljava/util/NoSuchElementException;

    invoke-direct {v0}, Ljava/util/NoSuchElementException;-><init>()V

    throw v0

    .line 583
    :cond_0
    sget-object v0, Lcom/google/android/gms/common/internal/af;->b:Lcom/google/android/gms/common/internal/af;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/ae;->b:Lcom/google/android/gms/common/internal/af;

    .line 584
    iget-object v0, p0, Lcom/google/android/gms/common/internal/ae;->c:Ljava/lang/Object;

    return-object v0
.end method

.method public remove()V
    .locals 1

    .prologue
    .line 589
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
