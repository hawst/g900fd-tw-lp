.class public abstract Lcom/google/android/gms/common/internal/service/d;
.super Landroid/app/Service;
.source "SourceFile"


# instance fields
.field private a:Lcom/google/android/gms/common/internal/service/c;

.field private final b:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 18
    iput-object p1, p0, Lcom/google/android/gms/common/internal/service/d;->b:Ljava/lang/String;

    .line 19
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1

    .prologue
    .line 38
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    move-result-object v0

    return-object v0
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 1

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 59
    return-void
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 23
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 25
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->b:Ljava/lang/String;

    invoke-static {v0}, Ljava/lang/Class;->forName(Ljava/lang/String;)Ljava/lang/Class;

    move-result-object v0

    .line 26
    invoke-virtual {v0}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/internal/service/c;

    iput-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;
    :try_end_0
    .catch Ljava/lang/ClassNotFoundException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InstantiationException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/lang/IllegalAccessException; {:try_start_0 .. :try_end_0} :catch_2

    .line 32
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p0}, Lcom/google/android/gms/common/internal/service/c;->setProxy(Lcom/google/android/gms/common/internal/service/d;)V

    .line 33
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/service/c;->onCreate()V

    .line 34
    return-void

    .line 27
    :catch_0
    move-exception v0

    .line 28
    :goto_0
    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Failed creating proxied service "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/common/internal/service/d;->b:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 29
    const-string v2, "GmsCoreServiceProxy"

    invoke-static {v2, v1, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 30
    new-instance v2, Ljava/lang/IllegalArgumentException;

    invoke-direct {v2, v1, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 27
    :catch_1
    move-exception v0

    goto :goto_0

    :catch_2
    move-exception v0

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/service/c;->onDestroy()V

    .line 53
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 54
    return-void
.end method

.method public onLowMemory()V
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0}, Lcom/google/android/gms/common/internal/service/c;->onLowMemory()V

    .line 64
    return-void
.end method

.method public onRebind(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 78
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onBind(Landroid/content/Intent;)Landroid/os/IBinder;

    .line 79
    return-void
.end method

.method public onStart(Landroid/content/Intent;I)V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/gms/common/internal/service/c;->onStart(Landroid/content/Intent;I)V

    .line 43
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/gms/common/internal/service/c;->onStartCommand(Landroid/content/Intent;II)I

    move-result v0

    return v0
.end method

.method public onTaskRemoved(Landroid/content/Intent;)V
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onTaskRemoved(Landroid/content/Intent;)V

    .line 84
    return-void
.end method

.method public onTrimMemory(I)V
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onTrimMemory(I)V

    .line 69
    return-void
.end method

.method public onUnbind(Landroid/content/Intent;)Z
    .locals 1

    .prologue
    .line 73
    iget-object v0, p0, Lcom/google/android/gms/common/internal/service/d;->a:Lcom/google/android/gms/common/internal/service/c;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/service/c;->onUnbind(Landroid/content/Intent;)Z

    move-result v0

    return v0
.end method
