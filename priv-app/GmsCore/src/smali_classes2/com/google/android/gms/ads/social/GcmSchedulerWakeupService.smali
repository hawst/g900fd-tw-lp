.class public final Lcom/google/android/gms/ads/social/GcmSchedulerWakeupService;
.super Lcom/google/android/gms/gcm/ae;
.source "SourceFile"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 10
    invoke-direct {p0}, Lcom/google/android/gms/gcm/ae;-><init>()V

    return-void
.end method


# virtual methods
.method public final a(Lcom/google/android/gms/gcm/be;)I
    .locals 11

    .prologue
    const/4 v3, 0x2

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 13
    const-string v0, "DRT dispatcher woke up."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    .line 14
    iget-object v5, p1, Lcom/google/android/gms/gcm/be;->a:Ljava/lang/String;

    .line 15
    const-string v0, "ads.social.doritos"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 16
    new-instance v6, Lcom/google/android/gms/ads/social/a;

    invoke-direct {v6}, Lcom/google/android/gms/ads/social/a;-><init>()V

    .line 17
    const-string v0, "Running DRT refresh task."

    invoke-static {v0}, Lcom/google/android/gms/ads/internal/util/client/b;->a(Ljava/lang/String;)V

    iget-object v0, v6, Lcom/google/android/gms/ads/social/a;->b:Landroid/accounts/AccountManager;

    const-string v4, "com.google"

    invoke-virtual {v0, v4}, Landroid/accounts/AccountManager;->getAccountsByType(Ljava/lang/String;)[Landroid/accounts/Account;

    move-result-object v7

    new-instance v8, Lcom/google/android/gms/ads/social/b;

    iget-object v0, v6, Lcom/google/android/gms/ads/social/a;->a:Landroid/content/Context;

    invoke-direct {v8, v0}, Lcom/google/android/gms/ads/social/b;-><init>(Landroid/content/Context;)V

    array-length v9, v7

    move v4, v2

    move v0, v1

    :goto_0
    if-ge v4, v9, :cond_1

    aget-object v10, v7, v4

    if-eqz v0, :cond_0

    iget-object v0, v10, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v6, v0, v8}, Lcom/google/android/gms/ads/social/a;->a(Ljava/lang/String;Lcom/google/android/gms/ads/social/b;)Z

    move-result v0

    if-eqz v0, :cond_0

    move v0, v1

    :goto_1
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    :cond_0
    move v0, v2

    goto :goto_1

    :cond_1
    if-eqz v0, :cond_2

    .line 20
    :goto_2
    return v2

    .line 17
    :cond_2
    const-string v0, "ads.social.doritos-oneoff"

    invoke-virtual {v5, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    move v2, v3

    goto :goto_2

    :cond_3
    move v2, v1

    goto :goto_2

    :cond_4
    move v2, v3

    .line 20
    goto :goto_2
.end method
