.class final Lcom/google/android/gms/common/api/z;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/api/v;


# instance fields
.field final a:Ljava/util/concurrent/locks/Lock;

.field final b:Landroid/content/Context;

.field final c:Ljava/util/Queue;

.field d:Lcom/google/android/gms/common/c;

.field e:I

.field volatile f:Z

.field g:J

.field h:J

.field final i:Landroid/os/Handler;

.field j:Landroid/content/BroadcastReceiver;

.field final k:Landroid/os/Bundle;

.field l:Z

.field final m:Ljava/util/Set;

.field private final n:Ljava/util/concurrent/locks/Condition;

.field private final o:Lcom/google/android/gms/common/internal/ar;

.field private final p:I

.field private final q:Landroid/os/Looper;

.field private volatile r:I

.field private s:Z

.field private t:I

.field private final u:Ljava/util/Map;

.field private final v:Ljava/util/List;

.field private final w:Ljava/util/Set;

.field private final x:Lcom/google/android/gms/common/api/af;

.field private final y:Lcom/google/android/gms/common/api/x;

.field private final z:Lcom/google/android/gms/common/internal/as;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/util/Map;Ljava/util/Set;Ljava/util/Set;I)V
    .locals 12

    .prologue
    .line 404
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 178
    new-instance v2, Ljava/util/concurrent/locks/ReentrantLock;

    invoke-direct {v2}, Ljava/util/concurrent/locks/ReentrantLock;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    .line 181
    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v2}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->n:Ljava/util/concurrent/locks/Condition;

    .line 196
    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    .line 206
    const/4 v2, 0x4

    iput v2, p0, Lcom/google/android/gms/common/api/z;->r:I

    .line 212
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/z;->s:Z

    .line 218
    const-wide/32 v2, 0x1d4c0

    iput-wide v2, p0, Lcom/google/android/gms/common/api/z;->g:J

    .line 221
    const-wide/16 v2, 0x1388

    iput-wide v2, p0, Lcom/google/android/gms/common/api/z;->h:J

    .line 232
    new-instance v2, Landroid/os/Bundle;

    invoke-direct {v2}, Landroid/os/Bundle;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->k:Landroid/os/Bundle;

    .line 235
    new-instance v2, Ljava/util/HashMap;

    invoke-direct {v2}, Ljava/util/HashMap;-><init>()V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    .line 246
    new-instance v2, Ljava/util/WeakHashMap;

    invoke-direct {v2}, Ljava/util/WeakHashMap;-><init>()V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->w:Ljava/util/Set;

    .line 253
    new-instance v2, Ljava/util/concurrent/ConcurrentHashMap;

    const/16 v3, 0x10

    const/high16 v4, 0x3f400000    # 0.75f

    const/4 v5, 0x2

    invoke-direct {v2, v3, v4, v5}, Ljava/util/concurrent/ConcurrentHashMap;-><init>(IFI)V

    invoke-static {v2}, Ljava/util/Collections;->newSetFromMap(Ljava/util/Map;)Ljava/util/Set;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->m:Ljava/util/Set;

    .line 259
    new-instance v2, Lcom/google/android/gms/common/api/aa;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/aa;-><init>(Lcom/google/android/gms/common/api/z;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->x:Lcom/google/android/gms/common/api/af;

    .line 267
    new-instance v2, Lcom/google/android/gms/common/api/ab;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/ab;-><init>(Lcom/google/android/gms/common/api/z;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->y:Lcom/google/android/gms/common/api/x;

    .line 330
    new-instance v2, Lcom/google/android/gms/common/api/ac;

    invoke-direct {v2, p0}, Lcom/google/android/gms/common/api/ac;-><init>(Lcom/google/android/gms/common/api/z;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->z:Lcom/google/android/gms/common/internal/as;

    .line 405
    iput-object p1, p0, Lcom/google/android/gms/common/api/z;->b:Landroid/content/Context;

    .line 406
    new-instance v2, Lcom/google/android/gms/common/internal/ar;

    iget-object v3, p0, Lcom/google/android/gms/common/api/z;->z:Lcom/google/android/gms/common/internal/as;

    invoke-direct {v2, p2, v3}, Lcom/google/android/gms/common/internal/ar;-><init>(Landroid/os/Looper;Lcom/google/android/gms/common/internal/as;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    .line 407
    iput-object p2, p0, Lcom/google/android/gms/common/api/z;->q:Landroid/os/Looper;

    .line 408
    new-instance v2, Lcom/google/android/gms/common/api/ag;

    invoke-direct {v2, p0, p2}, Lcom/google/android/gms/common/api/ag;-><init>(Lcom/google/android/gms/common/api/z;Landroid/os/Looper;)V

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    .line 409
    move/from16 v0, p7

    iput v0, p0, Lcom/google/android/gms/common/api/z;->p:I

    .line 411
    invoke-interface/range {p5 .. p5}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/x;

    .line 412
    iget-object v4, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/api/x;)V

    goto :goto_0

    .line 415
    :cond_0
    invoke-interface/range {p6 .. p6}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_1
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/gms/common/api/y;

    .line 416
    iget-object v4, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v4, v2}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/et;)V

    goto :goto_1

    .line 419
    :cond_1
    invoke-interface/range {p4 .. p4}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v9

    :goto_2
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_2

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    move-object v3, v2

    check-cast v3, Lcom/google/android/gms/common/api/c;

    .line 421
    iget-object v2, v3, Lcom/google/android/gms/common/api/c;->a:Lcom/google/android/gms/common/api/i;

    .line 422
    move-object/from16 v0, p4

    invoke-interface {v0, v3}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    .line 425
    iget-object v10, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    iget-object v11, v3, Lcom/google/android/gms/common/api/c;->b:Lcom/google/android/gms/common/api/j;

    iget-object v7, p0, Lcom/google/android/gms/common/api/z;->y:Lcom/google/android/gms/common/api/x;

    new-instance v8, Lcom/google/android/gms/common/api/ad;

    invoke-direct {v8, p0, v2}, Lcom/google/android/gms/common/api/ad;-><init>(Lcom/google/android/gms/common/api/z;Lcom/google/android/gms/common/api/i;)V

    move-object v3, p1

    move-object v4, p2

    move-object v5, p3

    invoke-interface/range {v2 .. v8}, Lcom/google/android/gms/common/api/i;->a(Landroid/content/Context;Landroid/os/Looper;Lcom/google/android/gms/common/internal/ClientSettings;Ljava/lang/Object;Lcom/google/android/gms/common/api/x;Lcom/google/android/gms/common/api/y;)Lcom/google/android/gms/common/api/h;

    move-result-object v2

    invoke-interface {v10, v11, v2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 453
    :cond_2
    iget-object v2, p3, Lcom/google/android/gms/common/internal/ClientSettings;->a:Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;

    invoke-virtual {v2}, Lcom/google/android/gms/common/internal/ClientSettings$ParcelableClientSettings;->f()Ljava/util/List;

    move-result-object v2

    invoke-static {v2}, Ljava/util/Collections;->unmodifiableList(Ljava/util/List;)Ljava/util/List;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/gms/common/api/z;->v:Ljava/util/List;

    .line 454
    return-void
.end method

.method private a(Lcom/google/android/gms/common/api/ah;)V
    .locals 2

    .prologue
    .line 552
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 554
    :try_start_0
    invoke-interface {p1}, Lcom/google/android/gms/common/api/ah;->f()Lcom/google/android/gms/common/api/j;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "This task can not be executed or enqueued (it\'s probably a Batch or malformed)"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->b(ZLjava/lang/Object;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->m:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 560
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->x:Lcom/google/android/gms/common/api/af;

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/ah;->a(Lcom/google/android/gms/common/api/af;)V

    .line 564
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v0, :cond_1

    .line 565
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/ah;->b(Lcom/google/android/gms/common/api/Status;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 573
    :goto_1
    return-void

    .line 554
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 569
    :cond_1
    :try_start_1
    invoke-interface {p1}, Lcom/google/android/gms/common/api/ah;->f()Lcom/google/android/gms/common/api/j;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;

    move-result-object v0

    .line 570
    invoke-interface {p1, v0}, Lcom/google/android/gms/common/api/ah;->b(Lcom/google/android/gms/common/api/h;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 572
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/z;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 53
    iget v0, p0, Lcom/google/android/gms/common/api/z;->t:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/gms/common/api/z;->t:I

    iget v0, p0, Lcom/google/android/gms/common/api/z;->t:I

    if-nez v0, :cond_2

    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_3

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/z;->s:Z

    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/z;->a(I)V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    invoke-virtual {v1}, Lcom/google/android/gms/common/c;->c()I

    move-result v1

    invoke-static {v0, v1}, Lcom/google/android/gms/common/ew;->c(Landroid/content/Context;I)Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->h()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/c;)V

    :cond_1
    iput-boolean v2, p0, Lcom/google/android/gms/common/api/z;->l:Z

    :cond_2
    :goto_0
    return-void

    :cond_3
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->h()V

    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->n:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    invoke-direct {p0}, Lcom/google/android/gms/common/api/z;->i()V

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->s:Z

    if-eqz v0, :cond_4

    iput-boolean v2, p0, Lcom/google/android/gms/common/api/z;->s:Z

    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/z;->a(I)V

    goto :goto_0

    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->k:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_5

    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v1, v0}, Lcom/google/android/gms/common/internal/ar;->a(Landroid/os/Bundle;)V

    goto :goto_0

    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->k:Landroid/os/Bundle;

    goto :goto_1
.end method

.method static synthetic a(Lcom/google/android/gms/common/api/z;I)V
    .locals 0

    .prologue
    .line 53
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/z;->a(I)V

    return-void
.end method

.method private i()V
    .locals 3

    .prologue
    .line 578
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 580
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    const-string v1, "GoogleApiClient is not connected yet."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 584
    :goto_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-nez v0, :cond_2

    .line 586
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ah;

    invoke-direct {p0, v0}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/ah;)V
    :try_end_1
    .catch Landroid/os/DeadObjectException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 587
    :catch_0
    move-exception v0

    .line 588
    :try_start_2
    const-string v1, "GoogleApiClientImpl"

    const-string v2, "Service died while flushing queue"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 592
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 580
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 592
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 593
    return-void
.end method


# virtual methods
.method public final a()Landroid/os/Looper;
    .locals 1

    .prologue
    .line 961
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->q:Landroid/os/Looper;

    return-object v0
.end method

.method public final a(Ljava/lang/Object;)Lcom/google/android/gms/common/api/aj;
    .locals 2

    .prologue
    .line 598
    const-string v0, "Listener must not be null"

    invoke-static {p1, v0}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 599
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 601
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/aj;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->q:Landroid/os/Looper;

    invoke-direct {v0, v1, p1}, Lcom/google/android/gms/common/api/aj;-><init>(Landroid/os/Looper;Ljava/lang/Object;)V

    .line 602
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->w:Ljava/util/Set;

    invoke-interface {v1, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 605
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object v0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(Lcom/google/android/gms/common/api/j;)Lcom/google/android/gms/common/api/h;
    .locals 2

    .prologue
    .line 612
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    invoke-interface {v0, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/h;

    .line 613
    const-string v1, "Appropriate Api was not requested."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 614
    return-object v0
.end method

.method public final a(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;
    .locals 2

    .prologue
    .line 518
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 520
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 522
    invoke-virtual {p0, p1}, Lcom/google/android/gms/common/api/z;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 529
    :goto_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    return-object p1

    .line 525
    :cond_0
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0, p1}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 529
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final a(JLjava/util/concurrent/TimeUnit;)Lcom/google/android/gms/common/c;
    .locals 5

    .prologue
    .line 683
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_1

    const/4 v0, 0x1

    :goto_0
    const-string v1, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 685
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 687
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->b()V

    .line 690
    invoke-virtual {p3, p1, p2}, Ljava/util/concurrent/TimeUnit;->toNanos(J)J

    move-result-wide v0

    .line 691
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    if-eqz v2, :cond_2

    .line 693
    :try_start_1
    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->n:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v2, v0, v1}, Ljava/util/concurrent/locks/Condition;->awaitNanos(J)J

    move-result-wide v0

    .line 694
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gtz v2, :cond_0

    .line 695
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0xe

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_1
    return-object v0

    .line 683
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 698
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 699
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 703
    :cond_2
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 704
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 705
    :cond_3
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_4

    .line 706
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    .line 708
    :cond_4
    :try_start_5
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 711
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_1

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method final a(I)V
    .locals 5

    .prologue
    const/4 v1, 0x3

    const/4 v3, 0x1

    const/4 v4, -0x1

    .line 722
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 725
    :try_start_0
    iget v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    if-eq v0, v1, :cond_c

    .line 727
    if-ne p1, v4, :cond_6

    .line 728
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->g()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 729
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .line 730
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 731
    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ah;

    .line 732
    invoke-interface {v0}, Lcom/google/android/gms/common/api/ah;->g()I

    move-result v2

    if-eq v2, v3, :cond_0

    .line 733
    invoke-interface {v0}, Lcom/google/android/gms/common/api/ah;->b()V

    .line 734
    invoke-interface {v1}, Ljava/util/Iterator;->remove()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 801
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    .line 739
    :cond_1
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ah;

    .line 740
    invoke-interface {v0}, Lcom/google/android/gms/common/api/ah;->b()V

    goto :goto_1

    .line 742
    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->clear()V

    .line 747
    :cond_3
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_2
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/ah;

    .line 748
    const/4 v2, 0x0

    invoke-interface {v0, v2}, Lcom/google/android/gms/common/api/ah;->a(Lcom/google/android/gms/common/api/af;)V

    .line 749
    invoke-interface {v0}, Lcom/google/android/gms/common/api/ah;->b()V

    goto :goto_2

    .line 751
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->m:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 754
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_3
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/aj;

    .line 755
    const/4 v2, 0x0

    iput-object v2, v0, Lcom/google/android/gms/common/api/aj;->a:Ljava/lang/Object;

    goto :goto_3

    .line 757
    :cond_5
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->w:Ljava/util/Set;

    invoke-interface {v0}, Ljava/util/Set;->clear()V

    .line 759
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    if-nez v0, :cond_6

    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v0}, Ljava/util/Queue;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_6

    .line 761
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->s:Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 801
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 802
    :goto_4
    return-void

    .line 766
    :cond_6
    :try_start_2
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->g()Z

    move-result v0

    .line 767
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v1

    .line 768
    const/4 v2, 0x3

    iput v2, p0, Lcom/google/android/gms/common/api/z;->r:I

    .line 770
    if-eqz v0, :cond_8

    .line 773
    if-ne p1, v4, :cond_7

    .line 774
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    .line 778
    :cond_7
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->n:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->signalAll()V

    .line 782
    :cond_8
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->l:Z

    .line 783
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_9
    :goto_5
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_a

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/h;

    .line 784
    invoke-interface {v0}, Lcom/google/android/gms/common/api/h;->c_()Z

    move-result v3

    if-eqz v3, :cond_9

    .line 785
    invoke-interface {v0}, Lcom/google/android/gms/common/api/h;->b()V

    goto :goto_5

    .line 788
    :cond_a
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->l:Z

    .line 791
    const/4 v0, 0x4

    iput v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    .line 792
    if-eqz v1, :cond_c

    .line 794
    if-eq p1, v4, :cond_b

    .line 795
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(I)V

    .line 797
    :cond_b
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->l:Z
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 801
    :cond_c
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_4
.end method

.method public final a(Landroid/support/v4/app/q;)V
    .locals 2

    .prologue
    .line 872
    iget v0, p0, Lcom/google/android/gms/common/api/z;->p:I

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "Called stopAutoManage but automatic lifecycle management is not enabled."

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 874
    invoke-static {p1}, Lcom/google/android/gms/common/api/at;->a(Landroid/support/v4/app/q;)Lcom/google/android/gms/common/api/at;

    move-result-object v0

    iget v1, p0, Lcom/google/android/gms/common/api/z;->p:I

    invoke-virtual {v0, v1}, Lcom/google/android/gms/common/api/at;->b(I)V

    .line 875
    return-void

    .line 872
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final a(Lcom/google/android/gms/common/api/x;)V
    .locals 1

    .prologue
    .line 931
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/api/x;)V

    .line 932
    return-void
.end method

.method public final a(Lcom/google/android/gms/common/api/y;)V
    .locals 1

    .prologue
    .line 946
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->a(Lcom/google/android/gms/common/et;)V

    .line 947
    return-void
.end method

.method public final a(Ljava/lang/String;Ljava/io/PrintWriter;)V
    .locals 3

    .prologue
    .line 966
    invoke-virtual {p2, p1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v1, "GoogleApiClient:"

    invoke-virtual {v0, v1}, Ljava/io/PrintWriter;->println(Ljava/lang/String;)V

    .line 968
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 969
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "mConnectionState="

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    .line 970
    iget v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    packed-switch v0, :pswitch_data_0

    .line 984
    const-string v0, "UNKNOWN"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    .line 986
    :goto_0
    const-string v0, " mResuming="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/z;->f:Z

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(Z)V

    .line 987
    const-string v0, " mWaitingToDisconnect="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-boolean v2, p0, Lcom/google/android/gms/common/api/z;->s:Z

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(Z)V

    .line 989
    invoke-virtual {p2, v1}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    const-string v2, "mWorkQueue.size()="

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->c:Ljava/util/Queue;

    invoke-interface {v2}, Ljava/util/Queue;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->print(I)V

    .line 990
    const-string v0, " mUnconsumedRunners.size()="

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->append(Ljava/lang/CharSequence;)Ljava/io/PrintWriter;

    move-result-object v0

    iget-object v2, p0, Lcom/google/android/gms/common/api/z;->m:Ljava/util/Set;

    invoke-interface {v2}, Ljava/util/Set;->size()I

    move-result v2

    invoke-virtual {v0, v2}, Ljava/io/PrintWriter;->println(I)V

    .line 992
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/h;

    .line 993
    invoke-interface {v0, v1, p2}, Lcom/google/android/gms/common/api/h;->a(Ljava/lang/String;Ljava/io/PrintWriter;)V

    goto :goto_1

    .line 972
    :pswitch_0
    const-string v0, "CONNECTING"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 975
    :pswitch_1
    const-string v0, "CONNECTED"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 978
    :pswitch_2
    const-string v0, "DISCONNECTING"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 981
    :pswitch_3
    const-string v0, "DISCONNECTED"

    invoke-virtual {p2, v0}, Ljava/io/PrintWriter;->print(Ljava/lang/String;)V

    goto :goto_0

    .line 995
    :cond_0
    return-void

    .line 970
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public final a(Lcom/google/android/gms/common/api/c;)Z
    .locals 2

    .prologue
    .line 619
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    iget-object v1, p1, Lcom/google/android/gms/common/api/c;->b:Lcom/google/android/gms/common/api/j;

    invoke-interface {v0, v1}, Ljava/util/Map;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public final b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;
    .locals 3

    .prologue
    const/4 v1, 0x1

    .line 536
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z

    if-eqz v0, :cond_1

    :cond_0
    move v0, v1

    :goto_0
    const-string v2, "GoogleApiClient is not connected yet."

    invoke-static {v0, v2}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 538
    invoke-direct {p0}, Lcom/google/android/gms/common/api/z;->i()V

    .line 540
    :try_start_0
    invoke-direct {p0, p1}, Lcom/google/android/gms/common/api/z;->a(Lcom/google/android/gms/common/api/ah;)V
    :try_end_0
    .catch Landroid/os/DeadObjectException; {:try_start_0 .. :try_end_0} :catch_0

    .line 544
    :goto_1
    return-object p1

    .line 536
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 542
    :catch_0
    move-exception v0

    invoke-virtual {p0, v1}, Lcom/google/android/gms/common/api/z;->a(I)V

    goto :goto_1
.end method

.method public final b()V
    .locals 2

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 631
    const/4 v0, 0x0

    :try_start_0
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->s:Z

    .line 632
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 647
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 648
    :goto_0
    return-void

    .line 637
    :cond_1
    const/4 v0, 0x1

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->l:Z

    .line 638
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    .line 639
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    .line 640
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->k:Landroid/os/Bundle;

    invoke-virtual {v0}, Landroid/os/Bundle;->clear()V

    .line 641
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->size()I

    move-result v0

    iput v0, p0, Lcom/google/android/gms/common/api/z;->t:I

    .line 643
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->u:Ljava/util/Map;

    invoke-interface {v0}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/api/h;

    .line 644
    invoke-interface {v0}, Lcom/google/android/gms/common/api/h;->a()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 647
    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0

    :cond_2
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0
.end method

.method public final b(Lcom/google/android/gms/common/api/x;)V
    .locals 1

    .prologue
    .line 941
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->c(Lcom/google/android/gms/common/api/x;)V

    .line 942
    return-void
.end method

.method public final b(Lcom/google/android/gms/common/api/y;)V
    .locals 1

    .prologue
    .line 956
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->o:Lcom/google/android/gms/common/internal/ar;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/common/internal/ar;->b(Lcom/google/android/gms/common/et;)V

    .line 957
    return-void
.end method

.method public final c()Lcom/google/android/gms/common/c;
    .locals 3

    .prologue
    .line 653
    invoke-static {}, Landroid/os/Looper;->myLooper()Landroid/os/Looper;

    move-result-object v0

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    if-eq v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "blockingConnect must not be called on the UI thread"

    invoke-static {v0, v1}, Lcom/google/android/gms/common/internal/bx;->a(ZLjava/lang/Object;)V

    .line 655
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 657
    :try_start_0
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->b()V

    .line 660
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->g()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 662
    :try_start_1
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->n:Ljava/util/concurrent/locks/Condition;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Condition;->await()V
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1

    .line 664
    :catch_0
    move-exception v0

    :try_start_2
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Thread;->interrupt()V

    .line 665
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0xf

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    :goto_2
    return-object v0

    .line 653
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 669
    :cond_1
    :try_start_3
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->f()Z

    move-result v0

    if-eqz v0, :cond_2

    .line 670
    sget-object v0, Lcom/google/android/gms/common/c;->a:Lcom/google/android/gms/common/c;
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    .line 671
    :cond_2
    :try_start_4
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;

    if-eqz v0, :cond_3

    .line 672
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->d:Lcom/google/android/gms/common/c;
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    .line 674
    :cond_3
    :try_start_5
    new-instance v0, Lcom/google/android/gms/common/c;

    const/16 v1, 0xd

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/common/c;-><init>(ILandroid/app/PendingIntent;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 677
    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_2

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method

.method public final d()V
    .locals 1

    .prologue
    .line 717
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->h()V

    .line 718
    const/4 v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/gms/common/api/z;->a(I)V

    .line 719
    return-void
.end method

.method public final e()V
    .locals 0

    .prologue
    .line 807
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->d()V

    .line 808
    invoke-virtual {p0}, Lcom/google/android/gms/common/api/z;->b()V

    .line 809
    return-void
.end method

.method public final f()Z
    .locals 2

    .prologue
    .line 879
    iget v0, p0, Lcom/google/android/gms/common/api/z;->r:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final g()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 884
    iget v1, p0, Lcom/google/android/gms/common/api/z;->r:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final h()V
    .locals 2

    .prologue
    .line 915
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 917
    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_0

    .line 925
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 926
    :goto_0
    return-void

    .line 920
    :cond_0
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/gms/common/api/z;->f:Z

    .line 921
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 922
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->i:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 923
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->b:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->j:Landroid/content/BroadcastReceiver;

    invoke-virtual {v0, v1}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 925
    iget-object v0, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v0}, Ljava/util/concurrent/locks/Lock;->unlock()V

    goto :goto_0

    :catchall_0
    move-exception v0

    iget-object v1, p0, Lcom/google/android/gms/common/api/z;->a:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v1}, Ljava/util/concurrent/locks/Lock;->unlock()V

    throw v0
.end method
