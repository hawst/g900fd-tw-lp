.class final Lcom/google/android/gms/people/identity/internal/w;
.super Lcom/google/android/gms/people/z;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/people/identity/internal/c;
.implements Lcom/google/android/gms/people/internal/s;


# instance fields
.field final a:Lcom/google/android/gms/people/identity/g;

.field final b:[Ljava/lang/Object;

.field final d:Ljava/util/Set;

.field e:Landroid/content/Context;

.field f:Ljava/util/ArrayList;

.field g:Lcom/google/android/gms/common/data/DataHolder;

.field h:Lcom/google/android/gms/common/data/DataHolder;

.field i:Lcom/google/android/gms/common/data/DataHolder;

.field j:Lcom/google/android/gms/common/data/DataHolder;

.field k:Lcom/google/android/gms/common/data/DataHolder;

.field l:Lcom/google/android/gms/common/data/DataHolder;

.field m:Lcom/google/android/gms/common/data/DataHolder;

.field n:Lcom/google/android/gms/common/data/DataHolder;

.field o:Lcom/google/android/gms/common/data/DataHolder;

.field p:Ljava/util/List;

.field final synthetic q:Lcom/google/android/gms/people/identity/internal/v;

.field private final r:Lcom/google/android/gms/people/identity/e;

.field private final s:[Ljava/lang/String;

.field private final t:Lcom/google/android/gms/common/api/v;

.field private u:Lcom/google/android/gms/common/api/m;

.field private v:Lcom/google/android/gms/common/api/Status;

.field private w:Z

.field private x:Z

.field private y:Lcom/google/android/gms/common/api/Status;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/people/identity/internal/v;Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/people/identity/e;Lcom/google/android/gms/people/identity/g;[Ljava/lang/String;)V
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 117
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/w;->q:Lcom/google/android/gms/people/identity/internal/v;

    .line 118
    invoke-direct {p0, p2}, Lcom/google/android/gms/people/z;-><init>(Lcom/google/android/gms/common/api/v;)V

    .line 93
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    iput-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->d:Ljava/util/Set;

    .line 100
    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/w;->w:Z

    .line 112
    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/w;->x:Z

    .line 119
    iput-object p2, p0, Lcom/google/android/gms/people/identity/internal/w;->t:Lcom/google/android/gms/common/api/v;

    .line 120
    iput-object p3, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    .line 121
    iput-object p4, p0, Lcom/google/android/gms/people/identity/internal/w;->a:Lcom/google/android/gms/people/identity/g;

    .line 122
    iput-object p0, p0, Lcom/google/android/gms/people/identity/internal/w;->u:Lcom/google/android/gms/common/api/m;

    .line 123
    iput-object p5, p0, Lcom/google/android/gms/people/identity/internal/w;->s:[Ljava/lang/String;

    .line 125
    array-length v1, p5

    new-array v1, v1, [Ljava/lang/Object;

    iput-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    .line 126
    :goto_0
    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 127
    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    new-instance v2, Ljava/lang/Object;

    invoke-direct {v2}, Ljava/lang/Object;-><init>()V

    aput-object v2, v1, v0

    .line 126
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 129
    :cond_0
    return-void
.end method

.method private a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 7

    .prologue
    .line 445
    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/w;->e:Landroid/content/Context;

    iget-object v3, p0, Lcom/google/android/gms/people/identity/internal/w;->s:[Ljava/lang/String;

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    if-eqz p1, :cond_1

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->h()I

    move-result v0

    if-ge v1, v0, :cond_1

    invoke-virtual {p1, v1}, Lcom/google/android/gms/common/data/DataHolder;->a(I)I

    move-result v0

    const-string v5, "gaia_id"

    invoke-virtual {p1, v5, v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v5

    const-string v6, "contact_id"

    invoke-virtual {p1, v6, v1, v0}, Lcom/google/android/gms/common/data/DataHolder;->c(Ljava/lang/String;II)Ljava/lang/String;

    move-result-object v6

    invoke-interface {v4, v5}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_0

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    invoke-interface {v4, v5, v0}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    :cond_0
    invoke-interface {v0, v6}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->q:Lcom/google/android/gms/people/identity/internal/v;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/c;->a:Ljava/lang/String;

    invoke-static {p0, v2, v4, v0, v3}, Lcom/google/android/gms/people/identity/internal/v;->a(Lcom/google/android/gms/people/identity/internal/c;Landroid/content/Context;Ljava/util/Map;Ljava/lang/String;[Ljava/lang/String;)V

    .line 446
    return-void
.end method

.method private h()V
    .locals 7

    .prologue
    const/16 v1, 0x64

    .line 183
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->u:Lcom/google/android/gms/common/api/m;

    if-nez v0, :cond_0

    .line 381
    :goto_0
    return-void

    .line 194
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v0, v0, Lcom/google/android/gms/people/identity/e;->d:Z

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->y:Lcom/google/android/gms/common/api/Status;

    if-nez v0, :cond_4

    .line 196
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    .line 210
    :goto_1
    invoke-virtual {v2}, Lcom/google/android/gms/common/api/Status;->h()I

    move-result v0

    if-eq v0, v1, :cond_8

    const/4 v4, 0x1

    .line 213
    :goto_2
    iget-object v6, p0, Lcom/google/android/gms/people/identity/internal/w;->u:Lcom/google/android/gms/common/api/m;

    .line 216
    if-nez v4, :cond_9

    .line 221
    new-instance v5, Lcom/google/android/gms/people/identity/internal/y;

    sget-object v0, Lcom/google/android/gms/people/x;->a:Lcom/google/android/gms/common/api/j;

    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->t:Lcom/google/android/gms/common/api/v;

    invoke-direct {v5, p0, v0, v1}, Lcom/google/android/gms/people/identity/internal/y;-><init>(Lcom/google/android/gms/people/identity/internal/w;Lcom/google/android/gms/common/api/j;Lcom/google/android/gms/common/api/v;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->t:Lcom/google/android/gms/common/api/v;

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->t:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->f()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 263
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->t:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0, v5}, Lcom/google/android/gms/common/api/v;->b(Lcom/google/android/gms/common/api/l;)Lcom/google/android/gms/common/api/l;

    .line 268
    :cond_1
    :goto_3
    iput-object v5, p0, Lcom/google/android/gms/people/identity/internal/w;->u:Lcom/google/android/gms/common/api/m;

    .line 271
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 272
    const-string v1, "PeopleClient"

    new-instance v0, Ljava/lang/StringBuilder;

    const-string v3, "Status: "

    invoke-direct {v0, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    if-eqz v4, :cond_a

    const-string v0, " (Final Result)"

    :goto_4
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 276
    :cond_2
    const/4 v0, 0x2

    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 277
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "old callback: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "new callback: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->b(Ljava/lang/String;Ljava/lang/String;)V

    .line 282
    :cond_3
    new-instance v3, Lcom/google/android/gms/people/identity/internal/aa;

    invoke-direct {v3, p0, v5}, Lcom/google/android/gms/people/identity/internal/aa;-><init>(Lcom/google/android/gms/people/identity/internal/w;Lcom/google/android/gms/common/api/l;)V

    .line 355
    new-instance v0, Lcom/google/android/gms/people/identity/internal/ab;

    move-object v1, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/gms/people/identity/internal/ab;-><init>(Lcom/google/android/gms/people/identity/internal/w;Lcom/google/android/gms/common/api/Status;Lcom/google/android/gms/common/data/d;ZLcom/google/android/gms/common/api/l;)V

    invoke-interface {v6, v0}, Lcom/google/android/gms/common/api/m;->a(Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 197
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v0, v0, Lcom/google/android/gms/people/identity/e;->c:Z

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v0, v0, Lcom/google/android/gms/people/identity/e;->b:Z

    if-eqz v0, :cond_7

    .line 198
    :cond_5
    iget-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/w;->w:Z

    if-nez v0, :cond_6

    .line 200
    new-instance v2, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v2, v1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    goto/16 :goto_1

    .line 203
    :cond_6
    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/w;->v:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_1

    .line 207
    :cond_7
    sget-object v2, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    goto/16 :goto_1

    .line 210
    :cond_8
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 266
    :cond_9
    const/4 v5, 0x0

    goto/16 :goto_3

    .line 272
    :cond_a
    const-string v0, " (Staged Result)"

    goto :goto_4
.end method


# virtual methods
.method protected final synthetic a(Lcom/google/android/gms/common/api/Status;)Lcom/google/android/gms/common/api/ap;
    .locals 1

    .prologue
    .line 85
    new-instance v0, Lcom/google/android/gms/people/identity/internal/x;

    invoke-direct {v0, p0, p1}, Lcom/google/android/gms/people/identity/internal/x;-><init>(Lcom/google/android/gms/people/identity/internal/w;Lcom/google/android/gms/common/api/Status;)V

    return-object v0
.end method

.method public final declared-synchronized a(ILandroid/os/Bundle;Landroid/os/Bundle;)V
    .locals 5

    .prologue
    const/4 v1, 0x1

    .line 385
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 386
    const-string v0, "PeopleClient"

    new-instance v2, Ljava/lang/StringBuilder;

    const-string v3, "GetById callback: status="

    invoke-direct {v2, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\nresolution="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "\ncontent="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 391
    :cond_0
    :try_start_1
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v0

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 393
    new-instance v0, Lcom/google/android/gms/common/api/Status;

    invoke-direct {v0, p1}, Lcom/google/android/gms/common/api/Status;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->v:Lcom/google/android/gms/common/api/Status;

    .line 394
    const-string v0, "get.server_blob"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelableArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->f:Ljava/util/ArrayList;

    .line 395
    const-string v0, "response_complete"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/w;->w:Z

    .line 397
    const-string v0, "gaia_map"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    .line 398
    if-eqz v0, :cond_2

    .line 399
    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v2, v2, Lcom/google/android/gms/people/identity/e;->d:Z

    if-eqz v2, :cond_1

    iget-boolean v2, p0, Lcom/google/android/gms/people/identity/internal/w;->x:Z

    if-nez v2, :cond_1

    .line 400
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/gms/people/identity/internal/w;->x:Z

    .line 401
    invoke-direct {p0, v0}, Lcom/google/android/gms/people/identity/internal/w;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    .line 403
    :cond_1
    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/w;->d:Ljava/util/Set;

    invoke-interface {v2, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    .line 406
    :cond_2
    const-string v0, "db"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getBundle(Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v2

    .line 407
    if-eqz v2, :cond_4

    .line 408
    invoke-virtual {v2}, Landroid/os/Bundle;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 409
    iget-object v4, p0, Lcom/google/android/gms/people/identity/internal/w;->d:Ljava/util/Set;

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    invoke-interface {v4, v0}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 438
    :catch_0
    move-exception v0

    .line 439
    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "GetById callback error:"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 440
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 385
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 412
    :cond_3
    :try_start_3
    const-string v0, "people"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->g:Lcom/google/android/gms/common/data/DataHolder;

    .line 413
    const-string v0, "people_address"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->h:Lcom/google/android/gms/common/data/DataHolder;

    .line 415
    const-string v0, "people_email"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->i:Lcom/google/android/gms/common/data/DataHolder;

    .line 417
    const-string v0, "people_phone"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->j:Lcom/google/android/gms/common/data/DataHolder;

    .line 420
    const-string v0, "owner"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->k:Lcom/google/android/gms/common/data/DataHolder;

    .line 421
    const-string v0, "owner_address"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->l:Lcom/google/android/gms/common/data/DataHolder;

    .line 423
    const-string v0, "owner_email"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->m:Lcom/google/android/gms/common/data/DataHolder;

    .line 425
    const-string v0, "owner_phone"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->n:Lcom/google/android/gms/common/data/DataHolder;

    .line 428
    const-string v0, "circles"

    invoke-virtual {v2, v0}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/data/DataHolder;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->o:Lcom/google/android/gms/common/data/DataHolder;

    .line 433
    :cond_4
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->f:Ljava/util/ArrayList;

    if-eqz v0, :cond_5

    .line 434
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    array-length v0, v0

    iget-object v2, p0, Lcom/google/android/gms/people/identity/internal/w;->f:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ne v0, v2, :cond_6

    move v0, v1

    :goto_1
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 437
    :cond_5
    invoke-direct {p0}, Lcom/google/android/gms/people/identity/internal/w;->h()V
    :try_end_3
    .catch Ljava/lang/Throwable; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 441
    monitor-exit p0

    return-void

    .line 434
    :cond_6
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public final declared-synchronized a(Lcom/google/android/gms/common/api/Status;Ljava/util/List;)V
    .locals 3

    .prologue
    .line 450
    monitor-enter p0

    const/4 v0, 0x3

    :try_start_0
    invoke-static {v0}, Lcom/google/android/gms/people/internal/as;->a(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 451
    const-string v0, "PeopleClient"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "GetById CP2 callback: status="

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " result="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 456
    :cond_0
    :try_start_1
    iput-object p1, p0, Lcom/google/android/gms/people/identity/internal/w;->y:Lcom/google/android/gms/common/api/Status;

    .line 457
    iput-object p2, p0, Lcom/google/android/gms/people/identity/internal/w;->p:Ljava/util/List;

    .line 460
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->p:Ljava/util/List;

    if-eqz v0, :cond_1

    .line 461
    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->b:[Ljava/lang/Object;

    array-length v0, v0

    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->p:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ne v0, v1, :cond_2

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/gms/common/internal/bx;->a(Z)V

    .line 464
    :cond_1
    invoke-direct {p0}, Lcom/google/android/gms/people/identity/internal/w;->h()V
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 468
    monitor-exit p0

    return-void

    .line 461
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 465
    :catch_0
    move-exception v0

    .line 466
    :try_start_2
    const-string v1, "PeopleClient"

    const-string v2, "GetById CP2 callback error:"

    invoke-static {v1, v2, v0}, Lcom/google/android/gms/people/internal/as;->a(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 467
    throw v0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 450
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected final synthetic a(Lcom/google/android/gms/common/api/h;)V
    .locals 2

    .prologue
    .line 85
    check-cast p1, Lcom/google/android/gms/people/internal/p;

    invoke-virtual {p1}, Lcom/google/android/gms/people/internal/p;->h()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->e:Landroid/content/Context;

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/e;->a:Lcom/google/android/gms/people/identity/c;

    iget-object v0, v0, Lcom/google/android/gms/people/identity/c;->a:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-object v1, p0, Lcom/google/android/gms/people/identity/internal/w;->s:[Ljava/lang/String;

    invoke-virtual {p1, p0, v0, v1}, Lcom/google/android/gms/people/internal/p;->a(Lcom/google/android/gms/people/internal/s;Lcom/google/android/gms/people/identity/e;[Ljava/lang/String;)V

    :goto_0
    return-void

    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/gms/people/identity/internal/w;->w:Z

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v0, v0, Lcom/google/android/gms/people/identity/e;->b:Z

    if-nez v0, :cond_1

    iget-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->r:Lcom/google/android/gms/people/identity/e;

    iget-boolean v0, v0, Lcom/google/android/gms/people/identity/e;->c:Z

    if-eqz v0, :cond_2

    :cond_1
    sget-object v0, Lcom/google/android/gms/common/api/Status;->c:Lcom/google/android/gms/common/api/Status;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->v:Lcom/google/android/gms/common/api/Status;

    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/gms/people/identity/internal/w;->a(Lcom/google/android/gms/common/data/DataHolder;)V

    goto :goto_0

    :cond_2
    sget-object v0, Lcom/google/android/gms/common/api/Status;->a:Lcom/google/android/gms/common/api/Status;

    iput-object v0, p0, Lcom/google/android/gms/people/identity/internal/w;->v:Lcom/google/android/gms/common/api/Status;

    goto :goto_1
.end method
