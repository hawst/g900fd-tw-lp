.class public final Lcom/google/android/gms/games/service/a/m/f;
.super Lcom/google/android/gms/games/service/a/a;
.source "SourceFile"


# instance fields
.field private final c:Lcom/google/android/gms/games/internal/dr;

.field private final d:Lcom/google/android/gms/games/service/b;

.field private final e:Lcom/google/android/gms/games/a/au;

.field private final f:Ljava/lang/String;

.field private final g:Ljava/lang/String;

.field private final h:Lcom/google/android/gms/games/snapshot/d;

.field private final i:Lcom/google/android/gms/drive/m;

.field private j:Lcom/google/android/gms/games/service/a/m/e;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/games/internal/dr;Lcom/google/android/gms/games/service/b;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)V
    .locals 1

    .prologue
    .line 34
    iget-object v0, p1, Lcom/google/android/gms/games/a/au;->b:Lcom/google/android/gms/common/server/ClientContext;

    invoke-direct {p0, v0}, Lcom/google/android/gms/games/service/a/a;-><init>(Lcom/google/android/gms/common/server/ClientContext;)V

    .line 35
    iput-object p2, p0, Lcom/google/android/gms/games/service/a/m/f;->c:Lcom/google/android/gms/games/internal/dr;

    .line 36
    iput-object p3, p0, Lcom/google/android/gms/games/service/a/m/f;->d:Lcom/google/android/gms/games/service/b;

    .line 37
    iput-object p1, p0, Lcom/google/android/gms/games/service/a/m/f;->e:Lcom/google/android/gms/games/a/au;

    .line 38
    iput-object p4, p0, Lcom/google/android/gms/games/service/a/m/f;->f:Ljava/lang/String;

    .line 39
    iput-object p5, p0, Lcom/google/android/gms/games/service/a/m/f;->g:Ljava/lang/String;

    .line 40
    iput-object p6, p0, Lcom/google/android/gms/games/service/a/m/f;->h:Lcom/google/android/gms/games/snapshot/d;

    .line 41
    iput-object p7, p0, Lcom/google/android/gms/games/service/a/m/f;->i:Lcom/google/android/gms/drive/m;

    .line 42
    return-void
.end method


# virtual methods
.method public final a()V
    .locals 1

    .prologue
    .line 66
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->b()V

    .line 67
    return-void
.end method

.method protected final a(Lcom/google/android/gms/common/data/DataHolder;)V
    .locals 6

    .prologue
    .line 54
    invoke-virtual {p1}, Lcom/google/android/gms/common/data/DataHolder;->f()I

    move-result v0

    .line 55
    const/16 v1, 0xfa4

    if-ne v0, v1, :cond_0

    .line 56
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->c:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/a/m/e;->b:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v3, v3, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v4, v4, Lcom/google/android/gms/games/service/a/m/e;->d:Lcom/google/android/gms/drive/Contents;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v5, v5, Lcom/google/android/gms/games/service/a/m/e;->e:Lcom/google/android/gms/drive/Contents;

    invoke-interface/range {v0 .. v5}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Ljava/lang/String;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/Contents;Lcom/google/android/gms/drive/Contents;)V

    .line 62
    :goto_0
    return-void

    .line 60
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->c:Lcom/google/android/gms/games/internal/dr;

    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v1, v1, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    iget-object v2, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v2, v2, Lcom/google/android/gms/games/service/a/m/e;->c:Lcom/google/android/gms/drive/Contents;

    invoke-interface {v0, v1, v2}, Lcom/google/android/gms/games/internal/dr;->a(Lcom/google/android/gms/common/data/DataHolder;Lcom/google/android/gms/drive/Contents;)V

    goto :goto_0
.end method

.method protected final b(Landroid/content/Context;Lcom/google/android/gms/games/a/t;)Lcom/google/android/gms/common/data/DataHolder;
    .locals 7

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/android/gms/games/service/a/m/f;->e:Lcom/google/android/gms/games/a/au;

    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->d:Lcom/google/android/gms/games/service/b;

    invoke-virtual {v0}, Lcom/google/android/gms/games/service/b;->c()Lcom/google/android/gms/common/api/v;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/gms/games/service/a/m/f;->f:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/gms/games/service/a/m/f;->g:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/gms/games/service/a/m/f;->h:Lcom/google/android/gms/games/snapshot/d;

    iget-object v6, p0, Lcom/google/android/gms/games/service/a/m/f;->i:Lcom/google/android/gms/drive/m;

    move-object v0, p2

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/gms/games/a/t;->a(Lcom/google/android/gms/games/a/au;Lcom/google/android/gms/common/api/v;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/gms/games/snapshot/d;Lcom/google/android/gms/drive/m;)Lcom/google/android/gms/games/service/a/m/e;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    .line 49
    iget-object v0, p0, Lcom/google/android/gms/games/service/a/m/f;->j:Lcom/google/android/gms/games/service/a/m/e;

    iget-object v0, v0, Lcom/google/android/gms/games/service/a/m/e;->a:Lcom/google/android/gms/common/data/DataHolder;

    return-object v0
.end method
