.class public abstract Lcom/google/android/gms/car/qp;
.super Ljava/lang/Object;
.source "SourceFile"


# static fields
.field public static final a:Lcom/google/android/gms/car/pd;

.field public static final b:Lcom/google/android/gms/car/pd;

.field public static final c:Lcom/google/android/gms/car/pd;

.field public static final d:Lcom/google/android/gms/car/nm;

.field public static final e:Lcom/google/android/gms/car/nn;

.field protected static final f:[F


# instance fields
.field protected final g:J

.field protected volatile h:J

.field protected final i:Z

.field protected final j:[F

.field protected k:Lcom/google/android/gms/car/qq;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 422
    new-instance v0, Lcom/google/android/gms/car/pd;

    sget-object v1, Lcom/google/android/gms/car/pd;->a:[F

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/pd;-><init>([F)V

    sput-object v0, Lcom/google/android/gms/car/qp;->a:Lcom/google/android/gms/car/pd;

    .line 424
    new-instance v0, Lcom/google/android/gms/car/pd;

    sget-object v1, Lcom/google/android/gms/car/pd;->b:[F

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/pd;-><init>([F)V

    sput-object v0, Lcom/google/android/gms/car/qp;->b:Lcom/google/android/gms/car/pd;

    .line 426
    new-instance v0, Lcom/google/android/gms/car/pd;

    sget-object v1, Lcom/google/android/gms/car/pd;->c:[F

    invoke-direct {v0, v1}, Lcom/google/android/gms/car/pd;-><init>([F)V

    sput-object v0, Lcom/google/android/gms/car/qp;->c:Lcom/google/android/gms/car/pd;

    .line 428
    new-instance v0, Lcom/google/android/gms/car/nm;

    invoke-direct {v0}, Lcom/google/android/gms/car/nm;-><init>()V

    sput-object v0, Lcom/google/android/gms/car/qp;->d:Lcom/google/android/gms/car/nm;

    .line 430
    new-instance v0, Lcom/google/android/gms/car/nn;

    const/16 v1, 0x64

    invoke-direct {v0, v1, v2}, Lcom/google/android/gms/car/nn;-><init>(II)V

    sput-object v0, Lcom/google/android/gms/car/qp;->e:Lcom/google/android/gms/car/nn;

    .line 435
    const/16 v0, 0x10

    new-array v0, v0, [F

    .line 436
    sput-object v0, Lcom/google/android/gms/car/qp;->f:[F

    invoke-static {v0, v2}, Landroid/opengl/Matrix;->setIdentityM([FI)V

    .line 437
    return-void
.end method

.method public constructor <init>(JZ)V
    .locals 3

    .prologue
    .line 448
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 445
    const/16 v0, 0x10

    new-array v0, v0, [F

    iput-object v0, p0, Lcom/google/android/gms/car/qp;->j:[F

    .line 449
    iput-wide p1, p0, Lcom/google/android/gms/car/qp;->g:J

    .line 450
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/qp;->h:J

    .line 451
    iput-boolean p3, p0, Lcom/google/android/gms/car/qp;->i:Z

    .line 452
    new-instance v0, Lcom/google/android/gms/car/qq;

    invoke-direct {v0}, Lcom/google/android/gms/car/qq;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/qp;->k:Lcom/google/android/gms/car/qq;

    .line 453
    return-void
.end method


# virtual methods
.method public final a(Z)Lcom/google/android/gms/car/qq;
    .locals 4

    .prologue
    .line 480
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 481
    iget-wide v2, p0, Lcom/google/android/gms/car/qp;->h:J

    sub-long/2addr v0, v2

    .line 482
    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/gms/car/qp;->a(ZJ)Lcom/google/android/gms/car/qq;

    move-result-object v0

    return-object v0
.end method

.method protected abstract a(ZJ)Lcom/google/android/gms/car/qq;
.end method

.method public final a()V
    .locals 2

    .prologue
    .line 456
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/gms/car/qp;->h:J

    .line 457
    return-void
.end method

.method protected a(J)Z
    .locals 5

    .prologue
    .line 487
    iget-wide v0, p0, Lcom/google/android/gms/car/qp;->g:J

    iget-wide v2, p0, Lcom/google/android/gms/car/qp;->h:J

    add-long/2addr v0, v2

    cmp-long v0, p1, v0

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final b()J
    .locals 2

    .prologue
    .line 460
    iget-wide v0, p0, Lcom/google/android/gms/car/qp;->h:J

    return-wide v0
.end method

.method public final c()J
    .locals 2

    .prologue
    .line 464
    iget-wide v0, p0, Lcom/google/android/gms/car/qp;->g:J

    return-wide v0
.end method
