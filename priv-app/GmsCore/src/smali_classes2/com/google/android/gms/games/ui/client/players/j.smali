.class public final Lcom/google/android/gms/games/ui/client/players/j;
.super Lcom/google/android/gms/games/ui/bk;
.source "SourceFile"


# instance fields
.field final b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

.field final c:Landroid/widget/ImageView;

.field final d:Landroid/widget/TextView;

.field final e:Landroid/database/CharArrayBuffer;

.field final f:Landroid/widget/ImageView;

.field final g:Landroid/widget/ImageView;

.field final h:Landroid/widget/TextView;

.field final i:Landroid/widget/ImageView;

.field final j:Landroid/view/View;

.field final k:Landroid/content/res/Resources;

.field final l:I

.field final m:I

.field final synthetic n:Lcom/google/android/gms/games/ui/client/players/i;


# direct methods
.method public constructor <init>(Lcom/google/android/gms/games/ui/client/players/i;Lcom/google/android/gms/games/ui/o;Landroid/view/View;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    const/4 v3, -0x1

    .line 340
    iput-object p1, p0, Lcom/google/android/gms/games/ui/client/players/j;->n:Lcom/google/android/gms/games/ui/client/players/i;

    .line 341
    invoke-direct {p0, p2}, Lcom/google/android/gms/games/ui/bk;-><init>(Lcom/google/android/gms/games/ui/o;)V

    .line 342
    sget v0, Lcom/google/android/gms/j;->oN:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/common/images/internal/LoadingImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->b:Lcom/google/android/gms/common/images/internal/LoadingImageView;

    .line 343
    sget v0, Lcom/google/android/gms/j;->oO:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->d:Landroid/widget/TextView;

    .line 344
    sget v0, Lcom/google/android/gms/j;->mr:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->c:Landroid/widget/ImageView;

    .line 345
    new-instance v0, Landroid/database/CharArrayBuffer;

    const/16 v2, 0x40

    invoke-direct {v0, v2}, Landroid/database/CharArrayBuffer;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->e:Landroid/database/CharArrayBuffer;

    .line 346
    sget v0, Lcom/google/android/gms/j;->kB:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->f:Landroid/widget/ImageView;

    .line 347
    sget v0, Lcom/google/android/gms/j;->nd:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    .line 348
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->g:Landroid/widget/ImageView;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/games/ui/client/players/i;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/ImageView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 349
    sget v0, Lcom/google/android/gms/j;->mH:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->h:Landroid/widget/TextView;

    .line 350
    sget v0, Lcom/google/android/gms/j;->kg:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->i:Landroid/widget/ImageView;

    .line 352
    sget v0, Lcom/google/android/gms/j;->nf:I

    invoke-virtual {p3, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    .line 353
    iget-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->j:Landroid/view/View;

    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/players/i;->a(Lcom/google/android/gms/games/ui/client/players/i;)Landroid/view/View$OnClickListener;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 355
    invoke-virtual {p3}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->k:Landroid/content/res/Resources;

    .line 358
    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/players/i;->b(Lcom/google/android/gms/games/ui/client/players/i;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 359
    const/4 v0, 0x1

    iput v1, p0, Lcom/google/android/gms/games/ui/client/players/j;->l:I

    .line 364
    :goto_0
    invoke-static {p1}, Lcom/google/android/gms/games/ui/client/players/i;->c(Lcom/google/android/gms/games/ui/client/players/i;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 365
    iput v0, p0, Lcom/google/android/gms/games/ui/client/players/j;->m:I

    .line 369
    :goto_1
    return-void

    .line 361
    :cond_0
    iput v3, p0, Lcom/google/android/gms/games/ui/client/players/j;->l:I

    move v0, v1

    goto :goto_0

    .line 367
    :cond_1
    iput v3, p0, Lcom/google/android/gms/games/ui/client/players/j;->m:I

    goto :goto_1
.end method
