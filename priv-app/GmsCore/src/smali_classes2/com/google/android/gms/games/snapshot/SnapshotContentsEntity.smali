.class public final Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;
.super Ljava/lang/Object;
.source "SourceFile"

# interfaces
.implements Lcom/google/android/gms/common/internal/safeparcel/SafeParcelable;
.implements Lcom/google/android/gms/games/snapshot/SnapshotContents;


# static fields
.field public static final CREATOR:Lcom/google/android/gms/games/snapshot/a;

.field private static final a:Ljava/lang/Object;


# instance fields
.field private final b:I

.field private c:Lcom/google/android/gms/drive/Contents;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->a:Ljava/lang/Object;

    .line 40
    new-instance v0, Lcom/google/android/gms/games/snapshot/a;

    invoke-direct {v0}, Lcom/google/android/gms/games/snapshot/a;-><init>()V

    sput-object v0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->CREATOR:Lcom/google/android/gms/games/snapshot/a;

    return-void
.end method

.method constructor <init>(ILcom/google/android/gms/drive/Contents;)V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 62
    iput p1, p0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->b:I

    .line 63
    iput-object p2, p0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->c:Lcom/google/android/gms/drive/Contents;

    .line 64
    return-void
.end method


# virtual methods
.method public final a()I
    .locals 1

    .prologue
    .line 72
    iget v0, p0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->b:I

    return v0
.end method

.method public final b()Lcom/google/android/gms/drive/Contents;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->c:Lcom/google/android/gms/drive/Contents;

    return-object v0
.end method

.method public final c()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;->c:Lcom/google/android/gms/drive/Contents;

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final describeContents()I
    .locals 1

    .prologue
    .line 166
    const/4 v0, 0x0

    return v0
.end method

.method public final writeToParcel(Landroid/os/Parcel;I)V
    .locals 0

    .prologue
    .line 171
    invoke-static {p0, p1, p2}, Lcom/google/android/gms/games/snapshot/a;->a(Lcom/google/android/gms/games/snapshot/SnapshotContentsEntity;Landroid/os/Parcel;I)V

    .line 172
    return-void
.end method
