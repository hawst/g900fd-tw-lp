.class public final Lcom/google/android/gms/games/provider/az;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "SourceFile"


# static fields
.field static final a:I

.field static final c:I


# instance fields
.field final b:Ljava/util/TreeMap;

.field private final d:Lcom/google/android/gms/games/provider/ay;

.field private final e:Landroid/content/Context;

.field private final f:Ljava/util/ArrayList;

.field private final g:Ljava/util/ArrayList;

.field private final h:Ljava/util/ArrayList;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 127
    const/16 v0, 0x3ea

    sput v0, Lcom/google/android/gms/games/provider/az;->a:I

    .line 165
    const/16 v0, 0xc4

    sput v0, Lcom/google/android/gms/games/provider/az;->c:I

    return-void
.end method

.method public constructor <init>(Lcom/google/android/gms/games/provider/ay;Landroid/content/Context;Ljava/lang/String;)V
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 177
    if-nez p3, :cond_0

    move-object v0, v1

    :goto_0
    sget v2, Lcom/google/android/gms/games/provider/az;->a:I

    invoke-direct {p0, p2, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 138
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    .line 144
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    .line 150
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/az;->h:Ljava/util/ArrayList;

    .line 156
    new-instance v0, Ljava/util/TreeMap;

    invoke-direct {v0}, Ljava/util/TreeMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    .line 178
    iput-object p1, p0, Lcom/google/android/gms/games/provider/az;->d:Lcom/google/android/gms/games/provider/ay;

    .line 179
    iput-object p2, p0, Lcom/google/android/gms/games/provider/az;->e:Landroid/content/Context;

    .line 180
    return-void

    .line 177
    :cond_0
    const-string v0, "games_%s.db"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p3, v2, v3

    invoke-static {v0, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private static a(II)I
    .locals 1

    .prologue
    .line 1107
    add-int v0, p0, p1

    return v0
.end method

.method private a()V
    .locals 9

    .prologue
    const/16 v8, 0x9

    const/4 v7, 0x1

    const/16 v6, 0x2bc

    const/4 v5, -0x1

    const/4 v4, 0x0

    .line 253
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lez v0, :cond_0

    .line 254
    const-string v0, "GamesDatabaseHelper"

    const-string v1, "Builders already initialized."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 1003
    :goto_0
    return-void

    .line 258
    :cond_0
    const-string v0, "GamesDatabaseHelper"

    const-string v1, "Initializing builders."

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->a(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "images"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "url"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "local"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "filesize"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "download_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 267
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 270
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "players"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "profile_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "profile_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "profile_hi_res_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_updated"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "is_in_circles"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "current_xp_total"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "current_level"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "current_level_min_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "current_level_max_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "next_level"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "next_level_max_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_level_up_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "has_all_public_acls"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "is_profile_visible"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_activity_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_game_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_game_icon_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_game_hi_res_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "most_recent_game_featured_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_player_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 301
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 302
    const-string v1, "is_in_circles"

    const/16 v2, 0x193

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 303
    const-string v1, "current_xp_total"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 305
    const-string v1, "current_level"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 306
    const-string v1, "current_level_min_xp"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 308
    const-string v1, "current_level_max_xp"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 310
    const-string v1, "next_level"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 312
    const-string v1, "next_level_max_xp"

    const/16 v2, 0x2bd

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 314
    const-string v1, "last_level_up_timestamp"

    const/16 v2, 0x2be

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 316
    const-string v1, "player_title"

    const/16 v2, 0x2c4

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 318
    const-string v1, "most_recent_external_game_id"

    const/16 v2, 0x2c9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 320
    const-string v1, "most_recent_game_name"

    const/16 v2, 0x2c9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 322
    const-string v1, "most_recent_game_icon_id"

    const/16 v2, 0x2c9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 324
    const-string v1, "most_recent_game_hi_res_id"

    const/16 v2, 0x2c9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 326
    const-string v1, "most_recent_game_featured_id"

    const/16 v2, 0x2c9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 328
    const-string v1, "has_all_public_acls"

    const/16 v2, 0x2d0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 330
    const-string v1, "is_profile_visible"

    const/16 v2, 0x324

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 334
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "games"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "display_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "primary_category"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "secondary_category"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "developer_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_hi_res_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "featured_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "play_enabled_game"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_played_server_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_connection_local_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_synced_local_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "metadata_version"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "sync_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "metadata_sync_requested"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "target_instance"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "gameplay_acl_status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "availability"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "owned"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "achievement_total_count"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "leaderboard_count"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "price_micros"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "formatted_price"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "full_price_micros"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "formatted_full_price"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "muted"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "identity_sharing_confirmed"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "snapshots_enabled"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "theme_color"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "000000"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->b(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 369
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 370
    const-string v1, "muted"

    const/16 v2, 0x1f9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 371
    const-string v1, "identity_sharing_confirmed"

    const/16 v2, 0x259

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 373
    const-string v1, "snapshots_enabled"

    const/16 v2, 0x2ce

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 375
    const-string v1, "theme_color"

    const/16 v2, 0x322

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 376
    const/16 v1, 0x322

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 377
    const/16 v1, 0x325

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 378
    const-string v1, "sync_token"

    const/16 v2, 0x387

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 381
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "game_badges"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "badge_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "badge_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "badge_title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "badge_description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "badge_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "badge_game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 390
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 393
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "game_instances"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "real_time_support"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "turn_based_support"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "platform_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "instance_display_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "package_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "piracy_check"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "installed"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "preferred"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 406
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 407
    const-string v1, "preferred"

    const/16 v2, 0xc6

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 411
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "client_contexts"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "package_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "package_uid"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "account_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 416
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 419
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "achievement_definitions"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_achievement_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "unlocked_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "revealed_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "total_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "formatted_total_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "initial_state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "sorting_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "definition_xp_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 439
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 440
    const-string v1, "definition_xp_value"

    const/16 v2, 0x2c0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 445
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "achievement_pending_ops"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_contexts"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_achievement_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "achievement_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "new_state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "steps_to_increment"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "min_steps_to_set"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 458
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 459
    const-string v1, "min_steps_to_set"

    const/16 v2, 0xca

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 461
    const-string v1, "external_game_id"

    const/16 v2, 0x132

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 463
    const-string v1, "external_player_id"

    const/16 v2, 0x132

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 465
    const-string v1, "client_context_id"

    const/16 v2, 0x2c1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 469
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "achievement_instances"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "definition_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "achievement_definitions"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "current_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "formatted_current_steps"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_updated_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_xp_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "definition_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 484
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 485
    const-string v1, "instance_xp_value"

    const/16 v2, 0x2c0

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 489
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "invitations"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_invitation_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_inviter_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "creation_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "last_modified_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "variant"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "has_automatch_criteria"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "automatch_min_players"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "automatch_max_players"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "inviter_in_circles"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_invitation_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 506
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 507
    const-string v1, "variant"

    const/16 v2, 0xc5

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 508
    const-string v1, "automatch_max_players"

    const/16 v2, 0x196

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 510
    const-string v1, "automatch_min_players"

    const/16 v2, 0x196

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 512
    const-string v1, "has_automatch_criteria"

    const/16 v2, 0x196

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 514
    const-string v1, "inviter_in_circles"

    const/16 v2, 0x1f5

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 516
    const-string v1, "external_invitation_id"

    const/16 v2, 0x2c1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 520
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "leaderboards"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_leaderboard_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "board_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "sorting_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "score_order"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "game_id,external_leaderboard_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 530
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 531
    const-string v1, "game_id,external_leaderboard_id"

    const/16 v2, 0x2c1

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 536
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "leaderboard_instances"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "leaderboard_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "leaderboards"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "timespan"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "collection"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "player_raw_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_display_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_display_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_score_tag"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "total_scores"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "top_page_token_next"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_page_token_prev"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_page_token_next"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "leaderboard_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 553
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 554
    const-string v1, "player_score_tag"

    const/16 v2, 0xc9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 558
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "leaderboard_scores"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "leaderboard_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "page_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "default_display_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "default_display_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "display_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "raw_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "display_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "achieved_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "score_tag"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 575
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 576
    const-string v1, "score_tag"

    const/16 v2, 0xc9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 580
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "leaderboard_pending_scores"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_contexts"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_leaderboard_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "raw_score"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "achieved_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "score_tag"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "signature"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, ""

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->b(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 592
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 593
    const-string v1, "score_tag"

    const/16 v2, 0xc9

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 595
    const-string v1, "signature"

    const/16 v2, 0x325

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 599
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "matches"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_match_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "creator_external"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "creation_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "last_updater_external"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_updated_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "pending_participant_external"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "data"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->e:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "version"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "variant"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "notification_text"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "user_match_status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "has_automatch_criteria"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "automatch_min_players"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "automatch_max_players"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "automatch_bit_mask"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "automatch_wait_estimate_sec"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "rematch_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "match_number"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "previous_match_data"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->e:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "upsync_required"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "description_participant_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_match_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 628
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 629
    const-string v1, "automatch_wait_estimate_sec"

    const/16 v2, 0x12c

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 630
    const-string v1, "rematch_id"

    const/16 v2, 0x12d

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 631
    const-string v1, "match_number"

    const/16 v2, 0x12d

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 632
    const-string v1, "previous_match_data"

    const/16 v2, 0x12d

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 634
    const/16 v1, 0x12f

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 635
    const/16 v1, 0x131

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 636
    const-string v1, "description_participant_id"

    const/16 v2, 0x1f8

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 639
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "matches_pending_ops"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_contexts"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "NOT NULL"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;)V

    const-string v1, "external_match_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "pending_participant_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "version"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "is_turn"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "results"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 652
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 653
    const/16 v1, 0x130

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 654
    const/16 v1, 0x131

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 657
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "notifications"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "notification_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_sub_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "ticker"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "text"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "coalesced_text"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "acknowledged"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "alert_level"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 671
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 672
    const/16 v1, 0x1f4

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 673
    const-string v1, "notification_id"

    const/16 v2, 0xc8

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 674
    const-string v1, "coalesced_text"

    const/16 v2, 0x12c

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 676
    const-string v1, "timestamp"

    const/16 v2, 0x12c

    const/4 v3, 0x7

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 677
    const-string v1, "alert_level"

    const/16 v2, 0x1f4

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 681
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "participants"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "match_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "matches"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "invitation_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "invitations"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_participant_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "default_display_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "default_display_hi_res_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "default_display_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_address"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "result_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "placing"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "connected"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "capabilities"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "match_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "invitation_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 703
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 704
    const-string v1, "capabilities"

    const/16 v2, 0x64

    const/16 v3, 0x63

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 706
    const/16 v1, 0x12c

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 707
    const/16 v1, 0x12c

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 708
    const-string v1, "default_display_hi_res_image_id"

    const/16 v2, 0x190

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 711
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "requests"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_request_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "sender_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "data"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->e:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "creation_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "expiration_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "sender_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 723
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 724
    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 726
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "request_recipients"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "request_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "requests"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "recipient_status"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "request_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 735
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 736
    const/16 v1, 0x1f4

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 737
    const-string v1, "request_id"

    const/4 v2, 0x5

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 741
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "application_sessions"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "session_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "start_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "end_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_contexts"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "ad_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "limit_ad_tracking"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->b:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 754
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 755
    const/16 v1, 0x258

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 756
    const-string v1, "ad_id"

    const/16 v2, 0x384

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 757
    const-string v1, "limit_ad_tracking"

    const/16 v2, 0x384

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 761
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "event_definitions"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_definitions_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_event_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "sorting_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "visibility"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_definitions_game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 772
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 773
    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 774
    const/16 v1, 0x11

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 777
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "event_pending_ops"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "instance_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_contexts"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_start_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_end_time"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "increment"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "request_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_start_time"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "window_end_time"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 789
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 790
    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 793
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "event_instances"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "definition_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_definitions"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "formatted_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_updated_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "definition_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "player_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 805
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 806
    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 809
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "quests"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "accepted_ts"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_banner_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_end_ts"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_quest_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_last_updated_ts"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "notified"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "notification_ts"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_start_ts"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 827
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 828
    const/4 v1, 0x3

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 830
    const/4 v1, 0x6

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 832
    const/4 v1, 0x7

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 835
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "milestones"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "completion_reward_data"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->e:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_milestone_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_instance_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "event_instances"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "initial_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quests"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "milestones_sorting_rank"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "milestone_state"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "target_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "quest_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->c(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 848
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 849
    const/4 v1, 0x3

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 850
    const/4 v1, 0x6

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 851
    const/16 v1, 0xe

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 852
    const/16 v1, 0xf

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 855
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "snapshots"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "owner_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "players"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_snapshot_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "drive_resolved_id_string"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "drive_resource_id_string"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "cover_icon_image_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "last_modified_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "duration"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "cover_icon_image_height"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "cover_icon_image_width"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "unique_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "visible"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v7}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "pending_change_count"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "progress_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->d:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 874
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 875
    const/4 v1, 0x5

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 876
    const-string v1, "cover_icon_image_height"

    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 878
    const-string v1, "cover_icon_image_width"

    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 880
    const-string v1, "drive_resolved_id_string"

    const/16 v2, 0xa

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 882
    const-string v1, "drive_resource_id_string"

    const/16 v2, 0xa

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 886
    const/16 v1, 0xa

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 887
    const/16 v1, 0x10

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 888
    const-string v1, "visible"

    const/16 v2, 0x13

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 889
    const-string v1, "pending_change_count"

    const/16 v2, 0x384

    invoke-static {v2, v7}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 891
    const-string v1, "progress_value"

    const/16 v2, 0x3e8

    invoke-static {v2, v7}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 893
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "experience_events"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_experience_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "games"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "created_timestamp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "display_title"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "display_description"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "display_string"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "icon_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "images"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->e(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "type"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "current_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "xp_earned"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v4}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "newLevel"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 907
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 908
    const/16 v1, 0xb

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 909
    const-string v1, "newLevel"

    const/16 v2, 0x320

    invoke-static {v2, v7}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V

    .line 911
    const/16 v1, 0x320

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 912
    const/16 v1, 0x320

    const/4 v2, 0x3

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 914
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "player_levels"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "level_value"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "level_min_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "level_max_xp"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0, v5}, Lcom/google/android/gms/games/provider/bd;->a(I)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "version"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 921
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 922
    const/16 v1, 0xc

    invoke-static {v6, v1}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 924
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "request_pending_ops"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_request_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "external_game_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "client_context_id"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->c:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 930
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 931
    const/16 v1, 0x320

    invoke-static {v1, v4}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 934
    new-instance v0, Lcom/google/android/gms/games/provider/bd;

    const-string v1, "app_content"

    invoke-direct {v0, v1}, Lcom/google/android/gms/games/provider/bd;-><init>(Ljava/lang/String;)V

    const-string v1, "_id"

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/bd;->d(Ljava/lang/String;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "experiments"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "json"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->e:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "page_token"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    const-string v1, "screen_name"

    sget-object v2, Lcom/google/android/gms/games/provider/d;->a:Lcom/google/android/gms/games/provider/d;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/gms/games/provider/bd;->a(Ljava/lang/String;Lcom/google/android/gms/games/provider/d;)Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->a()Lcom/google/android/gms/games/provider/bd;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/games/provider/bd;->b()Lcom/google/android/gms/games/provider/bc;

    move-result-object v0

    .line 941
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 942
    const/16 v1, 0x3e8

    const/4 v2, 0x2

    invoke-static {v1, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/gms/games/provider/az;->b(Lcom/google/android/gms/games/provider/bb;I)V

    .line 949
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->d:Lcom/google/android/gms/games/provider/ay;

    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->e:Landroid/content/Context;

    invoke-virtual {v0, v1}, Lcom/google/android/gms/games/provider/ay;->a(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 950
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 953
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->h:Ljava/util/ArrayList;

    const-string v2, "featured_games"

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 957
    new-instance v1, Lcom/google/android/gms/games/provider/be;

    const-string v2, "matches"

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/provider/be;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x1f4

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 959
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "match_sync_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    const/16 v2, 0x1f4

    const/4 v3, 0x4

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 964
    new-instance v1, Lcom/google/android/gms/games/provider/be;

    const-string v2, "requests"

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/provider/be;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x1f4

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 966
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "request_sync_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    const/16 v2, 0x1f4

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 971
    new-instance v1, Lcom/google/android/gms/games/provider/be;

    const-string v2, "quests"

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/provider/be;-><init>(Ljava/lang/String;)V

    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 973
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "quest_sync_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 975
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "quest_sync_metadata_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    invoke-static {v6, v8}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 980
    new-instance v1, Lcom/google/android/gms/games/provider/be;

    const-string v2, "experience_events"

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/provider/be;-><init>(Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 982
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "xp_sync_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    const/16 v2, 0xb

    invoke-static {v6, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 986
    new-instance v1, Lcom/google/android/gms/games/provider/be;

    const-string v2, "experience_events"

    invoke-direct {v1, v2}, Lcom/google/android/gms/games/provider/be;-><init>(Ljava/lang/String;)V

    const/16 v2, 0x320

    const/4 v3, 0x2

    invoke-static {v2, v3}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v2

    invoke-direct {p0, v1, v2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 988
    new-instance v1, Lcom/google/android/gms/games/provider/ba;

    const-string v2, "xp_sync_token"

    invoke-direct {v1, v0, v2}, Lcom/google/android/gms/games/provider/ba;-><init>(Landroid/content/SharedPreferences$Editor;Ljava/lang/String;)V

    const/16 v0, 0x320

    const/4 v2, 0x2

    invoke-static {v0, v2}, Lcom/google/android/gms/games/provider/az;->a(II)I

    move-result v0

    invoke-direct {p0, v1, v0}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 992
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "event_pending_ops_window_start_time_INDEX"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 993
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "event_pending_ops_window_end_time_INDEX"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 996
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "leaderboards_game_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 997
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "matches_pending_op_client_contexts_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 998
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "invitations_external_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 999
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "requests_participants_request_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1000
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "achievement_pending_op_client_contexts_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1001
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "application_sessions_external_game_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1002
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    const-string v1, "milestones_player_id_index"

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_0
.end method

.method private a(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7

    .prologue
    const/4 v6, 0x1

    const/4 v1, 0x0

    .line 1024
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->a()V

    .line 1027
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v2, v1

    :goto_0
    if-ge v2, v3, :cond_0

    .line 1028
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/bc;

    invoke-virtual {v0, p1}, Lcom/google/android/gms/games/provider/bc;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 1027
    add-int/lit8 v0, v2, 0x1

    move v2, v0

    goto :goto_0

    .line 1032
    :cond_0
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_1
    if-ge v0, v2, :cond_1

    .line 1033
    const-string v3, "DROP TABLE IF EXISTS %s;"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/games/provider/az;->h:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1032
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1038
    :cond_1
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v2

    move v0, v1

    :goto_2
    if-ge v0, v2, :cond_2

    .line 1039
    const-string v3, "DROP INDEX IF EXISTS %s;"

    new-array v4, v6, [Ljava/lang/Object;

    iget-object v5, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    invoke-virtual {v5, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    aput-object v5, v4, v1

    invoke-static {v3, v4}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 1038
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1042
    :cond_2
    return-void
.end method

.method private a(Lcom/google/android/gms/games/provider/bb;I)V
    .locals 3

    .prologue
    .line 1052
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 1055
    if-nez v0, :cond_0

    .line 1056
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 1057
    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1060
    :cond_0
    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 1061
    return-void
.end method

.method private a(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1071
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/games/provider/bc;->a(Ljava/lang/String;I)Lcom/google/android/gms/games/provider/bf;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 1072
    return-void
.end method

.method private b()V
    .locals 1

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->h:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1013
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->g:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1014
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 1015
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    invoke-virtual {v0}, Ljava/util/TreeMap;->clear()V

    .line 1016
    return-void
.end method

.method private b(Lcom/google/android/gms/games/provider/bb;I)V
    .locals 0

    .prologue
    .line 1096
    invoke-direct {p0, p1, p2}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 1097
    return-void
.end method

.method private b(Lcom/google/android/gms/games/provider/bc;Ljava/lang/String;I)V
    .locals 1

    .prologue
    .line 1082
    invoke-virtual {p1, p2, p3}, Lcom/google/android/gms/games/provider/bc;->b(Ljava/lang/String;I)Lcom/google/android/gms/games/provider/bf;

    move-result-object v0

    invoke-direct {p0, v0, p3}, Lcom/google/android/gms/games/provider/az;->a(Lcom/google/android/gms/games/provider/bb;I)V

    .line 1083
    return-void
.end method


# virtual methods
.method public final onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 4

    .prologue
    .line 184
    sget v2, Lcom/google/android/gms/games/provider/az;->a:I

    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v3, "Bootstrapping database version: "

    invoke-direct {v1, v3}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->a()V

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_0

    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->f:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/gms/games/provider/bc;

    invoke-virtual {v0, p1, v2}, Lcom/google/android/gms/games/provider/bc;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->b()V

    .line 185
    return-void
.end method

.method public final onDowngrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 3

    .prologue
    .line 231
    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Downgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 232
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->a()V

    .line 236
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/az;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 237
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/provider/az;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 239
    return-void
.end method

.method public final onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 7

    .prologue
    const/4 v3, 0x0

    .line 203
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->a()V

    .line 204
    sget v0, Lcom/google/android/gms/games/provider/az;->c:I

    if-lt p2, v0, :cond_0

    const/16 v0, 0x44c

    if-lt p3, v0, :cond_1

    .line 205
    :cond_0
    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", all data will be wiped!"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-direct {p0, p1}, Lcom/google/android/gms/games/provider/az;->a(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 208
    invoke-virtual {p0, p1}, Lcom/google/android/gms/games/provider/az;->onCreate(Landroid/database/sqlite/SQLiteDatabase;)V

    .line 227
    :goto_0
    return-void

    .line 214
    :cond_1
    const-string v0, "GamesDatabaseHelper"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Upgrading from version "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " to "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/gms/games/internal/dq;->c(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    const/4 v4, 0x1

    invoke-virtual {v0, v1, v3, v2, v4}, Ljava/util/TreeMap;->subMap(Ljava/lang/Object;ZLjava/lang/Object;Z)Ljava/util/NavigableMap;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/NavigableMap;->keySet()Ljava/util/Set;

    move-result-object v0

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v5

    .line 220
    iget-object v0, p0, Lcom/google/android/gms/games/provider/az;->b:Ljava/util/TreeMap;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/ArrayList;

    .line 221
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v6

    move v2, v3

    :goto_1
    if-ge v2, v6, :cond_2

    .line 222
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/gms/games/provider/bb;

    invoke-interface {v1, p1, v5}, Lcom/google/android/gms/games/provider/bb;->a(Landroid/database/sqlite/SQLiteDatabase;I)V

    .line 221
    add-int/lit8 v1, v2, 0x1

    move v2, v1

    goto :goto_1

    .line 226
    :cond_3
    invoke-direct {p0}, Lcom/google/android/gms/games/provider/az;->b()V

    goto :goto_0
.end method
