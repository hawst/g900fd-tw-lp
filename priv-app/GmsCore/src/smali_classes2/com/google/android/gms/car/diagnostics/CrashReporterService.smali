.class public final Lcom/google/android/gms/car/diagnostics/CrashReporterService;
.super Landroid/app/IntentService;
.source "SourceFile"


# static fields
.field public static final a:Ljava/lang/Throwable;

.field private static final b:[I

.field private static final c:Ljava/lang/Throwable;


# instance fields
.field private d:Lcom/google/android/gms/common/api/v;

.field private final e:Ljava/lang/Object;

.field private final f:Ljava/util/Random;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 32
    const/4 v0, 0x2

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->b:[I

    .line 42
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "NULL_EXCEPTION"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->c:Ljava/lang/Throwable;

    .line 47
    new-instance v0, Ljava/lang/Exception;

    const-string v1, "ANR"

    invoke-direct {v0, v1}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->a:Ljava/lang/Throwable;

    return-void

    .line 32
    nop

    :array_0
    .array-data 4
        0x6
        0x9
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 67
    const-class v0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 62
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->e:Ljava/lang/Object;

    .line 64
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->f:Ljava/util/Random;

    .line 68
    return-void
.end method

.method public static a(Landroid/content/Context;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3

    .prologue
    .line 51
    const-string v0, "CAR.DIAGNOSTICS"

    const/4 v1, 0x3

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 52
    const-string v0, "CAR.DIAGNOSTICS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v2, "Reporting crash for "

    invoke-direct {v1, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/gms/car/diagnostics/CrashReporterService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 55
    const-string v1, "package_name"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 56
    const-string v1, "exception"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/io/Serializable;)Landroid/content/Intent;

    .line 57
    invoke-virtual {p0, v0}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 58
    return-void
.end method


# virtual methods
.method public final onCreate()V
    .locals 3

    .prologue
    .line 72
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 73
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 74
    :try_start_0
    new-instance v0, Lcom/google/android/gms/common/api/w;

    invoke-virtual {p0}, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/gms/common/api/w;-><init>(Landroid/content/Context;)V

    sget-object v2, Lcom/google/android/gms/feedback/g;->a:Lcom/google/android/gms/common/api/c;

    invoke-virtual {v0, v2}, Lcom/google/android/gms/common/api/w;->a(Lcom/google/android/gms/common/api/c;)Lcom/google/android/gms/common/api/w;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/w;->a()Lcom/google/android/gms/common/api/v;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    .line 77
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->b()V

    .line 78
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    return-void

    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method public final onDestroy()V
    .locals 2

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->e:Ljava/lang/Object;

    monitor-enter v1

    .line 123
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->d()V

    .line 124
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    .line 125
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 126
    invoke-super {p0}, Landroid/app/IntentService;->onDestroy()V

    .line 127
    return-void

    .line 125
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method protected final onHandleIntent(Landroid/content/Intent;)V
    .locals 11

    .prologue
    const/4 v1, 0x1

    .line 83
    iget-object v2, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->e:Ljava/lang/Object;

    monitor-enter v2

    .line 85
    :try_start_0
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    invoke-interface {v0}, Lcom/google/android/gms/common/api/v;->c()Lcom/google/android/gms/common/c;

    .line 86
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 87
    const-string v0, "package_name"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 88
    const-string v0, "exception"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getSerializableExtra(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v0

    check-cast v0, Ljava/lang/Throwable;

    .line 95
    if-nez v0, :cond_1

    .line 97
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v2, "Crash with no stack trace "

    invoke-direct {v0, v2}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v2, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->f:Ljava/util/Random;

    invoke-virtual {v2}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-virtual {v0, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 98
    sget-object v2, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->c:Ljava/lang/Throwable;

    .line 104
    :goto_0
    :try_start_1
    new-instance v4, Lcom/google/android/gms/feedback/p;

    invoke-direct {v4, v2}, Lcom/google/android/gms/feedback/p;-><init>(Ljava/lang/Throwable;)V

    iput-object v3, v4, Lcom/google/android/gms/feedback/p;->c:Ljava/lang/String;

    iput-object v0, v4, Lcom/google/android/gms/feedback/o;->a:Ljava/lang/String;

    const/4 v0, 0x1

    iput-boolean v0, v4, Lcom/google/android/gms/feedback/o;->b:Z

    invoke-virtual {v4}, Lcom/google/android/gms/feedback/o;->a()Lcom/google/android/gms/feedback/FeedbackOptions;

    move-result-object v4

    .line 109
    invoke-static {}, Landroid/app/ActivityManager;->isRunningInTestHarness()Z

    move-result v0

    if-nez v0, :cond_2

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->d()J

    move-result-wide v6

    const-wide/16 v8, -0x1

    cmp-long v0, v6, v8

    if-eqz v0, :cond_2

    sget-object v0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->b:[I

    invoke-static {}, Lcom/google/android/gms/common/util/ay;->e()I

    move-result v5

    invoke-static {v0, v5}, Lcom/google/android/gms/common/util/h;->a([II)Z

    move-result v0

    if-eqz v0, :cond_2

    move v0, v1

    :goto_1
    if-eqz v0, :cond_3

    .line 110
    iget-object v0, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->d:Lcom/google/android/gms/common/api/v;

    invoke-static {v0, v4}, Lcom/google/android/gms/feedback/g;->a(Lcom/google/android/gms/common/api/v;Lcom/google/android/gms/feedback/FeedbackOptions;)Lcom/google/android/gms/common/api/am;
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0

    .line 118
    :cond_0
    :goto_2
    return-void

    .line 86
    :catchall_0
    move-exception v0

    monitor-exit v2

    throw v0

    .line 100
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    const-string v4, "Crash "

    invoke-direct {v2, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iget-object v4, p0, Lcom/google/android/gms/car/diagnostics/CrashReporterService;->f:Ljava/util/Random;

    invoke-virtual {v4}, Ljava/util/Random;->nextLong()J

    move-result-wide v4

    invoke-virtual {v2, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object v10, v2

    move-object v2, v0

    move-object v0, v10

    goto :goto_0

    .line 109
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 111
    :cond_3
    :try_start_2
    const-string v0, "CAR.DIAGNOSTICS"

    const/4 v1, 0x5

    invoke-static {v0, v1}, Lcom/google/android/gms/car/ev;->a(Ljava/lang/String;I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 112
    const-string v0, "CAR.DIAGNOSTICS"

    new-instance v1, Ljava/lang/StringBuilder;

    const-string v4, "Crash for package: "

    invoke-direct {v1, v4}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_2

    .line 115
    :catch_0
    move-exception v0

    .line 116
    const-string v1, "CAR.FEEDBACK"

    const-string v2, "Unable to create FeedbackOptions."

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_2
.end method
